PGDMP         -    
            z           sinea    12.2    12.2 �   �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    100927    sinea    DATABASE     �   CREATE DATABASE sinea WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'French_France.1252' LC_CTYPE = 'French_France.1252';
    DROP DATABASE sinea;
                postgres    false                        3079    100928    postgis 	   EXTENSION     ;   CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;
    DROP EXTENSION postgis;
                   false            �           0    0    EXTENSION postgis    COMMENT     ^   COMMENT ON EXTENSION postgis IS 'PostGIS geometry and geography spatial types and functions';
                        false    2            �            1259    101950 	   abreuvoir    TABLE       CREATE TABLE public.abreuvoir (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    code_ins character varying(255),
    latitude double precision,
    longitude double precision,
    nb_robinets integer,
    nb_robinets_fonctionnels integer,
    numero_abreuvoir integer,
    code_systeme_aep integer,
    is_deleted_central boolean,
    is_deleted_regional boolean
);
    DROP TABLE public.abreuvoir;
       public         heap    postgres    false            �            1259    101958    borne_fontaine    TABLE     :  CREATE TABLE public.borne_fontaine (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    code_ins character varying(255),
    compteur_installe boolean,
    latitude double precision,
    longitude double precision,
    nb_robinets integer,
    nb_robinets_fonctionnels integer,
    numero_bf integer,
    code_systeme_aep integer,
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 "   DROP TABLE public.borne_fontaine;
       public         heap    postgres    false            �            1259    101966    branchement_particulier    TABLE     )  CREATE TABLE public.branchement_particulier (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    code_ins character varying(255),
    compteur_installe boolean,
    latitude double precision,
    longitude double precision,
    nom_lieu character varying(255),
    code_lieu integer,
    code_systeme_aep integer,
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 +   DROP TABLE public.branchement_particulier;
       public         heap    postgres    false            �            1259    101974    captage_systeme_aep    TABLE       CREATE TABLE public.captage_systeme_aep (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    hmt double precision,
    cote_installation_pompe double precision,
    couvercle_protection_tete boolean,
    date_installation timestamp without time zone,
    date_installation2 timestamp without time zone,
    debit_nominale double precision,
    diametre_colonne double precision,
    diametre_equipement double precision,
    etat_compteur_forage boolean,
    index_compteur_forage double precision,
    longeur_colonne double precision,
    marque_module character varying(255),
    marque_onduleur character varying(255),
    nombre_module integer,
    possibilite_mesure_piezometrique boolean,
    profondeur_niveau_statique double precision,
    profondeur_totale_mesure double precision,
    protection_tete_forage boolean,
    puissance double precision,
    puissance2 double precision,
    puissance_compteur_electrique double precision,
    puissance_onduleur double precision,
    puissance_totale double precision,
    puissance_transformateur double precision,
    puissance_unitaire_module double precision,
    tubage_protection_tete boolean,
    type_colonne_exhaure character varying(255),
    type_tableau_distribution character varying(255),
    voltage double precision,
    code_forage integer,
    code_marque_pompe integer,
    code_marque_pompe2 integer,
    code_modele_pompe integer,
    code_modele_pompe2 integer,
    code_systeme_aep integer,
    code_type_systeme_installation integer,
    code_type_systeme_protection integer,
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 '   DROP TABLE public.captage_systeme_aep;
       public         heap    postgres    false            �            1259    101982    chateau_eau    TABLE        CREATE TABLE public.chateau_eau (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    capacite double precision,
    code_ins character varying(255),
    date_demarrage_travaux timestamp without time zone,
    date_fin_travaux timestamp without time zone,
    existence_compteur boolean,
    hauteur double precision,
    index_compteur double precision,
    numero_chateau integer,
    code_disposition integer,
    code_hydraulique integer,
    code_materiaux integer,
    code_systeme_aep integer,
    code_tuyauterie integer,
    is_deleted_central boolean,
    is_deleted_regional boolean
);
    DROP TABLE public.chateau_eau;
       public         heap    postgres    false            �            1259    101990    chronique_observation    TABLE     �  CREATE TABLE public.chronique_observation (
    date_debut_observation date NOT NULL,
    date_fin_observation date NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    valeur_mesure double precision,
    code_station integer NOT NULL,
    code_parametre_observe integer NOT NULL,
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 )   DROP TABLE public.chronique_observation;
       public         heap    postgres    false            �            1259    102000    classification    TABLE     �  CREATE TABLE public.classification (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle text,
    niveau integer,
    parent_code integer,
    code_type_classification integer,
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 "   DROP TABLE public.classification;
       public         heap    postgres    false            �            1259    101998    classification_code_seq    SEQUENCE     �   CREATE SEQUENCE public.classification_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.classification_code_seq;
       public          postgres    false    215            �           0    0    classification_code_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.classification_code_seq OWNED BY public.classification.code;
          public          postgres    false    214            �            1259    102009    description_geologique    TABLE     �  CREATE TABLE public.description_geologique (
    code_pem integer NOT NULL,
    cote_inferieure double precision NOT NULL,
    cote_superieure double precision NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    code_nature_sol integer,
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 *   DROP TABLE public.description_geologique;
       public         heap    postgres    false            �            1259    102019    disposition    TABLE     d  CREATE TABLE public.disposition (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
    DROP TABLE public.disposition;
       public         heap    postgres    false            �            1259    102017    disposition_code_seq    SEQUENCE     �   CREATE SEQUENCE public.disposition_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.disposition_code_seq;
       public          postgres    false    218            �           0    0    disposition_code_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.disposition_code_seq OWNED BY public.disposition.code;
          public          postgres    false    217            �            1259    102028    donnee_indicateur    TABLE     $  CREATE TABLE public.donnee_indicateur (
    code_indicateur integer NOT NULL,
    code_periode integer NOT NULL,
    code_source integer NOT NULL,
    code_sous_groupe integer NOT NULL,
    code_unite integer NOT NULL,
    code_zone integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    valeur double precision NOT NULL,
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 %   DROP TABLE public.donnee_indicateur;
       public         heap    postgres    false            �            1259    102036    equipement_forage    TABLE       CREATE TABLE public.equipement_forage (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    cote_inferieure double precision,
    cote_superieure double precision,
    diametre double precision,
    numero_tube integer,
    code_nature_tube integer,
    pem_code integer,
    code_type_tube integer,
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 %   DROP TABLE public.equipement_forage;
       public         heap    postgres    false            �            1259    102044    equipement_installe    TABLE     �  CREATE TABLE public.equipement_installe (
    code_station integer NOT NULL,
    code_type_equipement_station integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    date_installation timestamp without time zone,
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 '   DROP TABLE public.equipement_installe;
       public         heap    postgres    false            �            1259    102054    etablissement_scolaire    TABLE       CREATE TABLE public.etablissement_scolaire (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    annee_creation integer,
    denomination character varying(255),
    latitude double precision,
    longitude double precision,
    code_localite integer,
    code_type_etablissement_scolaire integer,
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 *   DROP TABLE public.etablissement_scolaire;
       public         heap    postgres    false            �            1259    102052    etablissement_scolaire_code_seq    SEQUENCE     �   CREATE SEQUENCE public.etablissement_scolaire_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 6   DROP SEQUENCE public.etablissement_scolaire_code_seq;
       public          postgres    false    223            �           0    0    etablissement_scolaire_code_seq    SEQUENCE OWNED BY     c   ALTER SEQUENCE public.etablissement_scolaire_code_seq OWNED BY public.etablissement_scolaire.code;
          public          postgres    false    222            �            1259    102065    etat_ouvrage    TABLE     e  CREATE TABLE public.etat_ouvrage (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
     DROP TABLE public.etat_ouvrage;
       public         heap    postgres    false            �            1259    102063    etat_ouvrage_code_seq    SEQUENCE     �   CREATE SEQUENCE public.etat_ouvrage_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.etat_ouvrage_code_seq;
       public          postgres    false    225            �           0    0    etat_ouvrage_code_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.etat_ouvrage_code_seq OWNED BY public.etat_ouvrage.code;
          public          postgres    false    224            �            1259    102076    fiche_technique    TABLE     J  CREATE TABLE public.fiche_technique (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    commentaire_limite text,
    definition text,
    donnees_requises text,
    methode_calcul text,
    methode_collecte text,
    niveau_desagregation text,
    periodicite character varying(255),
    source text,
    unite text,
    code_indicateur integer,
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 #   DROP TABLE public.fiche_technique;
       public         heap    postgres    false            �            1259    102074    fiche_technique_code_seq    SEQUENCE     �   CREATE SEQUENCE public.fiche_technique_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.fiche_technique_code_seq;
       public          postgres    false    227            �           0    0    fiche_technique_code_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.fiche_technique_code_seq OWNED BY public.fiche_technique.code;
          public          postgres    false    226            �            1259    102087    financement    TABLE     d  CREATE TABLE public.financement (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
    DROP TABLE public.financement;
       public         heap    postgres    false            �            1259    102085    financement_code_seq    SEQUENCE     �   CREATE SEQUENCE public.financement_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.financement_code_seq;
       public          postgres    false    229            �           0    0    financement_code_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.financement_code_seq OWNED BY public.financement.code;
          public          postgres    false    228            �            1259    102096    forage    TABLE     7  CREATE TABLE public.forage (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    annee_realisation timestamp without time zone,
    autre_info_margelle character varying(255),
    hauteur_margelle double precision,
    latitude double precision,
    longitude double precision,
    nom_pem character varying(255),
    numero_irh character varying(255),
    renseignement_divers character varying(255),
    code_etat_ouvrage integer,
    code_financement integer,
    code_localite integer,
    code_projet integer,
    code_propriete integer,
    date_demarrage_foration timestamp without time zone,
    date_fin_foration timestamp without time zone,
    date_mesure timestamp without time zone,
    debit_au_soufflage double precision,
    developpement_effectue boolean,
    essai_longue_duree_debit_maximum_estime double precision,
    essai_longue_duree_duree_pompage double precision,
    essai_longue_duree_rabattement_mesure double precision,
    essai_simplifie_debit_maximum_estime double precision,
    essai_simplifie_duree_pompage double precision,
    essai_simplifie_rabattement_mesure double precision,
    forage_positif boolean,
    profondeur_niveau_statique double precision,
    profondeur_totale_equipee double precision,
    profondeur_totale_foree double precision,
    code_type_amenagement integer,
    code_type_usage integer,
    is_deleted_central boolean,
    is_deleted_regional boolean
);
    DROP TABLE public.forage;
       public         heap    postgres    false            �            1259    102106    formation_sanitaire    TABLE       CREATE TABLE public.formation_sanitaire (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    annee_creation integer,
    denomination character varying(255),
    latitude double precision,
    longitude double precision,
    code_localite integer,
    code_type_formation_sanitaire integer,
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 '   DROP TABLE public.formation_sanitaire;
       public         heap    postgres    false            �            1259    102104    formation_sanitaire_code_seq    SEQUENCE     �   CREATE SEQUENCE public.formation_sanitaire_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.formation_sanitaire_code_seq;
       public          postgres    false    232            �           0    0    formation_sanitaire_code_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.formation_sanitaire_code_seq OWNED BY public.formation_sanitaire.code;
          public          postgres    false    231            �            1259    102115    frame    TABLE     ^  CREATE TABLE public.frame (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
    DROP TABLE public.frame;
       public         heap    postgres    false            �            1259    102123    frame_section_frame    TABLE     v   CREATE TABLE public.frame_section_frame (
    frame_code integer NOT NULL,
    section_frame_code integer NOT NULL
);
 '   DROP TABLE public.frame_section_frame;
       public         heap    postgres    false            e           1259    102915    hibernate_sequence    SEQUENCE     {   CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.hibernate_sequence;
       public          postgres    false            �            1259    102128    hydraulique    TABLE     d  CREATE TABLE public.hydraulique (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
    DROP TABLE public.hydraulique;
       public         heap    postgres    false            �            1259    102126    hydraulique_code_seq    SEQUENCE     �   CREATE SEQUENCE public.hydraulique_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.hydraulique_code_seq;
       public          postgres    false    236            �           0    0    hydraulique_code_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.hydraulique_code_seq OWNED BY public.hydraulique.code;
          public          postgres    false    235            �            1259    102139 
   indicateur    TABLE     Q  CREATE TABLE public.indicateur (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle text,
    is_deleted_central boolean,
    is_deleted_regional boolean
);
    DROP TABLE public.indicateur;
       public         heap    postgres    false            �            1259    102148    indicateur_classification    TABLE     �  CREATE TABLE public.indicateur_classification (
    code_classification integer NOT NULL,
    code_indicateur integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 -   DROP TABLE public.indicateur_classification;
       public         heap    postgres    false            �            1259    102137    indicateur_code_seq    SEQUENCE     �   CREATE SEQUENCE public.indicateur_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.indicateur_code_seq;
       public          postgres    false    238            �           0    0    indicateur_code_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.indicateur_code_seq OWNED BY public.indicateur.code;
          public          postgres    false    237            �            1259    102158    intervenant    TABLE     �  CREATE TABLE public.intervenant (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    denomination character varying(255),
    code_type_intervenant integer,
    is_deleted_central boolean,
    is_deleted_regional boolean
);
    DROP TABLE public.intervenant;
       public         heap    postgres    false            �            1259    102156    intervenant_code_seq    SEQUENCE     �   CREATE SEQUENCE public.intervenant_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.intervenant_code_seq;
       public          postgres    false    241            �           0    0    intervenant_code_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.intervenant_code_seq OWNED BY public.intervenant.code;
          public          postgres    false    240            �            1259    102169    lieu    TABLE     ]  CREATE TABLE public.lieu (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
    DROP TABLE public.lieu;
       public         heap    postgres    false            �            1259    102167    lieu_code_seq    SEQUENCE     �   CREATE SEQUENCE public.lieu_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.lieu_code_seq;
       public          postgres    false    243            �           0    0    lieu_code_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.lieu_code_seq OWNED BY public.lieu.code;
          public          postgres    false    242            �            1259    102180    lieu_public    TABLE       CREATE TABLE public.lieu_public (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    annee_creation integer,
    denomination character varying(255),
    latitude double precision,
    longitude double precision,
    code_localite integer,
    code_type_lieu_public integer,
    is_deleted_central boolean,
    is_deleted_regional boolean
);
    DROP TABLE public.lieu_public;
       public         heap    postgres    false            �            1259    102178    lieu_public_code_seq    SEQUENCE     �   CREATE SEQUENCE public.lieu_public_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.lieu_public_code_seq;
       public          postgres    false    245            �           0    0    lieu_public_code_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.lieu_public_code_seq OWNED BY public.lieu_public.code;
          public          postgres    false    244            �            1259    102191    localite    TABLE     @  CREATE TABLE public.localite (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    code_ins character varying(255),
    latitude double precision,
    libelle character varying(255),
    longitude double precision,
    code_commune integer,
    code_localite_rattachement integer,
    code_type_localite integer,
    code_milieu integer,
    is_deleted_central boolean,
    is_deleted_regional boolean
);
    DROP TABLE public.localite;
       public         heap    postgres    false            �            1259    102189    localite_code_seq    SEQUENCE     �   CREATE SEQUENCE public.localite_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.localite_code_seq;
       public          postgres    false    247            �           0    0    localite_code_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.localite_code_seq OWNED BY public.localite.code;
          public          postgres    false    246            �            1259    102202    marque_pompe    TABLE     e  CREATE TABLE public.marque_pompe (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
     DROP TABLE public.marque_pompe;
       public         heap    postgres    false            �            1259    102200    marque_pompe_code_seq    SEQUENCE     �   CREATE SEQUENCE public.marque_pompe_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.marque_pompe_code_seq;
       public          postgres    false    249            �           0    0    marque_pompe_code_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.marque_pompe_code_seq OWNED BY public.marque_pompe.code;
          public          postgres    false    248            �            1259    102213 	   materiaux    TABLE     b  CREATE TABLE public.materiaux (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
    DROP TABLE public.materiaux;
       public         heap    postgres    false            �            1259    102211    materiaux_code_seq    SEQUENCE     �   CREATE SEQUENCE public.materiaux_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.materiaux_code_seq;
       public          postgres    false    251            �           0    0    materiaux_code_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.materiaux_code_seq OWNED BY public.materiaux.code;
          public          postgres    false    250            �            1259    102224    methode_collecte    TABLE     i  CREATE TABLE public.methode_collecte (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 $   DROP TABLE public.methode_collecte;
       public         heap    postgres    false            �            1259    102222    methode_collecte_code_seq    SEQUENCE     �   CREATE SEQUENCE public.methode_collecte_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.methode_collecte_code_seq;
       public          postgres    false    253            �           0    0    methode_collecte_code_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public.methode_collecte_code_seq OWNED BY public.methode_collecte.code;
          public          postgres    false    252            �            1259    102235    milieu    TABLE     _  CREATE TABLE public.milieu (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
    DROP TABLE public.milieu;
       public         heap    postgres    false            �            1259    102233    milieu_code_seq    SEQUENCE     �   CREATE SEQUENCE public.milieu_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.milieu_code_seq;
       public          postgres    false    255            �           0    0    milieu_code_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.milieu_code_seq OWNED BY public.milieu.code;
          public          postgres    false    254                       1259    102246    modele_pompe    TABLE     e  CREATE TABLE public.modele_pompe (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
     DROP TABLE public.modele_pompe;
       public         heap    postgres    false                        1259    102244    modele_pompe_code_seq    SEQUENCE     �   CREATE SEQUENCE public.modele_pompe_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.modele_pompe_code_seq;
       public          postgres    false    257            �           0    0    modele_pompe_code_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.modele_pompe_code_seq OWNED BY public.modele_pompe.code;
          public          postgres    false    256                       1259    102257 
   nature_sol    TABLE     c  CREATE TABLE public.nature_sol (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
    DROP TABLE public.nature_sol;
       public         heap    postgres    false                       1259    102255    nature_sol_code_seq    SEQUENCE     �   CREATE SEQUENCE public.nature_sol_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.nature_sol_code_seq;
       public          postgres    false    259            �           0    0    nature_sol_code_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.nature_sol_code_seq OWNED BY public.nature_sol.code;
          public          postgres    false    258                       1259    102268    nature_tube    TABLE     d  CREATE TABLE public.nature_tube (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
    DROP TABLE public.nature_tube;
       public         heap    postgres    false                       1259    102266    nature_tube_code_seq    SEQUENCE     �   CREATE SEQUENCE public.nature_tube_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.nature_tube_code_seq;
       public          postgres    false    261            �           0    0    nature_tube_code_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.nature_tube_code_seq OWNED BY public.nature_tube.code;
          public          postgres    false    260                       1259    102279    parametre_station_hydrometrique    TABLE     x  CREATE TABLE public.parametre_station_hydrometrique (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 3   DROP TABLE public.parametre_station_hydrometrique;
       public         heap    postgres    false                       1259    102277 (   parametre_station_hydrometrique_code_seq    SEQUENCE     �   CREATE SEQUENCE public.parametre_station_hydrometrique_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.parametre_station_hydrometrique_code_seq;
       public          postgres    false    263            �           0    0 (   parametre_station_hydrometrique_code_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.parametre_station_hydrometrique_code_seq OWNED BY public.parametre_station_hydrometrique.code;
          public          postgres    false    262            	           1259    102290    parametre_station_meteo    TABLE     p  CREATE TABLE public.parametre_station_meteo (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 +   DROP TABLE public.parametre_station_meteo;
       public         heap    postgres    false                       1259    102288     parametre_station_meteo_code_seq    SEQUENCE     �   CREATE SEQUENCE public.parametre_station_meteo_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.parametre_station_meteo_code_seq;
       public          postgres    false    265            �           0    0     parametre_station_meteo_code_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.parametre_station_meteo_code_seq OWNED BY public.parametre_station_meteo.code;
          public          postgres    false    264                       1259    102301    periode    TABLE     �  CREATE TABLE public.periode (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    niveau integer,
    parent_code integer,
    is_deleted_central boolean,
    is_deleted_regional boolean
);
    DROP TABLE public.periode;
       public         heap    postgres    false            
           1259    102299    periode_code_seq    SEQUENCE     �   CREATE SEQUENCE public.periode_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.periode_code_seq;
       public          postgres    false    267            �           0    0    periode_code_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.periode_code_seq OWNED BY public.periode.code;
          public          postgres    false    266                       1259    102310    piece_regulation_reseau    TABLE     �  CREATE TABLE public.piece_regulation_reseau (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    latitude double precision,
    longitude double precision,
    numero_ordre integer,
    code_systeme_aep integer,
    type_piece_regulation_reseau integer,
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 +   DROP TABLE public.piece_regulation_reseau;
       public         heap    postgres    false                       1259    102318 
   piezometre    TABLE     �  CREATE TABLE public.piezometre (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    date_observation date,
    hauteur_piezo double precision,
    heure_observation character varying(255),
    profondeur_piezo double precision,
    pem_code integer,
    is_deleted_central boolean,
    is_deleted_regional boolean
);
    DROP TABLE public.piezometre;
       public         heap    postgres    false                       1259    102326    pompe_installe    TABLE     �  CREATE TABLE public.pompe_installe (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    date_installation_pompe timestamp without time zone,
    code_marque_pompe integer,
    code_modele_pompe integer,
    code_pem integer,
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 "   DROP TABLE public.pompe_installe;
       public         heap    postgres    false                       1259    102334    prestataire    TABLE     �  CREATE TABLE public.prestataire (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    cout_prestation double precision,
    nom_prestataire character varying(255),
    code_pem integer,
    code_type_prestation integer,
    is_deleted_central boolean,
    is_deleted_regional boolean
);
    DROP TABLE public.prestataire;
       public         heap    postgres    false                       1259    102342 	   privilege    TABLE     b  CREATE TABLE public.privilege (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
    DROP TABLE public.privilege;
       public         heap    postgres    false                       1259    102352    projet    TABLE     _  CREATE TABLE public.projet (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
    DROP TABLE public.projet;
       public         heap    postgres    false                       1259    102350    projet_code_seq    SEQUENCE     �   CREATE SEQUENCE public.projet_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.projet_code_seq;
       public          postgres    false    274            �           0    0    projet_code_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.projet_code_seq OWNED BY public.projet.code;
          public          postgres    false    273                       1259    102363 	   propriete    TABLE     b  CREATE TABLE public.propriete (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
    DROP TABLE public.propriete;
       public         heap    postgres    false                       1259    102361    propriete_code_seq    SEQUENCE     �   CREATE SEQUENCE public.propriete_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.propriete_code_seq;
       public          postgres    false    276            �           0    0    propriete_code_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.propriete_code_seq OWNED BY public.propriete.code;
          public          postgres    false    275                       1259    102372    puits_cimente    TABLE     u  CREATE TABLE public.puits_cimente (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    annee_realisation timestamp without time zone,
    autre_info_margelle character varying(255),
    hauteur_margelle double precision,
    latitude double precision,
    longitude double precision,
    nom_pem character varying(255),
    numero_irh character varying(255),
    renseignement_divers character varying(255),
    code_etat_ouvrage integer,
    code_financement integer,
    code_localite integer,
    code_projet integer,
    code_propriete integer,
    escieh_debit_maximum_estime double precision,
    escieh_duree_pompage double precision,
    escieh_rabattement_mesure double precision,
    abreuvoirs integer,
    aire_assainie boolean,
    amenagement_surface boolean,
    antibourbier boolean,
    canal_evacuation_et_puits_perdu boolean,
    couleur_eau boolean,
    couvercle boolean,
    dalle_fond boolean,
    date_mesure timestamp without time zone,
    developpement_effectue_au_cuffat boolean,
    diametre_captage double precision,
    diametre_cuvelage double precision,
    fourche boolean,
    hauteur_captage double precision,
    hauteur_massif_filtrant double precision,
    hauteur_recouvrement double precision,
    mur_cloture boolean,
    nombre_buses_captage integer,
    pompe_immergee boolean,
    portique integer,
    potabilite_eau boolean,
    profondeur_cuvelee double precision,
    profondeur_niveau_statique double precision,
    profondeur_sommet_colonne_captage double precision,
    profondeur_totale double precision,
    puisette boolean,
    trousse_coupante boolean,
    code_type_exhaure integer,
    code_type_puits integer,
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 !   DROP TABLE public.puits_cimente;
       public         heap    postgres    false            g           1259    103729    recensement_ins    TABLE       CREATE TABLE public.recensement_ins (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    annee_recensement integer,
    nombre_menage integer,
    population_femme integer,
    population_homme integer,
    taux_accroissement double precision NOT NULL,
    code_localite integer,
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 #   DROP TABLE public.recensement_ins;
       public         heap    postgres    false            f           1259    103727    recensement_ins_code_seq    SEQUENCE     �   CREATE SEQUENCE public.recensement_ins_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.recensement_ins_code_seq;
       public          postgres    false    359            �           0    0    recensement_ins_code_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.recensement_ins_code_seq OWNED BY public.recensement_ins.code;
          public          postgres    false    358                       1259    102391    reseau_distribution    TABLE     q  CREATE TABLE public.reseau_distribution (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    depart_latitude double precision,
    depart_longitude double precision,
    diametre double precision,
    fin_latitude double precision,
    fin_longitude double precision,
    longueur double precision,
    numero_troncon integer,
    code_systeme_aep integer,
    code_tuyauterie integer,
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 '   DROP TABLE public.reseau_distribution;
       public         heap    postgres    false                       1259    102399    reseau_refoulement    TABLE     p  CREATE TABLE public.reseau_refoulement (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    depart_latitude double precision,
    depart_longitude double precision,
    diametre double precision,
    fin_latitude double precision,
    fin_longitude double precision,
    longueur double precision,
    numero_troncon integer,
    code_systeme_aep integer,
    code_tuyauterie integer,
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 &   DROP TABLE public.reseau_refoulement;
       public         heap    postgres    false                       1259    102409    reseau_suivi    TABLE     �  CREATE TABLE public.reseau_suivi (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    commentaire character varying(255),
    nom character varying(255),
    proprietaire character varying(255),
    structure_en_charge character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
     DROP TABLE public.reseau_suivi;
       public         heap    postgres    false                       1259    102407    reseau_suivi_code_seq    SEQUENCE     �   CREATE SEQUENCE public.reseau_suivi_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.reseau_suivi_code_seq;
       public          postgres    false    281            �           0    0    reseau_suivi_code_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.reseau_suivi_code_seq OWNED BY public.reseau_suivi.code;
          public          postgres    false    280                       1259    102418    role    TABLE     ]  CREATE TABLE public.role (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
    DROP TABLE public.role;
       public         heap    postgres    false                       1259    102426    roles_privileges    TABLE     n   CREATE TABLE public.roles_privileges (
    role_code integer NOT NULL,
    privilege_code integer NOT NULL
);
 $   DROP TABLE public.roles_privileges;
       public         heap    postgres    false                       1259    102431    section_frame    TABLE     f  CREATE TABLE public.section_frame (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 !   DROP TABLE public.section_frame;
       public         heap    postgres    false                       1259    102429    section_frame_code_seq    SEQUENCE     �   CREATE SEQUENCE public.section_frame_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.section_frame_code_seq;
       public          postgres    false    285            �           0    0    section_frame_code_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.section_frame_code_seq OWNED BY public.section_frame.code;
          public          postgres    false    284                       1259    102440    section_frame_indicateurs    TABLE     t   CREATE TABLE public.section_frame_indicateurs (
    section_frame_code integer NOT NULL,
    indicateurs integer
);
 -   DROP TABLE public.section_frame_indicateurs;
       public         heap    postgres    false                       1259    102443    section_frame_periodes    TABLE     n   CREATE TABLE public.section_frame_periodes (
    section_frame_code integer NOT NULL,
    periodes integer
);
 *   DROP TABLE public.section_frame_periodes;
       public         heap    postgres    false                        1259    102446    section_frame_sources    TABLE     l   CREATE TABLE public.section_frame_sources (
    section_frame_code integer NOT NULL,
    sources integer
);
 )   DROP TABLE public.section_frame_sources;
       public         heap    postgres    false            !           1259    102449    section_frame_zones    TABLE     h   CREATE TABLE public.section_frame_zones (
    section_frame_code integer NOT NULL,
    zones integer
);
 '   DROP TABLE public.section_frame_zones;
       public         heap    postgres    false            #           1259    102454    source    TABLE     _  CREATE TABLE public.source (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
    DROP TABLE public.source;
       public         heap    postgres    false            "           1259    102452    source_code_seq    SEQUENCE     �   CREATE SEQUENCE public.source_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.source_code_seq;
       public          postgres    false    291            �           0    0    source_code_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.source_code_seq OWNED BY public.source.code;
          public          postgres    false    290            %           1259    102465    sous_groupe    TABLE     �  CREATE TABLE public.sous_groupe (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    niveau integer,
    parent_code integer,
    is_deleted_central boolean,
    is_deleted_regional boolean
);
    DROP TABLE public.sous_groupe;
       public         heap    postgres    false            $           1259    102463    sous_groupe_code_seq    SEQUENCE     �   CREATE SEQUENCE public.sous_groupe_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.sous_groupe_code_seq;
       public          postgres    false    293            �           0    0    sous_groupe_code_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.sous_groupe_code_seq OWNED BY public.sous_groupe.code;
          public          postgres    false    292            &           1259    102474    sous_groupe_indicateur    TABLE     }  CREATE TABLE public.sous_groupe_indicateur (
    code_indicateur integer NOT NULL,
    code_sous_groupe integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 *   DROP TABLE public.sous_groupe_indicateur;
       public         heap    postgres    false            (           1259    102484    station    TABLE     q  CREATE TABLE public.station (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    altitude double precision,
    cause_fermeture character varying(255),
    date_fermeture date,
    date_ouverture date,
    latitude double precision,
    longitude double precision,
    nom character varying(255),
    code_localite integer,
    code_reseau_suivi integer,
    code_type_station integer,
    is_deleted_central boolean,
    is_deleted_regional boolean
);
    DROP TABLE public.station;
       public         heap    postgres    false            '           1259    102482    station_code_seq    SEQUENCE     �   CREATE SEQUENCE public.station_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.station_code_seq;
       public          postgres    false    296            �           0    0    station_code_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.station_code_seq OWNED BY public.station.code;
          public          postgres    false    295            *           1259    102495 	   structure    TABLE     b  CREATE TABLE public.structure (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
    DROP TABLE public.structure;
       public         heap    postgres    false            )           1259    102493    structure_code_seq    SEQUENCE     �   CREATE SEQUENCE public.structure_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.structure_code_seq;
       public          postgres    false    298            �           0    0    structure_code_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.structure_code_seq OWNED BY public.structure.code;
          public          postgres    false    297            ,           1259    102506    systeme_aep    TABLE     o  CREATE TABLE public.systeme_aep (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    annee_realisation timestamp without time zone,
    nom_aep character varying(255),
    numero_irh character varying(255),
    precision_energie_autres character varying(255),
    precision_source_principale_autres character varying(255),
    source_energie_autre boolean,
    source_energie_eolienne boolean,
    source_energie_reseau_electrique boolean,
    source_energie_solaire boolean,
    source_energie_thermique boolean,
    source_principale_autre boolean,
    source_principale_eolienne boolean,
    source_principale_reseau_electrique boolean,
    source_principale_solaire boolean,
    source_principale_thermique boolean,
    code_etat_ouvrage integer,
    code_financement integer,
    code_localite integer,
    code_projet integer,
    code_propriete integer,
    code_type_systeme_aep integer,
    is_deleted_central boolean,
    is_deleted_regional boolean
);
    DROP TABLE public.systeme_aep;
       public         heap    postgres    false            +           1259    102504    systeme_aep_code_seq    SEQUENCE     �   CREATE SEQUENCE public.systeme_aep_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.systeme_aep_code_seq;
       public          postgres    false    300            �           0    0    systeme_aep_code_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.systeme_aep_code_seq OWNED BY public.systeme_aep.code;
          public          postgres    false    299            .           1259    102517    t_user    TABLE       CREATE TABLE public.t_user (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    active boolean NOT NULL,
    login character varying(255),
    mot_passe character varying(255),
    nom character varying(255),
    prenom character varying(255),
    token_expire boolean NOT NULL,
    is_deleted_central boolean,
    is_deleted_regional boolean
);
    DROP TABLE public.t_user;
       public         heap    postgres    false            -           1259    102515    t_user_code_seq    SEQUENCE     �   CREATE SEQUENCE public.t_user_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.t_user_code_seq;
       public          postgres    false    302            �           0    0    t_user_code_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.t_user_code_seq OWNED BY public.t_user.code;
          public          postgres    false    301            0           1259    102528 
   tuyauterie    TABLE     c  CREATE TABLE public.tuyauterie (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
    DROP TABLE public.tuyauterie;
       public         heap    postgres    false            /           1259    102526    tuyauterie_code_seq    SEQUENCE     �   CREATE SEQUENCE public.tuyauterie_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.tuyauterie_code_seq;
       public          postgres    false    304            �           0    0    tuyauterie_code_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.tuyauterie_code_seq OWNED BY public.tuyauterie.code;
          public          postgres    false    303            2           1259    102539    type_amenagement    TABLE     i  CREATE TABLE public.type_amenagement (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 $   DROP TABLE public.type_amenagement;
       public         heap    postgres    false            1           1259    102537    type_amenagement_code_seq    SEQUENCE     �   CREATE SEQUENCE public.type_amenagement_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.type_amenagement_code_seq;
       public          postgres    false    306            �           0    0    type_amenagement_code_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public.type_amenagement_code_seq OWNED BY public.type_amenagement.code;
          public          postgres    false    305            4           1259    102550    type_classification    TABLE     Z  CREATE TABLE public.type_classification (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle text,
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 '   DROP TABLE public.type_classification;
       public         heap    postgres    false            3           1259    102548    type_classification_code_seq    SEQUENCE     �   CREATE SEQUENCE public.type_classification_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.type_classification_code_seq;
       public          postgres    false    308            �           0    0    type_classification_code_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.type_classification_code_seq OWNED BY public.type_classification.code;
          public          postgres    false    307            6           1259    102561 %   type_equipement_station_hydrometrique    TABLE     ~  CREATE TABLE public.type_equipement_station_hydrometrique (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 9   DROP TABLE public.type_equipement_station_hydrometrique;
       public         heap    postgres    false            5           1259    102559 .   type_equipement_station_hydrometrique_code_seq    SEQUENCE     �   CREATE SEQUENCE public.type_equipement_station_hydrometrique_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 E   DROP SEQUENCE public.type_equipement_station_hydrometrique_code_seq;
       public          postgres    false    310            �           0    0 .   type_equipement_station_hydrometrique_code_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.type_equipement_station_hydrometrique_code_seq OWNED BY public.type_equipement_station_hydrometrique.code;
          public          postgres    false    309            8           1259    102572    type_equipement_station_meteo    TABLE     v  CREATE TABLE public.type_equipement_station_meteo (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 1   DROP TABLE public.type_equipement_station_meteo;
       public         heap    postgres    false            7           1259    102570 &   type_equipement_station_meteo_code_seq    SEQUENCE     �   CREATE SEQUENCE public.type_equipement_station_meteo_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 =   DROP SEQUENCE public.type_equipement_station_meteo_code_seq;
       public          postgres    false    312            �           0    0 &   type_equipement_station_meteo_code_seq    SEQUENCE OWNED BY     q   ALTER SEQUENCE public.type_equipement_station_meteo_code_seq OWNED BY public.type_equipement_station_meteo.code;
          public          postgres    false    311            :           1259    102583    type_etablissement_scolaire    TABLE     t  CREATE TABLE public.type_etablissement_scolaire (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 /   DROP TABLE public.type_etablissement_scolaire;
       public         heap    postgres    false            9           1259    102581 $   type_etablissement_scolaire_code_seq    SEQUENCE     �   CREATE SEQUENCE public.type_etablissement_scolaire_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ;   DROP SEQUENCE public.type_etablissement_scolaire_code_seq;
       public          postgres    false    314                        0    0 $   type_etablissement_scolaire_code_seq    SEQUENCE OWNED BY     m   ALTER SEQUENCE public.type_etablissement_scolaire_code_seq OWNED BY public.type_etablissement_scolaire.code;
          public          postgres    false    313            <           1259    102594    type_exhaure    TABLE     e  CREATE TABLE public.type_exhaure (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
     DROP TABLE public.type_exhaure;
       public         heap    postgres    false            ;           1259    102592    type_exhaure_code_seq    SEQUENCE     �   CREATE SEQUENCE public.type_exhaure_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.type_exhaure_code_seq;
       public          postgres    false    316                       0    0    type_exhaure_code_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.type_exhaure_code_seq OWNED BY public.type_exhaure.code;
          public          postgres    false    315            >           1259    102605    type_formation_sanitaire    TABLE     q  CREATE TABLE public.type_formation_sanitaire (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 ,   DROP TABLE public.type_formation_sanitaire;
       public         heap    postgres    false            =           1259    102603 !   type_formation_sanitaire_code_seq    SEQUENCE     �   CREATE SEQUENCE public.type_formation_sanitaire_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 8   DROP SEQUENCE public.type_formation_sanitaire_code_seq;
       public          postgres    false    318                       0    0 !   type_formation_sanitaire_code_seq    SEQUENCE OWNED BY     g   ALTER SEQUENCE public.type_formation_sanitaire_code_seq OWNED BY public.type_formation_sanitaire.code;
          public          postgres    false    317            @           1259    102616    type_intervenant    TABLE     i  CREATE TABLE public.type_intervenant (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 $   DROP TABLE public.type_intervenant;
       public         heap    postgres    false            ?           1259    102614    type_intervenant_code_seq    SEQUENCE     �   CREATE SEQUENCE public.type_intervenant_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.type_intervenant_code_seq;
       public          postgres    false    320                       0    0    type_intervenant_code_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public.type_intervenant_code_seq OWNED BY public.type_intervenant.code;
          public          postgres    false    319            B           1259    102627    type_lieu_public    TABLE     i  CREATE TABLE public.type_lieu_public (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 $   DROP TABLE public.type_lieu_public;
       public         heap    postgres    false            A           1259    102625    type_lieu_public_code_seq    SEQUENCE     �   CREATE SEQUENCE public.type_lieu_public_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.type_lieu_public_code_seq;
       public          postgres    false    322                       0    0    type_lieu_public_code_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public.type_lieu_public_code_seq OWNED BY public.type_lieu_public.code;
          public          postgres    false    321            D           1259    102638    type_localite    TABLE     T  CREATE TABLE public.type_localite (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle text,
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 !   DROP TABLE public.type_localite;
       public         heap    postgres    false            C           1259    102636    type_localite_code_seq    SEQUENCE     �   CREATE SEQUENCE public.type_localite_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.type_localite_code_seq;
       public          postgres    false    324                       0    0    type_localite_code_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.type_localite_code_seq OWNED BY public.type_localite.code;
          public          postgres    false    323            F           1259    102649    type_ouvrage    TABLE     e  CREATE TABLE public.type_ouvrage (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
     DROP TABLE public.type_ouvrage;
       public         heap    postgres    false            E           1259    102647    type_ouvrage_code_seq    SEQUENCE     �   CREATE SEQUENCE public.type_ouvrage_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.type_ouvrage_code_seq;
       public          postgres    false    326                       0    0    type_ouvrage_code_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.type_ouvrage_code_seq OWNED BY public.type_ouvrage.code;
          public          postgres    false    325            H           1259    102660    type_piece_regulation_reseau    TABLE     u  CREATE TABLE public.type_piece_regulation_reseau (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 0   DROP TABLE public.type_piece_regulation_reseau;
       public         heap    postgres    false            G           1259    102658 %   type_piece_regulation_reseau_code_seq    SEQUENCE     �   CREATE SEQUENCE public.type_piece_regulation_reseau_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 <   DROP SEQUENCE public.type_piece_regulation_reseau_code_seq;
       public          postgres    false    328                       0    0 %   type_piece_regulation_reseau_code_seq    SEQUENCE OWNED BY     o   ALTER SEQUENCE public.type_piece_regulation_reseau_code_seq OWNED BY public.type_piece_regulation_reseau.code;
          public          postgres    false    327            J           1259    102671    type_plan_eau    TABLE     f  CREATE TABLE public.type_plan_eau (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 !   DROP TABLE public.type_plan_eau;
       public         heap    postgres    false            I           1259    102669    type_plan_eau_code_seq    SEQUENCE     �   CREATE SEQUENCE public.type_plan_eau_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.type_plan_eau_code_seq;
       public          postgres    false    330                       0    0    type_plan_eau_code_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.type_plan_eau_code_seq OWNED BY public.type_plan_eau.code;
          public          postgres    false    329            L           1259    102682    type_prestation    TABLE     h  CREATE TABLE public.type_prestation (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 #   DROP TABLE public.type_prestation;
       public         heap    postgres    false            K           1259    102680    type_prestation_code_seq    SEQUENCE     �   CREATE SEQUENCE public.type_prestation_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.type_prestation_code_seq;
       public          postgres    false    332            	           0    0    type_prestation_code_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.type_prestation_code_seq OWNED BY public.type_prestation.code;
          public          postgres    false    331            N           1259    102693 
   type_puits    TABLE     c  CREATE TABLE public.type_puits (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
    DROP TABLE public.type_puits;
       public         heap    postgres    false            M           1259    102691    type_puits_code_seq    SEQUENCE     �   CREATE SEQUENCE public.type_puits_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.type_puits_code_seq;
       public          postgres    false    334            
           0    0    type_puits_code_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.type_puits_code_seq OWNED BY public.type_puits.code;
          public          postgres    false    333            P           1259    102704    type_ressource    TABLE     g  CREATE TABLE public.type_ressource (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 "   DROP TABLE public.type_ressource;
       public         heap    postgres    false            O           1259    102702    type_ressource_code_seq    SEQUENCE     �   CREATE SEQUENCE public.type_ressource_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.type_ressource_code_seq;
       public          postgres    false    336                       0    0    type_ressource_code_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.type_ressource_code_seq OWNED BY public.type_ressource.code;
          public          postgres    false    335            R           1259    102715 $   type_ressource_station_hydrometrique    TABLE     }  CREATE TABLE public.type_ressource_station_hydrometrique (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 8   DROP TABLE public.type_ressource_station_hydrometrique;
       public         heap    postgres    false            Q           1259    102713 -   type_ressource_station_hydrometrique_code_seq    SEQUENCE     �   CREATE SEQUENCE public.type_ressource_station_hydrometrique_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 D   DROP SEQUENCE public.type_ressource_station_hydrometrique_code_seq;
       public          postgres    false    338                       0    0 -   type_ressource_station_hydrometrique_code_seq    SEQUENCE OWNED BY        ALTER SEQUENCE public.type_ressource_station_hydrometrique_code_seq OWNED BY public.type_ressource_station_hydrometrique.code;
          public          postgres    false    337            T           1259    102726    type_station_meteo    TABLE     k  CREATE TABLE public.type_station_meteo (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 &   DROP TABLE public.type_station_meteo;
       public         heap    postgres    false            S           1259    102724    type_station_meteo_code_seq    SEQUENCE     �   CREATE SEQUENCE public.type_station_meteo_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.type_station_meteo_code_seq;
       public          postgres    false    340                       0    0    type_station_meteo_code_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE public.type_station_meteo_code_seq OWNED BY public.type_station_meteo.code;
          public          postgres    false    339            V           1259    102737    type_systeme_aep    TABLE     i  CREATE TABLE public.type_systeme_aep (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 $   DROP TABLE public.type_systeme_aep;
       public         heap    postgres    false            U           1259    102735    type_systeme_aep_code_seq    SEQUENCE     �   CREATE SEQUENCE public.type_systeme_aep_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.type_systeme_aep_code_seq;
       public          postgres    false    342                       0    0    type_systeme_aep_code_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public.type_systeme_aep_code_seq OWNED BY public.type_systeme_aep.code;
          public          postgres    false    341            X           1259    102748    type_systeme_installation    TABLE     r  CREATE TABLE public.type_systeme_installation (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 -   DROP TABLE public.type_systeme_installation;
       public         heap    postgres    false            W           1259    102746 "   type_systeme_installation_code_seq    SEQUENCE     �   CREATE SEQUENCE public.type_systeme_installation_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 9   DROP SEQUENCE public.type_systeme_installation_code_seq;
       public          postgres    false    344                       0    0 "   type_systeme_installation_code_seq    SEQUENCE OWNED BY     i   ALTER SEQUENCE public.type_systeme_installation_code_seq OWNED BY public.type_systeme_installation.code;
          public          postgres    false    343            Z           1259    102759    type_systeme_protection    TABLE     p  CREATE TABLE public.type_systeme_protection (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 +   DROP TABLE public.type_systeme_protection;
       public         heap    postgres    false            Y           1259    102757     type_systeme_protection_code_seq    SEQUENCE     �   CREATE SEQUENCE public.type_systeme_protection_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.type_systeme_protection_code_seq;
       public          postgres    false    346                       0    0     type_systeme_protection_code_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.type_systeme_protection_code_seq OWNED BY public.type_systeme_protection.code;
          public          postgres    false    345            \           1259    102770 	   type_tube    TABLE     b  CREATE TABLE public.type_tube (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
    DROP TABLE public.type_tube;
       public         heap    postgres    false            [           1259    102768    type_tube_code_seq    SEQUENCE     �   CREATE SEQUENCE public.type_tube_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.type_tube_code_seq;
       public          postgres    false    348                       0    0    type_tube_code_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.type_tube_code_seq OWNED BY public.type_tube.code;
          public          postgres    false    347            ^           1259    102781 
   type_usage    TABLE     c  CREATE TABLE public.type_usage (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
    DROP TABLE public.type_usage;
       public         heap    postgres    false            ]           1259    102779    type_usage_code_seq    SEQUENCE     �   CREATE SEQUENCE public.type_usage_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.type_usage_code_seq;
       public          postgres    false    350                       0    0    type_usage_code_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.type_usage_code_seq OWNED BY public.type_usage.code;
          public          postgres    false    349            `           1259    102792    unite    TABLE     ^  CREATE TABLE public.unite (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
    DROP TABLE public.unite;
       public         heap    postgres    false            _           1259    102790    unite_code_seq    SEQUENCE     �   CREATE SEQUENCE public.unite_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.unite_code_seq;
       public          postgres    false    352                       0    0    unite_code_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.unite_code_seq OWNED BY public.unite.code;
          public          postgres    false    351            a           1259    102801    unite_indicateur    TABLE     q  CREATE TABLE public.unite_indicateur (
    code_indicateur integer NOT NULL,
    code_unite integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    is_deleted_central boolean,
    is_deleted_regional boolean
);
 $   DROP TABLE public.unite_indicateur;
       public         heap    postgres    false            b           1259    102809    users_roles    TABLE     d   CREATE TABLE public.users_roles (
    user_code integer NOT NULL,
    role_code integer NOT NULL
);
    DROP TABLE public.users_roles;
       public         heap    postgres    false            d           1259    102814    zone    TABLE     �  CREATE TABLE public.zone (
    code integer NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    is_deleted boolean,
    modified_at timestamp without time zone,
    modified_by character varying(255),
    libelle text,
    niveau integer,
    code_milieu integer,
    parent_code integer,
    is_deleted_central boolean,
    is_deleted_regional boolean
);
    DROP TABLE public.zone;
       public         heap    postgres    false            c           1259    102812    zone_code_seq    SEQUENCE     �   CREATE SEQUENCE public.zone_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.zone_code_seq;
       public          postgres    false    356                       0    0    zone_code_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.zone_code_seq OWNED BY public.zone.code;
          public          postgres    false    355                       2604    102003    classification code    DEFAULT     z   ALTER TABLE ONLY public.classification ALTER COLUMN code SET DEFAULT nextval('public.classification_code_seq'::regclass);
 B   ALTER TABLE public.classification ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    214    215    215                       2604    102022    disposition code    DEFAULT     t   ALTER TABLE ONLY public.disposition ALTER COLUMN code SET DEFAULT nextval('public.disposition_code_seq'::regclass);
 ?   ALTER TABLE public.disposition ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    218    217    218                       2604    102057    etablissement_scolaire code    DEFAULT     �   ALTER TABLE ONLY public.etablissement_scolaire ALTER COLUMN code SET DEFAULT nextval('public.etablissement_scolaire_code_seq'::regclass);
 J   ALTER TABLE public.etablissement_scolaire ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    222    223    223                       2604    102068    etat_ouvrage code    DEFAULT     v   ALTER TABLE ONLY public.etat_ouvrage ALTER COLUMN code SET DEFAULT nextval('public.etat_ouvrage_code_seq'::regclass);
 @   ALTER TABLE public.etat_ouvrage ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    224    225    225                       2604    102079    fiche_technique code    DEFAULT     |   ALTER TABLE ONLY public.fiche_technique ALTER COLUMN code SET DEFAULT nextval('public.fiche_technique_code_seq'::regclass);
 C   ALTER TABLE public.fiche_technique ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    227    226    227                       2604    102090    financement code    DEFAULT     t   ALTER TABLE ONLY public.financement ALTER COLUMN code SET DEFAULT nextval('public.financement_code_seq'::regclass);
 ?   ALTER TABLE public.financement ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    229    228    229                       2604    102109    formation_sanitaire code    DEFAULT     �   ALTER TABLE ONLY public.formation_sanitaire ALTER COLUMN code SET DEFAULT nextval('public.formation_sanitaire_code_seq'::regclass);
 G   ALTER TABLE public.formation_sanitaire ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    231    232    232                       2604    102131    hydraulique code    DEFAULT     t   ALTER TABLE ONLY public.hydraulique ALTER COLUMN code SET DEFAULT nextval('public.hydraulique_code_seq'::regclass);
 ?   ALTER TABLE public.hydraulique ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    235    236    236                       2604    102142    indicateur code    DEFAULT     r   ALTER TABLE ONLY public.indicateur ALTER COLUMN code SET DEFAULT nextval('public.indicateur_code_seq'::regclass);
 >   ALTER TABLE public.indicateur ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    238    237    238                       2604    102161    intervenant code    DEFAULT     t   ALTER TABLE ONLY public.intervenant ALTER COLUMN code SET DEFAULT nextval('public.intervenant_code_seq'::regclass);
 ?   ALTER TABLE public.intervenant ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    241    240    241                       2604    102172 	   lieu code    DEFAULT     f   ALTER TABLE ONLY public.lieu ALTER COLUMN code SET DEFAULT nextval('public.lieu_code_seq'::regclass);
 8   ALTER TABLE public.lieu ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    242    243    243                       2604    102183    lieu_public code    DEFAULT     t   ALTER TABLE ONLY public.lieu_public ALTER COLUMN code SET DEFAULT nextval('public.lieu_public_code_seq'::regclass);
 ?   ALTER TABLE public.lieu_public ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    244    245    245                        2604    102194    localite code    DEFAULT     n   ALTER TABLE ONLY public.localite ALTER COLUMN code SET DEFAULT nextval('public.localite_code_seq'::regclass);
 <   ALTER TABLE public.localite ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    247    246    247            !           2604    102205    marque_pompe code    DEFAULT     v   ALTER TABLE ONLY public.marque_pompe ALTER COLUMN code SET DEFAULT nextval('public.marque_pompe_code_seq'::regclass);
 @   ALTER TABLE public.marque_pompe ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    249    248    249            "           2604    102216    materiaux code    DEFAULT     p   ALTER TABLE ONLY public.materiaux ALTER COLUMN code SET DEFAULT nextval('public.materiaux_code_seq'::regclass);
 =   ALTER TABLE public.materiaux ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    250    251    251            #           2604    102227    methode_collecte code    DEFAULT     ~   ALTER TABLE ONLY public.methode_collecte ALTER COLUMN code SET DEFAULT nextval('public.methode_collecte_code_seq'::regclass);
 D   ALTER TABLE public.methode_collecte ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    253    252    253            $           2604    102238    milieu code    DEFAULT     j   ALTER TABLE ONLY public.milieu ALTER COLUMN code SET DEFAULT nextval('public.milieu_code_seq'::regclass);
 :   ALTER TABLE public.milieu ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    254    255    255            %           2604    102249    modele_pompe code    DEFAULT     v   ALTER TABLE ONLY public.modele_pompe ALTER COLUMN code SET DEFAULT nextval('public.modele_pompe_code_seq'::regclass);
 @   ALTER TABLE public.modele_pompe ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    256    257    257            &           2604    102260    nature_sol code    DEFAULT     r   ALTER TABLE ONLY public.nature_sol ALTER COLUMN code SET DEFAULT nextval('public.nature_sol_code_seq'::regclass);
 >   ALTER TABLE public.nature_sol ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    259    258    259            '           2604    102271    nature_tube code    DEFAULT     t   ALTER TABLE ONLY public.nature_tube ALTER COLUMN code SET DEFAULT nextval('public.nature_tube_code_seq'::regclass);
 ?   ALTER TABLE public.nature_tube ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    261    260    261            (           2604    102282 $   parametre_station_hydrometrique code    DEFAULT     �   ALTER TABLE ONLY public.parametre_station_hydrometrique ALTER COLUMN code SET DEFAULT nextval('public.parametre_station_hydrometrique_code_seq'::regclass);
 S   ALTER TABLE public.parametre_station_hydrometrique ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    263    262    263            )           2604    102293    parametre_station_meteo code    DEFAULT     �   ALTER TABLE ONLY public.parametre_station_meteo ALTER COLUMN code SET DEFAULT nextval('public.parametre_station_meteo_code_seq'::regclass);
 K   ALTER TABLE public.parametre_station_meteo ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    264    265    265            *           2604    102304    periode code    DEFAULT     l   ALTER TABLE ONLY public.periode ALTER COLUMN code SET DEFAULT nextval('public.periode_code_seq'::regclass);
 ;   ALTER TABLE public.periode ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    267    266    267            +           2604    102355    projet code    DEFAULT     j   ALTER TABLE ONLY public.projet ALTER COLUMN code SET DEFAULT nextval('public.projet_code_seq'::regclass);
 :   ALTER TABLE public.projet ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    273    274    274            ,           2604    102366    propriete code    DEFAULT     p   ALTER TABLE ONLY public.propriete ALTER COLUMN code SET DEFAULT nextval('public.propriete_code_seq'::regclass);
 =   ALTER TABLE public.propriete ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    275    276    276            O           2604    103732    recensement_ins code    DEFAULT     |   ALTER TABLE ONLY public.recensement_ins ALTER COLUMN code SET DEFAULT nextval('public.recensement_ins_code_seq'::regclass);
 C   ALTER TABLE public.recensement_ins ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    359    358    359            -           2604    102412    reseau_suivi code    DEFAULT     v   ALTER TABLE ONLY public.reseau_suivi ALTER COLUMN code SET DEFAULT nextval('public.reseau_suivi_code_seq'::regclass);
 @   ALTER TABLE public.reseau_suivi ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    281    280    281            .           2604    102434    section_frame code    DEFAULT     x   ALTER TABLE ONLY public.section_frame ALTER COLUMN code SET DEFAULT nextval('public.section_frame_code_seq'::regclass);
 A   ALTER TABLE public.section_frame ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    284    285    285            /           2604    102457    source code    DEFAULT     j   ALTER TABLE ONLY public.source ALTER COLUMN code SET DEFAULT nextval('public.source_code_seq'::regclass);
 :   ALTER TABLE public.source ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    291    290    291            0           2604    102468    sous_groupe code    DEFAULT     t   ALTER TABLE ONLY public.sous_groupe ALTER COLUMN code SET DEFAULT nextval('public.sous_groupe_code_seq'::regclass);
 ?   ALTER TABLE public.sous_groupe ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    293    292    293            1           2604    102487    station code    DEFAULT     l   ALTER TABLE ONLY public.station ALTER COLUMN code SET DEFAULT nextval('public.station_code_seq'::regclass);
 ;   ALTER TABLE public.station ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    295    296    296            2           2604    102498    structure code    DEFAULT     p   ALTER TABLE ONLY public.structure ALTER COLUMN code SET DEFAULT nextval('public.structure_code_seq'::regclass);
 =   ALTER TABLE public.structure ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    297    298    298            3           2604    102509    systeme_aep code    DEFAULT     t   ALTER TABLE ONLY public.systeme_aep ALTER COLUMN code SET DEFAULT nextval('public.systeme_aep_code_seq'::regclass);
 ?   ALTER TABLE public.systeme_aep ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    299    300    300            4           2604    102520    t_user code    DEFAULT     j   ALTER TABLE ONLY public.t_user ALTER COLUMN code SET DEFAULT nextval('public.t_user_code_seq'::regclass);
 :   ALTER TABLE public.t_user ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    301    302    302            5           2604    102531    tuyauterie code    DEFAULT     r   ALTER TABLE ONLY public.tuyauterie ALTER COLUMN code SET DEFAULT nextval('public.tuyauterie_code_seq'::regclass);
 >   ALTER TABLE public.tuyauterie ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    304    303    304            6           2604    102542    type_amenagement code    DEFAULT     ~   ALTER TABLE ONLY public.type_amenagement ALTER COLUMN code SET DEFAULT nextval('public.type_amenagement_code_seq'::regclass);
 D   ALTER TABLE public.type_amenagement ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    305    306    306            7           2604    102553    type_classification code    DEFAULT     �   ALTER TABLE ONLY public.type_classification ALTER COLUMN code SET DEFAULT nextval('public.type_classification_code_seq'::regclass);
 G   ALTER TABLE public.type_classification ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    308    307    308            8           2604    102564 *   type_equipement_station_hydrometrique code    DEFAULT     �   ALTER TABLE ONLY public.type_equipement_station_hydrometrique ALTER COLUMN code SET DEFAULT nextval('public.type_equipement_station_hydrometrique_code_seq'::regclass);
 Y   ALTER TABLE public.type_equipement_station_hydrometrique ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    310    309    310            9           2604    102575 "   type_equipement_station_meteo code    DEFAULT     �   ALTER TABLE ONLY public.type_equipement_station_meteo ALTER COLUMN code SET DEFAULT nextval('public.type_equipement_station_meteo_code_seq'::regclass);
 Q   ALTER TABLE public.type_equipement_station_meteo ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    312    311    312            :           2604    102586     type_etablissement_scolaire code    DEFAULT     �   ALTER TABLE ONLY public.type_etablissement_scolaire ALTER COLUMN code SET DEFAULT nextval('public.type_etablissement_scolaire_code_seq'::regclass);
 O   ALTER TABLE public.type_etablissement_scolaire ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    314    313    314            ;           2604    102597    type_exhaure code    DEFAULT     v   ALTER TABLE ONLY public.type_exhaure ALTER COLUMN code SET DEFAULT nextval('public.type_exhaure_code_seq'::regclass);
 @   ALTER TABLE public.type_exhaure ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    315    316    316            <           2604    102608    type_formation_sanitaire code    DEFAULT     �   ALTER TABLE ONLY public.type_formation_sanitaire ALTER COLUMN code SET DEFAULT nextval('public.type_formation_sanitaire_code_seq'::regclass);
 L   ALTER TABLE public.type_formation_sanitaire ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    317    318    318            =           2604    102619    type_intervenant code    DEFAULT     ~   ALTER TABLE ONLY public.type_intervenant ALTER COLUMN code SET DEFAULT nextval('public.type_intervenant_code_seq'::regclass);
 D   ALTER TABLE public.type_intervenant ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    319    320    320            >           2604    102630    type_lieu_public code    DEFAULT     ~   ALTER TABLE ONLY public.type_lieu_public ALTER COLUMN code SET DEFAULT nextval('public.type_lieu_public_code_seq'::regclass);
 D   ALTER TABLE public.type_lieu_public ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    321    322    322            ?           2604    102641    type_localite code    DEFAULT     x   ALTER TABLE ONLY public.type_localite ALTER COLUMN code SET DEFAULT nextval('public.type_localite_code_seq'::regclass);
 A   ALTER TABLE public.type_localite ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    324    323    324            @           2604    102652    type_ouvrage code    DEFAULT     v   ALTER TABLE ONLY public.type_ouvrage ALTER COLUMN code SET DEFAULT nextval('public.type_ouvrage_code_seq'::regclass);
 @   ALTER TABLE public.type_ouvrage ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    325    326    326            A           2604    102663 !   type_piece_regulation_reseau code    DEFAULT     �   ALTER TABLE ONLY public.type_piece_regulation_reseau ALTER COLUMN code SET DEFAULT nextval('public.type_piece_regulation_reseau_code_seq'::regclass);
 P   ALTER TABLE public.type_piece_regulation_reseau ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    327    328    328            B           2604    102674    type_plan_eau code    DEFAULT     x   ALTER TABLE ONLY public.type_plan_eau ALTER COLUMN code SET DEFAULT nextval('public.type_plan_eau_code_seq'::regclass);
 A   ALTER TABLE public.type_plan_eau ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    330    329    330            C           2604    102685    type_prestation code    DEFAULT     |   ALTER TABLE ONLY public.type_prestation ALTER COLUMN code SET DEFAULT nextval('public.type_prestation_code_seq'::regclass);
 C   ALTER TABLE public.type_prestation ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    332    331    332            D           2604    102696    type_puits code    DEFAULT     r   ALTER TABLE ONLY public.type_puits ALTER COLUMN code SET DEFAULT nextval('public.type_puits_code_seq'::regclass);
 >   ALTER TABLE public.type_puits ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    334    333    334            E           2604    102707    type_ressource code    DEFAULT     z   ALTER TABLE ONLY public.type_ressource ALTER COLUMN code SET DEFAULT nextval('public.type_ressource_code_seq'::regclass);
 B   ALTER TABLE public.type_ressource ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    335    336    336            F           2604    102718 )   type_ressource_station_hydrometrique code    DEFAULT     �   ALTER TABLE ONLY public.type_ressource_station_hydrometrique ALTER COLUMN code SET DEFAULT nextval('public.type_ressource_station_hydrometrique_code_seq'::regclass);
 X   ALTER TABLE public.type_ressource_station_hydrometrique ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    337    338    338            G           2604    102729    type_station_meteo code    DEFAULT     �   ALTER TABLE ONLY public.type_station_meteo ALTER COLUMN code SET DEFAULT nextval('public.type_station_meteo_code_seq'::regclass);
 F   ALTER TABLE public.type_station_meteo ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    339    340    340            H           2604    102740    type_systeme_aep code    DEFAULT     ~   ALTER TABLE ONLY public.type_systeme_aep ALTER COLUMN code SET DEFAULT nextval('public.type_systeme_aep_code_seq'::regclass);
 D   ALTER TABLE public.type_systeme_aep ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    341    342    342            I           2604    102751    type_systeme_installation code    DEFAULT     �   ALTER TABLE ONLY public.type_systeme_installation ALTER COLUMN code SET DEFAULT nextval('public.type_systeme_installation_code_seq'::regclass);
 M   ALTER TABLE public.type_systeme_installation ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    344    343    344            J           2604    102762    type_systeme_protection code    DEFAULT     �   ALTER TABLE ONLY public.type_systeme_protection ALTER COLUMN code SET DEFAULT nextval('public.type_systeme_protection_code_seq'::regclass);
 K   ALTER TABLE public.type_systeme_protection ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    346    345    346            K           2604    102773    type_tube code    DEFAULT     p   ALTER TABLE ONLY public.type_tube ALTER COLUMN code SET DEFAULT nextval('public.type_tube_code_seq'::regclass);
 =   ALTER TABLE public.type_tube ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    348    347    348            L           2604    102784    type_usage code    DEFAULT     r   ALTER TABLE ONLY public.type_usage ALTER COLUMN code SET DEFAULT nextval('public.type_usage_code_seq'::regclass);
 >   ALTER TABLE public.type_usage ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    350    349    350            M           2604    102795 
   unite code    DEFAULT     h   ALTER TABLE ONLY public.unite ALTER COLUMN code SET DEFAULT nextval('public.unite_code_seq'::regclass);
 9   ALTER TABLE public.unite ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    352    351    352            N           2604    102817 	   zone code    DEFAULT     f   ALTER TABLE ONLY public.zone ALTER COLUMN code SET DEFAULT nextval('public.zone_code_seq'::regclass);
 8   ALTER TABLE public.zone ALTER COLUMN code DROP DEFAULT;
       public          postgres    false    355    356    356            :          0    101950 	   abreuvoir 
   TABLE DATA           �   COPY public.abreuvoir (code, created_at, created_by, is_deleted, modified_at, modified_by, code_ins, latitude, longitude, nb_robinets, nb_robinets_fonctionnels, numero_abreuvoir, code_systeme_aep, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    208   $�      ;          0    101958    borne_fontaine 
   TABLE DATA             COPY public.borne_fontaine (code, created_at, created_by, is_deleted, modified_at, modified_by, code_ins, compteur_installe, latitude, longitude, nb_robinets, nb_robinets_fonctionnels, numero_bf, code_systeme_aep, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    209   A�      <          0    101966    branchement_particulier 
   TABLE DATA           �   COPY public.branchement_particulier (code, created_at, created_by, is_deleted, modified_at, modified_by, code_ins, compteur_installe, latitude, longitude, nom_lieu, code_lieu, code_systeme_aep, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    210   ^�      =          0    101974    captage_systeme_aep 
   TABLE DATA           �  COPY public.captage_systeme_aep (code, created_at, created_by, is_deleted, modified_at, modified_by, hmt, cote_installation_pompe, couvercle_protection_tete, date_installation, date_installation2, debit_nominale, diametre_colonne, diametre_equipement, etat_compteur_forage, index_compteur_forage, longeur_colonne, marque_module, marque_onduleur, nombre_module, possibilite_mesure_piezometrique, profondeur_niveau_statique, profondeur_totale_mesure, protection_tete_forage, puissance, puissance2, puissance_compteur_electrique, puissance_onduleur, puissance_totale, puissance_transformateur, puissance_unitaire_module, tubage_protection_tete, type_colonne_exhaure, type_tableau_distribution, voltage, code_forage, code_marque_pompe, code_marque_pompe2, code_modele_pompe, code_modele_pompe2, code_systeme_aep, code_type_systeme_installation, code_type_systeme_protection, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    211   {�      >          0    101982    chateau_eau 
   TABLE DATA           d  COPY public.chateau_eau (code, created_at, created_by, is_deleted, modified_at, modified_by, capacite, code_ins, date_demarrage_travaux, date_fin_travaux, existence_compteur, hauteur, index_compteur, numero_chateau, code_disposition, code_hydraulique, code_materiaux, code_systeme_aep, code_tuyauterie, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    212   ��      ?          0    101990    chronique_observation 
   TABLE DATA           �   COPY public.chronique_observation (date_debut_observation, date_fin_observation, created_at, created_by, is_deleted, modified_at, modified_by, valeur_mesure, code_station, code_parametre_observe, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    213   ��      A          0    102000    classification 
   TABLE DATA           �   COPY public.classification (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, niveau, parent_code, code_type_classification, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    215   ��      B          0    102009    description_geologique 
   TABLE DATA           �   COPY public.description_geologique (code_pem, cote_inferieure, cote_superieure, created_at, created_by, is_deleted, modified_at, modified_by, code_nature_sol, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    216   ��      D          0    102019    disposition 
   TABLE DATA           �   COPY public.disposition (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    218   �      E          0    102028    donnee_indicateur 
   TABLE DATA           �   COPY public.donnee_indicateur (code_indicateur, code_periode, code_source, code_sous_groupe, code_unite, code_zone, created_at, created_by, is_deleted, modified_at, modified_by, valeur, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    219   )�      F          0    102036    equipement_forage 
   TABLE DATA           �   COPY public.equipement_forage (code, created_at, created_by, is_deleted, modified_at, modified_by, cote_inferieure, cote_superieure, diametre, numero_tube, code_nature_tube, pem_code, code_type_tube, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    220   F�      G          0    102044    equipement_installe 
   TABLE DATA           �   COPY public.equipement_installe (code_station, code_type_equipement_station, created_at, created_by, is_deleted, modified_at, modified_by, date_installation, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    221   c�      I          0    102054    etablissement_scolaire 
   TABLE DATA             COPY public.etablissement_scolaire (code, created_at, created_by, is_deleted, modified_at, modified_by, annee_creation, denomination, latitude, longitude, code_localite, code_type_etablissement_scolaire, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    223   ��      K          0    102065    etat_ouvrage 
   TABLE DATA           �   COPY public.etat_ouvrage (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    225   ��      M          0    102076    fiche_technique 
   TABLE DATA           -  COPY public.fiche_technique (code, created_at, created_by, is_deleted, modified_at, modified_by, commentaire_limite, definition, donnees_requises, methode_calcul, methode_collecte, niveau_desagregation, periodicite, source, unite, code_indicateur, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    227   ��      O          0    102087    financement 
   TABLE DATA           �   COPY public.financement (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    229   �      P          0    102096    forage 
   TABLE DATA              COPY public.forage (code, created_at, created_by, is_deleted, modified_at, modified_by, annee_realisation, autre_info_margelle, hauteur_margelle, latitude, longitude, nom_pem, numero_irh, renseignement_divers, code_etat_ouvrage, code_financement, code_localite, code_projet, code_propriete, date_demarrage_foration, date_fin_foration, date_mesure, debit_au_soufflage, developpement_effectue, essai_longue_duree_debit_maximum_estime, essai_longue_duree_duree_pompage, essai_longue_duree_rabattement_mesure, essai_simplifie_debit_maximum_estime, essai_simplifie_duree_pompage, essai_simplifie_rabattement_mesure, forage_positif, profondeur_niveau_statique, profondeur_totale_equipee, profondeur_totale_foree, code_type_amenagement, code_type_usage, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    230   !�      R          0    102106    formation_sanitaire 
   TABLE DATA           �   COPY public.formation_sanitaire (code, created_at, created_by, is_deleted, modified_at, modified_by, annee_creation, denomination, latitude, longitude, code_localite, code_type_formation_sanitaire, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    232   �      S          0    102115    frame 
   TABLE DATA           �   COPY public.frame (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    233   �      T          0    102123    frame_section_frame 
   TABLE DATA           M   COPY public.frame_section_frame (frame_code, section_frame_code) FROM stdin;
    public          postgres    false    234   (�      V          0    102128    hydraulique 
   TABLE DATA           �   COPY public.hydraulique (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    236   E�      X          0    102139 
   indicateur 
   TABLE DATA           �   COPY public.indicateur (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    238   b�      Y          0    102148    indicateur_classification 
   TABLE DATA           �   COPY public.indicateur_classification (code_classification, code_indicateur, created_at, created_by, is_deleted, modified_at, modified_by, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    239   �      [          0    102158    intervenant 
   TABLE DATA           �   COPY public.intervenant (code, created_at, created_by, is_deleted, modified_at, modified_by, denomination, code_type_intervenant, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    241   ��      ]          0    102169    lieu 
   TABLE DATA           �   COPY public.lieu (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    243   ��      _          0    102180    lieu_public 
   TABLE DATA           �   COPY public.lieu_public (code, created_at, created_by, is_deleted, modified_at, modified_by, annee_creation, denomination, latitude, longitude, code_localite, code_type_lieu_public, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    245   ֲ      a          0    102191    localite 
   TABLE DATA             COPY public.localite (code, created_at, created_by, is_deleted, modified_at, modified_by, code_ins, latitude, libelle, longitude, code_commune, code_localite_rattachement, code_type_localite, code_milieu, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    247   �      c          0    102202    marque_pompe 
   TABLE DATA           �   COPY public.marque_pompe (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    249   �S      e          0    102213 	   materiaux 
   TABLE DATA           �   COPY public.materiaux (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    251   �S      g          0    102224    methode_collecte 
   TABLE DATA           �   COPY public.methode_collecte (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    253   �S      i          0    102235    milieu 
   TABLE DATA           �   COPY public.milieu (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    255   �S      k          0    102246    modele_pompe 
   TABLE DATA           �   COPY public.modele_pompe (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    257   'T      m          0    102257 
   nature_sol 
   TABLE DATA           �   COPY public.nature_sol (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    259   DT      o          0    102268    nature_tube 
   TABLE DATA           �   COPY public.nature_tube (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    261   aT      q          0    102279    parametre_station_hydrometrique 
   TABLE DATA           �   COPY public.parametre_station_hydrometrique (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    263   ~T      s          0    102290    parametre_station_meteo 
   TABLE DATA           �   COPY public.parametre_station_meteo (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    265   �T      u          0    102301    periode 
   TABLE DATA           �   COPY public.periode (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, niveau, parent_code, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    267   �T      v          0    102310    piece_regulation_reseau 
   TABLE DATA           �   COPY public.piece_regulation_reseau (code, created_at, created_by, is_deleted, modified_at, modified_by, latitude, longitude, numero_ordre, code_systeme_aep, type_piece_regulation_reseau, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    268   �T      w          0    102318 
   piezometre 
   TABLE DATA           �   COPY public.piezometre (code, created_at, created_by, is_deleted, modified_at, modified_by, date_observation, hauteur_piezo, heure_observation, profondeur_piezo, pem_code, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    269   �T      x          0    102326    pompe_installe 
   TABLE DATA           �   COPY public.pompe_installe (code, created_at, created_by, is_deleted, modified_at, modified_by, date_installation_pompe, code_marque_pompe, code_modele_pompe, code_pem, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    270   U      y          0    102334    prestataire 
   TABLE DATA           �   COPY public.prestataire (code, created_at, created_by, is_deleted, modified_at, modified_by, cout_prestation, nom_prestataire, code_pem, code_type_prestation, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    271   ,U      z          0    102342 	   privilege 
   TABLE DATA           �   COPY public.privilege (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    272   IU      |          0    102352    projet 
   TABLE DATA           �   COPY public.projet (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    274   �U      ~          0    102363 	   propriete 
   TABLE DATA           �   COPY public.propriete (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    276   �U                0    102372    puits_cimente 
   TABLE DATA           �  COPY public.puits_cimente (code, created_at, created_by, is_deleted, modified_at, modified_by, annee_realisation, autre_info_margelle, hauteur_margelle, latitude, longitude, nom_pem, numero_irh, renseignement_divers, code_etat_ouvrage, code_financement, code_localite, code_projet, code_propriete, escieh_debit_maximum_estime, escieh_duree_pompage, escieh_rabattement_mesure, abreuvoirs, aire_assainie, amenagement_surface, antibourbier, canal_evacuation_et_puits_perdu, couleur_eau, couvercle, dalle_fond, date_mesure, developpement_effectue_au_cuffat, diametre_captage, diametre_cuvelage, fourche, hauteur_captage, hauteur_massif_filtrant, hauteur_recouvrement, mur_cloture, nombre_buses_captage, pompe_immergee, portique, potabilite_eau, profondeur_cuvelee, profondeur_niveau_statique, profondeur_sommet_colonne_captage, profondeur_totale, puisette, trousse_coupante, code_type_exhaure, code_type_puits, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    277   �U      �          0    103729    recensement_ins 
   TABLE DATA           �   COPY public.recensement_ins (code, created_at, created_by, is_deleted, modified_at, modified_by, annee_recensement, nombre_menage, population_femme, population_homme, taux_accroissement, code_localite, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    359   p      �          0    102391    reseau_distribution 
   TABLE DATA           !  COPY public.reseau_distribution (code, created_at, created_by, is_deleted, modified_at, modified_by, depart_latitude, depart_longitude, diametre, fin_latitude, fin_longitude, longueur, numero_troncon, code_systeme_aep, code_tuyauterie, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    278   �	      �          0    102399    reseau_refoulement 
   TABLE DATA              COPY public.reseau_refoulement (code, created_at, created_by, is_deleted, modified_at, modified_by, depart_latitude, depart_longitude, diametre, fin_latitude, fin_longitude, longueur, numero_troncon, code_systeme_aep, code_tuyauterie, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    279   �	      �          0    102409    reseau_suivi 
   TABLE DATA           �   COPY public.reseau_suivi (code, created_at, created_by, is_deleted, modified_at, modified_by, commentaire, nom, proprietaire, structure_en_charge, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    281   �	      �          0    102418    role 
   TABLE DATA           �   COPY public.role (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    282   �	      �          0    102426    roles_privileges 
   TABLE DATA           E   COPY public.roles_privileges (role_code, privilege_code) FROM stdin;
    public          postgres    false    283   0	      �          0    102431    section_frame 
   TABLE DATA           �   COPY public.section_frame (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    285   U	      �          0    102440    section_frame_indicateurs 
   TABLE DATA           T   COPY public.section_frame_indicateurs (section_frame_code, indicateurs) FROM stdin;
    public          postgres    false    286   r	      �          0    102443    section_frame_periodes 
   TABLE DATA           N   COPY public.section_frame_periodes (section_frame_code, periodes) FROM stdin;
    public          postgres    false    287   �	      �          0    102446    section_frame_sources 
   TABLE DATA           L   COPY public.section_frame_sources (section_frame_code, sources) FROM stdin;
    public          postgres    false    288   �	      �          0    102449    section_frame_zones 
   TABLE DATA           H   COPY public.section_frame_zones (section_frame_code, zones) FROM stdin;
    public          postgres    false    289   �	      �          0    102454    source 
   TABLE DATA           �   COPY public.source (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    291   �	      �          0    102465    sous_groupe 
   TABLE DATA           �   COPY public.sous_groupe (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, niveau, parent_code, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    293   	      �          0    102474    sous_groupe_indicateur 
   TABLE DATA           �   COPY public.sous_groupe_indicateur (code_indicateur, code_sous_groupe, created_at, created_by, is_deleted, modified_at, modified_by, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    294    	                0    101235    spatial_ref_sys 
   TABLE DATA           X   COPY public.spatial_ref_sys (srid, auth_name, auth_srid, srtext, proj4text) FROM stdin;
    public          postgres    false    204   =	      �          0    102484    station 
   TABLE DATA             COPY public.station (code, created_at, created_by, is_deleted, modified_at, modified_by, altitude, cause_fermeture, date_fermeture, date_ouverture, latitude, longitude, nom, code_localite, code_reseau_suivi, code_type_station, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    296   Z	      �          0    102495 	   structure 
   TABLE DATA           �   COPY public.structure (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    298   w	      �          0    102506    systeme_aep 
   TABLE DATA           u  COPY public.systeme_aep (code, created_at, created_by, is_deleted, modified_at, modified_by, annee_realisation, nom_aep, numero_irh, precision_energie_autres, precision_source_principale_autres, source_energie_autre, source_energie_eolienne, source_energie_reseau_electrique, source_energie_solaire, source_energie_thermique, source_principale_autre, source_principale_eolienne, source_principale_reseau_electrique, source_principale_solaire, source_principale_thermique, code_etat_ouvrage, code_financement, code_localite, code_projet, code_propriete, code_type_systeme_aep, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    300   �	      �          0    102517    t_user 
   TABLE DATA           �   COPY public.t_user (code, created_at, created_by, is_deleted, modified_at, modified_by, active, login, mot_passe, nom, prenom, token_expire, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    302   �	      �          0    102528 
   tuyauterie 
   TABLE DATA           �   COPY public.tuyauterie (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    304   7	      �          0    102539    type_amenagement 
   TABLE DATA           �   COPY public.type_amenagement (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    306   T	      �          0    102550    type_classification 
   TABLE DATA           �   COPY public.type_classification (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    308   q	      �          0    102561 %   type_equipement_station_hydrometrique 
   TABLE DATA           �   COPY public.type_equipement_station_hydrometrique (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    310   �	      �          0    102572    type_equipement_station_meteo 
   TABLE DATA           �   COPY public.type_equipement_station_meteo (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    312   �	      �          0    102583    type_etablissement_scolaire 
   TABLE DATA           �   COPY public.type_etablissement_scolaire (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    314   �	      �          0    102594    type_exhaure 
   TABLE DATA           �   COPY public.type_exhaure (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    316   �	      �          0    102605    type_formation_sanitaire 
   TABLE DATA           �   COPY public.type_formation_sanitaire (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    318   	      �          0    102616    type_intervenant 
   TABLE DATA           �   COPY public.type_intervenant (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    320   	      �          0    102627    type_lieu_public 
   TABLE DATA           �   COPY public.type_lieu_public (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    322   <	      �          0    102638    type_localite 
   TABLE DATA           �   COPY public.type_localite (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    324   Y	      �          0    102649    type_ouvrage 
   TABLE DATA           �   COPY public.type_ouvrage (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    326   �	      �          0    102660    type_piece_regulation_reseau 
   TABLE DATA           �   COPY public.type_piece_regulation_reseau (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    328   �	      �          0    102671    type_plan_eau 
   TABLE DATA           �   COPY public.type_plan_eau (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    330   �	      �          0    102682    type_prestation 
   TABLE DATA           �   COPY public.type_prestation (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    332   �	      �          0    102693 
   type_puits 
   TABLE DATA           �   COPY public.type_puits (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    334   	      �          0    102704    type_ressource 
   TABLE DATA           �   COPY public.type_ressource (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    336   7	      �          0    102715 $   type_ressource_station_hydrometrique 
   TABLE DATA           �   COPY public.type_ressource_station_hydrometrique (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    338   T	      �          0    102726    type_station_meteo 
   TABLE DATA           �   COPY public.type_station_meteo (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    340   q	      �          0    102737    type_systeme_aep 
   TABLE DATA           �   COPY public.type_systeme_aep (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    342   �	      �          0    102748    type_systeme_installation 
   TABLE DATA           �   COPY public.type_systeme_installation (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    344   �	      �          0    102759    type_systeme_protection 
   TABLE DATA           �   COPY public.type_systeme_protection (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    346   �	      �          0    102770 	   type_tube 
   TABLE DATA           �   COPY public.type_tube (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    348   �	      �          0    102781 
   type_usage 
   TABLE DATA           �   COPY public.type_usage (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    350   	      �          0    102792    unite 
   TABLE DATA           �   COPY public.unite (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    352   	      �          0    102801    unite_indicateur 
   TABLE DATA           �   COPY public.unite_indicateur (code_indicateur, code_unite, created_at, created_by, is_deleted, modified_at, modified_by, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    353   <	      �          0    102809    users_roles 
   TABLE DATA           ;   COPY public.users_roles (user_code, role_code) FROM stdin;
    public          postgres    false    354   Y	      �          0    102814    zone 
   TABLE DATA           �   COPY public.zone (code, created_at, created_by, is_deleted, modified_at, modified_by, libelle, niveau, code_milieu, parent_code, is_deleted_central, is_deleted_regional) FROM stdin;
    public          postgres    false    356   z	                 0    0    classification_code_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.classification_code_seq', 1, false);
          public          postgres    false    214                       0    0    disposition_code_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.disposition_code_seq', 1, false);
          public          postgres    false    217                       0    0    etablissement_scolaire_code_seq    SEQUENCE SET     N   SELECT pg_catalog.setval('public.etablissement_scolaire_code_seq', 1, false);
          public          postgres    false    222                       0    0    etat_ouvrage_code_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.etat_ouvrage_code_seq', 1, false);
          public          postgres    false    224                       0    0    fiche_technique_code_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.fiche_technique_code_seq', 1, false);
          public          postgres    false    226                       0    0    financement_code_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.financement_code_seq', 1, false);
          public          postgres    false    228                       0    0    formation_sanitaire_code_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.formation_sanitaire_code_seq', 1, false);
          public          postgres    false    231                       0    0    hibernate_sequence    SEQUENCE SET     @   SELECT pg_catalog.setval('public.hibernate_sequence', 3, true);
          public          postgres    false    357                       0    0    hydraulique_code_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.hydraulique_code_seq', 1, false);
          public          postgres    false    235                       0    0    indicateur_code_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.indicateur_code_seq', 1, false);
          public          postgres    false    237                       0    0    intervenant_code_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.intervenant_code_seq', 1, false);
          public          postgres    false    240                        0    0    lieu_code_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.lieu_code_seq', 1, false);
          public          postgres    false    242            !           0    0    lieu_public_code_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.lieu_public_code_seq', 1, false);
          public          postgres    false    244            "           0    0    localite_code_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.localite_code_seq', 1, false);
          public          postgres    false    246            #           0    0    marque_pompe_code_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.marque_pompe_code_seq', 1, false);
          public          postgres    false    248            $           0    0    materiaux_code_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.materiaux_code_seq', 1, false);
          public          postgres    false    250            %           0    0    methode_collecte_code_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.methode_collecte_code_seq', 1, false);
          public          postgres    false    252            &           0    0    milieu_code_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.milieu_code_seq', 1, false);
          public          postgres    false    254            '           0    0    modele_pompe_code_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.modele_pompe_code_seq', 1, false);
          public          postgres    false    256            (           0    0    nature_sol_code_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.nature_sol_code_seq', 1, false);
          public          postgres    false    258            )           0    0    nature_tube_code_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.nature_tube_code_seq', 1, false);
          public          postgres    false    260            *           0    0 (   parametre_station_hydrometrique_code_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.parametre_station_hydrometrique_code_seq', 1, false);
          public          postgres    false    262            +           0    0     parametre_station_meteo_code_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public.parametre_station_meteo_code_seq', 1, false);
          public          postgres    false    264            ,           0    0    periode_code_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.periode_code_seq', 1, false);
          public          postgres    false    266            -           0    0    projet_code_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.projet_code_seq', 1, false);
          public          postgres    false    273            .           0    0    propriete_code_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.propriete_code_seq', 1, false);
          public          postgres    false    275            /           0    0    recensement_ins_code_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.recensement_ins_code_seq', 1, false);
          public          postgres    false    358            0           0    0    reseau_suivi_code_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.reseau_suivi_code_seq', 1, false);
          public          postgres    false    280            1           0    0    section_frame_code_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.section_frame_code_seq', 1, false);
          public          postgres    false    284            2           0    0    source_code_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.source_code_seq', 1, false);
          public          postgres    false    290            3           0    0    sous_groupe_code_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.sous_groupe_code_seq', 1, false);
          public          postgres    false    292            4           0    0    station_code_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.station_code_seq', 1, false);
          public          postgres    false    295            5           0    0    structure_code_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.structure_code_seq', 1, false);
          public          postgres    false    297            6           0    0    systeme_aep_code_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.systeme_aep_code_seq', 1, false);
          public          postgres    false    299            7           0    0    t_user_code_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.t_user_code_seq', 1, true);
          public          postgres    false    301            8           0    0    tuyauterie_code_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.tuyauterie_code_seq', 1, false);
          public          postgres    false    303            9           0    0    type_amenagement_code_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.type_amenagement_code_seq', 1, false);
          public          postgres    false    305            :           0    0    type_classification_code_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.type_classification_code_seq', 1, false);
          public          postgres    false    307            ;           0    0 .   type_equipement_station_hydrometrique_code_seq    SEQUENCE SET     ]   SELECT pg_catalog.setval('public.type_equipement_station_hydrometrique_code_seq', 1, false);
          public          postgres    false    309            <           0    0 &   type_equipement_station_meteo_code_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('public.type_equipement_station_meteo_code_seq', 1, false);
          public          postgres    false    311            =           0    0 $   type_etablissement_scolaire_code_seq    SEQUENCE SET     S   SELECT pg_catalog.setval('public.type_etablissement_scolaire_code_seq', 1, false);
          public          postgres    false    313            >           0    0    type_exhaure_code_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.type_exhaure_code_seq', 1, false);
          public          postgres    false    315            ?           0    0 !   type_formation_sanitaire_code_seq    SEQUENCE SET     P   SELECT pg_catalog.setval('public.type_formation_sanitaire_code_seq', 1, false);
          public          postgres    false    317            @           0    0    type_intervenant_code_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.type_intervenant_code_seq', 1, false);
          public          postgres    false    319            A           0    0    type_lieu_public_code_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.type_lieu_public_code_seq', 1, false);
          public          postgres    false    321            B           0    0    type_localite_code_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.type_localite_code_seq', 1, false);
          public          postgres    false    323            C           0    0    type_ouvrage_code_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.type_ouvrage_code_seq', 1, false);
          public          postgres    false    325            D           0    0 %   type_piece_regulation_reseau_code_seq    SEQUENCE SET     T   SELECT pg_catalog.setval('public.type_piece_regulation_reseau_code_seq', 1, false);
          public          postgres    false    327            E           0    0    type_plan_eau_code_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.type_plan_eau_code_seq', 1, false);
          public          postgres    false    329            F           0    0    type_prestation_code_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.type_prestation_code_seq', 1, false);
          public          postgres    false    331            G           0    0    type_puits_code_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.type_puits_code_seq', 1, false);
          public          postgres    false    333            H           0    0    type_ressource_code_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.type_ressource_code_seq', 1, false);
          public          postgres    false    335            I           0    0 -   type_ressource_station_hydrometrique_code_seq    SEQUENCE SET     \   SELECT pg_catalog.setval('public.type_ressource_station_hydrometrique_code_seq', 1, false);
          public          postgres    false    337            J           0    0    type_station_meteo_code_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.type_station_meteo_code_seq', 1, false);
          public          postgres    false    339            K           0    0    type_systeme_aep_code_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.type_systeme_aep_code_seq', 1, false);
          public          postgres    false    341            L           0    0 "   type_systeme_installation_code_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('public.type_systeme_installation_code_seq', 1, false);
          public          postgres    false    343            M           0    0     type_systeme_protection_code_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public.type_systeme_protection_code_seq', 1, false);
          public          postgres    false    345            N           0    0    type_tube_code_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.type_tube_code_seq', 1, false);
          public          postgres    false    347            O           0    0    type_usage_code_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.type_usage_code_seq', 1, false);
          public          postgres    false    349            P           0    0    unite_code_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.unite_code_seq', 1, false);
          public          postgres    false    351            Q           0    0    zone_code_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.zone_code_seq', 1, false);
          public          postgres    false    355            S           2606    101957    abreuvoir abreuvoir_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.abreuvoir
    ADD CONSTRAINT abreuvoir_pkey PRIMARY KEY (code);
 B   ALTER TABLE ONLY public.abreuvoir DROP CONSTRAINT abreuvoir_pkey;
       public            postgres    false    208            U           2606    101965 "   borne_fontaine borne_fontaine_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.borne_fontaine
    ADD CONSTRAINT borne_fontaine_pkey PRIMARY KEY (code);
 L   ALTER TABLE ONLY public.borne_fontaine DROP CONSTRAINT borne_fontaine_pkey;
       public            postgres    false    209            W           2606    101973 4   branchement_particulier branchement_particulier_pkey 
   CONSTRAINT     t   ALTER TABLE ONLY public.branchement_particulier
    ADD CONSTRAINT branchement_particulier_pkey PRIMARY KEY (code);
 ^   ALTER TABLE ONLY public.branchement_particulier DROP CONSTRAINT branchement_particulier_pkey;
       public            postgres    false    210            Y           2606    101981 ,   captage_systeme_aep captage_systeme_aep_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.captage_systeme_aep
    ADD CONSTRAINT captage_systeme_aep_pkey PRIMARY KEY (code);
 V   ALTER TABLE ONLY public.captage_systeme_aep DROP CONSTRAINT captage_systeme_aep_pkey;
       public            postgres    false    211            [           2606    101989    chateau_eau chateau_eau_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.chateau_eau
    ADD CONSTRAINT chateau_eau_pkey PRIMARY KEY (code);
 F   ALTER TABLE ONLY public.chateau_eau DROP CONSTRAINT chateau_eau_pkey;
       public            postgres    false    212            ]           2606    101997 0   chronique_observation chronique_observation_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.chronique_observation
    ADD CONSTRAINT chronique_observation_pkey PRIMARY KEY (date_debut_observation, date_fin_observation, code_parametre_observe, code_station);
 Z   ALTER TABLE ONLY public.chronique_observation DROP CONSTRAINT chronique_observation_pkey;
       public            postgres    false    213    213    213    213            _           2606    102008 "   classification classification_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.classification
    ADD CONSTRAINT classification_pkey PRIMARY KEY (code);
 L   ALTER TABLE ONLY public.classification DROP CONSTRAINT classification_pkey;
       public            postgres    false    215            a           2606    102016 2   description_geologique description_geologique_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.description_geologique
    ADD CONSTRAINT description_geologique_pkey PRIMARY KEY (code_pem, cote_inferieure, cote_superieure);
 \   ALTER TABLE ONLY public.description_geologique DROP CONSTRAINT description_geologique_pkey;
       public            postgres    false    216    216    216            c           2606    102027    disposition disposition_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.disposition
    ADD CONSTRAINT disposition_pkey PRIMARY KEY (code);
 F   ALTER TABLE ONLY public.disposition DROP CONSTRAINT disposition_pkey;
       public            postgres    false    218            g           2606    102035 (   donnee_indicateur donnee_indicateur_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.donnee_indicateur
    ADD CONSTRAINT donnee_indicateur_pkey PRIMARY KEY (code_indicateur, code_periode, code_source, code_sous_groupe, code_unite, code_zone);
 R   ALTER TABLE ONLY public.donnee_indicateur DROP CONSTRAINT donnee_indicateur_pkey;
       public            postgres    false    219    219    219    219    219    219            i           2606    102043 (   equipement_forage equipement_forage_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.equipement_forage
    ADD CONSTRAINT equipement_forage_pkey PRIMARY KEY (code);
 R   ALTER TABLE ONLY public.equipement_forage DROP CONSTRAINT equipement_forage_pkey;
       public            postgres    false    220            k           2606    102051 ,   equipement_installe equipement_installe_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.equipement_installe
    ADD CONSTRAINT equipement_installe_pkey PRIMARY KEY (code_station, code_type_equipement_station);
 V   ALTER TABLE ONLY public.equipement_installe DROP CONSTRAINT equipement_installe_pkey;
       public            postgres    false    221    221            m           2606    102062 2   etablissement_scolaire etablissement_scolaire_pkey 
   CONSTRAINT     r   ALTER TABLE ONLY public.etablissement_scolaire
    ADD CONSTRAINT etablissement_scolaire_pkey PRIMARY KEY (code);
 \   ALTER TABLE ONLY public.etablissement_scolaire DROP CONSTRAINT etablissement_scolaire_pkey;
       public            postgres    false    223            o           2606    102073    etat_ouvrage etat_ouvrage_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.etat_ouvrage
    ADD CONSTRAINT etat_ouvrage_pkey PRIMARY KEY (code);
 H   ALTER TABLE ONLY public.etat_ouvrage DROP CONSTRAINT etat_ouvrage_pkey;
       public            postgres    false    225            s           2606    102084 $   fiche_technique fiche_technique_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.fiche_technique
    ADD CONSTRAINT fiche_technique_pkey PRIMARY KEY (code);
 N   ALTER TABLE ONLY public.fiche_technique DROP CONSTRAINT fiche_technique_pkey;
       public            postgres    false    227            u           2606    102095    financement financement_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.financement
    ADD CONSTRAINT financement_pkey PRIMARY KEY (code);
 F   ALTER TABLE ONLY public.financement DROP CONSTRAINT financement_pkey;
       public            postgres    false    229            y           2606    102103    forage forage_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.forage
    ADD CONSTRAINT forage_pkey PRIMARY KEY (code);
 <   ALTER TABLE ONLY public.forage DROP CONSTRAINT forage_pkey;
       public            postgres    false    230            {           2606    102114 ,   formation_sanitaire formation_sanitaire_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.formation_sanitaire
    ADD CONSTRAINT formation_sanitaire_pkey PRIMARY KEY (code);
 V   ALTER TABLE ONLY public.formation_sanitaire DROP CONSTRAINT formation_sanitaire_pkey;
       public            postgres    false    232            }           2606    102122    frame frame_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.frame
    ADD CONSTRAINT frame_pkey PRIMARY KEY (code);
 :   ALTER TABLE ONLY public.frame DROP CONSTRAINT frame_pkey;
       public            postgres    false    233                       2606    102136    hydraulique hydraulique_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.hydraulique
    ADD CONSTRAINT hydraulique_pkey PRIMARY KEY (code);
 F   ALTER TABLE ONLY public.hydraulique DROP CONSTRAINT hydraulique_pkey;
       public            postgres    false    236            �           2606    102155 8   indicateur_classification indicateur_classification_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.indicateur_classification
    ADD CONSTRAINT indicateur_classification_pkey PRIMARY KEY (code_classification, code_indicateur);
 b   ALTER TABLE ONLY public.indicateur_classification DROP CONSTRAINT indicateur_classification_pkey;
       public            postgres    false    239    239            �           2606    102147    indicateur indicateur_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.indicateur
    ADD CONSTRAINT indicateur_pkey PRIMARY KEY (code);
 D   ALTER TABLE ONLY public.indicateur DROP CONSTRAINT indicateur_pkey;
       public            postgres    false    238            �           2606    102166    intervenant intervenant_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.intervenant
    ADD CONSTRAINT intervenant_pkey PRIMARY KEY (code);
 F   ALTER TABLE ONLY public.intervenant DROP CONSTRAINT intervenant_pkey;
       public            postgres    false    241            �           2606    102177    lieu lieu_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.lieu
    ADD CONSTRAINT lieu_pkey PRIMARY KEY (code);
 8   ALTER TABLE ONLY public.lieu DROP CONSTRAINT lieu_pkey;
       public            postgres    false    243            �           2606    102188    lieu_public lieu_public_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.lieu_public
    ADD CONSTRAINT lieu_public_pkey PRIMARY KEY (code);
 F   ALTER TABLE ONLY public.lieu_public DROP CONSTRAINT lieu_public_pkey;
       public            postgres    false    245            �           2606    102199    localite localite_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.localite
    ADD CONSTRAINT localite_pkey PRIMARY KEY (code);
 @   ALTER TABLE ONLY public.localite DROP CONSTRAINT localite_pkey;
       public            postgres    false    247            �           2606    102210    marque_pompe marque_pompe_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.marque_pompe
    ADD CONSTRAINT marque_pompe_pkey PRIMARY KEY (code);
 H   ALTER TABLE ONLY public.marque_pompe DROP CONSTRAINT marque_pompe_pkey;
       public            postgres    false    249            �           2606    102221    materiaux materiaux_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.materiaux
    ADD CONSTRAINT materiaux_pkey PRIMARY KEY (code);
 B   ALTER TABLE ONLY public.materiaux DROP CONSTRAINT materiaux_pkey;
       public            postgres    false    251            �           2606    102232 &   methode_collecte methode_collecte_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.methode_collecte
    ADD CONSTRAINT methode_collecte_pkey PRIMARY KEY (code);
 P   ALTER TABLE ONLY public.methode_collecte DROP CONSTRAINT methode_collecte_pkey;
       public            postgres    false    253            �           2606    102243    milieu milieu_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.milieu
    ADD CONSTRAINT milieu_pkey PRIMARY KEY (code);
 <   ALTER TABLE ONLY public.milieu DROP CONSTRAINT milieu_pkey;
       public            postgres    false    255            �           2606    102254    modele_pompe modele_pompe_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.modele_pompe
    ADD CONSTRAINT modele_pompe_pkey PRIMARY KEY (code);
 H   ALTER TABLE ONLY public.modele_pompe DROP CONSTRAINT modele_pompe_pkey;
       public            postgres    false    257            �           2606    102265    nature_sol nature_sol_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.nature_sol
    ADD CONSTRAINT nature_sol_pkey PRIMARY KEY (code);
 D   ALTER TABLE ONLY public.nature_sol DROP CONSTRAINT nature_sol_pkey;
       public            postgres    false    259            �           2606    102276    nature_tube nature_tube_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.nature_tube
    ADD CONSTRAINT nature_tube_pkey PRIMARY KEY (code);
 F   ALTER TABLE ONLY public.nature_tube DROP CONSTRAINT nature_tube_pkey;
       public            postgres    false    261            �           2606    102287 D   parametre_station_hydrometrique parametre_station_hydrometrique_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.parametre_station_hydrometrique
    ADD CONSTRAINT parametre_station_hydrometrique_pkey PRIMARY KEY (code);
 n   ALTER TABLE ONLY public.parametre_station_hydrometrique DROP CONSTRAINT parametre_station_hydrometrique_pkey;
       public            postgres    false    263            �           2606    102298 4   parametre_station_meteo parametre_station_meteo_pkey 
   CONSTRAINT     t   ALTER TABLE ONLY public.parametre_station_meteo
    ADD CONSTRAINT parametre_station_meteo_pkey PRIMARY KEY (code);
 ^   ALTER TABLE ONLY public.parametre_station_meteo DROP CONSTRAINT parametre_station_meteo_pkey;
       public            postgres    false    265            �           2606    102309    periode periode_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.periode
    ADD CONSTRAINT periode_pkey PRIMARY KEY (code);
 >   ALTER TABLE ONLY public.periode DROP CONSTRAINT periode_pkey;
       public            postgres    false    267            �           2606    102317 4   piece_regulation_reseau piece_regulation_reseau_pkey 
   CONSTRAINT     t   ALTER TABLE ONLY public.piece_regulation_reseau
    ADD CONSTRAINT piece_regulation_reseau_pkey PRIMARY KEY (code);
 ^   ALTER TABLE ONLY public.piece_regulation_reseau DROP CONSTRAINT piece_regulation_reseau_pkey;
       public            postgres    false    268            �           2606    102325    piezometre piezometre_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.piezometre
    ADD CONSTRAINT piezometre_pkey PRIMARY KEY (code);
 D   ALTER TABLE ONLY public.piezometre DROP CONSTRAINT piezometre_pkey;
       public            postgres    false    269            �           2606    102333 "   pompe_installe pompe_installe_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.pompe_installe
    ADD CONSTRAINT pompe_installe_pkey PRIMARY KEY (code);
 L   ALTER TABLE ONLY public.pompe_installe DROP CONSTRAINT pompe_installe_pkey;
       public            postgres    false    270            �           2606    102341    prestataire prestataire_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.prestataire
    ADD CONSTRAINT prestataire_pkey PRIMARY KEY (code);
 F   ALTER TABLE ONLY public.prestataire DROP CONSTRAINT prestataire_pkey;
       public            postgres    false    271            �           2606    102349    privilege privilege_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.privilege
    ADD CONSTRAINT privilege_pkey PRIMARY KEY (code);
 B   ALTER TABLE ONLY public.privilege DROP CONSTRAINT privilege_pkey;
       public            postgres    false    272            �           2606    102360    projet projet_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.projet
    ADD CONSTRAINT projet_pkey PRIMARY KEY (code);
 <   ALTER TABLE ONLY public.projet DROP CONSTRAINT projet_pkey;
       public            postgres    false    274            �           2606    102371    propriete propriete_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.propriete
    ADD CONSTRAINT propriete_pkey PRIMARY KEY (code);
 B   ALTER TABLE ONLY public.propriete DROP CONSTRAINT propriete_pkey;
       public            postgres    false    276            �           2606    102379     puits_cimente puits_cimente_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.puits_cimente
    ADD CONSTRAINT puits_cimente_pkey PRIMARY KEY (code);
 J   ALTER TABLE ONLY public.puits_cimente DROP CONSTRAINT puits_cimente_pkey;
       public            postgres    false    277            U           2606    103737 $   recensement_ins recensement_ins_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.recensement_ins
    ADD CONSTRAINT recensement_ins_pkey PRIMARY KEY (code);
 N   ALTER TABLE ONLY public.recensement_ins DROP CONSTRAINT recensement_ins_pkey;
       public            postgres    false    359            �           2606    102398 ,   reseau_distribution reseau_distribution_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.reseau_distribution
    ADD CONSTRAINT reseau_distribution_pkey PRIMARY KEY (code);
 V   ALTER TABLE ONLY public.reseau_distribution DROP CONSTRAINT reseau_distribution_pkey;
       public            postgres    false    278            �           2606    102406 *   reseau_refoulement reseau_refoulement_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.reseau_refoulement
    ADD CONSTRAINT reseau_refoulement_pkey PRIMARY KEY (code);
 T   ALTER TABLE ONLY public.reseau_refoulement DROP CONSTRAINT reseau_refoulement_pkey;
       public            postgres    false    279            �           2606    102417    reseau_suivi reseau_suivi_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.reseau_suivi
    ADD CONSTRAINT reseau_suivi_pkey PRIMARY KEY (code);
 H   ALTER TABLE ONLY public.reseau_suivi DROP CONSTRAINT reseau_suivi_pkey;
       public            postgres    false    281            �           2606    102425    role role_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.role
    ADD CONSTRAINT role_pkey PRIMARY KEY (code);
 8   ALTER TABLE ONLY public.role DROP CONSTRAINT role_pkey;
       public            postgres    false    282            �           2606    102439     section_frame section_frame_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.section_frame
    ADD CONSTRAINT section_frame_pkey PRIMARY KEY (code);
 J   ALTER TABLE ONLY public.section_frame DROP CONSTRAINT section_frame_pkey;
       public            postgres    false    285            �           2606    102462    source source_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.source
    ADD CONSTRAINT source_pkey PRIMARY KEY (code);
 <   ALTER TABLE ONLY public.source DROP CONSTRAINT source_pkey;
       public            postgres    false    291            �           2606    102481 2   sous_groupe_indicateur sous_groupe_indicateur_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.sous_groupe_indicateur
    ADD CONSTRAINT sous_groupe_indicateur_pkey PRIMARY KEY (code_indicateur, code_sous_groupe);
 \   ALTER TABLE ONLY public.sous_groupe_indicateur DROP CONSTRAINT sous_groupe_indicateur_pkey;
       public            postgres    false    294    294            �           2606    102473    sous_groupe sous_groupe_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.sous_groupe
    ADD CONSTRAINT sous_groupe_pkey PRIMARY KEY (code);
 F   ALTER TABLE ONLY public.sous_groupe DROP CONSTRAINT sous_groupe_pkey;
       public            postgres    false    293            �           2606    102492    station station_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.station
    ADD CONSTRAINT station_pkey PRIMARY KEY (code);
 >   ALTER TABLE ONLY public.station DROP CONSTRAINT station_pkey;
       public            postgres    false    296            �           2606    102503    structure structure_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.structure
    ADD CONSTRAINT structure_pkey PRIMARY KEY (code);
 B   ALTER TABLE ONLY public.structure DROP CONSTRAINT structure_pkey;
       public            postgres    false    298            �           2606    102514    systeme_aep systeme_aep_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.systeme_aep
    ADD CONSTRAINT systeme_aep_pkey PRIMARY KEY (code);
 F   ALTER TABLE ONLY public.systeme_aep DROP CONSTRAINT systeme_aep_pkey;
       public            postgres    false    300            �           2606    102525    t_user t_user_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.t_user
    ADD CONSTRAINT t_user_pkey PRIMARY KEY (code);
 <   ALTER TABLE ONLY public.t_user DROP CONSTRAINT t_user_pkey;
       public            postgres    false    302            �           2606    102536    tuyauterie tuyauterie_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.tuyauterie
    ADD CONSTRAINT tuyauterie_pkey PRIMARY KEY (code);
 D   ALTER TABLE ONLY public.tuyauterie DROP CONSTRAINT tuyauterie_pkey;
       public            postgres    false    304            �           2606    102547 &   type_amenagement type_amenagement_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.type_amenagement
    ADD CONSTRAINT type_amenagement_pkey PRIMARY KEY (code);
 P   ALTER TABLE ONLY public.type_amenagement DROP CONSTRAINT type_amenagement_pkey;
       public            postgres    false    306            �           2606    102558 ,   type_classification type_classification_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.type_classification
    ADD CONSTRAINT type_classification_pkey PRIMARY KEY (code);
 V   ALTER TABLE ONLY public.type_classification DROP CONSTRAINT type_classification_pkey;
       public            postgres    false    308            �           2606    102569 P   type_equipement_station_hydrometrique type_equipement_station_hydrometrique_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.type_equipement_station_hydrometrique
    ADD CONSTRAINT type_equipement_station_hydrometrique_pkey PRIMARY KEY (code);
 z   ALTER TABLE ONLY public.type_equipement_station_hydrometrique DROP CONSTRAINT type_equipement_station_hydrometrique_pkey;
       public            postgres    false    310            �           2606    102580 @   type_equipement_station_meteo type_equipement_station_meteo_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.type_equipement_station_meteo
    ADD CONSTRAINT type_equipement_station_meteo_pkey PRIMARY KEY (code);
 j   ALTER TABLE ONLY public.type_equipement_station_meteo DROP CONSTRAINT type_equipement_station_meteo_pkey;
       public            postgres    false    312                       2606    102591 <   type_etablissement_scolaire type_etablissement_scolaire_pkey 
   CONSTRAINT     |   ALTER TABLE ONLY public.type_etablissement_scolaire
    ADD CONSTRAINT type_etablissement_scolaire_pkey PRIMARY KEY (code);
 f   ALTER TABLE ONLY public.type_etablissement_scolaire DROP CONSTRAINT type_etablissement_scolaire_pkey;
       public            postgres    false    314                       2606    102602    type_exhaure type_exhaure_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.type_exhaure
    ADD CONSTRAINT type_exhaure_pkey PRIMARY KEY (code);
 H   ALTER TABLE ONLY public.type_exhaure DROP CONSTRAINT type_exhaure_pkey;
       public            postgres    false    316            	           2606    102613 6   type_formation_sanitaire type_formation_sanitaire_pkey 
   CONSTRAINT     v   ALTER TABLE ONLY public.type_formation_sanitaire
    ADD CONSTRAINT type_formation_sanitaire_pkey PRIMARY KEY (code);
 `   ALTER TABLE ONLY public.type_formation_sanitaire DROP CONSTRAINT type_formation_sanitaire_pkey;
       public            postgres    false    318                       2606    102624 &   type_intervenant type_intervenant_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.type_intervenant
    ADD CONSTRAINT type_intervenant_pkey PRIMARY KEY (code);
 P   ALTER TABLE ONLY public.type_intervenant DROP CONSTRAINT type_intervenant_pkey;
       public            postgres    false    320                       2606    102635 &   type_lieu_public type_lieu_public_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.type_lieu_public
    ADD CONSTRAINT type_lieu_public_pkey PRIMARY KEY (code);
 P   ALTER TABLE ONLY public.type_lieu_public DROP CONSTRAINT type_lieu_public_pkey;
       public            postgres    false    322                       2606    102646     type_localite type_localite_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.type_localite
    ADD CONSTRAINT type_localite_pkey PRIMARY KEY (code);
 J   ALTER TABLE ONLY public.type_localite DROP CONSTRAINT type_localite_pkey;
       public            postgres    false    324                       2606    102657    type_ouvrage type_ouvrage_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.type_ouvrage
    ADD CONSTRAINT type_ouvrage_pkey PRIMARY KEY (code);
 H   ALTER TABLE ONLY public.type_ouvrage DROP CONSTRAINT type_ouvrage_pkey;
       public            postgres    false    326                       2606    102668 >   type_piece_regulation_reseau type_piece_regulation_reseau_pkey 
   CONSTRAINT     ~   ALTER TABLE ONLY public.type_piece_regulation_reseau
    ADD CONSTRAINT type_piece_regulation_reseau_pkey PRIMARY KEY (code);
 h   ALTER TABLE ONLY public.type_piece_regulation_reseau DROP CONSTRAINT type_piece_regulation_reseau_pkey;
       public            postgres    false    328            !           2606    102679     type_plan_eau type_plan_eau_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.type_plan_eau
    ADD CONSTRAINT type_plan_eau_pkey PRIMARY KEY (code);
 J   ALTER TABLE ONLY public.type_plan_eau DROP CONSTRAINT type_plan_eau_pkey;
       public            postgres    false    330            %           2606    102690 $   type_prestation type_prestation_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.type_prestation
    ADD CONSTRAINT type_prestation_pkey PRIMARY KEY (code);
 N   ALTER TABLE ONLY public.type_prestation DROP CONSTRAINT type_prestation_pkey;
       public            postgres    false    332            )           2606    102701    type_puits type_puits_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.type_puits
    ADD CONSTRAINT type_puits_pkey PRIMARY KEY (code);
 D   ALTER TABLE ONLY public.type_puits DROP CONSTRAINT type_puits_pkey;
       public            postgres    false    334            -           2606    102712 "   type_ressource type_ressource_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.type_ressource
    ADD CONSTRAINT type_ressource_pkey PRIMARY KEY (code);
 L   ALTER TABLE ONLY public.type_ressource DROP CONSTRAINT type_ressource_pkey;
       public            postgres    false    336            1           2606    102723 N   type_ressource_station_hydrometrique type_ressource_station_hydrometrique_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.type_ressource_station_hydrometrique
    ADD CONSTRAINT type_ressource_station_hydrometrique_pkey PRIMARY KEY (code);
 x   ALTER TABLE ONLY public.type_ressource_station_hydrometrique DROP CONSTRAINT type_ressource_station_hydrometrique_pkey;
       public            postgres    false    338            5           2606    102734 *   type_station_meteo type_station_meteo_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.type_station_meteo
    ADD CONSTRAINT type_station_meteo_pkey PRIMARY KEY (code);
 T   ALTER TABLE ONLY public.type_station_meteo DROP CONSTRAINT type_station_meteo_pkey;
       public            postgres    false    340            9           2606    102745 &   type_systeme_aep type_systeme_aep_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.type_systeme_aep
    ADD CONSTRAINT type_systeme_aep_pkey PRIMARY KEY (code);
 P   ALTER TABLE ONLY public.type_systeme_aep DROP CONSTRAINT type_systeme_aep_pkey;
       public            postgres    false    342            =           2606    102756 8   type_systeme_installation type_systeme_installation_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.type_systeme_installation
    ADD CONSTRAINT type_systeme_installation_pkey PRIMARY KEY (code);
 b   ALTER TABLE ONLY public.type_systeme_installation DROP CONSTRAINT type_systeme_installation_pkey;
       public            postgres    false    344            A           2606    102767 4   type_systeme_protection type_systeme_protection_pkey 
   CONSTRAINT     t   ALTER TABLE ONLY public.type_systeme_protection
    ADD CONSTRAINT type_systeme_protection_pkey PRIMARY KEY (code);
 ^   ALTER TABLE ONLY public.type_systeme_protection DROP CONSTRAINT type_systeme_protection_pkey;
       public            postgres    false    346            E           2606    102778    type_tube type_tube_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.type_tube
    ADD CONSTRAINT type_tube_pkey PRIMARY KEY (code);
 B   ALTER TABLE ONLY public.type_tube DROP CONSTRAINT type_tube_pkey;
       public            postgres    false    348            I           2606    102789    type_usage type_usage_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.type_usage
    ADD CONSTRAINT type_usage_pkey PRIMARY KEY (code);
 D   ALTER TABLE ONLY public.type_usage DROP CONSTRAINT type_usage_pkey;
       public            postgres    false    350            #           2606    102892 *   type_plan_eau uk_2m73eetgm2sa46cqbptnx8ku6 
   CONSTRAINT     h   ALTER TABLE ONLY public.type_plan_eau
    ADD CONSTRAINT uk_2m73eetgm2sa46cqbptnx8ku6 UNIQUE (libelle);
 T   ALTER TABLE ONLY public.type_plan_eau DROP CONSTRAINT uk_2m73eetgm2sa46cqbptnx8ku6;
       public            postgres    false    330            �           2606    102864 &   structure uk_2w71n1qwuytw1l0h3wqakevnr 
   CONSTRAINT     d   ALTER TABLE ONLY public.structure
    ADD CONSTRAINT uk_2w71n1qwuytw1l0h3wqakevnr UNIQUE (libelle);
 P   ALTER TABLE ONLY public.structure DROP CONSTRAINT uk_2w71n1qwuytw1l0h3wqakevnr;
       public            postgres    false    298            �           2606    102830 (   hydraulique uk_31txboj3jl71gyhmht86xcean 
   CONSTRAINT     f   ALTER TABLE ONLY public.hydraulique
    ADD CONSTRAINT uk_31txboj3jl71gyhmht86xcean UNIQUE (libelle);
 R   ALTER TABLE ONLY public.hydraulique DROP CONSTRAINT uk_31txboj3jl71gyhmht86xcean;
       public            postgres    false    236                       2606    102878 )   type_exhaure uk_4pdilq4ubyp79gplqt95j535o 
   CONSTRAINT     g   ALTER TABLE ONLY public.type_exhaure
    ADD CONSTRAINT uk_4pdilq4ubyp79gplqt95j535o UNIQUE (libelle);
 S   ALTER TABLE ONLY public.type_exhaure DROP CONSTRAINT uk_4pdilq4ubyp79gplqt95j535o;
       public            postgres    false    316            ?           2606    102906 6   type_systeme_installation uk_5asnieph4jleok144g1kt8nm6 
   CONSTRAINT     t   ALTER TABLE ONLY public.type_systeme_installation
    ADD CONSTRAINT uk_5asnieph4jleok144g1kt8nm6 UNIQUE (libelle);
 `   ALTER TABLE ONLY public.type_systeme_installation DROP CONSTRAINT uk_5asnieph4jleok144g1kt8nm6;
       public            postgres    false    344            �           2606    102862 (   sous_groupe uk_6d8m33hcyyhc09ebb1q7duton 
   CONSTRAINT     f   ALTER TABLE ONLY public.sous_groupe
    ADD CONSTRAINT uk_6d8m33hcyyhc09ebb1q7duton UNIQUE (libelle);
 R   ALTER TABLE ONLY public.sous_groupe DROP CONSTRAINT uk_6d8m33hcyyhc09ebb1q7duton;
       public            postgres    false    293            �           2606    102852 4   parametre_station_meteo uk_9e8eetwdqqtawflo83llfu61e 
   CONSTRAINT     r   ALTER TABLE ONLY public.parametre_station_meteo
    ADD CONSTRAINT uk_9e8eetwdqqtawflo83llfu61e UNIQUE (libelle);
 ^   ALTER TABLE ONLY public.parametre_station_meteo DROP CONSTRAINT uk_9e8eetwdqqtawflo83llfu61e;
       public            postgres    false    265            �           2606    102858 &   propriete uk_9khd2y3yp2rm1a1yuasi6jexb 
   CONSTRAINT     d   ALTER TABLE ONLY public.propriete
    ADD CONSTRAINT uk_9khd2y3yp2rm1a1yuasi6jexb UNIQUE (libelle);
 P   ALTER TABLE ONLY public.propriete DROP CONSTRAINT uk_9khd2y3yp2rm1a1yuasi6jexb;
       public            postgres    false    276            �           2606    102846 '   nature_sol uk_9m11a1gghchkl4ambdxcnmk46 
   CONSTRAINT     e   ALTER TABLE ONLY public.nature_sol
    ADD CONSTRAINT uk_9m11a1gghchkl4ambdxcnmk46 UNIQUE (libelle);
 Q   ALTER TABLE ONLY public.nature_sol DROP CONSTRAINT uk_9m11a1gghchkl4ambdxcnmk46;
       public            postgres    false    259            �           2606    102840 -   methode_collecte uk_9xw7vjt9tj2nu8vmowaavnss0 
   CONSTRAINT     k   ALTER TABLE ONLY public.methode_collecte
    ADD CONSTRAINT uk_9xw7vjt9tj2nu8vmowaavnss0 UNIQUE (libelle);
 W   ALTER TABLE ONLY public.methode_collecte DROP CONSTRAINT uk_9xw7vjt9tj2nu8vmowaavnss0;
       public            postgres    false    253            e           2606    102824 (   disposition uk_agpvolwmmy7xcyctcuqtxqox9 
   CONSTRAINT     f   ALTER TABLE ONLY public.disposition
    ADD CONSTRAINT uk_agpvolwmmy7xcyctcuqtxqox9 UNIQUE (libelle);
 R   ALTER TABLE ONLY public.disposition DROP CONSTRAINT uk_agpvolwmmy7xcyctcuqtxqox9;
       public            postgres    false    218            C           2606    102908 4   type_systeme_protection uk_b1u0y3cxrj769agh9x39mmh7h 
   CONSTRAINT     r   ALTER TABLE ONLY public.type_systeme_protection
    ADD CONSTRAINT uk_b1u0y3cxrj769agh9x39mmh7h UNIQUE (libelle);
 ^   ALTER TABLE ONLY public.type_systeme_protection DROP CONSTRAINT uk_b1u0y3cxrj769agh9x39mmh7h;
       public            postgres    false    346            �           2606    102848 (   nature_tube uk_b4h8ngt12257cac6htsrmnbt0 
   CONSTRAINT     f   ALTER TABLE ONLY public.nature_tube
    ADD CONSTRAINT uk_b4h8ngt12257cac6htsrmnbt0 UNIQUE (libelle);
 R   ALTER TABLE ONLY public.nature_tube DROP CONSTRAINT uk_b4h8ngt12257cac6htsrmnbt0;
       public            postgres    false    261            �           2606    102870 0   type_classification uk_bdabo40k4myq8qg69rp7rbpnx 
   CONSTRAINT     n   ALTER TABLE ONLY public.type_classification
    ADD CONSTRAINT uk_bdabo40k4myq8qg69rp7rbpnx UNIQUE (libelle);
 Z   ALTER TABLE ONLY public.type_classification DROP CONSTRAINT uk_bdabo40k4myq8qg69rp7rbpnx;
       public            postgres    false    308            �           2606    102844 )   modele_pompe uk_bg0835celrt18aviedt3e4aot 
   CONSTRAINT     g   ALTER TABLE ONLY public.modele_pompe
    ADD CONSTRAINT uk_bg0835celrt18aviedt3e4aot UNIQUE (libelle);
 S   ALTER TABLE ONLY public.modele_pompe DROP CONSTRAINT uk_bg0835celrt18aviedt3e4aot;
       public            postgres    false    257            ;           2606    102904 -   type_systeme_aep uk_c90mm5ar49jrat3ikliosiq80 
   CONSTRAINT     k   ALTER TABLE ONLY public.type_systeme_aep
    ADD CONSTRAINT uk_c90mm5ar49jrat3ikliosiq80 UNIQUE (libelle);
 W   ALTER TABLE ONLY public.type_systeme_aep DROP CONSTRAINT uk_c90mm5ar49jrat3ikliosiq80;
       public            postgres    false    342            �           2606    102838 &   materiaux uk_elkuq2cnnl07ta8h5q8l8wpie 
   CONSTRAINT     d   ALTER TABLE ONLY public.materiaux
    ADD CONSTRAINT uk_elkuq2cnnl07ta8h5q8l8wpie UNIQUE (libelle);
 P   ALTER TABLE ONLY public.materiaux DROP CONSTRAINT uk_elkuq2cnnl07ta8h5q8l8wpie;
       public            postgres    false    251            �           2606    102868 -   type_amenagement uk_eqgwfqd0sek5s6advmh5c0xqc 
   CONSTRAINT     k   ALTER TABLE ONLY public.type_amenagement
    ADD CONSTRAINT uk_eqgwfqd0sek5s6advmh5c0xqc UNIQUE (libelle);
 W   ALTER TABLE ONLY public.type_amenagement DROP CONSTRAINT uk_eqgwfqd0sek5s6advmh5c0xqc;
       public            postgres    false    306            �           2606    102842 #   milieu uk_fjilpya8rpi77fq62lrtd0cql 
   CONSTRAINT     a   ALTER TABLE ONLY public.milieu
    ADD CONSTRAINT uk_fjilpya8rpi77fq62lrtd0cql UNIQUE (libelle);
 M   ALTER TABLE ONLY public.milieu DROP CONSTRAINT uk_fjilpya8rpi77fq62lrtd0cql;
       public            postgres    false    255                       2606    102888 )   type_ouvrage uk_huou7probmngm0ysrnni82ug2 
   CONSTRAINT     g   ALTER TABLE ONLY public.type_ouvrage
    ADD CONSTRAINT uk_huou7probmngm0ysrnni82ug2 UNIQUE (libelle);
 S   ALTER TABLE ONLY public.type_ouvrage DROP CONSTRAINT uk_huou7probmngm0ysrnni82ug2;
       public            postgres    false    326            3           2606    102900 A   type_ressource_station_hydrometrique uk_i5cjbmjfx3quqgtw1m30x5qid 
   CONSTRAINT        ALTER TABLE ONLY public.type_ressource_station_hydrometrique
    ADD CONSTRAINT uk_i5cjbmjfx3quqgtw1m30x5qid UNIQUE (libelle);
 k   ALTER TABLE ONLY public.type_ressource_station_hydrometrique DROP CONSTRAINT uk_i5cjbmjfx3quqgtw1m30x5qid;
       public            postgres    false    338            �           2606    102856 #   projet uk_iw6bx9ujff4pku5x33fec46oh 
   CONSTRAINT     a   ALTER TABLE ONLY public.projet
    ADD CONSTRAINT uk_iw6bx9ujff4pku5x33fec46oh UNIQUE (libelle);
 M   ALTER TABLE ONLY public.projet DROP CONSTRAINT uk_iw6bx9ujff4pku5x33fec46oh;
       public            postgres    false    274            �           2606    102866 '   tuyauterie uk_k8fnx47e6g345ijoe08ftir2c 
   CONSTRAINT     e   ALTER TABLE ONLY public.tuyauterie
    ADD CONSTRAINT uk_k8fnx47e6g345ijoe08ftir2c UNIQUE (libelle);
 Q   ALTER TABLE ONLY public.tuyauterie DROP CONSTRAINT uk_k8fnx47e6g345ijoe08ftir2c;
       public            postgres    false    304            w           2606    102828 (   financement uk_klghjtwdbmadwxn8fkjhnobms 
   CONSTRAINT     f   ALTER TABLE ONLY public.financement
    ADD CONSTRAINT uk_klghjtwdbmadwxn8fkjhnobms UNIQUE (libelle);
 R   ALTER TABLE ONLY public.financement DROP CONSTRAINT uk_klghjtwdbmadwxn8fkjhnobms;
       public            postgres    false    229            /           2606    102898 +   type_ressource uk_l2k74r2u0c58sgqrev61iwtgn 
   CONSTRAINT     i   ALTER TABLE ONLY public.type_ressource
    ADD CONSTRAINT uk_l2k74r2u0c58sgqrev61iwtgn UNIQUE (libelle);
 U   ALTER TABLE ONLY public.type_ressource DROP CONSTRAINT uk_l2k74r2u0c58sgqrev61iwtgn;
       public            postgres    false    336            q           2606    102826 )   etat_ouvrage uk_l6fyfje78kmcr2qklf6nm1w7v 
   CONSTRAINT     g   ALTER TABLE ONLY public.etat_ouvrage
    ADD CONSTRAINT uk_l6fyfje78kmcr2qklf6nm1w7v UNIQUE (libelle);
 S   ALTER TABLE ONLY public.etat_ouvrage DROP CONSTRAINT uk_l6fyfje78kmcr2qklf6nm1w7v;
       public            postgres    false    225                       2606    102882 -   type_intervenant uk_mudj824sx7vmne7u6v4k0itts 
   CONSTRAINT     k   ALTER TABLE ONLY public.type_intervenant
    ADD CONSTRAINT uk_mudj824sx7vmne7u6v4k0itts UNIQUE (libelle);
 W   ALTER TABLE ONLY public.type_intervenant DROP CONSTRAINT uk_mudj824sx7vmne7u6v4k0itts;
       public            postgres    false    320            G           2606    102910 &   type_tube uk_mvq9qj7snkerovd9e0g3xf5bw 
   CONSTRAINT     d   ALTER TABLE ONLY public.type_tube
    ADD CONSTRAINT uk_mvq9qj7snkerovd9e0g3xf5bw UNIQUE (libelle);
 P   ALTER TABLE ONLY public.type_tube DROP CONSTRAINT uk_mvq9qj7snkerovd9e0g3xf5bw;
       public            postgres    false    348            �           2606    102850 <   parametre_station_hydrometrique uk_noho0mubkvk80dtewukhrtbf7 
   CONSTRAINT     z   ALTER TABLE ONLY public.parametre_station_hydrometrique
    ADD CONSTRAINT uk_noho0mubkvk80dtewukhrtbf7 UNIQUE (libelle);
 f   ALTER TABLE ONLY public.parametre_station_hydrometrique DROP CONSTRAINT uk_noho0mubkvk80dtewukhrtbf7;
       public            postgres    false    263            K           2606    102912 '   type_usage uk_nt9145vwd1sw3il96qh25ruqm 
   CONSTRAINT     e   ALTER TABLE ONLY public.type_usage
    ADD CONSTRAINT uk_nt9145vwd1sw3il96qh25ruqm UNIQUE (libelle);
 Q   ALTER TABLE ONLY public.type_usage DROP CONSTRAINT uk_nt9145vwd1sw3il96qh25ruqm;
       public            postgres    false    350            �           2606    102832 '   indicateur uk_o7hndgmg6mcktdy7wnpi73fcl 
   CONSTRAINT     e   ALTER TABLE ONLY public.indicateur
    ADD CONSTRAINT uk_o7hndgmg6mcktdy7wnpi73fcl UNIQUE (libelle);
 Q   ALTER TABLE ONLY public.indicateur DROP CONSTRAINT uk_o7hndgmg6mcktdy7wnpi73fcl;
       public            postgres    false    238            �           2606    102874 :   type_equipement_station_meteo uk_obgn9rf2emtsx4rnc71lw1jqt 
   CONSTRAINT     x   ALTER TABLE ONLY public.type_equipement_station_meteo
    ADD CONSTRAINT uk_obgn9rf2emtsx4rnc71lw1jqt UNIQUE (libelle);
 d   ALTER TABLE ONLY public.type_equipement_station_meteo DROP CONSTRAINT uk_obgn9rf2emtsx4rnc71lw1jqt;
       public            postgres    false    312                       2606    102884 -   type_lieu_public uk_ohbtuatefcbs4cstoua2wcx6x 
   CONSTRAINT     k   ALTER TABLE ONLY public.type_lieu_public
    ADD CONSTRAINT uk_ohbtuatefcbs4cstoua2wcx6x UNIQUE (libelle);
 W   ALTER TABLE ONLY public.type_lieu_public DROP CONSTRAINT uk_ohbtuatefcbs4cstoua2wcx6x;
       public            postgres    false    322            �           2606    102860 #   source uk_p2sd9ljk8vqnckqu127kor411 
   CONSTRAINT     a   ALTER TABLE ONLY public.source
    ADD CONSTRAINT uk_p2sd9ljk8vqnckqu127kor411 UNIQUE (libelle);
 M   ALTER TABLE ONLY public.source DROP CONSTRAINT uk_p2sd9ljk8vqnckqu127kor411;
       public            postgres    false    291            �           2606    102872 B   type_equipement_station_hydrometrique uk_pqv9pemn6qdufrucke0a4og6y 
   CONSTRAINT     �   ALTER TABLE ONLY public.type_equipement_station_hydrometrique
    ADD CONSTRAINT uk_pqv9pemn6qdufrucke0a4og6y UNIQUE (libelle);
 l   ALTER TABLE ONLY public.type_equipement_station_hydrometrique DROP CONSTRAINT uk_pqv9pemn6qdufrucke0a4og6y;
       public            postgres    false    310                       2606    102890 9   type_piece_regulation_reseau uk_pvmu3hs7ld0jex9txdiwxv4j9 
   CONSTRAINT     w   ALTER TABLE ONLY public.type_piece_regulation_reseau
    ADD CONSTRAINT uk_pvmu3hs7ld0jex9txdiwxv4j9 UNIQUE (libelle);
 c   ALTER TABLE ONLY public.type_piece_regulation_reseau DROP CONSTRAINT uk_pvmu3hs7ld0jex9txdiwxv4j9;
       public            postgres    false    328                       2606    102876 8   type_etablissement_scolaire uk_q1es2jddx61w1623jrry9ujip 
   CONSTRAINT     v   ALTER TABLE ONLY public.type_etablissement_scolaire
    ADD CONSTRAINT uk_q1es2jddx61w1623jrry9ujip UNIQUE (libelle);
 b   ALTER TABLE ONLY public.type_etablissement_scolaire DROP CONSTRAINT uk_q1es2jddx61w1623jrry9ujip;
       public            postgres    false    314            7           2606    102902 /   type_station_meteo uk_qk2dduyeb46o5tnc576m1jnx7 
   CONSTRAINT     m   ALTER TABLE ONLY public.type_station_meteo
    ADD CONSTRAINT uk_qk2dduyeb46o5tnc576m1jnx7 UNIQUE (libelle);
 Y   ALTER TABLE ONLY public.type_station_meteo DROP CONSTRAINT uk_qk2dduyeb46o5tnc576m1jnx7;
       public            postgres    false    340            �           2606    102834 !   lieu uk_rk6ggivk0ojtgvg8pykvp931m 
   CONSTRAINT     _   ALTER TABLE ONLY public.lieu
    ADD CONSTRAINT uk_rk6ggivk0ojtgvg8pykvp931m UNIQUE (libelle);
 K   ALTER TABLE ONLY public.lieu DROP CONSTRAINT uk_rk6ggivk0ojtgvg8pykvp931m;
       public            postgres    false    243            �           2606    102854 $   periode uk_rl96cs6uggv1qgs3v3f1hi7pm 
   CONSTRAINT     b   ALTER TABLE ONLY public.periode
    ADD CONSTRAINT uk_rl96cs6uggv1qgs3v3f1hi7pm UNIQUE (libelle);
 N   ALTER TABLE ONLY public.periode DROP CONSTRAINT uk_rl96cs6uggv1qgs3v3f1hi7pm;
       public            postgres    false    267            +           2606    102896 '   type_puits uk_s90jiyf7rvb1bpvhy6ilqxcxd 
   CONSTRAINT     e   ALTER TABLE ONLY public.type_puits
    ADD CONSTRAINT uk_s90jiyf7rvb1bpvhy6ilqxcxd UNIQUE (libelle);
 Q   ALTER TABLE ONLY public.type_puits DROP CONSTRAINT uk_s90jiyf7rvb1bpvhy6ilqxcxd;
       public            postgres    false    334            �           2606    102836 )   marque_pompe uk_s98ydsryeieh2rcqaybjc5qe6 
   CONSTRAINT     g   ALTER TABLE ONLY public.marque_pompe
    ADD CONSTRAINT uk_s98ydsryeieh2rcqaybjc5qe6 UNIQUE (libelle);
 S   ALTER TABLE ONLY public.marque_pompe DROP CONSTRAINT uk_s98ydsryeieh2rcqaybjc5qe6;
       public            postgres    false    249            '           2606    102894 ,   type_prestation uk_sljyo36xv38x7hh7qdg128rpb 
   CONSTRAINT     j   ALTER TABLE ONLY public.type_prestation
    ADD CONSTRAINT uk_sljyo36xv38x7hh7qdg128rpb UNIQUE (libelle);
 V   ALTER TABLE ONLY public.type_prestation DROP CONSTRAINT uk_sljyo36xv38x7hh7qdg128rpb;
       public            postgres    false    332            M           2606    102914 "   unite uk_ssh504ufedg2mqdl5uqsum93r 
   CONSTRAINT     `   ALTER TABLE ONLY public.unite
    ADD CONSTRAINT uk_ssh504ufedg2mqdl5uqsum93r UNIQUE (libelle);
 L   ALTER TABLE ONLY public.unite DROP CONSTRAINT uk_ssh504ufedg2mqdl5uqsum93r;
       public            postgres    false    352                       2606    102880 5   type_formation_sanitaire uk_td4sphg7u9qvn85sy51wk0udv 
   CONSTRAINT     s   ALTER TABLE ONLY public.type_formation_sanitaire
    ADD CONSTRAINT uk_td4sphg7u9qvn85sy51wk0udv UNIQUE (libelle);
 _   ALTER TABLE ONLY public.type_formation_sanitaire DROP CONSTRAINT uk_td4sphg7u9qvn85sy51wk0udv;
       public            postgres    false    318                       2606    102886 *   type_localite uk_te96l1xdmlqfuhd7b8uxmn8j2 
   CONSTRAINT     h   ALTER TABLE ONLY public.type_localite
    ADD CONSTRAINT uk_te96l1xdmlqfuhd7b8uxmn8j2 UNIQUE (libelle);
 T   ALTER TABLE ONLY public.type_localite DROP CONSTRAINT uk_te96l1xdmlqfuhd7b8uxmn8j2;
       public            postgres    false    324            Q           2606    102808 &   unite_indicateur unite_indicateur_pkey 
   CONSTRAINT     }   ALTER TABLE ONLY public.unite_indicateur
    ADD CONSTRAINT unite_indicateur_pkey PRIMARY KEY (code_indicateur, code_unite);
 P   ALTER TABLE ONLY public.unite_indicateur DROP CONSTRAINT unite_indicateur_pkey;
       public            postgres    false    353    353            O           2606    102800    unite unite_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.unite
    ADD CONSTRAINT unite_pkey PRIMARY KEY (code);
 :   ALTER TABLE ONLY public.unite DROP CONSTRAINT unite_pkey;
       public            postgres    false    352            S           2606    102822    zone zone_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.zone
    ADD CONSTRAINT zone_pkey PRIMARY KEY (code);
 8   ALTER TABLE ONLY public.zone DROP CONSTRAINT zone_pkey;
       public            postgres    false    356            �           2606    103152 5   indicateur_classification fk11ohc7sitbryh7qdtwdpj80m4    FK CONSTRAINT     �   ALTER TABLE ONLY public.indicateur_classification
    ADD CONSTRAINT fk11ohc7sitbryh7qdtwdpj80m4 FOREIGN KEY (code_indicateur) REFERENCES public.indicateur(code);
 _   ALTER TABLE ONLY public.indicateur_classification DROP CONSTRAINT fk11ohc7sitbryh7qdtwdpj80m4;
       public          postgres    false    4227    239    238            �           2606    103207 *   pompe_installe fk1en1kdpr9tenxq506ru0byg1r    FK CONSTRAINT     �   ALTER TABLE ONLY public.pompe_installe
    ADD CONSTRAINT fk1en1kdpr9tenxq506ru0byg1r FOREIGN KEY (code_modele_pompe) REFERENCES public.modele_pompe(code);
 T   ALTER TABLE ONLY public.pompe_installe DROP CONSTRAINT fk1en1kdpr9tenxq506ru0byg1r;
       public          postgres    false    4259    257    270            �           2606    103177 $   localite fk1nptbrukrygjvsdvi4fkgq8v1    FK CONSTRAINT     �   ALTER TABLE ONLY public.localite
    ADD CONSTRAINT fk1nptbrukrygjvsdvi4fkgq8v1 FOREIGN KEY (code_localite_rattachement) REFERENCES public.localite(code);
 N   ALTER TABLE ONLY public.localite DROP CONSTRAINT fk1nptbrukrygjvsdvi4fkgq8v1;
       public          postgres    false    4241    247    247            �           2606    103387     zone fk28gfcud12qs6b3k5xc55mvp1i    FK CONSTRAINT     �   ALTER TABLE ONLY public.zone
    ADD CONSTRAINT fk28gfcud12qs6b3k5xc55mvp1i FOREIGN KEY (code_milieu) REFERENCES public.milieu(code);
 J   ALTER TABLE ONLY public.zone DROP CONSTRAINT fk28gfcud12qs6b3k5xc55mvp1i;
       public          postgres    false    356    255    4255            w           2606    103082 2   etablissement_scolaire fk2c5m85dfuate1xl67qusq1699    FK CONSTRAINT     �   ALTER TABLE ONLY public.etablissement_scolaire
    ADD CONSTRAINT fk2c5m85dfuate1xl67qusq1699 FOREIGN KEY (code_type_etablissement_scolaire) REFERENCES public.type_etablissement_scolaire(code);
 \   ALTER TABLE ONLY public.etablissement_scolaire DROP CONSTRAINT fk2c5m85dfuate1xl67qusq1699;
       public          postgres    false    4353    223    314            l           2606    103027 -   donnee_indicateur fk2e0981edy0t8rmgtc0qwmqac3    FK CONSTRAINT     �   ALTER TABLE ONLY public.donnee_indicateur
    ADD CONSTRAINT fk2e0981edy0t8rmgtc0qwmqac3 FOREIGN KEY (code_indicateur) REFERENCES public.indicateur(code);
 W   ALTER TABLE ONLY public.donnee_indicateur DROP CONSTRAINT fk2e0981edy0t8rmgtc0qwmqac3;
       public          postgres    false    238    4227    219            �           2606    103277 ,   roles_privileges fk2fhnk2ccuyogt1yeaglcpcqh9    FK CONSTRAINT     �   ALTER TABLE ONLY public.roles_privileges
    ADD CONSTRAINT fk2fhnk2ccuyogt1yeaglcpcqh9 FOREIGN KEY (privilege_code) REFERENCES public.privilege(code);
 V   ALTER TABLE ONLY public.roles_privileges DROP CONSTRAINT fk2fhnk2ccuyogt1yeaglcpcqh9;
       public          postgres    false    283    272    4291            e           2606    102992 '   chateau_eau fk313axxuy79chabcpf2bvysr85    FK CONSTRAINT     �   ALTER TABLE ONLY public.chateau_eau
    ADD CONSTRAINT fk313axxuy79chabcpf2bvysr85 FOREIGN KEY (code_systeme_aep) REFERENCES public.systeme_aep(code);
 Q   ALTER TABLE ONLY public.chateau_eau DROP CONSTRAINT fk313axxuy79chabcpf2bvysr85;
       public          postgres    false    212    300    4329            v           2606    103077 2   etablissement_scolaire fk3uf1jrlu3em1f5a4i368tmw9h    FK CONSTRAINT     �   ALTER TABLE ONLY public.etablissement_scolaire
    ADD CONSTRAINT fk3uf1jrlu3em1f5a4i368tmw9h FOREIGN KEY (code_localite) REFERENCES public.localite(code);
 \   ALTER TABLE ONLY public.etablissement_scolaire DROP CONSTRAINT fk3uf1jrlu3em1f5a4i368tmw9h;
       public          postgres    false    4241    223    247            r           2606    103057 -   equipement_forage fk41kymmq7egli2xchu1th4jqsg    FK CONSTRAINT     �   ALTER TABLE ONLY public.equipement_forage
    ADD CONSTRAINT fk41kymmq7egli2xchu1th4jqsg FOREIGN KEY (code_nature_tube) REFERENCES public.nature_tube(code);
 W   ALTER TABLE ONLY public.equipement_forage DROP CONSTRAINT fk41kymmq7egli2xchu1th4jqsg;
       public          postgres    false    220    261    4267            \           2606    102947 /   captage_systeme_aep fk488gr5ntt0p2vd5a1jihghnv3    FK CONSTRAINT     �   ALTER TABLE ONLY public.captage_systeme_aep
    ADD CONSTRAINT fk488gr5ntt0p2vd5a1jihghnv3 FOREIGN KEY (code_marque_pompe2) REFERENCES public.marque_pompe(code);
 Y   ALTER TABLE ONLY public.captage_systeme_aep DROP CONSTRAINT fk488gr5ntt0p2vd5a1jihghnv3;
       public          postgres    false    249    211    4243            X           2606    102927 3   branchement_particulier fk4lr00l4tggkcgc5f459kcc5ov    FK CONSTRAINT     �   ALTER TABLE ONLY public.branchement_particulier
    ADD CONSTRAINT fk4lr00l4tggkcgc5f459kcc5ov FOREIGN KEY (code_lieu) REFERENCES public.lieu(code);
 ]   ALTER TABLE ONLY public.branchement_particulier DROP CONSTRAINT fk4lr00l4tggkcgc5f459kcc5ov;
       public          postgres    false    210    243    4235            j           2606    103017 *   classification fk4x01brtu2g07g3wr3l3m3pjjy    FK CONSTRAINT     �   ALTER TABLE ONLY public.classification
    ADD CONSTRAINT fk4x01brtu2g07g3wr3l3m3pjjy FOREIGN KEY (code_type_classification) REFERENCES public.type_classification(code);
 T   ALTER TABLE ONLY public.classification DROP CONSTRAINT fk4x01brtu2g07g3wr3l3m3pjjy;
       public          postgres    false    308    215    4341            �           2606    103342 '   systeme_aep fk53fsg3i57te28ge9qhllrw6k9    FK CONSTRAINT     �   ALTER TABLE ONLY public.systeme_aep
    ADD CONSTRAINT fk53fsg3i57te28ge9qhllrw6k9 FOREIGN KEY (code_financement) REFERENCES public.financement(code);
 Q   ALTER TABLE ONLY public.systeme_aep DROP CONSTRAINT fk53fsg3i57te28ge9qhllrw6k9;
       public          postgres    false    4213    229    300            ^           2606    102957 /   captage_systeme_aep fk5gcxnm52cbsl2e8d0owofi13p    FK CONSTRAINT     �   ALTER TABLE ONLY public.captage_systeme_aep
    ADD CONSTRAINT fk5gcxnm52cbsl2e8d0owofi13p FOREIGN KEY (code_modele_pompe2) REFERENCES public.modele_pompe(code);
 Y   ALTER TABLE ONLY public.captage_systeme_aep DROP CONSTRAINT fk5gcxnm52cbsl2e8d0owofi13p;
       public          postgres    false    257    211    4259            b           2606    102977 '   chateau_eau fk6olm9t74450cj1qgr3hl15u39    FK CONSTRAINT     �   ALTER TABLE ONLY public.chateau_eau
    ADD CONSTRAINT fk6olm9t74450cj1qgr3hl15u39 FOREIGN KEY (code_disposition) REFERENCES public.disposition(code);
 Q   ALTER TABLE ONLY public.chateau_eau DROP CONSTRAINT fk6olm9t74450cj1qgr3hl15u39;
       public          postgres    false    212    218    4195            _           2606    102962 /   captage_systeme_aep fk737tnraa5f2e6lcopgnsq05wt    FK CONSTRAINT     �   ALTER TABLE ONLY public.captage_systeme_aep
    ADD CONSTRAINT fk737tnraa5f2e6lcopgnsq05wt FOREIGN KEY (code_systeme_aep) REFERENCES public.systeme_aep(code);
 Y   ALTER TABLE ONLY public.captage_systeme_aep DROP CONSTRAINT fk737tnraa5f2e6lcopgnsq05wt;
       public          postgres    false    4329    300    211            u           2606    103072 /   equipement_installe fk7ojakn2idy3p9cjifr8k7wl1r    FK CONSTRAINT     �   ALTER TABLE ONLY public.equipement_installe
    ADD CONSTRAINT fk7ojakn2idy3p9cjifr8k7wl1r FOREIGN KEY (code_type_equipement_station) REFERENCES public.type_equipement_station_meteo(code);
 Y   ALTER TABLE ONLY public.equipement_installe DROP CONSTRAINT fk7ojakn2idy3p9cjifr8k7wl1r;
       public          postgres    false    4349    312    221            g           2606    103002 1   chronique_observation fk7w01r5difgfo41vgx0x2v9hlf    FK CONSTRAINT     �   ALTER TABLE ONLY public.chronique_observation
    ADD CONSTRAINT fk7w01r5difgfo41vgx0x2v9hlf FOREIGN KEY (code_station) REFERENCES public.station(code);
 [   ALTER TABLE ONLY public.chronique_observation DROP CONSTRAINT fk7w01r5difgfo41vgx0x2v9hlf;
       public          postgres    false    213    296    4323            �           2606    103187 #   periode fk96t5knym8jwihlh6vt7s9w9ui    FK CONSTRAINT     �   ALTER TABLE ONLY public.periode
    ADD CONSTRAINT fk96t5knym8jwihlh6vt7s9w9ui FOREIGN KEY (parent_code) REFERENCES public.periode(code);
 M   ALTER TABLE ONLY public.periode DROP CONSTRAINT fk96t5knym8jwihlh6vt7s9w9ui;
       public          postgres    false    267    4279    267            �           2606    103337 '   systeme_aep fk9jw6nkqs4w4kjn22f5p6o02qo    FK CONSTRAINT     �   ALTER TABLE ONLY public.systeme_aep
    ADD CONSTRAINT fk9jw6nkqs4w4kjn22f5p6o02qo FOREIGN KEY (code_etat_ouvrage) REFERENCES public.etat_ouvrage(code);
 Q   ALTER TABLE ONLY public.systeme_aep DROP CONSTRAINT fk9jw6nkqs4w4kjn22f5p6o02qo;
       public          postgres    false    4207    300    225            �           2606    103217 )   puits_cimente fk9obyh8iftp9fygy6c3up6vta1    FK CONSTRAINT     �   ALTER TABLE ONLY public.puits_cimente
    ADD CONSTRAINT fk9obyh8iftp9fygy6c3up6vta1 FOREIGN KEY (code_type_exhaure) REFERENCES public.type_exhaure(code);
 S   ALTER TABLE ONLY public.puits_cimente DROP CONSTRAINT fk9obyh8iftp9fygy6c3up6vta1;
       public          postgres    false    277    316    4357            �           2606    103222 )   puits_cimente fk9qsfhr26db5d6cm71wslhnfpj    FK CONSTRAINT     �   ALTER TABLE ONLY public.puits_cimente
    ADD CONSTRAINT fk9qsfhr26db5d6cm71wslhnfpj FOREIGN KEY (code_type_puits) REFERENCES public.type_puits(code);
 S   ALTER TABLE ONLY public.puits_cimente DROP CONSTRAINT fk9qsfhr26db5d6cm71wslhnfpj;
       public          postgres    false    334    277    4393            s           2606    103062 -   equipement_forage fk9vwavlk9q7q0kykff5r1chvma    FK CONSTRAINT     �   ALTER TABLE ONLY public.equipement_forage
    ADD CONSTRAINT fk9vwavlk9q7q0kykff5r1chvma FOREIGN KEY (code_type_tube) REFERENCES public.type_tube(code);
 W   ALTER TABLE ONLY public.equipement_forage DROP CONSTRAINT fk9vwavlk9q7q0kykff5r1chvma;
       public          postgres    false    220    348    4421            }           2606    103112 #   forage fk_158175olvll8wns4uij8ay4qh    FK CONSTRAINT     �   ALTER TABLE ONLY public.forage
    ADD CONSTRAINT fk_158175olvll8wns4uij8ay4qh FOREIGN KEY (code_localite) REFERENCES public.localite(code);
 M   ALTER TABLE ONLY public.forage DROP CONSTRAINT fk_158175olvll8wns4uij8ay4qh;
       public          postgres    false    4241    230    247            �           2606    103237 *   puits_cimente fk_32ubs2euxciiwhc2aultcqasc    FK CONSTRAINT     �   ALTER TABLE ONLY public.puits_cimente
    ADD CONSTRAINT fk_32ubs2euxciiwhc2aultcqasc FOREIGN KEY (code_localite) REFERENCES public.localite(code);
 T   ALTER TABLE ONLY public.puits_cimente DROP CONSTRAINT fk_32ubs2euxciiwhc2aultcqasc;
       public          postgres    false    277    247    4241            �           2606    103227 *   puits_cimente fk_6hqi5pqraquih53ha8h0xnaid    FK CONSTRAINT     �   ALTER TABLE ONLY public.puits_cimente
    ADD CONSTRAINT fk_6hqi5pqraquih53ha8h0xnaid FOREIGN KEY (code_etat_ouvrage) REFERENCES public.etat_ouvrage(code);
 T   ALTER TABLE ONLY public.puits_cimente DROP CONSTRAINT fk_6hqi5pqraquih53ha8h0xnaid;
       public          postgres    false    277    225    4207            ~           2606    103117 #   forage fk_83x8gexgi1r3lnk606qrq5fk3    FK CONSTRAINT     �   ALTER TABLE ONLY public.forage
    ADD CONSTRAINT fk_83x8gexgi1r3lnk606qrq5fk3 FOREIGN KEY (code_projet) REFERENCES public.projet(code);
 M   ALTER TABLE ONLY public.forage DROP CONSTRAINT fk_83x8gexgi1r3lnk606qrq5fk3;
       public          postgres    false    274    230    4293            {           2606    103102 #   forage fk_gxot83qo0supm8ewk02d96afu    FK CONSTRAINT     �   ALTER TABLE ONLY public.forage
    ADD CONSTRAINT fk_gxot83qo0supm8ewk02d96afu FOREIGN KEY (code_etat_ouvrage) REFERENCES public.etat_ouvrage(code);
 M   ALTER TABLE ONLY public.forage DROP CONSTRAINT fk_gxot83qo0supm8ewk02d96afu;
       public          postgres    false    230    225    4207            �           2606    103247 *   puits_cimente fk_jsg70yx0so0adpyfcrtnuya8l    FK CONSTRAINT     �   ALTER TABLE ONLY public.puits_cimente
    ADD CONSTRAINT fk_jsg70yx0so0adpyfcrtnuya8l FOREIGN KEY (code_propriete) REFERENCES public.propriete(code);
 T   ALTER TABLE ONLY public.puits_cimente DROP CONSTRAINT fk_jsg70yx0so0adpyfcrtnuya8l;
       public          postgres    false    277    276    4297            �           2606    103232 *   puits_cimente fk_kxxkscdyilkf1q76os40oeovv    FK CONSTRAINT     �   ALTER TABLE ONLY public.puits_cimente
    ADD CONSTRAINT fk_kxxkscdyilkf1q76os40oeovv FOREIGN KEY (code_financement) REFERENCES public.financement(code);
 T   ALTER TABLE ONLY public.puits_cimente DROP CONSTRAINT fk_kxxkscdyilkf1q76os40oeovv;
       public          postgres    false    277    229    4213            �           2606    103242 *   puits_cimente fk_l5wretuawngnu48ap0gvsvo64    FK CONSTRAINT     �   ALTER TABLE ONLY public.puits_cimente
    ADD CONSTRAINT fk_l5wretuawngnu48ap0gvsvo64 FOREIGN KEY (code_projet) REFERENCES public.projet(code);
 T   ALTER TABLE ONLY public.puits_cimente DROP CONSTRAINT fk_l5wretuawngnu48ap0gvsvo64;
       public          postgres    false    277    274    4293                       2606    103122 #   forage fk_rv03b51743x69le6tqmlplcgn    FK CONSTRAINT     �   ALTER TABLE ONLY public.forage
    ADD CONSTRAINT fk_rv03b51743x69le6tqmlplcgn FOREIGN KEY (code_propriete) REFERENCES public.propriete(code);
 M   ALTER TABLE ONLY public.forage DROP CONSTRAINT fk_rv03b51743x69le6tqmlplcgn;
       public          postgres    false    230    276    4297            |           2606    103107 "   forage fk_v1be5q83f6iocxyftmk73dk2    FK CONSTRAINT     �   ALTER TABLE ONLY public.forage
    ADD CONSTRAINT fk_v1be5q83f6iocxyftmk73dk2 FOREIGN KEY (code_financement) REFERENCES public.financement(code);
 L   ALTER TABLE ONLY public.forage DROP CONSTRAINT fk_v1be5q83f6iocxyftmk73dk2;
       public          postgres    false    230    4213    229            �           2606    103347 '   systeme_aep fkagpji9jk9ju7780h5d353bvuk    FK CONSTRAINT     �   ALTER TABLE ONLY public.systeme_aep
    ADD CONSTRAINT fkagpji9jk9ju7780h5d353bvuk FOREIGN KEY (code_localite) REFERENCES public.localite(code);
 Q   ALTER TABLE ONLY public.systeme_aep DROP CONSTRAINT fkagpji9jk9ju7780h5d353bvuk;
       public          postgres    false    300    247    4241            o           2606    103042 -   donnee_indicateur fkajpq7egccsqq6lmww09wms6fg    FK CONSTRAINT     �   ALTER TABLE ONLY public.donnee_indicateur
    ADD CONSTRAINT fkajpq7egccsqq6lmww09wms6fg FOREIGN KEY (code_sous_groupe) REFERENCES public.sous_groupe(code);
 W   ALTER TABLE ONLY public.donnee_indicateur DROP CONSTRAINT fkajpq7egccsqq6lmww09wms6fg;
       public          postgres    false    293    4317    219            n           2606    103037 -   donnee_indicateur fkay5b5l201o52cc2pryhmm98qh    FK CONSTRAINT     �   ALTER TABLE ONLY public.donnee_indicateur
    ADD CONSTRAINT fkay5b5l201o52cc2pryhmm98qh FOREIGN KEY (code_source) REFERENCES public.source(code);
 W   ALTER TABLE ONLY public.donnee_indicateur DROP CONSTRAINT fkay5b5l201o52cc2pryhmm98qh;
       public          postgres    false    291    4313    219            ]           2606    102952 /   captage_systeme_aep fkb6pgyhq3otxdjoobu7u6xgml6    FK CONSTRAINT     �   ALTER TABLE ONLY public.captage_systeme_aep
    ADD CONSTRAINT fkb6pgyhq3otxdjoobu7u6xgml6 FOREIGN KEY (code_modele_pompe) REFERENCES public.modele_pompe(code);
 Y   ALTER TABLE ONLY public.captage_systeme_aep DROP CONSTRAINT fkb6pgyhq3otxdjoobu7u6xgml6;
       public          postgres    false    4259    257    211            �           2606    103182 $   localite fkbjl3n9dn56dyeh2vwo9ogd55a    FK CONSTRAINT     �   ALTER TABLE ONLY public.localite
    ADD CONSTRAINT fkbjl3n9dn56dyeh2vwo9ogd55a FOREIGN KEY (code_type_localite) REFERENCES public.type_localite(code);
 N   ALTER TABLE ONLY public.localite DROP CONSTRAINT fkbjl3n9dn56dyeh2vwo9ogd55a;
       public          postgres    false    4373    247    324            W           2606    102922 *   borne_fontaine fkc5vfav2wafwa1a0g9pyr8440g    FK CONSTRAINT     �   ALTER TABLE ONLY public.borne_fontaine
    ADD CONSTRAINT fkc5vfav2wafwa1a0g9pyr8440g FOREIGN KEY (code_systeme_aep) REFERENCES public.systeme_aep(code);
 T   ALTER TABLE ONLY public.borne_fontaine DROP CONSTRAINT fkc5vfav2wafwa1a0g9pyr8440g;
       public          postgres    false    4329    300    209            �           2606    103137 /   frame_section_frame fkcbjv8saxyqpcv2bhda4f4xhld    FK CONSTRAINT     �   ALTER TABLE ONLY public.frame_section_frame
    ADD CONSTRAINT fkcbjv8saxyqpcv2bhda4f4xhld FOREIGN KEY (section_frame_code) REFERENCES public.section_frame(code);
 Y   ALTER TABLE ONLY public.frame_section_frame DROP CONSTRAINT fkcbjv8saxyqpcv2bhda4f4xhld;
       public          postgres    false    285    4311    234            Z           2606    102937 /   captage_systeme_aep fkcf1hk0t26p2kgkslpu56i71rx    FK CONSTRAINT     �   ALTER TABLE ONLY public.captage_systeme_aep
    ADD CONSTRAINT fkcf1hk0t26p2kgkslpu56i71rx FOREIGN KEY (code_forage) REFERENCES public.forage(code);
 Y   ALTER TABLE ONLY public.captage_systeme_aep DROP CONSTRAINT fkcf1hk0t26p2kgkslpu56i71rx;
       public          postgres    false    211    230    4217            �           2606    103352 '   systeme_aep fkd16k19ki3jswltu6v76oki4te    FK CONSTRAINT     �   ALTER TABLE ONLY public.systeme_aep
    ADD CONSTRAINT fkd16k19ki3jswltu6v76oki4te FOREIGN KEY (code_projet) REFERENCES public.projet(code);
 Q   ALTER TABLE ONLY public.systeme_aep DROP CONSTRAINT fkd16k19ki3jswltu6v76oki4te;
       public          postgres    false    4293    300    274            �           2606    103312 2   sous_groupe_indicateur fkd8j8jx0gxv0ty9ny57kg7ciqs    FK CONSTRAINT     �   ALTER TABLE ONLY public.sous_groupe_indicateur
    ADD CONSTRAINT fkd8j8jx0gxv0ty9ny57kg7ciqs FOREIGN KEY (code_indicateur) REFERENCES public.indicateur(code);
 \   ALTER TABLE ONLY public.sous_groupe_indicateur DROP CONSTRAINT fkd8j8jx0gxv0ty9ny57kg7ciqs;
       public          postgres    false    4227    294    238            �           2606    103197 3   piece_regulation_reseau fkdbssr3ltif06nnk0rw4gdpafc    FK CONSTRAINT     �   ALTER TABLE ONLY public.piece_regulation_reseau
    ADD CONSTRAINT fkdbssr3ltif06nnk0rw4gdpafc FOREIGN KEY (type_piece_regulation_reseau) REFERENCES public.type_piece_regulation_reseau(code);
 ]   ALTER TABLE ONLY public.piece_regulation_reseau DROP CONSTRAINT fkdbssr3ltif06nnk0rw4gdpafc;
       public          postgres    false    4381    328    268            �           2606    103292 2   section_frame_periodes fkdcmk4lsjxvu47hq71a6oxfqmi    FK CONSTRAINT     �   ALTER TABLE ONLY public.section_frame_periodes
    ADD CONSTRAINT fkdcmk4lsjxvu47hq71a6oxfqmi FOREIGN KEY (section_frame_code) REFERENCES public.section_frame(code);
 \   ALTER TABLE ONLY public.section_frame_periodes DROP CONSTRAINT fkdcmk4lsjxvu47hq71a6oxfqmi;
       public          postgres    false    285    287    4311            �           2606    103142 /   frame_section_frame fkdugqe1k9141j5mmo302ha6iyj    FK CONSTRAINT     �   ALTER TABLE ONLY public.frame_section_frame
    ADD CONSTRAINT fkdugqe1k9141j5mmo302ha6iyj FOREIGN KEY (frame_code) REFERENCES public.frame(code);
 Y   ALTER TABLE ONLY public.frame_section_frame DROP CONSTRAINT fkdugqe1k9141j5mmo302ha6iyj;
       public          postgres    false    233    4221    234            c           2606    102982 '   chateau_eau fkf0q34fjnlho8uwd1wyq6ni8fu    FK CONSTRAINT     �   ALTER TABLE ONLY public.chateau_eau
    ADD CONSTRAINT fkf0q34fjnlho8uwd1wyq6ni8fu FOREIGN KEY (code_hydraulique) REFERENCES public.hydraulique(code);
 Q   ALTER TABLE ONLY public.chateau_eau DROP CONSTRAINT fkf0q34fjnlho8uwd1wyq6ni8fu;
       public          postgres    false    212    236    4223            k           2606    103022 2   description_geologique fkfak5kjylyjr1rf2fkxeiq2vv1    FK CONSTRAINT     �   ALTER TABLE ONLY public.description_geologique
    ADD CONSTRAINT fkfak5kjylyjr1rf2fkxeiq2vv1 FOREIGN KEY (code_nature_sol) REFERENCES public.nature_sol(code);
 \   ALTER TABLE ONLY public.description_geologique DROP CONSTRAINT fkfak5kjylyjr1rf2fkxeiq2vv1;
       public          postgres    false    216    259    4263            q           2606    103052 -   donnee_indicateur fkfed5os54ev0m28iyocdh9mpa1    FK CONSTRAINT     �   ALTER TABLE ONLY public.donnee_indicateur
    ADD CONSTRAINT fkfed5os54ev0m28iyocdh9mpa1 FOREIGN KEY (code_zone) REFERENCES public.zone(code);
 W   ALTER TABLE ONLY public.donnee_indicateur DROP CONSTRAINT fkfed5os54ev0m28iyocdh9mpa1;
       public          postgres    false    219    356    4435            �           2606    103192 3   piece_regulation_reseau fkfl0cark5yagvr3bmg5yp1eh02    FK CONSTRAINT     �   ALTER TABLE ONLY public.piece_regulation_reseau
    ADD CONSTRAINT fkfl0cark5yagvr3bmg5yp1eh02 FOREIGN KEY (code_systeme_aep) REFERENCES public.systeme_aep(code);
 ]   ALTER TABLE ONLY public.piece_regulation_reseau DROP CONSTRAINT fkfl0cark5yagvr3bmg5yp1eh02;
       public          postgres    false    300    268    4329            �           2606    103377 '   users_roles fkg6o2f1sydl18tpovrktfr3v75    FK CONSTRAINT     �   ALTER TABLE ONLY public.users_roles
    ADD CONSTRAINT fkg6o2f1sydl18tpovrktfr3v75 FOREIGN KEY (role_code) REFERENCES public.role(code);
 Q   ALTER TABLE ONLY public.users_roles DROP CONSTRAINT fkg6o2f1sydl18tpovrktfr3v75;
       public          postgres    false    4309    282    354            �           2606    103392     zone fkghddol7mqc58rbvm3knhth9il    FK CONSTRAINT     �   ALTER TABLE ONLY public.zone
    ADD CONSTRAINT fkghddol7mqc58rbvm3knhth9il FOREIGN KEY (parent_code) REFERENCES public.zone(code);
 J   ALTER TABLE ONLY public.zone DROP CONSTRAINT fkghddol7mqc58rbvm3knhth9il;
       public          postgres    false    356    4435    356            �           2606    103357 '   systeme_aep fkh45m951f7rsjpl7kac1micv0n    FK CONSTRAINT     �   ALTER TABLE ONLY public.systeme_aep
    ADD CONSTRAINT fkh45m951f7rsjpl7kac1micv0n FOREIGN KEY (code_propriete) REFERENCES public.propriete(code);
 Q   ALTER TABLE ONLY public.systeme_aep DROP CONSTRAINT fkh45m951f7rsjpl7kac1micv0n;
       public          postgres    false    300    276    4297            i           2606    103012 *   classification fkhn82yq5oqa714pmet0yx37ki7    FK CONSTRAINT     �   ALTER TABLE ONLY public.classification
    ADD CONSTRAINT fkhn82yq5oqa714pmet0yx37ki7 FOREIGN KEY (parent_code) REFERENCES public.classification(code);
 T   ALTER TABLE ONLY public.classification DROP CONSTRAINT fkhn82yq5oqa714pmet0yx37ki7;
       public          postgres    false    215    4191    215            �           2606    103267 .   reseau_refoulement fkhr5mtigkr4fec4ljp64letb7h    FK CONSTRAINT     �   ALTER TABLE ONLY public.reseau_refoulement
    ADD CONSTRAINT fkhr5mtigkr4fec4ljp64letb7h FOREIGN KEY (code_systeme_aep) REFERENCES public.systeme_aep(code);
 X   ALTER TABLE ONLY public.reseau_refoulement DROP CONSTRAINT fkhr5mtigkr4fec4ljp64letb7h;
       public          postgres    false    279    300    4329            Y           2606    102932 3   branchement_particulier fkhsqlp7ev8q852e49merkl9r67    FK CONSTRAINT     �   ALTER TABLE ONLY public.branchement_particulier
    ADD CONSTRAINT fkhsqlp7ev8q852e49merkl9r67 FOREIGN KEY (code_systeme_aep) REFERENCES public.systeme_aep(code);
 ]   ALTER TABLE ONLY public.branchement_particulier DROP CONSTRAINT fkhsqlp7ev8q852e49merkl9r67;
       public          postgres    false    210    300    4329            �           2606    103172 $   localite fkid5ebx98bl3tajp5a78oy2dhx    FK CONSTRAINT     �   ALTER TABLE ONLY public.localite
    ADD CONSTRAINT fkid5ebx98bl3tajp5a78oy2dhx FOREIGN KEY (code_commune) REFERENCES public.zone(code);
 N   ALTER TABLE ONLY public.localite DROP CONSTRAINT fkid5ebx98bl3tajp5a78oy2dhx;
       public          postgres    false    247    4435    356            V           2606    102917 %   abreuvoir fkj8tmcq8caakea63xlgoke4g7s    FK CONSTRAINT     �   ALTER TABLE ONLY public.abreuvoir
    ADD CONSTRAINT fkj8tmcq8caakea63xlgoke4g7s FOREIGN KEY (code_systeme_aep) REFERENCES public.systeme_aep(code);
 O   ALTER TABLE ONLY public.abreuvoir DROP CONSTRAINT fkj8tmcq8caakea63xlgoke4g7s;
       public          postgres    false    208    300    4329            �           2606    103362 '   systeme_aep fkj9lb6983i30wcq5xh04cowkwa    FK CONSTRAINT     �   ALTER TABLE ONLY public.systeme_aep
    ADD CONSTRAINT fkj9lb6983i30wcq5xh04cowkwa FOREIGN KEY (code_type_systeme_aep) REFERENCES public.type_systeme_aep(code);
 Q   ALTER TABLE ONLY public.systeme_aep DROP CONSTRAINT fkj9lb6983i30wcq5xh04cowkwa;
       public          postgres    false    300    342    4409            z           2606    103097 "   forage fkjujc4rpwp2g915yio65ctabga    FK CONSTRAINT     �   ALTER TABLE ONLY public.forage
    ADD CONSTRAINT fkjujc4rpwp2g915yio65ctabga FOREIGN KEY (code_type_usage) REFERENCES public.type_usage(code);
 L   ALTER TABLE ONLY public.forage DROP CONSTRAINT fkjujc4rpwp2g915yio65ctabga;
       public          postgres    false    350    4425    230            h           2606    103007 1   chronique_observation fkjxbokxdpgvt6xcu6q2gxsu5ud    FK CONSTRAINT     �   ALTER TABLE ONLY public.chronique_observation
    ADD CONSTRAINT fkjxbokxdpgvt6xcu6q2gxsu5ud FOREIGN KEY (code_parametre_observe) REFERENCES public.parametre_station_meteo(code);
 [   ALTER TABLE ONLY public.chronique_observation DROP CONSTRAINT fkjxbokxdpgvt6xcu6q2gxsu5ud;
       public          postgres    false    4275    213    265            p           2606    103047 -   donnee_indicateur fkk72cuebr7pucq6reec9n1cx8c    FK CONSTRAINT     �   ALTER TABLE ONLY public.donnee_indicateur
    ADD CONSTRAINT fkk72cuebr7pucq6reec9n1cx8c FOREIGN KEY (code_unite) REFERENCES public.unite(code);
 W   ALTER TABLE ONLY public.donnee_indicateur DROP CONSTRAINT fkk72cuebr7pucq6reec9n1cx8c;
       public          postgres    false    219    352    4431            �           2606    103372 ,   unite_indicateur fkl48iyq8jehbr6roy5t20vnc6a    FK CONSTRAINT     �   ALTER TABLE ONLY public.unite_indicateur
    ADD CONSTRAINT fkl48iyq8jehbr6roy5t20vnc6a FOREIGN KEY (code_unite) REFERENCES public.unite(code);
 V   ALTER TABLE ONLY public.unite_indicateur DROP CONSTRAINT fkl48iyq8jehbr6roy5t20vnc6a;
       public          postgres    false    353    352    4431            �           2606    103132 /   formation_sanitaire fkl801qb6x9raqxmid68h6bpdbb    FK CONSTRAINT     �   ALTER TABLE ONLY public.formation_sanitaire
    ADD CONSTRAINT fkl801qb6x9raqxmid68h6bpdbb FOREIGN KEY (code_type_formation_sanitaire) REFERENCES public.type_formation_sanitaire(code);
 Y   ALTER TABLE ONLY public.formation_sanitaire DROP CONSTRAINT fkl801qb6x9raqxmid68h6bpdbb;
       public          postgres    false    232    4361    318            m           2606    103032 -   donnee_indicateur fklburpn3g6p9lorb4kra9nsuc3    FK CONSTRAINT     �   ALTER TABLE ONLY public.donnee_indicateur
    ADD CONSTRAINT fklburpn3g6p9lorb4kra9nsuc3 FOREIGN KEY (code_periode) REFERENCES public.periode(code);
 W   ALTER TABLE ONLY public.donnee_indicateur DROP CONSTRAINT fklburpn3g6p9lorb4kra9nsuc3;
       public          postgres    false    267    4279    219            �           2606    103382 '   users_roles fkleyquasaq6u8cnywvjpiy02st    FK CONSTRAINT     �   ALTER TABLE ONLY public.users_roles
    ADD CONSTRAINT fkleyquasaq6u8cnywvjpiy02st FOREIGN KEY (user_code) REFERENCES public.t_user(code);
 Q   ALTER TABLE ONLY public.users_roles DROP CONSTRAINT fkleyquasaq6u8cnywvjpiy02st;
       public          postgres    false    302    354    4331            a           2606    102972 /   captage_systeme_aep fklgdoo6l5k2crryttkkgkc8qqa    FK CONSTRAINT     �   ALTER TABLE ONLY public.captage_systeme_aep
    ADD CONSTRAINT fklgdoo6l5k2crryttkkgkc8qqa FOREIGN KEY (code_type_systeme_protection) REFERENCES public.type_systeme_protection(code);
 Y   ALTER TABLE ONLY public.captage_systeme_aep DROP CONSTRAINT fklgdoo6l5k2crryttkkgkc8qqa;
       public          postgres    false    211    346    4417            �           2606    103157 '   intervenant fkm2pxw82yyx1rs1e3thhskt5sr    FK CONSTRAINT     �   ALTER TABLE ONLY public.intervenant
    ADD CONSTRAINT fkm2pxw82yyx1rs1e3thhskt5sr FOREIGN KEY (code_type_intervenant) REFERENCES public.type_intervenant(code);
 Q   ALTER TABLE ONLY public.intervenant DROP CONSTRAINT fkm2pxw82yyx1rs1e3thhskt5sr;
       public          postgres    false    320    241    4365            �           2606    103287 5   section_frame_indicateurs fkm9hc4i97l137vdaqjb0jopye5    FK CONSTRAINT     �   ALTER TABLE ONLY public.section_frame_indicateurs
    ADD CONSTRAINT fkm9hc4i97l137vdaqjb0jopye5 FOREIGN KEY (section_frame_code) REFERENCES public.section_frame(code);
 _   ALTER TABLE ONLY public.section_frame_indicateurs DROP CONSTRAINT fkm9hc4i97l137vdaqjb0jopye5;
       public          postgres    false    286    285    4311            �           2606    103167 '   lieu_public fko1qphjc3s6ndfdypmka9nraly    FK CONSTRAINT     �   ALTER TABLE ONLY public.lieu_public
    ADD CONSTRAINT fko1qphjc3s6ndfdypmka9nraly FOREIGN KEY (code_type_lieu_public) REFERENCES public.type_lieu_public(code);
 Q   ALTER TABLE ONLY public.lieu_public DROP CONSTRAINT fko1qphjc3s6ndfdypmka9nraly;
       public          postgres    false    322    4369    245            �           2606    103212 '   prestataire fko3xbm92q72c22wvs3ccyic6st    FK CONSTRAINT     �   ALTER TABLE ONLY public.prestataire
    ADD CONSTRAINT fko3xbm92q72c22wvs3ccyic6st FOREIGN KEY (code_type_prestation) REFERENCES public.type_prestation(code);
 Q   ALTER TABLE ONLY public.prestataire DROP CONSTRAINT fko3xbm92q72c22wvs3ccyic6st;
       public          postgres    false    332    271    4389            �           2606    103282 ,   roles_privileges fkobj1qqif7ct36m3l6k0rhd619    FK CONSTRAINT     �   ALTER TABLE ONLY public.roles_privileges
    ADD CONSTRAINT fkobj1qqif7ct36m3l6k0rhd619 FOREIGN KEY (role_code) REFERENCES public.role(code);
 V   ALTER TABLE ONLY public.roles_privileges DROP CONSTRAINT fkobj1qqif7ct36m3l6k0rhd619;
       public          postgres    false    283    282    4309            t           2606    103067 /   equipement_installe fkohonecgpf9h0d06hek024vu2m    FK CONSTRAINT     �   ALTER TABLE ONLY public.equipement_installe
    ADD CONSTRAINT fkohonecgpf9h0d06hek024vu2m FOREIGN KEY (code_station) REFERENCES public.station(code);
 Y   ALTER TABLE ONLY public.equipement_installe DROP CONSTRAINT fkohonecgpf9h0d06hek024vu2m;
       public          postgres    false    221    296    4323            �           2606    103327 #   station fkonvebkn6da3obxja3qs9p3igc    FK CONSTRAINT     �   ALTER TABLE ONLY public.station
    ADD CONSTRAINT fkonvebkn6da3obxja3qs9p3igc FOREIGN KEY (code_reseau_suivi) REFERENCES public.reseau_suivi(code);
 M   ALTER TABLE ONLY public.station DROP CONSTRAINT fkonvebkn6da3obxja3qs9p3igc;
       public          postgres    false    281    4307    296            �           2606    103317 2   sous_groupe_indicateur fkp51onsy687wbji985t0kpwojc    FK CONSTRAINT     �   ALTER TABLE ONLY public.sous_groupe_indicateur
    ADD CONSTRAINT fkp51onsy687wbji985t0kpwojc FOREIGN KEY (code_sous_groupe) REFERENCES public.sous_groupe(code);
 \   ALTER TABLE ONLY public.sous_groupe_indicateur DROP CONSTRAINT fkp51onsy687wbji985t0kpwojc;
       public          postgres    false    4317    293    294            �           2606    103162 '   lieu_public fkp7x5ybvge643tmcqh1e4fld8s    FK CONSTRAINT     �   ALTER TABLE ONLY public.lieu_public
    ADD CONSTRAINT fkp7x5ybvge643tmcqh1e4fld8s FOREIGN KEY (code_localite) REFERENCES public.localite(code);
 Q   ALTER TABLE ONLY public.lieu_public DROP CONSTRAINT fkp7x5ybvge643tmcqh1e4fld8s;
       public          postgres    false    247    245    4241            �           2606    103127 /   formation_sanitaire fkpfruw6a388h4xfqa03ey002nt    FK CONSTRAINT     �   ALTER TABLE ONLY public.formation_sanitaire
    ADD CONSTRAINT fkpfruw6a388h4xfqa03ey002nt FOREIGN KEY (code_localite) REFERENCES public.localite(code);
 Y   ALTER TABLE ONLY public.formation_sanitaire DROP CONSTRAINT fkpfruw6a388h4xfqa03ey002nt;
       public          postgres    false    247    232    4241            �           2606    103257 /   reseau_distribution fkpiu8066n9cpbnsiosof5jjy6o    FK CONSTRAINT     �   ALTER TABLE ONLY public.reseau_distribution
    ADD CONSTRAINT fkpiu8066n9cpbnsiosof5jjy6o FOREIGN KEY (code_systeme_aep) REFERENCES public.systeme_aep(code);
 Y   ALTER TABLE ONLY public.reseau_distribution DROP CONSTRAINT fkpiu8066n9cpbnsiosof5jjy6o;
       public          postgres    false    300    4329    278            �           2606    103272 .   reseau_refoulement fkpxvyopi9ss9ldcn16awvkgped    FK CONSTRAINT     �   ALTER TABLE ONLY public.reseau_refoulement
    ADD CONSTRAINT fkpxvyopi9ss9ldcn16awvkgped FOREIGN KEY (code_tuyauterie) REFERENCES public.tuyauterie(code);
 X   ALTER TABLE ONLY public.reseau_refoulement DROP CONSTRAINT fkpxvyopi9ss9ldcn16awvkgped;
       public          postgres    false    279    304    4333            �           2606    103322 #   station fkqac63qo284b8ohlynui6gj3qp    FK CONSTRAINT     �   ALTER TABLE ONLY public.station
    ADD CONSTRAINT fkqac63qo284b8ohlynui6gj3qp FOREIGN KEY (code_localite) REFERENCES public.localite(code);
 M   ALTER TABLE ONLY public.station DROP CONSTRAINT fkqac63qo284b8ohlynui6gj3qp;
       public          postgres    false    4241    296    247            �           2606    103147 5   indicateur_classification fkqgq12x4b2nbu5lx6vk7qtu0cd    FK CONSTRAINT     �   ALTER TABLE ONLY public.indicateur_classification
    ADD CONSTRAINT fkqgq12x4b2nbu5lx6vk7qtu0cd FOREIGN KEY (code_classification) REFERENCES public.classification(code);
 _   ALTER TABLE ONLY public.indicateur_classification DROP CONSTRAINT fkqgq12x4b2nbu5lx6vk7qtu0cd;
       public          postgres    false    239    215    4191            d           2606    102987 '   chateau_eau fkqoybnnlk5qv3dqfvdi19i9hmc    FK CONSTRAINT     �   ALTER TABLE ONLY public.chateau_eau
    ADD CONSTRAINT fkqoybnnlk5qv3dqfvdi19i9hmc FOREIGN KEY (code_materiaux) REFERENCES public.materiaux(code);
 Q   ALTER TABLE ONLY public.chateau_eau DROP CONSTRAINT fkqoybnnlk5qv3dqfvdi19i9hmc;
       public          postgres    false    212    251    4247            �           2606    103262 /   reseau_distribution fkqqr7vpkjfk5c3artjkvh3uhog    FK CONSTRAINT     �   ALTER TABLE ONLY public.reseau_distribution
    ADD CONSTRAINT fkqqr7vpkjfk5c3artjkvh3uhog FOREIGN KEY (code_tuyauterie) REFERENCES public.tuyauterie(code);
 Y   ALTER TABLE ONLY public.reseau_distribution DROP CONSTRAINT fkqqr7vpkjfk5c3artjkvh3uhog;
       public          postgres    false    4333    304    278            [           2606    102942 /   captage_systeme_aep fkre6dipw4rmuabbd7dh9173vgw    FK CONSTRAINT     �   ALTER TABLE ONLY public.captage_systeme_aep
    ADD CONSTRAINT fkre6dipw4rmuabbd7dh9173vgw FOREIGN KEY (code_marque_pompe) REFERENCES public.marque_pompe(code);
 Y   ALTER TABLE ONLY public.captage_systeme_aep DROP CONSTRAINT fkre6dipw4rmuabbd7dh9173vgw;
       public          postgres    false    211    4243    249            �           2606    103302 /   section_frame_zones fkrh04m2ffy5n7k512nh1nn9xom    FK CONSTRAINT     �   ALTER TABLE ONLY public.section_frame_zones
    ADD CONSTRAINT fkrh04m2ffy5n7k512nh1nn9xom FOREIGN KEY (section_frame_code) REFERENCES public.section_frame(code);
 Y   ALTER TABLE ONLY public.section_frame_zones DROP CONSTRAINT fkrh04m2ffy5n7k512nh1nn9xom;
       public          postgres    false    4311    285    289            �           2606    103367 +   unite_indicateur fkrkd75qgnh3xwh9ogs4o2ayxm    FK CONSTRAINT     �   ALTER TABLE ONLY public.unite_indicateur
    ADD CONSTRAINT fkrkd75qgnh3xwh9ogs4o2ayxm FOREIGN KEY (code_indicateur) REFERENCES public.indicateur(code);
 U   ALTER TABLE ONLY public.unite_indicateur DROP CONSTRAINT fkrkd75qgnh3xwh9ogs4o2ayxm;
       public          postgres    false    4227    238    353            f           2606    102997 '   chateau_eau fkrvf5glp1b8irpf2rwvseq3rgv    FK CONSTRAINT     �   ALTER TABLE ONLY public.chateau_eau
    ADD CONSTRAINT fkrvf5glp1b8irpf2rwvseq3rgv FOREIGN KEY (code_tuyauterie) REFERENCES public.tuyauterie(code);
 Q   ALTER TABLE ONLY public.chateau_eau DROP CONSTRAINT fkrvf5glp1b8irpf2rwvseq3rgv;
       public          postgres    false    304    212    4333            �           2606    103297 1   section_frame_sources fkskdfqb4rbqg7k2ke58s0c0mic    FK CONSTRAINT     �   ALTER TABLE ONLY public.section_frame_sources
    ADD CONSTRAINT fkskdfqb4rbqg7k2ke58s0c0mic FOREIGN KEY (section_frame_code) REFERENCES public.section_frame(code);
 [   ALTER TABLE ONLY public.section_frame_sources DROP CONSTRAINT fkskdfqb4rbqg7k2ke58s0c0mic;
       public          postgres    false    288    4311    285            �           2606    103307 '   sous_groupe fksm5h0s4w9e6iqgflkjy89jbkw    FK CONSTRAINT     �   ALTER TABLE ONLY public.sous_groupe
    ADD CONSTRAINT fksm5h0s4w9e6iqgflkjy89jbkw FOREIGN KEY (parent_code) REFERENCES public.sous_groupe(code);
 Q   ALTER TABLE ONLY public.sous_groupe DROP CONSTRAINT fksm5h0s4w9e6iqgflkjy89jbkw;
       public          postgres    false    293    4317    293            y           2606    103092 "   forage fksqcw76g9fvn3c02uvf3446u6e    FK CONSTRAINT     �   ALTER TABLE ONLY public.forage
    ADD CONSTRAINT fksqcw76g9fvn3c02uvf3446u6e FOREIGN KEY (code_type_amenagement) REFERENCES public.type_amenagement(code);
 L   ALTER TABLE ONLY public.forage DROP CONSTRAINT fksqcw76g9fvn3c02uvf3446u6e;
       public          postgres    false    4337    306    230            �           2606    103738 +   recensement_ins fkt288i4mt4v05j9a3xkc1fmfph    FK CONSTRAINT     �   ALTER TABLE ONLY public.recensement_ins
    ADD CONSTRAINT fkt288i4mt4v05j9a3xkc1fmfph FOREIGN KEY (code_localite) REFERENCES public.localite(code);
 U   ALTER TABLE ONLY public.recensement_ins DROP CONSTRAINT fkt288i4mt4v05j9a3xkc1fmfph;
       public          postgres    false    4241    247    359            `           2606    102967 /   captage_systeme_aep fkt41ygkoe3fqvjw73pg6f70vpv    FK CONSTRAINT     �   ALTER TABLE ONLY public.captage_systeme_aep
    ADD CONSTRAINT fkt41ygkoe3fqvjw73pg6f70vpv FOREIGN KEY (code_type_systeme_installation) REFERENCES public.type_systeme_installation(code);
 Y   ALTER TABLE ONLY public.captage_systeme_aep DROP CONSTRAINT fkt41ygkoe3fqvjw73pg6f70vpv;
       public          postgres    false    344    211    4413            x           2606    103087 *   fiche_technique fkt5qfmio0mo39m4b1dlmtt1ac    FK CONSTRAINT     �   ALTER TABLE ONLY public.fiche_technique
    ADD CONSTRAINT fkt5qfmio0mo39m4b1dlmtt1ac FOREIGN KEY (code_indicateur) REFERENCES public.indicateur(code);
 T   ALTER TABLE ONLY public.fiche_technique DROP CONSTRAINT fkt5qfmio0mo39m4b1dlmtt1ac;
       public          postgres    false    238    227    4227            �           2606    103332 #   station fkte3tptuyvdtbqaadtpt1c1ing    FK CONSTRAINT     �   ALTER TABLE ONLY public.station
    ADD CONSTRAINT fkte3tptuyvdtbqaadtpt1c1ing FOREIGN KEY (code_type_station) REFERENCES public.type_station_meteo(code);
 M   ALTER TABLE ONLY public.station DROP CONSTRAINT fkte3tptuyvdtbqaadtpt1c1ing;
       public          postgres    false    296    4405    340            �           2606    103567 #   localite fktltnh5wo2a06fcpgy8bsosux    FK CONSTRAINT     �   ALTER TABLE ONLY public.localite
    ADD CONSTRAINT fktltnh5wo2a06fcpgy8bsosux FOREIGN KEY (code_milieu) REFERENCES public.milieu(code);
 M   ALTER TABLE ONLY public.localite DROP CONSTRAINT fktltnh5wo2a06fcpgy8bsosux;
       public          postgres    false    4255    247    255            �           2606    103202 *   pompe_installe fktqlg9y4s81e2aq5dnbkqn9scs    FK CONSTRAINT     �   ALTER TABLE ONLY public.pompe_installe
    ADD CONSTRAINT fktqlg9y4s81e2aq5dnbkqn9scs FOREIGN KEY (code_marque_pompe) REFERENCES public.marque_pompe(code);
 T   ALTER TABLE ONLY public.pompe_installe DROP CONSTRAINT fktqlg9y4s81e2aq5dnbkqn9scs;
       public          postgres    false    270    4243    249            :      x������ � �      ;      x������ � �      <      x������ � �      =      x������ � �      >      x������ � �      ?      x������ � �      A      x������ � �      B      x������ � �      D      x������ � �      E      x������ � �      F      x������ � �      G      x������ � �      I      x������ � �      K   :   x�3���4(������
�2�T����6F����Spqu���*����� �v�      M      x������ � �      O      x������ � �      P      x���ے9�%���
�u��lU�<�3��&�d�v��|����/A��G�P��Zm]]�8��R���˯�����ܷ�=Eob�v߿����~s�/���g��vAY�-��v��3��;.����$���T1fPO,�v)�_d��Ja-���0�~O�U���J�}��������D�3�.�EbE$��x�*���H��}�M��R�Ͻ�]�# �Q|纰�����]e=}��{��.%� �h��o�罿݇�m����ex�����##sު	2�k�N��bD����Ƙoo��z��۰y��s߿�2*�T(>a�c5���3NF�u�^����OC�L�(�S�?����XB{,J�Z@�_�w�w_+7��m��C���Y���ݯ�����7���os@BL�+�J	�����g�*�e��m�����=�P%���},Y�Cа6~��SX��7H���xy]/��w�RPb�V�@Y��p9^�o�C�Ex�'��Er�Iٿ�X�0��c	qz~]���`]�謴���}>��~�]��������B��~��"�#s�x�w&@:��r:��#�X�~��x��)ـ5�sv������߮��F���cK�Z9
Н�_�e�s�-B;�z�h1,�k�l��}=�xK���K��X\�*��X1&wnO ��O��V�a'Ǟ(Sm�~��1b��Y��!m~�?�s��su돕�U��)���6�@Hp�lV�WZ�ˋ>X�|098C��+�'�|S �)B���7��֜f|����窃r��F\ƈAYkx1�V��~8���̛9ջgu������Z��sV:�+�?�Ɖu\��R<r\Vp�c�s�I��aԙ�_�!�bxY�Q�.K�,���,���`Y�8������X�>��e7�K^_f��b��N�O���1د?Ern,�Kz[��z�n�&r��r}�l�ym��'���/#�^�)K�7C~+\�ͮ?��#��o����rN<�Z��S���ო�䡫�+'�e��_��_}Np��͏�=��4���	��Rhr(�Y�/������d��8�T�/�ǘ�:��鍕���׽R{ֿ��%,�kpVN������7��r�<.��I�Lu��`���s��'��o�=s�	)@�]�W���;N, P?S۵ϸ�\J�Ncy��p�o����~�7�9���8qu�qbl~�s�<yηh4�^x��pX�
�W	���d=>�v�������U��u�-�ز�r��>Gs�>0�/���79X��"���[7L�z�������A3`7�Kj8뷓�C�Y���_�lWG���'Gj�ߋ�ݖ��Vձq}&��0={������ϔ���2���J@�H�~."|L�ţU>V���_��O�N�����-�"�Ѐ��߶��u��ץ΀��2��/o��Xf�ʿ�Fe����O�����\�E�b*ઔ ��j�r����{ �ȿ�PFFy�d7���39�]��4A'W:x�_�͝o��j�VO�{|�4ܼ\e���be���)u��TC�;��	U����D�7���������۹���.Vp����\h�3����ʔ5]�G��X�G�˘�S�x����o�G�8c����s����bqT����v��oU����SBS�%l��P��$ ��n D�Ú�*aq.?$����v�+���^&K!��X[��-�pR�!��)q~���NΕ�v[�Jn�E7�b�G���e��6�AE���h�t��rd��1�\�W�:���u��TK��D�D����(8�5[�4T��%�d�!P9�X���`� ����O��t�OX����)/�&�#g�c�C�F~SO�:U!��S�� "����1?�5�q�z�!9�Lh}�+����L5 $gɉ&y4���o/}�B/����\�	Z�%��x�|�����|'9���x@r8&@�2���ڏ���T��x]
F.g�[�xܷ{��Ӳ�2�Pj}n��8����$e�Vw��g)1��n�dQf��p�7��ˏ��6�&GI�y��� ƍ�f�χ)����������/��E4e�k�F��<��#���[N?��OM_�=����f��0ꈿ9��m�UC���_}��0c2�Vfze��S��;*.�:�/�*����8	F�Nù���֢4i�PJ|N���뱿�C��s��V�ܪ���nj���>0��?���:	�Ř�YSL���T�e��������M��b�]��u�b�f�g�FڈE�K��ϝ��QIx*-�#�3���bnq�)��:�I5Dy�fm9RL
SN+�����'=GC���J�/\���1`���!gϟ{.��:QlJN�U|4�S��{췴D�K�����f���x(�=����wZ�H�h؈�,�>��v�7�����J�,$�l)���8��*����z��4�`u�ı��ZZ�r������[�Z�n�{�ǀM7Ndc]��CC��>u��`}d�������#S GH��'(_�G.F�/I�z��ԁ'1�k�Cv��,������Sl��r:�/ 6l�O�^5���b����-H�F��b89��X�Le���0���e<�Z�x���H+/�9>���ѶA�P�4@c��0���d��D5�$Z���N�A{~b�:D��)�8�>_���B��9+��'��� ������+�d6��'�7�Y�31��!�Auh�h �v�t��s�8}&<e�V5ha(�8�Bu��
�1z��Yd�0q]]xm��Y��������G���L���C�-�̤���LbM�!��7��B��Z���DA�d3�=���sU�6�+�'8���r׃hq~l�޺~�F�DA�F^ ��p��|�|�Rup^/i�d~��
]�~�砣�ʆj0��dn�q���������Դ��7N'��c�C�ׅ���#4qY`-#���,2E�8�V9zv�.���N]����c���u�4S�p����gΙ7���ˡg�%���3�#�s���</ � (��1�t�T?���C�bh=EɼA�IO�R�<�m}�Ů��?S�0�WdQ2q0_��i�@L��������֧�2{p~���15X+1r[n"H���?���n��q03�)]�u'4�C����H�tf���dI�q�<fg9�?m�R��ʃ�H���e8e�1�[M�X Z�O �oޮ?�Bh�\����֌�P����q�e-�',#q��)< 	KW7�Me�.���Y&Z͕F������jջa7l�r�Ϝ���Ud-��,F'Ft���g����.'�R� t',�b,b�K�1�7�R��X��M,�O��?]��Y1V�\%2ǐ���!��"(utn0�"S�p�(�o{�hrgEw�k196+��Y8�&��E1x}�&Oq2"��%��WƆf��Ig�X�^kKɴB�T,m�7KxF^�3U�nQ������%K�K&�����S��ȍW�~r^����ن9ӥ��⌟�z@Bv^��e���`�O�άT=��b���	��)R����n�Rpע"s�$I��6�;:s�ǵ��T�́�㮻ۘ��:|7��9��_�2x�Y9��&���u���G�o�D���L9����_�(����'�:^7H�e���*}	��WI�Z��]B����9_�����\�O�Y��9���Ux+p|��#/�Ṷ�L�[�2�p<e]b����Г����p�^�V�����Q��.��vu`��Š�=f�4b������q?�U��$�k��R��6ߨ�]$=�tD$�QL�P	I���v19��Ɩ$Ry��?��:���Ũ�6��(�M2�Υ
�07���q\@�v�=]]ɤD��������U��E_&#b��� %��������������������2QDS�9�@�?e�;_9Z��G@[:��*��k�#2��i\vm���"ʹ���0:ohxG�dE%W� �za�qW�X0L���94,�E�.�"τ    �D�mt��@���|*����4E���;�IXW����[K�E���������
�D��1D&)�k�k4�F���(���d�7j����Ჟ��`Jd��bdr�v��<����9[bi���e��V�Js>�\�;��i��h}SX�$Ee�B�=���}ȧ>�*j��}Ù��ݑ� �r�� �d�� �A6Ê1t�%��ĵ�Pg��kwZf(����������+T6�\��-��x�%E�`9}�O���b]bX���9v"o�����r�n�G�rq���O�0ݗ�)�m�gFۃ���W�h~r�Tp��b83uh8��{���;�?���}E�˿ևm��ht��w��NøDu�C�	Z�d�ȱl�ʊG�%������K43ȗ0�ρ�Y�v#�"���W-�A� E���FO����Ɠ���Vw��R�2p�{�9ZE��*Z�)C�u�c�?���u���3�������ѵ�V|���R�oV����X�Zg���wr�v I���	P����L]���D
.����YQ�v��>�`8�$�x��w��2q�j6��&�-���g�꠽�e�e��e���[,�����KMf�X��k5��Ѻ4����s���0�Ʈ}8s51LOQOue�Ҵޖ\�4F�T�58@��_0�X1(�i���4�ö�� X�������HU��[(�h�¸���fX�T۷O��mxj���l+-�56����j��K1�Y��E3��w�=������SZf.�E-�����4��:_,�j���0Z�-�ǒ��V@��A��DAa�w1�-�l��6��ض<5Op=F����Q%D������1!~-�$W>~#�`�c�@�Oϸ
`*	�&`d=LE�mFV�{̕c�S��zc'F�E�f}�#��A h�����@˴E�����.�Ja2�'-P�_#��,�4����ԈД�N#r���;~�PZz��k�żo�E!35�%�����vY�,�LS��:��GC�g�^=�M�q7-S�����n?\���B������=29�8$0��^�#�;��S�ӹ~�L���1 �lU�ت�m ���j|q�C?���Lm��Wd��RT=PKP�� ���z�3-�2�����h��.i�.F3c�%����j�F62|8'�g�	J��������F7�c)��"�S��Ma���l�شL=|�*Pey~&���ژs�D��	��4z$�Z�
�n�1�_Q��~�\��
�-�M2ېJ(�b��CA5�Ǟ�]_�9%������\��C���:��Ɛ�Xt6��r�܁N��'6xM�q�s�2��TB��z9ڷ����¾]ϐQ�/P�kԧ$_�+��k8�����bj��ݘ�O�w3�ڧ��Ǚ�Y�]7n`�d�@���u��Ë��!YBS�@33��':@R�`��Jr��;N`S����.�zƷPBSU�����Y�nɱf�	;������2�P�蒎��7zꩮ��^��@������s�K�Kȏa6B�T�f�&$Z�" l��������k����1��m���K��i�&��[̋I,B�<Z=��7�� �A�;�v�Gs4��!̀<|U���YHo��L��4�?��xI�A�=�`)�Qρ�� BxL���%.s�������.��OZ�v�u2Ofi$ڠ����[�)=\O���&��
} 8_녤��t��q!t��Y�t���ڴld �Xf
d����ve�tt�8��9��R�:vhUu�ϯ��y 2%�6#Z�zG.!%8Z�V	2Pv �� 5x����S�"Z���_�JDx\�G��X�G&�/Q�^�d�d��cj~�,v{��+c��h K��,3��F�t�x}��{���������yN'^�X$�Óc5��V��&Er�RJ6�癱6�Tc��aC��pE�Շ�I�)2Fdu�K��	ls��2"U��#�j�M�@ڥ9<�0>n������	-�Ő�=�HU�n�H3�r��,R�qr�A��L�5�n�X!9\�F$v,��C�I�j�t��@��3od:����|!��&K!(�
�z�#�	e8��j+8�G�Za�<��{dDEv�����B��G\ ���x����7��>�E�a,)��x��WZ/Ind栀E�,2��ڃ{j���
�1ՠ�nd����82��I|G��)�������42[в�F�����2"�D�@�%7ҥ�d���n	�S5#bGS j@��z#��A>^|k]�O��ʰ��4�Ȕ��m�*D���%���l�0�9n"U�g�	D��xf$HE<�t��2�j��FF���d^O�U�������a&��Y�����A��䰙!��Q�8�C����J/U���s,f�0hI�,��
�:� �L��p���c�ȃ
����p�'��c1K�̰	�̶��B��G�^u漾gd��#J���Gej��nG5��53�@N�pdVw$�����q��F׹�0D�̌t=��V���p����:�:��1�94�+Y���t}����C�c��I#i����w�"H��.CO�|#s-�!��+DG�yo�i��6�e���`��~����LM�{��)��(��]�t�B��������5�?��G��fy�Ñ����!?����90�x��n������ɀ��"h�`~���1u����ș3��?��K���}}X��dF��Ѐ��QY.c�RBɝz1�e%����KU�l��kd����-��5f��G#��A7��<�Q��M�ɱ�T�bhr��C����S�&�g�/�[,_ʖ��D� i��Ƿ����V��?W�|Y&�%��d����W�<��}^��(s�����7�����wZF�N';����ϳ��V^r~_�F�<�����_~B�1b�>�`��g�1��@�}Ҁu(�8g:]��:�ПZ�E�J_��;-���=w%c�Q(T���?6�*>�!��i109UN�K��⺣|�}i%��&d:�o���>��T@��Ց��p��3��p�c�}钮�D2%�;n�9�(��N�g�oPX0�J��B���0���C⪘�t>}�8�:���32%��Š�9���^�-��O�	����e���c�.�O�\�m725p�o��.A�����ƣ��݃T`jn��W�jPᕹ�Aq�Ka�	NՉ8���3��s�S/F蠉șO���0AҠ� s��a�������ɶ����+�O6dR`N5�9�haw��S���)�u�Ѡ�+]P�35G�<��#�/�#���PY��~@��u�B��A�P&��H��B���.ԯ���0Ffv���H��6'dw~t��$��pĐ�1s~JO��杘1�+��q���%d:������P���|G���t�U&��{����;L8���ӱP'��+��������P:^�7nu�:�7210<D��:$��#�e\�*5�62M0l"��H>(��ꗏ���u�o��� B=
ٱ�!��Q2e���
�H����b!ԟe֠F�0b��l؞�;<:�'�>�
��E##���L�������X��4^�L!�w����n&_���1Z6!t�߂���6c�6e/�i2�c\~��/���IӉKSa��=��u�n��eB����c?��4Va�Hm���a�P�a/��D�V����1�r0���<N�7�?/t;�* �����,q��.���z�:B��oR��
�Ő�,�А$��X��c~|�QS�����32o0 U'~W�<��pCK�SFd�5x���Aq}	�v��s�L�\CR�0���`���zQ#��/U�6_J�Q��By6���<4�b��A3WfFn{�T��v8C�19���LL����]�~���@��*@���L	�^y�r0�����'�K��������h��p��L\dz���A�#����2�[-���iX,�|���9��T�%������Νp[��1\S�W^�:mP_�Y�ɍw'6KK4uN����1    �k�8�nUWW.�*d6`�Í�n��S�
�o+s��1)��,�R���V&��K-��y���vR������uR'T���z/�$w�5��8��K���f2����2+�^��7��Z0y+U����Y���ܯ˻J�/�Z�%hGb2M��rN��P3� Ռ*����\o��q����]�%[}�[�%�V��S�d��LK�ˁ��ɳt���tL����Ls�M
rJ���A]����Ic�Gdm����exX�i��	gh{��TY�"�X�B���(q�CA��2��Pd��C�a�DIN�
�f/�WF�2?�Y"�*t�dݡ3=��.Uf[0�Lt�zL,/���̣#� �'k�Gfq� r�^{bv���S#�紬Lt��gz{��n�������4X�29pTQ�ۓ�QV:���D?��V��m��g�p���
� ���φZ�h;+vE�����eYZ��Z���&�G`롿���#yQ柦(k�����%q-PKQ��!��������c�(VY�����7��T�6��O,�4�m[�h93�p�+t�����>l�:�UR�FTŐ�ԭ�H�@�2��p��D��;�Q1� hDg����?�dt`p�t�����v�]���L������x��۞u��p?����� ����9cG�W�~<�`{��t|8���&h��r����
��qL+Q-@�h]T���0?�P�G�8eW0�����M|��]��'Dӻ#4�"�O�_Gre^��DK�yt�������{@���s�z� �j����#�V�	���8Ę�3Q2t$�p������.5HT%���VWFdP{��;�P�iQF��� ���4��0�\����M��0+�Qc[��b�������)o,�@��JA@C�
1��X�`����0��ΫID��Y�#�I���}�3�T*�p�7�S�"�>'���2lN=��_�Y/�`e�����ޮ����V���4������g�o>QoZ�v-*�3|��m9��v0�Q F���j�`�DA�����{�4�{D9��L�r�j�حܼ��4:�A�p��EZ�meΠsTwD��DB�yLS�.��Xc]���dA�(Mu��C�i�G{Q5Ѵ�2g0x�z�����8g��ɀR��N�����&@ʁ=o�����W���-Jd2e0�B4�a1OR�m ,e@����P+S���#M�G/�uJ4��0��Ԩp5�2a�b#�%/L��gR��l�W�����p}���tt)SMK�~$Vf	zC�KE抷-��rT�u���LL�7l �Ηf%���э��2O���ó�[?{>�z�����Vf	ZTs��1t�۝i��W��w�L�@��y(
]�����=���:m�d��P�d6��o
�d�y���O����;�3����.�LL]/+1�w鴋�f��Qez��D �ώ�O����P����=�x����V���Z�`x7ut�#����Te,v�����(����k,���G�2ME^�� J���˛}}Z-���n��h�U4��)S�F�f�C-�����K��0m�m�Ub���'o�h;��_n����t�94S����2�0&���]w�R��[����Z-H,B�p��yy���^��[�Kl��oe�`rL�2�C�R��q����YEL����&Ө�+S-� uPó�,�Y~,�)Ȃ��Ҡ�+��yH�)_����
�0V��,����OM�ƅ1��Č� {Rڎ���:�����mO�B�T�@��8����P��
09Zm���m�X+'P���z�;CTԘ3��"������'"�����	]iѭ[$g2{p�g��m8�	Հށ]M��ߏ�H�X�S5h|ȼBaX��M��dV�v�A5����Ab�@�+lT�
U�,�$�Q :g���`K���2�P{��4�D2���A��
Q���_h��t���>E����T�u»t1.�,2:��8����מ6Q9~��9�ŀ����WoM1��h�uƜ0�3�#�'���F�Z��J��iP���'dUKCbWo��Y��x��ǁ|�]=S���Z��`�iZ��P�0����:��^&�,Lδ2���ל�2�0����u���>��TX$$2��k,�n�c.�{$$0�W�j�c�,C�:��L��$s�/�+!�]��kH� P�{�	m�J�3�Zh�AX�>����4:o��衅��"�<��^^��K}��k�w�AiL�����������[�W2�0߯�a!��C��/�����[�9s:��فB,��35�2��ᄕ���{]�g����L::�,'���^�L8|��$�F��Hj����0N�ZG�B6Mc඿�����X��f��[�y���=����	��g��Ƞq�}��v2��'�;��!?QU�ש���j���ɔCR퇁<j(����PCFU54��us���E�1n O��;>tU�܀��d�!(�� !`���1<��Bb1��!�D��:zt@��?�*~-L���P�P��V�v���R5Wc�������u��� ���`Vp�'`���zZ��1%4<Ql����rQ�BW��	��ن�u��i�as��U�B�����X�(k��}�p�=���^;AMSU�PB�{1�;z�;3��n��z�K��]h.>�
3�Ap��/�SFBՠ��d��O��1���kU������<C�&{@h����c�A۹�:���n�j����젹���E������X�f��A\:0.��ׁ� M(�4(@9�e82���������}-[?��KK9�kƇ��$'~����᪣�����xfZ����$X��S
]m�� 3�-��� -c���z6���L$��q������\�k+N6��e����X��(���F�l<h���:쮛����o^s0�����%�a?y�j�)�,���=��qh�F��r�7O���ن]B����;l� �h�TC�6��h:�j���p��pzP��	�+ ��nƎ�߄ڰ���i��	��G8v+5�?#�fl���7��p�%a^<|*��^zl83�T�ip�Ix�݇�mWz��]?%�f�;��R0x�:�r�Ck�JMX��Y��Z��8��	�[��������N&F��;Jǁ��?�_�yA8��
��[�Nf�rf�]�����;�; � I��:$�``�?A���L	����������C�����1?��L!A��M��!.��f� ���>6�~���������s� �i�v\�d��1�N9C�p���@��� áj H&u��!H^<�M<��L�D�g}1Y��w{ 4��}�T��!-�0� *���2�Dh�VȘ�8˛�2g�@%o�E�I,B�2)	'�q� �}:1�r�?�([H$B�|���P�ĵ>�8�#H�� 5(pK,B�FP�H�S.���pYEv]����!V$�9R+l���fܟ�HW�֏�:�Ch�F�T��4�V���i�$�����7?��g�q����:6Mڜ�#
:�m�Ɓ�{��(��"�>3����p��Sq0o�!�w��َ� �T��o'1	���t���V	���i��m�����	�*ꠢ��f��ś�D����8W�oÑ��8�7�J��r�\x��Et�n���'u�7O�O���'�ۧ�͜4�/���(�l��A�����zG'�
��'�<�w��?�:P��Ou��² �ؿ�ц�4\��4�~�0	=eC:'��{l�p��L���:e'� `�����|:?�֧2�0��)|0|3�/����ǫ��-�ĝ���x(��չqzo��bX���.R��Ws������`N���A��>�О�Hzκ���"��̨qP)��+��0�ˡ�&�0�rP���rB
a�(����aJ1G���DYvL���zM�,? ��5�߯��w9��O� � -.�@	 �����ǯ��-
20$R{Dn(^�q`���嬽n�A�    �gĮ���,����vL?�-���ID@��s\�U�����m"8Uū��Dtd��w4��>9[��E���:$ ��� ���מ&���fz������@f����W +b�K3����m�H�D�����H
 �ٟG&�����n1�#3��m�0��b���3�A7�:2�/2#�F��F��[�!8����b����8)Mp�9{gG�6s�N����e�.����~��/M)��Z��d>_·[��
3�lHt��P�#whA�s2�/*���
G�����l� !�6�u����S�aR0o�dy�b�>T�w��O��=�q����>1�TӃ��ld"�e+�.�|�m7��9�գ&r��7�Q6(7	�a?�ӗ����|F�I��1yϹb��?�2�oQM�j�Hf��s3�c�o{��q	�uej��}��ˋ����K�&e�[QMD��賎���)CDcb�T5�5t�a�)��A��
���C�&�,��y}(����(���`0�zX��,��,�Hk50���p�-D�jZh��^&�E�j���h�߹b�����N&��J�Pd:���1Q;��4��:\��y�ЗX[!(n7��퍫S>s@>o="1.k͌�J������+�q}Vxy��g�+MZj�ȁD36b\5K�;aRk1�f����|�k�pDSZ�骟!�Ih�@3C�C��t���5��mU�6�/��r&Ϥo��#�gP�#<�\�op;x����溑���Be���/��@q����J����P~�:�X@iqoz������q0�@^"��2���3�=�V�#s���Ι���e�e�^N�h�"!4���kK+TL��&^�{�3��?�WP���������n&�㑣���3��Ṯ?�r�A�j���g���{�e-c"�G�џi�#:�6 %�*r�H�{Xȹ�i���1��+<3��B�W��4���Ƽ���e�!��@b��g���>&�f����@��-W�;j�=�����:5�[�I6�HՃ`��9��#:m8�^f����$^��9۞�gH|��"��{�4�3�7�E��/�2�j`��i��-ca+/���1I���>V���5���\���@ψ� ��o�y3vp�����M��L���
�N��E�l]����¸���zv��	|qt� ����v���tU��,#W�5��+�x���#c��`�����^&�9����P�媓��-b���sxy%�$�Q�ɹ��F���րP�#�O^��W�M��L_?J�e��v4���R����i�����/y��'�HR�(�T
M��0����nfe��cn����z%/�»��I_�pe/Y�*>~�:/����� �G�ϟ�.�{Tu�j1��M�[K�7z���WZ_r�B�Y��Γ�g��}�٣��MA��-� ��h.S����GK�]�W�/{ ͼь�1�=��L���4���z�/��2��g�՗]FD��8<�O���SC���Q��T�i�l"W� -L޼lh#�ٹ���o7^�/zhI��c���n��7�(8��>Kׇ�Y�@�L���Ӝ�D�	R��^��~���B� ��m���G��Ѧ�5^v
�;�~��j�]Oٱ-
2Ǐl"��At�-t�餅�q���A��;��m8U��'
ݕUh����5�8y7u�5\���0����mG�A��W�6ǣ�Muۼ��#�����z7�4�w[ϭ�_�Q���ӴJ�t�c�AhP���~��8?�p�L�ꉶ���^��Ѱ9�N���g!]a���Ky{�A�y*�E��x b���Ӎ�`�fh%t��R�dlI.H�	�w��f8��d�g����]�(��K8��	���C�,���幇�A�+�zc�/C��uQ��JչX�I��CTI��S`��Xk��2��*y����N����+���7������߄o�����:/�'�=Wd!*<���ʱ<MM�K6X09���SH���D �P�|7����fj���h����@��/�s��_/S��@���~�������?z���$����mZ��C�9�x�rĨ ��`�5�����?�|ݥ2��j����I�3��"��4V��|Ӱ�~I��">'mʫ�ҟ�#��WF�^	?�b,3F�@F�p���4�v�+��-���2�A�I���Xb�e��9B�F��3Ʋ�	~5�w_
n��L���ܔ��`�p��\����X�2���U��
)˾�C��,�KAϊ螬�^��cvϣU6C��L.���O�2�ӓ��Ğ�����@3>���]�Ŧ���z!34b����s�ȧJՓL�42��sd�f|�O�Z�v�o��_K&*�{��0Ȅ��"��o�L`� �W�l1&9���T_`-0�RΉ�D
=��pa0u1�A�QfjE4�_�;���c�r/� *�� �;��,�֎�p����ZUU�k�D7�W�#�|~��;�8܆A�/1�"�ʌ�+^��ľ��q'�S(�O��r9&�6�Yӎ1u�z�a��i�����r�4y�sy��A�l���T�,����L+��$�15��	�D&M�nH����%Q�"��o^��G�4�L�+<�1��5���dB�(��)G��kpxS1�T���������K����^'�T<��uʜ�$4,&c������f�v��ۺ�+d)��ȣ�ï���<=�i�Ф�dJ!�����C��  P<���ЎZ�(2��X���*���o[��[��a {82�>�ŗ����{�y��ER;�@"F�@a��'���0�漭Ys�_�A�:͹I���	M5����)Ȏ�&*���:�GY�bub�gu1%Ȝ�G�,�V�k<��Tv-ȬB����8�s�3���&�Y��,�$g�4b��3�����s��2���d�ad-�+�+L�`��f�j��:#���C7�+����5�D� C�������]��Nh7.F#O�)�&�������=�:�2���w_��?�E����0��M~��}�A�5�a����e-3��������Ԁ����r�[�aQ�Y9����������*~��$� �
��p9P�{�@�y{�/{����c�ė�G7��3CM#����9��m�I�y}?)���=���yKov��Ch*��^_�2��[�������zV�E$��ە��#q^������o_��Z��6$�>$g�Hp]��P1�����vN���Ar��c/��֠^d��ȷN	��m,���!��1>ȔB�H?�#1;::�9�	���P����qn:aG�0���V(XL'#�1�֤��ۦGW���9�5%j��_���ZycH�{�r@���������&h�(�^/~d�@�m�:�JE>�4�׫L:�ܠ)�a���b%!�֏��s���r��&o���V�b���Lo}&-3q�����~S�ȸ�0��d�aD�M���#H��d'T����T��tٜX���P���d�Ł��#$�o�+��6������Ը�'f21��;����bѣ�#*��yK�O�d&���鞿�a�:���w�NAf!z��(� _ʧ�o�T�0�7'��?L�$N��GW�-�}z�s}��j��G����ÏA����~y�)�P��AQA�7[eNp��Dp���GF~�m���֓�B����o�@rwI������ڽ2S�Y�`2��[��{�( dO��P�i���� ��HmT~��\5���w|,�<4jG��7�l�s�C�3A���	=��X�:G�{���D��)[MZ	���h�gG�3���8�9�IWei#6���� ���*�����kS���,� ��f�\�[�g�z�=3���8V#�i���`�L|��{��V�h�eV�anz"�Ǐ^,+�u�	�i��u�)�.��Sd���&tժT]�*�,�z�;1���Z�H�ZI��W���!mF�u��5U��P��4f��?�ٌ����;��<4�|BAЗ    ��M}�l�=q+L��=���T��A���KuXDy���a�C�L~2��qH�ꀩj�g������g��7�Ʉ`;V�1L)Co,"����VG&���p�@�2����j-�'0��_��2Vv3P��U�c,������q:��i2V%]���g����c�ӛ�[l���3�q�$���E7��2��u�/�X�W;�P�#(ӽƆ�5��4q�{��T��%4��!�Y��K$AW�<3����x=���r,��\�� {*~_h`���3Al�<2A�t0`~�+���p����CO��Uj��l����smL'��'�3�Z���������W��ҿ2��>T����bbh�(p*a��h�p��&3��zjC�9�*����N�f�k�4rL�F���B�[п�'��X�ζ�(H\A�7����L��
�M�H�Q�z2�5-"t�m���3�����~���Sx4?�t����d�f����<=��1�?`��8�N�~���qhv�y�ұh��&��a�4����;ή�W١��i��+#�0�!��hqD����M��lN��e(� ����LѴz���4������k~qT��-�����7�P'��#���PF���A�+T�Ⱁ��`�@\��ɴ��<���L�z#���5ܟC��ӈ��Ӻ�&�f�*�y�6���y�G��a����A�F�K��P���c�[8��=�ثǩ�T'��t��3�N��V�~�q��t�e��-fd2�u����v�o��?m�h�t86�=I�
)5ԑ �ȝ`@h���Mr:1�2b�E��B�:��ˑ��b,#u�'�נ��I�X�VL�~�*V��3C��i����{�`����5X��5�jH��>�� � ��I�#�:Z7(:�LC%e�bRS<u�^/�f���������"(���EO�˸�p./�:�� D��H��R����Kk����@e*��C5 �yr�w�!���	�TA�C��B�ٷ��cΪ/��؟�����턩��H�ح�JoGgh\�^�M����j��Ia%���C+SD���ϥ��C�����Sĸ�\z2�����6��K]y�Z��S�4%v�q���5��}O`���i�ލ2I���T�H�������5��F���;�ٲ��l[����2Wja�eZ"m�C���5��~��t���@��?�0i�w�s��&��ԕɓoЛ�2/���;�QL��2or�0�LM4��d0d�sC��O^�22�w�"���0�S�!����k�3��{���x�A~��L��p��πE������C[v�/{�I�ѹE�8J$E��Qr��k����3�yUg�@�9�և��#7�^*.��}�g
������8����SN����iRe����Q�'"��ûd3�&�����Z
E�&Co�H�S�X�:F7X��h=�Q��|��N�W`�׏��N4�b��>��~��N�Ѯf�#�Y/��B"�c|�����[I�az�g����;^���s����Jdj��U����L�S��uG-�������m�i�:��1��[�-qX�u�n��]AY7b�_����v�2D�D�(�u�L�^c���"�d�ή��_���
��}�=��	Y02[_x�3��D2(��m�ۡe0el�-2+�i��h�u�hİ����t�u��8g�ȇ*� :Q�7��Ltm�2H�&MΣ'�r[�v��du/��9�I�*��y~���v�פ*t4(�VbT�uQ`2����Ta��@l�)�8��|Q�w;�L��\2�i���:<o��{09�� X���(����ꆲH�xN����oz�&b���%��F>�N�T�Vj�(Q�����J�g��9��ʮ��I���4��U|z����Wn-&Sg��m���!^W�8L�����9#�TaZo�e*�U�D��-`���u������� 3sx��x7���Tp�+M�Nz֏wG��h�/
C���>__,>��T��_v��IDW����A'�C<��l=����e^�e���>&����VW�ܨ/�P�ͳ�	�_:cv�� ��A�@v>t� ����ӑl��k��2/1F�Ũ�P��]7e��¬W�23?�j���fo*�a=�Ӷ��D�j��n#>�A;vn޾�����QR�D��Qc���8̆��/ҫ��L=�B��5���Vb5����4UH�BR�Ÿ��a#�����%����Z�yef�EW�������} 8^�/�p}p���4�������	�{�����~��lrb��y_� ��l|h�S5m���x=��ɕ�Q��(�Z|~���è��Gv�W �X
h���u_��$Eއ|���ip��tE�i�R���4x>�����UD_�k���c��=$]e�X�x�|��1zE{�p��Md���Z�9���(z�@�wR=*��ɤŊ:�O�B�*}ᡴX9�F^E���GC�_���?�e��<VM;	Du�j= ʬE�r(�#��1ќ������8CZ��&���t��%K����d�"e��.����\��*0��(�LX$�I8XXO �"+Ήj�[e�b�0G�q�L�8�?�G��(�q �LRd�nCܗ����Le�Sj��k�������~E��cB�`%�ݶ8�2_1DZԆ+�X���a�9GA� b�бYJ��>!l�ɡ|H���Q&+ET%E֡\�{�Q�TQ<�^j����o�Q�`˪Y)NC!J���ezb`{��V���y�T�'���T�(KB?*L��. �r�'L�:B��;��<�&5�W����]��,��W�(L	E��\.�w���{�&	4�)G��P�r8��5�;�V���8cr�i|J�q�>��`=M1�4Eh�-i�]Q,���&�4exALjP�ي9�|�7h(z#ɟ�j_�	�`1���CT�N�vϠ���H7��E������X:g�r*��M�~��V������������Wj~XL{�����LXL��:�h:����j~��<��S��{N��NW����2;125)Xl�>���+��U�%"b�+�Tp;������z�]m����%�7/O��l���J���2]U�@Z�G�̳찣I�s{�$�h�b���[�c1�����8+:��6m1�-�3���x����2e��[��db�HH٣R���+���b��	��4�Br�ecl7�"�/���>�7|W硁����d�ᘎ9�������v\����a�d�a�v��/w�W!Z�˓L=$��L^��$����&���s�ه����� b����s����4��גhÙ���j��~��^!�|g�C:I�.��<U�#E�LT�������H�1Dt�>�4�EGmG�� ���/��)t����E��B]�6��I�N2�T<8�F�ONm��h��H3&�,f�oǁ��-k�[o��f|Qn�� ��p�jme=-5I����KH
���|�h̷�ǒL+L�T�pE`�l1#����Fe<�G���PҡI��W�V]e]�
L2�08�@t���X�S�Q��wE?�j}n�d�aT���@�h7.���?X�$���B{.!�)/g8��o�^U��Ӎ��3���'��O �}�����_��;��G�|���6�������T�
��Or��^]>����v����8�-���b�i��|n:������UP֗�L7�ÖoM�ގ筈�yW���&�i	��SG>�ЇǨ�+3�2�ʬ�ZM2�0r�զ4F�����ؾ��>�DC膔K,[��B x��TAZOQO2�0� �9S��=��7-T�*T�|�bT�w�UD���	��5��K��>#�ن�GT�s�\�.D��џ�����/@j�I�	�n��)22(*3���2|�x�,5��?����)���ϑ&�P��AIx�w�x��M�!�[���Y�Fn�@�,�ɓ�j�6yr�<�Hn�?{�n��
?�1�oQ�eh�Nũ�ME;!�>�&���l},�lä���<�2�0D�c%֡g��M�    y�IS��uX�$� �Lޱ-Pqi�T�C�wE���_�3t�8z�>��P!���XXY�W��c.$OLsV����%�3/Qe֠�f�������҄9����*U���2��C��ʪ���ٖd"b]|l��:a�g4�!X�$���ؕr�G�y}��O�_	��:+I��A(��9�����Q�T�f��J�	����ȜC��`�{x��/5U��F���,S����@�����(�P]�>t-�	2ᐜ��0 �x(��y~d�$�3\C(�ò(f�K��ꋳ��9�$S�e4��PJCF�"m���`���41���+����a�c��x����?�l���l��*�f���<_��:1q�[�
�d֡�Ȗ�4�( �? I��S���}�+L�`�Q�2AU(�5�gȆ�� H�qoҍ",�T@�ڸ�4t��~�~��Y��+Ug]�t2��4�;@d>^O��\��Au~�OȬ�����5?��
�6�4^�if|����-G�/`��Po���&��v, 7�O@��&�'����Z�IK2��(b:Oi����~�l�'�#����$Bg�3��ؿ��͑��OZY��TB�sǓ@���91�_�l$9>�Kk��/w�1)]��L�F��$������i�o�� V��c�I�I��63����F��L ���;�<�ʺ���ÈE�bN3t�ŋX&FEo-�g���.��k�s����B���P�Ԑ=�4Mƨy����Hp�]O�M3tBE�+O��mx�ۆ�G2�:�WUI2�p|x)Ä�s�Gt�M9�Z$a3��߿.�9�F��Rh",�m�o �'������L!��Z�Ʊab��Xb���;]bڜ����':�p��u�����ר����>��=�F=6�9���%j&=ڏ���2��ZE&t{(�2�FA��+a�ݝ��e����H��z.9�(նMm���DAGGɳ����ؕ��BR�%��D7>����H%Ю�z_��M25�Y�'�C��?��Ь�M�^ k$%
4?������\��^�&��@�5-�[�l��צ2�W�u��Lt�:�&���,��J���Y@�I�.� �#G�kWG���$�z��
��G��#Lu�Zr�1�]=�Bxt/F�ޯC��ձ����L�P�h~�[�)|�Z<�f�y�ig_�? G�(����D������:	/�Řd�	&R(�'�ױ�A�X6.d�61*?�3�D���j����sI�/t��XLfz��ov����u\l�ق�*K�cG^�瞯_����`7J�`-��W��3�W�D�߭

�#�B�m���ȂM$�@T!6w,��2�zZN��/��:�-�_
�h����?I���/��aE�aA�\OʟS0��\0B��`�6�Hs�_fu�����6*I\��a��;;�������j3���k� 4d����������y)F��{��N��POѭ�W��4B�0���]JH�O@��ϣk�gB��"ӗY�&�IvV$�m;%yF�:z7��3[^Z*�O�L}�Z��i���
��!�_��/3�-B��S� Tě��*�@9zb��v�츿�/�'�1h��t909�k�܎�p��y��8o_Ơ�?ܔT��6$&=�0&��������2S�EQW^��OxՂ6����Ƽ����s� ��u�%�2V �rk��	v%�#��8ݳ�E�o+��J�JRg@^��甘U�����U~B�����H9 ��s�&�Σr�-��(@G��� ����v�?���>�o�~�������߇Ε��5�OS���Y�,�g����p�������H�����(�9�W��Ϸ��" (�u1I���͕ͧy�7�#++)���dX(�m��{�/ˤ)�{\�Xґ����t�_pꃾ],��-&��d]�p.���6���6\.����RWAj��I<C��n���{0�6?ɵ!�*���Jr�C�PV�V	T�6c�DWl@�
;u^�4Q�f7�_�p�)��W`F�9qr�`w�%��@��P���$Рo`��#D�$zD���B����
}=���
*����[�+��DO���`b���&W�a#��[�8���(��O���v�������Y/z��9�!я��W�w$D:U����J���Tԃ�:���9�ƌ[�򈵘��q�S���o��>�+f���6[gfb�Η����+��(G��S%��5p�U��"�1�u���E/�D���䊱��n6��	T���8^ϼ�K>�m ��:�n��5��ݛ^`��7*U0]���d�a�4`�4�7h$o�U,P��#.J�R�_��F���
.��h������£Z?.�����Jr��h�W��IL0o�DV�J�i�I����w�P�ѕT�`���Avj|��bj̷�#���^`�`�~L���z�&%=� F:�O䐒_nWu�Иo�����R ���Dy(�?�gp��oTt�Y�&�KS�����eq]1A�;߀)$�%��a6��3�:���_�H���CΤdq�]��cN⦷?Ѷ(u�9L�8��G�B4����Ѡ�-�����bt�y�{���J.S�%��W�a2��-k�=+T��E_,��$B�pS=(VlD�]1� S�H)��L�����v �'�x���S]�'��H'���TSz՗m� G�qH�,��ܳ���'��HZz.�47���� �axg!؇�73M@�&o�d��F {�M���я��>t-��3��D�%l�oЈ��D����r����z���@y�ی��Z����Z�E/ ���i�ygP1����D!��	��n+gh��JG>Ay[���l�zyd'3ވ�x�ݨw��6�LxUHߤ)��3��Rx���Qig��#��~ʒ���i
��uCC�h�̪Q^�D,q�a90q:�s$&�H)�څ�LI���kQN�9�VQ*��
.e��.�2�տqQvJt�W�0�x���b��h�E�M�6&K՛�쇧^3^�N�����~>�G/�?��n�eA24)+�$��fF����������mq�d�c�T�R�F�����D�׹��&7���rX���c��G�f�v	�\���݌G�ך[�1�f�u�_����	�$������J��}`�H�!����� #G��"<��"���or�͙&Rb��E�`�p`,�K��/�0������w�Z�=�7yS����P���C��̐�[^�P���5����Q'\�H	D��l��j3|��LL��H�i8�F�7�5�o��sJ�����rHrl�x^D�/
<��m=?�bX{��\L���T��֤�&�f(�4��"Ue���5����5��c|ْ��Bc/c��'bm�8�4��韗����~1�k�$͐%��� \��t:���=�ņ�i��x�u��u�o�w]�AJ&I�Ĝ�4��4���[�2?rT�U	į^{J�v6����r rۑ�B���5}1�o�!�i]%*>dG������Ӣ�-�"ϲ����<J�}a@Uj��S�/ri|�h�N��[�M��̲M�M�����$��F��h���$F	���\�O/�m;�F����En��	�XҸX�q�W�m��;�NvW7G5�(B�5�O�} ������T$��hМ��CpH�ʅ�M���6�4L<�.�`���-.��M����ȩ�M�W5a����)5�HrX��jڭ�����e�m��rT2���ƺ~�p���wu�*L\%�#���q�w�O�hF�z�dj�UT3��P��f���Q��ȍR��4ȍ�D�� ����{ӿ�O�b�'��gGJfDz.�(*�߮���CWE+�e��)��|��8�
�r�Bg+L-��̈�@���5^`�n}���lR���t�pN�sT}�BՈi�nP3L�@2֍���LCеBr��Y�.�����Ms�/��M��j�x���Q�.��N�:H�(ʶ�Aui��U�Hd�1�Og0u�f���ı��Ԡ�ô1�S"�b\MzX�+���8�@�d�rC��'^��YԠ�    d"�5<q�H?�u��k�!}�m�d_E�I�#9���B��c���C�l�h5=�R�,d<�Hx�Ԍ���E�W�l�hX-��JX��6�z`�WDɎ��G�:�T�a�_ڻW�O��k�w�����
f����
_\�� �l�hh~�,_��+����KZ-�~����,jC��Q��.Sݸ`���x�(%�-ZM-lE��o����� ������q�y�4K�(��;
M��Y���l�h'�(�m�1߮f�]�8)s�I\!��[���}�58e2���quH�UlL�7Є�dΣq<g�6���S��D0��ɛ��146��e���P�� ��.6�Mf�:M��L~���W��G7/$4ejk����hf\�xu4@������o��?��2\�#�P V�i�L��~�n�z\��a�+�M �D�0 �Hجđ�&h�\&v���J��D^��Y��c)Z~ܾO���>����EG��Q��bF[�P� `�E}T&DY.|��ه|��Q_-C:]�I��0̂v.���'<�M�Y`lC�=[�Gi5��u�|`��P4B�@"�Z,�"��qw&R�&_ ��
�Z\��CP�n;��?r=��h(z!h��bmfl��+iT��i�X��5��܃�y��n}��o��@�2]l!3��̨��mS�������6�p_�e9io9Y�O�#j����t����X��bG�V �Q�8\9���������Gy�:�o�'��BWZ����X)������ѷ
�²�MI�v8T܄ɟ�*V@�;1!�9�i�G����K
�aM����%�m,S�1���<ue<���Ȑ:�ÎT��a�������]�e���C��O8��~��g�KPM��3~��i.�XV��4ecHT�3$JQ0�B!�و��$��(�.ؤ�����.f��o%��� -�9hb9��d�ZɌ�qD�'��s�_P=Y)0ז����[kV1}�����9cW*s��~��n�&�<j��(E�'De[16)Oɬ��ġ�v���+�O��DzN����Z�Lc���H�T��1>C�-"=�ұ T�Fjg�#�5c`���+{�EUs��<I�� �c8�^O^�[�^c���O�b3�����: �"'�{�]���@�s�5��|?�ˈ�����A#��hJ5��� բ@,��ѠDh-� 0���k�������Q�
_��9��g��O �C����T�}&ᱰ�mcӗ�2��B�G��]�1�/6���s,�#�I-*���S�y���I��H��X��c�U��#�� ����fL�z���IO�����֢.���-��2�1�ց����?�E�8��iJb&�啂wD7�P
<p�!�zH��@����>�zx�	FyJ�+J��W���'���ȕ�h�p�$��rL���f�16O��Mu��'eVd
������`&4�'KN��Ȑ�f�A�B|�,FU�h���$E��C�� j
����2��[D�L������z�3Q�����P1l�Ժ_�L&@vl�C]Q�? �
f��"mRS�%A��`V��\��Y+������y��Gh�}�_w�6{�F�L�Q5Àă�g=�����4i��<�����G�p}&#㱢����̩᳆�)Clr�j��&���HvA��:n��ޤ�][7���!��T.F$�U�ѱ�;nRc�H��[d�9��"�~���E$��9QLx����P�I�Yv� ��t����~�4�!Gf�\����K��n��y��-s��wt�� ۀ &�������Zf>���"$$�����mq�k�������d.;V��Z��-G$��%��-	��������4�}͆����&%��z[���kT�/Z�z��T<��il��@��||�/_�-\t3�j��*�C���d����A�F�)��ͯ�i =yZ#,U�69���*w'c%����p�Um�U��1Ќ��y�����&i�G-3���f-p'�YT�ICAl-�� N��[Fc+4�h-���Y+Ǿ����|�6u�$��\��ʐy�^c�8Q.�֊>ǚz(�W�"�s�GD�W6F\7��A�T�p#�P:
�yg���1��@ڥZ&=��jB.�?�5�ۨ���}�b��e�L͙)!�p}?��ϯ�h����I�DH�X�NM=�sZ�eH�߱��C��Goy;�(6��\�M��qu�t1&�2���y�;��,��~u��j��7I�d"d2T׎6S��5�2;�`���]��Q"e���`,LA���"�������D�jחz�Z�=ƎͮAn�����!��&M����̓���������!Nѷ�3S��خc��~��i�)�2��Lx�����
3�;��\V`���U���3΢��Wl�(o�{=�Z*�m]*=�x$�J�����O��Y��-����J�2���?�HR)�a%���x��#y�a��]��q��v i�,�#<��RL
9��c��`Z,��pv�$�,X������ŵ�q���.��4��
v*T��ڠ��%�#�$������p��:u� �#�-��<b��A�5.ϳ���z�9D˔��h���Go���u����!DIOb9"yF�ɩ�hT��.��$<E�\������#"���t��M��������.�7 �E=Of6Ǝ�il/�$�x�
R9-�Y�Ԃ�
wӭ�����Zܨ�/��w��j��B)4��B�@AL��46-���v|e���?U�lC&7F��s69���a{�_�3�#R��&f��S�Z+l{��@��~˩⎿]����ut���r�;j�l�����DҘ�r�ђ{�rTr��P����^���{��Z�V��-�]����1��S���B��?�4_Bϩ�K���~��!�[��H���AD���4��G������iS���[�@C��)�5���e4%C�I.����,H�/���~ة:̰Iń1r� 4#�D{&z��l]z���>����<
�o��LU.�3�ϒ.�۔@d��'���\1�==�^��_���L�āf��>Z��0�{��|��&��L���15U����5�&��6�z��Q��V6�]�p8 �/=�;H���F�s���`�p02�1O=&�P|=���Zr�S�8Q>4!�6��3N��Z��:�v�] �� �|}��(���Hǂ&���m�k�EE ��CȔH�]�5R"����x�]�޷�ꨋZ;ˁɎ��Q����Ml�F<\��H�߃e�+��_��lS�^#�Q�X�F��c��~��g���ȝQ�'>E>]�IfG��%v��7B�~��zD�"LɍGv�
a,�P|:^�ɒB(15�Ud��O�4��[h.A�k�%�P�ඈ�2CҚ�7���:�Ц���5v�L��;�t)Ol�O0���۱�ˇ�oቧe*����A:�?���GnĝI�^�or���G�;J�4���IE�Es�p��њsg�W�;HE������&,����(q|ȗ
Mb��ˏ��Nd��$�����]Λ蓿�D�m��@L�TH˳��#�<���0�Z�E/I�DZ.�w�5jAS���x�&t�E�D��|�@��=��u���>�-�L����\�Nqi�j=�8�����d{�.��!VnB��"R�v��v4������~�瘔�pZ�B�߷Z1dK	@2lK	J)ol�:[��C��r�R��n?1�>��Q���k��-�B���Zv}�Z���Hf=Zrwǆ?úQ�:W_�-z&3�G$�x��7�k��b#��J�i��'�Է�=�Pr�+������]Gv�d'�ٕ�ރ��c+�3���Ο�Y���į/'�~��6Yk���e�g�sb�/�>�}�h憮�Ԉ��ˡɔ��;aN�_�����O������Fg����L��c���+DQ��_����Wb���*˽�ܟR��k ��e$��"�m9��i���Y0!�,�#ϖt䙅e
����,|p�#W<ް@��,���:C���6ApJ��m�v��~T��N��    �cIk�g�hP��<l4W6�!�.��iQ�13���ۇ�.9��8��Y2M_�G.�H�١�m��噙�A:R>$���x�Ry�5ቛ2�e�K.�W�i1@ef� �#*L]���0�I�����L�L�����w[r9�E�{�FcvTB��`��D�֛ɴ�,���?j��	w��`��=�Y�Z#٘�x�m�Ց-L&�/�\�vc�����^X�&��L�ADFJ]ߪ(��ܒ$�%�$s	��5�ۦ�o���$'S"cB���� �Ǵ��1R%ms��;;��q�>��!�Ux{֐}_o���I�!�<3��诓)���e�F��Fho�%�K�w�<RLr���H������L;�R(��L��	M�z_����"Ƽ�����~�!&���Q>�j�;,i�L�t�D���"��̿�����'3#}�5v��wٜW�
�1����,�L�D،���ـ-�����i���p�Ƹ�UǞa�AL�&���y<+%���+�gs��V$�1<&��^���G��C]��րN� ���ɼ�I�b2-=TA|�ɮ�����~�c�dؚmݵ�xe�v_&�Քobͻa�(���0�s2�������&��$$Å~u(	NbA���c+��j��P�"zy��U�D���h�I"���U�e+�F��k0��D�� �cq<Z��ĹɆ<T�������c��#��2��o�ڧv�i$}�������)a�ߙ[	[�ng��i�}{�JD�D��ȻjYNA��R �i$~{��'q"=T�
�\a=�:�Ti��<�Q��3['S$����oE��|�/��
CR'q$!.�r^J5
�k��B�#�6]kXRh�,�$sU�S�@��&��5~nl^[�B�-�A��6�B�_���~K1��h�f��h| s��La"���J��F\C��-�>zL ����2~ǻ!*Ƹn��ф*�mO�k�+�c���5.|�<�����4��A�����Fj������f�a��d����ϸ�u�Z���2���*�7
�ZN6}Lfr���ifػ?��F��k�S;�)�N]�L���eޢ�צr�d��)	�A��~Y��8-̴\)�ďV+�����W���	5h�h|�+rF����p&L\�&wB�<&1�����0!"�WzYsj؋���pĴ���f��W���z�<��#Z�Y��a��l�W��7�EX�Ò\���%!K�F;��y�ck����#Db^��=�8�@�֎�a���d+T�)ң�.�Cr=_���p�A���y,b>m������_��	�� /��Gn�,����3ug��t�
�̂���
�>�5ݧl )�>�GHG�����H�Er���S��94���vA�)7���N;۽ڡ�m��I����������˚ZV��������p�쩿��ϝ��L��*\l'� ��}2.�N��꼊����1
�<N&AZ?�*y�
��"BD|��\�
1Q�@�X�C&���e��q��^?3�#-�I����>����g��D�dңMd�/�zޣg/ԭ��7��)�N����]������:��y��ѫDj����\�[�Z'�x��QcS�ɜ�`q}Ϣ��z8�W�����R������ ���μQ�?�Yw�͔6M�R�����x��mSO�"�v�[JFR�5�d�#�uE�3ՕP��#v��q2۱�Mف ����6�~�M�$�Fd����(��fZ�]^����Nt�y�N`,<K&��P�����/%T�*���9(���M��j�C��|�JX��H�bD]��I4:�G����n��Gyy#�$w�d�t6������1-�DA�_0w�2�L�P_�I<�%{{iN%"���\/��+�N��܆����$�?�Fv��sy�1�h!�&C2L����I��Į��o/�{���9�?+��$���f�a��7��L�v�I�>�d�c�T+�������l��k�&���I�S�:[�c�Y�����$y&?GnXS[-��b[of�8��M(Ri�˄�I�!�<�h�J��p�$l�h4�d�#���t�IP����v�aOM��M:�z��u��궊e;>�P��q���� ����	o�g�DoĦQ@����t/�l��IC��I���x2-�U?���߆�ۀ-k��U�QI�Fz˨�6����0�X�4��Fr-0��2�+�kW�5 �aL��|(��#�i(�����p�j?��}X���ڇ��gSD�U'���#d����vcH�D|������k8+6�]O�ycL�wB��<<1.g����g�y�%2_p��r#`c4�1��`̵�w�hl�'����J�'�������-i!�&��;U.-���[�U	�|�*-�$����&�0�e�b�1���i��*x�乏��L+����a�Q�������9��rX�+�X���}�>4��Z�y\�<��o���-ƺ��ZWr�=/�{Og�E�Jo/�/B��G��ւ�I���vC��~���Nun�(��^�,F�2} �_eC�%Y���l#��4��~��Z�NE�qorJɆ�%����!3�$��px)7-�0E�>�G��8]������j�BH�(\^�%�*waƹE���7���/�qr�q�e<T������7�|������e�bD!P��Buֲ�rۧ����#m�wi�s=Px��sRN�|�0Y����V�4�fLN������JuF��z~��'s<FA���LE7��v�����UA	��0)x�y��i����MP�D8�p��0�KLE_.T�uI8"�"�Al%��4����W�
�}A_��tE��Д�wO�|�k������*i������-�����y�#�y�R�����]*{2���AS���lץh�q�zD9Z���,�Ƥ�/x2:����퐰���C��v���c��7���|����ʦ��NS��r�U;KQA��5ה�.NnΗ3ȭ���ũ��Wƈ������hYw6����=���1��u������և���d���w���ǺE��)Ƕ(�n���.SKj\���������͂r'�	σ��K>V)d9�P�f7ڿcI}��*\�I��1�����}��c�խA�M-(}�:��n�Շ񼺯�9�9���6�����#As�f��~n1_uL���Yy���h�ۤ3A�q�Me�mI���e&b�*������5^�*�^&%F�Ջ�*�<z#$W�z#x���H�<��7��0�!9�gR�~>?3�2!0��g|�q��N$
Q@&$z�<��VӬNz��UV'�LH�t���F*��-�8=��T �@�!�y�ޓe��\�>��`�xٱ1���M��Nl����޵=!���l�"�O�-��p"�3�f`�
^b#�of������С\h�}�,�gZ؁�݀pWe5��ch��h4N��e4�+Ј��0��{ߝ��f\a��<G3\M/��4���\����iߌ���ֈ�(����qu��P��&4�X��&��ه>`,��^mN�ZC���#q��U�ecF4���.���91���T�	2	qщ��u����e:b .���K�I�;1��^�
r�~��H&zw��.Ն���p\�	����^f&Fڮ�=�V�vXFS�|��[�ef"�B�f}��&���U#]�i����>�p�=���ݽ���\~0w���@	lE�Ζ׶�~̦e�5Cꚻ�Ñ��>�R%:�>����h0�;#���=-�l�'Ԉ���#3c���U����F�;r��r���D0~��.�h�n�4���@��P�ˬ��O,�/�n��u� m�x��RK%�(�dzb���:Wo�ˎd�}zX)����!�����'�������N�~�����]�O�P>5�*uX�`�0�Msw�9��[Ӏ$S�CŨ��+��Ĵ���iteeJb4�t���r�Mz6������|���L���H->��F>$�Q�!�[�V�����7�4��|�ae<EF�,z֮V���2�0��0�B��´�Ǌ�^��Y��L�̺��.�����    ��_�XW{:�+Cf�����e�����c�������AM�R�"~���L-��_ꞩB�(.fC�]��-�|�сj��5�z�����:���5�"qh�$�\����x���á�(��`Z���V\Tc�J&#f�%6�jaZ���Rn 	��!���&Sk��.?��{F��Z��	�=�k�i<�SZ�T��]Hcf%S{ڨ.�*�"$�F't��w�(�Yx�����Wm*�������ȫ�+�v�,\��p��r�,bj]5.^��-*ZD��a}���3$������b��AM&-��U��X��#{.��Um�+��G��N�_:�~�qѣ�z�����w̀�>"��d�h1U[���(�N��P/��X�c����q�®+ϖ���&"/��ŗ������&���~�I�LYD��������~�t�J+����F9"3�o���WN�3_/�T�N��Tt���U�!\���_0*I6��I쯛H�Ԥ������i�� h��y��X��)'B�i3Vd��m)���L[�)Gas��0e����Y�k6ȜE7������.�Յ6C���g�$��� ��(�3:�� ��p��6X>�*'���H�)������nG���y��(ȌEG���	$}o0���v��@}2o����:�cֱ�$
ۖk��ȶ��Dx�1���/@� %������wt������
!~`���d�Eo�!��3&���W��k��� -zK�yN����91.8� S��2��Oy@��8V��|F�<dN���M���.4p�a�m1�WE�����d6�^�i�%�fV�a��1L��:?���iW�Y�s��Q�F5���l�)7�������P���6��#�#��ܞ�S���;�{����[X~��#��v-`�M�X�L�� �J��n9���	�ҍ�Vc�2����3h�[��d��p	�2Yi� �M(�ڲ��Gp�%;2ڈ�t����0pg�?���:(A�dt�hk��a�����d�L�0*��t�����e8N7�oc�B9d򣷤O�jX"/�)/a���н2���_�i���^�!�-"��$n�ps77�x��$[F�Z��S¯��8�}O�N�G!�i��Ȫ�9d�Xy��#���+AZ09ǣM�6�\W0�5V�����鞿u���o	[C�*ɑ�y��H�c�v �jCc���ìJ�M]��̝�Ȼ��p>"�`Q���"/a��� ��]��e5l��D�]/�u�����;h��ĀW���UZ���K���}�u PE�?j;���
���l�ڢ��9��|���@��[��p��t�߼�#dx�e+l�����s�"dO[��u���l�J[�:�]��}{^ j��[�n{��{�:�4e]�l��T�����[|[���5K9��Gb�����
^�Ab:��]��1���E�VwOaBd�#�����kZ�}���0�^�u��F=�{�ގM�D��c��n����gn�zttLz�Dpi��Ry�X_��ո.w�X�s�x��5T�Dp�2��V9�b�����O��BHb9$�M>'R��7�c�CP�!$�c�\�)����Tw#�WR�����w(��P�z��i�č�㒸�������^��=��y�����D�,���|��h����FT栌���<�?o��&ٲ��x�Oi3�"7���DV!HHN�H�qu1�������=+p���e�Z���Uх�Fܖ	��.?�o��{�����B�4�M�7� |����{��<��� j�ѥ���!i��~�&���1�ydCsG�A��f��}˗QX3
��b0hT�C����ޣo3*=A�5�L�j��4����ez��1@3��u[�G�0���.����g��7�+�V�<�|�i����ݣ!�UwA[�X����T�LrL���:��;��p��&h�YC'���ɋ}��`����c)E��>�f4��LuL~��Ŀ�7��H�O��|�:����r���?�Q���ܰ@u��G��r����3�Q�xh2*F�昦]L_��7�HoLf;�j�k;�3��ͥ�ޘ�vL������v��G&��y��SR�h�i�i(9�����	6ԃ�Ӛo�1��3���z%�u�����kmF��hh�*/k�%�:F>UR�Ͱ��1�ѫ��2�1v8��ۯ��d��{�9���d��d��3�J�,,�'%������ݮUs{5�(�i���%c����U��a?��d�F%k�i�X0&0_����+=nvV)�Df8B�/���}����5�?��ٴ#�'�1�_�7��X��}�����U�� j��%����<��H:��<���SdBJ��� ��ػ���L�8�`� Bm��a����]
D� Ss� �j��t{0�#|"σ����қ��R+�,��}l ��)�k����0Y����F�Bds��4ϓY��������	Aľ�7h�4IG����NM_�|��Վ�5)��În���u.]�������~ľH����ơ���1�	U-�_�׫����ٴ�N|�,�JՙC�.M��I=05#���dd�cI0�s�G�eA�#�`�kU)4�2�1l�z[�x8���z�f��Ukd�N�U\����O7�R�����'�l"ʘD�ŗ*=w�cԊQh�d�l��2{ 7O6�k��rOq���M�Rd��R�0@��W�-z됷��.�Ĩ� F���>���g�ǻu]��)&2�~}:����fL�@��|���=����+�V�c���7w����͹ɴS�ո���H������=gL�-���(�KΈ�J}M�g`l�`�]�xl?뜏����i�(tҢLc�Ď�����
,�4���;(�?�-����e�bO{D&��c�^�c�d�KB!��2k�'�p�?"�тK.ʖ���D�)A\�����Ra�f��(���LW����A�L�65J�Q#F�.�l'�w�x��h�G���Ds�E��3��	Ͷ�1��ҵ$>�G�	(h�F��8!��/�Pqn��pW���<�y�U{����(2�o��(s	}�X�n����C���`o^a�8�<B�m�5T�T��""���5��Q&�~' "Q�mΥ��
U��L"D�C�f�B;��2����*�s���g
M!i4���������F�*m[6�=)5K4��`���Л�{��!|/L����(T2?��E,�j�3�K���k>�����R�����a����'!�vK�^�9�}�{��as���y~�2-3q�z��L[�����~X���5jδ�n���$ŅE�%l�}5:\��(�#"�
��;���V�aTa]7�Έ�R��!��Q���H^�#�lR�B�1��cZ�q��&:M�
4K�����>��-�[�7z�F�=E޴p�nW�aUȂ�a�.^�s��q�A@�-�m"ʄ@,��'�i���U ��dRk����������+\2/�<�����~z��oI�/��X!ʑr��6�c�B�(!G�ŏ���c{p��5dtj	����a�ɏ�ucT����	w�'9R�?_��gz0-�Kc�"\6��DK��W�m�^XY}��0�p�[2�������볩'
$�� <J։.��5��I<F�Be�_]R�Bɓ���5���eo�a�D�(s�B�:Z �v������@W�FG����Ap�z~?A�G�}3w�*_��uz��>Պ��r�MK�Ҩ�e���g�i�Z��ϻ���V�F��~e�Ò(�R�sH�G�(=�6e�����zG��bW9 ��mM��=��a=�y8�PM��}wO�޶�Z�(��\��'�V+�`%�X�Mi�4b����}��@�N:�4z�^Xmd��wzO�@�n׻�@o��Fc�#��b@�%xa宸XR����U�`2���q���<���q����������    ۵�;n7��	�V�Dbd��A�ͭt���g�J>Gq�װj��:tn�iz�,ܩ|�2ͯ�(,�Uջ�w�����(s�&;��.�'�r�[�7�'*z�Q��e�Ft8ۖ���i[5�'dr_��D�2�70�{�h� #S�r@-�Rgر�^��b����eV�S�������m��=J�(t�d�_�0"U�H�D���;ҧ���w����&s�Wo����C�7����BBr��_�!�;�}N~
e�L�P���u$����V9�r:���V) 7�a��Lz�%��$�
je\й����dF�b���Sݘ����0�_�IN�i�2�*U9�>�)^�>i\��ux�#*F��/Y7R�s��q+&�[F&ƀ�^�n
A���3�^3*3C����������~"@Ͳ�0�@�(�b�J.G{w��o�)�SG��75Σ��iP�F�kN�׹FeMM���횐��j�g�c��K��yDbG�܂�֧��Y�jS�r���:×
m'����%�qC�v]�e�=�h3�����(��D��j$��^�a<�Wg�r4���Q�*�bmInl��=��)Ё�L,ne(�\�H�^��)d��bF�1;�f����k*I
;����5�b�ht�d�C�t�b��S:�X6:�o5����`9 �K�"Ne����~ɜ�Kp=��1TsSh Tì+vr����xBF�
��.��Q8��Jz���LQ�@/m���BdN�������c��,��R}P~'���p�k�u ��E����g�q��ƫ�u^��r�ί
�N&���#f���؜/7�ܹ6iTh�%KC�8��ǱTu$s$���H���Q�"K2Г�`p�m����Qe�d.`����Z��Gr�q���HZ�5(�ף&76�"%cӴ@4(OK,{�l�����x�<l)TgI� &�2����!\�ɴp2��Ryer���Ĳ�;���\ųiL�}'D����y	9���G�á�C�il��ƀ&�D�H����~};�c��4�^�ېd&` �����>!�v��p�<Vӟ�͒�	,�EM�2&���5a3y��;/�$�KF[�V�	�3���5��YGO<-�F4	B�-D�=��H��!-�/u�`=^NUOc;}�=���r�]]���]�.~�J��<�������ȶW�!��@�w�;<I,a��EO��
K�S)O]"��i�zn�au|���0YP����;��I]��3W%M�$��X�U�AWs�=��~�4�r��h�qc���X7�@��������*����n��T5MU�R�&�e�u=�8aU_1`� q��	��y@��1�ʷ�`ww4�A� q�d�a���%Ww�P;�4���Bҫ�4 *��I&f�me$����i�aX]`��lT�z��h�|ؕ���aH�<y1�uZV��߆���P�!׵h�E�}�w�tr�B��i?��c�x4���?�P���]�K�^m%W��/|�9��Iv'L=�Ӻ�����2ED�9Qo2��(�'o�ԥ:�0Ըw�󠀇$\ ϣS�L��<,8���YpH2X�����X!VMAi����IT�hr��T���·���!��(
�I&$�w����� �iMhZe�V�LG̴M ���x,���5ߵXb�LDtf��� �o��aZJ�I&%�L�h�Į2�!��l�r5�	L���.�W�6x��9�d@
-�$�$��;���6���h�l4�w�\���$���\�̞$�"�뉻dȍ��ň�"�Sx_2K�w����d?\~�'􂢙�4.�QC&"I$�?)��u�E�,�d�BK�8{��s9@GBõ2�J	+��:(��B2s5��o���0@R��'���9�F'_;3{(:����կ�r�E
�aɾ��1ܝݼ c5������k�dMb,��Fu3�&C�l��]�N��V,`�^'������*9|���5�����[�u��m��9t@��~���lM�쬠��$���I�6"��Xk�cR�&'��؇8�g��:OL� )�I���<��\eqcC��֐�"��I�pw0-7O�����:)O�^����G���e��I���d�b�]����_���AM2}1��W]F~-x����� �i�����U����t��ZR��xQf3FK[��Z�3lԪ#*�`��������qxP���Bfc�fml��Un�3C2}��Yg���R+0��LjDX��QI���i)���GÀ��|����|����|�ª4�dJc�i���
N�ۨ[X��L�3F���)��ژy?�	5IC�/Ɍ�@��UdnƮL�V�a�d:c�h�}��oi�ʤPz�d�h1�M��܍;�������i��:���<(1P��$(M���(��ZyЏU(eNc4�^��3�N�kK�6�+Vc��MuV)�`E�уJ�"Y�)��$n81٨��hR��J2w1D� ����h�W�*)dd����q�	X���sZ�.���먍6�d���8fuZp/��բ��uxI���i�#,�>G0�+�v`��XY����j��Lń��[������_c�k����h\Q����Q5��,� �p�?=�P�qBjk_�@R�(�	�	ZC�-��^��h��4�HEYA3�ᄷF�|��B��<&��h�l����O�[�l	�q��{�>���8O�̆���'�y:c5$W�DL,�"6<��Xê$��Eh�����T�@L��J[t�,h3�nW�����8`���n�bu����N��,Q_is[�&΀m�UDs�@Q�+�'�m�K���de=��cJ�y/���{e�%U�R�?{z>������麖i"���$���\��y��w+S��erbok��w���J�8�Vft��f��MD�jj'�:�W�c�d�bo�L-���z/Oh��	�q�2�hT��dnb���;�~Z�'�Q��0�P(3����������k�T!����h��J��Ӝ�	�4F@2Q1E�ɢ����rul9����,(�ḵ�߹�^�S����LۂQPD�2A1Y�v����e{� :;���T��G�,CU���ki��ѹ\C�w����< ��ap� ���.Y��m�4��,�,�׳4��B�8h��f��"#Դ|@���Tɍ�r9w���ݵ�4�w�Щ�9 �Ū�;��6�4e�y��v�l�V!��L�_�9�e�b���3,m P������?���*������Uђʆ	��^.�R���ҭ�:�
![�+�@�(`�t=�i;߹��Q�CYhQӎUD��u���\j)|�2_q2OJ$�F�tE	��.�OB��W����n.�l���A��,3C��B��}_��@ߗ�3�d�}:�箰��e��-�@2��6Gt�7��
u�i� W�����ߏ�qX��k~�a���V/R���tE��*��~�����m�L�!��ӄ�o���?Y�.�$�
�����+����'���"�����:~?�kbJ&�X-�!�����e��N�	��m��b�[PI%�/�.y�f}��7���Y#u6�6���*���^�1�c{�0�?(����],�m- n9���p:ѳ���`ؠ@��2y��������o���3L����2wё�g]���Bt�AxP�V�X���'ǹ��?H��tc]��ѹ�KF�X�~�H5)ȍܸ~�m}�PA�)K�F�-XS3ˈS�D�����n���A�q�$^����Z���v��Mi]�"�T<�H$�7oM��-�fj��ԠRp�������{l@��9,���Ŗ� 0o�%N#cO\���pem��7����Dp,�)��0}�����K�*�*���ػi���9���ͥv�_a�8K���'��Ty����\}�ͱ��*��=%fchg4M@�	���t��R RX��2���x�%��6�(�.���r_��� ���"@����y7$�@R�ͲcO�7���o�y h�v�˞4M��c�%-0���4yc�Q�8��H�΢l���$q��|��v毰���d�n��    j\�ǻ�L��L���1�5�ֵ���Q�uB�$�s^�<�ta� ߗ$\��[eI"˔G��9�����y�V��2��g�l�l�T�����"P<�;�C���u�k��<=����<h:���eΣϸPB*�o�$��zNMW�Hϲ]����u7��n�m)>2�3 R��2�1P��Plv]���tZ	g�E�,�}�W�;z:�)�g�QuI!T�\�ɉ�<�:�,�x=�F9�p��
E�Lw���d��v��Gp�M-���<1��.�u��8�R>�ո�^Wc}g��LE��
י�w�H�����[�A� ��#[��:�ݐ@C�"��G�m��74�)N�
��8_�?�	�m���u1�A��ܺ���
��g�uE�q-��G��7�n���;��՝廹�j�ajd��h�v+U>��c��nw�*���o�!�e�c��M��f]��l?�V^d�م��U�鏕����Fݗ�V
x�yfK����	�l�[�M��@����ũ�)-!mWUj|�3{�������z�y�% ԁ*�����X�z�[>�*��L������Y��8V�I)�oֺt�Jd��C~�_Nq���F�R��1⚉����A�c+{�QW
k���֩�����:��R`�d�
Y����<�z�x&�i�3E��,��D����V������u؝/CeDx�s%�r��DH_�[T\�úˌ�O�T�8�̃��eKf �[X�N��톀��A�	� i
\�\I>JL�_p5����B�]�A&rMv��5%��K�!�[�olA��6W����G�'=�Ԡј+�L���ʙ�n�!���U���tȔp��Y�S����1e���W��2'��$��v��n8��M�i�V�=L%$��L�,59�t�!��w�f"S94���c���K�:�;쾙J$���-��=����f���}Ó0����=�HkR�ﻡ�c��s|�P&��UY�M{곙0Y�Ϻ\��^�G�V���z;kpǳL�	G��qn���+<��lژb���Ac�(ne�lޘq{eY# pz[)i5F�2?2e!���K�#��EfG���-R��l���-x�����Ǚi�ڂ�l���`x�,7����J�#f;P&��T�%��?9�'Y��+�%�"*�wdiD�D
�ļ���$���2n�=tjB�̖���G�mxl�(�I2[2ލd�e�T ���T7�L*��L��ZG9PD_��<��F��=��P�%��z�l��g�T:"l�\��o�qs^U��*��踗�!�EW�1���Wk� 4
l�~�*Y�t���y(�JN7��M�>�/�$��������?
�6͏^�I&����}�o�d�34��'��,Ʉ�y��:�����!���_��
ʫ���c?��Uz+]a���6N��v ,!�>K��8}(Gd��5�ȡ+���PX��evdoH9�X:���%QM�5^�lAcp\k�SV+��
	
G�b	L�`)x��2O���2��B6+�R�RLj�%@~>������-lB���É,!�De-����%�Z~u�o���NZ~�L�L��L�)�L�^�2��e�d�
�Xb%�a��u=������^�L�'�yhS����!��8ײ	ѹ*���k��EZ� �tD"N�o=���&���	9x���(h��s2��SG�����Q���ڸ�!1�w@�S��2 �>����x��^'�i� �6-Q�h�2s����L�
���Bd�Д�|���"%��2_CD-��y�:�QJ��1��L�L���|��h�a H�Ҏ�<|ӂ�*����_�'�dH�ҎF�^�H�Vru��}��(�y��Z�<"9Ѧ�7�d7��8�� ��L/3%���>���&騐�ݯB��e�d"s�>W_E����,r���|��DG��+($���cL�6�~R�������/屹�>���ɣE��`��{����u�R�����cJ��U'��}LD4�����[i���㤫�zt
�Z��]�^6v�jU��d��b5:�šo#������G�m[�������Nh/{<&�U�1�l�����6�i�1&X2��P;,VI�dǺkT�Wu�Y�+6�
�K]6$���K�
{k����4��DA،h�5����A_ȴsDm�ڰ�����0*Ď���N�<�H�F����dS��yl�]��k6�272{Z��5RQ�z>jp2��V��ˤ��Ѵ&�X-�#���-��$F$�M�J�7��7ۋ]��z����5�h�wOpc�n�h$�|��lw=��'G����q��8�C��r��i._�f�D�,��Q�sK蒼W&2r3"�� ��񙉁��Xk��~���#�$��$�j&%����.�݁ �޸
�PX�R4l��>�z�~�n�/e���� ��/p!{���|X�_ӦH4�Xޔ�Uϣ������"��Lq:������\��Yo�J4�	���򍃠���T�w��v�5Myddb�Up\�M#!{��U���Y����ók��5j��GOj:���yƽ���8T��!�Dc�[b��-6�*�o�L}Ln��Xg��	Vt횸�k������B��?�I-�fE�a�i<V�|���mN*��֧
���i�)�=�u�xJ_*�w�S'�T�ǳ�ǋ�5�~,ʢ狅:E��s��<Aq�����k�9�`Z�K�F�5�����yCc�����`����Z>w]%b�q�L�a���l�˴Fd3B���> 2_�h���J����+H;��t	R�Ъ!J�˴��O��y�[�����*Vo��iL	e��U̿��T��v�������qK��m�Q��F����Y�q�~T��Y��j^�{&���A���V�B4"<�@O#6��d�R�U�W�!��ï`^���8�<ؤb��΂FCVf8����q��ӶT�<�A�]�[��5��?��16�9FA�_�9��8lE�Ul�4�
˱�����M+sM�͑R�;�13�	�>�@��ʶ��CG�����(|�2��w��^��A���m�{#�e�F!&���^+X0(�W@#��q�p���6��5��A� ���4FKNɥ:��9ВnL��1	��-�z�*�XmC���@c���<ZeU6w\Ĕ���$Gg���dS����N�}���|���!���ܡ,f��\���AE��=+aG�yD�HG��áF���� �s�*mTߝ�������:�n��hW���t�M��������o�h�Uu�G������*;=N�]��<�/^��T��m�`�/0��j迢� ��c'N����*��^f)�i��G��,GM�]���ǳ B�e)N�yӘ9ٶd�3/�:������m�e�%�7G�
{[[�����RW��JV���e�b4�D2�p��Ԍw4���(����������Kr��a�b�AQ�M��D6t�N�I�Ϭ5RS�:�Ç����a+�
�� 5�;ɶ�y@r�#�I���+��M[�~>��R���C�t�GK,?+��S������?(���k�ɵ���Cx+-��p?3-g�}���ƅ�ؽ�l��d�K��]$�}��3jm�?Dr
�p�<����|��Qy@r������lN�:�l�4��~d�C��K�%�yR����*@��˒-A����ק�<b|�}���)P�O����|bf�q�m���@�֓�w�m��y�Q4��!��x+��-@y4�0S.��.[`����R�E�S(��Ԇ��"ś2�ȩA�B���4�#��9"����u8��I]+����O���S��U�-j��:��z(~>]�[�'Y�Ώ�u�Dm"��H(��>YUԬ���)IM�+ض�:� c7� ��p\�̙u�3$�
7��m��������7�̄��y~��y�����p��L��>+m��`��J�b^�x�a���e�1�r�@��@`+r?��������p�E�DF�z"4g���:V;,PV���!=���p��vuXSR�D9��u ���{`� �0�� ��B ��+A�}�
,v    ����K8����J8�:�ݣ���\A8��k��z�n�#~�8�g!�	��ҬßX#�<�J^��a��V�m�J�#Ǳt�w`��[���5�U�"�uI�tNӜ�R�����w�0�.�|�f0r���n;���0.Ïq���A�p�J޺���'��q+��CU�r������h��4ހ7�jk��X��������{V��'a5"��=�r��� �W<9Z�+s�uf��D����>_N��z{%�	�F�+��"��%ن��5�@Y~���t�lW�-�^���]�)j֙�I�Q�G�2������:F)@	XM�ūX�9!n���6��H�P9C��~�͘�W`]$rЬH��>���kF�(�JB��ж�U ����K %KU�n��ѕI��"�2�N��#B�"���;�P���
7�@nC �5��s�J�`�Y~]���A/cuu�w:�h	��e�$o?5r�3=����*k�Px"{��@�W Y��@��3�`~8�;�GS� Ov�����zL}~�J| B�N�j� x���FW@,L�r(H�6���N�������a<�>���dj���U.p��80�:�~�1�	G�,��� ���{����0pM"��h=7hJ����h�n�&�O�Yp���@�I���͙��N�j�d������Ɗ>(0��� z)�
8���H�b�����vH@n֮ A��Ul�U����Ӎ�`�yң��+���?>�6��|��6���/g��d�l�5�BWW�~!�*�<���������!8+��??�"U� ~��璘�}9$na��[��gFKp>O�u���=�*0��X`��#��a����i_�yrp��v�C���~���<>G�������TLV��i7 �p�`��iV��N��a�1�̙�~�
�R a!��3I�y�X
�+��r&��>s��F�
ו�腿OeA�o�H���!,-1�E�A�$N	�� i�B*������@��|T����3��5��{�a�:C�Gu*ic�a7l�1�-w�%�;��B��Q3�a3bc�gMT(�>�VP,E˪���ۻY�G5�"��]�Szx�,h�C��3�P��	1�\�H3��1<0gھ�
�gB%H�@R�0Zo:a@�<�g�$���3���H_�h�\vѺM
���A</O#���}��%�@�0��w��E���t�9���܉|��IkjOc�~�!u_�ᙈ9���v�y��♐��P�o�oox��6���B�F	m�:;�g;���j3|��M<�5
�nV"�֧t^�(�	 �n�FAf��4x���1�����������j��+����*/�u�ih߳��R�a	�y11�in������
��:a��FK��Qa�BaF,�r�j�3�q�߫� y��~�"3KL!"F�:���ۚn��'�v>f!��L�
0Oq����um�T�\6�Q�6c��'s�Y��l�Cp���cx hӧ۩dv44�5�4.|�;�0��
��+6�&��L�,p�&$���"�3��Fm����(A�.��y�%4�ن�fX�����|(�`�;�0�ν����\�0\�;����K9p�I"~�߭F�x���s$����[k�x��u.��a8Ѓࣀ�`,g%�qF����$�*�F�e<I��1\�0��������9����44�"E�~��4�^��f<!�6v^�ؚ*�����өp��P�BA�"�?/�f��3�,��&g�Xt�kkU��e�t����a��/�yτ��2|��~�d�JT��Ah-]U <;��W@x&t�!����LԼCh�kT <2��/���gtc�ϒZ�^�B�Z�Qhx�2g�����,0�
��tW�U��(�34���?4�4~^�/������K�Fbk<��rP|������wb��	������+P�҇��@�zTh,p~����J��U �@a�Jr�����8��,jT���G�@,PD9���K��i7G�p0�?��v<a��m"��.�ʸA���u]�58�o�yr�DD*)�L|�@�>d)l �`����A�G�!�G���	��2�`W�?t�n/%��ă�������,����`\���X�>��rD-�߁�\Ǹ�5zE\ ���U ȑ��_ `����%�
 9x��f��p��6�*y���u8]n��]^����v�OԲͼ����f�㳇�e����z��s��3��9�\����>F���N��Ȫ��X>u|p$���x������iԛt�?biSn,rx�A������iFF�^>a�����Ʋ3�~P�!�?�_�_�����+|�Ʒ���>���W��Bl<�� �H��y�way����cY����B�<_6S�`�C�T�D�>����{�ڎ��c��?_������������9iNn��u�����7KQs#�8�F��w�<���x~�W*��kJJ3��OA���Cێ���1,M���`�A�M�<~��K�?*��F�pƷ|J�S$��Bm^��Udh>�a� ���+~��G)���n�>u����1/pJ�P�D�>dD_����}-�o/e��!΂gK�$�bԐ��T�.0~�?o����	�Y�)`�9F�/@�0AQݙ '	�Q�]�� �>X�	8�0�ä`�U`����/�BO�Q(g�� G��z��<�����9�] ��:T��7�U����@��3h>���� ��c��N���Ee7�.�z��/��F���{���K� �S�����$��.S���.���;��êNH(��5&�@�J��K1&��ߡ`]UG���4=$>�7�~��P�%�����a����4��B���
�;q�fr�l,�Q<�C�����	�������v��h�<9pV�p�5�-������.0z~#|�N�Y+�k�gw>U	cGI�l䪑�-pz.���n�hK
���.�y.cU�(Iވ8�H��y�XK	�n����5T���!��Յ^��S6'��<�B�����`پ]	�UI;�y$r�!���öi����.�y�t�\���7ؽ��]`�T��-�(�l����Uec?��g�<�ڱ���m,��~��ix���@��{3��{UTvy����YҋႿ>o��.Z�ֈ]��ܳ�
�Q:w2v���B0_A����w�:U��1�^��@�ɒ�+��x���L�4��"��;��)|��2�̀;U��?�D�I}(��T���5�ri��y(r�|~��u��3&�lG��)�]`��ȃ���^��w K�t���
	��})����0ӂ/0���F��@����0��#��0����خt��X���0�W�X
��0�K!���
K����%��;=,�ԓ�X��nXN%���M�X����<���YA�����o����m�ğ?aI�Ɓ
������<S3�ƥ�������_���|��2߄z�*��,�-�]�Bଽ��DxW)���8@���R��H�J;m�X��x�S�
��8��'�~�%�@�C柠��(rļ�F���P$��]��W�뾎+R�/ �KCx�σ�c����-T@�1yi��2�
�Xv���� h)i®���x� xh�i ������4��E��p.���ງ�� a��k��qXa�w�8�$�<b���[�+p,���/�+Z� �H���]`����\h��p̀ʩ��?׷�Tvi��uQe�����/(p .xk���D�1=�c!V��O0���Z�3|x�B7�L�k����9�3���#z�B�,%�8H�hu[٤Sj��\T��h@��4���1�~�W�Rx@�is~_��)4�r*i���0�u��������5���c{��_�,�x����ЯrJ.�t�1 ԥ�����0� 0_�@�A���ֻk|��$��<9�>`p_�A����ӏ[�����d����r�    (4lZI��y r��u>�&���/� ��*�*� �1�ϳf�c��
D!�xB%h%�!Z�����_����1D�n�iLz�qd�ߡUp��hn����k�4/+,��!��?�)K�s���5�C�����3Zr�Xc;a)�y���K�g�O��T���;��E[ϔ���wٶ�O�����b�;���?J��_Q�=�&vu׻0w4�[{~%s7�/e���;TAZS��������8�~���o9�X}�p�n5��EC�m�C�m���HwS�,0���Ӂ��� ��������_/��	>,֥�<�0���N����7��5��Z}�����S�����s����5��J{��	E�S�k�pP
>�\�Ɨr��!��*�ݩ|N��p�u���>�����?��a��!�7�����?���J�o�#,� ��5�h��N i��ͻٟ�ja1r�_�����T��|!��,|�²��r����%2'�v_M>��p��^ͱ�#,Am~���V۟ׄ��T{�W�� ZO!�Ʋ��.)զ��w���V�3\gQ�GRr�= C�z���Xת��d�Щ��kJgP�E�7�M������:#d�E����	A� �Mf�]	/�cl�څt��&�R8�n���oц
	}rr#:cU>	���@��Ep�/�2�H�����rq|���$����𐒂q쓅�e��x�.��e�U;X�[6���?��7}��ل�9r�uчy.D_3�0�4*���q��b�m�oP��Pr;;�[���X�/�+�cWdx߁p+p�""��1�ˬ?]�h�*��]d/��of+��v��6��S�He�_+w��O�J��7uD�H	�U��^,f�3��P�p�Z��Y�������v���� �qD�v8w��CKO�xA���nއ����t�lW��$aS{>fl�p�-���.���@����>+���6�E. ����6�� ��8��?����'D2�T8�1*�B3�,���g;,{l�d�1����<N�!jO�����߀A����H���y+'}��/�f�V0���<_�0*�	�O*�&g��4��+��L˩�R8�9:�c����*�Wu
\��f� �Ō
Dy\az�r�R���`_���Nǎ� d(,8�e����n�����8��}���b�%�|�
4v'�kw�c�ׄh�%�l����C�Pg|����|�������B��ɾ����X��̬G��B����BQ �ٶRT`��H?�rK
c�G�oohۚ�����oT:��X�tɤgg�Ը��cA W4�F��s���o�P�<SlD�Z	��o'��5�k7�`1
��h�g�#�ٔ��F����%�FՆ��S��p6�yr�hK�ʤ1Q1�t�FJ����$b'����D~z��Q�cB�c��Tn˿�-���#���}�r��ڳ�W ���L��n��������0?�r�c3JDx�����-D��D�\���y����;i��j
�h�s:I�@����Z��X�
��KS+$�m�r*d+BN2�~�lށΑ�������#�,y�K���1b3�\��� �D��mz��O��p9�>NN�nv*�PhM�+�y1��� ��;����vi4Ja��k�2*��6W7?p%��|}�>&\|	ԩ��Gp���V�t���°�ߖt��tlN�7����T���J�3��k��ki��-\�׬P�X�!����P�U؄$��h���GMok�e�[W�yqe�u�� �w��O(w�#�B{�X(v�B��l.'�`z���sEP��B ?�Q����u 1 r�+0�A��T��m��ӲO_��?i	���/N%�ȷ�^�[�ΰ�~�Q��@�s�XY����]�~��f�M�9���Tz~d��R5!�vT-T�c��%�c�m3[�x��&��`�du_#tϖؠ'+�|��%_!i>���<��~����y��]�7"��P�i�@s-H��lj3/L�鹿�����F�/p�!0v�R��f�:7���\�Hzq�J���ot����]��!�~5�Z CTz�|,[\�|���/��4������۴hʵH��Y�F�1l��iF՛�٥k��o��g�~L(;z��]�SpVt�����BA��w~������:m��KlfΔi�N �m=���ՐB�ڻaS��hְ$MJ���.�0mx�x�x�w�md� �e���7��R;�8�u�%U@���R��K�m/I#�8�\	pn=*��z��c'tn�"/�k�� �P^����
*N�U:`b'�6���ي�$������ I`]דţ�u������ 2�����a%���ϣ������|! �;ޮ������ՒSiP\m��;޴�@T�"�8R���Ni.�����c�N�7Gz׶`��@��� ��
@��/���	���2�6gl�����Xt'��1��v`߄t�8���E�S��e���V($^삭 _Qz���$�J�'p�k'z����/�"��@�B�O������92�u�p?��m��j�d
z�=�H[�*�g׽�m�r�L���2�@�o�yF^Y�ۏC����s�y��1���w��r�7:�F�>�ɬ�y=nB����T	>ep*Z��k�ιG��nA��Z��Cw�J�:KaJd�:!�{"c��ϭu�A�|�7��cL��K����U�hie@��S�R�Y���c����A@��Nh�>T�Y�g@�W ��WFq��4�v#����{pN���%�zy��R�- �:����^橇�>���[�|(PF���b��e�zn	��qw��X�S�.�cg�N�j�D�@Zo���g�w��8͞�Ԯ�+tb�DX�Da�A�M����
��^��Bf3�p�2x\�.jP]��U/�`�YX{	��6�i�f��0�Ktu�r�`w� �d<�hGE�ۛZa���ٱ�%Z6�ٝ�@T��i�g�N0����њG�s]����Ķq� ���Q�nr������G^Vc��/�V�4)S��-�8x �����J/����Z�?N������l�hy��4|��Q�5(��z^��Þ.���S#��Qvm�u����E{�>�q.Uo:p2E�q�3���hBy�|��1���[z��^��B��W�l�@-U��WD��-��d�m�/�A��*K|g/���'�����ݓ���*��u��0:{�xM�s��&�F����1���
F��0��q|���Nv]����Ȅ��sRF~ivHr]�r4 �����q�C���\�:�;X���8�a�B��+�ʿ�C����B4M�S�8$�:4���Ж��|�ƶ�)�#��x��m�cD�2\6�e����߆���Ty�ׇ���v%���i�r��
��qɍ+̘֭�.H��կ��h�3߫:��xDq�wL��/@�ĭ�im�QHcd�{t�����Å-�<��LvϞ'�����QQ� l���(�y�2�p.�P{-lG�F0y@�����Ʌ����$��a~��f��ٖ9'�<�R���5��}��ů�H�b��^��z�r���0����L��'�)���+��Lg��2jg~��jņ$�;�� �a
ve^b��o��>-����ކqC'еLq��I"��Zo#r~W_KYw·�h��v/��a��ٵ�%�<��M~�4z�D��X&��C����
@yRSVF�*�G�Z�� ��>��H��i=����pט~w��yp���_,��;w���Y��/�c�M��g�0�4?��U%�4vlXQOX�z�,��Ha��/�t�2{��?X���J�ufYX�5��w���}K�Uz��{/3�c�gR��4_ 7� (�a��¸�y�������a��/�<"��u�=��J�� eb!�u��~���ц���r�U+|+_�<ǎO-�Ć��;Q��<ۯGR�[��ꮑ�3\b�*��N�r,�'��t�m�2Ĕ����,�9��	Ho�@#���T�2��ϼ |  �7B�5��e��\����B�!s��M��ǤqJ廇� �f��x��k-yT��Dd�iD�ؗ>^�s%�i�cִј7�T��Z����y��Zs�pd܀Pe���vZ6���s�'��T�p�(�sg�~4>l񶉹cT ��~(_ƅR���E�r���l�F`aq?��:�'L�]�W��2���clK����p�#��Լ���k�ß�X��2�=�^p��45M;�ӰA�������)A�-M�a�8�Z����d��8��3o,�,�W����S���
�#��;4T=x�+z||�DE[��Luy��̌��m��]�T���C�Eu f�����]�^�LC�i��P��uZ�*����?�AH�X�O̅)'2��
�t^�����V��o/�iT�4�Fc�Jf�KpؠT΂��q�
f�LUl
�k�i�;1��t�+��s���]`b�q%�p'�;��0�S�\�w�m�pgYY�o��� 1
r�~�3�<��OU��j�fg��_ز���`!ږ��˖���  ���x�sU���U��p�(�XV����>��W����>?������βm�[�o�F��/��a���:e���נoj+�V�����o˃��䭵�/#&��v���,�#��.ɒ�����G�¦��b�i�WI$������e5*��u���n�q|�&�!�<0������V4��&�,-w9�y bq��씛����Jp:kt���<�9r�MP~�ԍ�WB��e.��ea]�D�t�P�я�r�����C]m0���h4dF��g u��j0�N�}�QRy���m���ڬ��i�o��*�.�K�q�</�@9XCs��3ш´�%Fc#]&�'W�+$u�W�����^���F��!��yC���T���.+&��'3�R?�=�t_��x�@���Ҿ�����e3�Q�� �j��%P�#�I�qZnL�LP�F���N�?O,!R����1k�����7�ق�{etc^b�C����Y���|���5�����X��i�qEa��^�R̂x�vU�(8�z���]�o�1F2*�\��7���Ԭ��Y�S�Z�"��wkf�A�~���$r��췗�Q��G�� dڷ���l^�ZH�:~�<h����d����j��y�3��2U� S��G0�W��Q���O�MV��F�������d{��Ǒ 7\�P�0(�K-�T�Idw�r03����w��/�*)�8?�T�·���F�򄳔��҉Im���2�;��k��7ϟ�r����>K�ix�S��#��h�����S���"��(��Wцy� ��@�$��b��
�M��9���H2<L5GnAFA�#�l��q镹BZ�&�U���c�ҹ]#R؃�yy�k	9����N��"[{��T�;$&xɟ��X9�_%��Jӏ��`Tkh|*d�A�,�|0����_?ˍ�:�R]�`@���$x]�d�bM�U��_㞠=,�i�ه#4Z��>�Ԯ	f땫��	��S|��qa"h��m?*͙/B�U����$�c3˝gau>ikn�@������~?��:Ɏ�ه�"�^B�Y�����^��T|��AW�͝/����pl��Fa�do��}@YO��nL]Ѿ��q8�s}����5�YA慧�������AV��˰y���m�V�1\&��fK+sQP¹����5��6�QPa2G<5�	�.�e�L�e���ݲ`y����3��x�S,���C��$*8t*�s�oe1�����_�����P*��F�|�1h�C�!�shh��T�����k×M'�	�����kH�T�$��m�k��2E���.M��'��}�""N�3c� ����;���L������P��A�'�<Jy��穬����7��X��S߭B�%ē�0A|x�o��ۏXA(,�Ù�z��Y6*$�������F�}���4�e�>i�Rf�KE�ۮN0;��s��+蝇�s3o��0���#���pٕ*zu:_6���1A��Z�>���_�|�z�KL���U�pd#t���@%��}`��Ƭk�4&�a�S���@�7�o8_ao�L��#))�B�Df����S��0��v"�m�V���)�4�O�M�~���"%�|Y�m/���vȢ@�2�ܦf">7��X/��V��c�BVX�
2��6^�h���2���e�ِ
2�����<6
n�`�z���@�x�L=��j���� a����K
�&�`����mc�����HA��p�W�y��2	=9�u��r��%�ft�Wځ��o�<Py3��oV�́bg܆6iܕ2� ��q���ձ�9Ȗ��R�24�ɳ��p*���*4{�6���U�8	��:���&�̝�Y6��kh!��tS��1�����):���+(��n}�f��OE*��
:A�E{A�8g�0��5*q�6�&�
]�z'h�ASQ�2�<�&o�����H�Ij��S��y�:qѵ�kЇ���������g{��aw_˱��zE� ���6(�aϜĮ����|Р��vQ-6�gǬB���l��2Y� ;���*�E��+�|�����A
��A���o.p�V�X����q����\
���:���
P.7Y���go�a�[�9�l�2�!�����u�o�I��P����|m��_`[w��k��)�5
��A6I�~��)6�%��9lE�4J:������L�qn����C�������䦞{� �R��r��=�s{�zoh<CY�w�%���āx5��(:���%ׯ�*�D���(�w-�������W�7�9,X�h<��yTj�?��uh���:�̫���]�s��O�p9��f4��$j;4��y��L&��!v�Z1[�2��ԁ�g��tř6���G$^"K��W �={�)�?goñ�;�}����V��i�2}�4[`�70�6���0i��s�����2���
���3�q)�H��-�`r*ӳ.{�D�TRЂ��%il��E��9��װ&�guoj�L^���03]�r�����E�ط7J��mv:��B"�{��i�aw�᥺���r,�nuj�ve�{���h���i�#�g�.gT����>����ZsO��
�s�pl�����2�=���V�Uh��P[���Q��
Ya���4���K�Dm��:?qU���U�0��!��\�0���8�
���"��7�@�����x��I�a$�{� U^{��/H�4]Q�9�,����\L��9�dП$�F���^�c4L"��i�j��:~�!D�H�X���`��.=�{fyͯ�	���)]�M�i�~�l�ާz�.1�Tw*��
Iz7x-���C���������>ϖ�      R      x������ � �      S      x������ � �      T      x������ � �      V      x������ � �      X      x������ � �      Y      x������ � �      [      x������ � �      ]      x������ � �      _      x������ � �      a      x���ۖ�8�%���z눵�r���:����H�9��y~�>�~l`J �����Y�3
��ͬ�le*c�������?��q��u[��gx�����UY�������E��������5}.Y��_C�v������r�����F2\eL�2��4o����ꡮ�Wr�J��6診L�>6��j�؇?^�v� �t}k��_����L�>�״���O�v����^|p��O/��\v����D�>/�v}����/��?mv��9����������矌��L5�L������]���):����!_$}4t��z�p�g����oi��[�{,kj���45����jz5�M�>�LK
򽩏雘� � |��9|�3��v�ﾤ��:Z^.g�f�й_�_.{�WX~;��B�oh�M����t�k���-���߽�@����-|Z�v�6�C��X��P�;^����mӡ�9j*(4<B{g�}G�����<�y+j�������h6�f�`�?��U�#�M�����$��O�I?����&-Q3�X`� �N��O��~�����.�lYÐ��vQ��O�I^?q+�1T�����,��כg���{��3����'[Z4\��?��"��0>�6>�D�ݟ�t�v]�ZkK�Aj�6��X���l�1��|S�0�:>������d�kZ�R�(m�d�����������bhZ�f��" �������/�Qj�����9x�O�/�L8��Uhf��Zo����=n1<m#{<?��t�����Я0�l�0V���=�-�mu��>�1�~��m�N�еr�����R@���������N�n���[8���vZ3p,Ʈe��%�Oа��֔]�5Fq]-��U>����Ͳt��Z�X[�$��l����̤c���a�!��g����>����~���Ec	8����L�c� W��3���"�u��T���g���^c�r���;��x�0�_;[f���k;�<.�_��y������T�ݗ�{>)[[v�95#�m���s������a���	�����v�gfQ&���$.�.Dg/&�P��w�(i���$#��ۮN���� o+���`�4����ѷgu�_qs�4��k\�0�pl�A�Vݣ�.:��͆���1�`�u�_g0zF�k�;��/;��S�� j9I���vm�;�'>�I��V1�mStF9�!�r�B�t���n��i���	��q��&����x��Mb�/n?����N�m�Hz[�9qC�1t��N~;|�|�Lnﬠ/3��N�����F�ҷ;��C�����:uZ��r�p:�7��%�<����%��Ė4����Zy{�'.W�l���G��	��s-U[�ՙ����͡��-��-FS+hr��nia�7�����|�#���H�i��X�[+p��S���"�t%B�bt�"I�З���� �`�7��S+������uk'q���
p��
��~�4�ގ-�����m�M�\{���E�#�����c����{�bag	���m1�[�8�,J�B`�o�d�0j;q���)�7?ȵ=���[w��DO,8zTA��N�ö���9��n�P[��:خ��J6�O�`(w�!>[T�3���@_�r��܉��8p8�.�`2ˢY�k�/���I<e������ҹBڝ�B+�[�dvn~��O�UO�Ø��C���[]�
��N#���,*��|[����U����d��rvU]d���=ü�ZN��vƖå�̋)d},牋忂߫���;<�^҂w��jE
o��p��*��?$F��\������Eϟ�r࣯V�x�ڷ����_9���g=�h/��T��p�Ȧ��Q�������P�5�|ٛb0�r�6qp���v�197�0a���]�m�gIg�{<��  ��]o��ت��9���1�
lN"��5`^�X��!}��N��v��0��W���C#Mlŋ���Ȧ�[9 ��I�=E}��8�yc��Ъ+�J��Õ}ղ#r��:��2��,G��;c�|�:6�_2�w	N_d:0	�&�M�1�8;`'f&�T��xY|؀'Ɉ�C��n0-�-��c���i��x�P}c�i9Fh9u�m���Rw/��o���`z�QzN�%U��}�S��1��c����|�R���ߎt��ɗ� 6B�q]�?��`���Xt+�6��c��3D{������ז�w�b�"r�>�;�=�}�o�%�N/��Y{_f�4�!���s�Q	Ftx�do�l���5�.��0J��`n�1��a��¼�p�|�d�1���������O�-����1���b��3܀قE�b��8ƨ�����h�ozmUE�x�I9FH9]u�bb6E���3F�3]�I����h��R�94F84m�������)Ԁ!&$�z��ɣ���.���W�S^�P^��ݸ���1u�ia�g;�� ����G��?ɀ}��ZF&�!��1�/(��	Og�X��j��#����AEX�}���nc�{�h1�c�3��Ɂt��*U��N`0���%���o�ZyO�)%��%�N�{[��3��b�̒�$������nLd�Ǖ
�����%8y�`,�w3W�0W�>��� I����|����b����<� :x�ll'��e1U�U%�٨DY>Ë¦�U�1�S���L6�]�WP��p���C�J|�`���Jk���^7��GA��^��L��N��Z�8{a��Z�.�����kE��]]A"qݞ[�]{=�,!��D=�q^S2��s<��!&������VS$	ҕ	]�]:�I�qz��O,�����#����$��/�b��L�}1�}�x���5��!����"��T�h�n�3d�^��Pfzc2�2����'b7ObFU�4x�7���Rg���_:��Q D������'2EW�4��b�����ib9�.f�a��Y�`x�a<���i v}Q��4��(�s�����O/Z��v�aF�FK[�/�T��+����#Ė�J��*	��������p�X��r��~��#���ƻ�����+r�0��(�%��n���)q�w��Ӟ���)3�0��L��S��ӤGB|C]�c����K֪�(�i��v�+�$�$�6�̓��W�L�[�!�Ly1Byi�8��Z�a�nhc}H��w��#���ķ�1刽��t���#�6)
 ��o���2��]��]z�|c����겣�]L�lL'k��kˁ�+�`[�_��>5�@�k�(���Vlq�#�~��c�;CK�'a]������Z~u�=1�=1�}r���L����V3P�2P�ܛޗr�hyA;�F�C�Ǖ���+5M?�2��`�	=��	�DY���z.CFh%��Wq&\Rȅ�A�=3�O�D~��~m1� ��1$T�4���2����;0���������)s-���:;�	&F&]\��������L,1B,im�-� 8�w!�/���o�)%F(%]��Ϝ��o�@��"Ί�l#l���m�gB_T&���ca3�Ɂ|�A�w������&��c:v���ߎ\�:�YL�0���Z�����L��l�U�2a<+Ӥ�����^|{���)|o�f�4I�:ٞ���ǜ#����̧C�τb��B��[D*U~ߕYȘ~b�~�&!?""��YDUfc���IZz��ҕ9�_��P�PR���,7I���_�3�oΊ�K��c���9�:��9�g�,X�X1����T���/Rv�a���ʐ�U�b��9}
d��Ƶ����}1S%5��0ӄ!=��_	?5�/kb0���Ź4B� )s���c*[�-��x��1��ӒJ���B�3Z�0ZR�����������o��ۘ�е�W&N<v2Fo��^��
qeY��� �J]IT#b�o��m���`���D�/�g��ӯ���
�\+䕔ٵ�[n�21o�����/��,�����4.;F=ޓ�8r����"-��JV(uq�-������X6?�-n�d�iR�;���    �7����}m�_���q-t�`}Ǔ0�\lH<UdK����FImRN���sr�h�X0��V$u}|���%8�_�Q�̈�K����z#5�=0����g�w��h�Zm�ԵIc��"���o�.!;5F"[{˼:�8���wWv"�H�c��q�����-o�[K�R����� ��?os���2��NϥC�$��ܵ�Ŋ5` 
�<;��!��B٩?Qܛ�τ���T��3ک��Y�\:���^����X�2��y��Wu_���1r��T,�61���V?$F���Z�n�N��t��%U����8�-�Z��Z�P��K"�+GdsYm`�J-�0:	�.;!��j����Y���z�J�u��PL�m5&���ꚲՆ�-�M�ڎ��Q�Go�,��eb�
M+�i�D&����B�O������2�6#rI��߸�K�g�J,�Ѫ?�:��f3�_��[���|��"���xA
�2Ơ�����S��\�_n�m!g�j���qEs��n�ͫ�p/�䢃2��6r�C�׬�,��Z�t`@�6/l8�_RD}y��tMVx��`T
�����8a�V�uY�����א���3�:J�|�f�1J���u=�/��P�8���B�t�c�h��"]X�l���
)����X�!+d�ހ��Qrb6k�d�٩�QJ"���&q�� IaV�u�LwV�2	�������QV"���q�
 ?�jo�ƥ]R�,��l7��t�)�զH}��o��V
k2.ր�+��Zʰ�ׯ�]����cka=mX�B��iw(�Ym��ՠ>�.�r��G[� W�l���ո��}�����	�S5���#�����o��,����;8\���}{�pHh�ڎ�%�yֲ�s���z.#W�1��p��o�g�|,�VxlT��3��%�0v�3®~���� �5vT۹R%�%W鳘��վWخ�^��M;��jѳ �2�sqۧ�{��W�õf�J�>&36��`�k��:��*�u�'��k�����g�R����r�o�z !�jק?����D�]i2Z9V������S���q��p���)/��1���9X������`n_`f��u�\)��jȅ������?l��wňV�͏6���y�3,�Y�q�d>s��T�`�9S�-���V���6���7ր-͟�>�:D'�fjU�`f*0��|��7d0R��S�ӗ�H���UZcL���p(��Y���ڧ�WnwMٱ�uvj��F���ՠ�˛���Ƙ�@��'�V��&������vuJ���9]�}#'�w;�,V9xV�;9��RA�6���s�Xk��g��NI̟*�~O�ۮ�>�������.N9���>���J�KrD�·��ٖL6Ʃ6t
��1��NƁ�!��j��!�)�w��e���5S�1�d;oOj���
c�og�eS��Fp!w�{�"?�	�u�\;~�D�FtC�m���v��cѮM�R��Ķ0yivV:uD�$S���{�5-��`�^+~g�xr��q�lfk�1�SR_���.H�����b#��*����i�-�s�ᒟ̋\��BJ�ՖN�6�v?��J ���mj��8g/EP����^�O(riOfA��f��mM�.��9;�x�����epX ��rV�:%��Z����]��xB�k��g�8�~�*�q'��B�K�9A�ovWZ�-���1�&J\l�O�?��n�u1��&=^�UB��O� H����ɵ�V���\VEq<���js�����Cb���E��`��Q&\ˠ�~HU������B�T�?�9ۃL�bl�# �����a���u����e�ՖM��������d��o�w�g�p-��!��d�WS����f>�A���LĦ6mJ8�oܔpC5����W"��TǴr���LvjΔ$���r�7��ܣ�=SR�91��'a��`
�1Z���(_<C��Rr��MV6Q��}���֖� p�&���*̉	~�/i#�Mھ����6Y�ڴ�B��̗X(���ɨ���Ɩ��GkN1��6�es�<�ɗF�MR�iZ��y�%��z�� <���O���ޚ�|��*h�
�sL�ۻ�k����ڍ���kW�<?��1hMHg��/��Ӷ�������>���9�'��ܛZ��`��^�e�~�hZ��/{��8���1���1[���[�ۼ|����>��H�Hm7��r�CXm�3�A���p�n�x�;��� ��@H��[���|*��r��B�'J�z$��!;����,���M���D?��F��n���qX�="�el��2��F�}\������I��mޙoiQ�7���\�V��Ѝ�⠣���`����yÞ� ����0�qx;�t��E���9�rI�薯�:������@��qe� `&�cҐ���_�8�-�u!Z�l��f��:���߼Rvf�.���1f�(��ɔ�6��p؋��zI%7os�������8�l����I�V��f>�b�b\�N.�8~k |�>G>�����DK��цvNSZZ%Q;�-�/7�QT�o(����ՂݿݟNߴ��=��`�Z���O�W:�h�W�b��2>zI:m�_������o@U�Ǽ%[�%�����J�9gqI	i- mc��u�o���g�Ӓ��Z���{�˦��=??� oQ�JU��T�?�t��R�{�/�຋��p�l}|�dB08k�=g��ʼޮ�rM����,E}�I��2ك�]t�r�>�=ҡ��wB�4s^�� /Q�f�O������x)s� ;Q����S͇�\/���c��#�*�c:iZ���z�;e*0l�F��׷>|y��6��!K���8��c�lR1�N�:�IG�p�1iH�%����8���^�@m����e�YO�@�~Fq#W�y-�q�E\�p�������_'͑h�k�#�c6#�l�$���ğ����(���>�AC4��V5���������rǸ����d��SJ0��f_s� Qӧ�Ѕ��ϑ���;Z~U�A'�B���W�`��A�|�����X�f�H]�|����az*y�H���c��^l�XƤ�6rqQ]��Ѐx���W5�p?J8��yiJ �P���W�4K�6�@�P�9Ns�*�QT�W�Ps}��e���G`����d��ob_�s���Q݊lѵ�&�:J_��c��1�[��d�8��/;+Q��]}�-~lSGTD}�qUt�?Ƌ��@2� �D��L@nu*��GT&��$���o��8�jm���� dD}L�!�,LZ~�1e�g�v-�)��םF9��j���1�|1~~}PGg���2���1�!��K�����&�5ư��2N�O���=��f�1\;'���v���./l�gk�����O�|��&�#�*��{N�_Zo�Ϯ�l��~=8�3�d/I�h��U2�kw=I�p�4ɶ0�bG��L�b*��&�B}̗D�ۿ������{z���P�I���Á<�~��_����K�(����k�w}��7g�/����%�D�(��~s�K�/�ǀJ��i�5����b��	"�_C�כ2^�Cx��]�C�5����}R6~��<~�j>,rI`/ LB�!��gI�������*����ȟ;1�Q����cr���iW�+��p
�1�j4��G��y���lC}��~���J��s~��l�A"D6��FU�{_�{,C}L9�����0���ˎ194rLǟn����D��P��#1�O���(N�e�1$!$�ic5g/��X$�aCVFt���߸}��n��_�o���B��O����Q&�_>���ݍ���?l=
�_W.FD)��l�x;�;�=U7a�|°�u\�t��,LΣZ�":�>&���$>r؆��6�2������=������B�!�46��~�nh����ߧ��~M���pD��R j�#�>���(k����\�����'����C+L~A�%��}��>_����_1    %&���c>��M�������lL�a�_C��N�d�m�~�~��tn�{�-| ̗�ǜw�_T��<Z�D�|�%E�p���ǴE�����g�Qh%�7�����%e]���
8o�z[�h!��>�(@|F����q�!=�;�282�}px���t]L)T=8�B�X����0f"�_��w��t3�y��),q�k~<'�Ȣp�����!��x8^G�~����Z#�hm��_�I��J�s��`��J ȿ��z���|"��~� � �5��P��(Mq���ү��U�q���K�~c�'4?0��I��ܩ����+�͠�����g����Wt�����*�.m�N�
��,�W0��&�͇.����>���3hޤ>8G0zP;�?O‚f~d`�Pϳ~��>�=�ڸ!����Y@FX@�Ijl�7�~������c�=��~K[�˧��G�|�a�=f��9��_Vxh��>�FlדHy�;�$���K�c^���
�гZ1(0����&7��|hv%��[�%f
0�&���@�������d٢�9��9�H��D���3��e����HJZ��t��ؿ��o�x�h<�>& &'���8�w�p(
�������Ţ`�r�����y��%��c����I&%�Fz�ǜ�h'�D�}����zޥd�0�t#t���\k�Fʛ0��M��~��}�d���x˒z�}��}��
��J�;Ӂ�-���> 2ܗ7F�2{�t����3Ŵ��Hh��>�*�����~Ӑ��7�8���vWa�$�Z Eq?��%�g�`�6sM���Eju�^��B�h��>�xLr*���4�c2E� 4KSs���\��F�99�`�Η	�}x�&]�ɡ��!�gߖy�
��4)�crRq���h��1��M}��3�XB�'hLq\��5�d��ة���Q7�ݳpXbR��l�ښ�Б
���\Ȇgk��+��:���
�6o�����S��0��������,fC�}��LF�������$>�2����R��ճPl�N�KA;�j%a8h����ߛ/�_�8�_��G��j-�5	��DK?����%�h�Z�#4uº�����筬�T�l��0m��ǍֻRE'��r���(�c�
�^Ʃ�k����?�v� ���c���*G�}��20�ɏYC3�RTP�D�좶�9bq) ?f8u1��r���z>�s���&?f��T����`�U`�-����_Ā~v�5^��;`�It3�����7Ex͵]�Af����[��]�k��ˈ}��@$�MA��?1���[�L"[�c{�_��d�Y�_����j��0�q�3"jd�v��P'?��c��:PG>GN�*`�S�-Β���a�R��	��X�&/s�t�YP�!=3�Z�X����F�~y���e
t!������ŵ��'�\���16^D�%\4�k�p���qJ4<�m@p��ӗ`H[���>�Q9�'��K�al%��'����������|x�`k�c#�p�Q�)Xd����`�a	s�!�h�������,n S����#ҟz6\߸j���FW�3�3�y�&��AhM3�ȧl�m�������ݙ�Ǟ�u^��c�j�#e����q<oE&eȁ�P�e�&p�-�΋@�|�L��~��'!�p�Uo� (���u&���Ȇ�k�m��7��8F���xL�or��3%�S�c,���$1֘27>��;��B 鿥�^�Rd�2�QO���i�E���|~��x�F(��li��� A�r���zƔ���X�mup�8�t,X1`��jI��c�řI��(�FsK� �U���O�do4�Bբi>/{�W`�J�J|�3q����_�����ҏ��m�nM^3x���H��&�Pb�����e��u�V��U�qp���rOӔo^�A�G;,�5�}Z�vl��S��<��� �=e�B�>��3t#�?p�#րA+AO�U������H��}��n��=?�a��s�E�y�7Ȥb�j�3i'�\�O�Y\�#�
0j%�Y59�= �&�����	c�v*�L�*��WAd����N ��T�bO���M�L6�'?�����9��R�&�QcM�Z�"&�l�`�J���#g�B�����@���etc�r���G�Տ&��t��Qg�%㼣�~5���V+õh��ۖ�ݰ��N%�3�ǣ51�n��(,{�Z��$͌H޶[s�`���*Gg����TM�`O>�{c��Rޘ��R�R���to�/[�te��+5�6��>�@8����X�h���I	4�D�/Y%�e�1n��1���*������P�4���Ne�u���xw���l�˄c�vj�G��z��.\��U�e�V��*>�����ܹ�����V9��՜��ǨkߖE�`���rGO�x%���Ig��+E�&_�
�g����f� X��6甐���3"g��E�����>�+���+y�:�T`�vQ6��K���l�0�;=�d.�9;�:^W���/��V�%S��2�"Nϛ`̽Ďm��I���e:X>)#�˾���%J����7`X�����\N���rU�'���U�O`��F�*�:0���A�{⠜�+�y�w&�.�Rc������n���hXƸ�XVɄ��]�3�X�}�c�3�K (*.��$���O�!>�`f<��m>��̼���2܁������G9ϗ�V_Z����IQ��:����hY,a�gf�������W]��0\�#{d��z����2<�rL�嘦�Ye?J�h:��~�e���&�l�X�B4(S��<���}��~��JW�CX*3S��
��O���#b�"���V+3m2e�@�4+�,X�) ��R� �z>��y]�n��8,�K�@�x������>
၉׳�Z�2���MuM2ە�O��m��avX�9���H���s��o�Hٺ���t��_���XN}L�8J�/?�&ws�����nκ^Z��8hJk����k�,���H9�1��:�n�*.��C�>,��Ғ���?s�F) ���܆נ��-M}����R�Z��\4�+�c���J' �����J ��>&���SX�1<	�>-�0Ɠ�K��H8�a0ل� ���_��kfR/��o-}L/� ߦI�2���LE���ƭT]6	�i��_P錔����0|��B �����7uX��l�i�J��aރg�`�Fo�42�q�s��R��A-ُ�06�:!�hx���`$ϥ>��0߻G/�t��h?��>�{ 6�E�e��Lk%ۨ��%}Lz�8�c��r��m���R�#�)��C�6*�ܨ���T,l*@`��|��3T&�SZ�~��b���-u�u�~�w^�笷�w�(�RZ����UJ�Ek[�XxK���R�`����ϩ�K�Al�m���]���x�.�Y�hIF�e��ls�����Д�1-Hr߼S��Ԥ�
c苖ٟ����IE�E	*���5K�1Z�={�b�[8�����ڲHL������4�0X����u���l��΀ʤ�W)Y2�͇g��<]�a[3l�D�������le0\k�kZ9p��l�Xo������M�������L���%���`4e�N�7�����A�KJ0J���$�x�8�Ze,�%}��E��&J�iw�SЖ�1,q2^�z�$��VaI��0����Ԥ�Y�~�aZZ(��F�Q�D��Ø`NG�,x��ô�[5��2��vm�t�S�ͤ��S����T�Dy�Û���~nC�K"��~����b�
a�1I�9�|�n�� -�L�l#�q\U2)�������/i����*R�oS�m��������<���<���~I����� T��g��E���`�:+?#��S�y�疗���1�np���e�����W�uͿQ!G͒��`�:qp����~C�7�7���$��i,
����x�B��8L��wX$��!}YQ���\!3�*�#-���ێ*҂�7hk�-Dʒ>��obl���s�    ���%}L���u�����ϲ�+��17�>^��9�n��1z[��]l�PNN��A&ö�6�c̾�QL����m�"M҈�#͈�!0+� (�c�����xc�z���m��cP��x���x�־��َ�����$�7�1��<�W��[d,)��k�'53��-K$}̦n"��g�5�-=W���c>#�p��z:Q��D��@>��|����xv�|6���ي`�uu�M�7���$�Vi�:��Y<�x�����M�&�tZ1�#}��:�_G
u]�G�0϶/)���4�_�G���Ԋ]�1��Ċ�ٺ\ج=h�zN�ZR������Ŗ3�����
��'2--�g'�h�%�7�,L�K��?��c^���=��`���+���c��;z߰G��e7: �cZ�!9 ~k��͟/�v7)Zj�'��N��޳�����c	KG[�$߼��v�H^�*�!�T�R�&��[biT��-�m�*��^��d��T~5i/�7�\ڜ���z������`�PҼI����
Ѣ�?Ҁ��3r]r�����nю�!},��.��qs����/���v@��Ǥ!&��S��MVNi6s-s��i�Yo\�Ϳ`>�c�`��Q��ik���࣏+L���8���c�]����'�"}����w�1[4�{��~sg��C��
�oG󝑜4t������Z@����Œ�,^i5��o�8�L�w]���ǩ
��'xh�5�O�f�ކڄ������3-�-
��6���SV�&�.�v�*Al��7�Ď[��`:��P��� R�]{3�v{�[ګߒ8泑�~
D��4���/'�l��-逐6�B:�r{�����0�T�5@X�����M$�a^³� �M��b�<9�J����G�A���1�yM�%�?��|��~��wI�F�u�a�Mͬ��S%�&)!)��;o!V�wk��H��-�#�ld����r���+c07�6i�]�h�6,8���PV����{8�/rp�g��XXI�7Kq���~�}�5���l�� ��O��>"8u�8Kk4�c�A�d$E�% Qwx}�Q���� �B@;[�Z��d�>���m*Rq�.�E��^35�Mݷ��_�
`�*	)�i=�B��yV%Q�e��B=�����4�2K��Y1��(�(i���/���c�
���)����7{sU�����s��g)�Y�3��6j�8��`��C���	?K�+t�.a[R'��<e�&����>�N1�5�@��C�RR�q/tz-��Q��:��~�dM [�<��2����a���[B�9dj�����B�\j̀n��{��>��������*`�)+(�+��f�Z�"�3��0��
U�P��Lw(�y������$����y��wd�RФ�S�a1�@�SG�g�_jf�̈́J��;c�<d�<�6~�o�?�p��-C5fa����S��9��k��T`.�.����kS�ĳ�徜��c�qfed��4*Y3�3��(�z �/��έPP�w},w漌��N�b���%��q5	���\�m����F����e�P�w}\E"����B����*ݫ��C�Sch-�Z9X{��e3�	fV"p���>�#-���.��0��^�h,��ժ�6a����x��i ǐӈ��˭�m�qѷp��7f�e���+�v�t��L��_d�=F���������u	�O�12� C ��`��}3��L��[��#pX-��.q�ޮT�'�ۺ�x�?�VV+p����j��}I.����%��� �7]��`B����|3��L0#�����l�>��Z�=^�L6�`��7�Q(�]:?xes����ޅ�$*�dnF��ƨ� L�ƷɃ>N����K���+��t3'l�ѸYKǨ�Q|6^�p�H�93o��$cVb/]���⟩�d��ʉ �/V��dN��3�{C�i�Nd��Z֓r�9e̮H,CU�.퐍�,�N�^����
s�Z��ڊ��*�p�T��t�w�il��M�X�Q	�$3����U&,����PE��Z�~���t�q{��"���[�o�
�
}����:J��,��%�����d���`�]1�Dj�6蜩uE�̊$�F�fM��o���Z��>�
b��:/r�:�a謩'� yI�w{6�Q�H3� 7u5��7�(��"�k*;-�t�ԓq`�_�|ܜ$�e�S���\Tq�w�y��Lu�^S �Z�46�&լ���]���1p�6H����/ܔ/�c��bpP8�y����o�ۖ�b��e�'lנ`+mo���Ʋ1Z�*����׏̦��À`Ċa�'iC)M9K��Ɩmwh�j��e�}J[ǲS����z��c� J�M�b��}����pY������VhA3�,�P�3��_?�7��?W��tIe����_���׳r��%|�͍f�2��&V�!i��oJp�z�0�TH�Ka�-���Kh��˂!)���N��������[{mE����*���w���u^��`,Z5�c}w��������,����D���j�&�7\iC��
02%��'�gi���f�X��v�Ul-R@�[������1��wcL$ڟ�Xz�g�:�1*��6�B�N�cN[�'�/�L��S�w3�!?�)r5s��ğ ��R���a�pKl=+a�Y�
YI��}����vi��gY�	���|�@g�߄S�!'N�M
38��zC>��"}��~����Z�����sQ�y�A��b�J��s�1�%{ӵi?��ys �3`��*�>-+��{ۜl��lj�]����>�/9Da��sL�RO^O�2&lj�`��,�*^<H�6o�aK�w���Hm�s��'�kc�B
�:���� k��f�/�����kc�5�еl]�����t)�g�F�%�����Ó�P�ɇe۵�mwiU5���oi��m\ۦ�p�����=��q�r7�kVj�:]���s�oƯ3����M��w��ڵk	!UE�[m%[t��R�ZK��$��
�_���8!l�dJ�}(��}�O+#E{�y7�%�N#B9�Ԇ�^�*s2a�v�u�]�m�h$5�әZtd���z�C��U/=��e!��u`Lk�v\�����u;��0���*`!w���=8y����2?K`�R͍>���#��aQw=M��IS	��Ae�r����	�k�k�JQ��z�x}�R$���]�QT&i)�z��7U��m/�oA�\Q%A	Wh)�B�Z��dC"�`g-�A�R$�)�ݨ}O�cXJ��*ǰμ/uYa�w���C���Q�y꼩)T ��k-�����dx�Œ^����4ć܂��߆`Mx�5�I���X�ݔhΆ!,����4�d��Y�ܚ�H>��u"E��m���i�b�۾�q���w`�v���y@k�X^kqx����o�e�C�y��k��<�L�rv�t/��`�x���˽��U����F�(���>!Z?���d�E���
`�x�e⽭2��.~�ˋ�X~=��1,�.ɳN���l���0��x�o������`Bf?��_�c|g^��);�ay�5�ˁp�K��nּX<^k�x�L%?�!RK�/_#���69�e�]p�t^�TY)�Ө�[�4<�,CU�KP,�%��ߜ���#[f�S����D�<�|�O9�|�r��"�Z����=�K���
Co`��>�s���qdf���2�1�P)5�����d%k�,
k�k�5�K�
nnW��D���-��k�6O�Z�z�t<�Ff �ױ6��A��Ur�Nڂ���5�MVb�+��B�!��q�A��in��U�V�wi�З��QbqY���K�k-Y�{��9�\�-k����.&�?��O~�L�[�� ���Z����~�P����"�ש�Z����
>F�����]�+��R݁O=n�YI?�
�Z+��ĭ}����2�.L��0=��?O&��:������+�k�POqĚ"��I�S/k���:�!)�N5�S�r��\\    �^k�z߁�{���kΘ��d[�A�����D��y4y�B,�[��;�q�c�!�(v>����(-^\�
D�B[t�̎2���/�'����5��ִ�C_T�\�ölWa��ֲ�$�g͋t�_^�F�/J-���Z�]��Y����d$�jxW��f�R�/��_mZx"y�5���k�jo��'4u�莶(3�K�k-m�Z`�Qq�NǪC������nU�~��`�<��&����̝s.�+���w8J��Ȳ�����q9�?�w�);�1�Jk݇�c"�^�y��U�eLp�{���M�J����+��^i�{oҗ����Q�z��@s�0���+��~Ұ":�{�9�|�ր�.|�dn�j��BS�/���-t�6�i�����H��[(K��2�Z��[��'��͘�z�r�a.�V�wIK�q{��#¬������%oL��o���N���3��LCL�2B�rq���38�79&���̛닢'���Wo�Q<f�����k-�;���'��ؘ��YW�Xr[��<�������|�c���wq������G�4:��nY[h��Z���ƙ�P�Vb������U�� ��?��#]^��D�T,���b��>r�nmϖ���K3����K����v���[�>�B�,�7%6�]�m�|���mgr1�J��[���4���eo\�S�3c������ :i�J�7�<�̌Ƅ+-�o]'a��)�O{���0�T�v�'���Ӟ���4n/�Nu^��Ua7J��3��#h�s�����4XS_kM}g��9i�W��#|+�$\ZϏ��͛���PF�E���wܻU'c2h���22�)��Ҥ��+\O�q�(�z��}�:�F_�ID���w_���^F�^���3�g�|+�˨���|T�%���?{
Iަ���Y��^�M�~ڐ�7������E�Ȣ��^F�^�K�٪������MA_ҁA�|���ƸL-y�q� ��^�)�9�q=}�[�x}_DE7��e�蕖*߼�pnMF�йzoFq�$��S3����4�$�x?G�e�A��(��2]x�a.�Q.W��Y�i΂��u�x��.#�.�%���X�f(;61��(�+i!,]1}0�tN�P��۞��e�����Uf�[؆�XV!k�K1Ҥ�[���cVlf��2�ƭ�uU��!�?Uʾ*�0���!�(`��=����߀���:�N��8���_a�gJ0�efG��)	�[jj]���\�U�G|/��ǛT uk�$�H�U�����cc"�"W�dA�"%�^?k�2%���l��$^�ݟNW��!���ҒGQ�#a��:�n�u�]�'L��w��n��7��w��뷐�W	�[(]�ɱw���r��|i� �{�u�/)
��#+M�A���Rא�d'��I|h)f
0������ޛ?=8��PT�i�W�W��T0��W�p-"b��Q�W����N=d9�[va����V썫�.p�d�<35۽��`Sݯ��t��R��e���mj���E\����4/#4�.��=5����]��e*0�{] 24��;	*fB0�{�ɏ�3X���/�������.#�.�O�#��څ���Ơ�U�4��T����ݧ?P����örJ)�{�j�
sn��e����x�=w4z���R� �`�Q�eJ�qB��&`�Zː��ϫ�^���2��rI=��զ�۫T�.�35�J���2Q�+_�+l(�]F]m��q�?���q�"Ӏ!+T���/u�@��8cT�ü.����|���m����0VW8#�]u��N�^f\�x��x{��?�ZoH���Sb1��VU��kg���^>wQ��bR�RW��%bg��\�eJ�Ť.��.��Nr�����z~��]���㴕�/��q�M�^+�!�>�
�$�͛c�V8]C��*l�7��iXD�U*W���&�:�Zhf: �m�h���G<�dӮ���l��i;� ��g-�,�SX�ܲ����l��ѧ��٢��1��ݸ��~Lٲ��*�rL*�.&�O*��On��[�(���5���z��|-+|�!T�잼���-���=�ׁ��7LղS�K�-F�h�.eщ�i[Vi[a��߱S��0����� S��EgfkY텙$���j[x��*��:��y9н|���.��p��Z:G�1�s=o�+���{t-ݣ�:+ՙn�h{�f��V��JͿ�C��h'Hg�K��m�َ?W�{�ع�FB����Kpۨ�`?�Oc�:��XY=W���h�KW���*?o)�b�="S��v�]h��I�ٽ� 6�#�%�+~��?������~��P�h�ȆB�aQN��G�"5�����S��e��T�]��lt]6��Q����^Z3�d�x;��^����Ѥ]�����G������[�Ѷ�M�M�?�τ�,4��bX*�9	��Z&�v�f�+�	�S.�6�F��C��,;��۹p�G�ʕJ���)�{��˨H��`t�5�됒���>��c�� k��2���elYفo�F;D�$��7j3!s������i�׃_�$$%7����:m^7*]�Z8��
�ȍ6�t	=���o�1��L6��ã}{�/�L���J˦���*≌C�./$)7�2|�7�ׂ��EB��K.3q�x�X�Ε��Twh+�g@��Z�'7t��#��c�谄��F�D6IwhUA�F��{�"�sb#�͌.;���;Q�M���L������ٌ�_.~eY���qj5��wL�zϙ��e�� �Y�ɝɋݥ��ȁ��÷�|q 3�s^*os��ܖ�)�����o��t��K�_�TʮO
���58n�ͷ���:��-V�+-"��{*���؞sٶ����F{E�!)y՜r�.�	e6*d,7�)�%T��,�z�j��L]�;�Xƶp�]��H�w�W�������םj�-��*/�0ƅ�3�^��L4����a�+�1�u X�[eSl��~�㒡oY�~Hon��d[%�c^%Wg��G�֌vHp��lŁPO<Vy��|�8�j�^߄��T�Й�M�L�|Cc��zhڮ_��)6��V4$17�@�u��6~S�!������:'���i���Bg�����.�n�f劆�F{T6I3�Oi�B��M|����xցA,D�x��v�T�eY��!���s������73��0��!e���.�r������c��?��C�r��)]�=^/4B`3�\eC��9`!s����K*@Xɉ�"Bӕ�me���1�����L-ݝ>���@Bs��+ӻ:���|G���̍6��� �Ϲ��)�EF*$37ڹ�m�!�<mH&J�mF���1���<����{��ʖZ���BjNh�oT7w�t|�o��c�N��1�'G��y�� \�}1����'���w�>n~h�R]-Q�E��o���f�Qib�*�Ê�X���b�۷?�^B�]>����̏9��g{"���[����̏�J���)]L˰�$b�����x�s��x~���e&f�¾��I��̏9����l"�П���X�pA�sSM��,i\ܛ�n��\o�������Bl3�5��}vցL���q��l�9�ύ��tݓ%`����ei $��M=S��N5�z�W"s=Ψ��jJ��I�é$7M"Mh4���3���.G��Jn�D
d�o������p�Zj���T���h�#��T��T�Kl	N���qw�9�,���n�F�w��T��DFS��+A�vτR|DOJ����������z��&�\��J�L�d(����X6?�~����*��g�wyC5,]�NsQ�0O��������@�0'�4'�%���\1��vE2Ƴ1σ�Dq�-�{Պ�)�����o�І�����UfF�u�盓���J0�%��	�:C�D����?x�wن��&7M%M*`t�ӎ3t7AoF��J0z%���o)��c�������&�ܔ��vbM��ϼ1�|J�E���hc!<���&���~�<��ư��Y��?i���Cf�ߐ��7    �̕�Ɨ%7�	��(sm�d
�B`��i�j�핰�h�|fyc��=��R-�1LC9MC���ߩo)u��_BQnE0ƭ՚����L乼�=�9Ʈv�qyo�`G�s^�t]��J0���M4>�}�!�~D&Ey+�r!&��ܴ�;��^��R�=�Ǖ�`9�K=����Τ��?f��k�jD��J�1,��r��P|3�}Iy��/�"~\�����_��*:"외���g��͓���c�h��:<xbuO}�w6�K�1S�<�i�xDlweAy�C�]G��]'�����\�B�4��2%���I��g��(3�[�f�Ba8j�����oN�ؖ���L��d}�\�(\�G���6�p�`��i�O�¨��~����������ӼO�l�.�<�B�-~���*T���hv𡂲��Sb���˖#����8�(X'���p���?�2M\@�t���j�����.�����9�ǽ�G��f�Q.y���� :\+����0��Ie&�:A(��f�;`�A�6��Ɗ0���|�q����7���"���ǀ�i!�i��� �Ԏ����{^c�	J�����ϗ�?�X:ƫ���;n&�[;CN��;&��&�\��(�w���B�-�C0��eI���g�EO�G�B�Ò�q��IIVN��y�(�>\*�Wr?�����6�c�S����`4K"(=�	ħ`�o�p�x�aG�H��-M͎Wrn׬:��q��I]f�������[�I�I�.��9S@^d�N�}�:n�@�~���뷟�BkQ��`�jv'	���7���4l�;&�Tg�_��0p[�o���)|��ՁX	Fo��P�`����ˮu��q��In��S��UĊ�P''��O �ƒ%ͩ��\v��i�3�ga3gz0�y>�����m:N:��5s���o��m���K�-]mR����\�uq�w6
lf�t*Y׻ �P}k^��'��k��!K��������z�2�6�q:��M\v�dkX��H��,��a�~\���m�v�F7�J�6�����E$�Ls%r��4c]t?�&5��{�+�D��|~���(=jR
��H^��*����ϊ��cǒp��:���$�`�7K:��x	^�	��|~��r�h��7L�c0f�OM���?�Hee�?�)�G��1d.JYM4��OZU�e��`�����d�|�ubN�ʑ�Ҹi YK�?(D�= �)|_�N�%�o����a���Zl��������Be�L�A�=�|n�!����&�j@P �����b%�;0ʵ�M�Ez'k`�˻�`���f�Zc>i#g��?I�Z����8Z�)�t�׸�5�Y��1�u�Yr꩖�Ƃ�ѬP�k�t�Y[����}
U;4s�	v�q:�,NE�����v�����X�Yf������6OR�R��|�s�X�l�>�v����\��� [{�$��[�D�,�!-�n2�_R�Q,�y�r��f�~���������'�0������sq�(���\ɍ;f�CZ&O/s:�,�/��/2�����"o�b�*"����_�C ��!f��g_��t�b%ݦ���6�@�_��=��h� �u�Y�����o���%�����������S �u��LK��=��`V��-�K�*�7t�7�T����x͘m袖6V�d�/zW-�����eO��'6�� ���G��*a�w�?�ɽc,mi~l��;���c�d���q�<��8�r{:6J���:`h�J�����E�=mN~h�gD{���'f�}rh���~]t7�J����o��`	h��_�E��^7Һe�� �4`2W>%+AwQ�������En�%��}נ
�����;V��
�r"�P�JA�?Ѧ₤��CX:��+����LZ��5S��*��*�$@
ޙ�������m
	���M�2����(۲%¨J`��x~�'R�Y�+��J����D�a��e!�$&�b�sB|?��1���9�k_�R�TF�Ƌ1�}m�i���oX�e���Ɇj �U2��2�������W�U�l�h�x�+"��%홺�g�$�ګ���o��B@���j�큷{�c��U<ģ����G�K�$Թ,�Z�U�#�����y= ��������	��kr.1ց1m�JN�����Ur������P��B}a ^o��ks7ր�,�+LZH�D�`q2�/��`�ҹk���*ڙ
HL�Ǥ�i�4Ҍ�S��B���|Vܩ���*I�$�����:]�~J��Y:ZK Ѱ��pe����-VRϕ`0��)l�#��0�ڐ�ȏ	-h-1qe-�ݲo��&�$l��3	U2g���R��K`�Ѱ�����O��2k��1�7qz<���/]��.ß;�%�D3�o�2��,e1�.���/4I�������q�vBVa��B�2�{���=whr�$�J$4�5\&`��Vt�A�`���
�L���G�_�2���1��u�Q�s��X���Y33 ��Uba�T��\��m��S��l!��Cét�l%�4g���!�B�4���yU\�wĐ��IF����HEr����z�:0\�9h�N��P.��/�a u�U����g��X	�O6�d�
�4���2�2	[e�W�n��~��t��wǐ�`R"��IԆ�(��nE$��0M�Ѽbr:������&s�am���
9���?V|s�}$B �h�pBe_�O����������
'��(;!/�����x�������>���U9���.�:*@#�J��h�BA�`�lAusiߎT���SYt���@�[e��:��o�E�X�,�*Y�$f�#~�c���f�b�m��L�	���pG��>F�0M�%��B�"U�6��4]8>ؐ�
5���L&F��Ґ�'6��D���:��k�׃�i�#u�fS��uX&j�kJ#�1��3	���Xl��B�8�<G9�������)0wA³'�C�A��R���;5�x��ȴySJ��Q�6��޴���BX2F�L����@�p�?��ey���b�r��Q8���ģ�WLMH ��b̶���������R����~�<��X������k��gM���tl"��,w�~����I�Aq�[�p��c�S��j����5�_z�Ɛ��*��$�4��H��)�d����3I�I;�{�y
���k��W%�l���<~G,���0�5!�b&h\v���2ve;B?f�3�~�'� �,(�}1D��g���)�� ���PT�	˟d��A�<�}�I'G���7=���'z0n��g�f�D�>\y�������0�LB��C��:�����Ǐ��׷?S��$)�°$��J� ���^҄����Jգ��`�j���y�8�����o )�UR��fZ�s�RRVvxAN^����N��$:6��j�����V	zՌ+�L��zJ�������c���S�ׯ���j�4`����bV��X��>u!����j^E�Ə�{����fr1%�Wo���O�g�2�ѩ��*i9Q۩��I�x�]��3-�ʼ�z0ŋxT��j��U�]�^�5��zy�� +J`b�]��;����amɤZW��U��I��׈3u�MQ���Z������6�O�W��_�j��d-�v墷�pl&��Tj��0Q#����+�)L^2�^���/cP� ��;�ӗ�q�Y�>���`��1�f���=k]E�|��yJFxJ�e�/�>��/��3��JS��Ŝ�*�R��B�f
�΋�8�}V+�]�b3%��V"b]��L��L����JF�J�@
�%O��7�/��5��/��ʷ.��Ҭ,�����u��Y���ёd0O�Oɘt0�S���s��4�s3-�F�
��#��M���+ ��]B��WH�7�$d�W=�@�17W�p�����a\ɫ������Z�SX���~U�J��z����^����G���5i�(��L�N��]�LS2���6B���;%ZwI��mxm��/!Y���:����/kW��eV15G��    �yQX	ڊ��g��6aW\��������ެ6v_�dSN���ю��-k�d ߧ&&�cf�;���s7�c�ڃ0ʄ�
�XX>���xE>�!ߵ��L4;:��&s������/)�5X��Q��'�#�t;�qd��I:�N�= ���'u��q�./�j0�t�_⿾����Ois&ցa��nh���K):�0�j�UE�[��^k#���Zlke�"���0�J�d��E�Ů��ٯ�86uZAw��0j!J��0��'���O��_�b:�Eg!4P���*���7�!��>'�A��6�M=T�t2	-��~���|e w�S�tzp��$MwJ����W�#H���j&-2���k�9��P�t�:2�^�Al~!_�S�t���;{�^�U:X4����q�����' Ce�ܨ���c�#򦻩sk��҆8�ԑ6�]�Uk���t�RN듺Rӫ�"wx�0V�v��&Ep�O��m�E�!� _���0���v<�VtkB�t�t�ւ�;+��2c�;�K�*��\��s�?�s+�1~km��v<��b���N5��O��O0]�:J�F�d�|E����MH��,��>��] }�S{�$���-|F�\L��,��@�W?���g%y_�� C�G5�~Et���4q;�7OK \��_D|
�.�D�y0�BG[���Z��=\�a#�;�<��`��(�'? ��-	��Htv���D��+:�z-�l���Q��#��٭Y�
]�^]�*�蹀��a�P�:F���)p_B/�W/�S��r単%��c0jL���)��o~�k�G\�����/ވ}�����>D�>D����}��te��C�
F��`&�ޖ�o<Iꅓ+ӀQ+D�Ƌ��,Yfr(d�v,#U��	�h��?c+go�����PW��Ӻl|�������al�,���W\�:�;�j^�7��<��`$kH�}Dl���T�<"%C��W��J��mzod��.R6����:0��uh��!�>{��E�C�CSO7�<t+G%tzu8?�@��.��
z�
�.�%F�ȍ���;J����p�M��E� �J�
6M��
����N����؎��!�d��50F�����LT�?�Y�~}M�
ꤍ���L���P�xX8Ƥxu�v���r�mX�e�.�	z�	j��H'2jPA��-�=�z��dy)tƖW�y]�^]�z6b�SKX��W���X��K��ݟ�Cލ�ƫ���i`��>�1���rc�ʰ�I�(ͱ�)%X$ƦT^�N�� �j�6sL�Bct�p�h"�FP1�,̾pb��l�ė�S�ɟ���(��`�e�slrH������T���+�,��ȲJr��P�٪��,���`�6Z!2��TA�'A��-{-���y��O����Ã�@[�����ײK���8��NėA�\�:�����5��lN��/�������K��d�C�G2>x2=�Rpi�9��#-�BV����Y�M�������߹#Lثu�bi�2�~*�L&�h�+�y�/�u��Lkf4��|����:,�����g��h�?�ks^0V��+C�����l7�2�\�ZpY����ۑ����2(�a�e?Mm�&r�F(��f4`,cUj/�$���$w��67��1�-{�А��vW
,Sk粋c�:����c�>G�n���@���R�i�:�����)dS�+<{M)¬��ڟ>���ŮMYl_�:�!�6�y���а�).���^K/S"PXR��ez��+	�]�ZwY��kD����yx���/�*,���Il�X��Q��Æe��Nj��� E�c3��W�;0p[��'��`���{���X�0��3ܵ
���o�c���]V1�c �J������D!������1{-�l(n@	�;_Hx�g��Ř��\�x�C��L��4}B\`6�0xS٤� �ׂL�ݝfQJO�����E��aZבNb�w��Ԕ�de�2,��3=�'��+��ǐ��L����tG��;�e�:���^���t�Յ�6,��035���[��9<[d����~��t�bL-���x�(s�X������U���ٍ�kp��[�ڴIG6��zD"j}ٝ+7{�ܤ�oLs����o��t�s=C��_g�����h�Ʊ��a�f�U�MRv@��ʻ��0A�5��\ӂ�JTs����:ק0
�7{�ߴ.v�T���Ɠ�V
c�o�CA����%�찆nX��kMg�D�%�3X�����# �T���8���YF14m��=<Y�~˵���-��MҞ��L�]F;�ˆ��?���g���~�׉��H?�0W�b�Zo�\�Q�8��8�5��v���MS���,&���$V�a+s�lnQ��H-OVc��
CWK:m���7�T\�L2ƩTt6���ힺ�tI�e��1l����nb��:�;�m�W���q)��b�B��_�*0Leʂ�@�n�<e�3��쵎�1�%Wc�t.︖�;�e�;pUg������,���A\��O������g0|(�Q=�y���z�0O�UXV 1�e�M�����Ѝ�)ڌ|�L\��k�g��'x��7Q���~D��~�tѤFp<^�[���P�{���дw��KT=#��P��k���RP��i��rW�q�k@{�m+p J���ϟ���R�T�Zj��D�5���-u�&̥2¥jb^y�i�s>�=�j0�^Խ�(�~"A�]e�_\,�k�(�9/��U�w32uQ^W��Z9�t�~�B'E�J�\9�k�h�'�tP�I��
7��c&ʚ���k�*�[�Tu�IƋ����Lƴ��\�K���z��e��|*3񩀗E��y����GŸ:���q)��22y[$�c0���ʶI+�I���,#��Ё�X�XսˁGU b��e�s��r���VTl��A8?\
ˎA�Q�YFYULR�(�֒L���,#�,�|�%E�u�1��+H��,3���S�Z0O2%���jjp��A�����T�N�{9�g��.;�3��0����c���lL��Y���ϊ�-�,W���\��3}��c������I��\�'}�hْ��<-�-���ۧ�����;��q��5lcҖѺ��S�Fz�P,y��e����X�hL4���vL`����A0�0~���*$�&��.#�j �c79(�2[3��2�^ƙJ&��?d�]�u��\F�\�\\U5wI�eu�0҅�մ V���W��:a��Q�L���.3�����܎!u'�"ޖ��-�h��~S��`A�"^apY372�=K_�4��e��U���}��w�Y90k�4
�x���ө0byg`���VM18j�����z���e��μ���ȵ�3����h'��ֶ1F�0��� ��� ��T+3��O{�Q��x؅C��	��3����O�ݥe����m̴2´�c��8�͏������¼+#�+�4� M��mQN�`ƕqJ�e��d�H�����*�]�]��,�d>�T1Q�6�}e�6���s�J�^*P��M���NZ�P���~!�`e�`�D��~���u�9�"�p������B
uew�X�X�8l�Fԡ�CB�dM��UF�U��=�)W&�zEq~�yUF��w�Dr��`�a��N�DH8�����H+�"����)#��&a�Ǜ��xM
�SX�{c�)i*1i,�YG{/x��t�<!M�����_���)� <Ӣ����Ğ�+��W�E�k�������^8R��es�^v�3o@�{�^���,P88��r�6b�0[������%�zecR��.�IN�f����e�uUf�bv�vUL����"?隟��Ne��}�*\n.)H鱒l�0`���Q�]���8�9SF8S5���2��%���:�b1�'e]�|���VT�g0M�M�iR�|��=\ek��ח�cl
E�nemhP��@%�`֒KƐ즶�|�{���n����z08� e�8�p�(�����aYb3��6�O����Dy6�ͤcx
K�&���%��>�/{{�P�I�	�k*�Z^j̅2�ŉ����0��P��O�X�!��Z�ә    �Ԣ>�51]��f����E ��MF�MubuP-y8⎒�-���jj�'�'����|J��7ZYY��t&�k�������wXz�z�)��e��j2Ҳ���oiGI��%-_�z�d�u2C������kU������d�]}�A���5�|X�yd0��dӏMoϔ[���d���Pw�`�͛-��8�S�4;��u㕌�r�a��Q�����c��T���ӄ,T�&�͚��C�"����s����	[�z���2[ S��P�,�"#C8͊A�MFM6.�~�Ԋ���b1��
��N��p?�4&���ෘ�d����dũ��t�n���0����O6;�Hb/��Ud�X�b��2c~ɤ��銒
3�l�������4G�fƬ��Ӗl�s���lݏ�T���\��e1]�N-�c��G]\���
 `��������ԕj�y���2�,f(�JCBqZ���6�,qdyu�!n�DM
'����L*��,�!Y�!q ]�j�^v�K���GV�G61E�]KU�""���#z\��o��*��OD�Y���qCъ�i1g�j�yn&3�;M飀�^u�B̖CNxCu2������PƮ��Ed�F��'��F�,�j1�Ți|Z&~�g�|jWD"��Y{���q[��o{�CuI��Q��������S��Ώ�B)L������*ٽ �D� 01�0��	���Otդ,6/��dF6D�U	�]T<�Q�׶�{��"'"�r���,ۣ ��d��7�WkF�L����.hJ���*��aĐsZ��
g$�^��E^2�a���P�̲����]��n �9 +�1v�Y8��CN�C.�V-D
%��gDF9�3`�����'3ӕv4	փ��)� ��X��Ȇ��'�0��)��u�;����V�ACN@Cu1ӓ�����1�ABN@B���s!±|�!�aB�
�����I�����c�\��I��P*dm~��J����0�=�l�0\���Y�|��B�j)�@��������"+��0$�	$��5s|�\:�L�䩷G��7�4�l���t���gl$Π��u6�jm���1^y~_4�ㅜ�<�A��(�C^V�at���f륓|;�>�4�҈c�`"�NW���_ǻ�ү�(F9AU]5��!�,ܔ�h!Wk�8���yɼk=�9���gV!cY�u׎���m-�u(�o^Q0n�)cS2��.
٨������6%@qN����)�.CD�4�*�)��W��3�I���vO�OTVq5�����Ɩ�(�!~�4<�h�O��q�C���8"'8�:�~��4��s"�,�_Wa���69�W��ȣvߤ�C�>���a��kB}5ys��*"�/銜���s	m�)�D�OQ`�5Z��*&)�KK�!�G`�VΧ&���'�����x$'x�"&���w��e��������6.��U�qI�U4n|Wx���m8?�#y��Rr�R�Zq���U�H��Y�J(Cs۽���G��0<ɵ�ޚ��/�?�-?�]���lt���T%��-Ńd�l��&�c^�c�7�S��r�[�h�9�N��6��D�<L:������2�b�D\O�m�h��e��8Kr�"���ݥ���B�0*�	*�LXHf�����j�m*>)�m���peѰ9Or������pωY�vɋ�1|�	|�H�<�������Y�cØh�P���Ow���R0JE2�H���$3_���\�QJ��1���2JJrJ*��^$r5V������
�</6O�$�Kz<GՀ��ʃ;�Pr���e�,s�<�9�A��08�� Z��S��� �Ô����2�R$�N�@�9�>����K�39���I��T�t6��z���yû��W�+7m�or�o����ڧ�^�XT���69�65I!C4�	��7� [n�T������o_i�`6��aH�H�K��D�����lƽ���`+JK?!*�0��)S[�h8��D:̳�)�59�5������0���2�[���n�1�]��>77975%�@Á���MX霒����0cS�l�lтzJ�;���ʌR� ����0g��?�{�QNn�� ��q�֙�t��©��߳
Ǡ�V`@�Q��YPOM&Ὅ�G��f%+{��D׵ �k�
�Z���;UvJ��{3M���JOuiN��)�Y	�
�� |�t�+�9��l�qO��������i�O�:��[�w@� ���>�H�U�@�*�y���2��;�yCֵ�¨�JQO	�}Vx/�,']aT��x������X��اJ�OU2�5��e�8+�����%U����M�ۭ��@�J�Pu�oD�}��"+��yzl��´�����x�S�0m!��������N���O���4Z.������a0hrf������;a�;a�#Ay�&W�����w��Edv_�uK���2}�*yؠb��	�u �:��dt���v�YGegxbȻ:.4��ą�NbS���ezIu�.������#�:����a6����@N���Ba��3kDޖ�9�{=�w(F�:ޫ���2�Մ�j?���j,-	����$yD��3�̌Xn �~���ez?m|.�J��}[֜�X6�RM:��C����:s#���Y�b�4��@�7*���T�`���9d5��压Ŭ�z�j��`��j��� �p�b��xl�a�h:3��HO�-Oaت	�ZQA$���>ȣ��s��Ŧ[����r|�c��4E{�����̰	�b�	C�S���,&��V�e���>y�klk	B`��A�cE�!����M���_��2���Ux�4*�p�W,[�^�Z�i6���/�Q����u?�h0�Uzl�͈�`&L*���M��O�7ZY�3,[��C�O���*HX>��{�v+xԲ����Y��cj�X:6_�#����+�)䞵�a�U`k�iQ'�f<di?�e�Je��P�������"F�Z&�߄;[8�ꍈ�D�Z�]�a������jC(�����̵��	ng�
�UbA��I"�?�z����i4+�/��a� i����4FK���;>���w������i��Mi+�FVIx["Aev�;l�F��J�Pi��1��!tt�Y�e<��䇀�ذx|֥�N@�����&�@�sB���CM��d�`�)ܩE�����������? �v-�݂-����_g�fSe~l�--Z���6��mu��`������O��w4�e��C�)/sD7��IRlu�iآkE�%3�B�q�I�QBS��o!u�ᡅ�Fd���i|B9��C*/�f쯟���C?��7�">tlh� 2�^R���&3X��kĥ�2F�����ˈY�m\r *u�y�ɻ���	�c�����v.6~o��	����Olq��!�e�m�h�����%5��ݾ�t������4��-['����IF���C�Bt�hB�#
�lH����x?��O��L�Pm=4�gş&�������q9��S^fSN��ƽ�ذ�hl9m���=��z�^�Ѱ|l��7�f��x�<�� �,[j�1�;�.�����7!���b�p?�q�nc�݃д��aTa�l�.�ū�fU�5��j��5a�l����؋�&	��ܲ=5X6�V�$X!j�3�D�$��'LnE]���dO}6��-鐡�ϻ~@�(/sx�������)#�q]��f�I��Љ���^�sy��E	J�蛄5Gö�4��T./��@�A���� 
����y�:D�: �J�]��u/-�UflᢃNu����V��ď�K�I'�C�6{�v/
���JE:�8Ђ;\�_*UFg���rC��Ƕ��5�Rm�3���eT��0�K���w�27J:�X�"nz�ܾ�c�}�2/@��R^f/o�@��� �Ac�Ea%ئu<hr�J�T�8vk�&�k|��W:r��W[�3ܮO:�X�"� oXo���î�v��_�M:p���D$;�Ȳs�^��    ��c:Z����y�[S�q���R^C�^�ȾxG*��Uf�LZ$�&ʐCɃ�%g#���R^XP��4P�*��C"Kyٔ�g*�Q�,��؊uH�ܸ���y��E�F:�h�"������t�`��NM3�	mV�d�s�Geɽ�w� �A灖.i���	��BFy����6�ۑ�ef�BG���z)x�|���vN(�/��v �cm�e�f�7+�0�{��R]M���c<��#XΎ��b��2h9��
v�ȗ�!��-�83���\�� 2k�Z�`]Ѐy(�D|������/���ahRK}�褠X��eY5�:�<вX42^�j�ּ��	��O��I'�H�w�����+�*�=���"���;$7K5i�l��@[�Y�q��7�}��<����%�&�OMW'{�l���0�UP��*�a���[*�)����� �j���ru�g���~8u�������7��M<ؓ}Ct������C.a.��u�v���o���;�1��a�0W����`#�ɞ�ڕ�!T�تz��[~
�n�a�))���y�:����9�ϔuA��v�R����z��)��<w�P���xg-J<�sС�%�� �u��So�l��~��-pҌ^��T�/[�P���}�U :8An~6^�3=��L+VYj<(C�th'�|��(x����F1±�*�^��T5ӫ�f%�1^z��\W.V>��/�ߗe��C<�e$R���K%j������Ȳ���)L2�	eQ.��+�3�e������n��*K�^/�?+��?�e���v2��ÿq��D�5��������2?{l�"$$�p��( �D���nX*x�7I�����2=~2�Z���.�<%Dz����,�H��~��B����?"˴��G���0y!, ��O_��4� ��z�B��/���'UT���>>�������	�l�6��3��@"3l}�Ea�� �Y��E��	;�\&�`Õ@�MZx���2Qd�
d��v\�SO�e�}�W`�`�K0�@�)���]V~�`�z4��0
�Z�W~6b	��قi]����A����e��E��@�It�"NGI����+���J80��F����G��'D�eY�MRI%�_虄���6±�
�n�`_���F��v�_'لU�!!@�,�/H���h���[�P�����L�l�Q����@̲,�I��w�Ⱥ�LR�؜�.�Y��iڇ�2
����Cl�b�&(M��-��)qE�N4wÂ��);!I�f)��e��'��Y<˯��>�}#�]Y��� u���ʄ�Фڥpl���Nw;�����m��Ҳ�*}w|���v�PԲL�*9�5��'^�7�����|ol�2��O+���qG�� y�
ZZ�9�aC�p\c��VdbC��.�&����o<򬙯ۮ��UTeqz�/�;�4Z���c)�x4)0�:� A�e�A@����h��|�\�O�����ǿ�<��T�W�`3��Ё�}���C����-�n�d��|�nh�W�c���a � ��|i�y]A�hYf��3���2q�~{��-،	����d��J��K�#)C;x�젎
�g��F�Oi�f��"���I�I��[ؤ��N�I�TRaGh��]KD���TH�]��'U��Z+"�)���F����ol����h[����((�24��$oHC~��DGJ�}���� 8";�W4�KMȿ���H��z�@r�ّG/��{�����eiԤI�@�0�Q���#G}�����S��:�dZ3��o�55g(��C�1�/�s�K,z�:�"�J)��"���Ϩ	=s�Ʋ��h[u�1C\�6u�v�{�JM;���q?n_��{1�Wjj���� JMAID��<��e�ߜ�UزJm݌��z�<x��a�+a�+��J�����3�oB�],�\E:����P��-�C������:��w!s1@m����E�Y���s�t]xA]R+ό�[R�v�e��%:o幱mK�u���c���x��N��_�G�kb4���,ʄ���{�0��Q;\���%��]�$=wf��lX�]�"���w�i�O�#��:-3%s�g`����d�+m�]zt@+�6-9�ouQ�n�����R�E�Ω������a2|z���envNy��Z4�T�~|��a^������:��=+ұUW�P�a�DC/\�#�cᗃ���m����/\�|~xlǒ�(**L��W��"#��(5mQ&�2�������ϊ�3{۬6{�i�ԫ�L~�~��a��4�7�(D�ρ(���*�P�����K2E�B�N�et�3�q��Z����2�x�YU3���\рm�ֱ}�o���S Ⱔ��S�LF���&J�k7^�
�e��Nz�Wh��xդ�?�҈aB�ԄFR�#��|<."+b��jsw��-�j���e�6i����bc�`c��gF�	�wd�2�;/PGMܲ��ձ���2JKf4o�aԿ-ˬ �����\>>j�e� 7^��&yF�K���ږe�g�������̐5k�2'bG�#��;����%%Ҍ�t�띸��_���i�]�%��lYfЙa��&�9��3���&��f�q~-Zs���[���8�j�*��.���[yzl�ҍ��iӜ圮+V��K@�ز�������t�)��g�\�<^a@�&��*//��q��Է�6H���,�)��K�CG�͕�-M+@��,�H|O�&$k�`ۚ`�@��4��&����DC�Flӗ(:��ή:ͮvM�<�e����1�CGV��,�+zُ��+n}���@��4��&m;{��mr{c �a+^Q���i��Mz�$*���VG3�mE>:Ȝ���${Idr�V���63�N3�M���y��:0��=̿�2UU�!��8�l�x���l	_�,&��x%oSc���J������w�yq�ք���y��̆6��<���{�N�܃�=��YR�}?j���ԙ�	[r)��$�����?3�����BgsX)�p���nY3��J^Yoq,��z���lF���LW8u<��5�id_ц�]���	�{&��"�)�X	���2��H�郷� .��V6��ʰ�K޷I���Z�G�R�%�3��+��#��͘���Ƽ�me��X����3>�t������C:�MB:��eMwɼoD20����1]B^E�'%�h����Ըc_-��QN��D
č��ք-�r�Mj��66܄q���2� ��W�W��[���M�	|��c Qp�n���uF�N`^��i���;!25�ن�?=L��2Y�O�d���#�<8�ʊ�粳�T��(�j�˄�\`jm�x��g��L�Lm3P�ua��Z��cgeM�p��c�<7S�NS�i�;�:9�k���l��fmz��4yN�~]@���v�T�+s85f>Պlܒn�d=3>��I%�[�_.^޾�p�[���7|4L�:M��U�{Ĥ�=���vy���//����DP�ω�W�iU�?z�_Η���a��ӹNAi]��Z�+#)�2��l��ln�T 1��d�=��:���?[�$x�.��9����{�����7\�]�z9�;��04�8Y�MT2�m�H����̋>L�:M�IG���_o��`��u<W���k�A��'��^^��Gk_�]�%�` ��a�)ug['��S�Fm���D��Dp]&�XO%���b�l��}`v�)mg�{�?�%5T�+�[�d��$�5k9	�d��\�C�I7�����H�mD�05�45�hG�L���{fv&��&�;�~}��L�*�v�'`s��p�����4���;I�a^��;e�sT�WP?��t���;(��,�/Z"�|�$�ޢ�l�N���o��)~0/%��=e�̢/�.��k�6�2��>e��ؿo����N.��2��o��#Y߯�q�0����-�m�ǨRO��Y&ʧ�*S�Qu�_�yJlʭ���C����� J�۴�z�=p���n��ʛ��,s�{va���    6��F�m��F&j����2�����$P(�nD1*˴i�AN�����݈hT����5j��1��n�[��hz�֎Cd�������p�ļ,l�]�4����#�mo���UT��շ���,�S\ӛ�Բ�Q+ʰ�w�+ �>��4+�y=��k�M3'A�$H����b/��FeY�&��\j�%�զ�(l�����BvH���{
"�er��M>��K2�	|+Z�������=��m��8`؂{���7�V�̀�rl�y9CD-*�l��Ӫ��X5������;�ը,Kb�zգ�Sk�"�EL���?
|�����r�6�:�p�xGe���4�����t�k�6�He����:�XS^��2_8���=~9���ӣF6ww�`sk��F[� q��2)�-�W�m�g��Xl�BE�%8����%�'�';����Q��2�[a�1�Yj���|'��a@I�|�>$۝w�G\��̱Z,�#�Pk~�1#VRY�zZ��|�y+�:) ��4%���J^Y�ҕ��|P�	q=}_5��,v jRYf���}L��%Z#���OeO?<wV|ņ
�9���x��CCo�0���@NRY�z��O�ў�?�(bA"RYf@���0�#��ƌ$X��v��4��p��z�(���'��h�JH�&���Mv�tYWn�� �,��d$�H�;�CpZK��5@3V2ҦI�~n���J��ݧ��h��YIe��r���2��r��D��>ݮ��_G�ɏw	�sb �:*�|�������2�{�53>V�@3U�ѶKۓ�m�5��r�ʲ��N�}q������l����]L�$'���� Ƃ)�hSI��G����r��2��Ҷ�C��H:*�r(u��(�I �u�P=�䃜��̹���a��p���A9���ez[������:K��_�iA~5}��" ��ͬ��_�m�/O��?o���tT�91�$��:^�T����v+A%�U
���ShDI�E�*퐈T�9;9Vr	��u�F�n��f�a�v���:�gN��$��VbTW)]�<��%~~҈M"��Q�����]�z}�g�5�O��z�*I�"l������ _(z� �p�%���
K@0����ˋ�0�t�}�v��7V۞�����	7+$'���$g��"K'��a�җ%�~�~��.r��`dXYiV;��q��7=��削�a�\h�A2R�gd`V��f9���
���0��W_�0��-��Fw�J@�`DE��6ޫ�5���ۊ�b�R��,�i-��,��r9$c�`����I��֙��[�b��2����t���r`�Ĩ�RP[]Һ�l���B�����$dɶ��M��{�0�� W���k~���ޕ�R��Z�>���hx��.�Z˱w$y��{��������vY��þJ�}%�ݕ��9��5s�V�
�����ؙӦ	`@W��cIcv�ąhq`�
$�|`l�u0V{��ņl�~�)�x����"N��q�r��U� �D���>�X���>{����Ve�ԜK�%��|��O�\�dlO~Y��i�I��x�EEˑn
��  ��[���;"��A��j)�����p����y�d�`��T�@*T��ͮ�}�и1:�TtT�x����_��f�?K��*�Ǎ�/f_P���̛�� )��~n 5��k*1,�TXT��렇B�y�F��ʵ�k���h��u�ύ1P�`�������^������L?���P�=��v�3b�\1��SJ���M�����y��J��ц���[c�����n ��%�C����Fw�?���6�uhf�!Q�B�Z�#���-q�I؆U'��H�tR�J�Լ0l�����W�Py�v�9��C��N�J@S���N2�(/��8�RqP�5���>}~��$�(�0{Kۆ�c���'�m��T�S�|7	"�6RbT)Ö�!;DZd��N�m���*1B��߶H՝�˒ ��9)1,����^w��BB���n�U*,��a�=�8F>�Z;���f�X��{x�/����@�I�Qfgb+�46N�ொK{�h�"/G��R�@����J%��q#��Cc�T)(�:�$�ݹ�o�ύM����|�{�$���9���`�R)0�����y���2cSblR�k4����8���*1 �@Rӥ�iZ���#��W]�P$Z.hz�t��q��!����p���JE�	��� �ʜ��!	Ш�ӷ�Mt*�Ɛ�)1ʨ4�M����)�sa�dg��(���R@GilN_���/��~�qG�\�ćI��n��lD��˷�8
�����s,*X�Uɔ?n�!)�f���yJl�a�u��������>�C�h9H��ɭ7�K@A}BO�S
��,\Jm�����R�AI/�����Ѿ�08�pPW[&.��7˃�:�r�J˹�@�k��f)��c��0Ӈ�?ɵ�Z
���$*�^�������'�6�(*aD
�6�8�ui5�� -�	��%�D��u'�rYoL�$d0_�N�?9n�m�ua��X�}�������%Qs�cXz�5���2�߮��0�����N�(aM�}�L��Y�L�b.wk����e�ű=�0��N�>DuB,U�[��Q�IEK/�NA����� N�n�|�+u��s�	�3D��=q��om�Xg�˼�bw����UI���~����LJ�*�vALR�연� ����+�ů �Zļ����ҭl*���e��-#ވC�"p�~y^�ضK�턄e&�ϰ}֞�u�v�Ƽ5/��r?�G�iL�K���D�ע���M[�5�eBAt�)5{g��}�������a��4l�%ۻ�,1��A∾�\��$l֥�u����t~�*�g ��.��"i���θ��ɣ���V�O��:��l�%�uJ�k���"���[�Q�0�&��rv�-�{�.�9۾ ��e�! ܖ.ӣ����o2ʼ_v欼 ��e>@/]��W,�4k:�5;9�#�����8Z*7c˕Yj+���oX0�1
��:6֪K����漭	@Y������`l�Ng�Z��w o].�6)�`'<��}�l^�N���ǖ��rh�3V+�R b�29���GkA�/gGZg� ��ˤ !�>{��Gw˷�Z���@�u籲g&M��w�[rOӂ��4@p�2�z�޾����e7��\l�2�)�K
h�~ɤ���!W�D�FRa��P/n�k�%Wl�u'�E��x>�л_�)��#l�2��N𛬀�F���\�ؐ�^��x����A���|(Zx�؞+��O��c�/ZRظ(�.��I��>�%��nO{�UCD�q]ʻ����F��؀k�&7�I��� Ȗ.����`5��{>�`x-]�W߬���G��yWn ��eb�{�1q��Y��5�f�V�|����0o7�����b�}�����8��� ���� ��|���0~K�t؈� �K����FO�m���o_�W^@��2�ONiާ7�S�$���F6�F���*bG��YZӄ-W�4�D=F�Y��5�~Mj5g.��=5��Õ˙�kz�%7l�u¡�?GG���k
�!7�|��.��a�W����a7�^!����M��y�3 7�e���(�����x�1[m~clӍd�� UZ7N��+�;� �L��c<�|���n� b�������]��0���2?~�$�tLL�5t��4��u[;�)��g~���� ��˜A����:�{Y݆\lʭ�I$�9~P���Cr�y"���2�T���ݚ l�m�%�h�?F�ķQ��1������NZ��t����坾 L��,��ӎ�nDA�Y
�1]6�����z�����B<����~��P)ֶ�˒��=!�-��%[Ԕ$�/"KvZvb`-��8�ش5�Ț�\���P�ao=�I[: P���Zt����`��&�j��):`�Ȩ\( ըwuX���&�ٞ�BP��M=gbv�\��d�`dyNk?����ApP	�!?aK>2=�e�&    �����}�so�?�H�MC]�����c?,��ۧ�����$L�4�\� �j<�-K�>5�ц���_F��Ė%ŗ:��O�+Un�Yũ%Jcج�ޒ�Z��տxڔ� ���W�+��� S]��q�lY%��&y��	߰lbR�i���{
�Z #�Dc�
��8����G�=���T����������s���_9J���?�b��b��x���^on0��ii&)�p�Q���|z:����"{��O㝰�<�ۿ��neK6h�ɴI����$'e�!��Tbb4f+���l�,ۯTa�6~9�؏��&����J��S�ӝ��%okSCX>�])���D
�|D~��``��|�S' Y>Q�0�{���B��H�z���V^�V^>O��������(�v�՗2��x%Tp���t�9�&l�Rwi��=2┹����-��8����6|�0��-,�&W�',�8-�4�R�m>G@���_��_�Z�9���&[pեS/%Wl�����j9��o���M}�Ǧ����jý��`ᗃ��y�,
1������J��JJ�D�^��t�q𞷍a%�i%������~��>�{�6R�H��%ݖ?�hlR��� ��|+�<ۀu�u���p��$k1<��-/K.�<\��c��*�r��l3Z�X<��E�ƒrcM8�)y�IU�xQ{Y�{���ʇ��Gy�����d�������Ź��W��/���=7��Fs�	��g��"sjM1?=>Ҵ�!��X�Y�����K����U�6�o�/��B#�2R��6���w���K�F*5���%ð�vͲ�!`Å
F���T����\mp.�kNk-� /�v4B��j	#i�z��Ϸ����j�j�:�7�����c��E�NRc�|�I�>7���JŢ钙�Π~єƱpl�R�h �e5&�#�7����X�pZ�h^C�+I2���6�VѶ��3�LK�>��!���s%�: �2Q��{x@\(�»��Iؐ���@�<��&y֢ˋ$a��i��k o?9��V���!�4�Bi�K����.1p�-�KW~
hk�e��#�d��$��x|l~��=�B}g�N����Vv]��*��h���k2�Ȯ˦L�D��\s����׏M��,�pJ�^x^Q�����̥���8�5 JU2/� ��L֐PU�8T* �&Z�x[Ӂ-�ӣ3��<�~I�Ԓ�rM>G;>G�%����mk�K�]�i����c��G����gw1��4A?�.�Fo=(�l��&H�͝f~�r�����0W����]��6�W+�My�n9fm��c������azy�C� �˜��i�o�S��e�jt��[�)��&؛'B�*��F06�^󯱏xB�ߎL�z:JԔ�A�.�-�N^�>��Ìv����;��̯g0�����CV5
�֟���2�?
�)H��R���ʂ�y]f����2��!!�ߐ��vp��-�&���r�+���a�*`j��tY�h��eNN�F%
N��l���e~���yg���?�O9ʔВ�dM��Al:�vO�:xϞ%�\������2#'�;�S5b�'
��!���/F'��A�R����/ď{����u#н�����I+�e�3ҡ��e����N1NE��|�s9��v����Fz�2�?mO�i���_�����I�I��{�������}9S<4�R���$��J�7�GЀ�D�@;.>T'#JEÜ"uY�h4�C��:�����U}B#<t�C�&�)��x&�Ē=pM6��R!CU��߶��l�C3-�i�f�|s�q�s���\h��\x���[G�hV�.s�#vX�����m/��@�v~W�;�x���Vy� 4�C��e&�f��tw����]�����Q�mR��H�t�&�xY�2�ͱԔSb�7Wu vI鈌yW64`{,%��y��)�+���J-F4K\,�	��(8���T���y����˒�<OG�������ۍaF1rx�Ma�O鴕���	���8�0ԧԶ�RN2�ApOiݒ�-�9���|�8�ʙn5�"��~�tz�P�=�yvl=�t=�MSӕH�v:�"%(�-؂�E��M��,)~��c#Ҿ��{�،��j�v��s��uY�!�����u�>���ړ�N��	����߲��X�Q�̕sK�{�����p���)+�&ک�u��c�L) ��f*�)X��3�j$�eգј	]f��3#�E�>k��}Eʔ��퍆�����F˔Ҫ�&cX�~���[}�~#%�FO�2W���N��j�s\����fjE��ѷ��G>n`���1��MZ�J[���q�W|B#'t�Fq�OȄ���)e�ָ�l_�c��B����W�8ډ[�(��T�ycl�� ����^C�z#��@�R�<]��Q�D��hìr�9�˒��v�·��W�`� O�����a���.M�	��B�*t�d���-
�r��گ���x���a��LDNUͰ�e�f�ɳ�ҋ>+юF[�@4Ҝ�������F�(4.�=]�q�C��À��Q�j���/z_�_�	ņ�h4v!��B\^]���e��ƛ�yC��c�s*QւMV[��p�k�DhT��zJ��M�j��;��,��ה`cU<O����%�0�����8���4�n�i�]m�>Y��v�\,N�C�2��v��s=�jB�4���eN��ѡ����<XoaY�m�d9�vu��N����᥵��m�d!�5�rY�W�T���g�������t�?������1�^�,��k֢�hM��-]�||�������,ۑ�4X#�eʇ$s=H��v~?2|�Z2ˬ)������+�_�ɪ���lȴg�.�ӌ����+�]�Wd�ԥ.�Iz��4|�v���Dc+��e�$�����[qɱ�&���.�$-��z�@�+�J]&��B<���*ZoU�1���.��I�^�)�L# �����~kJ��*9eB��JD��4y���.�e�!�n%���òh���lHa�/��c���^S��Vz%�$TfLYdڬK��5��~%c:��W�^�+{W,b���
��L�?�o�@d���"�Yyt�8�eM��O�a`P�k2��J�tH6�?����%9d�����K�&,IӮI��X��x�B�wq�
ʚlĒ;��<ҋN�=���p�����XR��3%D��3��Ň��؊%y�2*�N�?F�;h�F�cɡvIJ��LW&�M
46���LqO���QF�r>����m�B��J
�=h�-��^���<6��e֐�b^g�;��*m�%��*l��>�'��*�'�a�by�^S��[��}� 5����utl�
l֒���J��#���]�º,�e⚮�c|��e�e�@.X�9��`�����P���"��D�.�,�������V�	r��̗����W@X����t=�7`;�|pߥ�~t����	�!|��e���ߗq��M_,��+~$�u����&������ʍ�}u���'}�������=xM66S�.��!����Ŕ����.j|�Z����e���Tщ�O�[pgTa�dp�&�V5饦̼;�l�.��I\�ׇ�H�,k�M� ����s?�w�K@�ڃc#�p���y�!��ը�؀%<$�Ԃ�i�1/Ń4�.��[?f�+Y$�uY,-������_� N�Q^��Ns�4M޽��u�l/�~��6�hN6_6������º�aD�����Y��N/29�!�e�v���m��4CVcgj`3�4��/�W�x0�������5ؐ�c���I.�2oe U�˼���mU��Or�E* M��&V�0vf�X���l���v�">*�Rv4��2
Io,��n��W�L��H֕� �Au��P�=y�*�'��3���lqqV4�����C�u���L�:�{�x�3㉆.8&]��A;�.��H�k�}m��b�n��ɱU�eJx��+V��%�(򒍠	T��cX�t����_��H��uؤ["$��,ޏ
D]���V�<�$ߧB�D�%�e`�V��փ    HD81�7��K�6R]�`:~�A����)˼:h �e�t[�t&+�T���J+i�TK��o��R�)��+h&�e���o�g�PrB��Nm-t��7��霨�|�2�G�������*M�]�|�ЧBI:��i��[�4�	[��I�������P;={�/��q&�ԁמ۰�@.�g�4].Z�)�&ۭ4��i��kZ�/n�H�&Q]��)�&A,��q����Bu��s�Љ-[;�fM.���i��v��]O�=�[n�*�˼Q��T.�1��l��+���N���̲g}�G`;�aʥ[N�)]^t��r����v�T����8ᗆ��ї���{9O��f!�-G3�I��(͟m_�����{3�+/t�2�a���������݆��nO]�0+>v�+�^�j���I�69hF*N����5���N�^leA@w�.���״(���Y�(�ݩ��n��&���2�<u����Z�1IZwX�<�cc[�����߆D�y�tp�2}�8L�Bb%?�^���vM]�-=��N��w-J�K�6)�}�����\�Yr���FM���z�z� �rݚh�ڴ�&<B���s�y�]Ud�P�.�fOR���\}�JjT@��>ή�o|=I���z��,�Y��bO�C��X�5�8�[�o�}s�Ȫ�ؤ5��>�[3-.�P�o�>+�D횺L�{�pC叼�,����e�>���&�29������B��M�,R���g�:N�3��:+ُZ:u��z��hH��E:�	��uؕU*<*!>�=7�a{,���#i;�eQ��.��'ݛ���dyrԼ�˜j��6�����_��ȥ�j8"2?[�b������ʰμ�5r�2�(��i\�aF�O����z��S��N����'y���%�A����oAՉ�V'8���7��T�9�#S	�.{9��.E�kT�٬���=RFVR;Y��I�˜U���e��n�Q�.s<�.��mGۓ�]B�źʂ=.��p����Ǹ'���B|��TKz�5��R����G��_Qo�`P��.�ǎ�?:w�W.�F46�@�����)��̘Ü�Gth�W�eK㢰,P�"+7�ZGu�=YѧZ."��_��1Oe�Gk�Et~�OWI�&�6~F!i��w2�?���.`k�`�-�ǌ��R �Ղ�釴������c�`Ϸ�k��ԥ���i�\�su�f��s��M�)f^��j%�oT�' �!9.��?�(.3�h��� ������	������t�>Ʒ��/��{�s�Q�Kj�ב�9)bx�yz��k����u<��gUy�ԏ��|Ɋ�h�������Ɩ)��B���#u�Zό��-4��@q˙�K��5�ش��e�D8��~�|n��2J�����	n �v�~����5���e)�D{���SY�T��>��w?r�ʼ+at��h���(1%�pFc� �s�6�0Ps���d�Ϊ��n�q�0ֻEO�J�W�T����RC�
��_�t7+�р����H�%����
d��Ƕ�(.6�%A�V�����V�8.~�A��IFSn=9��F0�e�b����
����֪0�d��*�A�k�pF>��F��q��?#����A�+��vM�X/�.��rJA����)ވ��Mڜ�`8%���i�Y�玗��oJ�$�cPp=��ā�рW�7q1��aN �.��_bN�*D蘴��ⴁQ��XHه�[\���}V�F>�a�߄��j|����,dR�7����,���\���M�m("���nn�I�l�
�IP>��Je�㿯���wN��b1��[�(�Oc�'{�3Z˟��8�Bq�byp����g���h�R�8��7a
.,��ə7��X�8�9��;���ډ�\��q��ejeo�[�L=ip�n�6dA夙yU1�����b0��t�0�g<p%��Y7��);5���)�i:����<�O
��J�b�N�)�=�/}��]���o+�;� vʆ�ܹ݇�b$��]�uJ��t�H�OHN|�#f�7I��6ﶁa;e�
�Q�z��V�w/���W⢔�����]�@J�O���6��g���_`T`+�C�GT�x����?`�k�6��E::�,ިX�Y{r�����|�l�~b�9��.1L��$u^́G��-��˱`��)eN�]�W2]CwL���3S
Czץ$C!+᪼�
�̔Q7�)a���À�R 3]�|�Wf�1X�T�Lr��¿o�ʩ ��¼u^b�L9h�5�-�/'y�+ʹF86�A��Iۙ҅'y��0�gt1ʥ�KJ�@�礟��˪`�K9��t}k[հ��r���2T�W�Ī��)�:pq�ʋOm��4N�͜#[�"[:E
w)�헃��DC�r���������y�Ф�1���n>.R�hh�+�1vHE2����]-u@�q��CѦ�����Mp&҈�'�+4_'~o��Z�Y�v�]�a��~힜J�u�'�F���%���!�aK_*�`'`��+���X�,8�_q�_IO��ǚ��u���'!�������Ʒ���� �BI�M����+�0�c�fs�1��{z���>�����?e�v�٧t�6��9�oU���p��idE&���e���{���'==8�8|Y�Q�2;����Jv�ѵ��ڰk@n�)SY7$�J4�����	����A����$t���c�2�j���1h�*��r�j<~v�Ŝғ�I�➈�ȊZ=��"�唛,sD�ܨѬ|��#C��˂��Rj3�Z���Q��z1^f�M�!���=g�4�����`K�$b�œ#/�:��h9ziM>�Sa)����R���OS`�TK��^��Ʒ�W@���=�\!4�)MY�̐�Fcn�J�[ұ�*I�`!���������p󂰥
$/e->g~7�e�O�-V0y���!I=@,!�����#v��J�r9%j����@ ��d�r�3tDxᰏ4��
����y�x�ab�t�u�AyN9���49Wt�}]�f;舯���+��\ -+u��Ћނ���V,p��-��t��;�@��=�eݐ������۱?����)�c�9+�o�,����>H�h�H�SƎ��n��˒.�]�̯�?�t�Nk��L�=˸#&��y��D������R�	���Sr���3jHR攤�K�������{Pk�e�ѱU+*0θ���j~7-�&��c�����^�:q�����2Ib�ؐ��,��_����鸱�`X6e�(K�E�<F��E��Aي� K/KJ7���y�76��-���� �ҕIM���������R>6+0��H^f���m��uTb
|�0o�r���һ�l�{���)�(�b��}�N	�R �r�/��:mخ��,	U��f�%��1l}��e�!O�<���P�acOA�$/sW�Eh�-[s��C!��Cw�4YF� ���!��5�1��C���H��锪�� y��ė�z9�f� �β5�?>���8���	[rPV��8�h�ξ�W�����ΒAM�Z�]!��n1
E��X3=�,�2ߋ-��I��bX�|lSJ#Vطr���R�w��5`;k�Y4p˷��L� natJ&�'Me�H���YB�3��	���Fk�M�4y��] KFrD\P��&�l`J&V[t�m�
Y���R�u5 h�}�����!b��8��=������/~Ey�0:���D}�7�w'�/:%�:�^~�w���P|�5y�t�N�M��Ŧ��_ip4�/���$C�u��C^f�N�-n�۳��uy7f9t��+I^��D=_/G�Y�B�@�e�d`%W��O�{¦�|`	�����`�:���]�Mbq��G��!�IƴZȄ�C��_bpd_;�TfZ���U&���*�r��|lf�r�%�]݃6@�C�!ᅼ�1cު��;q�]���0V��V �]2fdVr�Ǧŕ��c� C^��rr;{o��������o!�N	���K�)����b�U`�U�β���rϏ��    ր-Vфv�>#��݊�V��RnOѠ����u@�!/�~M���v�ߥ�d��p'A|!/�f�Æ�0�"��]GX6�^g̔HE���Q��0^z=��)��O*���}|�.|�ϋ�!��)W��#�3N�T��t� ��>�P�5��4 H��%����~��Ty�"%yy�#��:6lM
��>�n���y�� �^�Qm�,~�|"2Ur-'����N��ʿ��׫�`�e}�e����#����s��>���N9��*v��#h�q��Vq�N��:���Ԡ���!���A\�m� 2����r��Ż�]�}l��{l��/h/��æh^6E�
K�n/��r����2�)eX� *�����<66�Ao��O�e7���k��= �0zQ��4o�a$�9%��L��ΐ��a%�qK��:�(���8���5�HF!�؅��x�v`���Ei�ǅv����[���ۅ�H<�e=��i҄��e��	eo�J��%��0�npR���b�8�(�����DSZ�JܬǊ���$@_�A�i�הH�@ar0^�}�Ru��u�Z�3��'q����,��9��r�uDO$�ge^IV���7�O)��!9(sY(����bd�IN���(Ig���F��<J���`�m�:�A��N3J��x�H������i��6����-TaH	�k�ަ��X8�NE!v0���j��-�0�ak���b�#���\�����lG�%|��?������UV"s�9���E�y_�m�!�I�,��Ѭ��IGg�M�=m|���j�}+.��]ö$H�*�a�0�\aCE?������@���Ԃ�[����>�6�<�u��Jk`�0^fc�,���w~$9��y0
)Іu)k��ݕs?��Z��b�0��am_�4��M��)�dO�l�BJi�����69�IĜ��q��ɉ!�Ev�8
1���r���Kb��+ ��@b�0�K�/�����ea����KLxGE͟�3:K|	�#1Eu��g��>ǟ�/�L1�����PC�J�"%$�O>?��d��h�lc�ot��T���������^a
2�d]|�Qe�:(E�n�*�b
2�d�;
�ϗQ!6MV�S��@A�t�K3�^��}�Mc�M� �M�ŗ�S���d�1�S*��Hv���(��%M^" 3���H��n<n�N�:J?T����@E��J𽛮F�ʹp�	�d�B��	�D� A�����4�[�P�����v���)Y:�d�[�l�"H� lH&�}�?�㏤��r��r����w�r�4���H�xlX�V�!�?�'!0�* cb0��ʤ�����%8Z4��-��x.���n�J=��L���)���o���-�O��Qư��k��JT�_��U���b
1�b}���7n�7���GhW�=�)��&h(UJ��J!�h�����]��+hV��9�k�j��ϴN���zQ�ҕI�4���8����t�S�q~�#�<��yWC��Q��2����O�w�rfu�a1Ē	=��ׇ��Ϊ�b1^f�0h����ؐ˓��Te�����&�´m��&�؞�,
9Q��Ow��)�|��`�n���u�)���ud`�1�b��Y�:���8�1��TL�,+�Si��|s;a�mu,Q�贏9��zr��)ݓ0r̻'�Uw������igXr�݂/�tX@�)A C��,�&sJ2�v%�N&����I��fӋ������Q`�U�w^c�������W�?�_<�Mͷ<�(�X�ęO��4<�/�?�L�6���2�IEc#V"�D�_Ӄb�������֬x���I�m�D<�g�$���E'���8eK�s��<�Sؔ��*��uލ��,e���;O\���%؊�(�?�����ʄG,�p_[�3�:[6��X���������;L������(���{{;^���r�O�ݟ��Ƹ�#[6���_��o�Q�L:��ςM�&���lcԎҘ-�AW�)�Ca38��%;��]a��J�S�V���l�i:�����Qa�^�ze֌�W�Z��
>92�*��$�=��e;;��F])OJ:�"�����ñt4WʓҺX���^�|FxR���A�C�!{�r��(�U��b�ُ��C�C�f���BHR��~�����EC"�l��O��5�/�H�4쾉��疚�|;�PΔ$_������Y��N�+ ��R�4���~s��O���Y��h�P�FK�@5���Q�V�MWPm9$�2w��v�8.��YT�T�Xg�%$���~�j/��߀-�T��;�����L�al�:ҬI���)����\괤����Sg�U#^�����\)I#���E%<��,�d�/%AWI靆80�;����^.�m�N[�{=|zJ�e~ٱ�?�ۯ�n�mC0B�)];1A�x"Dai���֩\(P��?�NU�E�J��-��F�í�Pc��@�%��������#�"[�-ӵ�,Ab�����7���6ʒ��w����V�xl�N�ط��MZAM��f*���MRLlG}ދ�؃J�O�Ae~�/(u�f�B�A�T'}�3z����nC�F�6i����x�\]��A(AXM��:���C�@&����r�T�e�.���FW�6��:�1����n�'�zh�iX(��J�?���ĕvV�;~)��Uч>�q�9��Z���e~�Ik�*Pg�z@�/�NiJJ�L�;��	u@�D��$m���u�ǟ�Q�j p�Rf�Mx�'�y�B&x�=��8hm����S�M��DX+6o�[�B$��U���B\nZ��ccpDW�Ƙxx��e� <�
%I%R俍���y~l�JP���/SY.)� �l��x�y�I6ة(��`�5���'o��4��ɇ�^��存���{;�Q)�I:6:fU�s���h��$�6/��maJTX>6Z�f�Κ��t��~�z��*���^�t{�� [?D[T���Y�(�2ǜ��o۬�,�d�b�"9�m��oۭ -g�m������^fQ)�I�t�v�U�h;;����Yw�Í)6A#	�xQ)�I:h*���7��A����$�@S�XW$�v���ܯ>�7�-I�2[�L�B0v����W���J�N�$C�m����Mc��ZE8%���x�g�#�8�J�NҮ1�?)$h�c+�C�R��F���q���� bjUA�.g����`�*0��?b"J��i�KW�P��}�����K�%o�����6� ATJzҖ���O�h�<!�Ҟt	�e�U�(��3!聗�F���y��	k��l�JxR.x�d/
�`D���v�����|���l=76Y�;�IBP:���繣ݖik�y���KȏD��z�9>��9<#�
l����']�ә��rm�K�A�C�'mR:��s��1��K�
 ����d���j���G�p+���z��Ir������|�T6���c��������_��v�Z�"���p��ՉbwY�X���h��$f�%��J���@2�J�L:�t�R��!�p'Wy�����$M�FB�@�~��t>���N���D`Y����&�P	JhsW�N�X;rKŖ*舶J�|�o7y����a��� ��LiΖ��5@#b��I(�����M*�6i{�*N�$��[D��!uX��A/�RuҌ�j�U`�t�x�А�;�іp�C�J�O�&�.���OԵ�:��0$@�
�Y4W�F���T�v�%\eL����ҷf�/ �IObP�ȿ]&�|�n��$A儚����,�?��Id8{g_�s<n)�ҟ�m�|��ܝ��$��qb�JiP�'e 8��ye�\(�r����j��:r~o{;�CK\(E�I�Cb?�o�� ��T��
l(M앃�g�b�����%����I��C�&a�6^4.e1i��H`�'��tg�0�I�4&M�$o�4
�P��`J�*P�$��$Y��r_�zV87-?>V    ��K�A�k��o�����[�h��.�c�RhP�]�Ts�ٕ�d�0�IhN��ʂ�@ǩ�c3��2�Ӕw!�y��,�ւ-N�B]�v�Y<�#����p�ʗ`��J)N�:>��AZ��S�ϤR>��N��'�F|ri���+�eR)�Ig^hʥ0"<�>���b5آt(�o%��8M��Rb�6q�O��T�Y�1D��$Iv�ǿ���V),۳�@7���Q�+G��<Ǉ�D�m�&7/�� F�����2앪�l�
$j��ś�����9�x����1E�t�fO�ǜ��$���ʪ�c��*��$��B/I������]+�I\�xIf��30+���"�7i���)v��ʹ-.�l�.t�F��v�Q�GGѫ��wy��aGe�����dh�<�����7�h�@w��-��2��9�`�0�	/��%R��f1���E
ӜT��$�ޡ,*��*�X��a�R�8��d||��|��`΋�1V��R�̼zp# ��D�AK����ص�;��R��3h�Z
\0n����+y�4�]R�6� �H����2�r��@[����� +V��}�1K���'~��MhAϛUU�T/U�zqq#�zy<SB1*qK`����1���c���np�hyBb��r�xsIXj��PP�y(7L�R)�K�>�Tį�D�J�˥
\.I���(��9�ǥR�6�<n�G���t0�s��ΥK�كr�T���nB�����:�T�kw��r�c��X�ԧ��G��O�3���>�
�1�%�ݻ�8*�*�c�*ȴ�������!q�f�f�aJJ �)��Kfn$&1gL��1m�:�G�\�q-�끶��Lnp{b���Ĥ���JDs�jK^��J�&&3��b5��`����E��|8@��,S��F��aJ��#C|��E1�KU"\��hZ��.��_���%�u6��T�<�)yz�ϊ�����`*e�I��+W�u%�_�рm��;n쬔�&�����+¶+0��Z�I��#/��%��J��*�DEL�t`I�EC�����d	�x�2W(`����QO�������	��H���&��$��i����e��/�MZ1PM|�W΢O��p����v�j+a`��u��:J�8�[c$���$�Ye)�f�T�R^�hk>ya�B$�ѯ o�l�^r+����w2ٶ�����v�����:�!���B�l�a԰��4*L��Vz/�l肏j�EX�-r�Jz�h���1���S.ev23V��]0Sm���%{*����:��+h�H\�w$s�~��3��*(-3��mԁ#@B��x�x��f�������G&S+9C���؝s��lo,�TK�^�@����wO��cu�U[���%5fi���!�܏���M���]�e#��B�Ԍٌ^��8h��J��'��<��S��ZK��m�Z����z9�p���*O~f��k�_����yD����5��N0^.^tN����ɥX:�gN�x�j8=U[��
�����II�+�w��k�?�����B����i=s*�Ǩt��o��Q���)�B��o��*\���`m�
I�)�m��[G~�r�eބe��'f~�5��������z�I��w:��GluJ�@���Xͤ�x��t��-c�uQ^c9LT>|'Q~�=keLh��^�DS��{`�V΄.�J���Uo+˖,{�E��@o�~���V[/�8�@���yv��{�b�DX�@�\�P����[~V1y�d|1A���H_nOX��������0^�C������_�@���	{��O�V���Y�t���E�#��eR��f#u��<R�Yƴ��2	E�r�]I�u�#h�`�����y�\h>[��T7�e� �T��Lo+#;~v���n�lo~�x���4X\���9���C[2�9I�p(@��
�ײMhX66�vi#T�p=�ž9-��QXL�g��iϪ������c���a5���h�(����@�jXƃ�a<w	ښ\;��X)6i�J����6T K��L q���� �����SsT�U��u�L�+��(��ށ����ޞ[+,*�J��&�/*��-����2��V����U|kP`�:X����Ki���80���%./°�X+'��BDn7t]�18�Lu�O�e�ˎ�\��ܮ�ۃ�ԩc~�q��b��m�c���,q,a^6ܚn��q?]�/հf��S>6,�2�m�$���ؐŕ��Ug��Z�,[S�E�t���į�C�X:6�P�[Ly��?�ɏ�m�}��t	���J���t���x��?�.��"���C	��~z:ٺ��"��W+�@ק-l��������J��H����	�����������`���u����o
nv v��`X��eV�r�j�����߰�W+�A�e�(�}Ug�4X86T�����W0��Q����BKX��Ao����:������c��\�̗�1�?�����i�M�% ��x�x�x��h�;��V�84i��7��|q���,X>6X��u�#eD۞�(���v�2��$n�����pX��gꂴ�d�	S�n<��2�X8V�M�S
�E3�Yޙ�1�E��1a-�T�� 9!����E���QX���Ѐ�)?EFGSG���T��P�6�a
y��t��d�������<��$�LOkgu��i�^���m:D��z3�+f������A�����]�뼌���g ���������*���j`P���A-��odc���d�\nz܈��NZ���
lX�{������!�@��]��̯���Z���x�n�N�3J�튥56v���d�L �H�:��.#2hb�e~>�zx��(z�KSW+��ۧ�
�ug��w���4!H+PZ�A���X��j���6�R
]�J|�ǕO	;!��\�r	�-�#ݜ�o�o;����\�\�������[ s@=3ğl��܀��ۤ0�U���;/BI�n�bh�Pր�!��]��=<�3T8maTa�U�$���.��x�Мv�����/6����	ԁM �}��9�_��q��:!H3��E�ʻ�%f��2���@�N���λIB�:���쏑�i���t�P+s@�޸C�R*�B�s�r��{��`�bv�:��	t�&�n��[f�� �)`��4�U]滅6�b�0��l=v�BL��	��r}%F��rA��i��1���t}���<F��vJ3~6hK�9�\������;g��hP%�\x
�i��j�H�pUZ��w��q1ҥT�Kr��~
���FU��8���^ٖ�UKLP:����{N�o�g���V�6��x�u�2e[6$b;Җ���<����E�T��S*��i���;��<�K����&���K����V���������a�����s�R���-]��hg�t8)�J�:y�Y	K�J0�%������I�7�ס��D���{-�]��0�Z`�!���F�F����''-���ouO4��贱��;��F!���܌�[4{U��l�_���9P-:Mf`����r��}���Mg�5��(H��)S���]���?�-M���8�~���m�@�KŪVe���"�VP�&���#G҄�aM2oc�Aw��1`v,9�&L���$�i���"�褰Z���x a�����(x�I�4 "�_��"V�_�O����N��x�(�69�r}�
'h���-�Q^��%�
�e�A��	Hզ� ��`�`X$�1���c﨨F�
l �`��ӻNȟ�gN2*i���c����@�����콩ǲ�v)ۛ�JXFI���EL����#z�@O�w��
<���M������c�t��)�^,x���a�6ƚ+�_�z���˳Lamt�W/x���CF8�Q��b�Tr��30���`����6�Ua���XF�j.×[g$D�6������ib7���y���i*Okg����"~+���`lӸ��:.:Og�ȁ��F!�meA��{06ے�a�4���i�U�9^��=V��SYv���    �?{���D���e�J��S�p˴�V�j��V��7�� n�p����5	�cB^��6I�S �1��X��U?)�E��I�r���A7�5+�FY� �~�0���x �Q0l,�c<�yo�@�|��m�$��0}�.ۭ+Mf
k�*Pئ�o����lB,�� `��Nt:?��;�a��Q`Caθ�4���YZ^a�[� a[0�� �D�L��+���&l��6hűĬ��,v.'���Z���3����I���K�1A�I./��s�(2�Nx�gG��-���֬���4J}�䡭d�IP��2�l�A%�yxD�e��*�A7z�O�O\���E���M6[�L�d�c�ؒ�۔v�U]��`�؎���=��1��և \^�-Ue���#�lֶ�q1�2�s���sd��(�mcєO<܄Y��Uk,���p����y7T�m��b�8��6��v>+T1��\�(��F�l�6el[�GJ�3Q��T��R�����+%o)���t���ӿ$��e�e��S�oG��?[%V�m���.3��m���Ұ
���{�2��u��\��1��`jU$cuN[a5��6��M�E�Yg.��+N�Z��F����ɉ���o��E6c��̯�f(���%n�kz[2w�y�
m�6�bNU�2�T7y.�ly�H���a���h�d,9�-ڇ+F�K� �F��%2Zk����I�+���r]T�(��qq����&�����nP�m�|��s3��˕�y�-��ǖ*�7�T��?%��°�*�M��j��Dm�NU���`�U��&�¬��<V=�֌xl�:,�%�?ƣ+�
��r X�Q�nS�.>R0�q�X�RA�6�[���.�l��p���x㉆On䇣?�lŽ�W�9]��y<�r>��NBz���}|�OL&D��39����!��Q�oC�F��Gzz}|�wi���F�����d� �ؠ�-��2�sB�l��.~B�|>�x%�^#�eK�j�5��-�E'��M(�\��f���F�]{wm����;�!p�	��b��)rqY�%� Z�	h�*>Q�W�:�������7���ȴo��;��ƮȻ�@l�8ض钧~p���R��谭&�V�d��a��6
�m[�'}t#�� �Q(l:��~<OG&�\VЎᰍ�a�d`���w�̊XO|`l�g���F}���cc�ƶ}4��A��)�����a]��0?��9n�CT��?+͋ѱ��cZ���%:^�c7�}`pl#�X>�sG#�����BGإS���),-�-M��Mۼ�u�3]��N�h�FѲ�t����*�M;
�L�k�	��
 �,���Є���	S���N��x�A�5��c�+u @�ϼxJ\��2�	�2}=�Q+�mV�hж�&��u�ۃ�u���)�vE�.#��l}�����o��7j�j�iұd������Y�!��6:w+�_]6�u?�KF�}bTn���v�Km�񝘽�JÛi��ʕ{.�{��ӎ0.���\H;�N�I�Ф��t7���B��?��Ţ��*�\ҩ]ϙ��c��&�s~\F-��*s��*>S ��A�C�x�)cάo���$���8<�{x���w-50���qW������4�r�����=؞qӴ�E)�g\#Ι4K�ؔkS������c�ζru���י?+�:��ܡQ��L�7u��Y����T`4`��Mӂr�:(l5/���]5:��-����{�3'��2Xx�/�~���3�����tq�ګCy��q�i4`�PN]6�ܓ�q�c$cc�_E����Mt����k�~aE�b��.�g�d�k�1+ob����6�*�mҥ�ЫT��NC��iWu	:�Y�1*�d^a1<�������qQ��ӭ�n3�ί�fRlPf��&�J��6>~�W��3��q�b�?up�}<�-���0x�U�����
�ɷdn���,�6�y��̫.�&����_�}�g%ؔk��Q�v��|�#>u�l�JQ�h .��	Ǧ+��p�7d/�0���Ы6IoRU� В�r6�wՄyWI��1��Zf�N1�F�]51��q�#��l1↖����6�=��qVM��:�1�a�l�u�ON�3�M٪�y
g����+>#[�o�������>�nDͻ�ačN����H�q*�.��u���0�*�)p��nt������t�褫������a�h��(�1W	u/ l��f�}�F]5� �u>ɭL����A9:���7:�29�C��F#�۴�u�r�t)�gXGޭ�ut�UL+�?��ر�\��s��sU�h�&s�ǂG��
��<:�-�}��9���W҇5Ϋ5��[��G3|�O�k��׃�;:ת���TiEg��F6i��KxƂ
9		���0�G�Z��C}�cTOg�tN�(h=��a���Z�mj���O���zn�>LW>��4-fD��C�v�6���m�qY|���[g=�n /z�R��nu���S�Q�c�����F�ӻ��jsu�)8���grǃ��?���M�J��1E]�xMzƑ�^F�e,�[(��>&q.���l��Ju���˩�Ǽ��7�]h	=��\z���c?������>�V�Ҡ�>��i�m��-�@y= �Ȓ�r���f�7z�m���c ].K��{�>��L�G���>��]��K�Q�M^{m\\K���ÆJ��'��O��/ww}��p����*��-�Eu_����p����������x�>���c�I3�=kq�F<��CK�c�J���B��1x
,Y�#�0n%�g�~=d@O���_cW�6���3�qp�CcK����(24��eߩ�|�V�/J����٦�r ���>�9V+n���[[0���%#Œ����P��K�B�A�6���~�_���1o�"�q{H��,�4$Ʈ��86ȣ�q���c������3M� k������m���R�o�D��W���u�:���e���?$��cvx����D���]�~ ƪ���DU{�I���h,���*C��`��xѾ�N�}�����j|����?�6?<�$�gf�ͻ6.-S�����ǳH��$�y�.�S�6邵'6��/.�`�x;˧�������h�c0J6�&dv�v�G��Ov��d �1�^�����rᥥW���M:Ͳ��ƿ�N�f�{�9A>P�;�&�a�fK��T�.Q�e�E����TR�-�1�:���[-���b�j�~����ț��]�X�4<Ƥd-(��^�+�
H �c�畲'~��n��5,��>f(�E��"\h�cK�c�JU�M¢�8���v�>��E�r~ڒ)�_I.����L���z�xO���<	�dCVr��j�ɻ��xIA}L���F��;n�Z@��Un*��cX����S��u�7��c��j:��f���,��e���#H��&��Uw��Ǽ��R���G=)�s�>:��c��x*���V4X]2�޵`�ӊUQr���T���>�_a���������u�düSu9`��J�
;Su����>�K6�`��an!�V�����K�Ws�.�T�V�v�4:K	���`��� ͨ��K���m�[Y�גe��s�'��,��48��dM��H�D�C@rQ�(v��H�1P�m��_�X��i"��x�[���ɋ�������jx����w��h6��	@fQ�����0���^�g(?@FQ�/H�w������q��+c�J>�$
C��~�~���y� ���ɡn�"-�#w[����9D��eUZ�vh��1l%�h����z
K��Z��C}���x/�No���9�fSwWYD}�E2�t�Ж��ޠuN#�!�c�TD�q7�n����௜�d�1��q��LB���;|<^@W�������=�����]���
������r׳iw�k�*�h�cف���ԧ����� M }�w��6�1]�7D��W��H� ��"��%���(    ��=����<����*3ջ���.���1����^(�|�����T��c��b��&nBF/<֥�@��>f�,NQ�/���)�)���ǒG�&�J���t>�)P���y;�Ǹr`�^kW�@@e�>f�o�_�ES3�_&����[��y�p�N'i�Z��f[���>v��>�k.}�y}�ܩxrd�(Y��H -���|�JѾ����[ ����LCZ���I�bpܭ7/����-� G�
�[�n|F%��µ7'�.��(��T�74��-��.��Y��q,j���Eh�;W���Ua�y��	S|�s��T���}̗�0���9$�J" (�c�@��)�o���y��`$�
��e��~ߥt�FZ��H�j�8����߇�tb�y=�z�K���\(O,|qÃ�1��M���c	�K�B�_�Q��"�}\�]ن�7��gwɉ�ˈ�Kf �U��n��G!3ڀW}E�W�b���>�����_�v%C�����N�E{�ΐ7�X� ��T�
�Ky5�$q����OJW�
o�IbT�CT��A�5�+ȧ����URA�������+�n���A̍RI�Tn$��,
)mѫ�-�H�m z��������HvHA��,i��M^&�44���������`�4���}#u}�bt���t2���>� B:�o�oqXX>ŻcT+;�{�{*;|g	��\�w�1�[�)+��%@Z�����������P�s�%�ڏ��-`�<�&hkc���K����`���H�@�)���g�D:����.���H�@����p�ޙ��W��H�@��X&�4�h�1����)7Τ�/LG���"�c����F���F�-zx��0��((�0]��:�鮹�ϊ1%�$�Ďgd��5u�:�,��A�+}���%Mw�;�}[�AB�XB�ѷ�죈�z��_ޏ�_�[U��ց>�)�=)+J�8��m�w��eW����,Y!�����!�����J,�Izz�Na�菧�8��"Lh��zai,bUi>B�H��)�������UCh33Z*�S����6}����6�:h���1�5CRS��8���kW^�_F%ʑ	#|�}�B�����'��Ff�h�K�`{������	�!��JZ$�3�f��H!j��R|u����?m��i@ho61����ÿ�F�(��I�nzݠ�KS~ �$�c�I\X�~~��9�-`����ڟw\�����a�}^�VJI�8*�:��,���WĈ"i�$�q��˦�n���� ӄ.�yL*B�ڬ\�p��1�Um��?��]y�V0n�>�':�b��.2u�&���Ə�q)�9��7+�`@�� �$��|��(_]r�ۥo��(��{E�A�??�Q��Oj�_�L�Rئ�F2��-���2.�6(�N��y�!t��?��ә���@7y�������Nj�"R���Б0)K����WNe����v0�|�j#<���Af��7�2�n"��R+H2�$��@ ��;+�A--��ԀOY��d�kKf0<U9��<:;@���k��N�]�,o"L �[���LhS�ǫ�/w�6�=��G���%]�����5�#2�Э�3F��D�} z�Ù����5EV�8F��A,�-8�m`KAD�.IQ*A��Y
"Ҿ�����+[,�0�$N�7�҉��il�U;ƪ뱑�����Al�ժ�R�SDf�(����"xW+,�0s#�g�R���v�K�b�J��K����Qek0e���uږ;+DқCZܸ�au�	�mY~A�Kjk��FW�b�/�`�
��/���N_�mğ��%�AA���'�ovy�sR��f���۲�&��W��o�Ж2�!�b����b��t�߀q-E]�l�nƷI|�)~ ƳTH�;vT�!���H-���^��r�,N�z&>��EF�$��*���%=�w��F�VI��mbᡅ7K��b�0��,"ބf7o�yɷu�;,�0Z�x]�i[C�,O��J�%[B�tG;��\q�
���BtI2&4�9�dz|g,&�Z
#R�Q�r�Ck�"��#a���J	��m�����vn����	�}����B����&�1߭��2�5��AZ���,�C"�~��ӛ$xQ���R	��]C=M���-^dݹ$�H�jС
�K��K$��H�1���5�����J��F��蒖����A!�pM�7W�%
.k�0�{m�Q��Y��9<[S
��^e���9�f�T�1�R9�6/�1�:|�/V�+��	��]�v�������E���P��F1�zRی���/��h�h�D���숓n�����F�%�$P*��@���/>��⩰l��m|4K�y(�
rK0��L���������/�p�R�җ�h�y��N|��B2���c�K���ox���v>�S��^+�
[J(���O:Dh���܈L"Tۺ�R���"��;����c�HG�^�*��Ut�l�}�k���}�q�(��q�������H�1�G�͊����+`a�	�cB�=�Ng��P�h���m`�kuGn�HF&1�Q���j�x�4�'F�.0���1��X3�´����ڏ�Kր�7�>%�Օtt~X������s~����; ����2~^R���k�NX�aB�H[N�Cb¼$�nk���h-G�ǇK S��,#-�����5&�t��B�����Y�E|���V*;rZm��w���m#�D�XD^�L�]�]�y���j`,�%��vE:~�Lm!I�(���n]%i o��<2�@#Z�ysQV����c�j�Ȥ����Q�b��1>�x��+�tTZ����\�:�/�9|a�����.�yy�~�����"L:���V�X�I����-&��CC�A{j�A[�wa����p+�t-�H=�1���ep%?n^���;��2�2��T���=Qo���\���<.�<.�x�S�H�����F��J犳=��hLqZ���2���?g��������R�LTS5xC��br~)��G��U���=�y'�Sp��#���*i˸�:���8ݡ	������?"7t�]v�kh�����r��r�ڄ�ģ�Rm��N�#�8BmyY�в*q��/~3��&�E�N $k��G;~M�����>V����m�nu�
�����\��@"�'}��ߋ',�ToS�W��ڲZ/�� S�B����0�	�_�K�i�d���>�HjT˝LYe�-`�	�M�X��cR����B&�m�
�h���/�7Y��ؐ�ŏ�[�IYVIYv�q��L �w�6�������#=`K���Ѩ��I��t��W?<2�Q�T�Rޛ��k�I�@ҖU�V������[��Wiw\���/�6��8G���x��;���#YUl���y%9?�`+�ʢ�Z������*6\#�#���Q1���e��M�"�|�o���]��V0����n3���JebW]I]{�& ��*��1���e��mR��Ŗwu,L)��H���!���q�[;׬�A���� �R��4�(��	aU��T:��N���UHtu�($rY%r��W����@ޱ��6����6��� #���:cWx]6i��j����yA �9��e���$MX_�ACWP�M�F�
�R�fV���2]k%���N����-���6�YG��9|XY2�c�[V�[	�%�}�{o�u���`�
���r��0~RnYA��:�\V�\-	����~J�PL���	�l��u%��H�$`Uw@B�U�I���b~L�K�S��1|��e�T����O��"yx=a}���e���$��m���0v�N�L���������nK�l
�۩&f[|�g�gS
.@��e�v�ϳ|a<[9 u�}�>��$�s�EyV9��Z6l�Z\���-H��j�k�	R��R���Kф��X"ح|gal-O܌�I�5�-<���y�Gs�A-����Kf�Y�`���\���[V�[�6�_��:��%p%o�3%
j�i3ݿ���f帆�-��-�^N�ז"�0v9[����^~    ��N��.�,�iY�i�ɍR��De[6��F0lU�֥d�u(~|�e����1t����.���媍��^WEm��Ea#�Θa億l-�l�6�7���|��{Q�S�ϲ��J��O
[�UNƨ��RŬ}�O��������B�I"���΢���?�B�2����9u��i] ������I���d�7�D,�k����?�G&b��`���H>V���F����L@"?fʋ��q�� #RxV& �E?�ƛ3�mހw�F���R������h��]� ��*��uc2:u��G�ᡅb��r�@f�Uf����_*F���-�/��B<i���cx
�
��Շ2$NY%Nq�QǻH{��T(��bT
S*mW�wN.�k/�Q8j�0hXݦ��gdB���J�ci�9�ٖu�x|�Q/��둓�쪻x�嘁�'���&	��o�;Su�5�ۖ�:x�1.E���EuK� ���R��騗�x�
���
����0VE�5�oy^�܌�F�!�����ٔZRp|ț�ʛj�8J�U	V���mA�¶0��ڢ%��E1�)�.���l)�l)ӕ� �B��T@ҔU�T��}�y�sf�-�/�F�P�̶�E�W[j[b�*����nP|U��ڠ�[Eu)��L`���[�h�w�r���ܟ�2��uq"ȧ�ʧJ��/�'>I���-#�� ��1�8,�4��f!
��̡�ʡ�b��	n����F ��nr�0�v�;^�G���w�hƴ)���2�o7�3��X�֪�-��0��Um��ڦ�q���v��sa"UunM�M��3�R�ۂ V��TS�l1���Z� S]�̊wk����]*��K%i�8��v!�y�3%	�,&���V`b�7٠n�b�B&1�*B;$��7��߲�I�n��ja�MT�ڤ��v��N����h��ѶC®d�i/Q�8�B�V�hۑF����b��g�JϦWZ��/G*��=�Y��0�I5gۄP�G%ά�f�A,Ƈ�b�����ׁ(�h`'��*7+�ڠL�ߤ��9�����������S*�*&v���8���;X�֪B��4���LQ*��� U�S���y�X٬�b�ޤz�i���o���p;
��U� P!;�Ia��ٳĄ�9��)�`�
۩�V+ψ�5�6\����n��MLm���#�hO�8�)����B-�<�F/��By������y�3�ūc(k�n�'e� �`J�4�XrՊ�?3�"p�滻�wO"����*ݴ��q��vX'���m������	�nM~TcHN��mR�,P���������×:k��csQ.p�m��y����ՍGPrʵ���f�;���4Ny�,>��H~��1�a2�2�8,��e���m>�fV��j�&�+2���-���	o^���!�Jh9L�wJ���1\�E[�D4V~,t'�r�M�te��A�ʞpx|�2q$9O'�^B��^��x4�(���^2�O��N�S�|��<�'"��9�D��pP�-�#me�d��/�^��6M��1ű�W
FXhn�a{J�^�jMqs��c䉟i�x����u�q��[[���`lU��e�_-{x��0�ĩ�nG"�ue����4�g#3S$$��@��f���|s;��J�FlX[!lc��b�W�QA��)��213��nB-��0$�_�A-��_�q��TѬ����g`�ko�ģ}v���^��l��s���N��i��bU��_�����PV��'�NN3Q͔I�	0�M�蓱/���GԔ�k+��[���m1H[Q7.h����;s�^�0?��l��;j�7hݯ��͑
��	��'En��1/��\y帛��ƿȌx�*�ah�ޯ�hV���e��q�V8� ����*�>��WN �z���o��`,s��"�~�~�%��w�8�Ҥ�����1�UA���į�.�!1�mܙ��~��D����Q1��`�\l�ֻ3�"V�����O��௣��KWF|x��($�;%޻!ދO�U/����y��߸�ݚ��Ê#	y�.�&D-*��{֛���_ )�.P�c	����lr��6�A�)�.P�r�`��S}������v�vo�q������Y��˨�0]eݣ�7��(k�.*�<��PזjѼ�}⬍�5U��f�����Μ�Fb@1�4���n�k��I��[3d�;e�'1�g7F��o~�����8����_PX=���U�����(v�?�>7kS�@潛��Q���vœ��{�[J��>5�
�/��.ȣ&tq"t��]B�n¡D�#0�U-5�/��.��K_����f!��T��;-�ci�K(D)�QA�AҽSҽI�k�����׮,v��Z��n����e7�7ĥxkC[��v�bԿ49����N�&ѫ�-��:OR�R�MRZR�/�-��-07	���}����n搡�ph�C�W�B���i8~L��/3+�$�;%�$�0}���r?�|�|���<I�{�'~~>���ᕊ��j	�Z%T3Ko��]0�'�0����:/�L`�E�Oal]?5��o��1�M����#Xt��Iw!�&#B��b1�U\u�Ο��/|�t������ẟ���x�+���֟Zy��s|�A���~���6��TS��R����!G�4�*���ʭ���N3N$���v�w�6��'n�ŏ�㣭��|����JB���vT78B�K��Ort�	��NsLi��n:i��>�c#�w!Ք�<���H���_�B �4���!EU�g�ٽ&���Ru�>��T�Y���,g���ڭ���ӎ{����$]�W��p`v���Kni4:�ʨ�L����� �Q�R�q&�p�x߫߹�'UA�6���$���Eϑ��ht�2��nfJ���T��ؕ�1�T�i({���TS\���C�Lږ�Zŧ����:����L�Yꞙ�1�$i����)���S��S����ʯB��`�i#�ޖ?�I{�oD)�[`���\S�+�pE�e8��G��c����,��4����䀺���+����M�>6igbI�[�5���3�llc.��.UeXe�.e�uG
��tAN)����Zo�Űb�!^4lT)���B f:�����%ixk*c���`�i?<b�1%1�E��>�&|Rz��[-���m���v� ��T!�1ﯔ:��X?r����Ǥ6�N[�e��f���v�K�ᨉ��l�e��VA%7'&�IDɃ��'��a��t����㳳�u�فɖN�-&���s]���,�#���&^�E��]���n���N3.�K�M��d ��a���T��8o�G|�Jr'��0��i��&gV���*��7-K,8���1��)o$��]ϨR�[���tYG�������J���KҀ���I�ŵ��(]0"\�n{R��e�'L�t!����P�.] ����5��Q�|��].>e�]<<hh:�M3��C7ek <8F��Mb�V�Y�ǫh���!�'�4Oҙ�!��<e YlCU;̵�R<���\|�ɯ�0u�i��v馋�14ckƣbPv�� ��f��b�I��K�Y���K�9~9=Iz��%#)��!��*���1
%��'3C��Ӈ��َ�I58!����l0��i��8�Ս���m���|�B�J��P������<�`���[�s)���EyF�xlSR6I&j�:{[;"<3J�<��a�i��J����DV�|��4	b��A��9U�_�
0��i����ߴ��4M[W��1�%�a��WH�Q|ԲG#���-)�Ύ��ȗG�pT����.��.�~Nw)�_���[���t�?yn�\Fה�Vx�b�K����o�1��m>���r�x�{`���G��St]�0Ƶ��+,\�Sm�Gk�NL�_ԩ~Q�ܵ�H���ü��18h�T��l�I|+S����P��CiV�_7���&Tޖ��P��C}����(�t�⭂���ǀ"G-j�挗�`���M^����9w9�~�9���PD������d��ׅRC�J���XPe���    lA ����O��(�(9ԅnl&��*C :W�F�`'�qr���f��ոmgN��/@����&5]�|��aa<8:�{�h�I��gI[��O_~��1{�c�=Aw��x������Gm%}��KӤ��6I��/�����$f��%g:�O҂Q5�����v�^s�i_*�m.銱 ��h[�5�鶝�y�h�"_�xP�����L��%�y=~����7W8%0��kR������j�F��*�>�O�迂TQ_�q�0�C´�h���|/륐=���a�*� ����D��Z�����}�YI����9���{����ަ�1~O�2Pwi�e�����*��>�W�:��R4�����wY�)Աm�g���	��2��T]� �u+�=̍����0��%�<����z������I�b[75��5xM��^^St���c�%��O���\������k�ݜ�>~kn(w����y�ƒ��~|L�?l #W�}[��/�,]y�T�]��Wd��N�{����1l���,���m�~k��}��Kr�̫\>b�,?�`��vic�8/���	�WI����0Ԝ�tV:FqKe}n �RS�0"���Bs�2���W�$C/ϴ����gcS�.�$�&~�O�m�)�Y�L���|.�&Ő?D�G��E��k����ZO�:�j#Jf�e"����o�i��$v*�0�������}�Nw�bLݾ���&{�.�A�&���ֹ0�ˏ�0�RH�����fw�ҏ~�V����5im�0L%��e���7�3l+'�Wr�~�_N��YF,%^ ZaN�לn�� ?�-�U'��,�9�>T��Yt����o���t������x��&J>+/�Q���1�	���\Y	�c��-m`���|w�U��H��n���1�[��C��n2�i�j�½e�d�5aB�ׄn�gE���3'���?�$�������J��ٌ�cOK�b���R��h�vun9�����uc̵U_���c�"c� ���5�ۻx����ܓ��{!zR��N������e���Xr��YI����u�^�m���-`�J�7�oG��RT�)��=�{��vI�I+'����,�~>��3��X�Kѽc��;���h�ێa�5o}L�S��d�l��&~{M�v.>�Ddnz�BӶ,���&~�q##����=��䚿ӻ}(nKTIu\��I�����<����;3ߨ�x�Z��肤[O8���?;�CӢā>n��/�?��4��x�2N1z�8��=���S��5��Ḙ˵����D�OHhSә�o�)�-ٶ�L�|��7����f����M�[�7�!K0,Y*א>&K���-����~�ь��K�g���"4w�hz
�%�I���$���x�� ��Uϖ�/�}L�'яy��l\��s�J�[)�}L_bL����t�E��T^0 r���Đ��?�93�b���K�cp��:���D���1�[#pH����$�0�f�٥�1�[F��11c�EZ�5��ٺ�2��,l�_������_�a�2���T?{������X\C���WK6������-���˛V-��Q�
���g�i��~w�b~0��Q�?{���k8��ŏ��2v)v�����OT������|)N���kCa�Fi������w{�w�^#�ى�#�s_�u/����A1<��&/ƃ?�Gӷu��'�c��\�sNgڜ������i�!{�)�r�������h��O$�R�0,�Ҙ,Y��mz��k��(ل�I���8�~G�v+���M��n��M[Yxi�J�Ǆ�>)C�iL��Kq��ƨ���T��WhJ�H�� ��1;x���<��>����Ii}ܼ��`�ʟ��NБ�O��XRb]^s���1�l';RYļ�1.qy\v����)3�����ar3^[{0�O/����uf~�V�C��Ǵ*�%~DeY���~al���<6e�.�R,�G�D@�^�'L�b�++hynd�1H�[�R�l�!�������ϰ$��Փ�χ�v[u��|},7�8���B?m�s��,`09�-��ޛ/�}�u��1�����8�*Ч�c��[1�!���L�|%&6O��>��_2�������f6%��yg�`(;9�����.r\u����r�?�_�����2 ���K������][��@F@�M�꺟���Ǒ�[�.���Yu#�l���󰍣�+�9��c�聉z����Ê����_>��ao�=�E[zq��Qݶ`�x�.�O>'�Y #���U���x��hu�r��c�����ב�/���mŝ����g��r�@>@�Qf����@�i
��X�h�_�$ 7�}�`�F�5� �IZ�{ja c��r��A0p�v��'��v<�!�Ǽ5%�O��D�rzq黺=d	�1MV�~���\�l�q�?��W��.����KS�a�+����
��-e3����@�Yz,-E
ʽ^G�^A�P�dr"Ă	P���7���7��ʿ�,���� ���yL�b+��~88�B����!t���|ЁcH�j�D��$a\����E�s�u�T��c��
���dB8��}�k��L�7N�9nʎ}^:�46�� ^w��+��\�5��SL��0ꁙ�P�O��z������c�+�]J)�����0��4ז���¨;�Au�>�c7�&ϑ�^n���L����?�����C�ЃN����],v�~��$Q籁&���k�o?M;
�|ݘY��jQK�9j�%Q���0/�n;���1;k��y��h���A�z}̓gENә{��JP������;��E��X�O�%�]LX6��9n5g���J��n��������/��F{�]�ts�4���[	��8��ݱQr4(Y� ��1�?��1L���k_bA����
4���|��W��2��M����o���Jh��,��`����A�>�S4Y�D��J�.�?Z���m�']�u����z|�i\W�g�n������7x/��0_��Z\E?Ix�K?�x+0�/�3���S�J�����8�J�;�햐�B�25���	��O�?���\ID�v����|(�9�dG� Q�
G���>ׅ�����-��/���+�
��f���wƘ��:��R���b�@t��QJ�7��=�$PW-B]��1����}���M2��k� Dh+���d�~�q�\��Z�#�lpj����cCD�BF2�Cp񷯹�k�w�^"�UR�C4�[��^����Ä��]���^U��iG�ЎLR�0qx��Z��cιū�{�ה����Ձ���b���i�.֍z��c��D���
��l�+r����L_���^7�x�F8���S�����L�ݏ��YyY�Ia���'/��Fuu�1jb��y-�wBZr���>#[[���2�c��E��%;C�ό|�1ѨU��P�w1��D�:��NPyg�v׾#f�8�Gx?���}إ����wL4j�hd��R]�A��w,��($#�%�(�.�:��c�E�D?�O�1���jD���q��d9���We;Pz}�k:N�������9��fč�]�ߍ����5UW���Z!��8�ͿM!����/����VHG��c�M�	Z�c�XĬ�VXGmrG���F���-^��*0��ӻ��YW{yD�Mk�}MR�:O��rcX�R�G�����tg	�>�_CӪo֏ۃc+}�|kA1�>�kt�>�{��g��,lix�C!15&^�3���Uz��A��Za1�IOF�Χ�-��(��&a�h-�C��C]6�ż�VxMm���#M�ڷ��1��u�&8�c=/x%C[E\l1��uJ �O��*��������:ua�59{
fm:0���#:��n�w�a[��o1}��R��;��>�On�4T�����Q���O���~[�m0-�Z����{:	��Z�@D+l`
�R\C��q�瘐�
!)��q�ڊ�Y���*)9V� Ps�+��s�Z��I^C��Nꉸ\�paF0��ڑ_`qy�X ���ՠ��D!��V>1̮�    7���ZH3�Ze��sI���o�gǺ�
Ӑ�����q@W�0L�,�|�{���neՙr�\Yh���&� 
h�L���ї3ZQ�b���t&����ǅ:���wqS�f��3ZQؙ���)�;��a�9���73ZAȺ"��põ��t�k����h5Z-اEp�5����?��xH���Phc���Qh��1��OF{�т@�՟$���}S����E���$T����R9��?Iʈ�a�mSn��:~L���_��?�ǆ��Z�5R�/��CuF+M�&��σ6��q."s��_��J���x$ў2�����������kc��'�&O������V�.X$"��;�����+-�mqX���+1�THELI�ݑx�+��X�]H�Q�G�����k0�%�璒��D̹ybr���o�1�y��"\�Wv�	��� �h^zx�W7�&�]�V�^�F�'���1�����m�xtv��RTm��� ��2B_y��'�%�����a>g�bP��]n�u��&�W�}6I������e9:~}Y	�%�/{Z���e�l!�����Ւ¤��ἡ�"@����;�`�%���8��_z+�H@ 6���c�J�Kn��vs��ގ�����s�p��^*~#V���cA΁�����6>���/�U���>���8��Tu\��'=�a�%�6��D����Sѯ������?�c2�E�Z�#��u��1A�������(��E�ǥ��j�(*?�����[��`����a�-4��c�Z�T/w��0)�M� �N
3�2�K��a��9|
�E�C #X�]�~s%��/m�_?_J0bh��5����4�hϧF�D�Ф���0��� ��;���Ƿ�2����`;��>w�sxn^����DL��gֽ���[1=�G��n���q/���~��@�Z�n�s=���yZ0�h��o��ʄۉ�Ј�U����7�1I��YcTB�ޏ*�A�\<�u�R4��192��}�b5�R���p�ZV0�[��Ě��T���!-]t���$���[�0xh�r��c�~�3{wf-�#�Fk�>໿��W�P�����bE�d4N�/b�\�j��e�sa�I�baV[}�'�O�Hp��8^T����iD<�ޣ�΋|��G<-��E�KL�L��4}��p�?��%�Dϙdn��n�����a=��zĬ���"�ru؆��FK[���"{�CY�k�e.�{i�?T��X��x�KŞ5��r�)ea����=�zipُG���'���ײ���O�c��%/O+^�R��L�y��"��,��Ǆ�X��E�� ��]�����;��w&v_.~�cbҸ�W�2;�evi����G�$�,�3��n��?�mW7��X�ʺ>�\R�5\5b��`Y�Ѳ���l!z��ㅺ˽�ʧ��uF+���Q���ů�d]WF��ں� �G?O_���hh1<F��إ�"/����~L׏�"~h�u V����,�P�%ڭ�!���h}�5�JTi�,��.D��9���餄���bp��QC��r���U��ԛ���)�v]���~OgI(�9g���h��K�Cާ݉�n����?g��c�J�]JlTn����TJ$��V�����N�=�ߴ{�T��c�����KUR6]~)�qT�@����-i�,'�ǔ<dlc�?$^R��.v���hm�MDkdD��K�&���7�UuF����ÖK_�D�۲�����ʺ���M�ϓp����7�?�0�q�U]�d�K_jL�E�����{)��a�nU�����0q�r�0f����qi�&��Wy�v��%����1d��rt)/\	��g|�F0|����ߞ��ٯ���-�sI�'XawJ�3�T��V0�����~�6�}J�x�J��"<�Ex]�m�����z�mh��4���m�9���@L��ϸX�h��٦���_��+���Clk}^�v�3.��~{*	�x\e-�3ۄ�:���¶����L(�K���u1(\�g��&c��c�n!_��y&��q����}�,@�j]^�����$�������3Z���M|��$��m(����� �z6��Vhĕq��������5Qp��J������d?C��.q���r��d�ib�����?������}=EC��T~h��VS�ɇ��b���Sa Y�?C�:_�+1&%rk{��h)�m�O�4��y�z�
��j>������Vt���1՚	��3Z��%�l���[�W�t�Z̈�ʈ��4�8�rE�S��-f�Zeæ���~�__�r�+�xx�3;�ac�ai&W|T<:�Ulڿ�^�%R�ZVP�����amҡf:)9ݖ^<6B�Un��b��BXf�i|�4��GV��&�_Px�mC�VX�����0d�(k����1�l���4V���������_\�S��ih���]�6�[���)vw<,Ƣ��;�:��m�C���<9ӕY߶/�lc��<�3��B���q��-4X�)�h��S��O*Gm��<��_]gk���t���s�/��Xæ�:��`��i���f�d�s;AW�����ra��n���b�<o��4Y�rv	�w�;UW~��~@a�XB�R���O_z�x~0��$��n���$e]q���B�+?�T��wt��&{g
��,�4G��:��o�񦸯��1p�fh�]�A��B�n�I�6�`�̂Į���E��W:j$�-�ũpV_$�����~�x�y칽4���N��Ǖ�ɰ�R!��C2��GOo���]#T	�.�6�eH������
��\A旽?���/��#m@��U��R�7ٸN_ܜ��������4W�4W��G5�.w��?��U��|���W��WgR:<<LR��t�3��*�WG�?��ЏP�	�1X��ꚴV�t�p{�?VȮ�Mҙi�*��7�]2��+dW[��Mw]C��-����k�i��[W9:���[f]�����޵�$��@r��W�됒 V��p���t���o?��%|c'v[�@A��U����vA�Tۚ:�2]�2]���*J�R ߘ��R[m��6�s��m���c�
���iuج���!��*�5iA'#R{jQ��յX&�Bgu	�N�U_�+�/� ��-I����Kd�1,�J�:/�m�2<�_�) �5�d�Rv�N�%Y|e!���j����N���RS�!��j�-�Z���Cu���K��Q)d�>�H��i'j��t��~OG�n�.�;0Z��j�ܖ����1�8�.$�Z%æ�2�K\3Sʙ���*���B9}��٤�[��f�o�Y0���v�$�,��`;���1d��N�ǝ]ۍ!1�*1��i���ʤ��ʑ��V��]B2��]�,�F���`�ܜ#��o�#��e��Q�\Xzs$�4����}��,�4����b]��#E}$�u?�d�0q�ce��6V�t��h�B���_��"�\a��z��wIf��bG���+�ގV��_�w"Fq/��K=$�Z%����L�;ٟ�\����M�0^I�D݄4A��\�m��^�{kz����M��iH�N��2ph�����\�JG ��mayx�N!�vq�[�����IwCݕ ���],��6^��OWw��_���.�����h�Ka�JH�J�\Rt�$!��q�U-^�Rx�i��c���g����͐4��<NA0�r}b|*8^�s@[D�*�'����j�B�?�������uH�Jv�7+R�G�16����v�Y�6]�'�}�����xpc!w.��lş�����F�҃�0m��O��jI�VI�nK�����mE�+l�+�%��iƣ�}c]
҅�҅mTj�ݏ����Da1G�c��q�=u�y�>#V���)ey���qBF�UFpJ�|^v=	�,`��&|����Q��qH��$�P��Dl���V��Τ� �O�jJ������H4��o�{�:'��;T}O<��m���`pj��c���qݩ[Y��l�    l���)3�������Ґl��W稡�b�l�l:d��/�W��4��+4`��N��0p��H�E�7sy��8C�Y�\`�\��2[�@�[��c
�U
p�Ȇ�#P`�`�ـ�	�@�{<3��6�}o(�l��qL�J���u�0���@H+)8-@�||�W��T�F�l�l�82r���p�R���m�
��8j�1x�JF����O�+l�+�5�r���R�.��� v�;�@	s#����YR�v�(���ר �L�r[�Jks��c3��2�[�P�C����+&[%w�m�i�F<�;`�a��Mz�|��3�O������Ne2܀�_��6(��Ιr-�D&K� h'��41�0��)5i���dJaHl �Z�R=�L���'�M@�6a�882}Ww�c�T�R&��o	y:[���@��I�����|䭡�[��"��@:��'m>OT$ߗ�x|\�Iuq����e7�ݗs�c����R��%����m�N�ؙ��p_�}AR����nw�s83_��VUL4 ߏ�{hmǷ?L�+,`���\:��W������IT�"$��b�|q�|"4�T� �K�n���kzH��T~̟�!..�}!n���a��p%nb�MH��Uq6J]se)%6�!���d�Tgj@��Ҷ���*m#��D��T��ʥ�>"V��tY�Xק2·�Xm,�%��O��� �C��V�0F�U}Ϥ�������[�;F�����������,u�?�Ni?�P�N���w��*z��Xm(�%K�t;��yi�K�j/۴�pgq�������".ɡ��|rǻ�vUV���J$��x��K��%��w��'`�*k���S�c�����f"V��tI����D:����ų���2�]ڂX;�v��'8�Li����.ܚ6�T]W̜��"}��+i[W��}.�C2���)��NI�dpt̗��"�eѢ�t>~IT�󤴽�c��J�}zU�t1��14;М��v���t9[�c�~"V��X�*N�]@�z���r����D��V���=D�1_Vc��r8>A�M��N����:m?�Vse��ܧV�On��~��Kv�m��P�2����_�]l����Z!Cy\���|��g�ߙ[_֦C�ժ4`҂0t�|�j7T��ZL�j��%BQl��{9i'p��++��U�Eu�ʊv|�n�`zk�D�1�7:ػ�;��8Y��A)|�v�Oy'�g��b�*>Z�z�����d)���b
ϩKB^������B�H<<F���ݠ������kI
�pj{������Uv���
Ӝ�~[2��bG{���m�ƹk�YN�����:����"�ᕐ&7�Jn*��XFJ�t�� Ɨ2�:`�fd7Ic���8��`���a��S+V��۞�I`(?�U+��s�Z�<t1�>�O%���f�~0ө�2'���H��v�(.,`�$�#P���r���N�vj��w�}����[u�.Ƃ9O���j�L�6�x��J�*l�Y8O���.R�z�}��Ϩ��`�S+���O�c~0sSț���#b�S+�'k��û�]�k;'&9�Br2	ے�7X�CU�>f8��p21B���!��U��S�Z�6%�?�o�+R'"��iZva�F0��ؔ�~��ߕb�|��;LojU1���F=wi�l]h�Z�=�X�Gڒ���YG�-)~ ��r��8EIF�I��7cc�
��Y�ؿ����[[���ܧv��q�R���4P�*�o1��j���_�hV;WU��b�S+�'���h��tmX�ԭ�[K�x(K�����
����VN]r�bI'b[?D��uؓ�pLwj���y�Q/$�n�`�Jkj�����E`���L0���j�6�h��md��p�ori�+��b�aDn�.�XR���tW凅[z1O�Bk�	�1ؐ.|���%Lh2Bhj;��5M)������@��o�(��l��M(���bh�U�mM��Lh9
3����M�IX�s��簀�S�ԍ�<:i�s���v�jtX�ʩ�U�t��o����u�m�F�T��_uU����r��}���+��UMR��~��a�-��xp�Q9�J�����==uۚ���1ڵ�qkS=y��׀\ܼE�0m\N%��1��~j;J��&�'mZNe�R%P.!|}e�ߒ��J:�g�TϪME*�����nc���7�T-��VI�������N���Y�$��r�n՚�4������0P[UCO��X۾-ãp�!A˩�Uc��w�ߏ�0���~u��V��ǟw.�юu�08ն���"^��eq��_M�S=*i�^Yӽ��bcAn�YN�M4��w�b8�VA�RԠ�uv�n�O:�I9��J��ghf&nK�+\��6��"�|;��%M)�8M�=�Tg*D����L�p��o��
U�ͪ[���v!�#3xt|"��u�ĺ*��ڎ�&-���?5F�ѳ�_1��~%j�����'&���(� (�`�j��?ÖHTt�&퉈������*��	\aQ5��tW�z�Pw�B�S)�&���'�&g-t�5�H���<�m��$5�����*~W�*�Lŏ����:���N
�=�5
+�J��io��gW��]2���E�&�S���pgn�3��&Z�&ϡQH��4�A�Q@�?֎���c��Y^����B�S371���N���ʺ����JA5�=1�:I]͋�flCIHMM�x>�~����;�����T*m��q���qk�#c���ocC|;���)X� �ɩ�SJ�|���S����g�P�Zj��!��e��,F�p��1����eML]�5 �ɩT�hM���R�|�)��S����A����X�b������?�Iݻ���8{��4<G��Oa6%�3^"1���S���tۧ���' ��
A�Ͻ�vHH!5����z��Ñ~L O��9j�]9*���U)?��r~;���O�`������+a���O�?I����D��Oǯ�~�q�����͠a�&��a$�
o�v~L_ i9����x�/b���h�B (}q]9��'8�mr#Y��v~�l�����M�;��U��Kch�����h㭆r�L�=	�"���NcEMRR��6���CY�_��1�x��q1��u��S�#F�����,2Ņ��3��p
����IT���N<EfeZ0����IzM쵥�)a�+cL���ƣ���M|ʭ�̞�w�pn��,�����S=l��7ư���~�
A�R	�=�k+puC2㜼��L_����1@5���sI7Em�_ƛ�1�p��R�|��T�q�#�F�`:ֵ��E�Pv��cTJ��i���Mm"	����L]�2�k��V��`������u��b%~?	�4�V<��6ŵ����5� /�#%X�ǿ)u�q	j��+���cv�P�^�w��a<A.�iY���������s|�#��N";}�'���kz�S��v}u��u�1��I��A(U۲���Qc�,k�eah��1�ʷ}�p�v���R&QZ���t'`��� O�t�x%��o�8t+Cb8Y9yR] �b�TSmģc<Yq�Vk��;uޝS���3ǯ��b�1�4�F��ypn���ۆ�����i�^�����tr�(�XVp��?���z�\�q��e)3CLJ����<�X��pP�A�d�������m�-�3J�BMR.6[�ٲ��\&�B�(Ҵ�5t�_8�
Wu!D4>GW��q�_��	��D�1(�H��l�g�)����qwpU~[�c�5@n�����Y�: a\�ӸP���q~�|��-�}a���4Ż̑�nIK�W
J�qel�_	5��ƻ�{��vu��:�
o��_������c�<%u��/*�>Q�X�@m�#c�R���ta����g��X��kbr��x�}�,�8,���N���>4:ؖ�xh\)�k���|��$��m���0p;��oy�Tv�V.��NB4#HDT��I&3,��u�	N�'l�E]�ֻ�c�z�]Z��/?k��_�s    ��r7~�Wl.��sOz����6~LSW��C����c�xY�Ǟ�,X�AX�K�.X��:U�N���MK��e�I�91�^�����ܤ]{:���v�SX�H�5��U��Z�xR�R_Ά�q��S{��Ku����ڊ���C5��Fa��Ҟ�XǏy�|�����\�%����#�hQ�մ�~�5z��¢7~LC&ݏ>�-4ab��*��w�LP��C�q[H��!1̤����hr����������1�I`��ĠZ��?�$]��h���I�dd��3ږ�y<<F٠d���ҼԚO[L���x	b�����F���?��5le��V�{:��n���Z4~�+���O+ږ���4~,��hv����:��޶2A��1�w�F5�L�J6��9)I�)ڬ�}�w��ڵ������Ǔog9���S�@��[G����2�x�n���tg����?,?�Ǽmơ*v}��L�K��c�����1 Um�M���D��jV�j��S��Ճ�'�����ϰ���Q��H�����������"���K�.�OҴ���}[@��C�=���d�ц������{���.ν��CfA�ԁ�W ����[c_~�s�P@�M�S��F�iq�n�-�l�}�$�����'���n(��m�7/;!�H���9B���v�^kG�XG�z�n��SxK���hC�~$&P�d�Ċ9I �;��+nH����s������+����jx7�#i�J�T�ba2� ��	�z���Tu?b�<�����ҌE���`G`����R�I6�*@�@�l�֥������I��94��B��y?^����]
3�R��$���蛗ʏxl�\-Xiӟ�I��Q�	��Z�U�VS�i����"]�G�XV�@rn=[�1��J>>d�Z��&~��k�����=0�er�&�;������1��D�b�9�j�&��x�t����J%0钤�����-䟃���0��z�-.�sv�U���p��+�֮K��BYB��X��!yۣ�G��>�a1V�J�%��_Q)��c�e[�h5���+����m���b�
q��l\�n�i.;g��!w��I��O:v_����	I���.+zd6N_2����R�Ҡ��|l��4T���z�E�κ#�m*����C���4�b���*��F�V�$�(��yP7�/	��y��k?��堜�݉0��AR|dY����U"6r]�b#�h�x5FF��v(��a�}���-�v����� �@�0��l5���i�_&nZܭ�6��+�mb�+�k4���-�A�J��堯79��'6�Q��s��f¥o�Ӎ�+lӷ�
<ʅg,�V�X���[V��K��~(�x20P]�=�t�v��i�@�ʗ�t�9����nmÅ��>� >��G5�k��i�ǜ|M��.8s��aѐ�����t��Ǎu$Hh����w��mkVn����+�!= ��h&=����z%5��s�$��_��(��� ����~:>]��v�1.;=HA��e���;d2�ɀ��<� ��x�0X��?���ٶ_`-c[��������Y�u*�8��	}�w��	2ze>ק>����O��)��xx�bm��$Υ,��!��$��A
D���Ӵ��A�ɇ2�_�X�����w��a��{[������=5���w��:z[�Ԉ^�	Q��'Ro�{�\���$I�J��҅x]v��rU���bt�^I��}�=�f�2z	�vHO�/�FC_�t	~,L�����@>�n��Ჾ�y�����~� ��׮�&����i^5�Q�Vc�"-ܶ�N{>���y��	���Moc&������
��톴~�y�8 ~��[w%��5��H�ڤ�(9Ycm1f��1�D����4x����x�ڒĸ]�D��;'Pk��ƯL�V�)��/o�����ISj������A�D���ۮI&�������c�j�v0�D��d�8�3T}_��l�^{��� �����PʥHJ.��� vu�u�ȥ�K�$>�l�U �-��	|<�R��P(������*([��x�0n��E�~��x8�^��� (�7���"�Յ�!O�<��OJ�T�<����?rz��:���R6��S���N.?�<�RKd��n7���X�c���v������5�u�U��IR��%��]��`����<�ﱑsh���`�t�*a/c�Jo*Kč�>������c4���dNa�V�s�.+?#;���#��w��yI��`"����S=��x���ۭ�(��Q�X`��C�jS�]�	I&~l��"l
�Xۃ���s&�+�@dk��1�|]%LMG�c�Cxk{pj�;���"|���s�m@����Dd�tH�};�c���!�[x�x�O�qL��E�o/lA�k�p�i��9[mn �8�ߕ���m�a�q��)�	��x3�ے�ႛ��9��x���S}��R~�-)��Ga�Q۪��E�%:�x���Ru����O�
���n!vR�[C����x�^�n��F�]�m��:��uu{&�A�$�KZ�u�
�N�o�*n�h6������0�H[�7	�x6�t�"��!S���xJ�U#�Ȣp�t��\��m`�K���;A;�>�`
R�B&T�B=r��up���i{�6����7s�G���aH��M��)i?m����,;a��w�Ǎ�B���U���A�a���'�`���/�����F)�i%O�X/��pC��o�K�J�W���E�v�_�U�R�yd	�t�� �
�OU�5�nEEM�f%��[���:�zY����O
C�R�.�ptL-j�Z4�E�cT_�b���#��&�؄:�yǅ�Q����Qʩa�(�� "���ۉ��N�IB���|���a���0I{���5A7�o:Y$d���Ľ�{�%�&��<��u����X��1��������i���d�qS�^����)������m,��`ԁ{�O�D��*.�{��c�$UH�ȴ�������e/���I�m�iC'��r�U��p��^ێ�Ğ��P�N��U$3�v�׶�m�J�ӽ/�`c�A�%��j�y�-D��*�b�ժ>�p.*(r�
s��9�f�pt0��GK��H�^�4�sP�vX`%-5�ZL�[գc�q]u��U�z`WU���?֎�)K��x<�$ �ctv�<����<oHm���>�d|Q�+�/�c��|ثT���BY?��Yۮd��d�U���
����N�>��A�Ǥ�i:��J�P�ۀ�Px��0�t>|펜�8	�wEFAˏ9b�I S�Eڅ*!���U_�cf���9�feq�ǃ��o���&�%�Q��Ӂ6�"vC����pe��ޮ��LW��]\Y�6�h?f�����nl������,L�B)�ͷ���\�h�Tu8�S�!�jM�o�[�c��Յm"��������� ��08��Q2�>g>�������Q*�ãId2�tn��dWJR��1@��Х��x���g�RFO�j��>Z]x�yW&��O��ՠ_|��-Hh���+F���L�Չ��oS$�Q��5ų��2b֘��Rv_Y���Ǩ��B����+كx�0�[�y$[����L]Y7?1��Zq�V+�U�xr�?~�ĕ����a�F�0�����a�`�P*m�7�h5�֤�*+ߥ�=^�����K.��Oo�+��@������l��[X�C�l�0�Ǐ	�.U;�R/���C�9tC���ZQ(�������0�7�����;�T�N{aSw݊k�|�XH�_5�T����07h���U#�/j��������Rm�&^z�����8�,�bt��9���<��!�1e5\�0�Əy��7�W�C4�"��F��WRv��oR�R2��c,��H��G�q�x-��Q	��)�5˖l cI�kHnw�ߞ��3����������<�9�$�ٺ-F���tA�W��x��/��'���:_%����JEs%    ��dgJ%�`��}�Keq��&0���͘���Rg�����1T�x���~J�
$)�+mY5���
�{�2(��`��6�J��
��7��|a���uGT�z�\)�F���Ze;l~��}�|Bٖٞu�2��;�����7�!�!��`J�u���+?#��+� 
�O���O��K?�`�k�刁��]V^��OFW�JF�\�y^�������Q�����Ϸ�?aw6��_��.���xe�`�vCy�G�y�g�a�@(�aĆ��9�y9�r��gz1,F���
�+�Y�Yu�>X�7h�^Z�0��W�;<9�`����-��?&��S�X�7�bT��U��e��n��">��ېE�o��)hx�0�U�8�����Bo���v��=~,�o���$�S��`��T�^��Yz�q�^���S��N����>R����AE���ߘ?���٫�����5�r���`�?��kZd��1�:�<:�h�I�]���kW*^��1H-���H1�tSJZ��c��*q�R��T�2�e�u�@X(7�vpR�M67q�W �ㆠ�4��Pۧe��]_91x�!�}XR�%$�b���B�ܘV\����ε�Er�j��͉V�YG/�!��aiܠ�qM�T���$���gaa�d��.���w�ʈ$����<-�/A��r�|;XǏ�^.�
~CiJv����P��9?�`Eܠq�_!ݱ�ӿ���UE����̛�Fw�����Ǹ��Y�K���B6���q+��3�zs<g0��Ca����ל�>����,�BI�����3Oܗ�|l#v�L�x^������8F"�1�
S�Z��b$S��l�ݰ̏KH>z=�1��3����q�aX�7�r?+)�g!�Q���+5�������ܺ,�B�_G2�9��-����[I��������RZO��vT����}퇲�����UQ������v����K��ʳ��u|��C�mB��I+���cw���-�p�Y�_���Crawj���{�k͊����z<~\0�����ǰ��}�<~,\���x�6��rs��S�,ğ�ⵦ�)�BP�M?�;{��6�1�x�D16��֩k�>X��l��%c1p�I$lpE�7�(��Vy3LT8�݀�S���V�4[��lᤴ����ՄҺmBX�ӷ��l1��UM�A�_�}/���&�c����bHy�t<~��Kj���D1�������3�Fe�
xp��+�ʾac��GL���K��Mף�I��
�'���w�T7���T�x�����8�'�*�����O�o����[}w���c��!vFv�$⨮H��������6!!���6�(�ze���f��1��!������Ƃu�'����[@3����H�����ÍcT>��+�D>�㤍��3N�@FU�w��9���\����R{�59yj���R�F���!��ݤ��ضk��������\!�2y�:�� ��c$I5�I����&�0�J�`ɧ?�����kks�a#����ֿ�D=��Ϸ-;��χ�#u�nx��������6���Mn1L��\o�=��W�
��c>ƈ�~gz�EnK!P<,��Գ:�3=b0�������;R)�]υ�O�0ED�_â������ƥ���6��_�^^�BBܨ��vrZBr���Xx���`�J���	�I�2y�`�J!�u��d6����W��]���y�4Z�������}%)��q1��y��qw�ގNd�.l#Z�a�����6Z|�0����̳��$Q��tZ;� oT~�4%��ӛl�E@� ���$��4p��O!�	�x��l��J�l�k�cVJ]�-PU�W)�z��H�)y�R�[RU~Ƽu��	���1��>Iy+�������\�Q�M��U߷�(���o��7���K8��l�be���⍪���}�޵ư��ăc�J��u�`�&5Ʋ�ƯS�8�3�ˈ��ż����R�j�������[
��Ǩ�����>�p�l�R��Q����'� ���r�� Y}�j�'����<M��X����1R����ah��S��L�nxh
b�%��7*�/]8"��,�?-qx��� ����|��bգ����t��q1l��%z���xމ�Е�%x�0r��5�>��0��0��7*񯳈A3d��6��U��K�4��B<[��!�oT�~�ty �8	�n�>l��b�P�Rr�2 ��+WH�����t�Yy5�Jݷ	�x,:�Ŵ��Aߨ�.i_���i��|�A�ި��nL�x|���I`���Bߨ�����2� ��V�c 
]�3���"0P9>�H�;�����Y�����P��m� �Kl�D���.a�}_ޕ��C�x}_iC�� n"]r�cYX�����C�6�h��@=�2��1}[���k�Ĭ�H����%J�*��n��M�� �x�.v���*��}:qA$��בoB�Q8*��OJ X��.�(U���PT7�/3o����w�;p ���C���<�%��� u�����T�R���s��H��4�W�^~e}�P��A�����M���紐�����!�k���f��H���'\�R~B}LV��!%(؊L�����ڒ���c.D���r"����D�Ѯ�(�d}L?#-���i��_�߉����dcZ�ڄ�[�>�>�П�^n�����~���Ġ���Q����%L�Wa����/,|�W��4v�$��@��Dk�m3�{�7��>.��v�䘦�V�
�Q�cF"�!<H�jL�?��uR����<Gf��p��{�C�qĎ	��j��A�RA��<����8F��KPO�����,ƅP��,b촂�Q<��	�~d��K?��ݖ�tb����;������
Ȱ�c��M��	!�D�(�8���}R�M�w�����0f��%����D��E��~���YZX� �����.�Q=�n�u1�DRbL(����Ng>Q�����bĉh���N;��tg����������U[��mC��q����C.��Hw)ʢ�1��(ޏ�-���B���·�yi�Z(~Z.F�&��HD.�E��@�Ǩ����%MD�ޘ����O@+Ȩ�d��,}�
��/��ZAF����ﰳ	� ��ә�Y�d��F=%��OF�q�������x`w�m�F]&��߸�y�~�M�<;��K��o�Y2	�|G;�]n�aX���d�c�q�����(���^~P�)���QG�f�nt駯��,��-Z`�M�f7�;�4����h��?1��,�W*0A�w��=��_xtfq�l�hF���}�P����r���U�A-�`]�1ݧ-�Bg�`8��fM�hv��D~䃸��1��{#z՞�OG
�ʯ�}�\��:�2�o���)��]�iF�4�o�C�5ys�x�<µ��tՌ�ji�/�r�;~?��p��f�aKiQRF%K�k�y�\.��d�W�7�4 $�?2R�ꪭ�qF�8�$j�����xv<~S\���c�+g�8_�q{p�%�W�&��zq� �I��>7����%r9u�g`�P��A���|d�+g�,���0�Hv|A�_CGM�$j���9$�$���Yʻ��Q��U�WFV��]���R�xV�������PˢRf�~?�ȳֈ�{�d
��Zq�@3l�}v�7�Ň�#��(��������"'�a�V/:xhl�+�I��,v�w�./M[2��-�e�O>�ܶ�7��ٵ |/v�7���Q ��j/���c��J��kz�zm���a�#`8�r�K�#��*����>���)}�����v�o[�X���|�Q������������HK�0Н^���j0F���M[�!s�2󋦄�ɫW����d*},WhW-���1��ܜ���'r*�z߬C`S�cz�X~���-jf�o���q;��9���K!?Sm�[�Ӻ������r9���υs�f�1�m;>��DL�?�_    �>��>^��L���b�F�ceS�+}�sÏ�t]YK��2�i�
���1oq�»���8�������m��!��3���C���<vC��ϋ��R�K�o���%����sw>p���2�X��~A.L@�$�Z���*`d�c�o������] +K���R��9�/�:�ж�1�����s��怸��e	E;�7��v��3��+� �[��t#���}�r�8���+��U{���ޒeY�
In�w��� /� b��鮪�F�F�$��\�R��ȼ�j���5q蓘}�F��]69Fr+��*�j�XN�zxmGN���[dF0�[�|��5��N�˛��/}h�b�\s	����r�iN�Z�#�U�<>6�=Z���+S�a�\:̷|3��Q~��)�z�US/Y��n��C�z*�ۄ��ڛѶ�*��?��3�|�Y˾ t�0_c�3MU���"S�9J��r�YSqq8��q�0~~+�+�u�0����X��.@ӕm�ץ�d���2��#P�8.�˥�4m� ��Ur�t��n��6�!�Ή�x��{��)�4m_hüS_�I��\u���0�;��+�2s�/�n2Cݝ8�s:���S��-��/���:f� �a��H�Ǹ�7s����T�t�Wp<����N�w��O �u:�Gx|�ucP� g���1�E�.:����(�ڢ�Ю�a>/�{a�{ee��X���ǻ��X���C.݅�Zu:̻ތ������-�b�K�֪��Q�.�0o��d�Y��j�+����^��(��q���kd��i��@�N����ni����N���:��\��%|#�۲���4�tXn�ђP~L�y�/c� u:�<Z57�|÷���2+��ޘ����tѹ(�`�3q��L6���*Ot��zG�rqڤ?P�!��fTl��Z�8.�=��B3��}ύ��:P����hF���u���b�@#���Rm���L��ԁ0�(����S������:���̂]{�ho�C绤�c�L��Ng�焼�Zy-ie��9e��\aw���<�LI/�Z~���R+����/��U��.�����抣B̋2�L�]yZ���p��v{�����g��Z���<\1��0�%qu
�a�t	�����+�[�V�c�~�V0��y��DjT.�1�Tt$�FJ���x^ҭ�w+0����&=��e��Acl�21���0-����$�,Ѫ[��Af�4�O�)��Wᷨ?�i�b��Ƣ҈����dp} %�v�H������&F(��+|���O��R+;�k�����E?���Jk\�0\�����]��d���R���)d��YJ�x��:��:��	��5b�6+i�_�`�	>N�������vxj���@4N:���hB�y��Ξ��xunH��#��bh�4�,+�/�L���"����&�����"j�_�d2���_E|˝kJ.�@�D�nVc�j�Ȅ�gt�󢣡�*��f�Ӽ���[�������A�C����	����Y��Ev�?��q���`��pV?�Y{O�k�c������!��w���΍Q,>wR{N�;��49Ʊ�M��ʝE(���< l#Zi�ZH�� � � ƳQ<Ǉ�Ӏ,�:��`���%���>d;~��<�$^2���<c�������(P�&0���k��PaJ�˧������E0�1�|����|��5��5�:y0r��hÀ�_Jǌ�>^�?���5���թ(s�k�it��3dwolZ���A�y|�'� Z���Q[����W�z��pirh� �	��
e���M�i�
}��Џ����������E�����-d�&w�I ���ځ�ȍ2��6��q��I�3�9n�s��9�oW��ܼ��n��<L�7i�H�o����O�͏�,䈘�F����ߺ,��s*���s5=Ҙ��ύa,4�}Y���)T�x������j�)Z�k+ҋy��)N��8i�'��`T��
�v���c�ս������a�Z�r���*��DFv���2~e��������x��w� Ɠ�~��v�Kɒ�����,�FY��M��NDn���Л�&�@j4{�,�q�q��w�X�cIX�uR(/
�[=p��:FQエ��:�֖-$�6����JHA��*̱�O�1��4���m�PL�_ �uӊ!R��,�X���Rh�IV?���?W��a ��Q�mk��^��b���-�lȸm�q�%}�5pAg���GV�XH�m�l���ϙ�A�%��c�¹m���~$�
)U�Jv�+g��1�q�%�}y�m�ۣ*_������F�M�Ա��<E����2nS�%9�uqxrW����l�����8<7��Pm�f6w8"��<9F��l�����Iמw3���fn�m��l;+ݤ�s~�S�젃<�Fy�������dT78<�͎�+ۖ��¤���^~[�$!��Qrm7Sg��69o��45ƫPj�
�QN�!MNG�&0X�X;�ݨ�_�VxR�Ra��5�d%���ݯK���Ɏ���۪�D��G����� �2'�i�����^�V�Tc����Qu���P?d�6ʰ�,�!��\���2LC�m�\��~_��?����Fɪ�9��u�(����a�(]���ʙZ�`�4��bR�p6�5?Ϗ18�O�܈�z~�����m�ۺj0�O&��e!ufȴm�i�⪠��B�lH ��	��$���}�iG)�\ ��	��>~;����R|ݲ��je�&��_ޯ��N���prh��:_��ӘF��#�f$��^T���$�򰩓.9��q�]%m׮�zȝm�;�&< �P:��TS���Fٳ}��Հj[~[Ƞm�A�f5r~�!���Y�W�������G��9J��]�|�B��7���&GY�v���Rgg%w��*v��肕�u`�
{6�8NVިc�ff�sɬ`�
�6�N��q�r����Fy�i�wN�K��;@fm���&)�;^?D����O-͍�*|چ��I����ʌ���� ?���E�e�ct
g��s��+�8�ʂ ���g,(��v֢b����+���W4�EjU�b����3cW����U�.���q�a&o��s��(^��<L�X٪@#g�w� �,h��^9k@�f�P����o�L�12	ӕ1�P�e��g���M$�9W�Ӣf�:́��[���
�7~E6+�����]�����r�S�0Q_f� ��2�6�g�P��5�!+-���^��wQԕY����C�]�k�������'6���$�5�P/ڛ��QK�C�iSf��צ�|f�RKkBN2�D�HԾ�T���|z�02����(_�ú��ό�,��i�,Y:}��-"�L42JO�@T���7gH(bz��u^��#YIG�+�=g�m�v
L62F�9]4�&�'2��m0���(�1E�E4H��aC�*iO����n�������n���OF.��e*�ω�����mZ�?يČMt�^}[�B:�6�G��4rT84�kd�kD�f���mv������w�����+��)�t!5I�GfLs��r�%�f�p�r�08|�%�U����>ԕL���ړcت�� 5�z�bjEWV�iDFhD]����<}G.Y�/����p�\���y4��J��Yy ~;�Vk���u�p�K�3X�@���!c�~o�Q��yu�?1ƨիf�W.�8k��pL&2��m�W��T)G�k�6�'2�'�zO�����ճ��HdT�0�	&.�>.�ƶ�H�\"S��k��WQF�{�s$`F��U%�pu=+�h��{in��:[ǫT}���R�;h-�!��Z�����Ɠ��]�& U/L:5?oȰv�[j05ɈZa^��n�2�m_�!a�S��G�$ɕ��V�q�&4!4�I���P��ص���L�Q�~��N3�NM�G�調�9MF8MmRD4Y�ҳ!�����8�8��J�d�2�X ��2��L�L6���Ch��Όaȋ��K\�;k�d��g�o�a�Y���-͎�-<���7@�U����(�o0�(��    ��8K,�����iPFhPm���{lE8�,J�yPFxPi3���u���̈�z�}�!�J���eRY�)PQ�����(2s��r����v%��������Z����%�+;�0���u1&����\�MQH�`�qZ3�JH�%F��&>!>u���Q�����;��Z/���J���[HV4�e���QNf_>�;�V�/����ڔ�^�ј�dZ���yZ;	4�&0`���H{Ԫd��5�n�K0l���b*�Z�7���v�[za�3��0��0&��������:�*��,9O�UD<������"�YN�U��x�&���9�4�yNFxNm�>��)h���+��D�+(�֪�&�&%l��=D}ʲ�S��P���y$_�9k�ؔE�0��(��I�� fW�mS�LW�@>^WJI��);�������8��M=���/da���`�Y#�SrzM�G��GF�Gu �ԝB�wi�3��2���dcGe7\C��^]8��Cꈪ�R�0�u���e;5�!��CJD�Ċ6�
��"��<l�_i�ߨHAU����k�a���}=z�.��la:�[���6���&��u��f�{=p�զξ-6� �T�ap���E_.�Z�+F�x��=R8��+/��E�oa;rN�!�m���LtH탙����B��az�xw�z�=�g]�e�j:���`A�5w���E�m����Zm�����f�J8/L��p���4���L�:�r�Z.�t(��/��1��?�X��o�_n����;�\2��> em?UY�N��_;���~yR��S��b��o�Ĩp�@ǌ��n4��Vҧi��+��݃��/P��VɣIAW��U#/'�?����̪��:Q�w$�?c����o�,��)�;Y���\@R���x`*��y-&{tx�_��'��`DY��`����]��
CK��C߿w����?H�xv�4I�CZ�z��6M���+�6��¦0�(��2I{�#l�=zjWvJ�d�SU�!I�ﯯ��A�	�xv�<+��c5������d��@^RxV������\7�4e���O�b
}�ɅY��_�g�g��>�Yo�gO��us�N��:ic7�;�����cM|&������a�36�\��]���>i�Pv��ܧS�a��?��������O����6��H��˂�'�ö��Hډ����_�;���o�o��o
�WS����X;w��~-4"��c�J޳O"��É���t_v�IO�*Cߟ��q7�cA�_lcY���MI�De'�+s�a�ө������wCχ��[� ص˦�h�4�?dL��"��q�$���0��T�!��{W��U���έ<Ƴ$6�D��[����ȹ���cK�s01�"��+\:ǒ��yʏ�M�m&O��_�Q+i�����/���<ڋ0Ƭ�5��'�Eˎka�|J���a:Л����k�׳+��ο��]āIO��}����T���_�6�"[����l
�X�i���o�3m.��*��	�	�dɃ2-�K��p=��S��>!�I��[����g��u�(L�@Qn�֖Ƭ�<��mO�/)�6xW�,`�j곍۴Y�C�`�\P��t���7BƤ�|;p����YP�B}�E�vޅms�zh �D�j@�ɮ؊�S7�Ϗ��J踋���&�C�����=�N
�&����0�%9�5)	�ßǛ��_[�X��h���n=�ʁf0�0�ӺjNv�0�<F�Uy3x<;F��I�d��_�׃�EW&�P��hz���w��{�|�'�&
��0i��|��:=Q����X�`Ւ0��и�����\����VA���t��d'��ӭN� �>cb�2N!�:��^L�:Ձ��d��,Id�P�r�j3�N� :�ƌ�/�w�#b�>{y�{�)���o�H�CF����/��>tv�S��VA�-�c]�]3���6���yF����lo�2 6�g������F�$g�D�|2�M�
&��u*�%�y�d�!�_�t�ƹl��Ӷ+9w�Is.�'9ԉ�av�(ı�b.مa��~��fM��L�o��E��-��3[��D�M�B��V@d_�;@U	��CMIxH��<����PY���xگq	\���σa���z������_,��
�Ee��\�%�&�~�����-zC����7]��Һ�^��`����ϊ�`�����J�!��>ܽ#C���v����A�%�*K���ǝY\��0�DZ�͡��Sq����Ã�5V���#V�%����q}�I�Qqyj�PR�h^H���_���9T�p�*��i����J�����Ә	b1M�����S���Hp0B��_GV�6Mu��*P8U��������:.�X��C^����0RE�b蘏�I������m����PSA��0�_��}�J�Wb�K��o�� �.����E�U�ԂE�}����b�W�l/����gʞ���4�̱��ߡ�0&#���	��v���o�7�b>�!b��~�P퐝+�ǣm�U�W��sD%����h�h��U'��x���6W��?m<짷q4�e<��*V�2����r�L�p7������̚:[���.Z�x�I�o�\1�Xp��;6�6������8��Z%p٘��2��n�IUyӀ��2�.��76a�ډ��y<3Ʀ���I����_�?6O/�7�1i�q��E	��EJջ�I\m q%�Xڻx�j�xn���dX��]�Y�:W��!㊇iݴ�}%��=hQ�/��bzi&��Qk+�@hTu"�GݟU,{v� dQ�ʢ�M�����
�7m�Ue�cl��
ZbQ�`awRUv��0h��e�x�g�"]y��,3�Ack%�E;��6}��n���z�Qxr+�!=��������X<LK���9������Biv3�&0����N&��*�įC�
�B���������ǸVS����@\e�R<9F�p�lB ��
��-��������tL4����Ϗ�ʬf�&l cV�X����1u���:l�f��z�S�C��,
W)$e�0���_O���R�x��J��oI�F��.�*�I18k'�I�������O�͎qY;2Y��>U�����Ǡ�����?����;�H��b�� Feͨ�+%h�k�����p��&��߮;���I����w�	 ��̪6^�b� �-Wg�N<?FfB�2�������+|�&�J�`�Ù���5o�[�*B�U�4+���%����
�T����!V���87f<��N��-�BN��-��w��R�Pv1��*~��[���(m��u.g�xe�\)�]�M�����1�\��8l�xt	}LܴM]^�燌�60��=�^t�)�7AZgW��^�ې	����I_���x�}�W�3H��a~&������ ��iѺ8HN�?v���a;�����%��(f�3!��MH���-; C���t*~��%tk�
�Ʀ� M�$�F�Luuٮ
�Q<�:��-�W!B�����F����x��G�k7j�9^D;&K@K�.�����x!�Vm^g����Tm���{	��9k�/H��aF9zW<�GQB A����_^~ީϏ��2�mU�/ aż�Ϗ���Z��+�ڹ*~�߭�Z��O��/{ie����`pQ��A�5 t���V.����d���&l�"'�x��5��&��B�ʫ��C@r���@-4��	��x���q�G�)|䎁���L���<���*ʢ�>������Ҋ/��ѲqyL�����I�0eA���0?2�a�v(���C���1��J656���3c,w�eS�_�ud�_����`$w���X?�,�,e!�|�a�28^w����\u>	�=�0�9�՞z�yoY
���g�\�V�NM�2
����Ӱ��^r�u��=d!h;90����;Ah�w�IaG#�g0�z�]k@/=�9�[z��<%�/�{y�����gK��8ٟ'#��t/��AJ�-    �"���e�.�M�a~?���Z���g�5Av3��9&	ue��l%f_�q�QJ�)�#�fţ�ܥV�K�LV����� ���n�L<̯+�<����]�	E2�5�Jk�I�Ȋ���D��0'�M<�IGp��)��{N`#�8�ԾX΍�ҚZ�5�
��	9��YK�A&s |w�D�m��Ԙ��*��6�S������`;�F�F����"f��Rp��;]~� ;{���zkn�hd-�؉��ܬ�qn�C_�_���6c��C��s.�� ����60Ia�V ����~��E��آ+�����x�?z8��*ln�Ӥ�`�m<E@�pIA�s+�&-S��r�0܄���0#���qqtr
6�Ș�d���������Ep��d�-}x���Ѵ�g��+�t�>><̇8R���]Vu;`H+{�ia+%պ��?R�S�B���g��([l���,`��:���}<�;�V#�]2����|�~χ�t/n�����#��B�k�@��d���M݋�3���y+r�qS�@~?�»:�F�<?ƙ��71��
�'s��[��d,��������u�8`���.v|�?��L����f0؄��X��q��������x���qx��XG�1���F�Smel�NL|26��5��W�$L~2B~jP��I�#v_���2��-��GY6�.%&@qz u�t�7{�i�>U(��=|�e�G�[�j�yx����s��B���"n���K^�)4q�2�٫¨�T���
�H�����Or�-r�1�Ԛ!Q��3��4e�&Q�Z�kp�;��琋i]j�\�T5plD��o�eWH̦�=M"��?M�j��E�vRuk��Q�p��������wx�	�Vx�`"���V��q�*��-��嵠p�c��Q�T6���-4,S���q�f��� ��/�M�~>��������(n���>���$��-���wZm�3���v�������|܂������]Fm�C�����m���Ë82��ɏiW&Ю `/�ϭ(��K�|Qa�Q���C�����E^��na�f\8Y��^d���efgg�r�ɎTw��ϱr!h��[�d�,�A_vJc����� ��&�fm�p!h� �>nG�6�����e��e,�z��	<��5߆��Ӹ�K<_jh:EA���1҅�Uw J#�����vw�2��d�0���U��փ
(�h��癿9L�2��P>��(:EV�ӵ�е��G�����6�q/|-׺hb=��F/~I���r؉�F�/#c��Q��w|Qٸ�r5y?$l���ض�vҝ�*�!fl���zg���YYF嫒�Ӝ7�{�2)��0����g<��_�mv��e��e*�3�K��Oҵ=�����ec*�*�M�!������������U]���S��BdlQ��2]H/E��p�O*�d^�Ga&�Q&V�������8���"D5$�?��ć-��T2J���e:�g��c��U�	w
�aZ�&ާT\�Vvŝ�T+#T+_3�|g�I����b���Ԑ�~\?��U��4+#4�t��wj�=��q�"�$+�$�����i����F���=�o0㫰b�v̬2¬r	��fdA���Z��5�Qe�QU���<���u-�x���͑+�v�ΔE\11�(1��oE~~)b-�Sc"�"�}�v�����qъ������X�׊��*Jܙ�g�U�=3�����ʿ��i��\dsbP�zv�!��_lޜ��Df^a^%�$�zRN��"���+#�+�D��?�F���ד��k�a+��)�]ra����f�0��۪I4
�CЎ�d���D�1�������'�bᦜY�`�U�:������eE����\r�����+�Gbj�j����B��� fU�A	��7D�˅k.iω��k��w��A�9$�H�G���b���r�+'R-7^�2���VV{�Qv�L�bI0Bt�Ͽ(�y��_-��L++}���z��=dC�"2��+[i�8�#�D�ػ�}�bz�zU��P$��ȑ��	������D�i~�G��É"�_T�e1��
�*�$Kn*;Ȟ��
�����#����ݨƏ#T��O��_ id!ޭ���'k�'���
��6������[�ۢW��V֨\FL5%���榊�ekS��
B���;����8nN�yr��5a��5���6��A��B(
u[̹�F�fK�~��Vl��p�Ea��:TG;�Q\��u�l����g�3ר�sc k��V~.�� ���6^v���7��oCY�[U҆~2�ɶ���e��U�K�&�*h]��[L߲&d����;K�e�.g1K�j+�䎑�p�W 6�*-N���'��"�cZ�Uݩ��|�X�fY� ����j�Fag5�,�MB�;JQT�bV�VV���ہzD�-��1j�l!v�N�MS/��dF0X���l�=�"����U�Z�̲��J25� ��ߣ$L�h?ƪP��It�Ck�@0o�2����JS%�������bi17�
7���n�]���N�2�����-	f���H2�Oޥ�H�b������^����vK���e��Հ���}�3
G�(6d11�
1�$`�q�v�]Q�b^WS��6�S3�l�J�	fu©}V[V�m1����ԓ�Oޯ�W�Fi�܌�G�w�Dε��bz������?�I��02i1ˊ�%���&��hC�<,+<��e8��_�2�L,+L,�����P�Ћ�0,&c�F+�H�?�Y���lvXad�T���^�(?o1�*+�!"�/�IRܕ=0ˊ�U����<�ˁ�:����\,+\�DN��/vr�+�˶��Eú�y�14ڋ������W��r�k�7)�m��o��+f�[|(ީa83���*�le����ko���e�>����.v��R����an�nU���n��̪�N9���Ң/o ��6F�S�9f�L���*�n��Le�*Fhl�b�%.{PeR%��ǨM����3��2����c<�����we2�IY�I�w��6-!��ߘe[�E�.̓����Α���1J[�ś����Y<FS�m*f�l	cV�QI�o�#�:"u�)�M���D"�>-�MY� 8���}�Ԕ�=�"��Ŝ)+����iZN�zO�oJ�GYY�Ŕ)������K����
��b��mU'>ӟ�N���/��bs�!���k-�*sy1��*���v�ͬ֎Qk-&QY��J*�d��捸�q���L��B���d娑��,���&:Y�7$�#ܘ�^{[T#d1��*�)QV|P�S�x�o��D�Ջ_#��Ԧ3r���$5Bd��`���Hj-��2�;d��Ge��8b�叨�ceOΦǸ^SM���a�Ư텘�d;eM�?�i����N�bz�zS_���_��*��`j�jSR#��#�L�UVg1�����qқ���>8SR&�k1��
��&�fj@\K��� F��R9'5@�߻�6.Lx�}�8�У6�+���k �>Y�>�=�HF�ӛК��Ly�b"�"��{�3~+�C���^�5��z�E�L�b:�t�8����(D	M����A�Sw�����e-x��PV�P.���9_U,����=̄�j���j��II����A����?�q��s�tV�X	Q���P&)8�')su0?��H$�B�&nẃtej��Y+��Dn��oB+e��q�i���F�{�m�-H�t�3�|V�y�5��#���ꦝ��y�_��s�����՜{���E��>fmڡJ����ݯ�]�m�<L'U>�ġp�{\���96��\&C�5�����h��anu�%��=�$������v��Y���+���(7�ٱ���0��6�"#Q�q���,��f��n�a�GoR����y��\v���c������x�gw�]������(���	�d��0�7W����@ގz���솉���0��YW!j��e}����6�C����܃5m���#�W�ޟ�1�~C    ���c���4��t��]=�����z�0�$v��PH���C��[b�1��~<P~�9��egqe�10��ܹY8�z��%�{��c6�ʹ)0<��μ�֖�NBAeӇ 7��m�RWb�y�ˏvH��a^���H*T�o?3$��0/S+s
�u����3�
�7+pK|)ҝ�19�?	�uڭ8������/�_}ޣ?��YJ2���_���K���>1��e��]�X���%��߻��@�
�J�'>���\�O��f�45�}�[CQ�3�2�E� {���J�eLV�L��E;���҅�j��7��O~��:��%咕�i0 k���E�Ƶ;���4��[�Ï�,<,����!��I�BC���MX�o�#�������(�������1�E]�j�>U���+۠ ˅�iݺ�Xxԛ�ɹ�xnk!�Tm��?��Zۭ�����t�!���v"���5f��҅NlP��\?��[�z�R[x��h�*�2�y��|�` 7r��Dz�[Dɮw��r7�*�9}���ü~�6NS��͂�xv�Z�����&Q�޶��;�d��p�+O��ƭ�����cU��܀{��y�/��(���`�6�\�cpFF���D�1��� nz���	�f$Ty0lC����u@т�쁕=���0}e��Z�� ː����EX��W0y�Sl c�Y���'����Kf c�i��Oo��d�$����`!z���3C��kj�/�v��i}*�kV`8;q�{ L��R���[h��0���hv��?�89�i�*�!�σ�$���)�p�o�)4�Q�q��k��_�����}����P��H�iǽY���=Ȏ�a�8h�r I�����CR3��ɑ��g��PȀ�aZc���rU�ȥļ��EI!!���:6,�@�֜?H��ᥥ�=n^��+��@JL���L^����q�x!�tH���X��Efü��;n[�:��=_���*3�����u� �MRO�.��`�|��!S��yd��Y�Z.��T��zgw�˸y�B-�M���F0܅�ӷ��U�YU޲�����L�@����s�gjl���X�n��90V2��О�v�	�����.	;<�'m�	x*ciص���i�4��>�w������y@:g'�}�Rd]5��4?� ������׉ο�'����ۑ|9�I]�!����[LV�X����g��bO}�i ���W���r����|ar/��8��Ҏ�͉��K�)�O��ӗ�iE)H��a޽��`x��	��^3M����8���>��p��97���`w&���'H��a�2b%����2�A��6j�Wu_G��V8�}	��^�hM����(�� ����s@^O���(�GU޽�p���aB�'-EC���� �;<,�b�R��~쨆�*g�������I1�5�/�I,|%����L��so������� ���t�;���o"��Ku�i1R9x�V�0ا�L`���J�d��1���O��8�ij�S������:���q��D?����Òt�����?0�:%/���s�)>�� �U�7�}�x��s	c����p�(ӊA�ĝ�x��d�luO���!~R�7y�ɛMn�T�'ݎ�����0��/�"B� ��%蘫�"ʸ�s�@�����-�d�i��5�B:��E����t�a�Aq��^����f��5��J@���M�)���n<̹�8�1�8�oq��6�Ţa�i���i�c(i,��qK7&6}O*�clѝ7t�aޕ3.��$fB,;�5\��!bCE'B�e��t�ϐl\���3NP6�ߋ�3�J�Ҝ���T=�yo2����p<���2��o�&.1_���ª�:��):TvNa2�Q2U��~�*�܇3c����o!"C����%Z+;=&P!P�fI�`{�-8/��s���s����������D�>n3c�V;ig0i���:�r� ���ay�0��(�p�<̾	�U6t�>mD.r��=��:�K�PVB�'K�9z���t�_���(����0p(%�ߖ3K��/-������`2�Q2&�ILj{��+)3��/��6)r���H�g[�C�]�x8�`͞�4���0���9p0������STc�������s��.p�8��2��x�����|m+[x'PnW�y��
�����2��8�p�k�ڼxPvW�Z'��!�o�A�����M�x�]p����;���2����Y�ȯ%����MV��'ex��/#ԯ.��Dn^���Mj#��E�i_�Yn^c�L/B���ϡ��_�Q��P��@u6���"m�<�J���!i��]��G�2;�B����=\
a�%�Kd����n^ո�!3��.�FU�#�kKt�,Pܳu��/$�.	ޤK8/$�;=&��@�I9*�ʚ�v2+%\�K�#�T �X��7q�7fƛ��,<��=3�����Ҁ	[F[��#�w�^w��~��"r������%�G��̛a�euPW
mp�7�Xs���H��R`S��1΄��%��l��!� 88:G�m�mu	)�ԃ�!'�K��׃��5�i�ͤA�"V��ZF�Z��?�9��.��e��5x��v�,�:S���S�L��[�N��H��U���Ya_u�w�<\�b�lJ�P�[��ϙ6��y�>E0�ZJ��3#����� �G�ie�i�_͆䚷����0B�*>�!�g�"�)����TIdo���r�Vn��O��zU�D���%�]8?������2?%+�0������>�}���OGq�N8̩2ʩ�b�T�T>%��YY���d���0Ĉl6n��a�}��wwLn2Bn����<���G�H��N}Θ\@�F�"��
����d����C��Z��[�\���Z S��P���� [� ���b.\���č�浈�W�������0�
Į�cr�Hv�}��6ysz��0����F/�?�M�����\z>f$a$��W_��\���p56�0�����~��<� SwU�a���i衈��۫�0{��N����T�Ee �oc�'Uj�����J�S�L�3��s��T Ō$ӫ΅��кT?W�J���x�{�f��lY
��&^�AJ�R)��������-�(|�)��B9��S,b��6k]h��ВĈ�G�����\f�w�"�8q����&&D;��m���JRJ�~�$ؼ����Z�*�6�ͨxn����sY}��Go�X�J���r�
f���8e%v���I-�n���Z(+�7'���%Qy���Cڟ,5t�hʏ�8��~��p�2�̼Q��O憋�V`���7�D�h~x�X��$��f�)Di�Pj �IF�e�ʧ��9�-W/�r���`JWq��.{��Wq��k�|�y����r}��
����	�#���n��ܷ��ü��8 ײI��2@	.�w^�W���7#�f}�/:�K �N-J�-{�a87�7��4k?�R/����|�i�g��HT��ǁ=�I�h"�a�.NqӪ����?��s�{���)f%ċ&�9s�F��S�~C7�ݷ�`g���}.sN>Q�g��G��,5�h ��°CD>��SI��3�)���%ku�v&Q��f,#�z�.��(���R�a�&)���x��|~=��X�-Ot��=H��o�~;Z�6��޴�u����f��k��Na��/Pa��4�����ټ����]�r�k���^9���Z_y:���w9r���?1�� W/��ot.-�Q��:hFV*�u�<�2V��8+Bz"m�w�tBkR����䵂ܑ�G⹳10�`��������N
j��~��/4�]�0���qG�U,�N�{5a��0�7�͵�|��\��0'Pv�
�I�>z7���z�簻m~�X{<������λ�a��{�1�^v���u!>���I rzF������~x��#֔��\J4\����@����i�    ���_��"�      c      x������ � �      e      x������ � �      g      x������ � �      i   /   x�3���4(훙��Z�PZ�����2®���(1�$F��� %      k      x������ � �      m      x������ � �      o      x������ � �      q      x������ � �      s      x������ � �      u      x������ � �      v      x������ � �      w      x������ � �      x      x������ � �      y      x������ � �      z   S   x�3�4202�50�54P00�2!=#S�?�4D��%��y
)��
��ɇWC$��qh6ĥ��$3'�8�$��fF� �m"�      |      x������ � �      ~      x������ � �            x�Ľ�r+9��{��)xWYfg�3p	.RT�S��e��u�N?T�؁A�Lϲ�}|�kO�D���?/ӏ�y�?����qOֹ)�HOƚ����?~�Sݜ�e���|��=�S�ܔ��������`|19�f|�Sv��麫?�q6���!�S�;yU��`�[^x�&��r���S{��f�T|g{��4����S�����1�vg����W=\/��n�C������'p�O`��T�I�Q���������<����\(���=����q�3�s�����`���7�ys}km3>�Կ�0��j|�K���Tl���=^7������f8�S����=4Q����n��~�s�������gӃ�g���?;1)�lCt��8�]=֏=�]���?T���+7���䝁�������|���1݁��;��Ŭ�������c�;>�[�����V/�����\����k�4);+)<pxUƧ��P�pۻ����� '�.��f��)�� �?��H����6�'8�7�o�αM�oL!�����-�Z�X3�N��׏�|�[���-ܚǽ>��%ǋOc��G��i��d$��Jħ1�Q�^+~?_�m۾�M���BO�>R�4V��6�^bf�MZ�A��s�߄[lo������/ѮTpc����둼��a�^�i����ݓ�t������n?��s�.��+}ҡ?6nJ�q��ܰ�.�6.��l�k�E�=����B!]�����+�m
�۷�|m��^v��#����$���q�\���bO�Z��v����S޽�ﵒ��"��2�?�v�������cZ$��^Y��@/ܷ���b��[�T^G;�~1�n�i%����X�]}E�0�i�X�]��Q��풋��f�i���q�:��X��`��'0�/_�	$�r���#����n��?���v}G��^�tɫf'>?��*$[*��ҝ��T�Q�_��ִ@�i��i���Z_Z>�{H��X��?�\��kw��/ݩ��%����Yw֭c�v�|�HN5E0>>�wW$�R�g�����su�[8��Xɣ��1���bj�o<��4�F���F�NLe��W�|)|��p�tN��~mz��C��;�f/|������.���|�^0N�a��V����v����������A�kO��؂'�P�6?��x�����
���(W(�_;P�Щf0��_8>��P�����ͣn]�I�a�������|��9��fGiR]v��d�/yӌ�� �p���>6���������Ο�q����I�!$��,��;��m���cPe�پ]�cR�l�jq.KckL�����ە��z<ny��RU�jq�@�����p}?�yI�޹���S=dz�Y0S�{}���~ӂ��k�x����cc�n���ڷz��،�!�|ê<�"C(õi�����b�k������]4`r���2�(ׂ��^-��Rċ��^8���I����k�a��aI�����yύoL�O��v������n��%?�	�}hc��-<@�S^�����5[�>B��Ã�Ӝ���ٹ�7��w� ؘ>Dh��򻗼���	��H-���u�iA16�#�b�BKmᇗ���p9��شc��<���u F�C#�T�H�b<6=8�ޟ^��4o��:�rҭ���&�e�{�~��[.r��M/�����$�"��pcB��z�/���hMl�d�%*�O���)�B^���>�I�r��"p����xn������ׯ��c
���E���_��L��fv�
jo\�l��"��v �;��|&��(����o��)S/��,T�yߵw��-ߺrK/H��\� �^=���FK\;Q�aE�S*��1mZ
K��Q_)Y�Mv�|-JE�T(r�]��M����Ѧ���;���Km���}��(�~�F��/���l����f���ҳ���랠(ՃmN��:?u���<jO�K�Qt�	*�������ϗ�FA�{|N; ��Wu����R%Q��'ڲ��%�{�Gɻ��7fq�Vj��gz���V��X],X�����O�]7疴��h�]�u0���4Q�~*ɢ�-@�q�>?��G�s�Pہ��g��;��FT.��/����Oh��,Zr�l����T��a�K=�G�v�>�sF�cM���p5f���3�ʰ���>u��$��>�C2��Ԑ7<�>w���)��Ir�	/���j~�@D��X��v��D�'%ɗ&��4�������U�2����$'������q�/,5i��`�vW8ɝU���S��9�9oy���Ϲ�<z�"^�|i��V�2��m�<�|�oo�6��V,��:�$9��H�nM����\����?�gJ�Ve2�_L[u�m�ط�p�ӎ�����VF�/�D�R��.A�&U<:����F1���:K�4G�C[�j�TwO���3�Z�,嬙G�r��_�oT��4I{�#�C����1cP0Vi�Df�X�^���lk�q��p�����f�%�8 +^����vw�*a7Ǻnր�U��g)[����HPo ��3oL��)([.&������2y�g�]�qJ4�N:�kq��'���o-a��m=^��X�x�FY	��$5���tc�?��2��`̭u��,N�N�fl10*�:������Y�n(S�a�	����$�cp������v{���O��8`�5��"&�GUj=��j鍌�'=�Q�b�Iy�?C�>�[�[L�Q�纺�]�ʢvA�H��	'�U��-:�fy�P�SS�\D0LT�v%l|�?�xʦ����z塏"�l�8hc�������>���v*X�T0x�D�G�խ�)6pC�5h��ErP�4_����[B�aqr���"%}�S�z�[}�;%m�,2��W�L��G)�b�]�K��e>��x�^#	 e�rj���G�I�9���������6[��L8�����bn�́�
_Wa�>+������	b��CS�n.�ێ�_ �z�,k3����	NHsBd��ʳ�X^���)��OR�Wx�<;�:�S'O�<~��֋�T�Q)��_��|⋥��[��N�$��%����L���[N�\�C���ź�D�]��(q��Aoz<e�E��%\��T��=��zy�n��Õ�H��q jke!��E-Z4ԑt��,]��W�Qn�\�nT/[��?������J�89m蒘�&NO�&���V[��1|�S�]�Ya# ��S�2g�4f�O( H����,��ё�S���T��i���?�3s�&x��0������k��}�x
[�� \"���Хz��[eFFMTƧD�1.���)o�=J#R��D��D�0#��y��Ӆ��7��v8/@���A�����[݁�e�|�Ѯ��t@Q��2�rX�SqoXDE�*��{�7ڗ��(��O�B�ȳo�Uod��	�N[�aDJ��(�
2_C|0���t��Ku�8QH�Nf5���$k#֓�M�D�N�r�:�X2{���F�`�0EFqa�J�2|}��p��\�A�����R�G�Ԟ��U�� ��5���N�k��H|
�����	�C�zu{��~���o���F�?C��ׅ�\I���P�p<(��zh�L=Wj+�����^�)ETj��֧�oe�tX�(!�}l#B��zĦ0Y�����S|�*���=���dtaF��d~2�#dw�M��>eJ�.5����
�"�/;Za�H�����z�ݪ��7��Ž��lcY	v�''����_y���%k)��ꁖ*����n�]O�5��Z?��˾/Q��r^Ҿ�%_���1cE���(��}� L�{#p� ���@�KX{G���@�������p"�84 �(f<v6�$�-܋]�+��� &j^�'˓�x[��Uړ���ї\K# ��v��u~�o\0߆S���(���;e�/)�g�Q\�txK(�SQ�g�C�X~dU�K]���{l+����F �N���)A;�@q�
I�l��Tr�T��N6MGhB�pf�U�����E��
*�L�E<�+=_���۱!�	8��\#ؼ��    g�w�dǤ��3�ȵ���]���`Д���}E hc�G�>�萍�̓��MW�Z�Q��8"Pn�8�|-��!d�.GfD�(�č�����q��7(il�u{�����C0�ŏFd�����Y�������>Yeݩ�D�):�8���u>��=��iCČ)
֢�����Q�tP(�W(5"��62�vcZ o���~�7�G��vGO`��)3E�o�ˎL�6�4���k�.���9��/��#�3�ޫEPQ𴳌�!u�.�#���A�$fDFQJ|`,��H�����LD{�~��h��+
�� ?�f+w�F�S#8mȫE����"+L���9��bw#Љ�UcHx�b\m�����2���ܧ~-WιEJ�/ ���b�Y�_���6�}5}���tJ���lf��G"��!��G��h�SEH���&�>��G�T;�c4^�2&��D�l�3��#Vl���I��!#�&������c}�n�'��=�F$Q�&>���v�C�M���=�"LQ���X�i�����~�����{2�'��}���s���%{;�u]ٳv�;A�L[�#��<�P�9�ө�}$�k{vH�Fq�U䟌!Y�-�`����ag��IX<���#un����m��rv# ��y���.�h9���w�� E5_�
-���j�U�d�.�<���[UR9oHE��຦%z_��P#��7���N��S���ۙ.�N/�u�$f�XƆ:0�.�!��&�ݢ �H'�C���J9Z���\^�q���O��C��,T��PQ˛
�֜�:^񆍽�vp+[-e�y���� K0H-nSo��*OdQ ���؄����>,R�%b�D<�p�a�v�,�k^����v�-��Zf�E��?�j"W�%��UG��#B���9m��=ʑ6��}��l�TE"m�fl�(�T���Kd<�2����y��{ό�F��k��j��5�@*�
�GZ�t[�۶��+�Ne��%n�G��G���k�U��c�\\�%���t7�km�yJP8�C�#��m���uf��
j�&.��䟨�������O{h�r��M#�@;ehJ��#�H�hT��������6���u
8%�C8��Q��b�g�S���1��.�8%��x9�"�vFTq��+��e�e(Ą�)���ViN�u��}����<�fw����/���Ͳ�?fʤH#���!ڢ���ͩ��J���<�r�݊l%�>! �Fy䆈���uCe iv�g�
���Y�''��w��=Ŀ��2~�㈳n l�R;9��ņ��A���ۙx��u�K���K�&%|A�d�?������[�+�h^�Z���o�͉���������X���sB�����c],�����"o)������z�m�T�q~=ߏ@:�s#宑�n13bG4�f����j��Kɑ��dD�|��(.�3q�1��]*�k��,3q�5}�i��h_�2m	�'з�+�U=#����0���a+"�2��=��=Шs�7��;�.TX�LV�.�x�E$��B�Kk��E/@�`o ����������X�E�a�����5G�CkS��_�t��*�ր�l�$�$�Zm̊�Kd/��@�����sA�m�3/��G�z�.T����V�0�f�!)�9�����g�{��E+ �v�&B�B���DϏ{����i�����	�\�J`
��_��u��t�ɨ?�b
�@4eAq�t��}�`��iYk$%���X@�}CXFV�b�	=����V"ܶtxJ/K�2G؊\��)b�kKU�ҏ��k��(4{h;����Ֆ~���W��f�ٜ	���p������Hq�������uEJSʈQ3��)�x�O��K��PE�0/w_��v�����͗�"���"?��P�x7z3�1���VD2�1�װ���:���kz6�)h�tq�ٳDZ�Tg
����gA{\�
L&�C>��=�'�T��)2��+B�J��t����b�����du�$VD2%XV	S��͸������m��E�!��;�5 }~�	��ͥ+���Ld�!��dF���G���P�.=@�{oES�:es3	g�_����Q��}_v��U.�	4&��N�D�C`�^�ϟ���Z�:��
0��A��p��#��-����������/Fy���m�X�ͯs�m�+H����e�d�+e"�)��N����kα��Y���e�5.ܑe^�Ǿ���V�1x��w�F�i��Ǟ\`�2(Њ,&�ؘ�#����F�f��3m�[�F�Hc
�����HX }�ڷҬ����(c���d�&�K�Q��^=}��ʒa+ �`��D]�2��������s���4Z�==V�2e�	�4Ct���xI�sC5`�F����LK���?/C�3�@}�e�V{w��L1��RV��-����<_aE:Sd�m2�7�@��Xo�tk{�w���|��2��L=�����y��������I 5�t;�X%[������{��trJ?qٜ�r� @�</M�I4�����/���j�ΰ��?O�|��1&��{2�7l,,I"�vI�H�DM?���ZBd2-�$oqX ��G�򓶃�L��ΖKy�]]��vA*_��	�m�Y7$Ra�`F��2�ЊP�`Q?iC�AJ�mg}���z�>��ژ+��e".�����W�H0}[���/�D5�� ��*�{����0pG�zo�o�L�S=�S׊�`�Ac�-�"�	�0T�eZ�4S�&ؾZ��H&����J��im7l���T���9+BY� �'И������V��$P��,I�[f�Q��kB��@���W*O�"�`	��>^z���q-�E��!�*�����w��7^.�xI+�Z�M�:/�>��
�f'0]yJ�
L&.����s=��}����ðn�X���R���a�(�;���m��3�[���ИhAb!F�+���ؑ�n���(L4~�����<܀2��fV�/9��̍�Q_gD6Co���l��}���1L_|��3K�n�,��2�9���Cw���J�]�ɞI���0���{�"��9�ud�����WˑB�F�_���:x�4͹^�.|Vz�7�1ʦ�<&�h�\�cz"t�o�!�s<��5%�����g儣kh;c�է"���[�|��Z����t�/U�0��0�-����m�G�PG��v�$И<�AZa;�����S}{�?>�uOA;��L��D�.�:Z�J��_���R�iD$��ERAi�	ݨИ�+���=Q�i���K¡��2��� =�4�I9XL �����>S==_og��5�%�յ�	�o�Bi���!��}�.z��>;?�NX�a˜.�C,B/r�V[N `��;�"��YA���c񯡿�[@���*��x�@@EU���s����.S�'��^��{ry�M�5�թ"�)ev�v"z���C?
����*���`~�.	3koN���s����������[ڡcd�h�;*��J�DFNϯ�Q[r��;Wg�WZ��"�"�A�\�ga=�G!�^�k���Hg�H�iϐP����h0�\��ܖ���-���݊�&��A&*�p����W�5�?f�ﶻ�Gţ�`� 5P��ܣ0,���(s'����s��O�� ��@����(���Ӧ8���(R��K�0\���`= S����E�zQ���㗁�c}f�_6���	ئv�4L�U����/�R�cC��|����7�<+���(����)���v7��Q'�R��K�1�nƱK�aw�rm��'Kg���zZV��ae�Ն�9���<m�6�.�t�t�7����oщ'.N4}��N���>L�q���*y[W�d�X���i,����G�3tN�7A��߆z����z�n��g�|?v���f'�����:���uO��7ɸ2 #* :�-B��P�p�^�� Cƌv��+Q��L7w�ZL���<��l��R�:֔-	�|~H��sG���/��`M��X	�$�i��OB��vA��S�ԯ�]    �t"�)[�e�X��)�`y/��TM'����Ҡk�+�|}��3�WY8������մ���^�;l.�V�8��HÍ�"�B����`~�h�6N�	l& WSH�]�s݂����U5a�[yi��L)R56�xh�e�?��I�3ENf3M4OW�kל+�����ڷ��1�?LN�ϊ�&�,,�4����[Ƒ쨬u"��X��_�s��[������	:V���ŋP��ZKX�uK�d�x�Nf3�.! 6�w��ٳ���O��P'ҙ�#]n�h�[�p@�՚��L��\���]����z��_�ų��`����	M)S��kM�&�}�[6���QF9���8h�2К�zZ�ƴ.����<ԉ��ăE>c޺����cY^z�%ʪ'O"�i�)�$�?o~}�@�F;}QM�PҊ�~jo�x}������g̤,�q"�)Xj�Ǹ�d���L�7ħ�x��ML�gJ��wʻ��S��#�O;��M��`~��끛	`��T��fei1-)D9ߟ�{N[�:߆V�B)2�h��[$8o-!˿-FS�gd^�DXR� ���-}�e���WV�;ڴ�w�n��&H�� $���r2�)���?�g;tV� ������N.�	+��Gx��5�W�i� �����K�-"�:s|Pֲh\ܩm�Lo�x�gZW�L].�-l;eŇ�MPy���m90`y��,��DlS�谉#��i�����<�H���\��	~��z�k,V7e�>��<k��a(�m>���~feѢ�M�S@���㭾¼��m_�(�OSq"��,�M!��?~Uj������z\#�� 0XH_$��ҙo�^��	��֊��
��[�����]nՄMzZؙ]���x�~�_���Hk�����FQ�q�r��Ez�o+Ga��?�_(����]�l�(�W���DTS��6Jq�bD�f��BG�t�=c􏌨_����|:��JDK��	�Y�2��]�׉�V*Q�uj�$�=�PR��&�41� B��やODhXߟ�8E� L�5-H;�`v���=:�m��vwU 5�tW�1�v����.6�'�݌ʑ��hZրXZ���M�]��?�e*�7�@j�}̼��`�wy�'ҴEW�	ړ<�i��Y/�+�/-����l�'��V��!2�2���z����|'�]/���l2'��I����� X�G�'�2��6ݔn�pL�>�Ҡ�����O0�$����7�c�0@�	'�5y+��vsn�-,"��a�h�q㫥� �Bxg�n�/�/]$|i�)SڝHsʞ
�&b���������MZ���8��uM�����<_���7h�&�9�T&� 3���a��mɫ\��� O)�ʆ��-dM��m�F��Q����-+��T,�xq����t��������yY�d�yZ�^Z(t!��	�))���	V ��Û�<�Vq�xhu�~��hW����Cx���=�A�DO�i��HN`?�&�S�d[��7û��`�v#���#!K�s��>^���[$��Hn)��=�H��\*��|���ۼ���wd\�<��))�~������Sr�m��I{�QD@y���N�7�����o|��'^�>e��L��"��a��{��S�wD��cb�p���y��R3h8�׫� ��5�\���	���9܁�ⴑ2N@y���x�`D��(��I=]�O�efZ��aY���;�@��9��ZW�q�=	8�J{/���LW�1��iYs"UE�.�oH��Dw�A @5�i��!y1N���-2c5��CE�x)��  �X�� F�m��ʛR��
���P�hA�����;}1���ؖ���A m���{ԯ1E9��O���.̤+^��}��[����;��|����ͳ6�o�!���1�����)$h���N��Fo�y�N�=��)��u0���]�?�hѥ�:[O��v��G�P����k�
t�9�BdBoPu��L~�}���P'P�P�,R��X��f��~i�����S�_`^�Gni6���Hg���]��N����^`9��4�Tt��3dqe<�*�K��s�6f%E?�/3*,�Y���8�� '�V���)��颷��V���ɩ��<�X:���%��@^�8Ai5�7ğy`�,�2���o/@���D����o��~,g��T_����
(8(�1F��Ϭ�֕�Y^�9�&&�G&�>s%ؚqp]Y����'P.��)�x����ˎ_zOU��e����8H�[���-w��a����!'�=ǸUU�F��S����
�m���͚��f�+G&:���rh}0���Y=>pN �dޣ}h������5k�� t�H\F��"�k�+O�7��]��	L�i��*08�K]�|�aq�����5���	�{���޵f���^9M��}�,�����vp��HP/0�����ē���UTc{�w}{���`�4v�ZH���K�����F��u/0��v�n�_�gb��޵:�X/М���xn�C�l^�޳�s��pr�v� ��ޮ�|� ̡��N֙��A�"ϩE>�ޖ��ܯGWs���u)�)�-�W��G��7K2����`]�b��`��N(��wĐ�TOˁw��jvkߔ��8RTc=f#��v;�������i/r�"��JG�Z�RYغ��w�S�^@9A�	��Ρ���8l;���6n���#���԰���)�"�t}{c�*�Q�x�ꔹr�"r��(�x��\�'u�*0�ڛwԇ��� h��7��r�w󙎾�y�J�@v�ԛr@WּL����E?e�A ;���3��-��J��Rq����vj�g��T�����(�{?�>3���S��xRx<R�6�wSF��#@���I�&m�q��̆ˣ�O���.�9�Lv�������5S�6^$G�;rv�*�������&�α�N��/B8�o��wX㸭_��<�ٔ�#H�,�)CxƏp�/�c��y��ыd�⹱3!��!����^��) � ��y)_P�u���{�j��&// ��Y1K�o}ՀZ�\p�5��U��E�Sq\���͇�ݣo�΢D�Sq���.J g�<����b8/J�Ly�Xv��ҵF��<���΂��I��?-�GSVeT�N�y�H���Q�-s���w��\�8E�JF�^K�F�,�H��ꡀ@r��Ҙ-n���M�-{��3'��3/��`�* ������d��C�L{_�I�Q�5��`��'hϾz�Ԏ3i%,vsNP��5���B��T6_tP\�),�j���|�^��>�W_&�D,35"T��]���ǯE��ͱ���j_�"�`�>�"��k|bAY��FY�""����*���Ҏ��*3T;����� Xn�#���[���\�{V:u�D�(/��".i�@��Aʫ�w@�?�7�ڤ^����I '��r��4�u��u��#�ח&�}eF���I��4r������x�<~��L��+�ิsC�:�����7�-��'=�� G����I��/�	���:<�������~����\*�j^(�@|��'ݘܙ�=)��dR�+s�����:�?�^����K/Z5Ft@
�%�� �'�M=�"#�#���3��ʅf.��ux�^�ŋp����E��}����ue�V��x��I�$��������0Hl������h`�J�p�atq�j��՛����Qc�Ն����q=P4�'����I�G�� N8�E]�� ��y4�L/@� CH8�0h����w��<�k)�3�4/(^�/�j"��Y.���;2�� 9սͯ��y���P��ݠ4��a�u�^{�o5lx+�D�A] /�� �@���P���u��ys`G�.����:�)b@�du߱	�%i����y�+����th�L��&���5��Uf�s;(�}�ng{�v��6�x *e ΰ�X7�둿б���V�p��жT��R�CR�"/�Mw�"�Ƞ���ZO4�s�}��32���l�w�RO    ��z�ƙ\��^��餷�1�F����Ƶ��GQ���pG$�e�
�:C�>�*����hyɑ@
O���4�����eD�iˀ��z����Lma��]��1B�'�Fh��:�ꝷ����n��*_�"���i���E��а>v#�QǏ1F�U3f���H�l������GE�F-�O4�_�7��~z��Y��<O/�����@�}Ǯ?_f:���D,���RZ�!(P;����#m_�خ �w3{kdY1%���s�n�2��P���2S˅���K[x�|d��W���(#���5�6����x����W��gd�TX�#ջ���e��5�8#��"a������}K<>��A$<�r�.�@#�i��,<���Bf��C�A� ��p�"���Z�����^�teNA(F��.k��oy�"��:@m�Z�p���y��b��}�c����?�8hN%G���\[�����z�GqPb[m��{ʆj�8����[���'d f������=1d��Dw���
���l�0Z�-�	�$�t� "�m=<��=��]��5�i�qJa82�M�!��G���B�{�ꖱ�������y$m�a�F�>�f�>��'����c�Cy|�F��OTn
	�b������p �*=F?�Ў�r�#@�`����UWw��4�G舲�-�t�)�>ެŞǹ��l~��0S���C�ܬGr1H&��;�hc��@7�1ü 	�����oB��e�'|#PP�/�*���~y{X��w�m����#�ɠ��U�-��x'���%hK���6j'�Q��)%<b����~F{n>�T#^��V.�R9��Ř��F�8#�C}6"����|���a�oE�f( ��P0s=����`����T,i�/�3IP-Q=$�DqdY�ʔ� ��`E	���p���|ԗys��vd�7���%#���R��|��j�� �����߬."��O�)�u��¥IX[�S�cN�3�A�������y��:�U��E��7l����B��K~h���6�� ���x���%��7�ǁ��J�rq9����V'��aP��@ A�G&��M? U�Q7T���	�ze�aXFcȌ�xlE���+��"�2�Z1H��ݫ 1�e,��(	ņxh�Z����D7L�$�i� ����J�e-�O?P����X��>� ��`�Mg�D<,��e=�a�&�_�R��p���a��x=�����|��o�H2j�dL���ai'�6�g��ѯJ
$����^�[7�;��������a�F{qR�EKb_��;h#��nd��a|�ҏS&�ݒ9F6q#dz��[�v���2�M�����*"_LL���P���Ȼ�n���84#yP9Ն���?V��t�C"�l�(l���6{���lw�$�L�0� ���3>�����$�?�zm���3r���i�߶��ޘ��e�@k��{"R�-&�*ʶ��t�����io��]7������Ee02�>���3x �� ��� b�L Yʝ���?>��_l�xaj��đ#]��1��2=���(��A��,-k9�>����)h������(���@frO�lpN�f�����~�BN$2�H�{�d~��[ $�+O���/�9ݶ�� Cѯ��Mq}�d�L��S@N��
�����km�T���W;��L`>��L����λ-�?5�-��N�-.�D�u�����Rtg���Ǆ�>
����v���Ο��
oJ���"��ҹ ��vA�MĞڱ~̸!�r�I:�!����G@29�*J�2����zI�]Ȼ+��GF��b��׋(� ��leYq���x�|�<&�D68[r�gԠ���/נ��v�E�i�ں�B~�nF
@��#������B�nO���q~��i-K+C^��%
�]��+�u�	nL�gh�AS������d��� ��<i�Äg�X1���󇞲6$ �l&V���������،�V;>��L��Р��I	���5�Q.�	p&������>��2��U�[�q�bk ��E��9���������<�F�{��
n� ��~�G?�������)�9�c�����!��fJ�"b3�>�Q��o�O�"�)p�>y�L���|T��S����f��"�P�r��[g����ƨp9u�+=~�|i����Km�K�z7PF3&yظ�R����kz�J��	�'1EFS�ƚ��t��d��V�������X�n�����S/�����am�a�z�-@��G�<�������@�/�z���u3_�s�N�0����1�"Q�ㄥW̫�G�E��#��se�v�^�8�JIm�x�Pϯ�歾P��q\�e���I�85��3R�Uh-BC���l�+�$̩E�\��q����-4�7��~���L[�"��Po3���1tk^���E��$)c���_*T.����@������ ��w<�Hur�n�/UŹ��/���D��l������±���?6��q�����w��q������t��l%�U�����D'<<���y):2�-W�*e��4�o��η��td�蟴��dH�:|�-����ш������g٩h�E�󥠫�H`9T����������o�%�I!����D���#w���a��|
�e�	{��"�ӆui� ��F�W�"��aѾ= �7����́��d� �QF�D����|*�f?�Z�2�>�-��'������|�t�S�~I0��'�+D�u���lvYD�>��o\���G�I;
"�}�apX���1Q�>� !?�L ���3�9��z��ޏ�x흆Q�=5�=M�e��j8�^����;e�p!��'jζ����f~��Ou�#�q(F���|� �i�Bt�Ng�2F��S1V{D ��'���,9�v�O�%/���1V[�Eؓ-t�[��쮻���m�����*򝬧[�:������7�.��~��NQ��E�n���5��;Н�4���PY�>E����W��0q�I�'��z�~3Z{���(r�l��	_�W-$��J��ē{�yj�,Eh�s��ⷹS�HA@<A��}�D*�vW��-�N��c3��lFOe2F���o�-\;�z{�nI\��vU"�ɧ��^4�.��w;�x3ʌ�(Ҟ��R��ޮg\` v��N98HO @\��Z�p����?ܪ�m�X0N�ϭr�9^�����)��ݒL�/	�k�S�JL���'X�Z`�ZY��h޷d�<�xl3����M�B�(P��u�
6�K���f~���{ؠ]���	��Ԥ*�v�-��z>�R|<�<N��(s�i�1,e���;��Ҁv��rx�7�'�.�n���l�����x�ל���r�O��%R�
s��('�N*��^d?%K��Ls=���\F��O}^��r�;
����2ԭ��V�wX��e��Gƃ�>�(2�#�
�ׇ���H�r�&�ݞQ�@�/M���h�wa=�Ͻ���O	�������_��^���}�<z*�%�Q$?%�n�eP�6�ʶP᥂��ݝ˯��+���"�@1�;Lj�y��0������#e�2-���U�-�||��G�p/b�R^�ON�\���갴�ZĈ���� o^��3�<�+V-G�����(B���+����� �^xg�[�?M�
�[|�����Q�h�n�L"��D�cp���b{ﬢzF(��vΨJ;��!]�y����*�7��4��et�g�T�?E�̓�
lg�\�k2j�Q��)}N�+P
��)4��ڍG7��Z��f� ��C�����~���?�޽F��]`A5�io���&�����e�Fu	����O�p�Eg�������M;��%8(}Z���Q�A�g� �;ls¤񠎜���+��r�^�A��j����-������=�Z�}�ETɔ��������L\�U^��Q����v%���d���i3�NWz�����/@�ړ$C7�k)s>���ӲY�.��(cS�Ȇ�L)i����|!�ǂfT�{D>TN4�֮хl�\�i;    ���6!m5��J�69E�V��[ݱ��t���m���^-��#YO���*$�v�H�*rB'=~t�L���l��^���{bM��k�<_>��3n���QdC>��<�G�\���?���&��(¡J�*���(��}�\�e��L�h��DFT��Z��⸝�=�<Jno�L�o^����Y��zzFmD��w���MF][$B�Z��(�ԍ���H3IxE<Ta��-��yLſ��M�L�kթ�Q�C�@_�#-�׌�f���@�(ҡ�=���ҹ��(���&����"*�L�׾�D>���M��>����H��v�����-3�����&�a�(r���@Ƅˊ�'6�w�q*ʷ��*��T���N�u˩�����W`Dy�W�'Q���W����@��Z //��xo�t��t�(������4��{�'w�y�72�w�N]5-�����2�@&l��|hzǺt�-ș�O�g�%���|�n����b\�O�TG�YZG�+�חz����Շ`�D�-X�����}x�=)�?���Y���(;-�K�r}Yo���Q�D�G���hr��ڡ�����w��r&(�.�=�%���\��<�;�N��ݕ�&�E�L�=��h���<�36��UN]�&b��cE�4-��h��49��WfЦF�]�e���m�V�����r�S�BA���,�v�A�����ǲ_R�S
�V�LlU���<�5�����U�Z
\(����H��^:��K����H���+�Y���ӊ����\_�c|�g�5 Q�C�G�O�qi���å Ǐw��m#p���ӲE��PD����e�L�P���l@��'��ab��l��$��,<��{�@Z%(����6��R줼|9
�(�yI!�ۇ������;[�>n%p� �iM�6W�F��Z��^@D�7�z����5V���d^�'5G��|$7[BV���W��_XP� �\���ǡޗ�ez���+O
;;4�a�|�r�zw
le�ō|���,�߹^�,�4���A��H~*�Jۅ8hP0k�e޼����ޭ��$��
�&�f܏z�Rn�ޡm�R�Op��4&Ow��\6��[��R���$⟊�Ze��O�V�<�v��5��f=ޑ	z����C�_�2�w�A[��DT*�D1�x��h �>u�-�$2�ڙ�O����-��L��}l�}�$��&L�nB:�����\�}��v�.�38���BI+�.{����u�\;�ԧ������M�rk�`�C���W�H�on�K�)�T[�ˊSgH��� ��ʎI ?���A<*���h��3q̎��G��{jّ����\Æ�<$�\ۈ�����ID>�@��H�zX�{:/x9��:c���bb:-ͧ�~q�H��{M�8���9����@��K����Q��,�O"�)M�����k��<?���)MQ�r�RT��4�a�:�O�씕�� w�p�ތD�m����>KR�,'씹��cc�W̘�<I;f�N�/`ْ�����+���(6T�D$���2��H�a����;��1i������$���׋���ؖ?^/,�� +--2����Ю�iNvm.���z�7��~�&(G1"�)F(ƴ���,#�!v����}��έL"�)%ڸ�$���G��,�L�o]r��K��g����jЉ�}�d��դ��9Q��R	8���H!o�S$���O"����e{��;��,��k���J"�	Mn��S��5��kwg�)��2�i�旣h��|��N�L_�#0]y>:�ĦlhZ�ý�/}�_�N읒���$��Tp� t}� =���iCd��m�O�وv��`"�˨�S�@�dhӂN�}���W=q3�S��}.r�W_L�~�'p��ʓx��*A���DRS �#�����������$p�|;4��#��(�i������zc)��P�'���i�O3���\g��/�F�����$�h#G�",�^6�|����3�B'�����{YMg�qU���9�i6�����jO7%�,Sa�9^�b;>��k������^{�)	�&țHI��;J����c��0X���M�|@ٱ�:�������%�ގ�v�]�6��p��'��Y *��ĝN��m���ʞJ�6��c���%�2����ʾ/�����Q��f�%��d\�9�W���e���~��K�l��¢}D�k���?�nԫ�	p��!���^�_�z7�n�k~�	T�)n?�je�ẽ���6%��P��M��;�%D�Y[��Y9/7ܡF;�N-{��Ǒ;"�{O�ק6�!	'<3pF�aG��˙����c���0	�&X���P�Y�κ��/h��?�`���I�5j)��$������=F����� {��1��9�|�����q��$���}�b	���4�_g2�4��ꗍl�P�j?a�x�fs��NYJ!����	Zj�}����~U� �d~��u���k�6��*����^.���vl�G�z��c��"��%�4Eڱ����\7�
ٹUαD\S4��^#د��X>�Q�׆�%�D�؎�^v�ۙ,ć��dD@5�k0��V�c��s�>@�D@Sp�)��F�M���/qs��o�xm�o)M�b9����v�ؾ�?����|P�ۊ���� k2�yrG[��}��W�%�eP\�]B�7}�ƈR��-�,.��U6���[	��C���4�d&�  �!18`�TwW���"�v��g��͊d�0Q�03���d{�6ە�I�cr��KC���6�o�N^ہ�˯"���-v9��w!���ɔ��r�$p��S�ܬ�ea��|��}��C�Rh5��@i��3J`q���	��wZ�=�}���y�8�Z���R�_�W�5�׷��pX�~)u��"��Q���P? ��3��I 4�D	�'I����}H�#a�!%M�M�֖C�T��o�r"@��z�^�ҠdoX烍�ٔ1,��J�45�=�O�G���-.Ӷ^l�b[� ťh��ݕNO�� �$eG%� �tj�0��z}U.m�n揱�v�Cd4A���؇��C�7��>�$`�\�\IϚ=y�k^>��[D�@f���A�C�+�.�l���n�X&GH �nc����جzl��J���}���|�t&HE���K�OE�4xY�TD�3A�����D�w���_��b'��#�X�h�b�b�I��2�3=���v�L`3����B�JG���p�f��"�r|&��Յ����S�T�f�p�O�� fj�;�L����K7�����}fD��y�£4�l�6����6���#u��YO��swd�pG��u����L�ФW��1p_6�Y>&%�
o�D-��������ϵ��C��hId�ˈ�X�W��f�n���]��vI@49@������h���!�te��sM �O_-oB���/3E�����MA�Hhʉ�Å�1ii�{!t�2��+��,��k2�8��A_Ҭf�:0�J����su�B�v�g\�ݷ�C',�m�,B���RV�<8?��F�����W�{g��DM�������<F���?�q @�h�EJ�u�"�n�>?WL� X��ړ_��w�՞��"���������H��q)���ތ�Q}�M��.��#0nW�g`4���9�3�&��>o�s���h�y�fz�e���,Л�S2���"�|���g:A�_U)+��Hq
��!���>��W<F�����=�F��R� 8 �����l�g¬Q�Pe���2|�!���p���f{_�jG�vq���gX���OH�M9�6CN`1�Π�y�g�NAP|_Ej~W6�A��q�<�[ۀ�s;�v^�w4>��3��D�S�EyД������N6��3�$�Mƙ*� �k�)�w��Pi.@i1`�e�A��0�s�� MB�4�8���=��v�\�P���{����Y�:5�=���ԭL�7�
�[xr���3h� d��s�|��*��/����ݒ�D��D�������Jf�U[a2�u�,�    �xoԄ���]Ad�1j�c2��(O2d���|! 16ǡ����\?a��Y�;�p8R=R�U�����G�J�st����i��v\"�R�f�z���z9�O1��ڬ�,`�<�N`��pa�������(eqEP�8(��B^���P�9����3`U\9x�PX�G?
J`w�(ҙ���r�i�Y@CA��du>����v�Q�
�]�~l�(�v�#������r�O��

��Y�|�Ἐ�G��I� �j'�1���h�2����,��.&��G(W�ElTp���]V:�������9s �`����-<��1�u��i��G�j��_�����֯�"�^Y�tT{�Ը^��}=<^����ώ�5.�F�gh�֜?IpSWG�_ *�F"G*�����}�����m��<_�����g|ҭ���r�=�L)T�g����C=5w|�����ذV�e&]GT����(���h���c�Z�s!"�b6b����BLt� �-o���b����	�[V�uaۇ�۩(Gr"K*ꙺ�Ś{����-�E7���kk���Ѥ�i:�~de+�Gʪ2�1�@��(��϶ޛ������T��f�,£�N���%�8��"Y&F19'�ȣ�PF�U��r�U'�f�R@�B��(�`��n�a����^�}��m^
9�u����漬5��c�0���,��ۜ��q�E�ú�J����v�,��X� sq��a�p)q`���@DG���y�۹�)���Z�9�,2����k�k��̨Ĝ�2��9��3�>xU�ݞ9Q9�pA���;�z���y�Ə^U��K^5j��^��R[JN���LI�� �:i����~�e�0=پ��@ F�rV&.�����;&�F= )Q0?��S!!Cs��P�d��IC�.0�<� �-�zp��n�x�T�������q���(��x��g�Z�݂�C}Oϥ �M����f?��©����ݬ��������NCl�| �&;���S��3���) ��G��77e\�xD��|c��V�vCN`Gy�)����%�;C
N��lz��xeۥ�qĞI{�T7��[�<\�tqv��7��x�S��|w�A�c^��k}�z0/���xDLC�w�/�FTb6Í?e�PDHeG�e`ۗ��I:1�1%�R��x9�C��N[x��5��pA��'�����[rm��(Hp��}����UI���Q���~���s2�������"@*eb-8�3%�xo�������YDH�B�'�Z#h0"���}٭ݠ�Q�'j|�=}��K��X���}��(�mh�J^L3��/��o��_���&l��r,�9-����I�/%2�"mU򙶴T@D�Rq>���:�E�K�eC�!y�l���;�7!|[��-|R��}p���#����!*;Y!��zG���+�Em�G�b�]��Q��?9�1POϰ���_�=�v&.��`��b�����.�u|�Ma=���XR�RG� �ݙ�3�Y����!��^,��T�,	�myX�@�����!���r�X$J�Hg?��!1������`�V9�9R�P�Ӆ����� ~��ڹ�ȑ�L!s+��z����S|�(���H�J�Q�g����C(��"��sp�h��"E*��iz��׶��Xm�ȐZJ��1«���0���rl&�Jq���Q���GRr'��{F�ĈrcXO�3XX��P�>��~���D7\-_���7F�#�9Q���YyR|�ؗϼ�4���B�0�����j��?�Q����E.���ɴ1/"L��q}L@|=�>��)�K�"P*1 ?���(�;H��헯`$;k�R�4݈����.���%DA{�}�R��2��/ui���S�=AE�K��G������]n�<eNc��<Wʔ�o��4������/M���"���|�Kg�}[:�B}�LQS�2����e�����1�Q����Ȱ��5"P���=�vR_���rϳ��������_`�j}pr�{�d���)Z��hx&���õ=Fg�0.�=0V�|�wK8ir�r��[y��k�w���`2�Es$��4˝��N�_�\��k_)�����۩�#��ҝ�n2��BxR���
{�q&-_�!f�ǡ$�D��)����7,_�Q����"�
���O��h.<+4{��N̈$M�F�Jeϳ%���������[}��d/q)�;��ȘJ�nJ��?���
@�����׺�[��ND��4��h��7U,�t!�����*)��17څ�"���gly�M�C��8c;�?�a]���mpS��*���'��6�-����Ot��խ4�4��L�-<��2`~�f㴝�Xa�/�S��P�2���<C��6g��+���i4��"��m~���[Z�z�B�N��rO1��o����p�1�f;H�	���j?B�J?�������S9_�S�ځ}��b�[=z+�	�"𦠥ő���~8�@�7�y����(i8���m_��[�G�!x�&��F0�~�@��D��`��)��a�������Z,-x��x����T]�M�c��a˿v����v>�q�ˤ<T�T�1��3g#�)j���s���H�L�!�"!���h�rr���ݨt)��Q�h-VLX#^�����ߎcp�^V)�.�2�fEٲ	�a�����+'�<�%���W����J�"��`s,������&�B'?:a�.� ��>U�-�9��gl���Ì����" ��4�
�o��=��}~���@�����а I�3�ɢ�.�?�����'��G<����D(��얚�F������,��P8Ge�����|�^J0~��6�5g&�<�]�<,�iv�Q��ڒ	Ra�;�%w�����g;��Q�("F*2Fj"�㽞o�U�\�2�#�E`H�v_�\M�����ܭ�j�Gh�sYH������/K)���z��L�Fy�:���	k�����ge��(�ċ@�B�;[����kM<�(�Qf�� � �OV4�	�u���W*"9��fj���:��u��Z��FQ[Dn�c~�q�\�n_`���~���[l�of�΅R���$��.�v�@ F�מ=Os�Us��������)�+l.�2���?��*뭋@��N�#ݚ��N>��}E!kS&�@��>�z������>���t�"P�<���nl��[o1�]�X���S٬=�_�|�Rda����|�������=�TD�NQ�7J��7@��R,z����)j�t���H��'�X@c���Uܕz�II��!\
snG�gb��F� A�I�f��9�?�e#��׻�ق8i�����ͣ��G��N-��lq��N�OD�@A�Iі#ڿ��%]3�wOf2������ad���D�L���/�0SRNIDz�w�|��F0���mw\�L?@��'��!�x��ro&)�R�6��)��Y��p����Yx��ʋ슀����-͵.��CKF�Lߚ��gE��������acn�ӿ5��Gѯz:*���-{����2_�8���U������L�h�>7 P��Ӕ����C=o��}(��f����$��m>�oG�$@�@0dV[�)0�<�a������'���=k�h�#U�
^� 8C�����KaQ8�r3����*+p� �TWދĨ�,��H$F�|�����4y�i�Վu�v��N�����dgyQ93[/��ɲ'�zD��4^�ڍW�U�D�]wԈ��r�gj|F�>]l�q=�)Ҟ���Ò�w�1;�g^9F�Q-%�G�A��S?�I���!��TP{�A�G-�Jŏͯz��Ǆ�;ڻ��@�jx�>	�C�/�rG<)<��
j7�D��+�$��9�u��Z�&�dm�Ȏ�-8�t����c3_`�|�K_�
��8�""��Eoo�������xb��(�j@uBMyR �x(���Ú�w�*    !<��f��)�'��J(�Ƃ�ZJ��v�]�=�LF٣	l)�Ps�$���{����Ep�>� ॠ�F���O�z���n�1&3Te�63���)h����hhoԌ�&��q�v=Y�L5g@�d�m.ͷ�'pՅ�"g��)"�6/v�@/�����)�%�?����_�u����k�>)b����`���G绅�g��<�m|�)m"�������sԼ��̥�����H��]� �rO	��T�v��(�EY��6S�b�WP'�m$ʹ�~����&)S"AQ�N�>?�tOsH�$��vIh���ɑ�W�.�v�a"	�E�g��{
�E�h�n��LD
��t�8�o=�y���.4n߫�[5���.�����8��W����� ����$�Z�Uݷ ��m�\N�:$N�զЬ�2�<�!�X	�v�{�s�8��X�f��1�a�������%�_�	s��B�Ւ����Qi_�[6������Y�=�i&�G���L�_��z��2�w�F���*���S(�-p�.��؟(�w���O3	���&��+���O6���w|��(*#�"�q_|��AL�\7�H�*����O�/�h�[�v�f�h����sǰ�� ��M�RoG~��&��-�2�H�r�j�B�׺��&������YI����g�h��hW-���09�[+/]9^a����[.����_��Q_do&���a�O��3>�i�b�W��t�N����7�ڱ���!��VZ��o��/F�/���p�٧>zO��U����)�n�7�!�t)�ji̧>���k�VP�?�c<k�C��~&�/�������fxP-���w֢�8�\�������h+��$��H����'���ˣ*���]8HP��"�Z������~&��XY/j&��,�m�P�����:r?��N�2��r)2��r�%��#T=�)P�*�n��na��^X��K�Em �e��Yd�	2��:���T�X�j(�$�� ��zC���Q+}0f��&����D�^�u�]D��gKU/?�<P��`��k�fpO����)yʜz����o�]�=%!)�-�{Z`p����-�#2���'��%j��pB��ܔ/�D{��*��`�d+Pgw�a=FaiU���b;J��	�� mp<GO 	F����%1�e'��y!�0y�B��00��6��L"�)X��e�������eG<A��"�G�"��@E�ݑ��|�2|(L��#ho�0���'�L��/�~�V���D�~�)ҟ|�8��;��OP�[^{����}
u�@& �;̭N�=�)LQ���'�h�[)���뿏�jz9�1fV�$\5d��t)������O�n��e��c�E�(�H�2�?�+�_����s�/I	 �fz�� Sw���iy���-�G�A5��<��R���^�Yu�]�]���l�ܭ�˛Gd���孯�~��i����'�`'Tm���������/0,�f�gd�#d�[=�us�R�$�^��1��ϭ�
r�(�10X�l��n>����̤��J��Zڟ:=��éEh���X��Ah/���e���������u�3n�q�?�l&�R�����XI��z �5�j��j}Y���*N�����L����^z�
�/]t��u�
bYZ4��pw�����N��d��Ꙉ:w�s=�Y��}H�7ȘID@=�@��7l}e�{7�����O4�ǽ��z��z"��9��O��i�#��@)���}k�J
��}k䰌"X���]l{�Z���ID@y\/����K��>`���uO����R����L�]�Ky塞w|�~�D]Uo&%[>�X}�ű�����[���g"ӎ���)
��{U���̽R�g����W��x!�*�}����O0:We��D�M�Eќ��u��������,��r�
�PV���/m\_��UJ��k��Gk�~�M$?:X�$fXP��>������K��3@9�b����ѷ*�d3� ��ci*tհbsZZ
��{�.Ԉ (g���ibKO�!�NJ�"/������x����AA�>����rԍ
X_�sN��g��[Χ~fD����z}���.⟈��`�ê�_l��&�#.:�?�Xa��Փl��_&m�a��!�$�u���ŏ7�z[ =AӉPg6�^�~�RXU6������(�X1�wk&�@R�>A;�� OZ*L���
S=�վ+����a�B��߲�8z)uч }��Dq��Q�`9��ݜ��_��ۘ��:Y�j�����;H��	��6^�8"��ƟB�d�uM�=�؎�+�n������"��d��[�~(¡�k�[�I�I>�;���͋h��uA�j)p���m�ߝ<��v�ư���uQ|�~f��X��cy>��qߚ���tʎ~�������܁�Ip�v�&0�`� zڼ�I�+C�\�\�f��j	�	xE����e�w����7{�����ٟN-�z��<軍z�O�;�L�)�n�PO�b�P��Ey���D�Sr�֙:�x�ڲ�$���y�P'K�I*������^U?�x�d��X�;E�ǵ�c�_`*�ӫҋ��>�Fd<%��Qh���~�AY�}�B�������q��y�6]{�݈T'�g�<V#v�}X}�N�0�	v?���3�͎PGh	H���q����U�x"j!h�G�'�l^���qoze�~�_��������B���^yI�1ة��lo����������t�9��Np�O�[(��@�J<�8�W�U�m��t���n\x+a�.Ė��V��F�9��ꂬ��1Bh���p}g�A�ƑAm��Nx޹u����wׅ#�ow�������d����-�_-�A��pSZ����N)Qs"Qk,��+a�\4c���	
�B�e�čN�����"�)[*UB����l�;��M��)#�f���}�b�?�W�I���:T,޴�����v�+���{�+�2
�+�r�Bd�.��aO��;@A���+ь����u32�ɓ�	���z���h���@��iO���&�������q��8���>�.�6=fG%�=�l2�H�ژL+�ܔ����@D�ǫϑ��z��d�F0~��hzo��S0"�)����6��;,)�����מh5�	�{�0�[��윩��G�')�P#����@LE�c�Y�h��D��g�:��w����e�O�'�C#�� )!D��K���q�|_�V�oD�Ӭ��p��s�!6�S ��43"�)s3ّ��mꈶ�'��(��O�׼,���x^�����}ǋ��zP��EM�\?���躛�8��F�>�X�f4��kR2]�����HQ�"�T6�:f�ݸ��6Ct�-9��R�	B�i2�����>Q7]d>�� ��zfnW�"]_�.�)�H{*<o�c�]�v�r(���=�W�֓��=d ��Ph!�C��o�VԪ#ЍH{*O	�����]Q���M�k/F�<�B��"���u|G��ux��{FF�:��C�n�b����HsAѯ�S��J��sJ���~|]��f7���l�҈v�-�r��&�`Ǻuѷ��<�3"�)���iV��~|��u�v)��J�����u@����́�]�hD|S��Ջ�฻�8��B{��ǖ���� ���Kc�v����x7KA;�[�N}vՈ�&�@�I!p��.�q�����tN}�؈�&ï��+ �����ş ma�~	��#��N���^�f���;�qU��<�~*+R����G��v��p�k&��F��	��|��M,{���t�4�C���T4"��1F�^Zw��9���J����ND]�P)a��\��H�c���N-wg,��|����@�#hj"��9V�#�aܯ~�n�)��m��Z-j� ��Z*�Un��G�c#��alDw�L��c�������G���7�'�_g�jP�|���/�#�ڷ�B_F&>�
	��I��q�{���.��O���)�.�p������X��["��'b`g���W�PT��|jj�    �#R�h��x{��&��ܖ���ܢ�z��I�{Dh0u|7�Ǽc����r�|
����������~����ճ�|
���k�K�ھy�m�Ы�ҍHz��^<B5��m�u8�������+F=�Hv�G��c��#/�"/�ڰ�������J�h�s�H}�H��C;�r�C��>]�˳��z�MD@��s-Dk��̢���ݣ>D*7�E��؈1���.gD�S�V�2�+,��hEj&w�wtE�St0�]�z�n���4�����O�0�ӟ��O��pSX ����
��́�O RB>l��l��4��i�k��Ywpmj�T2��}^2�y�"w}C��	�O9`uħ��������2w͸���`D"T.\f�8��^o̿��7h/^U�(�6���_`��zۑK�B#�
����$o�q�Qo%wW��2�aP�R�������hsj;Z�L#��F�qE�_a��vPsj�Ӡ��#p�`P�G-b�=��o[�Ψ��"��`�Y�)���a��Ƣr������P�S/d� �ᰦ��e�5(m�D
�׏�_ӝM_���w,5�(��fgmַ�|�M���`��8�~�Av�s����x9\	B�����Y���S��
�G��ݽQ���6(������Wu<-B[���S��~������%��~}|�qO��6(�'/jk�x��Rϊ�%Whr�<���ǾE���-����B�U����<�f���?j�\_^��g���w�g�s!�t�3��w�=�غ4-�qJp;�Ɨ�j{�F� �D(Ǵ<���ó����?�Ī����}�_�s(�"�F��~���fIUJj�Ψ�*2�;$���5_��]���
5�Ŧ-W���׏�v��S��a��DF�O�E�~�0$K�����6�C��H��q3�����ş��N�ۆ"��H��B=��0�rJ�u)5�E�q&�S�/.��\���:��Tx+��\�:��d�x��Y�IC�������$r������P�J�u�A�QQ�FErV�%����FE�j֋z��(�@ct�ǚo����ZO�u�N�"0*8����9�����'VdG����%�� ۯ�o>��!<V�C������ �o� �AA�Y��t�A��eE��?S"/�w�J�V�K-�x0��y�Z�Vӹ��ᡖ�� �lvf�l��b�a���i�v���"]�o�
>��O�c��,���vRlE̔t�bF����g�ǿ����[�0��gϚ���W�s=�5���}<�6Ê����p6f|ۿ�qo��u�E�l	��{��������L3y=�}k�t���K�TP��}jz�a�U��+r�<�.V�͊k~����j��j+X0<EA1b# �n����O���Ej֋3U����J/
�P���}�]²"a����(D�2���O��})B��kE�TN,E��{��g��2NU�A���p����C$R���aӇ�E���l��T�!E�����eV96C��rV�KyC�p��K6�O��K5M_D?k�e"`*��Զ{ǽV�����U=e����-m�T:|/o0�S��;����}�"h��t�X��r'��L_1��[�1���Q���������+>g�/c&6"��<���p�h��Z1=m�EJF ��_g��?�>7ke�T �y����q��%{�	g�%D�����,R 0�����pV��/p�`��N{�q�^j4���=Y����hAVdLE�]9�7��-Ip�"3�Ӯ��t)�"������#�هt���[�ތ� ��!L=��$�kxei�:�Ʊ[�-�a�c1_[�qGqdñ۵��H��w"�Ze�X�ƻ�gݮ����0S��1��-�V�S�cfӻuuu+r������a(p���OX������3=p%Z֋��X���Bx(��;��wK�g+����|� :"0�0K����7�]�QS18<79��f}����O�8�R�L��:��q� ��y���w^J?��R�1/�����_t���I���"M*{*��&m�s��?������U��[)E��A��e��i�j��;V�Je�*��Z:?�G><mm�m ��R�k�>v �5b?��<e�w��G��~��u����s��)�>���Y�^;�f�m�tdQ�#��2���"R*�:p8���L�}�����o5n�7=J�x�Q`��x�p��,>���خ.�mE�Tt�ɖ��r���a�"6�B��+����[��"74;�%k_�L
�v�ق�%3�{�[l���Y�(O}��@;��i0��?Pb�E}RS`J�@m\-�w͸���_r'?��z�D�J��UK��?S� ��R��f��O�X16����fw����Fi��Z)-�}�3t����w�.��Y����^?���-#�� _�.��<U0}��z��7$�� _��oe�T$�xOS,���a��}m�%���v�d~��;nsY-��z�D$JѲ�rW���X�ƋN�S��%\e�!��y��MǠ�F[C؊D���7ﱠY/��L�7C�c{�9�)���AҍB!�E˳Zߢ]��.��R1R���gk�5G9���i#F����(���R�&!�n?��^M�ܔ~�^$I�hT5=�Y��V�l|�R��l�,)Jem�,�=Y�9'��n����3ĭf�ȵ�M��҆vX$UoO��'5O/���[��Q�"F*��r�R���L'>��iP�[t��z���i�&���&"H*���5��k+ӿ��# � ���l�c�߷y�%��I�aE������ó�]&�o���2	\�k��[�M<��&�2H����<�2�(�}:�{"�流0��xʯ����y��f���'�i64�HA|�i��Ɨ|��E���+1Sʍ�{V�@_)j������?�t��i$�{���k�qb�.5D�;��D5��W/$�a=K�5Lg3tK��Lg+2�2�ַJ�<�+�I��Ag澡��"L*B� �ۄ���Mݩ�Hvm.�YR!0�=S�f���$~(�'^�Hd`9Z���7��q7�a�Q��dT�,����K&��	.zV�2���!��hV�F�KK�ẁ���{{u�PQ8��|(ސ�$��؁Z��3%S�x���U|Ndx;嚆A�J	Q���ݲ���_��CO"#jS�/=��z�_vl�P���H����yH�)0�'�l�;}�$b�Tڊ�;��#ݴ�VD��ϕWJ\����ַw�SW7q"�x꣱�-YO�cL[5p�q�Q�fɬ5M����;l���B�x�w�*��6�1�Eo�UĜ��*D+�V����{{�;�X��,�L XXl�=����|�z�؉짹Ɣ�� �Q	~�_J�Q��O����h�m���x�^��UdԱ<N�<1�#�,���'ƶ&��yr"�i�k*oǗ��}���W�k�睈r"�B�ّ����u���7��T��'r�B�SV��. �T�6vM��Ӯ�9�TsU2�p�q�<��e��{��q;��l�����[޶���V��N�,���-�am����g�wNN�|q���@J����8����?�'�s#��Vps�xݍ�'�Ld~����eڜt
��%`y�4�N� @�_�'=C�k8�-�5���x��=%Q�w�����HTo֫����>�J���s2���#M��S)۸����W��t�~��Q� j��\w�m��N���v&IK����eϟl�Ii���v*��d����8ݦ�z8ȸ�ki���v
�@m�/TB�q����D=��؉�e�c]�ufj�g&E;>��N���X��nOC6��^J{-����Dc*%��m�+�вꛍN:����˯�4�����e�^}wի#7� uB���Ҫ�r�������X)N�Y��x�9>��Z��N!�*(���ٕ�"�J�Sסu"�)d�,�&g$�4*���D}։|'��#����\��c� �	��R��e��ϲ�e��`B7=C粌6��	�'�sH    $��-�_#@j�)}á�2��#���'��x��>���:��棄C�����	�%z�.�2ګ�N@@�3�Y��8݇�0L�u������~���@>�b%i,�3���rտ���r=�yPsS�w�\��-���`e�#���V�Ջu�z���{�D�S���B�2�7�K��&�55mQ��K�@�6�šZ���U颯�%�U;M,6��}��'8��Փ��N�>�S�G�D��D�bH)��گ���U�^��OѲ�5x�W;n`_�l��zm���O�g0pח��)���2ߖ���Q�!q2��pnHЛC�l�|��i�L��K��Cx\f�8�:�Љ����2@�~��	��]Ř��{���1�.����/b��A�H�r�a�ᕌ����"�)��x�����N	��tD�<��_��m!�������������YO�>�����P�p��~�nj���r��OJǤIJ��	�g�I�n����/OD�p�r�u�y�+��"��V	�'8�4��O�9s)<�rYN}��ɼ'?��u)a�ܪ�-f.k�����	i��X���AB��NPר@��|hJ=8�&���Y�ϰԇ�D��<�,�W�ym��>�����{(��\�� ��:|g�����ħ9ҁJ�RQ8�ol|�m��e��{��J��B�OAr�[o���a=QY��J���m��SP;ƔIO���ش������?<�)C�U$=%G~��`?� iM�	2�eL��d�''���f��@ �TOdE�SH�0��5#�K�xNF<St����;��vk��h��u��q'О�Oû���t�ws\�H�]�S+О�,@���R�>M/��-L��/ƈ���h9�R��^K?	�~W
�'������ ��x��}�Nem�*��J&Q�֭�L�� �;z��¬/C�D�Sδ'���ʆ�~T�qw"��1�8�7�AF;�ꃌ2�i&����k<Ol���R�\e7K��)s	������wԇ�Dؓg=�h�^�����y#"Ҭ���"���|:�NS5w�5�z����#� ��'W3�,�'Ϝ�Ӹ����o�'П��Oa���Gx����>�`����O�k3� X�,����A���MR�u����D.����D�r%�����oWr��P_9�5r��z	m������ ���)�Zg�%3�~���:^�L�e���#h��"��&�qN�<��^C��m_׾�vm�P'������
�?����+'/|�5�JN�?9杅�Ё�H`k[�k�7� |���԰^v��m���v�U$?yǧő��2�h��q�j�N ?U_5�]
���[f�����:BR�+: U`C6�r�j�pnnZ۲A >�'C�"��~�L��ջ��6���jgS2*�ӇY�r�-�:7ߠ��D �hz{���W͜�$��8�<0g��k�j�$"�ɳ�r08	�|�7*v[�]��%^�?%�1��
��-Ժ9�r�BCPZ�?ĕ����Ȼ��u��1����O0�@މ�M���a�|�Ca��W�H}B�j�by���1��rx�[#^�>�7u�"~�^�3����pR�ۮ^�>��vJ��{��Y��#�$�"�)9*�Ā'���z�\��wن��'y�T��47��)n�{��-i�e�S!����c#aC�$�mo�����"@�o>��x:�M��j����P1Z$���6��^?�f��kުϢ{�q]*[�ߗʴ��́2|�/�N0����;�w2uq//��`.����d��r��f�����<D�}� ���p��ԟ5���i��_{,0^���eT!��<�8=�l��Lڭ)/���M�G�,V��zM�]�q�I�pq����X7y����4i��H~���a'����M�sm���a�K�|���3��w����F۬$���j7Ҽ�}�L$�t�p!�Է�N2�v�,J ?���o�ρ��Ve��]�V���E��V�������!��\djV��q7ڻ^^&>�xV}�tj.0"TowR3���$�U/���wc�j0��4�x}��f�7�p������yT
�?(�|g�ס<����}���N�m�!�?h��������d^$?�H,��qh��.s�&��W��"��c�r	�q����߼�]�E��^��o�<PD>9�����8�z��Sj�o��E)����G}�)� .��x�׻ljo��.A�Eޓ�tMZ��9Cd@S�6���'��9m-$��s�,�xh�j��"��G�gRiG�Y��>�����@v��=�<_!����tA2�|G6�-�|��/r�|�@xpX�$	;�;�7��6qd�����~M�x�lh'ӹ���v���rZn��u�	9/�b�`I��I��%���d@��ۗ�l���8���e�w/fЦ;y�x��)��^����:�WGSy�*��c���nA�a�6�������}�F���iO�Ӕ�Sq�����@�|�v ��z���-�����u���j;"��[F/����~�X�k92����޻�"��7Ư���c��w�;�3hx����u��З�N��҂ZX���{��8�ܻ��V��Ǉ�dA^��9N\Jp�@%���V��'�3+ �7�o8ᚙJ�]�䈃N��y��P�? Nq�Q\3QI�5e�E̓sp�y��?M���x>����F]�݋�'�i����<�a܎����o���^�=���rI	�J��>�ROE���T�]����������z?_@==VG@~��5��d��8Y���Hyr\I����W�a	͚�Vx�I�v�l��Y
�xٹ�EYc�oq�xn�<;��n��t��:0ܨ�-"��S5��M�2���KV}#ǋ�j66���]5������z�C�8y�L���o��ݻe0ѹ>�P�D���4d3Ѝxo��D��NH��E�Q�Ϛ�m���S^�Ο�zsߺ�	N5�ûͪCB���:f��zQo�� '�����.���߷�mo������K�d�E ��q"���T�20��(�jV��A�.��n$��H��e2�PL:��^�^�5�A��a� ۿ����׆|x�-Y>At��������V��&b���R�r��q���/}-Ɋg^�[�`'H��+���\�wot����j���s����N��ǎ�e�oL�ϙ	@'x����2(���>y��wɓQ���@'=�����<��η�S�)�}��s*���PV�G86�,�� �ԇ����S��
M�����@Bo��iZ���S5~�|p��C�Ĥ�cd7��ʚ���;���K�����|�#��ʈ�e��*;�^�����a��i<ӳ�K��,�	���`����X���@���xz����|��@{�?�%q�@���t������kߠ����NDR1��_W���âм|����@�)�L�V_pc�&��'X��;�m"�0|6<�`>����o���}	����=) E����ʹ���p��w̅���lH���P��]WB�޴N ��0���\��i`rQ�8����W߉O������ �L�˿��@��Yq��F]��|'+%���9L�h��������w��:����j��z0C�9����mr�W�D�S�)RK=-'������k�7��w�?-�Ej�n	q^y[�s��@w�V,�&�`��ƣ7��:��kHݩ�B �j9���]Mo{��p��0�c9�����{&���v�;kH\��Z�~��9��0�N�MB-Ӵ�۴��͞��mS����ɽV���nXć!���~�G��Gh��z�Կ )�-��*ߌq��	Y��qp�߀�4I#̃ $�^���=��Z�O� oET�'|�5@�?~���	��D0��z��0�ȇǭ��re�����!����cq�����զj�T#KKn+���߁4�O�	]�k��X�g�V���u�Ǧ�Ciwn�@�U��xK�Z_?�r����W~��[�AFA�`3Iq�k�wuM�X�a��f�Pqq� n/�if    �к$��+�F��eq��<ގ��;W_�	��_}�$X(���͟�mo�'��}�H�z�[����;?N��i��l%���U_8G;�`�8�o���~����}{��;V��@(O�ר����}|E�Mp�~��(e�A-A�B��+z �`��t�p=�N����k<�� ��<~�9�o�W���-a�ZY� mn}�P)�Ӓ��'�������4��Xh�P�k�7��|����y�ԷhVW@%
�l�Zu^e졋�M֎H��$0T<b����2iɇ��T�v����>�a�������/�+��k��UO���
`��������EJ�4�3�r�_m���E�F��Jt Hߗ����(����V< 
z?�X(Y�O�`�`�P��~S����#?K[���W?@b�=�:�O�g�x�΄oZ�U���T%��,��$�~���w�J5j���\,V:�c���.:Z�����]�lx�h�A�  ���Ei������2����w�����%6����Tvaѷ��&�=	�1� 𢠪	[c��X�� �Ҝ���a%�{F FU�ŕ),5��_��_��(I�f%G�мQ����}�]� `�<]/X��@�X��l��w*0��K3��x:q��9�L��N��DpTI|th�!8΍����7T2EbTIt�ImV��!-_�|��^@Ey�F�+'D�R@���q����D��#�om�*����su����^U��"���<�����u#��J��MQ��[��ٯ��n�7f�oL����������&2���#�� wA�:j)����DM!����{j2ų���
�~�~�n�u�` �L[�T������j}HX�I�2&�X������v�#¢����t�aHq=˺�ܺ��>�b�$4�E��7���z?eՏ��duaÿ�-�6���"A`D�MI��@Ӎ'.a��p^��/�J�}7�<Y\����SQ�)U���i����<L~sx��|�v�GD�`U}�G��לN�ֵFu,T�P5���o REzʾ�>5A;��P��d\�KX��l}�S���b�P0#dpM���k2�wM�k2A�C��J�eQw��� �і����"&�8�h�r�^P�m��K���\E`T�<k~ Vl=��<�^�[_{�6�̨b��	k c���/���v*uH��wOxx��j�"";*;*�2TD��8������">j�/�/�����t�k"�:6 ����~����,�mF���"J*{ʨ�G���@'O���ȑ*oo$�2�����'l��>����� �é{�/���[�翏��t�J�N�G�@���p�m�£�ߙ���i�����p�������҆��U
���rg�^��������o�13<�7�?�,�L٫���Bӡ�]��ӆ�����e�>�'"�J�Q�hqi�^��̃���9J� �yխ�V3 ��o��}��m�"A$H�L�Ut�wh�{�W�w��:�)�B���vl�SM���Q� �V���z�ü������Ui�~��3�.͆�x��]�6�{�A�GY���a��xǍ'6��kcm��*}�e0�{TXɤr&�v?���X`F�ۧ��+5����'�%��Z�#J �uy�.;�n;_?����wC�V�Q% ���&gɄɤ��m�8��O.K���Qe��k&��>�b����톺����o�Q2t�5E�����[��1m�E�k��2�!��������ȍ�������E�ҍ�M�`����EhT�-����-�{��m�ӛk�Տ�(��H�P���@\�Ķۋ/�0� ��{O�\�p�&����z��&#��z,����aO֯���b�̌
$g5-������p���J[�!��(H��z��j-�������wE~�AIovzG^� ��[%�<� ң�'�!�q5�ㇲD��a�<u�;�u�~��h~��=�oU^��	.�=��y����=�C "�����H�֑��|���~�A}:G�Fe�o"z�_���73�4t��F��J F��7����<6�B����XQ�P
��Q͙N�����	�u�(¢h��2���yu9�+��$�(R�l��o�����m�@��	U�	���Ed-��8��ݸA�9����Q.�DE�'T�����{�VT��7��v6e\�w�R#�A���S�
`8QP�����X|�P�^����f��R�n�1�C�Z.�M���7��JK�P��>'膮���m|��� =1���"��e��A���[����V�v��Q���.�D�E�eȪ�a��x�]���g����݃�"+�:�$���w� -m����-&��'�e�칠@�]C"�n��~�J;��24���ԋڌ�g���	��R��HHf��g ����j�~�J{V$��(�}R�[��?��xX`:ho�D��c�0��Ђ�g��`�.�EZ��Br�e�}7_W΋�A��ጬ�@���m9����4�i�7=G��Qwc"4��]�92����&�H��귧�i�z�!D/���dH=SA{%��(h������c�G�����ڏ��_�F�RX�����۳��G�56C󙐺 �|�����V�QC!V�m
��6\o��O�Y;z�Q�	�*y�t3��iA�X�F1r�W�M�����=�EhԀ��y8�M�t��n�6��FEUs_�Z���~����4s�~h��dn@۰J�^�F����6p����2>��^K�������V~��i�!eM¸m�k��Q�C�z[�N��7)�C[�A�'���ӌ��n1���~j
��Q��F�� }��'M�eܢ��e�Ԁ��7)� ��=bn	����©\���ݕ��b�9�cb$�i�IAu��Z��˲��t�A��I6��獾(i�Q�K����
���7`����J�MO(^tD~?����0�P�1;m��(C��RGL���ut�Зֲ�U/��
����ί��KS��u<s����l�a� ��1k��(�Rjr�7�k�p�|V��oz�g�+UQ
�O���<��{H���O���E���*8�̀�g��ҥ�I�q�Ӭ���R88��%f�ط�#��P���\���� ��7ַA�wX/,'�vqW�s�8`��>h�G2�̝�(�!)�/��o{���2�t��c#�^(HS�C�/�L	��W�Hv�ʰ��s�qS����%�h�!��E�T��)�D�~~>7�ܗ��2kя}��G\fŘ�x��~��A��f�F�158�H,��]������^��ډ�#�ݙhS�<�z]]���|o>Q���8U�\'�1v�� ���7�)�>1��z�@{��+tD�J�W�����8��<���S��<�Vn�oz�'R����Sh >�V,<v��b�QdNele��׏���Bw�}�V��Uoi*�Ƶ��v�l��x�XGNq�n�wMw�d߯�s�ӭ�uJ\��Sy�0U�K�<]�ͥ{��AQ�%���G�rS;����x����y�6#��Th(��!�b��M�9�-g��O_�����J��������[��n�=o������F[�4�0�m:�ĵ8`�/br����7�F�D�I�25۝q���7�;V���J����ÑrJ3%|/$�;Qe��Y��
�>�z�Ӝ�;�r��GS�eD,��=h.��t�T)��s��r�'�vh�ａz,-"��K;��M��.bi;�Q�"�[��w`��~���/�ک���J�O��,�}�y�v���>@}vF$V-1�I=.7����b]P?;bx�����ʯ�v���\�/ɀ*�\�G��|��;�5�>p��Y%����AMimWd����k�����Q�-�9U���a�t����W������6�8
�*O;�X�M�-����{wG�y����r�
�mi�'��m���I�^��y�L��B�˿~�7��C��<-\hG?��    > k���$��'*�����b�>�"0�j6��(Eڱ�@q�� (u�Ƞ?�&0�@�p[ٓ����Y@�:^�f�䲊�@��7�C� �����3�`�?^q�7O:1~�f]��53�~&`���3ܵ0�	'�@x%�۫���
�*��w�0j,�.O��Ė�~Я�
�*������%w�&�Q�b��H8���͠?2)ª���d�5=w��6+�m�܁��}�EnU�tUR�=��*T{h�h��M�-��S�� 2��UC�a��Յ���\q�l"�*�HwbA����ݩ���/�?E{�D�XѨ(��i[v������l�~#p��X��������M�l+Ҭb!Rgz(����2� zaPG;�X�I�''<����|~���A} =�D��&%{v���i��DuڵP����ʖ���H:�5���~������F�e�����o�v0F;�I"�*�A���˰��y�!WW���MPV��|�GL��*G2}����?�S�}m��!O�� g�;
�)҂���I@Y��.S�z�؝k,_���c�����h\ ^�j��A� Y�0m�K��D�P��B�	����']90N"�*%�+I����S,�+R֓DVU�ŭ��X��g��rѡ�_�9A����m���J������S�T��H�Å]����PꈐI��D.U�4C�hט%g\?8�4�xu�Y;$�T)�v4�K�w?�۽|��f�5�R
)�M���p&���P�����R���IDVի��\��|��Q	:P񩬦\PH"�*Y�)�;ڠۍ$<�:�b��kTr�!��[��=s�Sl+
I]q(��*oɭ��P��zy���D�v�����K��*�$@X?L�Ŷ*��œ�H��S1���>�`��w���N[5��}��]Utz\�U��EtQS��t�@g�iͩC.z���$� ]���1f�s��/��Vꘋ^���Dh�q4Nb(m�q��pXy�k���H�򙕓L��(�?C�k�N�ԏ���M$. �)����<�~����sme�$������䦇^O��x<�S����R�U��hr�����$�z'ҫ����  +�,�&�"=C=�m�$B�\�8.{̋��j�l.�+�q���i{�a�$�� 9`�0�FS���j�:J� k���#��j}��}���Åm_��TӀ���K��
�fش�)����&�����Mڡ� �����8�Q���5�X"8k������$ �<��|���V�Y��H�������>I�Z�!�oL+oz3��%�L� }�h�1p��n5|�O�X�j7�p��?_�[aD5��\9����7�l�i��7=ź��AA$	x+��jV�9���{3�cM���A�G�h.A�	�@+-0�9�83����0/S��@���E�	,��,
��8K��X;ax m.N�V�@�>a93< @ZHT�Z�v�h��hV\� �����T�;O���D+�b��-��� DZ�Љ�׹`�����
�PG�F��ǿ��f���?i�5v�N]�m��f�>P�04�t���u矡���t�$����O � �]>6���X}TF [�Ah�2�����D}�?B��a!I�\�f2h0�#�_�38шj����*B��_q�#ݢ%=��r����דȹ��Z��;�8�Ǖ�y�A=!W����ѯ�i;.n���I�[�@o4h:�eV���L������V@v�9�jz��u7Z���G��_fZـ�g���"��tN˨@� �`�<��u��O�i�ǧsYV{8�P�(�r�Ѿ\/�	=��z�k��y�S�uD�U���#��A�8��R߿L�A[Q��{_S^�?�Ib��9,����d�D_�F�����z�0h�%�f�9Q���{ �L���N�+uI�$��2���H���S��.��l����rmfs	VTBEJ�ަ�u������sܟ~�ʱB�z�~�+��P2�C���P���6�/ #�f�*��vi���s�k�>�@�OdWeõB(�՘�!�RMo�-ȑi�.�(8jYg�;h�q{�.w�q���&�HM�V�B�A[P� ��	�'/P��7�N7�������j�n��6�Cl��;�u�A��.�2�Q�������\�	���o^��@�U�z.�������$��������;���a��_#G��=.K������J�OH5\�;��.=T_]O2��q~����a��Ǻ(����d������P_�E,��#=տf��حOT?<���TF��e�%�}5;4fu	�$0� WK�Ƒ�n�e�,�ߵ+Q"�*{��c������-�T�gt��O\w��ݞ�S����v<�b�=t�`t�����mOm���n��ȆDi�!(���י�[ݨ3l������b�x)�m~!���R=����ٓ��I����X~@5����Q��zn'��H2꩸ ���y&���w��<d+����g��� ��Q��ܳ]�©MB�	�'G\H�ik���lh��ݬ�$�Q��0*dHGI���k>���$̏�_��Yܥ��a�0_�
�]o�J;���i�I�Q�" HR���:����*a���,e"���MW�;aq�}<��j�0�W�:�T�iX�Z<;���g�]r��3�E���>�m;���0[�ac X�u����n����n�B�����}����^�S�;K"p*f|u{�P�{%��=g�߹�[3_�56�z=�J��:��v���b����ܬ��N��W��C}�C�K��j7��Փ"ͽ�F���#�Y�K`�b@戮V#���l�ŉ@	�(&l��ӱ�%LO�+�j0y�~���F�޺��_�Rn Q8�+uϬYs���+�9���
?)a�6S�f|y��q��Ԁ��5gG����ԓR�V��#��N#��O<��� �����,��:���7����wɇ��if�2U�7T �

9��g���m�B8S��%���*{���$`;�3�S7Y�LEKU�$���
3��޻�C��E�THt�;kV�;��ܼ�gvW�O@����Ȝ
����)Wؑ����~ۧ#ڋ>Y�O���h,>�k�>��]Rb�G���"�5�=�9�Tm�����M)T�I�|G�٭��LX�������v33����(F��nՑ�ȶSu���Y�NL�N��ӯqV�����V�E�T���)G�:�G�:Qg��˗<.�2��lx�^Pn
��)zϫ�銸)CUC���2��{`���jM����������Щ`8����oH�N����W�LKQ���̂�T�n�������"g*���~���1����w���V|�"j*$��\i-������f��/�~�0�	���z��wsDɪ_>h��,(_�����ŝB�U��0hG^�a�<#��-̃����i����"X
4���dq1	n�DT2�>@�E��Z�R0�ԠZ��O~�m������۱>.�2����p:�ꁿ��m����rFxR0eL}����/�'W��Є7��g&"Ij`]�ѷ֘�m��D�3�5���Y$H����=^/���N�i�j��2���_��@�������,���C4�^w�ޏ�yD�]��+[�~�Q0�\�w˟�ᓫٽ`�6�;��(�y�1��v�.�3��6��F4�ELԀL1Xu�V�?�����Ss��]�8�O�e5t�s�� i��2�|IvʷF���EBT��p��������#�vf���(�Gg���	kQ�B���"� ��z�/@dD�B�k@���[QʷCƪ�;e��2���jr��[���x�ϱx�E�y�nÝ�ޟ�M��O�?_�U�V̾a���6 ����[}�����M�S�BҕEJT䞹��&��K��>�!ԇDXTȰ��E���i���ס��U_�"-*���7k-D�T�	m�gH�~���# �9oa�w;�����o�j�zID�Dϣ�g/��6�p�沯?=C���݁XE�&�Ut�ޯ��G�>�f�g�    ���#�# �~,�Sg�g��Ep~� O���� �/���sdF[P6��(?�A&��Y|1�P�$�ލ��DX a@x����7�.��^F{�*��(��'ץn����	�Ѕ2���#��齗zNF�B9L$��z�/��>^�nL羌zE�EyC��E�	7�5� �&�]&��]�8�>F�� �p\>�%�?���_�T^�F=A�o�t�p	x��n���ҨS	 �GC=$�� ��O�w���,�S�C�i��R�'��Y�IA: e������p�h�!�ޓe�0T�H��4�iO����c���>���@S�P��NӪ�l��ԗo��
�Fz�h�m�fz�}
�>�!"����U��
:y�[�`�*�6'<�(��б��"��3��޾&�'�;��=&p�`;�� a���$~4��l|﷜����º��\H�|�T��x=�v�t�Wb��+�Y�H��Q<��0W7l{{��}Y�HE������g܋TZJJ}�,F@I��d���ǈ�:κk65���b�Y@J������7h���HU��Z��w<�4ZU/P�G���Ǚ�-��x�RtZL}ZmW&Х� 9����!V�Bnz���L�L�p��<^���ſ�A}�V�L�B�MƭK)���yh�/�h[/�Y��w�2_���)X���K�����!]�/����C�̛�ѽ�"k�s%bZ�k.:]Hᅦ7ֆ���;��XZ�:�#�#$[���cG���g������)Xs�|��M����z�DQDL�=[ږ~�^�{J��i��v|/ӥd)���//t�M�M��V��\ʁ�)A����jqM���֥�A}AB�I��5��[��K��t;��9��F!����~0��]�I��.�[2� ���ÒW5-Q��O
^�G�����ȷ}�'7�=d%0��O[H Ȓ|��G����`��	T�q+(�D0��n�+�����x��Z+Ug�˂�@�j3'���A�	$���0̀���:#�ymA�BM�rɿ�{��F�
ƾ�h2�����V?�f��K����C�����ʯ���ᑼ�?z������&g)�]/���;�դ2:U�B S�4����_�ھ��Dx�k���1���DrB�mO}֦�=%Z�I�~��b�u�Z3���2�>����S'M&*_�E`P�|1�=���3��g�g�ǎ�z1txo[��ן��V��bOA��-K�A׊F�x�k}٨|����Q.��Խ�j[6�W#�V�,q

g�����dw��V�T�7�.pG�����r7l``j�u�xkju浩�E`NA*�]���-� wҤ�܂"��@���B�6�Q}&((ty.�w�s�R��vݾ�)p�4oW�&5oL�Nz�4J���n�S`>�q���?\o2t
�I�Q�[0�0�[������N�`�^�-v���E�s)n�#�[na���P��ɓ�(A��9��N�Ш:/����,��_<�`�G�g���Xp��S)M&� Qp�Ý[��zn.�d�ǧu�I�K[D�� ��eP�X��y��$�[��I�E�Mi�Tٴ�D�޹���V��V�^=8b�I���C!�͝�!x�hu��lhS�~C�>pl@x��e�L����`2�WdM%O��L����8�	mi0m�QQS)ܱ)tp��{ΰ�>�7+�����Zs���_��T���D��k�x��Ν�gR�|�=� G�+y.$��@��"ঀ�œ\~��\��4_5m�*��EdN�LPĒHeY�������:�4h+;U����P㜹�ҩ�&�e�"��@�`�sp��Aڎ����&M&hW��T}�)R�������z����s�T��@嘎q����x�F�j��}!S[Ѡ *衰��c&��6��'L���L�=�"@�~g{�m��v1�m�K����,���>i~t�Wu��$��L P��-�m"^��1'�H~WU��	�)����o�XC��!e/�R
�)��d)S��u��{�X�=�U�(�r��,a�$�<��zϪ��
ܩj}�ᜰr��3ݏ�4��fsXS0]Oq&�����n)��°��(�4Lh���!�O�w���6�\)�C"Ώ:Q?������~;��.e��Z�l�Q��x�1=y'��.����"����m��pz*Pj�I
_:���J(�KG[���/]�$�4������
��'i�C
_^?�X����+ͭ?
� �"�d�[����9�`y���[.��`K%Ӵ��Hi �:���殬j�W}����D���,��i)rl�Y��VT�`��_�	�6�p���%ɽcUO@Z�xԌ�!��Xn��7�PT�<q�gp�'[�B0��/�jo�<���"�ɒK�M��kd!����1	��%|/O�U{�C�B��t�����ۖ��K>��1�֊�������g}�W�s�i�1�O��Mwdy�rQ��^��΅~��
j`��FX�zA���,m%5�/����}�5 8\o F����gV����?N�.��B�U]��U{��(8���IX�_#�sy*�jwk�hx�P��pѡz�m�����<z^:�JP����+y��;���a��8J�8%�$�@L�=e�;R�0�*^���~����t1e͸�4�q�������׾��G���(���9�-C��E�~-
P'����!���l���t"wZ��çh$��f*�_M��;�g>��(�Q���N(g�#�`4l�����B����ǿ�i�K?Z��2��T�����5X5@����⽍�s�������I����32�M����^O�c(��	��s���L�V�)"��0��Fl�,s�g�@4{���RF�Yؓ�r>��n�#j����.Q�M&WP���}�Կ���Xm��"@��ۡ�"j��	�f�(�0�Deоyꓧ��p�__�ֻ��:���'���M}���8S�F`A�q�y����pQ�z�E�=Uӹ���qQ.rV��Du9�"��������.�������&���Da���ՠ���k:��kפ�x��R\ŶYVu��G_�>���ι~��sMo�~�a��\��.HC\Mؔ�Y�6"�D�	w�R�,�^l�N�:;@ =yd�B���a'�GNkm?��ϋ�W�@�����X�AXƯ�����m�YqO9��ڸ�ﳇu���ڢaE$=�@G�g,\���r`�oq�w�F\�𔹗�祖������J���V}	Y�<�X���\"�S��zת�Xa��T��o��	&Vv�n�I����r`�"8��Kʑ��i����t]�j�n	m%+����c��>u���V��O<�TQ����h���(q!���y�}������+�v�O��Ѱ��(�.�q���{��ꏃ�:�\? E���C0�����,֧��������x�^�M�2�(��V2�������>晖��c�6'�/r�}��� ���he����(W����v���\��
}�3��t���f�����\c���r�;*��ܮD��K��kH,�f�ǅ�Z|놽��d�C�T��,��;]�*X����}��漌���_�ؗ��#��rq��R�g eH82O�t���]����|
�����,�^f�:��g����{2�QsB����C��M嶛��I���vB��nنv,-���t�����0o����<!U:�7V;J`O��\!_� y\Fb�QW.��A�<9j��ݸWx������Fy m�y���*����ᤜ��E���0k'T�	T�9�|zZ4�A��Ϲ(s����5r<?�>��z���z"�4�Wa���yb��w
���+�}�*��r��<@
��	��.��t�W� �~�V�-$�08���'�b�ox����+�|�����=X�	D��?x�~5��@\��OЭ'}��S���	�-�ox���Bm闕��	���<�7���"��<A�X��'�?����>Y�A��'�    v�,S���"�$%�RC�ֻ���'qԎ�E4T����	��_���ٟ^���f{���M��>�q�"��hc�>H�.����*���)2'�1�j��qK����w��U��~�%�H��HErB���;Z�_�t�(O��A$E��^~�!�������6���-~��%gE`�D�J��'��Ҵ��5�[����A";*�W��_�n�ܮ����#�v�NH�H̆��쎭H�"3v�Q��Vߺ�F�f�Nt�c{^��9$£Hh��p��kq��u���~K]I�2A�����@�f��x���#I���i{��_�W� ����B�!b�0��:|���u�@^�֗AR{�;��49�zq�u9�i�����X)�_o�����7>=�z�_K���J�0p`9:�Mz��hÇ��e��̖
�|4�����5�'���߫�/C��xR<������x���m��e�LO�#�/����+���w���ʨ{;�`�TH�3/��������/}e��j����"�+�%}�Y���ĸ!��&I_H�"U*����,o=�7X.B���ؑh�m�q�i	w8[��eN�M��f]��b�y��fi�4�w/�����z��!S���Ɵk%���V�dΔ��7� 0i%�j�s�lk	��*�]�n�eO� ����1�lS��E�o�F�h���-@e�Jb'<W?����w�eU梃�����	��P�~�"��#&o㉪����͐Տ�ȟB�s=.�b����۞���F�F[fC���X,�y���Ӹy��Tݶ����^zqTf�ߠ�U�p��Zg���^vxT��|�J��Tj}�5r@q5����~�J�RU�}��?X܀�w�X.l{;�A�cO5��:��	Q' #��m���NL={�����\<fh��o�xD2���~W2(��q���}�L���ȣ��qwD����m|��� ���ޗT$8�{�,��>������цOU�E4F�ښ-땹z�PxcLV�Ȟ��$�v���_\g�ڪ:��=!§�f�m�lN�n��x��M;��'�;�Z��L�����oڻޙ�}�E�7����W��V�������> !,:�챥���t�;߶���o���)�ĖH�~��f����,1�95�EX�Fy���a�6�Bl����A�!�<�Q_��T���-+��.Ù�/j�""���0�m���V�4*t��]�����vM�x���a����^�F��&@��A���K�p���]9Ĩ�H�*ci)`�1��o�9�bd��^Q��12tC�W����c�G}E�x�ۺsB���^��m<���]����V�]r��Cj�sw��}q��p&�!�z�# ���c���%8��}�������.�@*7�=D�EF��qk�Y�F	��������D�<��.Cwz���j�4W�z�z���ux��9��ڟ���f�*��1�,Q�oP?B��Y�W���t6��~��w;d*ĝ� �Y����P�d��F�SU�=�����~���.�ϯ��1"�*�h�$m7Ӊ/�~fJ�cD<��AnnC�玖�]gM[Nsڪ�8�j}���R��}��q�֚���ԉfFfPE�X#��9�gԂsֶ�*�󳌀����R��5�D|͍�Ak{O�c4#0�<�0�a�Fo��d���{�#/B1"�:#�q7r�c�ҷ����<��iδ��;~��X�vxiD�!��j��<���sh쏃vbbD�T1<�HN����eu�E��8/��j����O���gG�N���ډ��[%S߉3"q�>ס�2�J��r����S��(ړ�F�O�H�	1�TN�Pfi��ʌ�Z�+�r 54���ea�|���׮�C�~���[*T�����}�����6\ͭ���r\x������*IT��G? �O;V�Si�SXwmy��c)Xv�H�6�ǈ,�<W��zE��C��(�ޭ�T�q^N�p\-����>~��M�g��\����>޶{�����M�R���ú��k���;(I�aD&UIa�����`�v]���/ʭ[#©R�l���n�:�G�Y�݋>�<`L�0�~�΄�z���_�fDFUqtvRY�d~V�)bp�)�׮JɄ*O���q�y���S���hO6O���d*E��~��0����ouh�\NE����g��n6���מT3"�*��K�kp�j��GK�v�+0�P%�1������k��_�@��Ad��r�a�}�����jjF��*h��:��Ҧ��|�c���F�O�f�5&���7U�{ֈ��J5\���j+��O�D��n���=R3]�"
,m���<U��C�d��u#��J���y�
��K}�V��&B���k&�������x�(S@�6���P���5!��gb�O��(kY#�`����1����8mH̉��tSOf�ܗ����oWn2���}�׬����5��.%�M4��B��/,��_�>�`�x���f�g�}R���cD�T�8��/�R�����n���%G=�E"-^����w��d���b��P��T8���\Eh�@:�꟬�i�#J�M��"g:/N��N_5[ԙ��A�f7�nY��q������ɓ�Be;x�l}?�����
�6'��_㍗�]����`D~T68��<����"���:�ˬ"4j�5�us�>KnW��<ڃ����g��ܧ��4��۱���//�k�0��Kp�K0"&���	_y��Ѥ�r^�!�IQ$4ύu_&�8�]��-F�E�LWMĎΌ�p�_���T�Ux�0�n��g�OhR��^&%S
2�#)�:'���#�O����Va����k�÷���(X�q0��ڡ��;�-'hv���(��9-a�;~տNlyO�T1eF��`�{��֌dڱ�}CV��&p� ʡ��pd��e90�r��Ǌ`�<Ё�ňs8���5�9�2ˡ���x�q���6��� #��2�@�E��{����J��k��A}U@�D�GKE�j��O'�n���BЯ�|(B��+��ɇ�H�t�Vxx,�Y�Of�kνD;���R����B��UU-���Pn���!*�j Q�8y�59V�'�!�P�⻶x��6����U�z��c�d�!�]c<��[�����j{6X�?#���v�qN�wT'
�ZLrS�u�j:����mC���j�����,�:��;��̰k�{u�Q�r��2a�g2+�V�{G�-�lH$�l��)�!Po͇�E�W @<��J.�ꘪ���+ｓ���@��(� Ϲ~�PE������1%��^ C�;��r����;�f�d"�|��������t9N0�K���)��y�x<`�$��z���2�j���<4k���F�BɋF @AE;��dj3)��~W�h{S �W����U5�����Z;>��_֎����l���3u�����)->3�CՆn���X7��j�h7o���;U�Z���j_J����:$.�u�0?��D�Ӏ�N<�h�Vv^��֣zu��	P�U�|�}�ϱ������S�O�O�rG��`3v.1��g�O���#���j>x���4���Ҕ��y����&�8�8�t������O;
�O�ճ�_!r`�1��w�9���"��@m��k�܆��n��hs^?�iO��6l�8A�>��G�;:�'�^�"�i`�h5���dt;fc�e���xu?J��%u����>��֧Z�֙	O�[9�sE`��ە�̓U���"�i(���S=2�}�O\
K�?�^=��	�f|�g,ll��k���֡Z����	O��@��2������^lU��i�"����[���:�s�2��Ԫ
Y�Dxehv��vOA��֥����G�ٟ)~?�/����ƞ}n�U/�Y��Tm�N<�ri��wlV]u�
x'�צm�L����}����&�V]��
\' V�/C;�u������U=��N0�E���S����¹vn�BV��a��$:E4U�W?�}��gգ_+@�<0�    
X���׏y��wk
V}}�
D'`n�pݏ���=�S�|���d���m���������e�x�-�#���p�m:�tݗ~W�k�\���F����\	(�=����:�V�9���#�k5D��LݮHVWo��ɣ��e,ȟ��&����e���:�ʬ��iE���㤉�e"�r���B�[�ȃ�>H"���x ^�������M�t+����N�����%�븷P�B��(�yg��-bd����Npw�	ݙ_��C'X��Q�V@;�G�oy��K�~z�e�߾ԋ�,R�-NeU�� �|P�����/�����-�o�8F�;��qu�e�����Q7_ܑ���OSj�[��d�̖����Yxa��|���"��^�+�崀3����u�]��N���)�a���8��0��H�O ��%�L�����cC�
+�v�����v�A��~�U?.��$a�K���bK���q��7��}������L���WD��ñy*h������y����N�����,ඡ�[4}ID��,ҝl������A.�sM_Q�v�t'cx%������Yfr����N �K�m+��u��h�v�k9"ߩ$.��O���Ȗ��r��+��ݮ��ә\�;R�\i����.����k|�)��;����[X�]	��SO�
�˗�?�de��"1����A_w褢�cym�� O����Du<��:��N�ԳV�8j N3��5P�м_�Os�љ@z��7��$O؆�D��h{�޳	O&s� ����:o~G����vL�	�ܬ�����8m���^��ާj+�Z�g�?OGe�I���G�cU��V <� %!|~�YԾb'p��Q�_�(�������s��x�o������"�)�Vyb�����cˇ���R��V�;�c?O\B2/`��/��/9i�٬xJ�WJ#]�����/ިW*�l�c�#R�M���+�����}tD�� 
�X����<��@�����	̦�n�����x ӻ�Lc�'���i�۬rؖ���ح7z��t����<:�����:��t ������zY��b���V3\�։���}�z�k|p����X�w�Ԛ��S��+�\7�O��v�'��J��l�B�c�+��OU��'�
SX�~��ct���}@�>T/#�xu�����!���$�9�߰� �?a\��DB���[r�����^��"�)"�<��J�v�nNTmJ���������	l$=,��`�_����_�C܄2LTY�?�ǿ���{2��F���N�E~S�d�'��n�M�=Nd�����A�"��f�Ɵ�K_������n����?_�'�̦f�
�N�
��YZш ��ԱF	ǅӝ�m�����+�[�i7t	��9̝�V� fE��+��4�G�W�ۈ��(A*���t�Nn���y���Hy�,�C5��M�_o��^�����1���M1x���B�O�r{��,	h9���"�M�"��g������z��b��n��=Ctg�n��Ġ3������D�Ǜ)�����9�����E�l����'����"�X�n?���o���O���z�q{C;'h~y�b�g^�5�wZ���0P���� �)V�ͥ�P�y����Y�i�	����#��� ��O��/9��OK�$W��a�G~�#��'	��fQ���Y޻-��L�j9�"9�~�������y��{Q���b���	������8����oz�u�3~QD�D�����E/��kfX��+���!�������^3���*�� 4i�-
��`�P��?ڋex��ʡ��QM��P �o�W��fų2�^Y�5���?|��+�?���>a^D�hc�����B�N�I�'u�� .�>��{�����%��8a�#Lơ����2�nX{��	$)��F��[���9s!��.������:fhڔ���2t֤i,S���� ���.��Dc����j㽝��Z�N¿P;������ޤ�8#UM�<�9ǝ�-�Ȓ�����s��Q[�k�c��	h)�3��n*�z.���>����_�t��;�eAe�EA��U������6x�b�SP��"tݭGd6��^��R-`����z�HSP��Q�.'_�n�ڨ-�95�����67��Y�w�l�BR�b�9�F� �>hA7T�Q�t��U'���|��A�����k=��v��Hj�JQ�����!|&��z\�tU���Q��F����W��R-������p�+>Du���S�
��T��4�KW��ק�'�S��x|�׏/�&�ޡ���z��	��j���q��\���+�{,mn��R09 �j��疰�8�������J`K9�:���DWc�
6�wV�b-N�KU��<8C��uc�]u!��:�-Q����y���H3~zo��E����pwX��a�dz���{Y킿�R�eRǎj�ͳ����p)��E�L���J\~h�Z���'v"V�aiH�8ܳowz��.����yR6��@Ʀ����o/x�ш()[(!1t���G�ݧ������#���O"�T�>�1�����뇐EJ��}��nѥ��J�#��j��q��P�G�!��1��N�IAl�3`�<�����?�5�%e�������QW)p"_�&��y�t>aOtYT�ַ�1��N�L9���T�A��>���8��'%"c�E���O�zo�{N��w#��5ł�����?|&�K3��/q��8���^�[��֨��%��(��Q�ɔ/�j��?�D�
��n��]9�c3f0����_����3�8B�c/n�>�h��u�S��C��7�vGu���.<��k
2�:���m�t�L4\�"H%�G����*��̩b���������k8�C�j�C!�T�N�b{\�ſ�l����=����^��M��H����p<��K�t:���ț2���1?\���i��>n����Q��6�	���g��3�E��yP�D�^��_���"�!��P�T��XrO�����i�/;BX�BoZ3*���v�>��:U�=�a3m�O�rl���T^�g���N�S�3M�Z������6p�9��5j�$D\�K���#̵������5��6�^�s��D,�:�1��i8��y)h�!��U,���]�!�׹�x��s�d,���6��	�*���G�I�����sU��Jգ��K
�=�d��7�_W��:�Љ����;�e�b���]���m�>#R���B�(�9�@<x�7����0"�*��/��C[�`�M_/���Y�Ԉ�*��� K�{���e6���$�����	;�Մb�ix#�Do��w�i�W�@�r/�Ƿ � o��u�����ε��
!ɵ{��`w�����`�zaD S��5Ґ%@��i�'�7�'�?R��A��n�_6%(ۗ�nㅿX۵_�z7W�T�D�nΦ��&�B��n�Q��	�*Gh-�%Ϳ ,���q6� z'2���yA�����Clz�9�ywR.�t�=�ʁ�|�5u8溨��ED՜�v��u�\��J��ԭQ�?�&b��#� ����>��N�5�؅ӟ/U���H�Z��cS7w�~3]�Su����_������ï�.�9�.�����/Z�top~��ͭ�����O`�T��PA$UY�)�"c��f^,���x��=DTe8,��,J�sb��)�_��T���\�(_.h4y.uFJ�D,���!��{������y�n�_�v���G�񋜪.�r4w,�g��Gz�z��鯙�`���e��:\�_o%H��I�ˮN��,��:�Hy8
5�M���L9�u	��G�"��t�!�˱��[>?uΩ8I�q�ۥ��
�\X��	�r.\�6�؉t��|İ���a�n��^��g,���C�;�< "���� �:ω��?Y�^���K�pd@�鰲�x��X��\�d}׫��B��@-��    �Up60]�k(R�"+�u���J�ׁ��u]�wI�)D\"uf Pꍋ��-S�'�"�*r��P�B%��3����`�V�݋����S�d.�pc�����@"�*2��'``O��Z\���"������1��SJ�0wSl�V��v#��(R�3=�yx.	���!ӷ^S�b%¨�<L�^$?��w?��=DJ5�j���p㖐�Q�W��)T�g�`$<P�v8��}$�'��x�?3M� m��1��,�3���N�Ȟ�s�I���}r7δ�]���E�T����Y]v�Qyܙ�[:e?�E��|�mo�+J�yg��8k+�z=5{J��Vx��+#�ۨR[���)�i�� �D�i��������s0ۮ��%m!g;�0Ȉ�T%��2�����rY��YN9�Dr�X��}��z��d�7)�%���* ��b���;u�8�85�/޿W?��Hn��So�m~s9�U�7�	��^ N�dhF�P�{(�^���]�lY�7�^`N=΍ǖ?���ep�k�y�3�K@���)p�;��-/V�����P��!//`��C،S��ٔ���n���6�݋���S���8��Wm��&�v���x�`q�B����g.ÕW_���c�Lu\5<>�p��j��Nz��2;y��%�)���(���u���z��q�k|��b��n�x��eD#�&b�G���-W[��!b��6x���]ؒv��?��+K28���,U
j���.��]Dd��Ȉ�,�0���pyϳ����� {�x� >_b��:�����E&k�X{ "MaZ���A�S;m�-/�ExT�|� ���^�Ɛ�uU2�{L�p����8f����pp�R]�ԗQ�"D�X���v������ا����s(��ȓ
�m�$�:�9އ}���C��'/B��!%�v��ǹؑ�� ���R=k���X~%��	����!�W�ȓ�� ����;��l�~P^�Bʿ�|9�����?�u��Ԙ'�.�R����>Vd��7����7���3y)��?���\�޷���X��{�����L�Oo���[h�>�ƽȍJ�\&�]y�7Қic���"*��7��w���H�k�]���"%�d�¢�T�9���xFڧre�qo^�D��A�1���%rۃ�g�:�ы��d)3��O�l����7�C%䞻d�g��@���֠���̞��{�����*mt/R���_���>�**��^�"G_�����9Q%o"~?���Vl �����n/B����l.�]1-���q7n,=B�#���y	ż�}.� ��Ռ��6MTo�<�Ⱥ���ӃlF��ѷ����H���*��Z*�۬P�r1���Vm����ʇ���zV�@	�7i��Q����3ۯ���z���w���u�o�8:h�,�4�ų'6�>�N}[ċԧhΕe����}1�\b\nx�'��{���ܣ����_%/7z�>6�ES~ �"�h@˔g����:p��u/3�������������'�d�o����|;*x츸c���w�@~���e,��݄��v~��N�{��/&��j�.{�? ��cB���y�:?�q�t� ���߂tݦ����rCXB�fG\`�FlW���&�՛"��b�t��ێ��v��7�
�f�^�����q�5ښQ^E�4�����q��_Hs%돌���̫���SҶ@� �S%��:�Ji�.�����H�&a	����'�
�7ѧ~�_$E%C�q�I����7���s�EXT98��2�N`�������"(*uJ�sC��"!5����"'*:�\걚PI.�&e1&k�DJ��ޞ³�۽ȇ�~�Ѩex:�%�m}U4/b���K_)Ѝ���b.ٿ<��	J	^$C��:�vӎ�^ަ�W��E=O�)�|A��4��Gf	�D�r�W.��j�G�� U~�Wz�i���N�vUD$A9ߣN�=��qO�/������H��J:��)	
yƴ�p��WD?O�r���P2(�KU�v�_�"�)8F��%�����9��H/"���XP�Nߎ>.ˋ_�v%AD<�А�K�:l�����w�����w�����n�1��nJ��d�+�"�)��Dj���o��X�偧TM��'�y��4dP������j�u��lO2�dj��f⥼\�@ӵ+"�)"kEb����e����E�S4�)�W{�Y��w�5�v�%�S�b��'n2�w�Ѻ�iYJ����	��qr�a��������n��e�C���m�F����f�������d	�C�(E��caW?ðaf�U����R_D�N��9���
V��y��679T�^�N�&n�A�2�����?ι�JH��X��H2u�'���Ч��i��oo�`�á���m����6�2�)I��N��:o��7Kiw�$S�U� B� �
q�RxGan짾M�ze�D��<�~�gj���J�	�W�`�Lvb !�q��v���h�d�/� ����;��O���
��\���k_�A�;�w�CU)�`�ر�t��+�+g(A`==�|03�a���X�뼰�u���v���5w�LSZGSѰ���s�^��A~�Nw�@ ��-�(�N�'ڃɦ]S�N� x*A��m��M�t���.�ܿ{�<ȍ��ΐ@{*�o�3�2>
�]�Օ��k�x$�h��
�&*=��5����E�T^��{�34K_���ּ�>�Dړ�T����0�R�q���VgK��D擳|v��w�	�}y�o~�z�ӫ����=?E�,o�N%�lkѧ���/�f�-�[��޿�voY{j(�(�h�O8� 2g��C�Zv�ӏꐹ  ʩ��x92���Mu�rqP8Q�W/�ר2;]'��v�Q��"��e��t��O�*t�P�\��:�GdB�4k۠::ͭO��7�߯o�ό�T��&{����<��m��7T<��h�Z� ��0�[���^g�[^�̡B��h��U��c�'S߹#�~67n%�j��C[�#ȸ�L��@S�AއJ-�_�i���iA Ey�SaՄ4'J̶O0�s�o�j?h��$��Rި[T������:�/_�r�j"q�K���6����5�X���(��:�+�d.��Io�ܝȱfb��W��؉�	��J
�� d{�u�����8f
�@����?���M���BA�����'���h(����j�~�j~��\�Q��HC���� o�|���7�"�>�X�{r����k9���/_�E�RU6c,A����� �3^��`T������z����#l�m �=Ad��~�w������\����W/t
�(�F9l��q�)ʹ��L�}|hTy��X��E�k;!p�nR<ι	��˞B��>��^I#��K���\K�S-�������[j��pVi�:��P�Q?F������R��i���$tBOR��^}�9`)_.`���rZ�6\���:���k�*��ac¬#�}�}�fuq� Ф��O,�Gq�m܍T��e��u�H��\gg=�)��a�m_;�l���A`I���K�M�O��������jϓM�=E�To{��%�"���v_u�KƢ߰R ��V�����c����`�z�"��c�1�Gu��p�=�����r�Ӕ�}<�z���ʟ�p�_9N�ڷ�]�>�"D*[
�MJ��^��*u��<�:\���n6�A9��.�o�3�4�ㄾ�/��ch��"_*'ϳ	�����qFl��=*Q]�}W_^����|�eg �Sϔ��FV̪�"s�n=L7�7x���|���z�ס~��vo?�,�>�?@�8�ӛ����`�A�_L��f�%�`�0�������_8T�PG8VZ��an�����.i	"��w�s��^�+�'��K�.�EuhQT=�1ƗF�A�9��������A��7 ��D3O��?O[��K �t"����Q0���_�ؗ(�k��F��`B����D�� 12    z����Q��m�B�:�?�詾'X�x4��־��GJ��d�x,`]a\�̯�H˹Ѿ��T97���
y���T�� �x��������"j�w��|oz���?X���z[W���~�S�gp�<�I�l�? �(���Rod�䩞E K��ԧ��}L�s(j�*������*?@��Q|I�ȨlH2k��='�h�.��-�6cI��?7�Pq�|:�-�ڑ��@�I�g7Kxd��`�pް� �Ů�W�2<����E����Wz��R�V�c��HXޢ����y�[j���}�x*���6����=�sM�h��� �� ��hhǽ|����o��-DD��M@TA�H�t4z>m��6�w��/A���(U��	�i_�����}��-�#���KqG�5c��/.��{_���}��*�`H/�qrx_yd��V�bWJ��Ȫ����%�d�a��Ʒ�z�V�U�8�%LY����ۏV�� ��<�M 2�%hxZ� 4a[Vz�U���fG	�
�%L#g�J�U��	�bz�)X����
(Ӈ&F���U=V@�����7f?��P�ճ^�^�*f�~wS���8h�]�<��-�~Ӕ�axg�f�Q�V��;RR6��f�O��D�lh�3��X���J}x���q�t���+R?7by�h���ρ�^����J�yB�.Ы���4b��2~1g9�Y�0��	[%�Y�������ʮ2��-����G����'��~�b[�&�]U���K"�s��+�����̆�`�@�*gߐ�fL]����0�CQ��?F])���O ��\�G��nX~�퀞vo4��g3l�(��gg�Ȳ�;^���cĂ�f� H�o�/o08p!6H_��6�͍�
J�lѥ�;Q���ٕ���`�kTQ�Z�����9�/1������+��2׮�D�j�$J�;lUl�로��2iI�j��+�@Q�ZA�ɲ�	�L�$\9R=���Q�Y��c�����K��x���<�e�9	hVՒ��N�V�s<q0�ڿ�^�74</=,i�K�@��0mKuB��pI�M��:��Uޯ��ʗK��=`	?l�#�'�.��`7��m�e�g�vSGכj��k�*� ��^��[V}�#GpͦZP��EkU���(�(�]�q{���\N���J�V���;^t�b����x�XFq�	T�(���*���ru�#tݟ��j�<gR.�D�k^,�gd�����$`���O޹�BgPV��g.sf̾nЄ.��:��fQ_m����Q Zy`�`O�z$��P\ƳG��4�E��.�Gh�_O��p+�w�Cg~�hGl�
��斵�U�["����`󅦫@֬y�~E�V�x #���w�L�L���J�Q \�#-� �"�lsy�����ݨ�F�p��;���4�7?A�4,�v� `�`��ʟ.1z},��r�O�2��7��@�*,o0�*�p�Ndya��S�\$8g�oY
���-�,M��kcPW&�"͊�}b�x#��ps@���\d��n�0_�X�ր���&��_���;CW͐�|�v�#�"�-m�z_� ��c�p�o�?0�h��	�;�br���v�	�D�����<�H�H=��>E��>�)�L7!���e��[j�����/q��vaV��)f��>,!��.�^�����(ҫ(4��0�5��Fo_��q�����E}V�%E�U�oY1r���:����z�+����@�	"|�݃�_罴�/6|�y�fH>$�a��\��h����I��
du{,&")Q����͉й:l(�Q6!�Z�dЕy�l.>��W�]�(�B�=�- L��Z7ݩ�6D_U������2�G��rr�m���v�/p�����2m�} �k��.Xfw���_K~����6η�eYҝo�DeZ��i �y�g�\�����UO� �*�
1��z+F�/�f$w����aB�rQuס]����q����ޒ66��LE�T���f��C�s<�W���LW�mRUy�w�z,�m���C{ax@�må�5��*��V��<�M���W��"b�J�N�2�������0pj^�E��~����Ɓ�C1��_���s�\�!�M�]��U�]��e9���}6��}D��`j�����:�
�Fh�:g�_�؍f󛲎Q��
�*�͢����¿�(a���u8̯���P/)�*x��;����0���=D#U�f
�*_���^���o%�B8L0��=���́�)�O�Bߛ��rDi�`�Y��,SU���>.�G���OC���ኔ������Yd��!��&�mc�[W;��T�r�Ϩ�|7$Cz70=�OPi�d��h����=�ƌ�r�n�����
f �[�K$R�1[~)�3�8s��:��w��͊P�h��l蛽�p`�|f�ۺ.i�("��D�X���	}s����&�����(�hi�:���sZ��Z�\�L˵f�F�@v��|,>�!�;::i�X	���@D������<2��[�j�����R���m����E�/�2��u]��;K R�ǧg�-.˽���tN�WZX�T?���-�t�4R�<�[w����s1��簆Bƻc���"a* WX��:R����?7�Ζ"��\��g�oX-�5��@*/{V%13W�c�[4?JM��z������.��sޏ�\mҴv��||�����C����5�b�і̊"������v|��?`���YDC���
>,�^������y�
Lw�J���Ė�hC�XP C��
`���M��z�Ql�|��U,�b|��f�FU�7����m�����m#d��qE�丛��;�&��;O�3��O`��p�n0i�6�wڮ'wO8�������z�\,/H�W��Ny{yU�e��C�Q]T �Ý�f���������˿�𥷎����(ZG;q
s��j �|F��ww<�͖�G�zt$)p�و�=��eZ� �l��4~s�j�� ���+��ɿ�@�1�����,��[/k���D�S�D�	�?��ty#��|�iZrF[(;
,' �ب�K��������j�����iN|��%F���ȥ��h�M�"�����߮�D���F���'�"�)$�fC��gvJy����m��Uo�	'��-2��q�6���F�X]W=��&(�c�6�n��p:�a�r�> ���N*���.g�~�8�T�ſ��=�����_��MauH��������z!����&	�&�D�V�x����Gv�m$��ٔI�5��2��xXź�B��T���ݔD>S��L�$~���i��?�����Dur�LF�2�/h�u4���]�_�h���e�&kO'��T�I�|!�稷᛬w˺�D���$Ø:��w*�a ��(D��(W/� _��
9"��8��etg��b�Z����D���5�D^a#�aO������{���ԁ�W�&��yO�$����I�~�u�Ρ� {�L�[�y�<`XO��:���<�$\O,�q?�8]v0)RQ��C�!�W�p'���pZ!b|3~�L)*�����~S�G�J��x��hz�L�$Y��M�Gs	Ҭ	A#|��ַj��@h*Oah#([��`Ua��ӄ��@I���y�nV=�M @�f�$�|7ü�y �)=@�5ڸ�$���������vs��k�j��n�tK;�<�=�~&�\���<zj����NI"���4��Y�9l���U�F#��G��m��h��/�%g��	�˙+W�@F6���$P�<0�,-D[jy�zN��gCK��.�'�Tl�v��_��6�
Y�u�e�$��F��n��t���zG_k��.�>��J����B	ܑ�Mh��:�q�$0�|�N��6� �%_Z?�M�M�Ƀs$&��l��p$:p��Cm�%��;I�4��	�ˊ�͡8��#o'4����u��ԳO���9s��<jcm���#�d/{6�����g0�4CT�I�	�����2N��いǋ�u��w���k�ړp}�    �<wÎMo��ԯX����]���5VF\X�݁N����L��hpja_|�M~�FG"��i�!MP�]�.����J;З���f��"�AC��䑏K{EE�oT$E�H�O���M�ު�x}�$Й���3L��������ET��&��2�T�ϤW�4���Wlj叴g� f����4�¦׷k6ڐ�$p�`ҜUw2�^������4<�Wk6꟭Hf�I������5g�u>�_��c���j2q����dĦF��^K���R���6�)�zKַ'mq�$��Ls]]@j���&\����b�z\19�o�� �n�I�ZU��㝓�a�#�h��ͳ�MH�w��@��T �|�maxL�8lqi��o��:�&	@&����m(��`ˡ<�~�ZaS�S 4A�I�b�Qɛ�z'�}r�;��	D�0ezl�����g�� _=�	M���oC
�$-��qn��ڐ�$��:?����p���������K��	n,º\6����0e��:��V�MP��c�p����:�ﻬ]5�LPx��@��Ŀ��{ӿo�L���ɽ�(�.,�X��?���ȴ����e�z���ni�Ƨf?�,����I@1��<���bT�n�����տ���KaI�2e��v!�{���17��f��	�����O�}����׉L+������7�:��6ݏ�R�ýN^Z�xt�V���cZ'.I��[�~����yK��������ID,	��h����	K�ƣ�j���$���S޾'z���3}@ZG)�ɾ��;Ӷ��w��:9	���1I�f/�_:�W~��hF}ԧ�e���P���3,K~�\��Fr��&o���}K���10�ǘ�uS㶂��'L2W���k�3޻xs�@;�[��I\3�S�bQU��VF�,T�\5�U��[B1?Ҁ/I$?��0��a9�t�CȖW��&�a3|�Q��r�=XWEB����'tT!-�ޠ^���o���T�[g/=>P�6���yW.,�)%UףJ��%�{(-ޡ��ܮ/j��u�����4�׻fz�����qK����/m��cN]l���S�2m�������R�z�%���A��������5���C(���0n�D7�u�t�nZ�-!�y���m�_�ZYg-� �;[���}���L�F�u�w� p�\���yy��]�@۵�֡KpP��y�����1���� c���ȹ��o�qt��/ؘ�jG�뼥Ep:�4O욁-�.b���J�[��wj�~_(1h���~�x��Ā����R�ة�Adx�]��zd��-�������Z߁?ƙԣ#��dXw��������c�.�Ӟ��U�dX��RZ�p��� o03LO�}Z���:.i���9�kF�����F"��Qtn��􍕦|��ε�~w��eS7�>�j۟E(Rh"8��l�>����Us;˯=��2�{�~޷��u��C��5�>Q�y���Hp�o�_��.���ox �Q����)o_�|���H�Y>�[���˗g3�S���C�2��ч��k�c���ak^G#�X����p�<L	����y��N���ȯ�Ǡ���xP(��&�u{���j���׮�d~۞�qt��X���<B��]:{�&m7�r1*�#x��0	�?�0,{w��5T���Za�~}J8N!��~����x^G�?)W����m<���Mӫ��glrDJfvc#6�v�Q[l"�ӌ����_��`��WpQy�md����W\����|�W���=�7.�%{Do���4&�Q�|n�����9�~�C�2�E�W8��2�s���M��� ��� .r/�E�gJxU���t�賭��`��^^'͉(@���ڥ�L���n�X�Ŋиq��2y�jR	��v5�T"���sq!2_M(%�Ҏ��+<,p��}(Y��������	3���:����x�M}_MP���^\�M##���Hf�{���R�EE��h1#nG���/��vl����e�c���ކ�o�d�]�:V}	5"1��aF�܋�)V���e�,Ї�p�yP�{��cq���X<�,��m��W�~xDП�H���
��k����{,Ẻ�bE7��q��v������� ��!��di��0��C�?���*���bd�F#)�{[;Hg���� ����ᴟ�I����:t���ta��Nsj���	�E�=T2":3=�	�{������a�3�ݸ,Ї
���ϸq�@��s��]�;����E���T��$F^�V�4��U���!�IN�[�0����3W߭��ډ��������4﷓k�U��RDA�?��
Ý�]z�w�ޯ�:&Y \}Fk�
��△��շ�S�fF��'�%���c�|���g'�;�;�x)�=���aD��q��Ù�+��eF�׿�D������J!��4�יC؎����r����Ru7Z�󋥢bI���q������ۧ�e���:_��t%JB��1]'���e? _���G��Jt{���
�o8��+��p��{�n4.^_�`/=4�gm�v^gQ1�pɍ7V_��!�|����F�/�>/������!u�e|���P������@�I�'w�?�@��`\�P����_�׉@\��/&n|��7�v� �� Rb�@�a�m�������?����O AU%�P\tD���׉������=��EP�l@�3Z�O#f>��,O1�@�������:���4��<M�z�FO����b���d^��`����As�\I���D�^ح�+��@H^�H��4�Q��h�Vn`���7�8 ��8�s�Y�/'|�ŸD��������E�)�/'�{ ���������qA#���ԏ.��޵�}lp&�[.�E��)G��A���]�
"��	�B��5+�& ��w`�'*�I(IX��.&����lh��+R�uX�N��=��=P�>ձn4���,C����0}n �;͟`���� C�eZPO��!d8�8�z����V��K�^��� �}�\\�ǲ�q��n^f���:� qvw��qv{��⫕8�\;J\���eO�ZG��ᱴ���w��Ŏ�:jI��{�-��v��۾�9w�v�B��L�r<�ܫ�[�ق'�.0G��͜EZP4�I��$����*�6�r��� ��Xp�䊇����*I���h8A�����3%>w���t���P�g���k�u� ��Xҍ���0���IuI7>�͋xO/?�պqϟluK���t��4z�����ڧ7��}k���Y�	��дE�H���E`^��~�Mg:��t^�
�~1������do0<vM�Ө�v��pc�$GS����7���QF�?��!CT����e�H�cӗx��ŬƯ���z%yLQ4����onXuDD�C�� M�La�p:L��`��h�X겺����%2@W>�<�i��6����<���c�u���~ޠ���l��q���G"Q��Z��ү�x�IItKO��8���P����:�Bӣ�7�Y���m�\^G���]R�Jy�+K���F�V�Q�N-�p��m�:HH��{9N�����l��'���*M��(Q�c�P�+⸗���ϰ�͆���z谎7�rM�����y�/���_���$\�����V>���:M�F}�M$��]��>�l߀�>�P����
h�¥��u<��¼�<���B��-�,��6��M  �Fp3�xն����� ?��d���͘�ަ]S��nA]E��G�����&�z�m�<Ä��0�8�M�ݟ�&���#,!���0X�kC��k��#4{bI�����#D6&�:L��:/���#YT����1Hp����Rn�W�nܬO�~x�{�C���L�AױL��}�zg����:	�f��>����;@��-z���ߋ���vZ�e�Y5��U���ԋ�$�SA�&������u_ө��z��7��^�}�|�N/�מd��    �Hh=D`����p>�z�l�/K�/���ѱ����a���l9D�����r4@���M�q�F[y�_G&�3d����t�w�#�!�y���I�i���{��z���8��1\�{a�f��4�㸽�R���P M�r���:PjZ?�=�Ӌ���|l;�����,f�Sh=�z�/��"ϹDZ?<�H�:��/����:ը�m����CH�v��n�%~�~Y O���Jw�¤"�\��K?n���ݯC��3���g����Fqz��<�v���K%a�)�����q�ކ�@��)�I�6�_G.��7���Qh��s�c#�j��"��hz��\����g�<�g�� ���PE/P��7�$)6�h�u�^�1�'H�N��P�������|�_H^����n(���tc�Ώ���N/�:�1�I.�|��p�(�i��b���ы &`^����>r��(&uU�^@0�o)�IT�a�8�d{�|;�}b�)L�nH/���c	����ؽS� ���ec´��TÆ��2ӂ���5%@��{$xG�9ƙ�L���k�z��TL��7����o�ݟa���֨z����4[<;0_z7���Оh�(�{ܰ4��{�NKa����k�C�Ȅ����Tl#2ͥDǷ��>�_T�	�4����F����l������'�@R�|�S��o:�\J�1AMjF��ǰ��>�/dԇ�]��+S�3����ㅟ�]	��c�@�}%��-�&��05�)��(��E�� ��kG7�&�R�k�)�;(�aJ)���7F[�_�1�5�'�	I9�[�3����r�q�~�΄P�x՞A��\>�y�*�u	�<�����q@��|P&h�O#?@]�F=�\g7�PW+�b;O����e��Qe���cxu*ݷѓP��J�����B����	A��]�<-e�.x�r��q�n���������d�Է�i,�������R�'"�͡p_{ɠN���N8HG��öO9א�7y�Sr#O����iV{���"�4r�L�<�װy��灗9R_�h�h�d�u�Nr���n�rW�i�x��x��Z9�������u�غ?1+��=m��^�;e����@Q���GA8wui�W����߷���os�ܦ�����յɒ;i�_"ީ��gGBg_����΍�ST���E�)|���-F�����^��V��^в] <�� /F4�o6�fŗ�G;��N�:N��ý0��=��`U<;�oLP�Ҙqǲq=B6ʁ�ޟ���%�-u^�/�@��X�$6��FFYp��EJ����JMG��tA5�N^�/�����~��/Y`	?���PuR�R��Lh�9���E��yO�����+����u/s��`=#r#��U��u���D���/�9~���=�=�-y �^QF��Qq��`C�H�V=["p�-XO���Oc�L�:Q�e���6���O��[��[������2|��2�6T��O��ցJ_��3�|�e�?<&��(f���r���S�T�|�� �i�2�i^�%��ůsq>l|��h_O"���S
K)��b�������;J�?u=�6R�k��q�w�+������O ޲45�������I�o4�IY?(['@a-�$z��3��F)w^�Q#��QC���%�F�p�_o�5�HO(����YO�gW�ҟ0�/B�@����xۗ�2n ����K��h;m�M/p�_�����w�v�}��r���;mj~��|�*����ʆk+�m�u��4��:\v���}`H�^�͖�_�v0_a����r���_i82�ڏ�oU;O���$XL�W�I����[�� "�)R�K�9�InP���s>�zum͓^�;����:m4�[y�#L��uQ�X���p���s��=�-N������#��̈X'ב`��q�f��q���D�;��Q�:Aݝ�$�(a�3�*��R�>�!����qm��;n�m�(Lpe������&p8�8K��>�_����/+��m�XI�%�p2�9O�5յ�z���9b[���4�t�o54&�M�u�5ܠ9M�j"��k)m��^�7�w��8C��-N�Ϝ/>cw!���&� �@��c��d�7�)�Od6��,I����ʕ�FǼ�Ƒ�����DnS�ԣ�D��4��cPջ��i�����E��/VO��7Z1v�p�"?���'�G1]� ;��*�������tm�׉�&��z����:B����j�&������v:�C>#I8�XƎ����F9���`���ʱ<L^��:ɝ2�X.f����	�>����oT�¹���M�R'%����"R=]P:��;c���H��p�s�ȣ�m�����N-���p���� ��/>�H���>�x��{Ϗ��(*aTPx�?W��0->_���Gm2v-ֽS��v�Ht��}]�4=�E��,y��R�|�Nd9e�ṱ�� [p��������N�ݨ��-������:�K������vq�u��~�86�!��k���ؙzh��x�u©����r��e�c�Uu��v�I�Zi岂Hy�0:�Χ��-(w�iR83�t�`;X��'
�+P�3��#��M�5%�Ř��/ثgۑO�黬/����JH0<8�=�F���O`��v1�u�ɿxd��d������O�ٲ]ҾDlS�T�����s�l���^;Q�M%O����Ia��}���F����C&�m�؋��6\��lm�ʳ�6z�Ul��b�Ӹ�f��F�dm.��֩Lh5��E��N�����byE��P�	�N����u�N�|7�d�f�|1�eEEu?-)���Q�&rx�SŢbN$��[2�3�T��X�V|@��9QD�;|����Ӓ����hRE�RN�YS,��Wބ��O��lr��[�!"��r��S��}3�qj2-�Ʌ�\J�#ͤ+9 �7�aXt���:h	u%�%�&s۱0uW�W�C��p#[��k}[�r�"��u$4Ŏ+\�Q�]Ԯp�%���Z̾ �7ߞ&�������/ޞ�%38��Ͳu��&w�OJ��M{�l�CS�9������=]�'Ug���Ѣ>�k�"1)�����I)�kؔ@k$�� �eU�b�<��ᮕIX���!B,��wӀQV�(�i��SC����$�wa���I���9�FD4R����5\���4D3�H�S�h��Ζ.]7T�Ի���#�N���H9����(�;.f=�� �v�vd��Hؾ��t�jG�[���/q�Ǚ��	�Z�3�jA�[G��u�{�0�{2��#�Q���G�1�\�[(�_��IV�o	�F���G!�,G�|@��J�@G���a�2��u"�($��"�}���Sǵ�^�z_gaq��� ��Di�k|�ӯ?�C�P�� Ƈ>����O���78D���YH!��B$'Io��Z���r.�.^ !�x&�4���/��D���sI�ѡ�B���y�l� �2]i���.>[e��DR�����eA�G��W߬:�7NXO�>[Kӻ�aǯ��(��<� C�=S��p%�����`�uq�jۻN�!AM��EV���~Y�E˵K.	��rnU�%�
��}������P�+�~ ��XN���ӗV;��H�@��i��5�v��0]�b�SJ��)���,P��V{M�u�l$�!�(�>~���~���&ԗ�_@�Z*В`o9�P	�Q�6\���i`�~�Z�KV�$�Ϛ]�YBj�{���+2��WlП�8I x&�l���'�s���ߩ�-������:���a`��.�2�}1ر�c���� g��'��"��(ɽX��lJ8�G�F:>'�Eo�ړq���I*o1[��\℗�i�Mʜp׉ܤp��Q����ߕ��h��v�.�� �b�Li@�a7�g���	+2��$D&�|�ŠsF��E���M=͋v,�9}^�\쟿��3�����Y�o�U�w�2� w׭#���p~^"��!��ʦ����    �ED�RoI�1K�{8td\���>#���Ņi�j����M�X�d���?sD���VJG�ǳNPB?C����4���J9�AU���U�]Li#�^��	��/����_~�"����HJ�/�]T�S�ϯ�V�^}�� ��d	�iݝ�sN�y�l���)���0�z�)r�R�`��J�x�^��{l����(s9]'��RF�ey��o�<�ݴ���wdK蔅:��^�$a,˂D�����S����Hpk�/.��p�g� �C}��� #Е��C��<i�^�����`���������E�R�{��˱ ��v�&ZU���Hp6�Z�2^���]���C�D����!)9B쿾���z�'��8A�4��Y��(�J��VA'h�a,P��'lf�إ�	�]|!�����<W�b͹
��Y��[�xk� ���p��ǚ}���k"x)%"���6y��lz?Iڰ+g�	�V���w~F��׀�z�ub������_|�00�Ȥ��@v�0�
�Az�jW�X�}a���?�<���x�[�6ky�b}P�;F�/��ܑ̓N��@�p�xe�W�!�Y���7.�@��㏳���p߅���Q��e�%�9?ʅ��k>��nR�~Ō7�y
�����0���D���f��c�cɧ�0׵y���ߟ�����,z�s#�Y�=�U	Jİ�sӲ��W��@^�*k�ш��`��B�� 5�+�CP��^�Z|`t�� ׾��yЧ���!"F�/�H2l��x���D<2z)Q~��o�3ކ-W8S�jSO� _r/�|� �e�-/ᒰ��=Om�v����#܆��N���^;4(K��x���xN�K������`��V��n���������,o%�-!����N4w�OKV@������d3�_񡶀=&��kz����F�)a?P�+,@O��SO�h�Ji`�,�;���D����`F{ӈ4��̅���ᰟ�;W}/6WE�Z���4����Y�(-J�}���ū߸�]���kWMvFF+�	��n�8�-�h���u�()���P�8.5%5��%�:`	s�����!d�~ȸy�>�B%{�'F�9��a�o�)��t�܃VG����d���uӢ��z\j$3�ݸ��'#�H  �ՠk�l�s A���Z{w�V[���Ɣ`��0���hz5��Z�EB#���l�{�������P���ya'*LNT�����+s�<��M��(�?�W�:���fĄC8���j�|�4�]ܤ
�����F���GE���/���,%u΀Y�5�KE��aQ��!㉿ݶ����l�g=_Ws�.�O?q���u`Ț���<�'�O�[V�P�A���+�ko	�T߳rL����^=F['8�H�"U����ڟH˒Lo�q�]��r�=uac���c�:�������F�� t,0� `�c���\��|�[!u�)S��}��o���k� '���]g���0*
y��8�|�tA��u�ӂ�U��_|�t���uz�g �7܃[�X 6��:�	
{��+�߇�r/�BnTH4��'C���Oԃ>.��D���ħ�ƨJe�)P781M&�֯�wYE��<i��6A�c˶��<>
j��K;�'�;����,	�x�6G4tX��d��@�?��?\�g����Å��!4!�QO���є�MД�&BO�K�lk��8֗�����@�B�3��5=�6��Ě�m�Gl�R&����D�U��:��sM
�(�x{�W�Q�3^O�N2` Ty� f��8�ӿ��:}B�R @�/�Q������\�kf��P� ~� �H���3�M>D!�^��u���X�y�(�ߖH����a��|�3�|�����ԯ>���'<����@E�3T���yx���������>�U՞A\�鉎���?v�B3����ȃ*9-Oӳ"����p#�����P���B����g�(zϣ�!�^��_Z�\Z��䊊���3�\;��Y#��ʋ������,n��8�:��G:U�;6y@����%A9͓�!���<J�j���N���+�8y���~8C^C�3[}�# �D�M�g���(�\{x}�#P�ʡ牊b|"���S"��ҥ�>��ÖƘ�c��6�M�Vi�W4����*7���1s^����d�P-$U�l�RX��{��B�� B��|��W��	�'���H��1�)!�u\�V�\߯�i5��l�cQ.�ͣ=r�B���Y�o3�	�>m�$k̲b����Ŧ���bV�Xn�[ꌟ���]���Y/�6�ڬOX��0ly:od|�읺���OP��Π��fO'~魯W�E�S�8\=�T������8�6}��6���'80�lM�W��y|���L�Y/ݲ����s�雹���͖��u�ڲF`@y�{�g����%U@b�xzu� PPǡ���:�/P/|��Ec׸��@����5�	�`��Ox���m�oϼ���:�	�]�k��_n�t���"/��o�
����ܷ�q��Dć���R3��;��U�<��2�g(���b���I_o�|��f�E�A)�`K�7�2�5�t'8�w�bߨ���:ΉVEf�����}�<m��Y�7�/p�u ck\��+��5�o��l�FK�uDN�!�c,nzO���m�?}�Ndz(��;~������:~�����L�(Nz�܌Y�-=^.h���O�2됥������_��@G��d%%�>q3u�E0�#�v��D��Xb�˰.������rVnׁI�vK�	��3k�X\{k�ϯ]�$=�/�RgzŦ�B��"��T$:E�bmC�UW��$TO 7�Eb���V�Ѩr�Ͷn���F8�I��9GGO{�&Z}�Z�]G=>��p)���c��&������> ���&�1�-ph��E`1�w�ۂ�C�bԷ��	|������8�\l�Y���w4��q*�z�7���&�'�7��d��n��K���Kg�9Y���; (�O#Y��3��;o�}#�z&�R�)�v�ϊ�%���=q��7n���/�J�""}I�|i����a�qs����j��ކ�"u)X���Cݺn(���rq3���:{	�E��u��\K,�w)�R�T��<�	p���p�~EpoE��7�=��v5��a�̵�E{9Ŋ�����^��=�f(��8�qk�N[�=��jR"O�w-�U�uEȒ�iȮ�vS��>���n\�bEܒgVg�'�:\h�<u�u���/v��D��Q��::g�*��o:����^x�K%H�g2��N��K�����{Y�ա�e�Q��Z��}�̷��i�R"y�[z��f��뀎�����.��pP
�e�k�w-*�7~|ڑ��a��S�̏�M�r�~O�.p���Ǉ��$��=sk�}	K.�4�V��kESH���̝?��nԎ������Ҕ�#�:�=B�b7�H�L��s���b�4�z�
	w7�X�d��ܤ_ [�1=��=ң�m4�~`��^Z��x�?�V^}2ٮ���O��U�+�v6U_�ʊh%�[�8�WmW�lW;��u��C��h���I��~;n��
�w��@�Q{��
h%�|`)8��u��Y���h�����J@Q�{��Yү�������nJ������1��z�:A�g$�+Ux��~[D*O[2]�����Z&[���:L	O<䬠 9�-㙿ґ�����~��Jpd�n���,[���+W�RE�L� ��Ї>~�_z�#��:W	�w;�r0�S��������Q��"i��"�fB��z�_	h��׎b�8I
z�r�����S|`w��HkE�R�	��9t7_8q���z'<�#Z���%��&"{��RFy����0���hd��f�A��$b��,�˻lw�dc݈���ܴxJ��K���=�݆���^�I^]��
���o�y��@<�b~+뫽�d��oX�ۑ�m،G>4���W'$X�_��:��*X:�&��S��3+���7��@��a    ����6�휸� � J���0S�w�bmS�jz�g�oT��e���I����KڎF�)���Y�>X�o�S}å��O��r ˆ\��wZe��&���*��,$�����v�|Lo�q����z�6��L
�=��Ӝv�,�헐�b��T��I!��c"m�+���O���4	/U~#]M���o�9��#J�x��h��|�p<�rYɆ�y�"�:��N��I_j��W���/���!E�X�s9$s^�_b��ͫ�"3)��c���x�/���g׮���$�h��7��?C�����]h��UKP�K$7	�����+��=h��ؾ�� 6�>�&P��>$؎�����2�sl�{�T�Ȥ�^7��pڇ��[�V$'�n��2���I� <*����ߢS���bCJNï7(����x�����Ϝ:�֊����MŃv(�?(��&d$���hN]Ɗ��dI�����_��~�O'E�\>
N��?��ݺ�ԕmשW�ExR�̇Li��+a�~sޟf����N�!J��y�a�0�:��uUީ3��Pʖ���"_t����/'f@�V��*��fǗ�q`���ކ��a3n���J�.C��v+R�B�a3�֟@L�~�~`���!J)RK*���U����|����J�i�����ӈwp��(O�0�=��9��_��W�뫯X��8z���h��n|;����NV��h!z~���q���R�N�����V���=�ɽ�k3�3��<�2�u8�� "��O8��_�^�'N���Kc��� �h��X����g<�Eh�#M%�j#?AݮӖ���ߙ_J^�x7�;�����|����g�g��K���,'i�Rw��� �f�6�ɮS��b� ��Y���c��,�w��C������&T�}��� ������H�S��E�t˲�)� ۽��^����b���]9��I�aĄO��y��d�
�]�\}�簤`�$�y��~����u�.l� :���å�g���y������Od:�S[��������u���{�p�px��c���zW�����s�PX���=����l��3�y�^�p�ȥ� ���/����Z��',��u��&J����B?��X׫����[1A�)_�q�DA.����Y$��u��wڻ^nτ�������Ч8s�<6�]��E�XJ00�UӘڅ,���T��o���ˁ�p�65��艚�"�>9,��7fmX�)JjصcL��u݀&=LLݠ��"��v�G[ʭ�H�6Lm"��rt,������j��9��"��&�����J���0zȭ2�:B�0]��A}�ĉt�>��e�cJ���ԬX�����G�Q����}�&/{��8-I�M�=��DV�������Te���k�E�v�qIh} 0t�[۩~c�i��ZO)ӣ�����&��yF���ձNd(Y�7o��+�P�e��"I�z�0=����3,�C�y�Б�Og	5�F�l&��A��t�tx�=�A��q��|3�o��|�	K�S����%�,��Tu�Em��E%���2��Y�}���OΒ�;��v�P=�8K���
6�j���	mS/u��D�>p���t�y7�E�еy��`��J�>�\����Z��vwX=}K�3"*a<q`��D�������	'Ov���$�����W@+���|Zd�N׉�Bh�&�[fN*�*bp.uB*�uB���p��T
1#�.��܍���W˜:�ȭ����\�7�谘{��DZ�r���[�*!@�H��a��.)��=��:�X���CD��9KT!	�<��%���Ey����D�m��SC����&z،;|���_�x��>��Ѝ��\������:�b��a2�_h%b/�G�<��ү�0�#;^E���W��h}l�M�z'����O8����]��D3�!�C$�K�זHd����q�r�`��Z�H�Q��<&P�woa����߰�F�/o/x���SN 2��B��S �~��:l��zx���VJm�+�pp�[6���g6����.k�`"���8��z�,�3_��N���D$�|t���m,y���;Pd}=�R>[�y)'s� �T���p��x�pc=��yXN�2���#�4�3��T;�QC�r��T,�Tݏ��0oHǮ����z1D�2�o@��,u�jv��<A����R���q��_F9h��/ ݴ��:N�w������3_���W��i�=�t�k��u�p���Uz�z�+v�Q��jJ.�����f��Y�J�.�l�vx,r�r�]KW�0���&�'����kJ܈=8�iˀ�X-�&�־�X����z�2l��b2<V�� ��>a*p�����	s�H��6����o���������%@�`Ԕ�~p�Ra���_n}��N�6%���Aゑ
���:]��20ď�����n�[�7�H��U!����^��窱�̦rh2UCJ*�y�*F[_T%���tnt���^b���>v���o����~�li�=�K��(m}_�Ω�~)�59�.J����ҋ���p�_y�-��
��t|'0��uHt�D���%���o�f��ʐ�����%/WY�Z�Ꟁo��9�fD�_�a{63�`���>.b����á���<�.�Z���i
�&�8�r�i��/n�DWߪ�s��QD6�8_R���<]Q2*�������Ws"�)xB�wS��t)�3z���k���ErS�%"���Ov~��8��>��ݦ�&yux��M��$�~�y�M��o���>b!�Ppի��ܩ��h���W ��"j#0��om��uM\jy�1F�L 8ñmeLM�vؔ�"�f��ׯ����	=�h+�2���nb��~��^��4�E<������8�?��z��m���M!������u����s=Zg���� t��y��@�~lz�ۑ�7K�c�}���r��,�}	=���E��TbA�M�n��l8/I��h���uzԥݜ�l�מ_:{�|-��ͦ���p�։M$Jwn�z��m����O��#�P�o��䛛j�>ԭc��Z6]�3���ه�ꡗ `*�?��#~�p��o�)��N�T䥒o�;�h�]�r<M�D[���%�G��-U�ߛ��\m�a1^;fXKȪ�B��qי5�7K���I'�L�0�KT���v��Y/M-��ޝ�V�+��h���e�^bY �%0�VJ�Ʒ�,��������&�U9�~�&a�8�(��j >�ËH%�B{�0����D;ɦ�EB�O�*�D�W�0����lv)�Ǩ�_�_(	K.ͻ��?�A�/�E4^���u��"�ZA���v��6Z�w��-�_>��p,������e��`O�w.]�1���˰�*���ɖ�j��X�]��"��x�Weiq7�g8�'r���)��'��=�����ȟ�V�i>w�;�z��P�A��?>�Sr�z����!��P11?���"�%���@Zi[/R�-j�y" �#�spcM]���_�B��Ff�
�����M=��Y��>���yt^�D����Ȱ�%����z�!��}~6h�Q�g�xD4�\�C�u�%2���g��@ �+_��[���xU,����G�zy�q~�K�i�'|��P(l����6������߷#�#��J7�~U���ʓ<l&'O��-�h��;�z>�"���R��;z��О}��DR׫��o����:��f�r=ہ��.6V-��6`�����:}�B�9���浛^�@����mx�%��N'6�^��^�RP�P�Zr$�'��,���nS�'��8"*G�� H�ߦ�
�S���WqF[�ŋ(o��H8J�v� 3u?�ൿb���Q�0� �[�.���C��>N��u�7=L+�y���Ӿ㴽N�_�{2�NU�7���2�Qط<"g f���S�'���2
�Y�����V=���׾�d���H��x��#��q����dZ�RT�T�"��    (�1�צ�W���5�z�Y'/���&&� ���)��禆��It/�� B�[��/;ӐY����E�S�,�֑P�t�.�qö�β�y��A�?C͓Иm[/�o��P�9�!a�W�Gmi��_{ɤ�'��	����p*�5����0^�]]"�w9�P�ڟhAT����_ljv�#�_�����6�6c���������hq-/��b�k˯��-�N�Z3���<�dq��H0��̫>���[���D	���Ljd궮��=K2�n܄'������h���:����k�K[}s�?�V��;��:`j�x�O8���z ���}-L��zu ��\���/w�u$sCЩg�눥2�It�`�|�5�.�`�c�|������t{��^(��ob>�1���5��Æ6Á�s/f�C��C�g��%|�`:ٻ���� ��V�E ��Jd����q�U?�묤��e��K����tC�'H��uY:��򎬍�Ymtm����\m�y���5W���as+��ds�/�?&�u'�����;�ޯ|ё�p7��2�\�y<�r^�ͯX7Skw���N)"SA6�L�3���P�S�#�����':��:�������:�}���]V}+�n���J�0@�A���B�Y��4\�����#��u�����p�3���.(8�{�u�Pe0��5��>�E�o�.��M�עGOh������$E����c\i�o{N���:H���D���(F���#���/Z�S�^~®�_������@��v����:臭5�������!���ـ�m���:�g����)2��؍��.��>�dn��s�k�̽�ln�W�~+�c{fs�#d��6O8��u�4�F�?�T�[�ZF?��u Okj������v#P�N���tn��u�|����֥�EG����t��#�Y^�ln3��a��sfs���e܅oV���;�����K���Y]� �g�>2 g���)~�����Z�?�� r�P\"�Ҟ�X���uNΊ�˫�)��q��ˀE(������m�3q�!έF��2V7�w������0n0y��ΐ�ks� ����~C���Ǹ���μYػކy5W���~6���-�P�xZҺ�H�'�{r5:�f�<�ش�a[�;�Z'��	�.������iu[�p���%���%��k�Q4SÏR��?\h�#ܿ� U��k�����B��E�`n=ULVv�a�2C�~|�W~���NP���:O�l-q�+���:���?�u��l/����P/d�_a��׷�3��Mv㾥�ه��/��0lj�	�W�8�R��^�p�-�m�}ԍ]G����+g��u��l��N�&۽oln$u�qX����˴��ot%Ѥ���b��� �hFi���2�����a���L�����@e������0�g�akͩz����Ul��*M�F2��@��k�y�s�o���e]�)֮U���p.��e֐���m�x���6��u⪖/&���v����^���n����ψj׹(d���#O.9SOB��j;�u

��	
���е�=�N?�w�#L����'��A���2��E�.�u�Im���>���C��/Y�����V��M����3yX���R)��:���pװ0y�ۈ&��� ���a^r��m#
�n�xݡ/�;2�a`��^VH0Ӿ��q%K��A����8�/�uF	Y��Bȧ�ķ�[N���]B]�,-j:U/ج����;�@�a���?���!�A��Ǵ�u~�݀c�^��$��}fS�����m�.<�����v���8�,Tc�G�T�����8̜^v�J(&k���G�;+�����<�}�e Im��n�n������3���%W�=���:����5�GҚ[>��?\o�����3����掗a�pU��$�B��������3���U������e�?]��a��﹏#��֙���W�Dr��D�,V7�"�~C���X݈�<��?_}?�^��ϱ��7 $L�7�N�h˾�WB6s0���ˠ��عL���r�+2������Y���32h�S��bjw����^��q%�`��?��?�y����.�-��_���%d�;ԁ���3��ƃL.���x���+'��	%��W���A�^BF�gk���lV�_��%3+���7˞�◡%���t|s�'��"�)��1�ؾ��~�C����w:	Հ
L��w9d6	Z;}����z�;�+w�O�J��2<��qY&��ĕ���m�W��_���ͨ���0�ؚMɍ��eOV׵W}Q� cK���a�܆KI��f:�a8���3��y��"�x���n������j�=�/-3M�l>ݭ8���'Mfc�<ߵ�.��mB6_�4���Ꮧ�q܎�f��Zk�ދo�k'"2�l>!>�����L���:���e�	�|Z�d��W:��=(O���e����x����<2���Pf���4�M�(K�ծX����ښ���*e�	Y{��¦�dO��?^v��ql�
�h��_��v8'P��[�)l�7��7��6Olk;��R����� ��L!Y^�ɪ�i��n�[Fϱ����F7��;W�P��U"#Qpk��q\~�e���e.
=�}Xm�Ģ�%��\�[s��a�o������}s�xs�-pK�!��.%�X��{�_�0�bߖ���σG����3�tg�v��Q���p�yK�E5ql��HG��X�L��c�/�EE�׭G�^j�2�a�c���z�Ъ����ĽK��8�h��Y�W'� ~d:���ȼ�<_�z�z{0 �`�d���1*�D�XwU�� ��Р�L�jw��X�G{���)I���-����+�Ʀ��b���.��m������>Ѓ΅�� me��e�%CCT(ѱMYC�%��O��TAT��%4CU��8����K��v���7S�T���*^��$�,I��	�L� i����O�
�
Bs_5�����CQ����x�����������&ԫ��9���Rn��t�L`X�wr������9v�M#X��}S��,w�zC�W�����+ �?�K>����e�{����B繠u�^�w�]�U-�V�/�^��oQ�7?�Z1��M^���[���e�����J���x�X!@Z �|z��Äfuv�>^�Խ?g��f�+c� �����p;����������-c/^�f\����*�~�!�p;5�Ƒ�øm�Y�}9�K���XR܏�d��%4�_r���2h��H9����d(+#W�9�0����Ҧ.���(o9C�-/ V��Ь��ٴ��[�ޅg+s�q��/X<�8V��|�K��n������Z�d�+����bi4�,��W��Z	Sx�~��f���m�@+s�g���/���8�"%^7x6��?p+XZ�籆���v��S)����;���- W ���$q|d9^�^�_ ���b������6��1B/��٥]�tHZ�ewiL��Q�2�c�h���Xm�F�{B"�e�Xr�5h��3��>�uD&-�ޝ��ur�~�B�9ʎG�� ����x�+�*g`���V�ƛ�|ձO5��x��f��mC�UvG�g�����<QrXܾ�V�%6�F]K�T��н��;Il0�dn����C�puo���ء_��ӮQ���*�p|7�<�3
{�A�U@`H�a�<���~h�����C�?g����X���xt{yI�6�#4֐�4q{���]�}�g?n�����k��쇘�qυ.'���&m��m����a��qg�h���[�$j�P��G4S%5�\G��vt0a0ٵ���ꆻ��f���/��Ǝ�}����Otu-�3�G���w�v�x�!�;�C�{9k����\�ܚ�/�������b��e���v�L�S���U+\+4#��$k���_����Z�i��.�	뒃]u���?�CS^��{<��MCS��a<�|{��놻����fw���ޟCj"�]9a���Д$xl��    �{B#��%�����]6������]0��)I�c���=�#��2I{L<O���q���JcS��%�*W�����t��)�]�1*���Pƍ��+�Sأ'����n�t�1\��P��I`����`7C�p�w4He9V�&Q���14=%���]��c6�M�43e&/�XC�_��L^��!�/������.4�KL�RJqqP�����{�e]��Qv�\
��MN!e/v� �/_�y<Ş#�K�B�R�2�0�;�ޣkg��gn˴4S�.��~���bL�����́��+����6~�a����;�#�/,d���S�.WUpW�,�^�RO$�4����l�-MZ)���+���4i�!.䜿�P����r�ְ4q%���Ƞ��P�f?ƿ0��ϘMJ�P�/�~0~a��;��%��ۛ�4`%�;ގ�����ذ��[��� �i�-o�6����Di��GAޢRԆ9�ei�J&ot�T^Y����	+I�]*A�i9���=�hi�J��w��w�h�����*I�H����?M��H�'�*����!�q�s�a��8빂�4c%J|L��F��%����*Qܓ�/oi,$M8d�ݖ���l�I��G�<�%r3,MX�}w]��,��������mJpM~څav/���L���������
r��4|�.����$s>4$W�
[�r�y��2E_=���,��Ӧ��ٞ;D�Mb����>������>4�ŋ:~G��ڳ�Nq��2���8~c��z���bi��Lޙk�.�,�]ޙc�.�L�]^��rye掱e��%��FSW&�O�t�A� �Wfn�4ue���mc;���,�͠�+O$.�d�����{;ׂY�܉�r�:ޠ����xٲե��ɤ�+Q�a��:�-���]�y+3yg����ŋ=�w1��;W���w��P��e��_�_X���;��0m�D���f�
�Q��{�.�/� ����c���?SO�X��������F�ƛ;��_�O3ۘ�^!�A-P�;�UR��C���niP˾�_h3�\@W��;R�)-3yK��//mKyq�eyic8n��K�ya3��Lh.K��+	;�j��Ȃ�͝N�o���h�~�D܀fk<j&�;�Y,HV<_�������;�3����	� `�dx`Y�+��_��0N+�m�F����� `�Ym��~L?q�~����)������k��q���=+4\-�X���A�mJ���<.pXrY��4�Ⱥ`�m��ZO�z��Y��J)/.�!�a��HgQ/qp������~\�=��fw<�9�`�qG@4����7L�����&�� ��A?�)�]-���<{_����q�7��I��2�=o�@fICBh��v���A�
gَ(v3�@fy%w>���ܿIb�}_��������-�Y�ЩI3Uw���g��V/�Y&yәȟ8��]�B�$�a�xƈ��m '����>�pHc��r�?@-��0y/��Ƌ|��}BH-I��ߏhVNS��VK8X�_���L�$�a�5��b�xnwn�2�5�BG{���%�8,���㱏޳.� :��3�q�Ȋ���WxW�{A����|�?�����.�'wb~��J�`�/�o��\�p�/�o�b��䆟1:�Qx��M���_�L§j�����wN .Q^oɷw�K����.Q�����X<�s�e�{�w����&��-��m�5B����S���0�B�<A�.�o�b.q�F�/�o��vw�LMJ�N3�B��W4�mZ,l-.�Yv�] ���� |yc�Ų�-���s����ω��v��������O�Ɂ�w�������c�>���4�8��n-ӯ�����Ql��SO�H�$���a[\�0���v���D���!�o, �y�:`��7p�Nasٟ�k��
�im [s��[�7q��">��V��`�7�0��7�x��2	|>���"�Edn��] �L"�N��#��F�>��.�[�ķ.a{��\0{u���.r
����{ikK\>�98HG��U�����hgݩ-�{� r�?t�棒W�?�ܞ�ǥ�W�֐�7��!/n���Ó�K��3��5�q���K)on=V�w�ߒ�����E�����&���ĥ����7�� ��(p�/�Q��^��ex؅��1%�ڒ
-j�����/�.��
��yE؏�o���q^c�c���W�$7*
�N�o�.��^bXPrK������r��\�I������� �f;����g�І���Pz��M?m�]=Q���L�����<�E������$5L��#㎻���!)_�UM�&
t��Qm���?X ��úL��Ҕ�o��S��t>���ב�7�3H��GL���*�ls�*��L�KL�B�e/�W�������d�\��O�G|Pd�g�v��;��@��,�DSLg+�wq�b�$�!��ұ�CJ�}���nH�tY�f��̘Wr#�p�c$��C�a�Al�Cykv�W��&�GE ��fL���� n�#�ڸ��d�_�I�^��.pc��w�T[�zYc%|ls�����i���z�S��H�_����!Mx��t>Zn{����C��e/�.�b&�T`�|�C:���7{�]����~�&���㪨��7��QL��(r��-��(��%���\��-����������sh`%m��Av3{��s;��({;J�)�}�E� ��$>&nr4�fv3�]��L|�Gr��U�>��.Ph���>@S�^@h�۷@�I��8�b��)�#Hnq[��3l�ג�5(~�c6��8�'ƫb�V���''ۮ~��4I��G`�v�c<-n�lػ��R��q��b��<^��!��k�����f.q�^XG���E�,O>H��οĪ]��<���!�o,���U�9MB��ɖ��o�]��:N	����Av�c����N�ċ���7�Ɔ�����L!:�������g���_w�150�a�m7��p;���R�r>��O��\��޽�H��$���E�Ah���d��_�c-ꮶa��[�%������Wmi�l^.m�k�"-ͨ��a�h���1��K�i��x"|YI��eHfXJ�ĸh�?x��X��������7�{�OK�h>��wك��xN�v+ACh���2�����͟��,w��Z���~wr��$���Y�i�N�/�oڂ�<{/M���������/^H����-
��q�C}�mH]1Lcf��,@�F�1ٓ�Ҝ��$�pu�>��k��k(�-͛I��vt�5e�7լ�I3Qޏ)/ѡn�p4�c#23��Ɨ]?�+��Al�G�&�D����.���_8�`pj�2��ĝ�}YA�C]��
��ο��ArXQ<��ݬ�ӟf�D�?�.��wE����4s�!o���C��6�͜I���t�g�;+��ș(�q��:Ul�\��h�L)�첱���wr�xv"-?Z���2I�{#e�Jkg��tGser����]�w�&��P���]�)�u���q73w4T��}�w���Δ�]�͒�!@��������ǝ�ٙҿ��U �^?Tg@��ܮ��Xs�:1�B��d�lGSf�a?������x/j�����U)70��ͽԡ�)3�po��%7������ь�����Z�P�`��g�4���W�;���2���_��;Ұ/�h���&��T?�ߺi���(=.4y��c�h��;t���7�[��rGse��%��]X*�����Җ	[�p��%m�',I��]:D+V!{�DSd�����G��(fcY���M��������j��p;��0�ns�H4*��l��DZty��ۿ�1���?���.�c>b�qIcvpWmc���b֑��4���K;��5�+��%�ԗ\��9XGTҒ�ɚ5E%�x���-F���u�{�hG�]��pI3��`�\���t4���ށ���^ڲ[�=�G#\
i������!;��?�xp�(4�}:�MX҈���5�Qеd�c6l��Ʉ�ڷ7�    �f�`&����n�{��
�?p��,^�������WȂp��hK)/�P�#�A�?�I@ܩp�}/���%=���YSK���:�2�?�kȻ`�|HV]�=��ɛZ�N[�o�h�J�9��y@��aw4c����14��c ���b^[�܁$MX�"�#\�GB�A(�>��ь�'�8)v�i�J�x��M�:5��5�oFV���ܩ��Y�ͰC':��~\<�k�����H��kNWE�l"�7��夹�94J�ܳ�4����ܳ�]���\n={���^��ö?Bk��m㛇ic�e����̝�w�a�/I|E�̣c��a9ь�|D.0�3M�S��k��x�&Gm86+�:��Vw�A^��Ǎ`��^cR�|�W��B���W8[�!��V�T�,َ�`m�>b����	��e�r)=lf/wE�Q��mI�r�{��Fi��?ӹ�,<o���킕��s4ȶ��]�h"J.j�;�"*�A�E�@z����'^�qK��=���.M=9�o�]O��V��=�ԓ��|SZ�\W,�gCO�7	�;��Bج`ø�s�vōy%�+F�
��d�5lr���<�
f�����'���K�jG%~J�쇂6f�������_7��u�E=�>�i���0VF�NrY�<�XGVڜ�����~8j����9�e-ћ��Ҧ,�'Sא�6e��yBaYi;6�6�6u�ݟ׺�^r��t��8\C�FW���s	���qav��Mr�R�|le����1Q�>z�겟�kvަ�t/l��	�w.��xaw������Y5N,����6���e�����&�xA��x�-	\t�z��.�2���SW�U�q��4�$�gk��`���l��.X58��O��VA`�a߾�Ѱ����fy��yp�d{�s���!�˽���9%���*���b�j�y�X�4���^ےG�LO��.ر�m��,�̛��.X��&�6�c��u�N_/�)�O�K�h�,w_��x���N�c�G��N��Ɠ`��1�$�nz�����.X5/���U���-V({7�7�/صۻ«;�^<�o���^��v; �̝��ż��1�w���R���=V�˞�>�}�hI�����1�i��\��h�#�+����d��N���^�r�=s���i�����'��c�`�{���x���y����'���7ŝ.�"/��a����t8�'��L�LB_��E��a�ۄ�$�B�ׁ]�%�w~�y 
����`��i	Hz�.���E�j�s��4���y
x��������1�Dǭ�6 4�� ���.�=	�� l5���$Gwܥ�u�B�y�4#9=u�A>j���rS�[����͍�:�ƭ����������`��k�ת��$���]��g�C�̝���$VQ�3,7�V�4��S�����~F����CЌ��{O��c�H�/*i�b�靅��H���������-;iآ�C�����m�iN	�z�]@�G��x��ٟ	Ҵ��?��d��Jc�/�4-��CsK�c�����~r�,.q��F���׺���|0N�o{���%{�b��/{�P���ӂ/p9��r�Rh�	|�*mId�Ҡji�^����8h֢�.��l/�?��1�f�'W�4�|/����I�{H�g��x�e����' �mw��W�#�ߙ=ܧ'Oe.��� 3i���~�����j��W�4������1ڔp����;��>l�5��-Uӄ/��߻j3�`���gɟ���&O$.�X+H�d�.���������7Y/.l���m1h�Iv��:��5U�/�w��!�>�D[��Ń��~�h������������z-fH��i���v� TS4bk�U����%HXԄ����-�a� ,-���a��_
Q� �?j4��ؼM�y1RjBkpʺ`َ)�o<;��;:��%��8񺆨f�x���48i5=6qLw6��Rr�V,�����XBsc)UM3J��%ň_�#�ɚ�ud]�aY�.�d��$HVܠ����+�uV�e�u�v�ٜɓ�e�yR�4��7�w��f�V��F�����,�:n����\�=N���h�\!��$1�kMф(��	��k<����e;����i&������;�B܆��(6�Z���S�4�������ͬq��ǚF���b���.شIܔ���V�D�n�7m�H�K~�~���b��J��ѳ�Ki�j㟙�(n�6{��ƞxq��(Z���ݻ�8��Z�؄^��O�.���B/YK������x�R�����̓��Vns��g�y��
�>y"16�+HLPN���h�l��y��'�q�?�ٿ�ޟ�㺜�o���''��K1fV��64��#=n��=�{�IK�9,-v�֐��rX������y�7-�N��������+{7<MDy-x�.ZGp�����`����ke��4s^�/w�L��w�4J�'���(^^���dŎ���ASQ��k��ߎ)�іf����t�-��q��B0��P�f��#�������M��'h6��N�pH�nn:B.��K:(.�D򡯆�Q.��n�=v hL��l>l>�}�ZLހ�����)�gN��?���n|��qorT������@K9�ߺ��)�O(A�R���\v`F�.6�I����;SMKy*s�W�#3i��.��H�CC�̋���A)g�>�#HQ�$�G��H9��L
L}���#h8��g~?��P)rp �-��	�E9�`w�&~Ƥ`f6+ACQ���XmF�j�b�D�J�.��L�H�s���ܵ(���9�"h(J����r�IHY�F��'AsQ��go�?��h��n�AACQ��qV��~�r��@CQ���� ��=a�=#,hJ)n�<cqi�6��1Z>Mw�`�h~�L"�O��g��$J�h�(ﻋ�R���������v�F��P��2���PЖ.H��e( m ����H�nj�4)e����������m�i�d>�}̸J�p�h_p�K�Sf���sy�l���t��I�}cͽ�W	���wpqÕD�b��nߘ�D���|���i�aA3Sf��P~ya�.��� ��T�ڌ�Oy!5�
�!�/��
���u��S"�����n�49%��]��%��슍��{�F�Ԕ�ȟ�i��w��._��~�ip�]��jxw45�H��"@�]�J���;�Ǵ��O�$�!���D�Q��ذ6|���O���'������gSa�>*|����G1(�~l�&�@� j�-hP`܆7F!�B����~�z��P���$�р$�����L�X؎o�B%*8�;�@SW.�X��۝��I��ަ#h������ݏ�г��;�!,��C�*�vf+�����y��H�*��徂4�~a���Ǐ��^�r[h�`�y�>B���O:���b�cy"qnבx����1F�0U�m�hK).��A�	^4|�ó:��E.�h����(�3l��&/Z;�^ d��d���~H�'��oK�X
iqC��.�H�a�� qA
���a6���\�xMId��Cr��U���<9�Ϭ#2i�.��J�M���@���,q:�;Ǖ�J�i'�Nt4�e&/�^C^����-y2��.DtӴlO��G	�M\Ҽ������ťI-�,��H�Jad�
}�4�%:v�����N�<�|(���eL��	��ۧ~V�b��4}��w�]�%8� 
'iMÞ��.�<��x�A��W�v���^�5�]0~�cx3WOi�l�!���+.+��/�?/pѹ�W�.�5�1���<��|��e^��^�l`'
]���{�.υ.P~�����ᶏ�Qbً74���%ܐ_���%����6�Dr�4���x�a��m�xf��%^6~Xb�hYC��S�)��I��f�\І\FQ?B��sѰ����w�m���̬P��v�hV��{w��lՊ���7�S���K�#������r�C��Db<�����n����t"0����    kKn�ޛ*����ߌsr�l�q��Y^0yS���v���4�%��C��}W�m{�!�l��&���6���S�-*N6	��nI���_��_T-~1{SpC#\&��a諰	�O�{��$g�4��rq�n�Z��%�����)��f�$���gw���|Þhh�K:n��Ǫ.O����׮�y.�]��]S������o�r�1����9h��'54�e&o�8�//i����B����G����������%{��a���-��� ���Q^.�.6��!.m��,���A�������������&a�C�����,l�ޮ#,iѮnӟn:�9� 8짖�谴������-��
\]CZڒ9�_,������ozi\Kx؏��4¨�!Ni����1r�%;�n����<����;��aC�[�^Rm]�-��,S����䖫K��䣜!��w4�%HZ��x�~�n0��co}mhd�\`�!���;��g_M�S��f�SܓMm���)�Yu��<x�l�.�9,.v&�w��yq���um�*sMg)\ܒ�K�YJqq7��.X5,.�CYC��v�n�����)\jY\r�C�2_/�f�Q_���Y��/7�����E֗F�����HЌ��C����~b�fM�V���\a��K{�t��M
�:WCCY��V�W�GzR��ap�����d�b��Se�)�pY�m�ٷMe	BWAj��>�8Ϩ��ۆ{eLCCY^�]tB�!��c�,Q�L�����y,W�wz�}Gag7����䕫�-�}\��uY"P�&�������
�u�X��;i�
�����`�ra��~a���wUdeDmJ۹�*�[���2j�e�����C��z�
����u{_-�Rv���2��ƥ<�l���6lO�.�8����S�1cq�iC�T貞�/4m��[�O�l��u�q�i4A%�������j6ai���c<N�f��v����hJ\X�������a*�򗫿��_2���� v�i�ʵ����x�i8����&\)��Kd�HK�E,�^]Z� bi��5��-!�6�YGZ�bi�:�:��p|wշ;�Pa���d��|(���4n����$�e�m�xإ\�یq ]w�,)��؄��S����4���x��i��-��,�Ҩ/:�7���4}��%Ӗ_f�>��d���L��g2��5d��"�����}�XΠޖ��@#X^^N���`S-͠>o+����ÒI�ۿu$]�^��jS����'���@6ӯ��j�c3	���BwC\�cޜ���k��2����!/m�`I�Ч�{G�zf�zO��Nz���0!}�U��#U#L��V+��L;X(Z���-��ܛ7��r��1�lP_d��?h�Q����.�ZL�{�4`����eŭ�Bps����pb�����r�6�H��&�xy�!�3��3�]PҤ}�㮂?���l�a���P�BZ�b����9��>���n�a/R�,�o���=�'<[$��'�h�ʷ;o�Ibtٔ�>F�T�ö�8f46��2{6aIS���e]�P��b�qaIk�����~��Y��v����)?nx�o��m��`�ǁ�g^�Ԕ�0��)���!�LҨ�R\<󼆸�qK�^.�#�%T��Wn'RҤ�$�O�T��ws��A)Q�O���$�)V7� �&�<9&�y���E.�E^0vs���5D^0ys�ˢ>��4;����/�/L9�2�y���*y1�NU�[n����$y7��n��<n;B�TA����Ȫ1]�]Ԃ�T/U�`�Ru��:��l�s��d���J��S���˦ox�϶�>�˷l���3��.��Cό��4S%	���s:�%�_�%���NmU����ܑ4S�����ĝ���Ò����8#h�,y������C�b��
��q�^��*G���,��CI�T����Wm�i�d���&ɋ�P;�W�L��������6n��e��./�U����
�(o��=�JsUJy�hyi�V�[f~��]�m�����ߐ�f�?�4Wex���YY�b���5�i*I���O[h-b9 X�{�@�T�$��c�o~ւ��@�\�$o����d1dQ�{�$�Ty"q�Mb�_;��ZLm����y�>���[1�g|�@������~9����kҊ�0����eߴ�+*���x;�,XM]�F�w��in�^�L���F>>�}�O�S���?�IeZaq3���R/��WJ���y��M`�Mu��=��B4x\�e���!ߺ�G��/��h���rw��K���;D�k���}���{����sh�bFG�W襕��K��wt�_�+$k�<��
��0M�v�ä́�Yj�3q��a1�3i� ?�7�տ�ֿd��;5�'��9��$/-z|��㓘��5��׿�����[�Wx�_Zz����&>or+��Ŧ���V�o�6�hE��X�+��{�_b_-��t�.�k ����?�}�~�e�KԽ�T�xT������xUZ�Vj�������ֈ���>@_*��{��R�&րA�
9b�iȯȤ�L�
aϲ��i6`P���)$sUP���^!��y��3�I7^!�p>^_���V>��
ݠ�Н�~�X\A��K��w�}D���.�B�'Y��!�k8�W�Foؓ�ӆؔX��#f+���|3 ��o��u�:?��5!.�5+\~��.���Xl��=@2��k�.
wb�5O�!2\����Ѡ�0g������ے�_1�D�Em�����SThvY��<B��xY�nP�rr��{�i����}��gym��,�/du�����;۽.�$���,��+c�,l]��i�P�`����q>����}SB"+?@�Xm�����04�-y��[�+�o¤����kz�0JU���c!�7(A��h8��w񳴸�k��}��ymT�P�%w���J�|�jɴ�=ׇ�Z�_�ɜaȇ�"�I�c�=�Z��qȸp7e�&�qY��k�l�>�1]�cc4�:��B���۷V*��M����M�V�2�5��4u��Yw�v��~��5l��Z7���	��lw���w9����#�V�s_��H"��uv��f�V������N@h�gퟨX���5>��2���b�@y�Fa��
q�fJ���o�]�U�����n�0:�����]�ٟ�.ٙ.XJ���~�AG�U�Z��1������!]!\�+<�T~_#���>�_���y�ǚ�|������_F���P�d���>̷����X!���)1+��1|�:�Zxe@7��Ո�����J�5�=M����Ai?i2{�����%��D4Ȋn��A��!ޓba��w	^�  C�V�f��������DZ��d�p��qM�2w���C�1��8)��Ȇ?���$��p׈R&��
�Q��q�=�����1�:��!�Q9��[��>R�������7��.�M6�_��e
����|�#֔��쉱�\(���>1�\K�4��������'�J֌o̯��(�O���A;ő�O���&��I�ט)��,>y>�!z��l�#�Hd�f��:�FPy]l�;e�͈�o���m�+<g�U��آ��c��w>��p�y.��-^�5�
���ѳ/sNi��������+�5�
ܳYf��pK r~<�>^{��T�q�_s� #[� U���sZt,H3k�_^����P��-���C�?w���g�d�/tȻ��
�;��r.b���kdVHQ�>x`[yP$Xİ-2��k�f�Ǘ�ߓ��0o�b`�x޻�%�,�AѨ����������W�oD�ETy���R�H�˥^c��+Et9;Ư�ۦhx��o���{X�W%u�_��^ü�6��4��:W������m��k�4�����o0 ����o
Ê�{%Y�F���"m��?f��ov�ء�̕��K�.���=�E���-Q~�b������a�5r����C���{&_>��xm���B%po$%~�ƃ����#}lS�
�P���S�ipmb<��x\�:h+�מ[!*�/�    1�;�і�)�}1��H�-xP�6ؽ$U�#��cNM�ט2]|��Fu�*yA��⶟��z�؏�k��`.�7
g��z����:�n�_���ڔ�1�6��WZ��L)�8k��9�>8�/j��P�}��0�S���1�ז~�� Pa~w�ӎ���T +h�^�z�z�O��b��/��8�w�)..X��V��������R���$��n������
��2�5�ګCJ)w��Os��c<��n����k:��u�=���h�Ⱔ��F}��w�}�ht�$�)��Ӂ*�=��*����^��������<n?�-)`�^��k�\vd
A#�>��_�I�2pa�_� �T�޺���|�8���dr��f�&���Q����8j2UWP#lP�9��^��`&$o>�r�)��O��������ET�f�A��`��{$������]�#�g�S��s	ZC9K�~�b�9��H"��~�mj}m�J�p⸽��`�𑰗,%*�zMޡD	�S�e�';���kv��N%Y ��P�l�38i�� �+F��tMA�De�FR�5�f*t[�Q��	�
Y7�D=�6��������ƃ<R�  ��Q����_��`�r��7eʏT�!��X}�n����碫�@���	^?{�������!]�bVݰ!r�O䡦�6%eA�~�����Y����������*H�������b�9m������{�k6�7���r]��.qN�(34���$�}��̧�W�5�I��s���� h�gV���I!>-oM�A�����x���ׄ�������IY��8m붡����@�L%q�0�WK옙�)hfʔ�����/��<�C���#�S$"�[r�hhF_�[H�f1?-��DDDb��߅]*�ƄS��*��~����=�A�����އ��{��+�!0�ΞM&�|R�ަA��^����'���?��S%��C��}�0�OʄgPFe`RL�?�T�o$�kU��
(��$G���&���Hj_N��$+�v���4�+Q�{P�;|!�}��#�E�6�asL8KY�0�G� ��/�) �+�C6�3J�$W�tu��)�� �AR	5�-�&�sq� Q;�X�� �AW,L(�A����� ZjP���'a}�.F�T�i;(��28W�w����y����a6���+3&�f���aHz��̋.��� �\h����'�0#��%��FB��6R��<^�ͮ�{�vӧA9-��~��)�՚��=;������)�����!�}�ǖ����h�'�p�9>��������!�}^l~��z�T��3���{����i���bz�b�'(��W�y�,���o���������Q'����l�D��"?i$'A�Ͼ��C��<bnkB���>�㕺m��������W��������OGu��㧨N%+��� ��6�]�v�t��}�i�D�x���o�$�}m�3��$5>��=ܛ	�+�Y�ۡ$�}
��¸~SĒaV����Xr���kP���N���K�v�q�i$ȯy��Ux���%u/��}T�$�iS&�PI��w��\v�����>�)a�����5Q��1���}������:[؛����|�*���$�O�r�ɜ��-�q�_��icO��>�fN/�bvu.����䐱wd�>�&Cs�F�#�=�-�A>-���mr�^�Y��{7)�;�,�0��+G�u7s��C��3��q�E���_�g�$�Y��2���Z��R��gt+��'0��,�`O��zUQ9��M�� =��hl��rq��������ȋ~�����*eȢ�}I��7O7x#�2�k�����  }�U)��r:X���b;rPD�}U��tQ1���j� }�
`�P�߯G@�ԛț[��0E�W�9�i!Z�_s���=���Ћ�U�^ݗ��A?�{d��r��� �{��UHp�k~o���f9J�7����KXJW�S�
�cG�lI��g#fU�I�ß�ӤT�Y��,��Dym`�,� m@�]3���8/�VP���˯��>r�Ȼ���sh�2J��#�[�5Ne��P}����LK����?Q5L���[5��k���J�#��N��(D�
:�1��7y�	I����ԨIYca�H"_S:T0(?R:U;Ք`���U 9�23��?w՟k��@.?ݒ<�7+(�	�n�}�ׇm�|�����L�&�����{�R�~�4�8�&�{����HSV�a��M�����v�5���~��(*�&�\�~L���J����	�|�k"e�N#�}q�ZY����Tc�~BopSzǟ��D!��Yǥ	�Bs�!)��L���_.��T��g��P�z�:om�!�����@�$w{�&|^��ٴ�S�=��2�ŝw�$sO®IX9YzYwܧO�������	H؞G�9�림*߮ڸ�ǿ]ѣ�R?���Y��5�SP�ǳ��(f�"_I�Yu��M!�{�N�N�R
����I��R��=4����Va'X���;zQۇ^��	�tp�C|�E���	���dR>_Rve���mE�c�o(�]�4������=@�A2�+�����黔�6��J��wuQ!���9�c����+��H�#�6w�[�=�(x� \�ې��R*#�2�)aM0�ԛ51U�7�C�ݹ��U��n�O�̲�ܟ�@������͞үiz�s��Z��ָ�5&bc�;��T�^h7��D���(9�iSM ��14x�)�7����`{TH�)!�!M ����g�B;4�p��	��������,��-�A0ͨ�g�n��s�dϻ�gU;�Z2*su�9�����:�Z�󛻘�I�^+f�̼>�t��f)�>��I����!:)R+����T��`��{%4	��K�}��^��>*�S^���8=]��^	�D��.i�O�i���k<I'���vJ���I�Cŝ~$�y��(Ř�.��-���rr�+B��[�G��-�w����5*R�[�*Aσ��bRw���i�{ua�N��s�s�&�y� Y�~�N���_�cʯh�`N�r�I�n���/�*��G��!)3�Y��	A̓u&yͮ-	G�����I��������*];���;��Ua �2�ZͲ��zP�]�\��_�0���(�j������jkԹ������L(Z��\��I���< �y"U�L�Vg�٨O�0#��0�<��bB�3X��!��?���)���}��ߧ����L-ښ;$!�x�-�;�U}u�Ý���M�k�p*�oZ���M~O�7��w){O�]ʶ��C����Ŀ�ǰt���Xwк̧6�ך2�R�	�|��Wk�6} /FJ�F�"!N�&�i��W�El�"F��A�+*��^�P{U�z��w	8>��yd%x�}�^���iݻ���a�P<-v��N�x;phsB	ІQ$�o����rB���d;�e�o10�Q�
i^g9i������U��.��ݰ�p�������R[�ʮq���w4W�~4(Z%���#��Se�I�x⽑���}���}
���s�+�n�;���8���{7.������Diug��m�0dI�]�6hWDQ��['pw����QO��xc�AT�f��Ú`��/��+�C�Z��ǂ�4����L]�-�Qv�\ZZЊ��ޑ�!��_��y�h;�j���㜴ylO0u9�c���׬;(���l���m��Q��s�T4ɿ�m9���GD�1)�3'ްp�K��|S��\�&N�C�1�������e�[�S�|v�>�1i�[9����z�ew��J>1���~?V���W�D��7�B	�`��%�Ò�c��4�'�ۛ!x�	�g*���P&@d�.u�g]V�>$�m�����n}&�A#�A���� �ɷ��϶�ww`?�o����! ���N\�3�t�����=F�u.����9ү5j�O����t�?�����;�ۼ�]$]0�}N��Z���Y���t�OZ���We�L�/���-z�eg�$_Cbt��e��&�w�ť	�>��`    ��>����7��;�f:]���<g�PIM�*��~�쾨q��6O��=�i>o���`��PG�հ��w%�����|Gm��p{ SŚ�gq�),��ؒ7�><H����.�ڟ��u�A�����	�_|�9yZ`$�R�otͬh��8�wlM0��u��Idp��'�-3��=�|�&gt铀 t�Gmp�d߻�	t��,�F�����YT���g�P��E�
�~�L���mK`��=	�,\�޴VO*7��5�v՟���xs�>�˘i��A���*`VsE7P���o�:Ps���%a{Vk d3���w���|��wk��� �S�
4Ǿ�g��(�XC�{@J��$J�{ 1){�~�(1���U�6W+�G���Y�����BA�0]�h�5Ҡ$q//�`R>���q�C2'�bk�Mh��yO٩�;�8��t���ϡ�*�o@P����|*<�Ə@��+}��p;�$o�FZ� C�6��_�����wv��F��8���xH�
��֖�Ӑl�0��I��V�F�%��2G���uŬU�a�s<��隬����#�Ur�7N��Խ'c}�b��7�ӽ����� �����v������Ɯ���ec1�ަ����	�k�|��~�&�m�jB�O!��!�}�k\�~(Ҕ�ް+��҇���v�[~���vvP�yM�K2ץ�|�n[mv�� `Q�����<9�����;^c�w;�\d��(y�e�nމ���%�V�m���'"���'���j���2$h���أ�$��,�겠�a���SoZ�ս/��t8B��e�|H��يh�¿!!{u�CܵO?�	P�1�g�њ��̐��N�meq.9i��h�ꆮL�CP��.�_O�]�����~���zS(64;n�L�y7u [Ti�K�=jH��1$�ΌI����S���rνc׉��7xA�ģ��N��i|��Bb��C�?�\���c�h�c�9�����
�3�:�9v	[���`G��1n�*GO�����w은���	���{7��Yo��HcM���������Fh�u�SO�+3L��t�`���'Q��O���$�33��o�����Us�M��K��ܥ��)Y�-�G��Z�W��Ŕ�įަG�D>sO	�'�D1�/Ngܵ��=�^O�N�M�yM�9�If���3�\�8��^�Os@ԫl��q��b�8�bQ?Sx���Y���U���;~��V?���o��|���o<4����'h��6�-� �1㶘��S#w�l�6�U(��so�!P}��Yl$m��%ۻ�X���Q)���(aj�j����hRj;�����F�w����hE³~f%Cǂw���OA������1�O���9��y������QK-����(���,?��,)�'gh����9�X8�	R��r�h�����|�\��-�-�����j��W��MuMFyD;��o�.�m:���=�w0��b�C��i:p�m�ͭ�*�UJ�,ιkS���wvHʟ*�a�%A#Tu�?{w����X�i�����q;�$��)�SV<s�McmSLዚۙ#�����)�>��G��6�Wذ���k�_�6�5��Yr���O�G<r�C�#q�A$ � �Ӿ5�Y���heɗ��4!6D�ȭk��>�0Mt��Z���.܉��ۃ�KW��&����S�'�Ųt��q{	100�Q	�ݓ'���3B��u*�m��	��
~�ӆ��-ʌ�$>C��RI��s�z�:%<���Ӽ�Hl�|fK�� Í��\��B�Gn`��<hR��N�.*$��Ӑ.BW@�;�B2�N������'� �Y+z�,��
]4�W۷�ۛ�Hpn�b?��B�V7!��g6�+Կ�/��X���m�H���AP�޹��s鯉���T~|�!����ܠ�7[?�@�)�S�,�l�!���}�$�}���&���ϲ������t��	&}Ҧ�֖�����@��a]?k���!}܂���AH��<�g9�k?$��Pq�~{H�* ��/}�2����=Z�[��7�9�+D�	m��]�O�x�}����&tB��p�-
�r�b�eӧ�wԨ@wn��!`��e�^�>K0~���xrqcM�F���c�x^�:�ۦ�Օu`�#LB ����}����f���OX��S]�!.�I��j[���n�[����(�L�!�!<[ݽ���k:]]tH	�9hC"ü8�i5�mhɅg7�ǻg$��K`�g)����@A�MuBy�=�DrM��@ is�Z[T'�P����!)�ɫ}���MJ\�@܍��Ӕ��-��x���z&��	<�,���%����zZ����Ͱ�O߉��
�?�3B"���*@$�����v&��(�b�}���������5�Eւ�2�V�`�]?� ��e�����t�ق�	� 	��N�������ƨ�^(�[�z��źO�疣����'ߪ��C��Z�����w%������ǝ�fݿ]�o1�oR�|��賻^���#��G��כwQs_"h�4K�49��EC�]�,�=�$/���k��D;)5���h:�cv�Υ33P�y��WB���ѿ��ѩ��4+��53��&�ц��m���`d[���H��|8�r8�eى�� ��kg�:fsW�_�䇗��٧	��x�E�ep�e�.o��	<�-w��Q1�n�/�
�ybȾ�'-�3��;�G��Yֆ�J@&�&�wrF��[�{�M��۬�s34@�%���[*�`Z��?��7	T�Qʉ�\/<R=��q�=���|�R�^�)3���B�I�"ق�~�Ȣ]�8���� �y��J��OwN�$J�����!�w��N���S�H��I��iA��i<��ƍ��~�z���0Cz֠�~�v����8}��]�J����)�NI�`���î��!�ٴ~�0a@�u�|�,To���K$*l��c�.��ă�L�x�}c,�4]^�h�j� �p����ux��S�����Z�	ѡ��+�3��?��{~��@���f�Rw���{����S��;,��3����1c����y��ƾ���v5%;g׾
�ld�aQ�\?�������e��մ�Q��$P��m�����G��۸�8�Y<����l,�!�� ����������ɟ4��"ICo��
�h���_�"�'j�{�,��3�Y�醐�z HBxԘ�0K��6�/0���z��c�
7�c��,I4�C�$F��?���꒗��|�{A�5Z� ^������Ж �,�P�l��aG_�S�-��6��U�q[�w15���ZŎ�_��w�(��/�_���"v�w�U{*�d�y�2�O?������i�?᷸���h�:ƽ*�6ޮ+S��#K��5�
����7�����iE�����TK���8�r�?� l�zV7�v�	���Ƭ`ƭI�%�Q�B9���П��IItl��-���y ���������}5Օ����5��^��;�@� ����8m���^�܋	y~��@�7��@B��^��V�#j�[��FqK��z���]�|��~��ט�Z�e1K0��j8������/��m�m���l�kF�����g9�h����Mj&Ͱܣ!�58�����ݧ]y'��&C��<(p���U`��+(�� Bޮ@�R��;Y?���4�$�.KUA�Q�?��������$�W~�� 	4u�Ƅ��!F�N�o�B�l_� ����XYhR�'���W�������6�L0�qNh|�F��.K��ɏ
�c(n@p?c�D��܋H-��������:u�&���p���A�=�(�5v�I��hj��T�v�j��(?����x2�:�Z�TZ�Z�M�qA��a�
��7Eo�M���r\�Rx����',&�I��A�{3�%�}�PQܖ���w<`���N#�4�O�9N���8���yW'�;�g���A�}�Չ�hc�    .�)X2%{ɂ%�|���� ���]�����)a��7�^݇�X�	�%������~�H���a<Jp��>�8�S������%0{P+�C7��Q�u��b ;ó��@vСe=-��3��~�w����eiɶ�b9��3�m��o7?^�1A�So5�=C��E�w�o�lх��U"�z���T�Z�$�������On0�JC��i#�zP��[Ł(�ߜq�[[?�7�@=��A�b[�aܻstQ7(��� =�a/�J���o�${���m^pd �y'Lq�M��9��Mnr���y:�w�� q<����K����%8y�;�no�@�ΧTZ��y������bǷ�}y�&S/f5On��%!yV��CS�^�ÏgoRB��(���b�$*��N�,l��@ܾ�t鄉�(�5�	A��Fe�����{�����ܤB��ΰ�[�ge�l����I��m�:r����!�!4�}�]aZnǘ�h�]�Y�@��� �C9ޠ�i8����D�pEZ�ﶶOz뒿š"���g��)Sx�T�7׼���&\]�;�K�O��Wh�"�x0�G�}�\����-��>��߅;
#�xmѭYl����M:c�!ؗY��u�p�BU��78��w�`�Γo�pS�i��wAOEf<hE��`D�u(�t�� �<�Jbt��@��8L}5�g���AC��ڰ�C&�U^�{lQ��Z��+��nܜ%�y��� �8?:�?�]��~�>��.��a71�K
�R�k�^���~����_ ����5>��6���5�Ӧ��v�?���(��z�z��$XϘ���XT�<�'X���`� �1����Pp���(�	�\�~ī]��a�"�6� �]6RZ3ʖ�@GH�^c�Y.��5<��3b��n9��k�M�w�6n1�^'��4�-��3��d�!���d�ͬ~�훽�A������Y-��'�;�$z�X/�qr�P����c����o�OI�k������]1����Ra j��2'ν��@=(��d�?���SPi�����sG�G�?���BH	P�z�[��mu�ʐ�{/.���)*�ޖ�l�<	Tu���� z]ޡ�����2�QɊaow&�x>Z,��aV��h?z���t)�FJq����=�� <��=4��?񦣑L�Q4춅
��U�4}��X���G���OT
g^M�r���:���kQ*u}�qiA|:ԟG��E�!y5ni�^�"��><M]�I��{��g�����ܓ�;��*y�j��/���]�+��c�M��F̾
��L���.�x�*��F�7Ӱ���L�N�M�)W_}��>l�غ8���27�{�-��S�X�����>^�ͺ����B�|)�st�@�u�Q#���BO"���4���C���h�*Ԛ؈���}�� ���ڠ���fzR���SOK�L�%`��A�����v�������t"��/��N����9�N�� R��%��Q�d��
�IZ�v� '\�Tq��ӫ�?�z�=�-	��-���F��;p�N��,[��yВ����
7ua�a;���B�ᶰ?Od������շ�&h�W���spZm�\�}pN���x��Ǧ�&�*w�w�k������,,d������{W]]hn�D����d�[v��s�c/�o��x���{��^�CR�.�l%B�!�����Dؖ�ݡ*)��h�#��p]��6��~g�o�ٛ�[�~�޼��=������C��6EzŰ����5�.xa�Җ�,yi��؛s[t�޺b�N��t�����}ŝci	����-qd��P��-��-�}x]B5���f6�MOm	�����^
���F�ҟ�'�ML��-?��>&�͛��?�O�(�MS��{v�%�w��ȿ����Unxw���A�9n,b�`�/I�����c���28�gw�5�T�])�P�ˢA/BT�����,�{�Z��d�&��]�{��Иy�^�bƟf��������i\�c�Qk�4�`���4<�22��۴��8^��6Ҵ%L�Q��P���Z�V����B��Rp��[��
L5�m��@���5h� �]q_�v=U\����Ѭ�t��w����I���fo:�5~�I��93�pk@���I|�A�c|����t%݅���<���f(H���ioV`3�)�pѬ�h	Z���!7�~����7�5P���R�nӕa��55�?ڊ�R�a��b�D�he��;�A���	Pו7�pu�srb$����;eN ��7(�f�V�:,����{-���-��[�>�ב��q��K��* <�\v�u��>i���A#n�H��w��:Q�{���r��`4'E�`�&��W�R����+B ��RxS��(�=H��t����Kv�[K0��_�Z ��*����V�kR
9���D��=�]��g1o6�,P;FE��I�%��� ��x�q�`ܧ��۫L�ݍܒ�=<fl�L�20g���hu)�����Ɲ{!y{�à�rc!���ݱڌ{NF��l�������} 	Lp�z�SvLz`�����!7�K���"�8V^�c,��GǪfw�	����e�h�����suoq5�W'�� ��ݵג>Y�ﷵ����3�Mg,��C}�;����zk�w;��
.z'\
n;B��[��h��� FuN��1��U�5�i"@{��+��8��INK�M��N�����B�:e��X��'������"�C����ޒ�=[�w�桔�~V},(K9+(�<rD/H}����s�����67#�d�>������͗x3_#$�M������]��Y�����O��oI�=wؾ&�e�*�!�π�
d�=_�.��0�_�>פ<X[w�)���f�Oݒ����� n��4�S�u��f��$j�h<-�Z�T���C�"��eg�$䮛$q(:T����-_�܁.	��O�5�Գ�!�c`M"~[���:� ݺ�D4���t��i0J���,J�I�g�*�*)�o���I��+4�t=�>�t��zB��U����-VI�b�Q�gK�����t8��qB�Ҟ�7�P=�WE��6�I�:ޝ?o"�E��(�]_��WǾ	�2�ئe R�6�a�Z���q/�c�;��<Fe>�&iS����+���k���І�C��09��CQ���qǤNIGi�]��MȦ�x:iq�Kk�E��DC-��s_|��;<�Sw��hҩ���	�n��1v KS>j�A��Y|��FäRT��yeد�4z��ܳ��č��g�̞in�O��[N�>�DQ�ؗ�g���+�� �(�o�Q���+�Z*������4P>^�����7���%`{f�5n�����(_�itr��,�\�
��*_�\{����R�np�3�ε�w^S�Bf H}H�y��IjU��YK���gա�*�����V�I�{�������ɀ���nZLu����yOZʼi�GϦ������n���?a���-�Oܴ����L�FrGj�H<:�"�LP�5Q�2�^yԾf�=�(o%F�ͧ(��j��
A��7��T�&�O�ұ�4*[<�*D��DS^�!����+�-~�d�q>�gU�R�Q����&>�m�?��Rh؞.�6O��8տ��Z�����݃8,vRA�U ���m���ґi؋.j/���.Hݭ3۾���g9o\u�&������ ����7�X�po摊w� !;����E&�(���2�ű����e�� �ڡz�J
M����G������b�3���}��D�f�������hꊂ��g�H�B�#�;qgiH�jp�6s�2��S*��yN�6��������pz�sf�>��4(�P_?t*q����`����#�6�% ��>6���&y�iW؀֑�>�B@0��$}L�@%0�_���7Pw�>�~Q��"�.���3���z�H�O[�?5�&'�WQ�r���I�#`}�ա��ׁd�����     �&}�.�x�]�A�ḡ��:קެ��`KX��qn�a⤪�n�3����RQ���W`��7��r�Q]v���P'v�N�3�4e��*��3�u���D��!���N�����z-�dy��ǝ#eA��z���ю@�Aky��P�=p9�}�6iO�*V��	:�>8�)�Zd*����OPx�G��=�k�_��[�⹇V�$�xM_*؆;��aρv$��bPu=�1J��զ��P�,��h��Ê�M)L���k-:��M��Fj��D���-�m(���$�{@��:�8�)�2ݓ�x|̰o���l��W]id4A�/W]�8@ԧ��(�T@Gp��YEkw��vq�ce���O�<��\�?��� :76:�v�)���Jp�=:�g�����@���y32~�?+
T�������7���n���ՙ������s�s:��XreP='�sp�ף�A��|A'�rzG��`8�:M�zh&��a�2�}�P�Z�b��H�_c�%ld�w~�����D��j�U2�;�:���u�W���Y�E ��*��0�pҒxU�*j��]��7���?n�8��I`vC��r4�.v�` ��A�܁8��vm�_�.rm�zmN��שQ	*T�?��E��E^�����"�s��ମg~4��!1 �.� `R#m��5��5�����-�^�R�IΨ�5�a��$?�x�:o�+��I� ��4��O�x�D%sp&�%e�:�:T`�����v��'g;��yqPKs͎�	�G���I�#�~�_��m�:��B����/IhHc�(�_��e�/�O��W����_(R���!p~z�YXu��!*R����	E��}���;���V*��Q�
���;����uA���{cWG�� u�$�J� ����c��g
�4{_w��u��(�5��=����8N&i��*��X��7a�/��Hh_�ٛ7ht(��e�c�r���.P����`;Ҩ�.�R�ƶ����t|�:��OG� ��OW��'^�	
�� �U�t����5�/��Aܯ0�Ɣt�����kLd��@�{c����Y��G����i^�rFCs�+����;4I*0����'�KcD\�_� 8~@R�>KS���@zx����%��׿#�}0���G�>%4~}u�h'������ָ ����kDn�չ�h�:]\X0��ڔ=���<�i}��R��\K4O5"w�ʧݤRQ�d������1xM�k��J������"m�1��ǭ(�iՕ��G)�Ĵ�̎Ŏ~��C����ZW|x�&���S�/ Ww��CgO�kԨ@`b�����W��b	����:^�jq"C�`	��{�BB����w�����7�e��Br�g�"9�;�*�#���A4ž�{M���­�h���F��?E=J,{�6A����-����U��K4~���FP �7)��la~�z�p�L�)3���I Ty/b`��L�W�ۜ���4u���?i �B-��r2�.{�س�$�OYl^�F�ԯ4Ma���;5�#!���ζ�w?h3|']������S��VaJ�)�i(��*�m��_zr{	u2��}D@U�j�=�z��9��9�w��PY"y�����dD��}N��}�Q����p6��Y�-�{��ּ��������>i�����ID�P���~�s0 �e��('�ys	�
8+K�sN��(S� IĎe� ^��Ɵ����\�x¬cg�v$��6
��������w:[��-�'!�;�J�.2��i�]}��	���'�����?�����~�W���Ʀɫ!�����m~-�-v�/i��A5e�S�~�g�û,�wWwq��4EG�h��렂ܘⶄ��wp�ٌ�L��L�#5����h؟����Z=���:��We;D�K�T��#�L4��=w�,�D�a'�J�+��S}����#�vY�೰���	�,��ߏ��be�����"Ҏ �����0zw_��n��{��!��Ő�?�p��x_�?�s�ng�HU�7�f��__��5�a��%Ǿڎ�1�1�:��~���&�R����;����MJ<#�6�ӯ�cSc-�U�r>�:��M6h�L���� v$�_0�Y

���+ܝ�}Gv��'�G�����"�&����^��f?]�^���o�C%�/�I
�F��>=��Sou�I��"����5*��]
��O�#i�A@��=�v����l�A}?P)�Fv$�O��%�?�k2� �^${٘���(/�$�'��{��b8��.���/
�������5��"yc����
�E�F����&�}�͖����+5��Jq� x%Ȟ����D�0���S�%���sJ�'��&߫Fe�1��J}�)I�gƑ7F�zP��m1ÿ +~��v���;�҃�q(������P�}�FĠK	}�`�H�C���(��t܁��I��u�
�!�u�\c3{����Y�2Yǯ M����AU�I��.�n��ׅ2���.�%�.��>Y`�qy���?S��Xң�GF|�G���0��*�!楇��[	��M��f��ބJ��y�CnF�Y��$��˺d�JQ��89+�n��׆�իQ����	�����Fm�NE����k�� �Kb�p��g�m9p��zm�-=z|�ݠ�k�o��C�w�������=�;PM��~��zGۙv怱?i�~���R����'��'�ʽ��u���fp�r�Ӌ0Tn���~�_k]��5���3����:M�;���p�+����tx�`R't�F}p����!i<mnX`� 2=I����0��k�}�I4�wǲ�f�h��?\�:�ʧ�];��$�گK�O'%�>��3տ���<����<�3Χ��᭓>��Cqz�t���]�H���J��u��?����ϳ�����P}+l$\ �-iq�w��O��q�h�{��P�r+�x�dTg���2���lhAR�
hبf�5�����$q�
/�����v��ۤ���6~�F�����A��Oz��:�Z�C�����B/~ $₅���S�����]]���w�kBN�U��<V��pU_��Hja��w5��"g�;��C�w"%%�e���1�k@���/�L�ۆ�(��)�LQa��K�,k`�$�+����D��{ٹ�����~9��^�x�\��$�O���C5C���{1 ypG�lN��mbO*}�a?�B�ʻ@�J�=n�>�:���؅|�(���lF�ٺPio���S�����>N�o�˹�
z�B�f�tT�^����A�t�oe����/%h��AV���z���PqRO�$5�1�`�E;.�B�F�� ����)�Y�j�1���E��2��#��k!�8׉��Г�1�@�E��'�nJ�y�^Wݼ�}	�3����mH���|��[Y��>���h��S�[U�O=�$ �)�}���:��+����~Jc�I���}��ؓ�>�1S,܏AǸB�,�1-=�
Q�V��U�f�.��T%�b?�����;d)I��}E�ex{���u��?�04���[�� �G�o+>�E_x�#�+h��U�Z/Z�V�@f=��F>��3A�3�:C=�z�Tq�[,���~��p?�m��s���'P/¶�nnwV�y�æ��,���q<�'�1J�fV� ��8��u���˒f�އ˼uj�37)(��{��!�V�������Ϯq���'����P���!��(ڍӮ�����n/	h�	��ҋ�+�z��v�-��Ťi��PA9��cs-*/�X�.���9�U&�N��S�6��)��6Q�����pZ\*������N-=!d#M}Vt=�����PKsWi�Oo���]M����?�o��ǭ5���V�>���\w�59��˯ޮ��S����?�<�������yU*�"�e(�M5;�'�U �RO}̂��o��V��j�{�q�Q�98H�RvS��=uU��h
,zRep)�2ޭr)�&���p>� �   3]�Υ�t�{���d�=�g�������UD�ǟ"[�$����>� ���Z�Q�9RW� �'�nw�_h��N;Ѫ��\�8<�t�Vz��=�/�{�|�a7�?�1�t���`��j43D3���Wd�=�m��*���E3R�zٺ4���|��ms�f�'���IX�<��)>p_�g�i�?{_Mb�:��F��ə.�&����= �$_��gW����^��_�{�
��yyy���[      �      x���[��<od���C�������?P��|��>v���K%�(r1��������?��'�Oz�g����R?�I�����Ϳ���c�g4��������O��_�.������>5s��/�5/b�O�S�uo�?���{ۗHOY�U�k}���pݜ��¸����M/j����������-�R�����㸢��/=��O�)�Ja�����6�~�u?��Q?Q$Wi��#��i���_ru�o�六8槊��B[���ʣ�����F)�9n����H�8�,r���[�I#�ڨ�����1?a$W���~M�� �H�4�e�h8�2~�/��(}?�R�<~�����]ml���_��p�M�6�3o[�m���G�'���������ٕ�|>u�u�W�O�U��ݳ}&K�������6�\��� ~�/����E��?G�ԑ}u�����z��Oٟ8��U���"
�ɣ��܏q�͘,��_�+�8�k���������C��=j�z�ž(�:�ї=��ԯ��@�+�-���K.׏�	��I[�c����H��h��C֞:g�ɠ����"�Yo�?����F��:����k�ʯZ����|:��V#^}��gU��^#P^�Y��A��M��~^�Yd?�6��z��"����a{6`���XWᑶ:���\Q��S�~cZ���[Q
��B��)�6ʵЩ�q��@d���L�U�s�W{��g�><��F���+����]��r�?2I.)�d�~��\m��O�G�~C܂��^�<���>K���"�f����*C�FEf�+��67_��=�r�o�[�,�AS���~�\YY��2b��#�1n�2�O���v�`��(�����������$��a��� w�˲Wi]f�r��o����%,��n{W%�b�����O�UFٓ|ʢ�t}��O��.�����Fw��>[L�E����_c
?At�CRd}s-��O��D����O��6?U�;��Zu�Es��~��\�T���a?QW]֕�_����w)Ef���;�w)���^���~j�2Cv�{��O�U����זc�d1��b���,���ϟ2���޹�o���c����5,��d1]Y��Z�֢\���S��'�-N��W�5��	c��RH����//ޯA������G�\H!\e�UE�{�M�'��
��{���m̟0�+���ʎ,�:a��1]a�º��k�t1�I���������4���Х�̸����'��g���만�/�g�����b�����~���d맋���5>׋�~�X>ؐ�V&���)�ߐ=������%Y?e�h�*STZ4Ѥ秌��^��[�S�8꧌�*C�&Y�2<i��޿lL�� �3����KV��됮�fX��w�Iq(/��!G��ߨ�>K
3����X{	���+p��^���ߕ�P����H���z8�ѕ�l��Vn|��ɞXǐ�����屯��{�x�H@�-�)�}$����E��L�*�@�2��nu˺����g�{�����6m�<���c?��X� �>�K�;�b��2p����L�d/f�0�%�x����f�_������z�>ò����0L+�_���K�����RYT����ߓ���R��|��g����2^�ݮ�b����{�~2@�|\�5)+���> L>1�m\��',D�Դ���&����L��KS�L���ٮ���`����M;R1��̴1��cT1��T6��_U���g��{��v/_3�1E����z���5&��O����YUL>4�G\�kU~��SDM��5��b^���C(����XŰ�*�+�&GP�
�ax�����ٸ��4ɦ�s=|#���S6����6D�|^����h�i�0��T歱g�~}�������y*1L�|l�犽���x'�ѾF�g��o�w�Ǧ{�R�$Y��W�h��=%���T�0l���m�ޗ�l�lZ�MޏÆ�禲���|�9$C���s���R����S9k���%-L>9M�[��@2^�x��BA��#bM��~^?�� �v�r�:��L>@]�o����TA�����kf 0�U�YE~�d0��TVZ��_�%L>C��'���L>A�����d0��T>i�k-h0� U��Ē�/ـ���~G��f#���-�܇�8
�H��5�~��]��f���r|��W3��,Uλ��''������T�O�U5��d00Hu(������UeFݻ�t=�ɇ��%�5�pL>T}�Q<�LR�[|Oo�	ˈ��I{��]JF��UE�{NH�4bX0�hu�2�'^�h�ɪ��}���q��Uq!�w&Â��{����8��s�%/�a'c���[�{.��ݙ6|��������L>V�O`��^�k#�)��r�T����a(�ɪ�G�r=����g��|�����ʦWvx|��� ������(��L#>d�EY��o��
���M&e���a\��	}ܚdٔ��gj�`����L,��Ug�-�t��^|�*s�z���X|����2�d��Vټ*�?g�|�G}�*���ř�'��A���p���|�*���6�e�H�Z� )&�u]�a>km_�L�x��4��U�(���
3O�O[��</�I��4��U>5Bo:_�@m���u������>o-2u�?~��i��[e�^d��9��ۧ�}�	�$֤A�Q�M�y��3�9��{���2�57� �}�*��*sB�~��[� ����~]φܧ�Y��g]A��I� ˾�R���pg�_ 6���$��&{�g���$��'�&JN�(΄��}�Mf":iχQ����%p�z*����`��溜	�'��
��X��L'>iIqEy�10���Uf���3Ja&��
�k�f���>l��ﺨ�	̐p����/x��0�	g����gy���V|�*�J���iP8��u�k�}>��T|�*m�tݥI%���7�n��ănT8�U�ٟ�#nL8�Ud'�VE�4���U ��M~����\�Ʉ�W3&��*K���q&����gy�%��>2���%G�����4��U?v�B����C�%��e:�uoB��<
2���u���}�0��[��?s�!������R�0C���{�eT���7���p¡k
3��uOA�s-�@���*�����~΁Ou|�.<�4��\5O�߇��3׭����%b�:��Uְ#����>r]Cw8������f��R����VI*���wi�:��U���z\a���*0�j&�0S��]��5٣����>w�=�V���(Y,�ɍ,�������Vś3ԟCa&��E�U�1���U�����P����;d/h���s`b��}�K��s`a������6�������o�;��u����װw��k��B�둘B/�,y�<6� |g��*���<���>w���pf"�ѫ`���|}�|g���Jy�|ȑa��>w�C����޸w�����H������W�q�+.�	�}�*�&R��#g�;��US���!�"M+>��c#/�̈́�~� �>��\l��w �U�.#�ه�2�H2b�����>����3��w���>Ō5�M����Xٳ������ ��{���7�x���"��<�F���_ez}������Ǹס��L >vkG�,~m��g�����?�j��9 �B��eȰ�f���5��d��P��a�͡�u�<�>�[|�*��اy��l���|�����n�c��_����sl�� ��~��s����W9[�����篲����Y���_%7\�T���\K����&�y=�l��k�C]�&i�O���U>��\|5ï�ǯr�(��3�n�.k'���"[|��\f˪UV�����WY$�^���%4������H6D鋢p����ʶ�]$X��������(q\�}ӆ,��#Wq��/��S��[ee,9d�^ȢL�>n�Ӕ���a1�܀��6��~1���Y�-t� <ʷ���1�F�ׁ��%�E����$.2~���V˻�����J�ΰ<!�29`��1}�+�|�    ��;�u�29D9���R"=�A	x�O^��/�N��R���['ĂL>H��O;ץP!�7�}Tg��!@�C��UL>>�#���BL><}���cL
>6�mb�^
;(AZ�[�%5z)���T���*F������5�E��r(���-�d�cRI��_�f1&������+~T������o����#R5����ǣY��׭�|6*��K�%�sQ���1	L��5UQ��D5s��7ܨ~�fO��(ӁOD��Izn��OCJŐL�3�_�iѯ�
��d2�bB����I����/]����bdbh��܆�K@?�-*?;SC`9U���~���=<��%Hٗ�9�؆�O<���cZ����C�%��o�n�i�/>��RW�W�ʂPf&J��Z�wψ}	H�睃�S�O9%��9&��qN!4���0}���~��ݚ!���G�]ix�ϗ l.��"Y�/>�l:���1�@S֦}�<�e:���w\�SA��߅@�kjH�� s|4}�BPcȟ��]���`4�y�I�����p|	f֕vJeb����ʟ���3���Ka�be���.���2�a$���|yK�8|����3����% ��������\�c�k�SE`�
���>�A�)���/G{/�,������� ���k�(S�O+�Zb�]�Q�$�qN];#�%���z}-���Sʔ��i�@{�!e�<�E#k��pr(����0{��dV�u��U����,��d��	�SsC�Ҍ�� ?�!.��jx�FXR��9�ʏ�Ю�K��`F�k�ݻʯ����
d>���I�~��,�
��<R]���V��WG&-p��%4�^������Z�1Q 2���遦W�C΢��[�ĢP��_AdI�?G���(|�4�!S�i"�E��+�JA&	B�ogo�6��Lb�Z�Dp����RLdj��_���ד�f��O����Ya�c��琒����e!6�QR��U�
�ߌ�_��?Șb�I��y�|w6��/����j�W}9���kP}T�XaZ仮&�L�A}�4ZW[U�5 �
C]�Xb���d\����X9��maPq���Qvq���MT�=��_ #�է��؃zݝ� �o&��z�� �Q�A��?�wI� �%�O �c���)!��J�+ݝ����G5��;g0���1k�<� PU��rj�"����e��*�/���bH2/!QK��Du:�{3M�y2� F� En�u'V�@�|-�v�#j��uP��� �� ����FT�AN�EhRD)Z@�ύ����d�lR5����Q��~�)Yj��!�}Ř|��?�(H!&=
@�t���>xlM=u��(�@P/��T��C�ڀ<�V�Q���w�2��#V�<�a��5mX�� ������f��G�P��F�>z����E5�X}�(�R�[��g����7��wH �>�\��:�đ<X}����{�#q��Rl��*iY���!���$$���T�!�NZ�bl�|)DR�(��T��$)��l����7����!�����8��>� ��������t��[��F�0RNA�{�W���Q�Ъ��Bl\}��BR��܇��>H��NÊ�!�|<]�5�!�w&O�p���>��:����ѡ�x�rL��8�k�̓�&�d���Gu�b|G*`���e\�'X���!��cZ��ܜ��(���HI�v���B���7�>�j���f�k�L�MR^�g�)�����_%zX���1i���1&� 7�-Y�=	[�7�@.5�O��z�dOg��EYy@��f�"�Dd���ȩ$���Zo>~�ۓ������� R&�vM(�`oq�����-כ�KU���r���1���-�[���h���8
m�V��HS�-�[���Ɋ�cL>z��������|�ږ�	_���sGA�R4�'V[����g�� ��ҽ��Q��J��Bez�٣���,�jl��|���?i��e�,�U��d�������w��3\���{-*�)Zh4��ʽ�<�[���29�Q�*�BA_� ;jU�E%����'��#[���Qs�&��z-�y��L|-���cI�����
��8�t���!hx$;����(t���&d��f�h>�,�I��� �G�j;�,�G�^�!d�oŢ5�Z� �<ǘ&B���ѣ33@Rǵ���h���;Q8ƴ�HY��rz�2�� -(ҩ���i!`�Ia�#�.H5�R�L -��:����< ͇�r�#����2@
sV��v���Ђ����Zg��2@H䒉u����/dт���fh>��<�'k��3��H��YI`�2I�4�+�$���42iͳF�0@�õ�e��|)��c��� $�vC�6���H2����~��%�x�(���#I�Vj>%��	��Hr�	���12@Z���ί�� Z���W�fh�T���Ђ��~h\� Т�VD!hi�ݔa�Ϙ ��$w���T�i˙�| )ITZ�/e�]j ��_Jꄬ��1��3���M�#��<f��#�+�5�:ߦ�XPxS������l�|<Y�"K	~�r�
��'Y>��J1�D��oŰ#ʰa��6=����e����_=||˙2����{���0���	��D�WZɢ�M�1�'y����!��JA��R5-ʴ��o׽;ʤz&��"�A���JI�=�"��j�eriQ�ccXμ���J��N�t{�[��-�_�N24��|d���4*����d{K�ۧ�z7X�I#����(����6�\o=��;4e���hg!&��[.��W*�$���.�V��5Ӳ(S�/���^��eH���RJ��Sg=TlA'��=��(F�/%agO'�%�:(�r5�8,���f����5�݇��tm��Be������ٱ�+gnQl���Qֿ���,��h��� SV��!���F=@�R���W2��}���<i�1����ZY�{�Ĩ~��e��af~�b.a��ݧ��e�Ϗ!���=�J=��ڍ1v�gJ{�o��3�D�Ҕ��G���3vhJG��c�A�R�b���d��Gf��ք��8cR�%�P��2�؃~E��\�8v�jj�����hQ�� �;�-�h9��z@7{~��6�����RW��_G2�2}��SN��A�ʙ|�}���k��;��t�bbc���o��7����x���7`�SM���Aܠ1�|���9������H$�n`�\���A���\)Mr���0Z��pS*J�1~�7��C����{�^�>ߔ��U]-�T�N���G�.|Ʃ�����c�1����r�x~�>�Ď~x'ʙ��}�)�N�N;˙�ԃd��y���A��5�vl�0SFн]ܓ�����'�R�u+��o2��}ҙ^�IJ2��}�9��J���c0�nym��0��*<U9s���9���ʋ�	�ǜ�~w�|�&���KI ��dP�ԛ�Yh�4�}��_�W���(d��?R]L��f�͇���řA��ϩ�	4EQ������Aj.�Ә4X���<�!/yu�ӊΨb�_�4�8�+gbQ�_�7����O=���c����٫K����^ԃLp�
�� 7
�wߧ����V�(S��?��/����[4q��]w)��L1�>�LZ?�f4���A���v0L|%��O=%Q�s=u�E@<��#�/c��q������L>��r`�ڞ")�G��59>Q�I"0c��s��/ʙ��}ܩF�tʙ�ԃC�;O�C�DP�Rrl��qF�(|�9�
6�]&���e~����e���#A�ޕ�I#�/$�q{V���Ѵ�K7:��o����3Om�8u�yF�<��[�C�5v�}��Y$+uwy�_�t4�����\��μ��w狙0|�)S�d��	#�*$��%*�<�s���^Τ�d�K?�r� m����u~�:�K= ��J&O�2]��s��8K˕3w���S��7KD�Rh��|W��ic����o}d�ç�jj���5,*؎$��_h��<���Η�    ZA��Y��b[����yL�g�I�E�[b�r��S2i�U\�L�����%b$HfA	ˬ�ȉ�U-�_^h�.��Z� u~;y&�?S��:u��g]g�r��S)}�;rXF�5^d�:��C�Q�7����#�eu+����^�ė�N��E�b�0]tꬔ��/#pn�DEa|�zT}{�̗3N���a����++�RG���N=_����}��`�Bz�8�>��2(I;��Q��nN>����/Ʒ�k=$zt�tA�ʩK��I��q�s�j�W�19��S�6���(��8u1ҴK�e����rf>�q�=�����q��i"0o�{���R�#of)�E�?;Ř&��q�+�*��S��)ǌ88�{���|9&� �<ijI�sR�H>��<Q*ǌ�[O�f,�/e�	��'�z5�4�N����?�r嘔|�)˗��&,�4�#NYS(ޛe*�)�~�6�8�07u-�{���L��j�~<�1;��S��ŭ��i`v
��?2�H�Q�!�� ���o��b1�PA���[�UV9&(�vZ�)�a&� vjy���S��,Z��˄t��/;���ǜci��Rh�1A�.ߺ7)����N�*�QZ�LA�K�����;=v���vj�f�U�e��������a�i�:?�E`��!]�ɛ2��4;��@�xmc;|�)u������E�n-٢�3�N�LA��?�i�2q�/��w�M�a��Au-q�)ĔX<e�������3|����6���bA�[� �����(�$_gC�"�1=����ƈF�� t6�9�{��l�K�=?��v`C�:�G��ԇ/fj��k] &�g���&8e�X�3��ғ�-���Ga?q[�o.M��ڀm���c^��MY�4J=�(��̩O��J�ь�o�G�IP�j�y>tB��0�~R@~����4#,~)e)��3��𩦘{�*�F�|�Y�_�]�yf�P�����K$��j�[jK�J�4#HA�,����f�/3�|�S�4�"V��@3|�)��O&ݙf�0sj�#��O�'�I=i�Ȭ3��Y�QN~ �bBSE��Dl�� ��n����of�S�.%��?If��a��ߣgg���CLY�������Ѷ)4�f���]S'/^)�af����r�33����y�l��䞿eZ�J���_y���5������P��L_yp<�Wf/%����P4����K-<�g�i���4_g_���:׹�/cZ��e�H^u��*�]fm�%���5��(��/���y�����L����pТL>��f�����(�E���p99�1���!�(�ӊ��M�aJ�����6�g�b�v�f��>���EI���<63�k�5�'ǘ.�t���Z�63迣�TXf��>�|���LiޚX4��M��q29m�����e�kf\S<%'a-���(}���>�)�D�#�.Y�L<�(�O0�?V�dy+p�Laj}�r�������Yd�q�,�E�����fa���G��{��7�4�z������sآL>���J�ߟ֋����e��rh������R��4�2a��R|d���L>���I���C49}�)�	)	[�S�d3C��4�ej����w��F49}|)�ޅ�W
6ӧ���Ds�क़B���CCl$r��RK^f�s͌Je�\��x��2|�9t	M?�8���z�M�w�kfاG^F�E&\�u\�q�	������?�0�� 3]k��hr��2�O�q�I§�R�^|gR��fM��$��J�����y0dz|F��,u.�ʚ�(�DP/s�Q&
Y��d���d�SK�d�w�?�E���ń�Eз')��� ��e~Mx�p�	�g���3Y��_�q����m8B_F#�3h��tjbk��eי�i6�5�������g�L�Z�/c��ϳͽE�(|z)��ϟ�����ΊGe��K�5|&��xf�G���aNa�%9酦C��G�r�)	H�Tks�3�t��N�2M��~S�+_�T�̥&�����e�˄��	�,�Tt-E+��$[C��G�b̾f[�3�8�;��2&����f �������dsS�L�10gпg��k�'�����U�� ��>�L�I+�c~*XA��(F0WL0���ΠaA���R�nI�0W�T��rƿgY�����$��^��^��?�%�E�oҧ����ˤ-t+����
z��7q��T-�W�[䎆���

g6Y�N��I!��S%溎)!���	u��6%�R[K$^�\A��V��������KM(�4'�\>��z�@/����˷�	=i�+H.�N3���\�Ԧ�4j�r]z�|�3�\>���#π����Z(��2�2%53�~�ό,�,�&H��A�\>����T��Ȩ�򩥬�5�\>�|-
��X��c� U�5��+�N��~��!��U�~�^"��+��U���;��+�(��c�ӂ�+�V��_F+W�P.�ֵ�P�
��Ku�N1&TJ�u�D�3�d�Sʥ��I!&��[x</d����*|w�g�)!�3��d�+(��Ǔ�*�\��RK���|8ٴ�/_�T��ɬ�whu��K��[����A�9��т����Iw��$�$���� �\���Y�l,r�,R�ӑV6��>��� �޽�z*�P�
Qdgf����H9z�B̃�l����Nr7��VX��.�V��ZQ��2�W���O"��:ߟ	"`��:ҟ��sP��zY㺰?��.����'��&�pQ/F��.8��*\�+h�&��)D��z�@r���Z�,0G�@R�B�v�]�'�z�]��E7��"h�#.iKf�>���=5"Ia����o9k��i#`��{
N����3I�r�I�I#��C��&���#(��:6�7�0V�9��`g��]� k\_�Bb�����H���آV�^>�|�Vztvr�|)�Ri��ng�Ǒ���J�c���{����q=
S�$�ؗ�N�z-ŋny�����ȩ��g�l�wz�8r���c'+h���{/��[,�E
��R{m�Z��b�J�vb�����b�4�����\]Vx�WPsẖ��{4]8R�I~&��X���M�����7���>;�XA���b���m'�瑕��I�[,G�w����c��I�o��i{�PI=�⏢Z,�J�T�km�c��bE`Rμ�@+�"vt�_���-,;�@q�{�&����;ϸ�����p_�.�z�y˅^;�H��5�t�N����&�׌R�&���eY�Я�a����U���J���`����&��x��#ԓ�3�h"�C��Ğ�oV����y[e��L�%5���(x��O������&'����H����H�I+l�(+}����Ur�H|�)%�u��0�ć��[ڢ�Kڡ� ����w� ;T����ؚ|�P�5U!I�LgTt��~��W�H�)���!B!>��&��Ƶb���oO�5�au������Uʇ��� ������EKhBl�y��&}�@u;�m�9o1<�_69�Kߍٵ|D:�^�^��;�6��� ��)�`��B (rJ�z�;^Ԗ��*�I6��4�yQ]�R��Ƅ�4�^��v&����^ԗ�s�w��&$�,�3�%�h����J>Ն}/JL�}&�8�tꆍ/jL���)~x�h;_���T����y�ְ�E��+,i���&���YP�Þ��/*M���:ɮ�A%AM�	�f	���;�V���8t�����a�b�Y��3��qh�&��u�DT��$��p�<|,*�{�@Ӑe��OF�:�Õ^v��7}I��W:�2�t�۰�E��{+�C�\��H�P�4�����U0�>m�1��͂����`�*͎��x$[T����*LAo�(~�G�Ge�+��gAa'�6h'ذ�E�鿟�2��f�8���ϻ�X$�
q���S�%A�6|F�edu^�W��J�(���BA]M�K*��]n��\
J�ߩѨ�� �\�ВF������sQoZ�����F�MɃz>�'�c��Rʞ	����yiV?H��>7 �oR�    ����TC��������T�g�z��n@M���K?�$��)�h���w	���6��8�K�$胾���~�gD���Yޝn�����~�$S2[i�F�g��yV:��
����U>��)�����OQ'sa�v�s}���^ge�ܨ����;A>Am���1�OP�O~|PFPnS��t�������UV~R��C>F�HBB����Q��{i�1)q@>K�����,GPt�������:|��5���l�NFa�둴7O��;m?T��G@��א����Ci��$M�L��0�j[��V�ZlHSFi�k��j��BX8�$��А����W�Ҽ�7
f�Cu뿟�Qne��������(w�ɂ0�f�~Ӵ�_�",2�ka�Jʚ�HX�sO����Ї�SU�� 5d,�����M2���P�OSe��d�7�,������,�e��L
]���Sũ�m���-��?��)6S��i�RLJKfz�����4@H����XU����H+Z	��w�VI�j��s��pOỄ\�&D���2���'��T����4�֮�����Cn3*a�KY_h�$zݑߌb��׵趴4�"T��G����b��yg�3Z�!룥}�@(��T��B �0�g^6D<�|UR�V� �A�SP��-�OY7�9��r4Y�\��Vhˍ�c�@�)H{����Q�*Hzs)�?�h�k��蜛���#�Y����du� �)��S�^2?}��>]��&�r^i��
˕���N>]���՚Y��q��K��B�td�|ª����y��p'��q@2�g���N>e�M��i��;��u%�uP? w�9�v֓���;���A1�ĝ|̪V��O"	Z�������@���X�`^��!w
���=D��3V���4``�)p��GQ�~0�XP����~ ��SV)O!W,�� ��]{y+�V�x
8��*_	@�lI��VzC��SX�S�3�8�|�*\E��e��x
h��֧� O>mm��~�gd���*e��)���ɧ��uR��~���ȧ7u�Վ�;��"��0�>n-�����h�[y�n�;��������N>g}���݁s����oid�^�s��y{�K"�s'��J;���)
�|�s֜��&?C�ç��%�MM�_u��Uؠ���W�:|Ȫ�Ŀ�x  ��
\�o(w
,��u�U�8w�L�ҰNx-#�S�S��*#� t'���E�Vh�&d��d�?��'=Jp��sV�HT~m���Zž$�n�ՠ�������r�;��U�PRϹ��A�S�>�|�W�N�;�Uj�����Н�����`�48*o[>������[�kQɼ��NA2�B�҃t'�~��,G�������Ѡw����i��^�u�Ǒ�Dw)��΁s5�mV�ˁu�@��5ǟ��� ������h��^���]�ќڝ}�*�)�+4����V��;�A�s�n/u���\�[ڝ}��5}u�ȁu� ��]]/}@���[%�X�e~��I�_���� �]x[��������wz ��O\����O?�w��k{A_"U�x砆h�E<Ѻ~��V�i7��A$�{u�mE��}ֺ�}�r�����>h��S���I�Yk{�>�;��U��2�vg�f�ɠk�ug���g�+��ԝ�B�S��]�X����U9痊	�;�l�[ΞN������V��y��A���٧�bտ�l`�9h�����0��8E�AG�^}��yp���}����J�(Ç�R��Q��~���UYH,ɿ���,�<�G[�� �������`�9��NM /I=ߵ}�H"0�~�c�T�c�_����Q�3e��o�-�P�3e�f��?H@�ه�r��[���8|���p&e��� �>���BPF���d�!�QAQ�:COH4uɮxq�pT��=ԥs�A>G�6؉�CuT�L9���/#�z���ЫZ�_S��>�7�稒wX�k�8�7��"]ITx*����$U�4��0<����N�|���G��Y��Z�xΧ�Gƛ�yf(��)����/�j��)�Use2%��)�U[iwN��YS��J����=ϐ�qؔǪ줥��9O��)UF��6h�>�æ�V��+����i1�3���!�8.ʁau�U��qX���Z0��c��\*mIN}L�A��;�\8d�ￔ��0�AJ�#�L�IQ���|�@e�D9p�JE��#����:��SRΊ�OP5�w�1�y�ᅍ���q�倡j2��kA >A�L9���� ����J9i�e�J9���J�qT�}~���2�8'�><��(w+�q��i�dP�ׂ:��IKj�˂3���Z����6�,�)�b��Q*�~�z$z[p@���I�0��h��S�sS=��
x��S�sS5L<�rs��2�JΖ���w��2���^VV-�� ���^�6)�8|�M1B:������ዞ��3={�_2���-)2(:���$���������Si&,���8|��O�wj�4��`	\��MX���w���TrQꣵ~ΰ�0��Uݒe�$���'��1S��1��`�ɩ��\�_�T)m��I_�p�*A��BP`%��;C�	,A���������	,>2�fb��W�:�u�Wz��
���F������8��0�o`	��M�fU|K�1�)��.R>�%H�Oߎ��r�CX|d*	�C�%�����*���)�<���G��.s9�/a�y�<D����2��a	��V6��������QZ��	�D	|�SVY�H&,1�?�m)��qЈOL�dT��
�D7�x)R����b��.=��dQ|d�Ն.��<lŧ���@f~zKa�(8�[j{��a�(q?y���	�E	J�n��޹��<�%� ��l�	0�n��CT-Ztl��ߢ �����w~��cT���� ~��cT���h`v��TyS��W?�[���eH�y��ݢ�,U�j.ެ��mQ|�*i,Ofp3�E	X�0)��S�@%��T��'�o:�y�x�4���-J�T_�j���ʋp\�J��3�*��(>X��y�N�88.��V��=o+�3B�٪v��]��rQ�jz�_�\��H�nx=�E	J��e��R�<,%���QG��E��X�$w�z IP	`���CY���7��W�@|���l2YI��([�:4��kQ|�*���>�3����m��a�FP	@�v��gQ�*uӦ�n I�hQ���Mk�Ӏ�eQ|���޴�$�E	���o�@e�,J@U��A�F�n^ ���/�ɢ�R͝ "8�E	z�7���Ţ�XU��T9{'u�aQ_j�7Z|�|9($(��_/&��,��Ve������dQsjy����dQ|��t����Ţ`5�>�Z��cQ�&�C���E��j
�I�0X�����8q�����;���g��73�>�<�%0���#�I�(>U�#4���XQ|�����0V��r�lp�t@U=n����}��%-�sE	r��BЄOS�6^�M(U�Ξ}�kE�Y�۲��Ԯ���������V�N���YQ|�*���=W��SԬU'?h"��`��<��g�r��E��a�(>@U#�i�g�I����帒�! j��T���gG�yx+j`>ݏo<o��W�����θ��p_�m�w�u����]+��k �"��~�DPTPF���Or!,j	�+��5U�ʑ��̈mQSj�&͔e �������kA#>E����~
�QwXl+L��}�A!��T��:A>E�o��Rh� Qk��Iҵ�&M<��Շ���\&?(ć��p&�-��@��T}�*��Rڵ�oG�Q3(�o~>�6�蔚��Q���T�r�A$>G�o�*94�+5U��J�!�5�*u�Kҩl�O}�+u�������@��T}���#=��V��ʾY+�ie�3"	(�w��E�>E����
�Z}��e�3;�'z��0�����='�Ih$(���o/�I�MM5H��&�I�$,���Y���P��Pk�f0�o�J�"�S_��K& ��#���    �7	��5T�>�h=TwXT���7N�PM���=�[����ިg�$I��Ti�-N����$�kG�E�����ㅼ�o	zIM�NTda������
�&r����S�*�y����O�7��;�O5ՠ�����M���T�M���MtVM�G������Uw�?�<o�J^� �V��3�3	2���s�i|�og;J��賺â�9���j�A	�G�|�^����T�/Kj^���V��J��"V�og$D��k�/^����h5�oe��ր�>o~?�j��5h65�ڝ��DU&��ɉ�;.:�}W�|9���r��*'0цu�Ŕ,i�3:	���[*�� R�R�Ѵ�M^��֠����������V��~KB���YwXTsW���%t�T-�<ny����:U*T)s�C����|��e�h�a�禿E����Z}�*�5�Ӌ
�Z}�*���Љ.�;�_��o� ��'�2���t���5�>oP��H|�*E�%р?�@��G��$��ր�Շ��}G��T�r�R�q]ωέ;���;�l�u�����"�=$�j�ɪ6`����[Sت��.RŠQ4��=X��m\S �Գ->B+��/J����D7�6�j�5���k�Aw������&!�~�
�D̺�B��2���2!�I~�hC$>h�Yj#U�I��$���R�ɣ�Ulf�B{���J;Fw��ɬ��+�&��5����|���raq?���#&lp8��m^w���ѕ������s�Wd�ET���JVm>Vl��=3�6��;�3m3�j��s�����|���^�����Ӗ�$d�� �-0��7�z�DЈ*}0�/�(�D��������j�d�_A���SeNR�.���������0��0u�gHm�!U���+���*������#szx@�-��j��O���814K�� ||Z5�/=��TV����_*���Sy�{Y2�i��j#��ش]���Jx�ش��T�銋�� ��{����^�n�|n* )�@{�nڂ��7w���nڂ�}E��O�8|j��_ϵ�Π�-�ߗv|��wA>3��!Nj��d0��N��ڑ�AM[@M�&x3Z̠�ͧ��Z(kB�kd@��CS���߾Zg�CS��W������Ǧکf]��l�|l*�Jkq����M[�{��)zf`���˚&]Efg:m>:�>c���g:m��T*��&���|�@N�ON� ����	L:m>:�_�5�A2�i�ѩ��^�f:m>:-ߺ���<mQ*9��i�i}�����𴾠��I���T6Db��$�z���kޥ�3�6�*_�(π�͇���FJ�H O�O��(��,�x�|x��[�f��TF}4�����E�k��f�f��"�����i�E��q��%?���-0�ַ.�.��O��. K�KS״8�A��G�r��ޤ�3�O%���}(m(2��w����T��q?
�"���b��;t�CRM�h7$m��Ȣ��@�-@��[u��@�-H嗫�V|�|�]�R�Mm��ʂ��:�|:*N��E�gmA��`�m�h���E�gA��@խM �-0�&y�� 
��7���.
�hj��� }%�h󙨔+_,�"�|"*��| Z�C��Ceu��Q˩���S(*�|�� 
���R9/�
`h�a�4��3�3�J�*=�-D
m>
M�=o�4 ��q�eqB'L�t�A �&�����TS��i�V�4��@C{@Cu]H��Q��vS�d�9Z�t�j��uc-l=�|]�x�Ca��|���L2����E����	��)��:�g���Ǟ���l��w��֭� �Q�����{�>?ˁ��x��NaaA��,B������e��QO�:�\t����U2������jtoZ����N�J��m�����M�H�{�<�����E�1�v�L��c�R|N��Ѯ�d�0(�'��KR�$�u��A_����Qd��R9�n�Č� S)�5$��g
�g���ۓ���tʄ<h�q�Ѓj�C�x�ˍS�e�K�@��uT��A�ӪM&�u9($�7)2��p��x*�����r=@����O���`���TSX�^�gd���К�4j8[�AW)=�������([у�R�*�\��(Z�lZ��l�,pR�}l�k;�O����Q��tR���M�G"X�":H���4�㸠N��vwjt�80�>4�O���qfУʧ{=4��h��5�:JU��f��t��)�����Vc��G=�З�;)�IS8+�>,�n��jڭ�JE�H�w���e��]}Z���ӂ�R!��d⨠��Tk���}�qV�}N*�w�e��0�����l�_G��?mz^�AP��I#��f�u���A�7��x�:�S��d*�;�?��<E�1���̓}�(Oу&RZ���c�8L�����98&�>$��Lz�B��=p��iW�߅C�>�f2��upHЃ§�<�����8hć�bzֽ8_�q���j�I��)A����g�9���9A�������8	�����4e��w����������{�/m�Ú<��(7�}l*���(��a� �N���d��}p*���pM�u���>8~2?<f �=���B}ˤ?4i�c��"��Xǀ�=h#��w��1%��T2 ��J_Q����J���U#�ܥ�J?37 ��>@}���� ��>@��d���F|�ڵ�=|���#T-�v����D���|�x���>B�N����w��X�:� �{�@J�2=B��qT�I� �=�ʥh��{�P��C�4����J<l-[G��@T�U�n�Ѵ���;w�W���=���NV�N �S�zz�@MG�Qxb�DU֛S�|����3�tSZuqG��L���z�P\B�?��{�rV�YG�pա�i�TU(��U�;?��QXE�_�n�s���uԢA!Է�$?����%�ݫgu(F�5}n���:#h�ff��fu(F T�[9�v&G%��U�U��y���J�G�z�Ck����qj�tE��4|�*�+e!����Ajn���Y�Oդ@R�A�zhF��>�o�k� �����2I� �#��ڦ�60G���S!���^GՉT=M���'q���jS�I	d{J;���sZ�vQ��S�7ǧR���!��t�0h�'�嵔����T��,<:?�'����ޒ0H1h�Җ�OJ�ѩ̽���<���:����>�HA >7UR�����A >9�o������I�H+�Š����I���|t*8^���DuLS>9���uf/H���R�:?(��2G��.AЅOM���:���c�
��E1�I$������/i=���){gb��A��T<)�!��A�մiC�I���S������0�#Hҗ�J���.#����N}��I���'����4��u����{~{g'����1��,��8����f��T`��}yV�q�*�ʡܙ�!A�G`2���#p\3t��*�_����&m���B�F@M�V$�fD�MK����j�=���f��T���rЌ ����g(����-D����9���s*1ЂI5K%��ә��k�2U�̌�O��K�GOf,�_<<�������!ax|]��$O��ZhRq	�C��xk��I<ǀ����EAǃ�^I/���k����ٌU��b��T��d	û�R�;�{�'y,�D*�R�>�L�5��H5o�P�V	�x��T��W=�0�AQөv�FA��b*(�o���%��B">�^>��umR����W HǛ0i����H%7Ez&�ڱ��xk��-E%��L���{���q����j�1��H%�`i�4�iPG`7��j���s�T9��Ï�X|���7���#4�RI�M�2N ��QT{K<�$��0��=�LgرL�R�K�'�%�`3��ߓ�|�8�`3(u��
��fd���OI�B������*l�ӦI�|�Aa    ���҄�����T�Y���>"�l�qbK������g:�-�up�y ����a2���� �#h%;�JJ�jl�tT�B��Y�hT����Kd�Q�=P��aFDG���	k�d�뒞�$�L�t�f5R��`��>"�Z�k�=BA2�S�<xef��T
�<��
J��cR��Jm�A�cf�M�͞��A">*����� ��e�du;H���L��j��D�1�23��/��V�`�睠�?h�Ǥr���8�����s�Z���gK�w�}B�|l*f`0�c��6'Qd����\�}a����.�˓��c?1�z��ּ:+�H�ه����qH��ZBI��C����1�T\W(�@ I�!�°��>$����Yt�I��_�K�F��w��Tpl��$�
���1(����0K�X>6ӧ���hC�|l?��I��"Q#X	�4|N�[	.w(a�F�I�ìfV��<|L*0iq�Ћ�-�j���vv�0ħ���jM��d��T2�
���؂L��o#��aI��/��AH$Ƥ�4qK�cR9��H� ���>�#Ma�1w�i4Ga�1<�,��.��A��o�:����|�r*�[�����J���r��T\B�XQ�rl9|J*5b2��0(��YG8=��{��WE\�����sI�o�����xi}��4$�×�4dǦ�'�Z"o�;Z� �ַb�锐0��B$��=�c�d���s�@ӡ}���_y��4�~�S�:|d*�E��|�TA_� (���-�g�	�.|Z*U�)�w{��jGȬ��0(#���Z&���!aP��J�-�OǞ#0��K�߮c�d�y�+A>%U�q�'�c��s��|+��G��x���M+,���~�����p�O�f��� <��G�Yx�gr��RY��M>�3��E����;����=��ӧ�Cđi�g��	�lR�p��R��$���{�3f�o� ^p��?����w��
>3���+��?}<��R͢���?8�y�{�yS/�
ਤ7\�Q8��OF����0�/���U���aa~I(M���>��SQ�qZ�$f &��$�e��ÿ|$Ze��9��'��m>�˧��Mq:��JXB�o0^�n%�6��_�ut��Ç>Dp�/��_�=R=L��磺Л�iJ � }�d!A >����ˠ� �>o��Er����Tϵ:56�0h$H�W7�p��_Q��˕~��+𐶷�O0��S��;I���w�z��a�_>���0p�/��@K����_>��I��yF����]���.�T.]�S4���_>#Ճ�a����T����y��H�8��z�)��[�`� ��E��l��a�_>�{��P<� ���!�T���a�_>!���[�ý���A�0�#�\�;kJ��#�,+^[�����m1��"]a�'�K��HW�H?����� ���WP2�.�Ç���0����A>$�j�i��T�Ƽ����_A���!���B���ʷixt��r2��.�J"�`�B�]A��.{��ׂ8�|=UYqp��mv�7�0�Ç��R��������w�����L�Z�&Q����Qy]ۉ�Q�F��juK��Ѿ��]#W`U�(��`�+`�C� �[6�|6����1ЄF�|�F�h�"�Td��]>�k��G���Q]v͋5}"�OD߂�$����'�hҗ0t�0Tݏ�����"r���}�Y�\ 	�j�H泃�C�
�8--�H+�A�
ph����G{��P�(H�`��g���e-�� �jÐJ�.P�
Q�'ѻ��f�'�"@�SM�)ѯ]An�[3�~P��Q���_���!�
<�E�lp�c���P�h�|
Z����_p��Eh^� ]a>�t+��(��٘��!��WӸ`��	r��_��w �
 h�j��;�@��OS��~ �.��o�6:9=Z@��IS}y�B�S�=�Ĕ|t�\}�N� ]�Y�t�g*���\���3t8~4\AER-E�/��+�R�	�*�$��q�W��g~�z=z&���s���gf�|�o�L�=o��8�w�}�=��u���B(�k��<�w���һAP������5yqaΡ?q�U�.d����B�ida�`�֡[PҭEZ!��G @5u��q��S2u��|}&���Oц�?�<�M�#*F�'q$�	u��3i�n#IMhÇ�{֓?b�6����)|)h�'��f���]PG�>%9]J���X���?��k�ϗ�:���j��s4y�sțy｠��~jQP�
-H#0���ܘAA��7疟�;C�Y�\� �z~]?�����yV6^g�u�i�V��hI�G�&�Lh���_��y��v�0����Հ^���YJ��=Ε�٧>G��B�O�5g���B��\Eh?����w���Hs|�����x�~�3�ڃ����<��d��Wh��X{�{�@��ǞYw�EJ�^�
��SJ#J��������*���-��D����'O�o5A5A:����"
�	j�6�\�~�f|*���h4A5>	���n����U�t�ޮg��H;�7"
r�A��*kͥr]b	X����RI��OCDk�{~�J	��Oy"�g�R���Z@�hi
�X|*��U�䡲�3��Q�*��ϯ�8��G��#�Y�⠗ �>�2��� ��#Ҫ��eG��D���&��_�-Q T��R����1�	8C7A��^�H?����/��M�")=�� ���V�<�Ǭ@3A�RiY�_��r�M O�o�~D�*���=�^�a�j��NS�OQ$�v���&\���)��X�g^�	��E����q�K�,��T��X���W�ߤ�A0AN�|�^:Ͽ�	z=m%�A�*�*UH�G��Xx +�����iW��}��K�W�6��~���Uy�痝#
b������Gh%�ʗ��1X,b	�>�&�﹙�5h%�˗���R�⠕�x�~�����7h%�7��\�	�����ɏ�A+A��G����]o�J�\��R���K��ǮRát��Jq�K ^�ԥGO��0@/��T�E�Q��^��ޜ�!���9�4k�
�C->��~����V����⠖�1���巨C/AYSi@3���8!��?T�H��\��	�l�=4O/z�,��Sg]i�_��[�y���B\P�����a����Fn�w���,�Ϥk[�B�ߑm
�ҽP�=G��^80#��Qk3wz*G�(|K�T�����QX������BTGT�pɺ@�mΑ>
���Ϻ���qK�1*ə�X���VV�uV�rb#m�k�h��B~��G�Ȧ*e��'�uAz`U��D�<!����ڎ�u���)(w*��,A�@}�W-�8����`�n+)�S��4<�>*�+��z2���j�$nw����g�R��[��a�}|kE�x���8�W��<�CK����E�=Cp"�A3������'��j�����)b�A�Q�5,@��������@ 禌\LDA(>Е�%���8�S��gI��=셓vuV��7v�)H���3'�%6�fz�K�.c�P�N��S�L�כ��p
��P�O�� �i�t��������{�� ������BZ�n�����jR' ��)⺢��� B1>��~���G����p�Gh�T)⟈�m
��^��m����M>ڕR�����G�v�Ď�0�>�o��;�#9��6���H<�c��Z������m
� ȱ�t�z���ڦ��TҔ��q,�0�A���u�׍b���4�V������ݒuR��X���&�jQ�Γ/6�)���lJ<�as�"��"���^��̞�*e�@�% ���e����7���`��_-⠗��ʔ]�x!���η�K�G��m
��t��h���6�l��L^AЊ�ue�����A.>�}�d�;��m
������ ��cg`�=��<?8�ء%V��g^q���lB�6��İ�\W&��wT�,`���A*�\W�    .�2��l0�a� i���R)0�.��CF>�F��O��Q)@�Z��}��$��-_���&��j�2o�"�?*0J� �ʡ���i�S8�R v����u=h��S�R\ׂP|����)F�8�$*��hzS����K%��J����:�!b	|�ڍw^?j��-��kE�^��;�\S)��{�'P�W~�`�J>Օ�EI�]W����j��уo*�TW���z��N��T+nf�޵g�}*�TW���9\Kr8��Ou�8[s��]��*%X��/��E�|�+��қ�^W�Z"�+�$�6�D)�X���m~�8SK��nU��⠌��8ɞ��RrF����v�\w]�"0��-���]G&)�Y��[�ip\G\X�(Y�6���ܥ�=2�j�9T������(��@����a -�A�.�Ŕ�0h%��j�
�V|��%�fQ��]��c��&���y�u\x���Ҝ��^5v���+��}�[ެ*uL�\n��گ{��G5Zž�g�K�@�9��*S�7���1���N���~ ��g��&���9�9�Dw_J�B��������d�b���rꗯg����ow��b���J/ӽ&���y�%��VR�TP���	$���])�P���Fvg�A���/F��42<s��n����^& ���]�Qb+��^���)�A^+!�3��՞s�F���#�]).��t~@0�Ov�k2��82>w�?��9�2���'x�l��=s����:Q,hP� ��u��b�tU�>����DF��uS]�9hg���,ğt����Ϝ�n�
��� ���s�j�w6H �ه��/�1�!�J��WI�����t� ��e�� :�r5��C���N$5�� :sP��-ʘ9@g�I�02)T�\׃T|���i����F�,^�� ̙��r�8���(�G�3��$7G>|٧�rBX�,籄
����(�_+�)_�M�qz�Fd�*ߜ�,;�AcŕD�G̲���|�+~�����8�ŧ�}�	z_[��A,A�׬k�R��A.>��V"R:��8H%��>�ީ�?������M��8��i�^�w�7zl5�o��t=�c�9P,�]Y�1����ׂ�}�;�k=^�o�X|�+��J�yЏ}F��}^�<�m86>�C�VI�H�%����+.[�R|�+����~�q���G��-�V~e���Oq������-
:��.ݭb���yF7��A+>��>�S:��69�),�c_��[i��4=��8%����������W��k��@*>���oϒ����#�W��M^�K,pr�К+���C��C\1���/,r�!�:Gʯ�� ���wA]�8�%`����@��D!��4����w<kq�L�D��H�o�UB3A����@�&(�P�����Oq� �v �?�C>�mK���A"�+�	P��&����$W�����ְ)*��j����⢢;��pEuD������[<�aCT|���Ia.�M����d$�����%�99��I���\5?~����W��������)V슊Or��i��)��X���5��vE%(�P����>!���j����]Q�Yny�إkб#*�՞<�=B*�5W��>��@x?JPHv肺\3x?J@q�2���z0	��\-�u-�a�(�/W[�u����(>�]oc�r��`�(AE��O"/�a�(>��:��0��(>���~O���A*>�U���x�ERC��7����1R�;��盳8h%ාε�Z��/R~[%]W�R|z��t�ⱃU�ue%�K�A^�jXE�o��>��F���)�����(B�ѭֈ)�Z�-R|t������[�H	
�J���kbqp����w���8(�'��V����xE�q��2M-Z�!���w��EJ q��%mK�nj	 n��T�B1>ĕo����M��k�����1R|����ci�2R�r]�?r㑇e����$�L~!�)A��GW�:�-R|�+'RW�ڡ�/R�����Ƀ�H	�5��wE��s�A�����K���@)>���]�	n��Z�}��Z��7kC����,R|�+�$c�wQ�Y��W�e��5�n�H�9�,�\]'�A,>ǝoa�����U������
�~��j��]���(W�PR�����)A׮�u�\׃f|���q���$W��@�����H�Q��-堈q��)�����χq��\Y�k-rVhn�in���	�A5�)��n��b|�kEu��	��8W�;}J��B1�#W���y�)$8r�P�~�uEh�g�Z��K��mR�[��GIW$�3���x=�&ŧ�����!�->�-���6�n�
.�	P��OH&h�%�./��@h&(m�^{Ӛ�qR�Û[�=�8(&��j͹v]r��-<��+����������/|A���c]����˘�0�늂5#���c ��=��:|#��RRB���`0>�-�6���0>���ϯ��������H�no�d�-�� PW��&Y4�1��-ڹ�:;$���"��9�a����~���a�������H�� �ˉ�����IY�@�5(�P_`��-)����0��T[�����:J��s�*���AIkT��/��H,��#�CB���ҍ��9@�5��W,��h������� ���BѶ�<��� �r)�_�h������!��_w�顐x�>���H��!�݊ON�щ�#�h�*�����>�-�F݌�QM<W��.y��� !���·�5 �V�m�w�!�s�%�.&+�(��m��]��P$�C��-*~�+Ck\�WVQ�H�l��?D���ZM`���m����u����k�#�T^����[���?��\��k�li����
�>�U/W�����5(��4=�\[r�� [mг'�u	,���Lm͛d�«m%N���1�A)��h?��zyڪ1U��JmCm��@�5�jV�ڊϣ,�Gq��EXQ�S���f�*)�o�l���.�f����Z���`�����鮥6�f����1���ج��6)	g�QE��6�s8� ����#�k͵�1{���H0�>��F��/q����Om�l�<Z��0��v��$�q�m��P`Bu	�A�bU9��3[Y���+K֠|��^%�h��V�L5������Z���	�z��������EDVѪ��z점�ǳ�R�A0�K؛��<�t{E~�޸��\�����R��I3�+���������<��YWwmR�^���y�=�@��q���c�p�MQ�����@�v�'44����(�AŁ�v�m1h}�ot$����ጮ�o��O��bʹrM���FW����*3��|��k�b�ۛ���
��X9.ת�����ž}�h��otH��>$���X-m_��A)>�͚������Wª�t]����]�1�L�RWa����נ>��Ọ�gy��SX�������,��kP"A�t�g	�^����Q�*��Uo��W^�B+>��.Z�6q���wr��'p��S��d�/� �W��ʻ�ī�����jP��w֬��kj��,uW�:	����R�?��V���b�]q�b%�b�0�8�L�A���<X�����K)�f5�-p"0jK&i��~0q���̃�߂"	���s዁���[;��\�v�.��2ȇ/��<V�2~�E@�͇��K�;�P�lKj�e��Y�dVw������NBR�z�%��Y��8�� �7��J1�64���0�>��G��a�#_-�"
��Y���ҟ�}Ȭ����ݷ�Q;�.��<���)ʣ籹^q� ����)�}���Z45��0'�hV3��f���7��*��V�� ��W�T��������g5����È��v/��jE����-(v�/O_@�-�t[��4^�c��C��1x�A�[d�Ն�,e���B�W��߂��\1g��星8 |��@�ka&`l�gP�~��
��06i�H    ��&�~e{β}o>���5�� �����q�ܽul�"]�r���xk��uo>y�bYR���"����.�ޤ�k;�����<��|���dbJ��P��[`��Rd5ѷ�/�0��BG@\�г@]�y��ڧ��k�o�IV�{��^1���W�f��a�8��8e�ғ�1�)�|��oa���:�|�����>�`��g��k�t�%TP��^��o�OV,�U��(b��k{�ai���w�|��_ӡ��(
j	�kR�t]R	JT����;<�-�W����|g}��X�>��*��s��kA��2����8�q*��+���ZP�@:�Uk�gqЊc�pwײ��D5k%�Kڅ_?b	�HƵ��r�@����F�$�@�*u�����b�~q�-�X�k��?���ȞoU<�i�h1�!v�T��*����!m�o��z���DVr>�Rς���DV��e��]�	��ت�����"m>�գ�Kz��^&��(ר��� Ȏ���7~�׸Ed)�!JІ�}]��@ H[P��|3���A(�����}���ϸ�DV���Ͳ�8(��Z��ω�a3n>�m�>eY�6��6u�]��Y�[���.�+�~>��D�Oa��{���[P�Vk�<B��uI%��J����07���q��Ίt����_�l��$]��U ��gr�F���A_֧|�r����_�[�.��zqA6�rMx����^��Zx%v��{��-/�xx {�`�f��o`c�Us]J����طi���[�R֥.�x�&�5 �gZxrnǸk�������s�>�՝2���&�+��>�>|��'�/a�����=p�J�yΰ>��u���_�� ��_�lA�?���Uf�Q5!���+�2��;޽Y�ˁ2��;?wd�sפ�ki�p*���aP�ԢM"�k�xX��O]�&50>�=`��Pt93�p��k���A%��[���o2�d�Y��B��${�Sl�F�zT��g_�MJ<�@��g�������l��1�A%�OZ%�9}�UU��e�������V�H��r�8h#�9��|�k�+9���ݔ���7֕��	:&:f"�pm�(=>Rɐ�b4h'_b�sW��ֻ����2Ib�
�.���:��r&��G������3F����-w� ��1���R]�{L�&&��B�Jw�Qr�C�WRl3 ��{���r�0���{���Wat\G=Q�>Rvad�3���jžJD�3!��ҧǬ)yތ��H�o�J�ɐ,���$������i��)�ތ�,�N�nl �R�͘�^�+3/5�ћ���p!\0u���,*��]C�єJo&L�
u�~�)��L�a+)6����f�����@R�3Yiڿ���8&L�H����M��f�e�Z^���)��Lt��*�P2_�� ��1����~ܝ��tz3�����x�)�ތ�,N�?��M��fe�<6?L��B��n�j�Pv���aBͤ��#�[ڶ��$j�H���e'	��#Yq�*ν�)�ތ���ƘbRm�-�FVJ�.����|�+�I�j�DV���M1b���7������3IԌi,|�q5{��#��1�mw���z3q�=Ҳ�z�4�Л�Wn~�����$��1���#v��&5z3�(�m�UДFo�@h[��v �%��G��̥)�ތ���gS��l&�x��Z���uL������Hr6c$��,�F1Y��\6qS��f��lʥM)�f�>��n;675��lR��
�%d�~2��1S2��L�3LA�D+;7Q�7c,{�6�U�)ԖY�^�o�y��%��.��۴��X[�f?b#;����B0������Y1q�����θx{;o��q�7oW��T���M�ų�-���$M)�Vf��~�&x[��,���7nJ���2:�o�A��,;^�7ՠǩ2��m%Lv��Nq+3A�J<g���$I[1��нC���,m�h�b�����N�bWw�v´��c��0���_�!�	���ᡸc	�ڊ)�l9'��~+��>(�ShJ��aK:��K�����t4tߔ��Zl���ln�AĶb^�Ӛ"q0qL�D&���ܒG��b`U:�ڊ�-��7�}�%11XWZT��G��n����R�;6����xv�~���/1�����/��	�V�o��抋c��Ks���(\1ǅgf�vxM	WbfP�M�;�ȭ���X?L���b��6�m�I�Vqq�����#�[�����a�@�˭��o��]������M�#�+�tI�g�\rsn*W�n��.���z���CK��{&L����F]�rǦ�+���[� צ&�+���Q�9�Q�1탚�
��4�+F���ܑM�W�p߭ DG�>�
\1�Ťt��25%
\1Ņt
��v�V�����bB{�Q�2c���z1
��}+���!rB#�lJܷ�{��N�@9�1�����%T]I���R� 4%�[	��m'����D[{�V)ޔ�o%w�{7_+uߊ9.̘���6��[���E�Q�%����iJַb��d��_�ߒɒxς@�WZ�	J�����'�� ������$�Ma{�Pھc\��Y�/dS�c���{�d�+f�]�b�� ���7���C��V�o�qʩ��̄2W��:o�q��t�	�T��5��[�����<��0OU-z	@��������b�Ca��`u%��޽7�AcگJ��b��ʆy��*)�ߊQ.$}�n�A$Z]	��}�w���)N�������7��2mb��*:&��^%�Օ�k���j$֕�<�|Js����q�@�=�J�.ZgaEj.�y]4�����)e�y��O��vqS��]���n�V0q�L.�Xs-+U�ĕ8�~.��0d�+q�ݟ���1_b�+�>�����t���4Oj�~��8�K�t!�?|O�WLuQ�{���IYWLu��	�6ah���8�����1ab���ʨNޔ�q�`��=��Q	w"�u(�R5�邝I��g��������Ww�s��q�FiwLsQ������;F��6�8��5��
�u�H �c��TfmTcTrUnM� �ELqqI[z�O��Y)/��Y�n��%��1�}7���6�bw�p�nE%��	�]@��Z(i�N����I�v��V:�[����;!��C��m�ם`�%{6W�S��S[��`k�Jݸc��wGz��q��wkm>���q�����Ng�ݲ�
R-�w�j1e��l����N�{0;خ�f;f��&�*�6�c��*�?�X��P5��S��`Ǡv|z��ZWR�����\����Ji��Y4��:)]IM(G=f��6���]i�v�jQ�<�9Eu%5�1��y�;��";f�،�� ��1abV��*��ѕ�lǨV�>2u%6�1�ņ��� s&��P�H3 ǜIp�J��ݏ��L��vVn3`���21��ס�l�j�";i�~K�jm���$g;���#I;�#;����j[Е�l���t�.,�hHvLm��h;�bW��C[�'^�5ܣa��Զ���o�N��*��Е�l��V8Nw���!<�Z�ҺR��ٮ�}�b?��d��v���ە�l'��?���.�	3�u���Kj])�v�l�8�kW;��#;�C뢉���d#!�1��nCJ_�72ibp+76�d�������u%=�1��[�|�%qDp7p����JW�b�%�R���"@G��F(�c�7�p�g��Td'ƴ�M�S솵+�َ�-�����|FvLn����O�(#;iv�#`�K��N�>,�M�#;qCX���ƞ钸Ӣ��j�i�dd���X��@���[�Z�A��<&M�,��@�m]	Fv�m�x��e��}$s&�"���/o�31�ퟍ��9� I�m��Y�'ۜQ�$���HC^;1)JS[��9�P�8&Mm�	���<�����GA9>�	�Kj���=��J¶j�����v#
�$=��_�+4�m�8��-/����8I�j�D�oM��̚��V�8�ܕ�m'6	�.���)�&f��    첣���P%�����֗T%Q�n��Ba����lG$&�I��վ�X��[�MI�p��M��*�&�"PD.�5'i��ί�o��c�٧�},�I�hGj�/0ui'f����[ԨJ;	�-���+�K������!P�vI.��M��$N	���}��G;1���M;�[�v� ����Q�vb����"7(D;y1�S�+ډA.�sC���2��\tq/�[�;�Q�v����.�(D;1�3���C;1ѕ%��k�\�C;1�=�g��B�S�~o��(P�v��v�n�I%ډ���x��]�h'��(���lK9����9ו�Ĉ�q�1#��h'F����#���.1�78|m��c�Ĕ�fقǌ�)��x�ۙ]i�NLy�/��	H������X����"ەA�I8/����8fL�yцMͫ{.L���$m��+	�I�nu���GX{��bh�����0�"K��pw�`;1���"��] 3&Ƽ��*~�8fL�\�]��@�Ȕ�9��d`�=\�מ��v�"������ĘWnl�*���D����?#\�J�vb��1��]�R����v���wi�/JP{_|�]�T���tW�m!H�.:���W�f����'����Q��bJ�\].%U�:��]�*4ȔN�.\���ӆ	dJ�p���ٿU�:1�E;G�3�D��Z�D�+V��2���+�,f��fub�a���t8d��Չ�����;kU�:1�%�D�ٟ�&¤������61ܕ/z;-�8fMw�B���PE���rQ�}�C�����\����PU�#^�^�sZ�hub�+wD�?��hu�d��T�+q��Չ9/fk��f���	3�[�_��^ub�+W8���6T��$�
��?r1q̖��>�	����r�I��P�fJw1Y�k`�(1�ŋ Q{w�����ro��Ø&1م���+,~�Y�:1܅P }p��Vub��]Z'�7����]`���q̕�������JՉ���Tҋ�M��NFv�-�[�CթN҂L�\��>T��$rܻ�-� 3&&���K5��@�L&Ǖ��vX�:1�ōr�����X�:����=�M�,X���vܭ��O����0]�\�Nu��c���Y�:����?�����܃&���0�IBr�o�P�3\�~�M3$�U8�F���c}�$
\�,�
��'��I ��X��ȱ<ub�+������\`����2E�2�����tY�Lѱ����l��[��a�ovA�覅oe��e? 5����oa��e.����<�����Dq[�oa��en-�7����[��a�/���+���hX�wNu*�U)����^`D6ݼ>�01�]����} &���k1X��c�{0�C��(&�������=�1Y�1�;��7�L��趻��+�b��D�-�\�dS&&�ґ݋��R�-�Ť���l.&LꄋK�v��%��m�g�Q�,��:���&��
4,��zC�wd��������$��� -�����#1ýBs�73$������HD��x�ڤ�L������a���,`ѫ��[.�[�5X��W�&���sC��$1�� �iSys��{�����D�{?�^�,+Ѫ췬ԚΚ8`�o�T�~����-�Р�q��Q��˾M�ݡ���K����%f
�R�S�ms:XU�U�?�H!u�c'���<Q����X�a�$�
o�C�e=��Jt+�s�{�i�!�J�+��m�
�<.��\i�إ��c�� �v�d]��e���2�`U��e��Y����1e�i�����`Y��e~zA���΃u%����<ď�&�J4-�A�z����M�~���l�XW�i���Mk����|��ܜ�&&���[�i���`U��e���$��D�27�0u�L�6�)�8��O��&va�$�(��v��D�3?� ���V6XZ���/�����c�$��b�4C�U�L�o�-w�=+S&��ŜV$��1eb~�Q@g'�JT�L�oq�C[�e_�ʔ�	.Vx'CtI��Y���s`�H���;�#�-x:$���}�,g-�-�y��1��Xa� ��g?��xyѠ=�\ؖ�g��T����#��LS%qU���?�����hse�
Q�E�ߙ�qKd����-��܈�H?���t*��7fJLo���'�4I�D��ߘ.���[�.���Bg�$ƸP����3[bz+�Φ3tE��Y���We�$�o�C���6�3cb�ۤJ���aB�4I߲�3�"�v�Mҽl���M��ĉa.6�� .7v&OLs���m�v�N�s���g�o:�<1Յ&:n�<�9I��UF��̜����>�(�]i{{~�0��&m���ʝ�qߔ)	�}�S4����}��L�����h|��C�"��=��%�U�V�_�#��g�}x�w�h����H��OE�n��XK�t�A��<tJ�W���I�} ��%��(��C1���3Zc;�Ī%Ḣ���t�$��=zi�i��D�{p�!���15��e:��bj�����]Aj��-N��'=1jɬDy�7�w&K-1�ż����<�SKb�P���^+0�̒���5j���Y�$�V���x�yV/1�]蘳Ew���.�=iʻ��ҫ��U�/��i&�:�*�*�ϋ��xw���	�q�&� ��υ��|���Υh�1�(���(Wi140q|�b�WAȰ���|�b���0��8����޳��}�#J"ƕ���v�	#J����%���=iD�a����i�2�]��Wa4;�%�sV�#H��F�=��ѻJb�] S&W�B�Qܓa��D�A1�pqL���v�]�=Q�L��lȚ\GQ�&�U�LC Qb�+e��'`c�&��=Wֹ��D"QY.��Z��TB��/`�o�PL�̉�.���o�N�K��bgU�l��N�vt�E1s�,ho\����I�r�E�Վ�DIz���\bǴ��.�E.���t�$�C���/ʴ�ٮ�:�e]s&�zV��\�O�D�+wU�ߩ��̘�,W<���Ee2_煉㴉MBQ��.��e�{2_R����#B�ƑP���b��D�v�!�(1���M��$�(1����W�d�%1�}����L���͢S�b�3]�]���}g�$J�t�X����<I|ޜ�D�T7rL���0r;F��,WZ�BQb�+&��>�8�H�s�eZ�x��8�H�s��a�0�$� ��t�&iDI����/�LI�re˄����L��q��w[PE#rI��f�c�d.�8A|~d�$.�ri����En�l{씩pD�t�����b�$r\��r����D�ǝ2�7�)*C]�ze����1Yb�+]U�-�'s%Q�^���.YщD�;���8fK검^��,	������Hz����I5��,	�]�I�ۯ(>c\�U�䏌c�$����<��D�q�'v��2_b�+�����0����s�dfw<��$��G. }�E2�	�\�X<w@����LM<�����\5��"�vCA)W�i.��j3w ���&wF�t�0���ə�~��}y��1��ܳ���}�d`��|�����'A�1�,���LM�X���ȏ�O�n5�I� ���I�XM�];�O�;�1_R��&�g�|I�n���r�ƈWC�s�f�%Q��O1��M����{�s�q̕��D/�7�R9V�hRwߒ�^��d�&3%�rX��M��j�w����1Mb�+]��~@��j�v�L���'�%I�3�)?����.��0�_�Ә#1؅�	ͽ��Q��j�u�a�CT�6�)�t=��QV�b�&1���?q[^6�i�p�#W����*�XM�+v*Rb�s]l�`�`��J,Vc�\c0{FQb��jt����a&ƺ�8�?C1�1a��r���}&��"��~IWR�C]i�ץLb�/1Խ����Ş��X��\��b؏$���Xw]+�i��R���"g�4�6G	gku��)�Nd�5����>&˞ܕ\��P����_�%F1eb��oZ�ɕ\�&N��r��C��7���q�c�������A�j    ���rU%��1Ǖ�H�yk�Uk��}�r���V�WLq��Cw�=\�������2J�J�џ'
N��;��@�������;[Ua�&$�Me�
n�����Y���r���0TU٬I۳uw��}Sfu�r�%��a���p"�Q.d��U�c��6d@�S�	��\�הޘ:N�����to��6i�ҙ��0yYfm���70N�0��1L��J���8v���>{.�8�0_��
7>8��U+gLtA�q�cN;�j錩�L2�q+�11��cz�)&�o�a�mfKQU]�f2�wݬ������쾛3t/��>�p�`w]�U�"�U31[�B��c^-��P�@��᫪�Y��gE�c�
�p&`s���aS�����O�O_�1c2]��e{>ݏd��t��(k�T�n��/"��] S&��"���h[U=�&>�(��p�=�5�[�����&�i��5>M��eM�&6���*� �Úf�/�	,���i��s/�x��7�e͚8/L�,Z���5i�V��A��ǪfM���밟ƒf�/��}�XҬ1�o�ܶ�>��y[��ngC�5k⡻?%w�h�k֘�x'd�y̗��i�\�MP�6[w��X˙ս,n���=Jm�g��⭌hn�8���b�+�����
��UI���g������$ �ɦ��-7`X߮��KD/�Ԏ:k�-i�vD�Y�{��q����vs��fKt�@��(&Jv߰���sc�L��.��p�>6�Y�lIc�!�];����b�g��~EV6[���[P�б����~j�\;�i��͖�w�o����i'\�6[�{����>���]��q|������f5�E�/��n�aԶ9��fK|t;Ns��&�-��#ų�3K�-F�@Ǣ*�67Y�l	�mR%�u��j�-��B��WX�l1�E���������j�;��ْFi��p;z�k���~���~ s%�a��
��a��U͖�K�W�`7a�}-Zy��c���m�@�LLy��j��L���Y�{���,���%`wꂪ*�-Ƽ���>�9���
���ZhK��݃��
&��J=��M��b�+���"�q̗�Gw�%����Ж�]i��q�	d�$xW5�
cʄ��m�hgBD[�(mܳ��)�"�b���}W>k�(+�-i�&r�w���v���D�; /wS=�-Ƽ�A�I�m�ٞe��^�@���+UUF[&څk��Ί��U���^�C)"�7q̛�Rw��p�(�L�zAn������qIۈ�>�`�� qKX��D��a�o	��TS���|Kڥ�K����)F�b֋v�Ғ�}�-c��V��d�Ŭg�S����TK�~�M�q��y���ǜWD����,�i1�]�;A�qK�t�ߏ.0ۍqKX/.�y<���L��Q���bM1��^9�?�1ab��>���2aK]���r�� qK`�s��n�))qKl�{�^���B�7���_�����qZ��I�["�]37�yL�D�;E4W�}	�[�;��=g�cC�81�%4�Ӿ�$�-&����gP�8�Mb��q�2�)L���N��ǾQ$�-����cS��%����pq̙�����q7'��-�P�������%�-�`������NܗGQʎAqKx/>�0̘D�+hU"����-������2흢����0qK4��z5&�9�X3T��ܺFR�b�����-�8�LL|a��e�IqK�v�=��=�11��s��}�'�w��b3���ǴW�{��t��M{u�0�����g�A &�;õ��.���{B{�י�f4b��3t�?h��M��ߍ]B�)MJܓ�i������q�:��&S�	d�d���^��n��{��o?��=fKL|�
�J7_��\͋K��~Iq��/��w�l�f5Yq��/�N���8�KL}��Y����=��]\ݯc�$�w�����GR���kJ�$�%����cxM����!L��v���=&��2��]�%1lؘ<���������e힀���������n-",�a�;-I;��=��}�7׺;����~W��o��k$�=!��H-cU;����J���n����Bp��Zv� 2�iv��%�v�D�=���ּn�9�=��J���F^�φ�R۲ϓ��'�R��6I��{�|qSe��R�0����B~,���̗��q�u���C_�0���Ms%�������c��з��c����C_\�Ʋ�����=c��2�#d�=1i�r���(fJB|�#\w���_H�����8�J��0`p��Wr��^�h;�]VH�{�=��Ɉ��g����N�R�2�̕�b�a�_��d�$ƻwS]�QS��p^���Q7wZ��=A�.�s�Od�d����g/��E�c�{�`�jl���G�cҋ���?ڣ��G�c�+�a�_K\�1cbҋ�\-���&�i�^H����c�51��W��E~�&MOt����@�ޞx4@�v��������k0�� ��������#�o�.M�y/�H��~ �&ƽXi!��{2ib�+�W�ܤ�=���F�$��	학͢7u��'�����+4u��ǤS�|������̮�-Qwhz�x��Cc���MO�wQ'�gO��M�	/Z���+2M��BdK,��LO�wѐ�ɚd�'&��$�(�H����C����=qh跡�Q6u/��P��0I=/�G�ŊL��m!�ۆr�c����%j�O�v�c���i�_t�Y���a��.���R�~���qq�����O�v(����º�j�=���ڔ���0��*bO�w��࣫"b�/�=�s�?KW�Ğ^�o���&�ZX�	`�)��G�,�k��4�O�}�jiI��Z�Od�Ĉd
"���ZZ�۰�sc���BJ��͹���#�n�Z����\��<=�F���zo�v�ue$�h�+o�ߓkˈ/���vZ]�G�xq�G���D./#A����ִ��%f$��w�@�����UfĜw~�IW��s���'��%&f�r�"�0q̗�b
^��Ů��#F�j���v8bċ��?��,���"Y*n��w��Ñ��Bl �%v�X8�����n�Y9����+pn�d�p$����9%�Od�$��f�ms�����9/��f*b/fB�4�7��1��j,���8W��/m�|cq$
�#����wU?��ù��n+�#ƽ�L���(S'��h�#"�,��82�#:�Q���io���2���Y/�I�-e�n0�̝���]����L�����/��P�8�̷��ۄc!q��Wް��#qr@�=�g��:�HX�s�y;��#�����|p5�`XK��N�"v��L����a���`-q��W�b���0c��k�bw?�����Ni�h��Gb�{=1�\�2�y�¸��k�#���?7���8�{�m���,!���B�qI{XE�����tt�U�)����"�z7Ov�YH	�E�����Y���C�.�fe9q������ٷ��đx94�sb��đ({���o�l�����/�T���z�:f��J_��tf��P���8�;�=��11��`�.�왇:d�D��deT�C�G��;�i6[ءN�#��L��2��zİlq��c��|I��M&^���nm������\���}�<$����������q�c����6�a�*O�#1s�f.�j9�1�}v��Fq�y���E��\7�I9�}(���5uP��/T� ��}Q~����υ�
}TNԽ�z�=�5)����{Q���7�cC_Htߑ����>���|v���O��'���L��&����'��y;�5�2�3o�#B.����>��/��mێ�:�&��.�ɚC}�	���1>6q�_/�j�'u�Mlz�u��M��ԛ��
�u�y�Co�{m�&�î��̛�_)�P�[c�����Q�Ч�ĥw���6=թ5����[��#kL}1_��Ų�EYc�n�RȴFY�Vk�n�����J����CW3E��J�o�:��r^P��~����bK?�Va�j�Ћν�] �%��R	޿_�ɒ�^�    vw
顎�3i��^�6�҅gՙIz��Pj�n+��ꌱ;X.�1.�{sp���^ؗ�֙���ˠ��6�x`�1�]���@��dhl�y�www�5�Ygf� �cD�Fs34� ��e$�:�Θ��_���Jyt���è��w���L��50(nW����/�<{u�߇:����aJk5�Lxr��W�\��O�3F��.�����֙td�8���2YRmow��C�Vg�}1c78�@&JbՋw��j�c�L��\�.�[�%�����K�v�_J`gL~q�����l�Q�:����g���u���[�=_�M�PZ֙tf���%%f�1�lh�"Բ���O�ç�u��Ջ����c_��� ���V�d�e�1�}sf�X�e�1�_q-8��$3a�3�n�ԇeN'6�S�{�r�'�B$3�p	Dm�4����D��@�ޭ��w;"��·_�Q��djƼ�JKK����1�S��g��S��ގ��uƚ
���ԡ�p�qT��B&�m�ک�ʌq�!�)t*�2c�71�v_��C_�/���d!Q���û�1�v*�2��J�j��Le����.<�d�$��U(�ŅS1��^ �]Pl�O�Pef޽��M;��*3A����7��B��L�{�6����ʤɄ���Whv0�cf�}���ړ3�Yc_�m�\�l*3c�;?¬��4I69�v�-�2	�m(�t�h131s�7I��N334��]�\&��#_��y�;��D�+^O�}K�Jb�0�3���eg4�����vl�ݴD3c�;d�u�YS^���.S���@^8#�.�)�8��wϐ�]�ɞ�>@"���y决�br$jށB�,�s]�J����T�f&X��v��&��1�B���l�#�i',�d��X�����,��n����ҹɄ2U�I����r�̘����`[OMmb��m���ީ�M��P�6D+.�)�?��P�m�lb��v�/Üv,�I������Oњ�����%�B5	��iP0��L��[���2aÆ���fjJ��v�xV���a\Lv��W-�y��$漘b�}�
�$J^�6��mB+D�8�[�rS��4�����=�I�u�����=�I�u��q����L{���x��~�&P=��6Һ.�WP͸�]	ם��{0�3*��$2�}�`\�OA�L���������N�2��������?�Ͱx_�\g����ŐeKK���/��r%^o��I��(�
��V��{b\e\v�b����0	��$���}Q�LLs��C��Ɣ�an��+F1_b���5�}�b��(Wl�q;ōs%i����Սs%��(�@�>�3�r�0w�o�}��+i���'�\�q̕��yɿ)1ʕN��B!�(	ǝb>�\F�D�b����@�{2Wb�!nH�y̖ī�]Eϻe]�&'�+�ϵ`��ϕ+�P<����t�s�J���w�>�D�r�5�Q�\	ǄIZ��mrsCϣ���ae�'ʓ�Jl�d��q?�9S\h������8�}b�ۡy�Q�D��?t�=�{��[pV���o �+渠�P��N�ȸ2���٘�O�+渟���`W���v{�e����ܵ>u���ظb�+V������q%�ǣe�
�W�rE����1�	��?�<7Q�o�{��#������p����b���>�����k�1�qq��@�����[ �{&̔�u4�X�*�)o9���]󰸲lXᛝ\xV\1���*�g����.lK��}�<-�D�{/Un�5yX\1�[z\��O��ŕ�[\n����ȸb�;�tj�</����5���o�8&KLp����b�+	�=R���ɢ�J�b��"�$������m�X�_	����pV�W�n�y��Y;/���R�n��ܬ����-6Ÿ��"]�V��m�����g�%�֮-���qL���Z�w��#0��+ƸoZ��s	k�+Ƹ�����5��X2�u���}sX�_�<��ی��oʒ�J��5y&e�W�E��p�{��	f�%���
��01���g�ib�b�+�|��ՔqL��"�DBn�>V+Ƹ8���go�8�L�q���5�gS&������l��g�d���Nt�it�Z1��į�_G��!\��{ۭ#4�ZY�5ic��.V+&�o�ly4&��������G�hc�b��D��NﴱZ�!ù�*��'�d���P�s�,m�V�q1B��h%m�V�q�X�J�X�ċ�sʮz��je�Ӻ�f�r�Z1ǅ�'�5m�%Ƹ�:�I���o�������4�`�`\�T>%OFMF�m?�f���-��T�S�o����1��^3Ds?�0.��>� ?���8�؝x1�%�B+�#���O�}^e\�,�j�-NP�U;A��N���8&K�pvvR�U;淃�b����PX��i̔��J���]
dS%&����f�*1����?{�V�U;���D\�$��k��#~�	�ݢ��؟0���;�?��1=bn��"�M�����X���b e[�Ӟj�����-�d+����1G2���L��^��;�ؽ��Ţ&�.~'�?#N��-����2d3%�6�U+ۮs�;�Z8w&�[�B���{XDMV;�;�rݰ�@1�ɒ����M�<�*1�}��9>��kw�JM*��2KbV��B4[7,D�;i��o�=�ڝ�Z���q�����J.7O�9�N8��k��!��1�m��*
!�N��xhۗ��v'~�Mt�AB�C�~%I�1M?]�ѸM�-�%���e��v')棘%��B�\��)���x��'I@�k�KΊ��hwh���5%��1�EC��G}��Z���G����,��Z�Ϣ#��m��T2���w�w�T�%i��|�����ɤ8���.��OLha������ĄV�W����<�?I˴u�U�X�%�
�CL1U���;f� ��`U���Ji�cF+�[���8�KBiQ[^�Z��������J�ìJh�T{U���RZ��vф���d�Y-.����qL�����z|��d�i�4�o�Y���Z�׊j�gӦ���Ǽ�3a���/T��Q���0��n��v?A�M����>����Kk";��.1�}� Wolr��~"�}�Z�bl�*�Ŏqm-�z;�j���um���̘�Mw�Bd1hUb���)`9��ש�~"����9�v?!�Un�T7�j��Z��>���c�$z�%�R�]��v?��mWn[l���~� ֲ��f�+1����g��8�J�jv�n�P��Dk��V�._0���Z�[�?���'��d���ة�~Lj�~��1�q̕�H��������ǨV���'WԞ?F�8�ܫt�Sm�cV�<����m�jA�@�܊���1�E���5q̗�Ja_ӿ���o��Dq�,����w�'f��,-��n���Il>�|��ʸ��zNw��*yƉq-N6��>�3.�����NW�B�$
�����Mƥ�"q�3Q�Q���α�Iu�I��5\��6�d�İ��ZF����z���:P�qbb+1�I���g��؞���e�����'�y'
w��f���v�nl�;�&���G��gw��!��-&	�i>v��jS�����qK�`�e�8aBU�X(<���}P��CZ�~�����۽��js��'Nf�P��K[��Bŉ��t?ߓ��[��]�j�.�k{��C�It����̗�=���֧��7�
��uG���.���P��;��󳧠��$J���NUI(NLqQ��4�'Nq�yY}܇1_rխ��l�P?qb�����I�ĉI.�*v���8�wB��$n�-	�]rm�>6˨�81��<I˛��O��;Zyvuԅ򉓸'���S����4g���y3���\���#a�$^�"��v (�81�EjIL�4I@�n,n��v��(��oggj'Nf�+�T{�W҉��\���v��1O��h�fb�i�i%�81��{�y�ͤ��'�f���Ai'N�Ejq␪�'��c�J}��1ab�;�B�E]I'N�r�v��6���\Tˊ[��n�$R[ܨ    �gW��81��- D�`Ī'Ƹwf��b��81��������L������[2I�����I����[f��}K�H�m��y�[2K���n�H%�8������t#�<��R�௩���=q��P�q��4A�86Uv9�#n[�༩�ˉ�-��h!R]�<����h4�zSe��'�ύ��8�1�őd{�Se�ܾ�`ޠ��.'�R�nN��T��$�	�s��f��b~��Ɵ{h&�ْ���=*�{2_��D��%�%i��D�n�N��nA��e��b|+�Dtv7{���.'Q��k�p59��V쁏#�MU\N����Ӧ%�-'��}�4tه�rˉ�Iq�2qL���6�|h��rR�-ނ�lV��r�����qV[No�����&3%��N�AX&�T��$v	W.�%%Qٖ�O�MKVZNLn�l �k�J�IpQ����T��
[�C;�k-���-��U��Z��N{�p�oϷ�����
ӥ{��60����ֿ��/t���0[������w��.�c����j߄o���'�a����-�vhe��÷���Mx>wmn~�-lZ���K���3Ybh]�#��4&K�p��b��$C����b�@;�%Q�~����c���V|����H0ablx e�h��c[X^â�QfL�m�d]�~�`�$޷�[M��?�21�m����} �&涟.��5L����N�V=�Xma���ŽH���t:kh�i̖����i�]c���
�3���c�0�-1��M�����c������h�-�Y貺����M&K�l1����8&K�lqچ��K��t��-�&𺁘L��(����{�L�Dy��py�r;�0����*�rqL���V
���'�%��^?�:L�Km��e;�hcq��
�B@�V"�X�a�� ����b���m������b�����YY���b�$>	���Zjb1an��w��/&Lb��¬[7�%f�R����c�$>	ذ���ag���C�������[l��s�m�7�%�J�����s3]2�[��U�ƺ{�Y��������[������L����c��En&K�n�~�0��|��-�
��]�k,��C�Ϫ��N7�%�q�>��\�%&�r/��̺��3\������;L���/J�O�0[�g�3
v��%f�X3߿���fK�p��g��(�J�p�]ٗ�#s%f�rf�={�J�p�n�n՛;��J�p1�K[3v,��'��r �N��X��OLqѸ9m�������%,���cy�?9ǕP��1[b��׳��X��Ob����[sϓ�s\��p���|�9��C[b:h�/1��ng��5Y��O�q��}s���ӟ��ʎ�:��8�K�q���T�\
�%����-�c��?1��e��o�_�lIz��ts��\I�oA2p��ɒ(q��L!���2_��丸m�%�����>���\��3�����Y.���w��L��4AL��M��DI�u��n�&۟�޽�1�i�%۟������wd���%@�.�vA���$���s��} �$1L��ʰ�Je�$(w��C��#۟�0a����s)/��'1�}_9���>��\�y.n᰿��E�[.�_��ɨ�S�������X�¢=�me�We{����vo����2������}HtKLt��F.n3.>t���|��l/�oجe5�2�K�t�=�؋j�Wf{I����ti�h�(�nI�6&�xxg��D�[�z�+��l����kB��嫚@fL�t��#�yq���<��xItKLt��A��R�H��HW�Ë0�}SfL�t��.�4^��%f����W���#���n� �-1�E����#�-1���ӟpx�����Y�њd�$TWЄ=�R�S]$��6��0	Օ&�=LfK� ����N�[b�|���q�%�rg��ۗ��D�+�n�qn�q�����ͪ���B���K��b�%qnIp���E �-���5��V�sK�sQF�k��(�Is���AsO����/�"grτ��8)4�v�%�-1������綸�%��������oNwE�B�[b�{��Q��M�[����X�ޣB�C�vYG�!n�!.v(��	���W�W��ە��N|X�k��8�J"��a�x[�qK�q�} ��G�[��e-��%�G��xr�s�~�w�&�-����p�
)nI�n�o�?wiF�[b�[��)��c��N��U;�������y��$��W�d￶B�[b�+}�Ϲ���Wl���#�-�}�w���I�[�[�+��%�%mZ�Y�η��%��ˈ�%��%���!oJw�'�-1�}3�p����
d��*�mI4�8���)RIoKLo��:��Ǖ�����_�sWf���%�Q��/�V��[x��^)��ES��
��.��8�E���x�_[m2�~[�E����B\VK��:��1�'�w���4�I��Ye���XI��2#S�C���I�21�z_��~�:�E)��)D�' ��هB�\�r�a
)sI��0ʓ+2�US��lo�;�lH�ⲩ��.sO�Þ�q���m�q /.1�����5gc>�}�����~�����]7d�%��ʞ��q�7�z�p%Y�+�P�����-�-;'����bN�S�2u`j��J�ٹ���L��L���iͱ�}B\|j.���M2r�`�w�����%�_�F��
X�#%7�1��{+ݵ�c�)�qMz�U�$�ۤ!8�	�}W#�����D���)Lq�������驮�ٕ�&�܎�o��ɨ�3L��n�&5�1�=�U�=�͠ă�+X�ܳ<̀��ٞM�>�q1��W�dQsW2��sѦK\ߟmW$B�c\,�8O�gCl\c��e7��Dn\e��p�I���ɍk�qŰ���dfS&��2_LńI�p�ttO�\�t���]��k��l��g�=D�5qý���Gd\c�ۯ����P�ݚt1�r�޲����5�b��9@�׭1�Ş�L��j�[c�����K��<}��l�a��ւfS0�V�&~�U�h1w���֤�`�%�b��¸�bG�[�3\p�7�j�[S�{5�6øխI�2?�E�T�ӭ����96�ْ�/â��p����E�1Ǖ��K���8�KLr�Yw�g��"ט�|�"V�6ϸG�1�xk���&�&!��ɹjm;��(����D���vO�;�P]t��T膃��S�u7���]�1�EA
F�-�v�&�
��T���a$\���f9���}oM�.�E�(�M 溒9b~n�����%�\��Um|�����OT�ޤ�Y�]������MD�ϭ5M���5ƻ"P[�l��kBw�۲��ڿ�l{�-]W����p��ςm�}����.J���4q��eVg�o,���1�����؁W{؄�@��3�z�x-�{��ۄQ;��k�	���¡7�����v�X�.�].N.������&����$���`�Ġ�6���11��b�t�ƌ�Y��B�۟��M<s�&��t����&��-�I��R�Pcދ2�g�&KC�f��in�_�v��V܋��(]����ƸW�{LFҸׄ2mb��#��f�,���V3�;�N�SQc�'��<�]��&�W�	�
LD��/z�I�i��I�/Nv�{�&a� j"�-W{��-&u5���� �þBԄ��{?��/ʴ��/�әΦ`�{m5��>��}�%F��=��\�@fK"���v���&�w��v��B��V����/.�钵;�����!j}߼���v�<*"jB~eW��t�[�)�������&�&�w���6Ȕ������)����_9�ֵp��m5���$�k.M)��1��r=�7����_�.Vu�iC]mk	�-���;�P��;e��[Ө�h�!C�;��P�b�V�>hk�2�ݶ�2t�)�F2C�mk1�E[��b�ꔡ������%f6v�t30.�޾�TE����4���E����_lE����h�%��&�<��x\���{��b梘d1�]�D�0�
ȵ�.h    Ъ��9�k1��_�
ȵ��{�~3,��6)��i*�b�+���-��q-qb���%�+&�(a�f8�k�fW����c���w_���ء#�k�f�vGwF�SḖ�Οn9S��#��Ͱ#N�b�+w����
ĵD��@�9�LE�Z��`���f�T$����*8��憁钴@�c����p-i���ǻB��T���W�j2�k1�݀��s�bp-�&�H��<�K�|!];�b��1cb��)n���w-A�Ws-~}6���Z�߅.L�<�72gb���}㢩�]K���{�&�ԮŴW8�'�8&K{�"�����̗L��oo;��Z�bػ?��n� �k���ȅ�����Űv4�u*��b̋�_��Z�x�e86�9^���]��|-�eKC�������|�N�}��$�k1���W��$�k1���7���D����j��%I/�Gt�6'��Z�M���C�T���l���w��L��aK� �w�"|-f�H0\�u�;	_K��r��?�x��\w����B{-Q�bm�bpe�)1�=Ռ۠쵘��N��]���뵘�bYF��yL�����.vv �k	�}O�h��A�H�e��"<����^Klt�̲����Z�u����c�)�^���n�k�5���굄ꢛ�����'�^��n+�� v�D���nm�RM��ZLt�9�m�*��~V��g�B��b����������n�X��TH�% �}��w�}���Z�r�t�z�q;Ur��8�~���=��Z⤋�A_��0�k1��L���O$�k�j�b�!S&q�W.5]��͵Ą�6Q��i��p��H��O���&�ß���5�&�M �?鉆τ����V��辡r�G�b�;>��R���B��ϭI���0wݢtu��"s�����#��vyWh.F��<��l�8^b�۠¶�rjP�\܄�wf���@YLr� �U}�Fe1��$z�>5xIo�G��vxx}��(V;�+SA��[�^�=71�QO$�"�u@�w�zr/Dv�����wq����l�z�q��ȱ���T��m��TO��A��U�uK?%��5��S��-����*��v?��Q�?���v���~�#��C��bz$�P�V���1��r��6��C[Ht��	�}�U��#��B�Ƙ�3qh����%���'-�>�B�}�D�=iy���j��r{�1���ϸHL���b���ށ���'�v���$Ƶhtv��I
���$�	N�<���J���_��y�	���惘1��<�g�!-�(i�$&�쉽f`l�ㆍ �ǘ��ь]Ӕ�SOZ���VԮ��'�v�zf��O����@�w�]� ��:�!�x��'2]bT��d�{`�F��z������OH@{i���![IP>O=������l�zLj�\iv�d�1-��P�s�g�)m���UԔ�SO�re���9�g�	��͋�3��S�	-
��E�@�cH�ӿܖ�/;	hO=PRo����;6����ݩ'�vI�+�'D�=qX�R��e��cL+\��	��c�Ĥ�I�mq��dX�;�D�[>3�MjBО�p�GW��>�)�%��~w���5I�3����\��x�iϳ���u�<�'Ķ���>��6ў �%����2��S����m��Kb���=����tfN��сq��U ���LԸ`#P���s)-CO�G��j�M(ߎ�����W#!�� �������Hȭ�̇�QL ߌ�ܞ;ݛS�R������:S=���N$Р���~���'�vcyl���{�ĝw�0����-|�l��h�e�-	��FІ�-%g�I�3\�k���R��c[t0���Rr�3��H[$�$�%1��V<������'��a6��>>Q߶r���}j#[��{�i��P���-���O\�&���'���������v��O|sE����S����|"�}佳�~)AC��-
_�!q������ɉ�>�Y�Hp�_(d���`�$�&�jkeK�z�l����T��3n�&��ښ���{qض+�ښ���,\�c_�A���Y��ܧ}6j���[�/�M�R��ۻW��$�>ƶ"�=�~�R���/�/ �t��V���.���w)��Hz��k�.rE��ƥ�D���qX��(�3e)��H�-�ҿ���Pp�>bz�[�e���R���[��a�Z�����������!���F�g�kU;~C�{lց�e���;�#\9������[���vv�˸�1�E������Rr�C\i9Ԝ��Rr�s\)Ay�ߥ�#F��a�&��<�i.��Ti`�'1͕S\��Lɚ����NH���_��ZK���:�m���aӅB���&&v�2	����S�	d�$D��J��1jF����֮��1[b�{�r��m��51؅��_žE���µLq�=�#Q�K�OΰC��H�sѣ����S20.�p�7�9�1��5��W��O���\�P10]�~�h���f`Ĉ�v�Xv�	�}�H�KK��8�IK�~��nΦb`�|�\����KiFLx��Ǻd�j�������j�{r�����h���5��A�����ع��yFwgpD�����)���\JFwן�c�$p�~�x��҄�]i��#�	#q�=�u�ͷ&�L�����!�J1����t�}3$1Xx�,�e��%���b��0.)ǭ��&�D��t�7�Y�	#����Ky��%&�oj��FT(�����|��(�D��>��1e�����1�2g�Vh���/�R*���~��_J�0�+:�㢘21ԭ�U���+�D�����=H)��H����ݖ�,��>�;�v�K�tqW�z���T#F��q���N�J�0b�+�3SP[
ɏ������܊ȏ��푪�cj�@W|x�뼶��w^5���ފƏ�ڟ|w�8&g�t��P���3����X�����@ۊǏ�ʩ�HS-�쌹n���Kwߓٙiqq x���VH~$h��L���e����»��7���m��G�Ž�M�`H�GvE�ۥ祉���\}���i�#���G7��#���U����'�
���vԉ�GLt����T!Z1��������δ���>[A��8�B�*uB��!Ƹb�����V@}�Wn�V�O�D}$��{۲�}�VD}$���U����4I�(8m3$&� dh�8�g1Gb�ۮ7Ev��6f�
�ߴߒ~��?-C��3�ݖp�g�o�����nߊ��Dw[�7��������E�63��g"�ݷ��+��gb��N0\�=����04����l!�	�ş��ְߔ ~&К<�ѷ�3�E�$սB��3���}8;���3Ƹp ��4qL����k���C\\?-ER��1_1�$�]%	�g��l����.�0_b�+�����[q��X�{���	�g�q���d�o�0	�mW�]&&�r��V�~&f
��;��&���4��sϓ�~& nɨG�5��L@��8m����~&�\x^�eV;�����Pa`}X��/1�=X��}��g�p�i���HD�3���n� r����7%룷��1�}��]E�}���3��;,�@��$Q��ɕN���3qͅA���n����3i~&]|�p4}(�$3Q�������3����y۟Hj?�\Ģ�h�c��3邆��c�:���[\�
��%F�RLl�Ez+�?z�>�x��t��1���|���ZΆ�3�P���3�}��CI.<�9)N����O{�ᾥz*������A��SO%�����Ye�q|���>F��G��䋔8������w(�;�~��%-���g�I�}.d3F�R�o�>��8fKLpqF�1�c��n���/���8�O�#B�?�o�=&�@�O���}�8�1���C�6���X�_��Dh�0�{���ι<�zLo���b���@����]'�1��i��a	�����Ʀ�aLS%����=r��[3��c�������R��<�I�nq%wq�l�o`��m�o���y��nk��:0��l �eo    �^gs��g��q�|�6��2�O��nt�q���21ŕn'�A4q|�bp�o3�?7�o_��
�x��[�8�v1�Av1qj����w�uqL���B�ц��M�%ƶoŝoS%f��{��Ś�8fKBm�=�6;zj���-z	J�y�y̖�=a�7x�6=��p[\��}�.	��=�)�2_1�]�}�4���G�T��D\��m����1_b��=����:��B
�o�|I�����Oc�$ ��ڱ�k5S'N��t�;�ۙ�@�:o�� w� w���� �I"�}��?;d$�3&�k���ߝ�oS$f���6�-�0HnWj~;�Nf���yퟝ��lW�lغ��ve�[�t;�خ������B���͠�8גl�aLrMi;�HѮѾ#u���+�'��
Ϯ���h�L<�b<+w��;�T�gW�g�l�ˋ�u̎��,�pwC@B���f� A���L��2aH��Zܓa�$:�v���.$�+&�[nۗ�tv�t:q��ή��*����8�Kg�y[N��>L���ٱ.&:�{2]b8[E�rl�ή΢��ld��c��p58l�c�$p���v�6��J�,,!6ɈgWfvۤ�Hw���:��rǄ�!-����ΚĴ+�pv���<&Lj��5QL��b��_��c�$��d�5m��Ү��v\+!��%�]������=J&K�h��q�-\D�+F�"ʅ2��-D�+���Ge�>�Z<��&s�8�Jb��WZ�ϓ�vŌvݶ_u�d!�]��B���8fK"��bmS�}g)�^1�]��U���p���ٔ+a���8�K����.}�/1����̝x(�^1��E�o��{e:�G������2[bD2?����lI�n�����c�$��ZlP�MO
�Wfx�� ���Js����j���8�KLi������c�$�	�
��NSн�-����K�GA�J��b��w��Ɣ�)��}zG�q
�Wi�x<�(fK��l
E+��5Wi��j߅� ���<�[��+f�X��n���b�+��b�v�&�)ַ�4D�|�dj&�	Mm|�������K�\���g���5��V"��w��vЉ�V�iOGm��������>b�+��q�c\��-*c�>R��Z��e�����oŴ���<6���VҼ����ݷd�Ĭ���W��F1]��������8ߊ9-���1�&9�J�w��I"������ѾC��?7`�{+�� ��1;:�m�8b�m�1���J���Mѽ��}���쨺�ec6�6�q<6N=ʬ>�Ū$��D�u+���dH׳l"���7�7��2�H�<9�7䓌���ؔR���s([p�zZ��츮z��Q�SY�i,v���S�rڜ�`���8fIҜ��r��F1GY��\{�OE��1��b��e�o�7Kv�g�rs_r0(�3����#��x'��*���Š���]���m�6���DV��~�aT6��*��:���!�4K)n��Ք�cP+=�o7�o\e\|�PZg��Ơ̲j�b���Q-�������1IbT�V"�ߴo���Z�w\҇��1M�vdC��bj5]U]v�i���.��c�}�%�]_v�i�a^m����s�z�<<�y���R)E�� �/;ƴWY2\%���J-��+����	S%Q�n�����"̎!��-�%�}�$q�E��7v�W���3�oK�=Y��1�/.�}]Y��	����=pvU�ى�v�d��g1O	m�n�{{X�ى�V��u�%�(��Clb��c<������1U>{DWl�}bv�}б�r��avLgE��;;��c:�M��G���0;f�X
����o�$&��QХ���B�)I2��g��]�av҃lb5�_�i��������*�Nĳ���<�*�N�m�{�ƽ�����OM��,�������=6�X��1�����'�X��1��+M���U�e'P�����z������_�g���c�$޶;�e3��CY���6ܧ1Sk�"�AL��Զ_�Z����N��N	�Ә&	���l�6����Ƣ�S��ѿqL����&pQL�Ţ�,"C; ���ł��(�,Y{�1��,i�K/�^vby��44>��Y�T��e�$��8��{��ˎ�,�=�<�d,��	8���g�����t�ea���#Ya�M,!L3&���)wa�e�P�1�4�N����J���_d�e�PV���m�c��Dq��1,��L?�]X�@&L���?K,K/;Q϶{ЍK/;���Z�MO�^vbg��-�R���N��9��6�^vLd��������1Sb"�c�4.3饳c,ɂK��=�7�������ɚV:;�>cQ4Sp~z�����ص��9�tvLhŋ�NJ�g'��w1�\u6ȁOZ�m�)����g�xV���1A�����#��A��΂;���ܗ�'���+�}�<Q�n)HU�b�gǐ'o���8�1��l5ݛ��pvLi��'
Q������&>;A�p��v��[T�SZi�Q���8�J�w������o����`�n�vǩ����>8�n�i�Qy�nU�S���Bt+��Gu�7i�I,k���¸8M���2��#Z��s��|����Q�Q�@�����z���ާ9ܧ1A��c�X���b�Ĉ���+7]��9��v��i��$1�ŰC�z�Z�	���1��Dټ'^=1�����b����$��)�oۇO�zb4�e~�6�ّ����n;�����TZ��S�{N�fWPFQ}{N�fŞl|;Q}�#1�m�����#c=1���_�"��Y�4}�A��9�I�='�y�|�,1��R�i���sb<�}~�}�Ĭ'��"ܬR,5qL��.$ِD3qL��� ;Qv�{�L�Ġ�\�Rs��tI���v~ i=�~��B�Vel��[O�h�e�&v��[Oi����iݪ_�I<K�LP�ZO�}��=έ<��'f�(���u���1[J[o�����'���:�-n�$n=	�=��7��I����p���8ד�g��t�֓�����~��1mm��}��'�8���.��C���5�����ٻ���%s%�ͦ���[Oi�D�Uq�B�zo��q	v��YSZQ!۪[!l=1�}�l�M%j=1����\)�0fHBhߝڦ7����'�����#v&�iCZ�q=ލ6a�I}���.$�'A������,D�zWZ���%�$��b�ۿV��8&ILgq	n�F^5���$t��{;�\?Jqv�,���'���Jrv�lG�s���']ƞO��c�Ԉ��t��?�[r�����U�{���������q���Ah'硤g'��=B�Q'4�a(�ى�,V�L�|`(�ى	��V��7�x�DN�hő�q���h'A�oJ���}S&MLiA���&��"1���D�%��"	�}�n��güI��韦&�����Hw��l�*B��ڍ��w5�
�$��ݓ��b��A-�?�����*��Zlq~.6���Q�{{�A�+ܑ��"aT��Ӯ*�3Zu����Z\F+�.Xfu��aO`��l��Gr�cX;�j˛C��N�n"�5J\v�Z���}3%&���\�)���i�=�m�P��Z�Q_Yv��Ȍj�_�P̎��1�ŞGȟ��K�w��/H�ؔ��.�n���lH\�a\^0���Π������q��/�~p�wÀ�zJeW�u7�w���/�����������ΨX19����6�2�z�E�����~p��Al���MZ�.�	�yբ���w�}o;s&��� �scȜ�	n�N����LBp�?m�[֋����!�|D��ܪ2�4�_����큵] S&���'��jG0k��cr�|ڹi0gb�+���y��3.���-Ml�%��XU`	5ܗd��0W^�O��\I���e�M��T�Q.`���y��1S����tQ̓�a����+�Is\���j�}s$sA�rG�m�P�&1��^�4wcuPoƭAp>jN:6(9���o]�ϫ���1Sb��A��s%!�]��٨�TIe�8���-�J�q    �dwv���3���|��|�.1��~`g�6(:��ৈ"�]R/fK�qQ�>��gGo1[b�+wH���lI�j���+��3n~����~�%!��xư�-	�mr��^��qg�S,a���f��{jq(���f�� �a�o��+)��s���A�7���r�C�f���B�X�]%7S%i8�Ē��5�*����Sh�����\I`�}xqd�0[�wؤ��r�.���Y�k����0_b�[d���0[b��۲�r��A��n �x�T���%�@��'�[2]b�+�Ɛ;��;L���H�U�tI�.�2�燷�K�u��OG1ػ�����m��}��9���4�HS����u��^�7��I�˽��{[��T4�u�7��#��$����Rͬ`�\�[�/���j��y̳Ĳ��;Z͆1�b�+�i�훉c�%��r�n�F1�b���-�Η�D�;_r ���ְO�0Y�i���,�趙?�8&KLtjVsj�rk𻿪^�8	f�5p��;�6�ԅ�]�_v�1�Q����[1'�,w�[�^\Q~�rg�@�
��*G<i2�]D��\9�	�}��t�љ$�����K�U�k^9��ܶ�\���>�	���5�|l�U�J�r���#$MS%f���;�]�I�ˍ��^Գ.5+�%�8&P��j̕��`qDc��gnxȺ�1c�vcrf�6��%i86�!��li̖��ظ�q�1oL�Ķ��Џ��:�J�q۽sU����\)1�}�$X�����qYw�O��>�0.��/�iG� �� :��j�Sqܒ��d�Bܒ�CI�t�nI���7�s1n�1���=a��������.�b.p20�9��p�6��S\�:]�`*�[�։�0[b���W?��"�#\�pŜ� 	nId�K�ۧB�[b�+�joǄI ��8�3�
��ྛ�
H7���b�����T��^	�z-O�pK�pk�L�b�%�Jxw;�1Sb������ے�[܅)�i*|[b|{pG�َnI\d�>�9�[���w�T�����n�}C�G�n�]B���ݖ��N��� �-1��x��7�mI�kaoC�1��.�>"ے(o?w��l@d[bd�f{���1=bd+M��?k;0��J��YVn��v�Ff[��r�`n�#�-��V�kn[Jb[Rw��iS������ȑ�{*̒؎[��S�[��6��%浒_�~C���Z�]�^���Zi������c�$mŚs��I�\K� B�>����ػ?�N7%�JsL�:��T�~�9���a�[Kb��]�m^���Ҏ�Y�/�D�%f��"��.&���n���ϒ��$F���ƛ��Ą�^��d�%&�0'}qH���$������d�Č����n���Ā�W�b~��%��,������"��b��hV
�E躉c��hc6��RM�%A�K�U�����QC����$��쐶��1Cb*�eq�o�IԶ���5L^%Ʋ��𲟆�O�%沐��M�=��:�(�;m�o�%��\a5=�r�Ғim��<��c��\�к�ga�$��� �m�<f�����NQ+&��4�08ډ�v�*�{�%�A�]�i��X,��$e�E^�%���,�e�7zG�il{��~��� ���� 6���*�(�}�fWܯbf�V�5�������k�Ny��;J�_��Q��yG�٫���i��17�*7��}d�%f�SV&�Q̌Ēv]Ņ�0�G^�t������=Y�ɫ��$^R����(CW(��Ų�(��
�e8[H���;j]�y���k�oPV�Y�j�'o���ǣ)���U�%�)���mi�l��y�,����&��}T�w���)/�/y�"降�A�W��E�7.,	�vð�u휧�@k�[���b���;j�[�mkb(o��O#;�ğ5ƭ�%nM&/���V�7l����L�����႘$	h��
8�;2Gb̊S�D3�Y"��1g�b{��'���T�[c�ɻ��&��+������wGM����gS���ִsح�Y� �&�ÖXq�Oc�ĸ��]d�!hM����ɓ�wG�y�����y}w�D/+r{�����__g�%1��u�a�ـ,�.s�s�eg
fQ��maY�"i,io'�i�J*5p}N׆k~&̕�����|�;�P���c��V�Pi����n4}�B�a�Ӊ_�~Q�;�q=x?xh�2�ށ,Tj�
I�[U�e�����Ë�%!����Bܙ�s�q�U��Jp���7��w%3�8O�J�`�b��ϕ��ĢR3X,P���A�Q�,��3=�sI8*5���Bu�q�c��$�%��e�!�IS�X���ɐ�:	U���������O�T&M�c�}�I�u�J�d���mZw�Je�g�>���M�*5�=]��6�ī�`ٹ���&^�˚O�񙐭J�d�+��	�I�*Mݬ���L5��I�*5��*y�Ǆi���T0E1W3��k�/}L2�Ui���|��/ī�t;���2q"V������4���h��l�-:R��&�X���<NLB�*���L:��EZ�����s�,�� �ǅ-i �� ��"n�25�E�b�^��0c��Y����RCZ�_�������P���^B�*�U�P�1��Ҕ��U�ۻ��@�K�h�y���-�_->"��%���0�n0Oz(̕�>������J�hum��s��V�Z��C+.���VjT��y�%��R�Z�6���*BZ+�i-\�\��>��V�
�o��3>S2[iZ��[�{��V+�u�"Bt+m-�Ҩջ��Vz;�A6�
�4F׷"(�1ej~kKm�.!Õ���0d��%d���~[q�%��J�q���c!ƕ�b���J��J�qW��r\�9�7�9���b\i���&�	)���*�8]q��q�ḷ�A�+���R� �HS:��'�+���8 8hN�<b\�1�{g�,	��jS6;�s�+@!��Ʊ�Y����R\�).�>&���%���Pܵ!��6!�՚�x�|�q�j���Q�jZ)�V'=��A5o�Ӏ��[m�j��B��;M�ķ�vs�n)�i���U��"����������\�	.H:��(�"\m�ko7���p �Ն���j�4���j�q����B\m��oUo�fB\�!��q��.ǽ��=N(��ڸמK]�W����K��c�����r�>���ZS\,����A'�����}��ơDLO�\�A��j=���%ĸ�T�N?L�:!���bM+�&R�q��<p[��0�*Mk1`N ���r\�9.�nP���?y�6�o>��Q<��X�9.`�8�(��X�8�����jSA;�:�8�$�ZC������B���nEқN�5��e�e��m�۲�����"�զŘᣐ6٤������)d��@ΐ_X��-��?Q̒����/�0Ejd�FT1�u������g��_�c�Լo��P���U�
�ux?=d��t�o[�G��uepD�B檍c-�cI� ��ڸ,��D�\�f��㢋�$��6���&��	ݵq7 c���'g{`�{���zn2wm(�a�	5$s�k�h���I���iܵC��|�|kj>�_Ii gsӸk�m`��0�M�5���肤8�35��/���ƹ�ܵ����C�'�i�6����n
wm�jQdq�.%2Q_��\��~sӸkgц!�R�Mᮍ_�-��3�q�6�����t�ڐY�6��r�w�4Qk8�~��,M31?��ԝ�8^�
Z� ��������:S��h��~ӓ,Q�Z�6��R G����_1ed��ig����M�5������ܔ������8�5�u-�
_�&u�ƪ+)4�J�.��6e���Z�8�ZZ[=��I����Z�9��c��jSa�a��@�ј��]kB����k�CS�!�n��_�A@�5�w���d��x��)4]!��f�o����_ڍ6l��N�!dC�5�E� �օs��k�e��J'}sS�k�f��J&�*l �  j�7i��Ȍɵ���ڗ�8{��e�Ԑv,��~IbMK�w�Ӝi������S�?��tC���|���$�˞.m����
ZeW#�8�
Z�d͛K{C��{��z�ަ+>yBAkZ��e�s�_�k���;K��Nw���,f��;37I���Ć�ٹ	ڭ���K��4����@V/�=�H�j0;�����"A����S�<7Q�5P֙�HQ̑�Ⱥ`���@k����1���!�9���K��\�.�9R#Y�Q'ڗ�#����\v�� &JSX{�g-�i��Hvx��8��Z�cq����K,��^(��c>ZCb?��arC�1�_ZZdZ�<�<�$��F��؅�_J|BCk:�]k²��h��;�ė���j{-wqI[p2C�	,v*h�*�!3����
�g|�Ćִ��� �=I���c:��)}r��s2@ӥ�A��Ȕ�I���wJ3:1��$����^��IӸ\_@S��7HRO7�q̚�ƢNm�Ӛ� �Y XC�!H�F���nx��Ǭih�[�Ӕ4�j.��t�x҆�<�j0��i��q̘̺Rvܟ�D�j4�;�X�i��a~� �~�Z���hI47���t���H���&��ƃ�ϝjM禒�΃��WX�ǽ�����ҺjR:�q̘���q��`�17��՘�ˈ�\7��5&����~���`Z,ޟ�Gz*L���ο`�g�M0o]k��3�ya�ӹ)武���b����������$�"��5�ЍQ�C����+ڕ��in�y��-Nc�O�5�Ll1�k|!�]����Fp^X��M4o5��en���=�����z�;�qy�i����h,�2qjr�"B/$L��ĩ�-&��.�_��K�pS�[Mm�G����Wk��\�^���&����{�F5� �+��1n�6���e ��ү1ed�{Jm�ZkZ�M�@�yIk����j
$S󤆵~t�S��i�f���j�[#�����2��	j����f4ͺ�ր��[�
9�՜����xnz|�9-���?��"�3��U�O�b�Ԡv��G�)�Iju	s�4/�Z�j��ȏE�E��Z�k��7��&�jb+��U��M�o5����H��Ti�h��9v���$�j\�~�?K�Z�Y-4���{�M�?j\���c�n5�M��{���w\�ێ���Wk��g���~��ܭ��#%�5�uG"�B�'�k�]SS�(v���8��\hn"�Q#ܹ�L�uƵ������&������t{������~$�]u-���uJP�Qc\��� >��р��F�ba¦�M���n��X��	�GSa+�Z)���h`.6c６�7��h*l?�n�����{�+E1Sj���Ut�~ҫ@;j��~ (�M$;3ZL���'���~�4�v����͎��J:�󣩯U_|D��)�G�t����4�������p��������B)]'�qJ�!L��F��X�:3K+b��%�2XH[0b��%��Pt��NGSc���?��o��Ѷ{�r�$25�uً撮M�>j��`�MM\:j��=�,u����}�\�\K�|o���b��^t����k��Adn:�Ѹ#�:��?JGMt=��G��A騉.��:�_�@V:j����Dڀ����>�c�Bg����{�*>�i� 0Mo�7��R8;#35م@[��KH��s�pw����Ҫ��Ҡ�H��<Û��,���@��
}��@h�yW;4,��g������C.7?���rLk�k�[|�&�rhj��~��\/?7u�h�l�D{%�M_>��]nGz4ɦ����`an��Qc�����&,5兄�+iRǽ)�}�CH"`��|t�w�Zk��׌��6�Z'��a�qn�УA��
����%��n���:�˪w{YK�_�v���e���{����I��Fَ5c�B9aոW!�{�It��㎏�oFM}m����*|�|?�R��}�b���.Z�!���j�ӕ�v�H�PS��c���0�U�����W����Ӟ�]��9B5�U؂<��cumOY����*���{�C������_���n�̧cM�����c]G��/�I��%CWv<�����1���]礪����2�j|~G��\�`�C��'&��)��W@��D=�&{�6Q7T��֕��!���=|��(�[��LG����Ą����)���A̗����7Y����������      �      x������ � �      �      x������ � �      �      x������ � �      �   3   x�3�4202�50�54P00�2!=#s�?�4D�)��y6W� ,�a      �      x�3�4�2�4����� ��      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �            x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �   v   x�3�4202�50�54P00�2!=CsS�?�4D%��)��y�*F�*�*����E�n�Uy�U&�>�z��n��9F�Eơ�~U���ť.��eI��P3�fr��qqq �5 �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �   =   x�3���4(�ap�Ix@ō��\�&h�!PS4�@����chH̸=... ��"�      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x�3�4����� ]      �   T  x�u�ݎ�0���)r9s�+����KC�)M��������uJ�D�F���>��8a�����7�w�r�b�:=��� �����6}�/����. ���6DA큪�/��; 6����Ʈ�tHE�1�"� �Ԉ|'���J��p��fMe�,6�-^�=)��bQ���P�*�V$}Lg��Ik���IO�d�է偺��]cfj�F�KJ�l4uojb�K�w�F����mf*$Z&�+UQ:�F�pVjȫ5�y��Ԫݝ�ݝ���Z-mBF��[��z*�gS^f�:����<.j�5Wl�&'�?C�}W\��x�D�d�4���C���a�G��ޛ#�?���ɚ��O�9��{ՙ��Q�.C����A�}I�X�[�4З
�F���9�f�Bc�=]���j�[�͝�N��!O�a ?d�������>�U���u����"_L�:6���
�����2��%��,y�/� o��.RH<~fEޣ�.y���x�Z����=8ieyO����~�Ԩ��u���!x5/��;_�N#Qe9o�R�Q�0F�f��Y�x��=o�f�7c�{�v���C��PTP�j�����������?K�37     