package ne.mha.sinea.referentiel.recensementINS;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ne.mha.sinea.CommonProperties;
import ne.mha.sinea.referentiel.zone.Localite;

//annotation de lombok pour générer les getter et setter
@Data
//annotation de lombok pour générer le constructeur sans paramètre
@NoArgsConstructor
//annotation de lombok pour générer le constructeur avec tous les paramètres
@AllArgsConstructor
//indique qu'il s'agit d'une entité=> création d'une table au niveau de la base de données
@Entity
@Table(name = "recensement_ins")
public class RecensementINS extends CommonProperties {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer code;
	private Integer anneeRecensement;
	private Integer populationHomme;
	private Integer populationFemme;
	private Integer nombreMenage;
	private double tauxAccroissement;
	@JoinColumn(name = "code_localite", referencedColumnName = "code")
	@ManyToOne
	private Localite localite;
}
