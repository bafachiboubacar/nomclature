package ne.mha.sinea.referentiel.formationSanitaire;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import ne.mha.sinea.Template;
import ne.mha.sinea.nomenclature.typeFormationSanitaire.TypeFormationSanitaire;
import ne.mha.sinea.nomenclature.typeFormationSanitaire.TypeFormationSanitaireRepository;
import ne.mha.sinea.referentiel.zone.Localite;
import ne.mha.sinea.referentiel.zone.LocaliteRepository;

@Controller
public class FormationSanitaireController {

	@Autowired
	FormationSanitaireRepository formationSanitaireService;
	@Autowired
	TypeFormationSanitaireRepository typeFormationSanitaireService;
	@Autowired
	LocaliteRepository localiteService;

	//@PreAuthorize("hasAuthority('Ajout formationSanitaire'))
	@GetMapping("/formationSanitaire")
	public String  addFormationSanitaire(FormationSanitaireForm formationSanitaireForm, Model model) {
		try{
			//récuperation de la liste des formationSanitaire de la base de données et envoie de la liste récuperée à la vue à travers le modele
			List<FormationSanitaire> formationSanitaires =  (List<FormationSanitaire>) formationSanitaireService.findByIsDeletedFalse();
			model.addAttribute("formationSanitaires", formationSanitaires);
			List<TypeFormationSanitaire> typeFormationSanitaires =  (List<TypeFormationSanitaire>) typeFormationSanitaireService.findByIsDeletedFalse();
			model.addAttribute("typeFormationSanitaires", typeFormationSanitaires);
			List<Localite> localites =  (List<Localite>) localiteService.findByIsDeletedFalse();
			model.addAttribute("localites", localites);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Referentiel/Formation sanitaire");
			model.addAttribute("viewPath", "referentiel/formationSanitaire/formationSanitaire");
			
			}
		catch(Exception e){
				
			}
		//afficher le template avec la vue (formulaire) à l'intérieur
		return Template.defaultTemplate;

	}
	
	//validation du formulaire d'ajout formationSanitaire
	@PostMapping("/formationSanitaire")
    public String addFormationSanitaireSubmit(@Validated FormationSanitaireForm formationSanitaireForm,BindingResult bindingResult, Model model) {
    	//s'il n'ya pas d'erreur lors de la validation des données du formulaire
		if (!bindingResult.hasErrors()) {
			
			try {
				//récuperer les données saisies
				FormationSanitaire EP = new FormationSanitaire();
				
				EP.setDenomination(formationSanitaireForm.getDenomination());
				EP.setLatitude(formationSanitaireForm.getLatitude());
				EP.setLongitude(formationSanitaireForm.getLongitude());
				EP.setTypeFormationSanitaire(typeFormationSanitaireService.findByCode(formationSanitaireForm.getCodeTypeFormationSanitaire()));
				EP.setLocalite(localiteService.findByCode(formationSanitaireForm.getCodeTypeFormationSanitaire()));
				
				formationSanitaireService.save(EP);
				
				//récuperation de la liste des formationSanitaire de la base de données et envoie de la liste récuperée à la vue à travers le modele
				List<FormationSanitaire> formationSanitaires =  (List<FormationSanitaire>) formationSanitaireService.findByIsDeletedFalse();
				model.addAttribute("formationSanitaires", formationSanitaires);
				List<TypeFormationSanitaire> typeFormationSanitaires =  (List<TypeFormationSanitaire>) typeFormationSanitaireService.findByIsDeletedFalse();
				model.addAttribute("typeFormationSanitaires", typeFormationSanitaires);
				List<Localite> localites =  (List<Localite>) localiteService.findByIsDeletedFalse();
				model.addAttribute("localites", localites);
				
				//envoie de la liste récuperée à la vue à travers le modele
				model.addAttribute("formationSanitaires", formationSanitaires);
				//indiquer que l'operation d'ajout a réussie
				model.addAttribute("operationStatus", "operationStatus/success");
				model.addAttribute("horizontalMenu", "horizontalMenu");
				model.addAttribute("sidebarMenu", "configurationSidebarMenu");
				model.addAttribute("breadcrumb", "breadcrumb");
				model.addAttribute("navigationPath", "Referentiel/Formation sanitaire");
				model.addAttribute("viewPath", "referentiel/formationSanitaire/formationSanitaire");
				
				}
			catch(Exception e){
					//récuperation de la liste des formationSanitaire de la base de données et envoie de la liste récuperée à la vue à travers le modele
					List<FormationSanitaire> formationSanitaires =  (List<FormationSanitaire>) formationSanitaireService.findByIsDeletedFalse();
					model.addAttribute("formationSanitaires", formationSanitaires);
					List<TypeFormationSanitaire> typeFormationSanitaires =  (List<TypeFormationSanitaire>) typeFormationSanitaireService.findByIsDeletedFalse();
					model.addAttribute("typeFormationSanitaires", typeFormationSanitaires);
					List<Localite> localites =  (List<Localite>) localiteService.findByIsDeletedFalse();
					model.addAttribute("localites", localites);
					
					model.addAttribute("operationStatus", "operationStatus/unsuccess");
					model.addAttribute("horizontalMenu", "horizontalMenu");
					model.addAttribute("sidebarMenu", "configurationSidebarMenu");
					model.addAttribute("breadcrumb", "breadcrumb");
					model.addAttribute("navigationPath", "Referentiel/Formation sanitaire");
					model.addAttribute("viewPath", "referentiel/formationSanitaire/formationSanitaire");
					
					}
			
    	}
		else{
    		try{
    			
    			//récuperation de la liste des formationSanitaire de la base de données et envoie de la liste récuperée à la vue à travers le modele
    			List<FormationSanitaire> formationSanitaires =  (List<FormationSanitaire>) formationSanitaireService.findByIsDeletedFalse();
    			model.addAttribute("formationSanitaires", formationSanitaires);
    			List<TypeFormationSanitaire> typeFormationSanitaires =  (List<TypeFormationSanitaire>) typeFormationSanitaireService.findByIsDeletedFalse();
    			model.addAttribute("typeFormationSanitaires", typeFormationSanitaires);
    			List<Localite> localites =  (List<Localite>) localiteService.findByIsDeletedFalse();
    			model.addAttribute("localites", localites);
    			model.addAttribute("operationStatus", "operationStatus/unsuccess");
    			
				model.addAttribute("horizontalMenu", "horizontalMenu");
				model.addAttribute("sidebarMenu", "configurationSidebarMenu");
				model.addAttribute("breadcrumb", "breadcrumb");
				model.addAttribute("navigationPath", "Referentiel/Formation sanitaire");
				model.addAttribute("viewPath", "referentiel/formationSanitaire/formationSanitaire");
				
				}
    		catch(Exception e){
				
				}
    	}
		
		//afficher le template avec la vue (formulaire) à l'intérieur
		return Template.defaultTemplate;
        
    }
	
	//modification d'une formationSanitaire
	@GetMapping("/updateFormationSanitaire/{code}")
    public String updateFormationSanitaire(@PathVariable("code") int code,FormationSanitaireForm formationSanitaireForm, Model model) 
	{ 
		try
		{
			//récuperer les informations de la formationSanitaire à modifier
    		FormationSanitaire EP =   formationSanitaireService.findByCode(code);
    		//envoie des informations de la formationSanitaire à modifier à travers le modele
    		EP.setDenomination(formationSanitaireForm.getDenomination());
			EP.setLatitude(formationSanitaireForm.getLatitude());
			EP.setLongitude(formationSanitaireForm.getLongitude());
			EP.setTypeFormationSanitaire(typeFormationSanitaireService.findByCode(formationSanitaireForm.getCodeTypeFormationSanitaire()));
			EP.setLocalite(localiteService.findByCode(formationSanitaireForm.getCodeTypeFormationSanitaire()));
			
			List<FormationSanitaire> formationSanitaires =  (List<FormationSanitaire>) formationSanitaireService.findByIsDeletedFalse();
			model.addAttribute("formationSanitaires", formationSanitaires);
			List<TypeFormationSanitaire> typeFormationSanitaires =  (List<TypeFormationSanitaire>) typeFormationSanitaireService.findByIsDeletedFalse();
			model.addAttribute("typeFormationSanitaires", typeFormationSanitaires);
			List<Localite> localites =  (List<Localite>) localiteService.findByIsDeletedFalse();
			model.addAttribute("localites", localites);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Referentiel/Formation sanitaire");
			model.addAttribute("viewPath", "referentiel/formationSanitaire/formationSanitaire");
			
		}
		catch(Exception e)
		{
			
		}
		return Template.defaultTemplate;
    		
    }
	
	@PostMapping("/updateFormationSanitaire/{code}")
    public RedirectView updateFormationSanitaireSubmit(@Validated FormationSanitaireForm formationSanitaireForm,BindingResult bindingResult,@PathVariable("code") int code, Model model,RedirectAttributes redirectAttributes) 
	{
		final RedirectView redirectView = new RedirectView("/formationSanitaire", true);
    	if (!bindingResult.hasErrors()) 
    	{
			try
			{
				//recupérer les informations saisies
				FormationSanitaire EP = formationSanitaireService.findByCode(code);
				//envoie des informations de la formationSanitaire à modifier à travers le modele
	    		EP.setDenomination(formationSanitaireForm.getDenomination());
				EP.setLatitude(formationSanitaireForm.getLatitude());
				EP.setLongitude(formationSanitaireForm.getLongitude());
				EP.setTypeFormationSanitaire(typeFormationSanitaireService.findByCode(formationSanitaireForm.getCodeTypeFormationSanitaire()));
				EP.setLocalite(localiteService.findByCode(formationSanitaireForm.getCodeTypeFormationSanitaire()));
				//enregistrer les données (l'entité=) dans la base
				formationSanitaireService.save(EP);
				
				//indiquer que l'operation de modification a réussi
				redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/success");
				
			}
			catch(Exception e)
			{
				//indiquer que l'operation de modification a échoué
				redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/unsuccess");
			}
    	}
    	else
    	{
    		
    	}
		
    	//redirection vers la page d'ajout
		return redirectView;
        
    }
	
	@GetMapping("/deleteFormationSanitaire/{code}")
	public RedirectView deleteFormationSanitaire(@PathVariable("code") int code, Model model,RedirectAttributes redirectAttributes) 
	{ 
		final RedirectView redirectView = new RedirectView("/formationSanitaire", true);
		try
		{
			FormationSanitaire EP = formationSanitaireService.findByCode(code);
			EP.setDeleted(true);
			formationSanitaireService.save(EP);
			
			//indiquer que l'operation de suppression a réussi
			redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/success");
		}
		catch(Exception e)
		{
			//indiquer que l'operation de suppression a échoué
			redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/unsuccess");
		}
		
		//redirection vers la page d'ajout
		return redirectView;
	    		
	}

	
}
