package ne.mha.sinea.referentiel.zone;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
//import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import ne.mha.sinea.Template;
@Controller
public class LocaliteController {

	@Autowired
	ZoneRepository zoneService;
	@Autowired
	LocaliteRepository localiteService;
	@Autowired
	TypeLocaliteRepository typeLocaliteService;
	
	//@PreAuthorize("hasAuthority('gestion des zones')")
	@GetMapping("/localite")
	public String addLocalite(LocaliteForm localiteForm, Model model) {
		try {
			
			List<TypeLocalite> typeLocalites = typeLocaliteService.findByIsDeletedFalse();
			model.addAttribute("typeLocalites", typeLocalites);
			List<Localite> localites = localiteService.findByIsDeletedFalse();
			model.addAttribute("localites", localites);
			List<Zone> communes = zoneService.findByIsDeletedFalseAndNiveau(3);
			model.addAttribute("communes", communes);
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Referentiel/Localité");
			model.addAttribute("viewPath", "referentiel/zone/localite");
			
		} catch (Exception e) {
			System.out.println("erreur "+e);
		}
		
		return Template.defaultTemplate;

	}

	//@PreAuthorize("hasAuthority('gestion des zones')")
	@PostMapping("/localite")
	public String addLocalitesSubmit(@Validated LocaliteForm localiteForm, BindingResult bindingResult,
			Model model) {
		if (!bindingResult.hasErrors()) {
			try {
				
				Localite L = new Localite();
				L.setLibelle(localiteForm.getLibelle());
				L.setCodeINS(localiteForm.getCodeINS());
				L.setTypeLocalite(typeLocaliteService.findByCode(localiteForm.getCodeTypeLocalite()));
				L.setRattachement(localiteService.findByCode(localiteForm.getCodeLocaliteRattachement()));
				L.setCommune(zoneService.findByCode(localiteForm.getCodeCommune()));
				localiteService.save(L);
				model.addAttribute("operationStatus", "operationStatus/success");
				List<TypeLocalite> typeLocalites =  typeLocaliteService.findByIsDeletedFalse();
				model.addAttribute("typeLocalites", typeLocalites);
				List<Localite> localites =  localiteService.findByIsDeletedFalse();
				model.addAttribute("localites", localites);
				List<Zone> communes = zoneService.findByIsDeletedFalseAndNiveau(2);
				model.addAttribute("communes", communes);
				model.addAttribute("horizontalMenu", "horizontalMenu");
				model.addAttribute("sidebarMenu", "configurationSidebarMenu");
				model.addAttribute("breadcrumb", "breadcrumb");
				model.addAttribute("navigationPath", "Referentiel/Localité");
				model.addAttribute("viewPath", "referentiel/zone/localite");
				
			} catch (Exception e) {
				model.addAttribute("operationStatus", "operationStatus/unsuccess");
				List<TypeLocalite> typeLocalites =  typeLocaliteService.findByIsDeletedFalse();
				model.addAttribute("typeLocalites", typeLocalites);
				List<Localite> localites =  localiteService.findByIsDeletedFalse();
				model.addAttribute("localites", localites);
				List<Zone> communes = zoneService.findByIsDeletedFalseAndNiveau(2);
				model.addAttribute("communes", communes);
				model.addAttribute("horizontalMenu", "horizontalMenu");
				model.addAttribute("sidebarMenu", "configurationSidebarMenu");
				model.addAttribute("breadcrumb", "breadcrumb");
				model.addAttribute("navigationPath", "Referentiel/Localité");
				model.addAttribute("viewPath", "referentiel/zone/localite");
				
			}

		} else {
			try {
				model.addAttribute("operationStatus", "operationStatus/unsuccess");

				List<TypeLocalite> typeLocalites =  typeLocaliteService.findByIsDeletedFalse();
				model.addAttribute("typeLocalites", typeLocalites);
				List<Localite> localites =  localiteService.findByIsDeletedFalse();
				model.addAttribute("localites", localites);
				List<Zone> communes = zoneService.findByIsDeletedFalseAndNiveau(2);
				model.addAttribute("communes", communes);
				model.addAttribute("horizontalMenu", "horizontalMenu");
				model.addAttribute("sidebarMenu", "configurationSidebarMenu");
				model.addAttribute("breadcrumb", "breadcrumb");
				model.addAttribute("navigationPath", "Referentiel/Localité");
				model.addAttribute("viewPath", "referentiel/zone/localite");
				
			} catch (Exception e) {

			}
		}

		return Template.defaultTemplate;

	}

	//@PreAuthorize("hasAuthority('gestion des zones')")
	@GetMapping("/updateLocalite/{codeLocalite}")
	public String updateLocalite(@PathVariable("codeLocalite") int codeLocalite,
			LocaliteForm localiteForm, Model model) {
		try {
			
			Localite localite = localiteService.findByCode(codeLocalite);
			localiteForm.setLibelle(localite.getLibelle());
			localiteForm.setCodeINS(localite.getCodeINS());
			localiteForm.setCodeTypeLocalite(localite.getTypeLocalite().getCode());
			localiteForm.setCodeLocaliteRattachement(localite.getRattachement().getCode());
			localiteForm.setCodeCommune(localite.getCommune().getCode());
			List<TypeLocalite> typeLocalites =  typeLocaliteService.findByIsDeletedFalse();
			model.addAttribute("typeLocalites", typeLocalites);
			List<Localite> localites =  localiteService.findByIsDeletedFalse();
			model.addAttribute("localites", localites);
			List<Zone> communes = zoneService.findByIsDeletedFalseAndNiveau(2);
			model.addAttribute("communes", communes);
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Referentiel/Localité");
			model.addAttribute("viewPath", "referentiel/zone/updateLocalite");
			
		} catch (Exception e) {

		}
		return Template.defaultTemplate;

	}

	//@PreAuthorize("hasAuthority('gestion des zones')")
	@PostMapping("/updateLocalite/{codeLocalite}")
	public RedirectView updateLocaliteSubmit(@Validated LocaliteForm localiteForm, BindingResult bindingResult,
			@PathVariable("codeLocalite") int codeLocalite, Model model, RedirectAttributes redirectAttributes) {
		final RedirectView redirectView = new RedirectView("/localite", true);
		if (!bindingResult.hasErrors()) {
			try {
				Localite L = localiteService.findByCode(codeLocalite);
				L.setLibelle(localiteForm.getLibelle());
				L.setCodeINS(localiteForm.getCodeINS());
				L.setTypeLocalite(typeLocaliteService.findByCode(localiteForm.getCodeTypeLocalite()));
				L.setRattachement(localiteService.findByCode(localiteForm.getCodeLocaliteRattachement()));
				L.setCommune(zoneService.findByCode(localiteForm.getCodeCommune()));
				localiteService.save(L);

				redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/success");

			} catch (Exception e) {
				redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/unsuccess");
			}
		} else {

		}

		return redirectView;

	}

	//@PreAuthorize("hasAuthority('gestion des zones')")
	@GetMapping("/deleteLocalite/{codeLocalite}")
	public RedirectView deleteLocalite(@PathVariable("codeLocalite") int codeLocalite, Model model,
			RedirectAttributes redirectAttributes) {
		final RedirectView redirectView = new RedirectView("/localite", true);
		try {
			Localite L = localiteService.findByCode(codeLocalite);
			L.setDeleted(true);
			localiteService.save(L);

			redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/success");
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/unsuccess");
		}

		return redirectView;

	}

}
