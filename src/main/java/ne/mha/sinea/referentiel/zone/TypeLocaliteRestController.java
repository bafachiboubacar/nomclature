package ne.mha.sinea.referentiel.zone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class TypeLocaliteRestController {

	@Autowired
	TypeLocaliteRepository typeLocaliteService;
	
	//@PreAuthorize("hasAuthority('gestion des zones')")
	@PostMapping("/typeLocalite")
	public int addTypeLocalite(@Validated TypeLocaliteForm typeLocaliteForm,BindingResult bindingResult, Model model) {
		TypeLocalite savedTypeLocalite = new TypeLocalite();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeLocalite P = new TypeLocalite();
					P.setLibelle(typeLocaliteForm.getLibelle());
					savedTypeLocalite = typeLocaliteService.save(P);
					
				}
			catch(Exception e){
				
				
					}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeLocalite.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des zones')")
	@PostMapping("/updateTypeLocalite")
    public int updateTypeLocalite(@Validated TypeLocaliteForm typeLocaliteForm,BindingResult bindingResult, Model model) {
		TypeLocalite savedTypeLocalite = new TypeLocalite();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeLocalite P = typeLocaliteService.findByCode(typeLocaliteForm.getCode());
					P.setLibelle(typeLocaliteForm.getLibelle());
					savedTypeLocalite = typeLocaliteService.save(P);
					
				}
			catch(Exception e){
				
				
					}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeLocalite.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des zones')")
	@PostMapping("/deleteTypeLocalite")
    public int deleteTypeLocalite(@Validated TypeLocaliteForm typeLocaliteForm,BindingResult bindingResult, Model model) {
		TypeLocalite savedTypeLocalite = new TypeLocalite();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeLocalite P = typeLocaliteService.findByCode(typeLocaliteForm.getCode());
					P.setDeleted(true);
					savedTypeLocalite = typeLocaliteService.save(P);
					
				}
			catch(Exception e){
				
				
					}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeLocalite.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des zones')")
	@GetMapping("/getTypeLocalite/{code}")
	public String  listeTypeLocalite(@PathVariable("code") int code) {
		String res = "";
		try{
				
				TypeLocalite typeLocalite = typeLocaliteService.findByCode(code);
				res += ",{ id:"+typeLocalite.getCode()+", pId:0, name:\""+typeLocalite.getLibelle()+"\"}";
				res = "[{ id:0, pId:0, name:\"/\", open:true}"+res+"]";
				
			}
		catch(Exception e){
				
			}
		return res;
	}
	
	
	
}
