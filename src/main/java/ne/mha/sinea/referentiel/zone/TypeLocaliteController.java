package ne.mha.sinea.referentiel.zone;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class TypeLocaliteController {
	@Autowired
	TypeLocaliteRepository typeLocaliteService;
	
	//@PreAuthorize("hasAuthority('gestion des zones')")
	@GetMapping("/typeLocalite")
	public String zone(Model model){
		List<TypeLocalite> typeLocalites = typeLocaliteService.findByIsDeletedFalseOrderByLibelleAsc();
		model.addAttribute("typeLocalites", typeLocalites);
		model.addAttribute("horizontalMenu", "horizontalMenu");
		model.addAttribute("sidebarMenu", "configurationSidebarMenu");
		model.addAttribute("breadcrumb", "breadcrumb");
		model.addAttribute("navigationPath", "Referentiel/Type Localite");
		model.addAttribute("viewPath", "referentiel/zone/typeLocalite");
		return Template.defaultTemplate;
	}
}
