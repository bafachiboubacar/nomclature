package ne.mha.sinea.referentiel.zone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class ZoneRestController {
	@Autowired
	ZoneRepository zoneService;

	//@PreAuthorize("hasAuthority('gestion des zones')")
	@PostMapping("/zone")
	public int addZone(@Validated ZoneForm zoneForm,BindingResult bindingResult, Model model) {
		Zone savedZone = new Zone();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					Zone C = new Zone();
					C.setLibelle(zoneForm.getLibelle());
					
					if(zoneForm.getParent_code() > 0)
					{
						Zone parent = zoneService.findByCode(zoneForm.getParent_code());
						C.setParent(parent);
						C.setNiveau(parent.getNiveau() + 1);
						
					}
					else
					{
						C.setParent(null);
						C.setNiveau(0);
					}
				
					savedZone = zoneService.save(C);
				
				
				}
			catch(Exception e){
				
				
					}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedZone.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des zones')")
	@PostMapping("/updateZone")
    public int updateZone(@Validated ZoneForm zoneForm,BindingResult bindingResult, Model model) {
		Zone savedZone = new Zone();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					Zone C = zoneService.findByCode(zoneForm.getCode());
					C.setLibelle(zoneForm.getLibelle());
					savedZone = zoneService.save(C);
					
				}
			catch(Exception e){
				
				
					}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedZone.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des zones')")
	@PostMapping("/deleteZone")
    public int deleteZone(@Validated ZoneForm zoneForm,BindingResult bindingResult, Model model) {
		Zone savedZone = new Zone();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					Zone C = zoneService.findByCode(zoneForm.getCode());
					C.setDeleted(true);
					savedZone = zoneService.save(C);
				
				}
			catch(Exception e){
				
				
					}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedZone.getCode();
		
        
    }
}
