package ne.mha.sinea.referentiel.etablissementScolaire;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import ne.mha.sinea.Template;
import ne.mha.sinea.nomenclature.typeEtablissementScolaire.TypeEtablissementScolaire;
import ne.mha.sinea.nomenclature.typeEtablissementScolaire.TypeEtablissementScolaireRepository;
import ne.mha.sinea.referentiel.zone.Localite;
import ne.mha.sinea.referentiel.zone.LocaliteRepository;

@Controller
public class EtablissementScolaireController {

	@Autowired
	EtablissementScolaireRepository etablissementScolaireService;
	@Autowired
	TypeEtablissementScolaireRepository typeEtablissementScolaireService;
	@Autowired
	LocaliteRepository localiteService;

	//@PreAuthorize("hasAuthority('Ajout etablissementScolaire'))
	@GetMapping("/etablissementScolaire")
	public String  addEtablissementScolaire(EtablissementScolaireForm etablissementScolaireForm, Model model) {
		try{
			//récuperation de la liste des etablissementScolaire de la base de données et envoie de la liste récuperée à la vue à travers le modele
			List<EtablissementScolaire> etablissementScolaires =  (List<EtablissementScolaire>) etablissementScolaireService.findByIsDeletedFalse();
			model.addAttribute("etablissementScolaires", etablissementScolaires);
			List<TypeEtablissementScolaire> typeEtablissementScolaires =  (List<TypeEtablissementScolaire>) typeEtablissementScolaireService.findByIsDeletedFalse();
			model.addAttribute("typeEtablissementScolaires", typeEtablissementScolaires);
			List<Localite> localites =  (List<Localite>) localiteService.findByIsDeletedFalse();
			model.addAttribute("localites", localites);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Referentiel/Etablissement Scolaire");
			model.addAttribute("viewPath", "referentiel/etablissementScolaire/etablissementScolaire");
			
			}
		catch(Exception e){
				
			}
		//afficher le template avec la vue (formulaire) à l'intérieur
		return Template.defaultTemplate;

	}
	
	//validation du formulaire d'ajout etablissementScolaire
	@PostMapping("/etablissementScolaire")
    public String addEtablissementScolaireSubmit(@Validated EtablissementScolaireForm etablissementScolaireForm,BindingResult bindingResult, Model model) {
    	//s'il n'ya pas d'erreur lors de la validation des données du formulaire
		if (!bindingResult.hasErrors()) {
			
			try {
				//récuperer les données saisies
				EtablissementScolaire EP = new EtablissementScolaire();
				
				EP.setDenomination(etablissementScolaireForm.getDenomination());
				EP.setLatitude(etablissementScolaireForm.getLatitude());
				EP.setLongitude(etablissementScolaireForm.getLongitude());
				EP.setTypeEtablissementScolaire(typeEtablissementScolaireService.findByCode(etablissementScolaireForm.getCodeTypeEtablissementScolaire()));
				EP.setLocalite(localiteService.findByCode(etablissementScolaireForm.getCodeTypeEtablissementScolaire()));
				
				etablissementScolaireService.save(EP);
				
				//récuperation de la liste des etablissementScolaire de la base de données et envoie de la liste récuperée à la vue à travers le modele
				List<EtablissementScolaire> etablissementScolaires =  (List<EtablissementScolaire>) etablissementScolaireService.findByIsDeletedFalse();
				model.addAttribute("etablissementScolaires", etablissementScolaires);
				List<TypeEtablissementScolaire> typeEtablissementScolaires =  (List<TypeEtablissementScolaire>) typeEtablissementScolaireService.findByIsDeletedFalse();
				model.addAttribute("typeEtablissementScolaires", typeEtablissementScolaires);
				List<Localite> localites =  (List<Localite>) localiteService.findByIsDeletedFalse();
				model.addAttribute("localites", localites);
				
				//envoie de la liste récuperée à la vue à travers le modele
				model.addAttribute("etablissementScolaires", etablissementScolaires);
				//indiquer que l'operation d'ajout a réussie
				model.addAttribute("operationStatus", "operationStatus/success");
				model.addAttribute("horizontalMenu", "horizontalMenu");
				model.addAttribute("sidebarMenu", "configurationSidebarMenu");
				model.addAttribute("breadcrumb", "breadcrumb");
				model.addAttribute("navigationPath", "Referentiel/Etablissement Scolaire");
				model.addAttribute("viewPath", "referentiel/etablissementScolaire/etablissementScolaire");
				
				}
			catch(Exception e){
					//récuperation de la liste des etablissementScolaire de la base de données et envoie de la liste récuperée à la vue à travers le modele
					List<EtablissementScolaire> etablissementScolaires =  (List<EtablissementScolaire>) etablissementScolaireService.findByIsDeletedFalse();
					model.addAttribute("etablissementScolaires", etablissementScolaires);
					List<TypeEtablissementScolaire> typeEtablissementScolaires =  (List<TypeEtablissementScolaire>) typeEtablissementScolaireService.findByIsDeletedFalse();
					model.addAttribute("typeEtablissementScolaires", typeEtablissementScolaires);
					List<Localite> localites =  (List<Localite>) localiteService.findByIsDeletedFalse();
					model.addAttribute("localites", localites);
					
					model.addAttribute("operationStatus", "operationStatus/unsuccess");
					model.addAttribute("horizontalMenu", "horizontalMenu");
					model.addAttribute("sidebarMenu", "configurationSidebarMenu");
					model.addAttribute("breadcrumb", "breadcrumb");
					model.addAttribute("navigationPath", "Referentiel/Etablissement Scolaire");
					model.addAttribute("viewPath", "referentiel/etablissementScolaire/etablissementScolaire");
					
					}
			
    	}
		else{
    		try{
    			
    			//récuperation de la liste des etablissementScolaire de la base de données et envoie de la liste récuperée à la vue à travers le modele
    			List<EtablissementScolaire> etablissementScolaires =  (List<EtablissementScolaire>) etablissementScolaireService.findByIsDeletedFalse();
    			model.addAttribute("etablissementScolaires", etablissementScolaires);
    			List<TypeEtablissementScolaire> typeEtablissementScolaires =  (List<TypeEtablissementScolaire>) typeEtablissementScolaireService.findByIsDeletedFalse();
    			model.addAttribute("typeEtablissementScolaires", typeEtablissementScolaires);
    			List<Localite> localites =  (List<Localite>) localiteService.findByIsDeletedFalse();
    			model.addAttribute("localites", localites);
    			model.addAttribute("operationStatus", "operationStatus/unsuccess");
    			
				model.addAttribute("horizontalMenu", "horizontalMenu");
				model.addAttribute("sidebarMenu", "configurationSidebarMenu");
				model.addAttribute("breadcrumb", "breadcrumb");
				model.addAttribute("navigationPath", "Referentiel/Etablissement Scolaire");
				model.addAttribute("viewPath", "referentiel/etablissementScolaire/etablissementScolaire");
				
				}
    		catch(Exception e){
				
				}
    	}
		
		//afficher le template avec la vue (formulaire) à l'intérieur
		return Template.defaultTemplate;
        
    }
	
	//modification d'une etablissementScolaire
	@GetMapping("/updateEtablissementScolaire/{code}")
    public String updateEtablissementScolaire(@PathVariable("code") int code,EtablissementScolaireForm etablissementScolaireForm, Model model) 
	{ 
		try
		{
			//récuperer les informations de la etablissementScolaire à modifier
    		EtablissementScolaire EP =   etablissementScolaireService.findByCode(code);
    		//envoie des informations de la etablissementScolaire à modifier à travers le modele
    		EP.setDenomination(etablissementScolaireForm.getDenomination());
			EP.setLatitude(etablissementScolaireForm.getLatitude());
			EP.setLongitude(etablissementScolaireForm.getLongitude());
			EP.setTypeEtablissementScolaire(typeEtablissementScolaireService.findByCode(etablissementScolaireForm.getCodeTypeEtablissementScolaire()));
			EP.setLocalite(localiteService.findByCode(etablissementScolaireForm.getCodeTypeEtablissementScolaire()));
			
			List<EtablissementScolaire> etablissementScolaires =  (List<EtablissementScolaire>) etablissementScolaireService.findByIsDeletedFalse();
			model.addAttribute("etablissementScolaires", etablissementScolaires);
			List<TypeEtablissementScolaire> typeEtablissementScolaires =  (List<TypeEtablissementScolaire>) typeEtablissementScolaireService.findByIsDeletedFalse();
			model.addAttribute("typeEtablissementScolaires", typeEtablissementScolaires);
			List<Localite> localites =  (List<Localite>) localiteService.findByIsDeletedFalse();
			model.addAttribute("localites", localites);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Referentiel/Etablissement Scolaire");
			model.addAttribute("viewPath", "referentiel/etablissementScolaire/etablissementScolaire");
			
		}
		catch(Exception e)
		{
			
		}
		return Template.defaultTemplate;
    		
    }
	
	@PostMapping("/updateEtablissementScolaire/{code}")
    public RedirectView updateEtablissementScolaireSubmit(@Validated EtablissementScolaireForm etablissementScolaireForm,BindingResult bindingResult,@PathVariable("code") int code, Model model,RedirectAttributes redirectAttributes) 
	{
		final RedirectView redirectView = new RedirectView("/etablissementScolaire", true);
    	if (!bindingResult.hasErrors()) 
    	{
			try
			{
				//recupérer les informations saisies
				EtablissementScolaire EP = etablissementScolaireService.findByCode(code);
				//envoie des informations de la etablissementScolaire à modifier à travers le modele
	    		EP.setDenomination(etablissementScolaireForm.getDenomination());
				EP.setLatitude(etablissementScolaireForm.getLatitude());
				EP.setLongitude(etablissementScolaireForm.getLongitude());
				EP.setTypeEtablissementScolaire(typeEtablissementScolaireService.findByCode(etablissementScolaireForm.getCodeTypeEtablissementScolaire()));
				EP.setLocalite(localiteService.findByCode(etablissementScolaireForm.getCodeTypeEtablissementScolaire()));
				//enregistrer les données (l'entité=) dans la base
				etablissementScolaireService.save(EP);
				
				//indiquer que l'operation de modification a réussi
				redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/success");
				
			}
			catch(Exception e)
			{
				//indiquer que l'operation de modification a échoué
				redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/unsuccess");
			}
    	}
    	else
    	{
    		
    	}
		
    	//redirection vers la page d'ajout
		return redirectView;
        
    }
	
	@GetMapping("/deleteEtablissementScolaire/{code}")
	public RedirectView deleteEtablissementScolaire(@PathVariable("code") int code, Model model,RedirectAttributes redirectAttributes) 
	{ 
		final RedirectView redirectView = new RedirectView("/etablissementScolaire", true);
		try
		{
			EtablissementScolaire EP = etablissementScolaireService.findByCode(code);
			EP.setDeleted(true);
			etablissementScolaireService.save(EP);
			
			//indiquer que l'operation de suppression a réussi
			redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/success");
		}
		catch(Exception e)
		{
			//indiquer que l'operation de suppression a échoué
			redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/unsuccess");
		}
		
		//redirection vers la page d'ajout
		return redirectView;
	    		
	}

	
}
