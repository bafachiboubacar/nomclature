package ne.mha.sinea.referentiel.indicateur.frame;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ne.mha.sinea.CommonProperties;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "frame")
public class Frame extends CommonProperties{
	 
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer code;
    @NotNull
    private String libelle;
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinTable(
        name = "frame_section_frame", 
        joinColumns = @JoinColumn(
          name = "frame_code", referencedColumnName = "code"), 
        inverseJoinColumns = @JoinColumn(
          name = "section_frame_code", referencedColumnName = "code"))
    private List<SectionFrame> sectionFrames = new ArrayList<>();
}