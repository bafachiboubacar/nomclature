package ne.mha.sinea.referentiel.indicateur.ficheTechnique;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import ne.mha.sinea.referentiel.indicateur.indicateur.Indicateur;
import ne.mha.sinea.referentiel.indicateur.indicateur.IndicateurRepository;

@Controller
public class FicheTechniqueController {
	@Autowired
	FicheTechniqueRepository ficheTechniqueService;
	
	@Autowired
	IndicateurRepository indicateurService;
	
	//@PreAuthorize("hasAuthority('gestion de la fiche technique des indicateurs')")
	@GetMapping("/afficherFicheTechnique/{code}")
	public String afficherFicheTechnique(@PathVariable("code") int code, Model model) {
		try{
			Indicateur indicateur = indicateurService.findByCode(code);
			FicheTechnique ficheTechnique = ficheTechniqueService.findByIsDeletedFalseAndIndicateur_Code(code);
			if(ficheTechnique == null)
			{
				ficheTechnique = new FicheTechnique();
			}
			model.addAttribute("ficheTechnique", ficheTechnique);
			model.addAttribute("indicateur", indicateur);
			
			}
		catch(Exception e){
				
			}
		return "ficheTechnique/afficherFicheTechnique";
	}
	/*@GetMapping("/afficherFicheTechnique/{code}")
	public String  ficheTechnique(@PathVariable("code") int code,Model model) {
		try{
			
			FicheTechnique ficheTechnique = ficheTechniqueService.findByIsDeletedFalseAndIndicateur_Code(code);
			model.addAttribute("ficheTechnique", ficheTechnique);
			
			
			}
		catch(Exception e){
				
			}
		return "ficheTechnique/afficherFicheTechnique";
	}*/
	
	//@PreAuthorize("hasAuthority('gestion de la fiche technique des indicateurs')")
	@GetMapping("/ajouterFicheTechnique/{codeIndicateur}/{codeClassification}")
	public String ajouterFicheTechnique(@PathVariable("codeIndicateur") int codeIndicateur,@PathVariable("codeClassification") int codeClassification,FicheTechniqueForm ficheTechniqueForm, Model model) {
		try{
			Indicateur indicateur = indicateurService.findByCode(codeIndicateur);
			FicheTechnique ficheTechnique = ficheTechniqueService.findByIsDeletedFalseAndIndicateur_Code(codeIndicateur);
			if(ficheTechnique != null)
			{
				ficheTechniqueForm.setDefinition(ficheTechnique.getDefinition());
				ficheTechniqueForm.setDonneesRequises(ficheTechnique.getDonneesRequises());
				ficheTechniqueForm.setMethodeCalcul(ficheTechnique.getMethodeCalcul());
				ficheTechniqueForm.setCommentaireLimite(ficheTechnique.getCommentaireLimite());
				ficheTechniqueForm.setPeriodicite(ficheTechnique.getPeriodicite());
				ficheTechniqueForm.setNiveauDesagregation(ficheTechnique.getNiveauDesagregation());
				ficheTechniqueForm.setUnite(ficheTechnique.getUnite());
				ficheTechniqueForm.setSource(ficheTechnique.getSource());
				ficheTechniqueForm.setMethodeCollecte(ficheTechnique.getMethodeCollecte());
			
			
			}
			model.addAttribute("indicateur", indicateur);
			model.addAttribute("typeClassification_code", codeClassification);
			
			}
		catch(Exception e){
				
			}
		return "ficheTechnique/ajouterFicheTechnique";
	}
	
	//@PreAuthorize("hasAuthority('gestion de la fiche technique des indicateurs')")
	@PostMapping("/ajouterFicheTechnique/{codeIndicateur}/{codeClassification}")
	public RedirectView ajouterFicheTechniqueSubmit(@PathVariable("codeIndicateur") int codeIndicateur,@PathVariable("codeClassification") int codeClassification,@Validated FicheTechniqueForm ficheTechniqueForm, BindingResult bindingResult,RedirectAttributes redirectAttributes, Model model) {
		final RedirectView redirectView = new RedirectView("/classificationIndicateur/"+codeClassification, true);
		try{
			Indicateur indicateur = indicateurService.findByCode(codeIndicateur);
			FicheTechnique ficheTechnique = ficheTechniqueService.findByIsDeletedFalseAndIndicateur_Code(codeIndicateur);
			if(ficheTechnique != null)
			{
				ficheTechnique.setIndicateur(indicateur);
				ficheTechnique.setDefinition(ficheTechniqueForm.getDefinition());
				ficheTechnique.setDonneesRequises(ficheTechniqueForm.getDonneesRequises());
				ficheTechnique.setMethodeCalcul(ficheTechniqueForm.getMethodeCalcul());
				ficheTechnique.setCommentaireLimite(ficheTechniqueForm.getCommentaireLimite());
				ficheTechnique.setPeriodicite(ficheTechniqueForm.getPeriodicite());
				ficheTechnique.setNiveauDesagregation(ficheTechniqueForm.getNiveauDesagregation());
				ficheTechnique.setUnite(ficheTechniqueForm.getUnite());
				ficheTechnique.setSource(ficheTechniqueForm.getSource());
				ficheTechnique.setMethodeCollecte(ficheTechniqueForm.getMethodeCollecte());
				ficheTechniqueService.save(ficheTechnique);
			}
			else
			{
				ficheTechnique = new FicheTechnique();
				ficheTechnique.setIndicateur(indicateur);
				ficheTechnique.setDefinition(ficheTechniqueForm.getDefinition());
				ficheTechnique.setDonneesRequises(ficheTechniqueForm.getDonneesRequises());
				ficheTechnique.setMethodeCalcul(ficheTechniqueForm.getMethodeCalcul());
				ficheTechnique.setCommentaireLimite(ficheTechniqueForm.getCommentaireLimite());
				ficheTechnique.setPeriodicite(ficheTechniqueForm.getPeriodicite());
				ficheTechnique.setNiveauDesagregation(ficheTechniqueForm.getNiveauDesagregation());
				ficheTechnique.setUnite(ficheTechniqueForm.getUnite());
				ficheTechnique.setSource(ficheTechniqueForm.getSource());
				ficheTechnique.setMethodeCollecte(ficheTechniqueForm.getMethodeCollecte());
				ficheTechniqueService.save(ficheTechnique);
			}
			
			redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/success");
			
		}
		catch(Exception e){
			
			redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/unsuccess");
			
		}
		return redirectView;
	}
	
	
}
