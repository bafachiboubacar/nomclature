package ne.mha.sinea.referentiel.indicateur.classification;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import ne.mha.sinea.nomenclature.typeClassification.TypeClassification;
import ne.mha.sinea.nomenclature.typeClassification.TypeClassificationRepository;

@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class ClassificationRestController {

	@Autowired
	ClassificationRepository classificationService;
	@Autowired
	TypeClassificationRepository typeClassificationService;
	
	//@PreAuthorize("hasAuthority('classification des indicateurs')")
	@PostMapping("/classification")
	public int addClassification(@Validated ClassificationForm classificationForm,BindingResult bindingResult, Model model) {
		Classification savedClassification = new Classification();
		if (!bindingResult.hasErrors()) {
			try {
					Classification P = new Classification();
					P.setLibelle(classificationForm.getLibelle());
					TypeClassification typeClassification = typeClassificationService.findByCode(classificationForm.getTypeClassification_code());
					P.setTypeClassification(typeClassification);
					if(classificationForm.getParent_code() > 0)
					{
						Classification parent = classificationService.findByCode(classificationForm.getParent_code());
						P.setParent(parent);
						P.setNiveau(parent.getNiveau() + 1);
						
					}
					else
					{
						P.setParent(null);
						P.setNiveau(0);
					}
				
					savedClassification = classificationService.save(P);
				
				
				}
			catch(Exception e){
				
				
					}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedClassification.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('classification des indicateurs')")
	@PostMapping("/updateClassification")
    public int updateClassification(@Validated ClassificationForm classificationForm,BindingResult bindingResult, Model model) {
		Classification savedClassification = new Classification();
		if (!bindingResult.hasErrors()) {
			
			try {
					Classification P = classificationService.findByCode(classificationForm.getCode());
					P.setLibelle(classificationForm.getLibelle());
					savedClassification = classificationService.save(P);
					
				}
			catch(Exception e){
				
			}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedClassification.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('classification des indicateurs')")
	@PostMapping("/deleteClassification")
    public int deleteClassification(@Validated ClassificationForm classificationForm,BindingResult bindingResult, Model model) {
		Classification savedClassification = new Classification();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					Classification P = classificationService.findByCode(classificationForm.getCode());
					P.setDeleted(true);
					savedClassification = classificationService.save(P);
					
				}
			catch(Exception e){
				
				
					}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedClassification.getCode();
		
        
    }
}
