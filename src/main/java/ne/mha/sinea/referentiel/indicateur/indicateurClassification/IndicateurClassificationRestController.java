package ne.mha.sinea.referentiel.indicateur.indicateurClassification;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ne.mha.sinea.referentiel.indicateur.classification.Classification;
import ne.mha.sinea.referentiel.indicateur.classification.ClassificationRepository;
import ne.mha.sinea.referentiel.indicateur.indicateur.Indicateur;
import ne.mha.sinea.referentiel.indicateur.indicateur.IndicateurRepository;

@RestController
@ResponseBody
public class IndicateurClassificationRestController {

	@Autowired
	IndicateurClassificationRepository indicateurClassificationService;
	
	@Autowired
	IndicateurRepository indicateurService;
	
	@Autowired
	ClassificationRepository classificationService;
	//@PreAuthorize("hasAuthority('classification des indicateurs')")
	@GetMapping("/listeIndicateursClassification/{code}")
	public String  listeIndicateursClassification(@PathVariable("code") int code) {
		String res = "";
		try{
			List<IndicateurClassification> indicateurClassification = indicateurClassificationService.findByIsDeletedFalseAndClassification_Code(code);
			for(int i=0;i<indicateurClassification.size();i++)
			{
				if(i==0)
					res += "{ id:"+indicateurClassification.get(i).getIndicateur().getCode()+", pId:0, name:\""+indicateurClassification.get(i).getIndicateur().getLibelle()+"\"}";
				else
					res += ",{ id:"+indicateurClassification.get(i).getIndicateur().getCode()+", pId:0, name:\""+indicateurClassification.get(i).getIndicateur().getLibelle()+"\"}";
				
			}
			res = "["+res+"]";

			}
		catch(Exception e){
			System.out.println(e);
			}
		return res;
	}
	//@PreAuthorize("hasAuthority('classification des indicateurs')")
	@GetMapping("/getIndicateursClassificationDisponibles/{code}")
	public String  getIndicateursClassificationDisponibles(@PathVariable("code") int code) {
		String res = "";
		try{
			List<Indicateur> indicateursDisponibles = new ArrayList<Indicateur>();
			List<Integer> indicateursUtilises = new ArrayList<Integer>();
			List<IndicateurClassification> indicateurClassification = indicateurClassificationService.findByIsDeletedFalseAndClassification_Code(code);
			for(int i=0;i<indicateurClassification.size();i++)
			{
				indicateursUtilises.add(indicateurClassification.get(i).getIndicateur().getCode());
			}
			if(indicateursUtilises.isEmpty())
				indicateursDisponibles = indicateurService.findByIsDeletedFalse();
			else
				indicateursDisponibles = indicateurService.findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(indicateursUtilises);
			for(int i=0;i<indicateursDisponibles.size();i++)
			{
				if(i==0)
					res += "{ id:"+indicateursDisponibles.get(i).getCode()+", pId:0, name:\""+indicateursDisponibles.get(i).getLibelle()+"\"}";
				else
					res += ",{ id:"+indicateursDisponibles.get(i).getCode()+", pId:0, name:\""+indicateursDisponibles.get(i).getLibelle()+"\"}";
				
			}
			if(indicateursDisponibles.size() >0)
				res = "[{ id:0, pId:0, name:\"/\", open:true},"+res+"]";
			else
				res = "[{ id:0, pId:0, name:\"/\", open:true}]";
			
			}
		catch(Exception e){
			System.out.println(e);
			}
		return res;
	}
	//@PreAuthorize("hasAuthority('classification des indicateurs')")
	@PostMapping("/ajoutIndicateurClassification")
	public String ajoutIndicateurClassificationSubmit(@RequestParam(value="params[]") Integer[] params,Integer classificationId) {
		try{
				Classification classification = classificationService.findByCode(classificationId); 
				for(int i=0;i<params.length;i++)
				{
					Indicateur indicateur = indicateurService.findByCode(params[i]);
					IndicateurClassification io = new IndicateurClassification();
					io.setCode(new IndicateurClassificationKey(indicateur.getCode(),classification.getCode()));
					io.setIndicateur(indicateur);
					io.setClassification(classification);
					indicateurClassificationService.save(io);
					
				}
			
			}
		catch(Exception e){
			System.out.println(e);
			}
			
		return "ok";
	}
	//@PreAuthorize("hasAuthority('classification des indicateurs')")
	@PostMapping("/suppressionIndicateurClassification")
	public String suppressionIndicateurClassificationSubmit(@RequestParam(value="params[]") Integer[] params,Integer classificationId) {
		try{
				for(int i=0;i<params.length;i++)
				{
					IndicateurClassification io = indicateurClassificationService.findByIsDeletedFalseAndIndicateur_CodeAndClassification_Code(params[i],classificationId);
					indicateurClassificationService.delete(io);
					
				}
			
			}
		catch(Exception e){
			System.out.println(e);
			}
			
		return "ok";
	}
	
}
