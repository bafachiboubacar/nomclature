package ne.mha.sinea.referentiel.indicateur.indicateur;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class IndicateurRestController {

	@Autowired
	IndicateurRepository indicateurService;
	//@PreAuthorize("hasAuthority('gestion des indicateurs')")
	@PostMapping("/indicateur")
	public int addIndicateur(@Validated IndicateurForm indicateurForm,BindingResult bindingResult, Model model) {
		Indicateur savedIndicateur = new Indicateur();
		if (!bindingResult.hasErrors()) {
			
			try {
				
				//récuperer les données saisies
				Indicateur P = new Indicateur();
				P.setLibelle(indicateurForm.getLibelle());
				savedIndicateur = indicateurService.save(P);
				
				
				}
			catch(Exception e){
				
				
					}
			
		}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedIndicateur.getCode();
		
        
    }
	//@PreAuthorize("hasAuthority('gestion des indicateurs')")
	@PostMapping("/updateIndicateur")
    public int updateIndicateur(@Validated IndicateurForm indicateurForm,BindingResult bindingResult, Model model) {
		Indicateur savedIndicateur = new Indicateur();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					Indicateur P = indicateurService.findByCode(indicateurForm.getCode());
					P.setLibelle(indicateurForm.getLibelle());
					savedIndicateur = indicateurService.save(P);
					
				}
			catch(Exception e){
				
				}
			
		}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedIndicateur.getCode();
		
        
    }
	//@PreAuthorize("hasAuthority('gestion des indicateurs')")
	@PostMapping("/deleteIndicateur")
    public int deleteIndicateur(@Validated IndicateurForm indicateurForm,BindingResult bindingResult, Model model) {
		Indicateur savedIndicateur = new Indicateur();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					Indicateur P = indicateurService.findByCode(indicateurForm.getCode());
					P.setDeleted(true);
					savedIndicateur = indicateurService.save(P);
					
				}
			catch(Exception e){
				
				}
			
		}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedIndicateur.getCode();
		
        
    }
	//@PreAuthorize("hasAuthority('gestion des indicateurs')")
	@GetMapping("/getIndicateur/{code}")
	public String  listeIndicateur(@PathVariable("code") int code) {
		String res = "";
		try{
			
				Indicateur indicateur = indicateurService.findByCode(code);
				res += ",{ id:"+indicateur.getCode()+", pId:0, name:\""+indicateur.getLibelle()+"\"}";
				res = "[{ id:0, pId:0, name:\"/\", open:true}"+res+"]";
				
			}
		catch(Exception e){
				
			}
		return res;
	}
	
	
	
}
