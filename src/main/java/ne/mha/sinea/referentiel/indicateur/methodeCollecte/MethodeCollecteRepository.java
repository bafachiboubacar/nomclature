package ne.mha.sinea.referentiel.indicateur.methodeCollecte;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface MethodeCollecteRepository extends CrudRepository<MethodeCollecte, Integer> {
	MethodeCollecte findByCode(Integer code);
	MethodeCollecte findByIsDeletedFalseAndLibelle(String libelle);
	List<MethodeCollecte> findByIsDeletedFalseOrderByLibelleAsc();
	List<MethodeCollecte> findByIsDeletedFalse();
	List<MethodeCollecte> findByIsDeletedTrue();
	

}
