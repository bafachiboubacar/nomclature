package ne.mha.sinea.referentiel.indicateur.indicateurClassification;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import ne.mha.sinea.Template;
import ne.mha.sinea.nomenclature.typeClassification.TypeClassification;
import ne.mha.sinea.nomenclature.typeClassification.TypeClassificationRepository;
import ne.mha.sinea.referentiel.indicateur.classification.Classification;
import ne.mha.sinea.referentiel.indicateur.classification.ClassificationRepository;
import ne.mha.sinea.referentiel.indicateur.frame.SectionFrame;
import ne.mha.sinea.referentiel.indicateur.frame.SectionFrameRepository;
import ne.mha.sinea.referentiel.indicateur.indicateur.Indicateur;
import ne.mha.sinea.referentiel.indicateur.indicateur.IndicateurRepository;

@Controller
public class IndicateurClassificationController {
	@Autowired
	IndicateurRepository indicateurService;
	@Autowired
	ClassificationRepository classificationService;
	@Autowired
	TypeClassificationRepository typeClassificationService;
	
	@Autowired
	SectionFrameRepository sectionFrameService;

	//@PreAuthorize("hasAuthority('classification des indicateurs')")
	@GetMapping("/classificationIndicateur/{code}")
	public String  classificationIndicateurClassification(@PathVariable("code") int code,Model model) {
		try{
			List<Indicateur> indicateurs = indicateurService.findByIsDeletedFalseOrderByLibelleAsc();
			TypeClassification typeClassification = typeClassificationService.findByCode(code);
			List<Classification> classifications = classificationService.findByIsDeletedFalseAndTypeClassification_CodeOrderByLibelleAsc(code);
			
			List<TypeClassification> typeClassifications = typeClassificationService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeClassifications", typeClassifications);
			List<SectionFrame> sectionFrames = sectionFrameService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("sectionFrames", sectionFrames);
			
			model.addAttribute("indicateurs", indicateurs);
			model.addAttribute("typeClassification", typeClassification);
			model.addAttribute("classifications", classifications);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Referentiel/Classification des indicateurs par "+typeClassification.getLibelle());
			model.addAttribute("viewPath", "referentiel/classification/classificationIndicateur");
		
		
		}
		catch(Exception e){
				
			}
		return Template.defaultTemplate;
		

	}
		
	
}
