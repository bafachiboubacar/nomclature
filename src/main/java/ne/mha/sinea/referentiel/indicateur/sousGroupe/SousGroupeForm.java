package ne.mha.sinea.referentiel.indicateur.sousGroupe;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SousGroupeForm {

	private int code;
	private String libelle;
	private int parent_code;
	
}
