package ne.mha.sinea.referentiel.indicateur.sousGroupe;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class SousGroupeRestController {

	@Autowired
	SousGroupeRepository sousGroupeService;
	//@PreAuthorize("hasAuthority('gestion des sous groupes')")
	@PostMapping("/sousGroupe")
	public int addSousGroupe(@Validated SousGroupeForm sousGroupeForm,BindingResult bindingResult, Model model) {
		SousGroupe savedSousGroupe = new SousGroupe();
		if (!bindingResult.hasErrors()) {
			
			try {
				
				SousGroupe P = new SousGroupe();
				P.setLibelle(sousGroupeForm.getLibelle());
				if(sousGroupeForm.getParent_code() > 0)
				{
					SousGroupe parent = sousGroupeService.findByCode(sousGroupeForm.getParent_code());
					P.setParent(parent);
					P.setNiveau(parent.getNiveau() + 1);
					
				}
				else
				{
					P.setParent(null);
					P.setNiveau(0);
				}
				
				savedSousGroupe = sousGroupeService.save(P);
				
				
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedSousGroupe.getCode();
		
        
    }
	//@PreAuthorize("hasAuthority('gestion des sous groupes')")
	@PostMapping("/updateSousGroupe")
    public int updateSousGroupe(@Validated SousGroupeForm sousGroupeForm,BindingResult bindingResult, Model model) {
		SousGroupe savedSousGroupe = new SousGroupe();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					SousGroupe P = sousGroupeService.findByCode(sousGroupeForm.getCode());
					P.setLibelle(sousGroupeForm.getLibelle());
					savedSousGroupe = sousGroupeService.save(P);
					
				
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedSousGroupe.getCode();
		
        
    }
	//@PreAuthorize("hasAuthority('gestion des sous groupes')")
	@PostMapping("/deleteSousGroupe")
    public int deleteSousGroupe(@Validated SousGroupeForm sousGroupeForm,BindingResult bindingResult, Model model) {
		SousGroupe savedSousGroupe = new SousGroupe();
		if (!bindingResult.hasErrors()) {
			
			try {
				
				SousGroupe P = sousGroupeService.findByCode(sousGroupeForm.getCode());
				P.setDeleted(true);
				savedSousGroupe = sousGroupeService.save(P);
				
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedSousGroupe.getCode();
		
        
    }
}
