package ne.mha.sinea.referentiel.indicateur.source;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SourceForm {

	private int code;
	private String libelle;
	
}
