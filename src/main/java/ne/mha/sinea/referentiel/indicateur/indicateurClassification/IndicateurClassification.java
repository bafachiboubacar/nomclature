package ne.mha.sinea.referentiel.indicateur.indicateurClassification;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ne.mha.sinea.CommonProperties;
import ne.mha.sinea.referentiel.indicateur.classification.Classification;
import ne.mha.sinea.referentiel.indicateur.indicateur.Indicateur;

//annotation de lombok pour générer les getter et setter
@Data
//annotation de lombok pour générer le constructeur sans paramètre
@NoArgsConstructor
//annotation de lombok pour générer le constructeur avec tous les paramètres
@AllArgsConstructor
//indique qu'il s'agit d'une entité=> création d'une table au niveau de la base de données
@Entity
@Table(name = "indicateur_classification")
public class IndicateurClassification extends CommonProperties {
	
	@EmbeddedId
	private IndicateurClassificationKey code;
	@MapsId("codeIndicateur")
	@JoinColumn(name = "code_indicateur", referencedColumnName = "code")
	@ManyToOne
	private Indicateur indicateur;
	@MapsId("codeClassification")
	@JoinColumn(name = "code_classification", referencedColumnName = "code")
	@ManyToOne
	private Classification classification;

	
	
}
