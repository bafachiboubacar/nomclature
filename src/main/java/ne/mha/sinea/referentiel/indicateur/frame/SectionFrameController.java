package ne.mha.sinea.referentiel.indicateur.frame;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import ne.mha.sinea.Template;
import ne.mha.sinea.nomenclature.periode.Periode;
import ne.mha.sinea.nomenclature.periode.PeriodeRepository;
import ne.mha.sinea.nomenclature.typeClassification.TypeClassification;
import ne.mha.sinea.nomenclature.typeClassification.TypeClassificationRepository;
import ne.mha.sinea.referentiel.indicateur.classification.Classification;
import ne.mha.sinea.referentiel.indicateur.classification.ClassificationRepository;
import ne.mha.sinea.referentiel.indicateur.indicateur.Indicateur;
import ne.mha.sinea.referentiel.indicateur.indicateur.IndicateurRepository;
import ne.mha.sinea.referentiel.indicateur.source.Source;
import ne.mha.sinea.referentiel.indicateur.source.SourceRepository;
import ne.mha.sinea.referentiel.zone.Zone;
import ne.mha.sinea.referentiel.zone.ZoneRepository;

@Controller
public class SectionFrameController {

	@Autowired
	IndicateurRepository indicateurService;
	@Autowired
	ClassificationRepository classificationService;
	@Autowired
	PeriodeRepository periodeService;
	@Autowired
	ZoneRepository zoneService;
	@Autowired
	SourceRepository sourceService;
	
	@Autowired
	TypeClassificationRepository typeClassificationService;
	
	@Autowired
	SectionFrameRepository sectionFrameService;
	//@PreAuthorize("hasAuthority('gestion des sections de saisie')")
	@GetMapping("/sectionframe/{code}")
	public String  sectionFrame(@PathVariable("code") int code,Model model) {
		try{
			SectionFrame sectionFrame = sectionFrameService.findByCode(code);
			
			List<Classification> classifications = classificationService.findByIsDeletedFalseAndTypeClassification_CodeOrderByLibelleAsc(code);
			model.addAttribute("classifications", classifications);
			
			List<TypeClassification> typeClassifications = typeClassificationService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeClassifications", typeClassifications);
			
			List<SectionFrame> sectionFrames = sectionFrameService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("sectionFrames", sectionFrames);
			
			List<Source> sources = sourceService.findByIsDeletedFalseAndCodeInOrderByLibelleAsc(sectionFrame.getSources());
			model.addAttribute("sources", sources);
			
			List<Indicateur> indicateurs = indicateurService.findByIsDeletedFalseAndCodeInOrderByLibelleAsc(sectionFrame.getIndicateurs());
			model.addAttribute("indicateurs", indicateurs);
			
			List<Zone> zones = zoneService.findByIsDeletedFalseAndCodeInOrderByLibelleAsc(sectionFrame.getZones());
			model.addAttribute("zones", zones);
			
			List<Periode> periodes = periodeService.findByIsDeletedFalseAndCodeInOrderByLibelleAsc(sectionFrame.getPeriodes());
			model.addAttribute("periodes", periodes);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Referentiel/Saisie de la série de données");
			model.addAttribute("viewPath", "referentiel/frame/sectionFrame");
		
		
		}
		catch(Exception e){
				
			}
		return Template.defaultTemplate;
	}
	
	
}
