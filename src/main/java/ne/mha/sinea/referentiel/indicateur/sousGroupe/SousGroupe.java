package ne.mha.sinea.referentiel.indicateur.sousGroupe;

import java.util.ArrayList;
import java.util.List;
/*import java.util.Set;
import java.util.HashSet;*/
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.CascadeType;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ne.mha.sinea.CommonProperties;

//annotation de lombok pour générer les getter et setter
@Data
//annotation de lombok pour générer le constructeur sans paramètre
@NoArgsConstructor
//annotation de lombok pour générer le constructeur avec tous les paramètres
@AllArgsConstructor
//indique qu'il s'agit d'une entité=> création d'une table au niveau de la base de données
@Entity
@Table(name = "sous_groupe")
public class SousGroupe extends CommonProperties {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "code")
	private Integer code;
	@NotNull
	
	@Column(name = "libelle",unique=true)
	private String libelle;
	//relation reflexive côté fils => sousGroupe parent
	@JoinColumn(name = "parent_code", referencedColumnName = "code")
	@ManyToOne
    private SousGroupe parent;
	//annotation pour une relation de type 1-N
    @OneToMany(mappedBy = "parent", cascade = CascadeType.ALL)
    private List<SousGroupe> sous_sousGroupes = new ArrayList<>();
    @Column(name = "niveau")
    private int niveau;

}
