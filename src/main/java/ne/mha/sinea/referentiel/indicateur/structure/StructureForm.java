package ne.mha.sinea.referentiel.indicateur.structure;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StructureForm {

	private int code;
	private String libelle;
	
}
