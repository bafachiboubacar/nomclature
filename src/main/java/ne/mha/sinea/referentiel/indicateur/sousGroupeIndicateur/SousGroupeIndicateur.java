package ne.mha.sinea.referentiel.indicateur.sousGroupeIndicateur;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ne.mha.sinea.CommonProperties;
import ne.mha.sinea.referentiel.indicateur.indicateur.Indicateur;
import ne.mha.sinea.referentiel.indicateur.sousGroupe.SousGroupe;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "sous_groupe_indicateur")
public class SousGroupeIndicateur extends CommonProperties {
	
	@EmbeddedId
	private SousGroupeIndicateurKey code;
	 
	@MapsId("codeIndicateur")
	@JoinColumn(name = "code_indicateur", referencedColumnName = "code")
	@ManyToOne
	private Indicateur indicateur;
	
	@MapsId("codeSousGroupe")
	@JoinColumn(name = "code_sous_groupe", referencedColumnName = "code")
	@ManyToOne
	private SousGroupe sousGroupe;

	
	
}
