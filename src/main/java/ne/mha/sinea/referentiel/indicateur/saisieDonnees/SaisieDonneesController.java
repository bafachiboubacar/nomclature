package ne.mha.sinea.referentiel.indicateur.saisieDonnees;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ne.mha.sinea.Template;
import ne.mha.sinea.nomenclature.periode.Periode;
import ne.mha.sinea.nomenclature.periode.PeriodeRepository;
import ne.mha.sinea.nomenclature.typeClassification.TypeClassification;
import ne.mha.sinea.nomenclature.typeClassification.TypeClassificationRepository;
import ne.mha.sinea.referentiel.indicateur.classification.Classification;
import ne.mha.sinea.referentiel.indicateur.classification.ClassificationRepository;
import ne.mha.sinea.referentiel.indicateur.frame.SectionFrame;
import ne.mha.sinea.referentiel.indicateur.frame.SectionFrameRepository;
import ne.mha.sinea.referentiel.indicateur.indicateur.Indicateur;
import ne.mha.sinea.referentiel.indicateur.indicateur.IndicateurRepository;
import ne.mha.sinea.referentiel.indicateur.source.Source;
import ne.mha.sinea.referentiel.indicateur.source.SourceRepository;
import ne.mha.sinea.referentiel.zone.Zone;
import ne.mha.sinea.referentiel.zone.ZoneRepository;

@Controller
public class SaisieDonneesController {
	@Autowired
	IndicateurRepository indicateurService;
	@Autowired
	ClassificationRepository classificationService;
	@Autowired
	PeriodeRepository periodeService;
	@Autowired
	ZoneRepository zoneService;
	@Autowired
	SourceRepository sourceService;
	
	@Autowired
	TypeClassificationRepository typeClassificationService;
	
	@Autowired
	SectionFrameRepository frameService;
	
	@Autowired
	SectionFrameRepository sectionFrameService;

	//@PreAuthorize("hasAuthority('mise à jour des données')")
	@GetMapping("/parametreSaisie/{code}")
	public String  parametreSaisie(@PathVariable("code") int code,Model model) {
		try{
			TypeClassification typeClassification = typeClassificationService.findByCode(code);
			List<Classification> classifications = classificationService.findByIsDeletedFalseAndTypeClassification_CodeOrderByLibelleAsc(code);
			
			List<TypeClassification> typeClassifications = typeClassificationService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeClassifications", typeClassifications);
			
			List<SectionFrame> sectionFrames = sectionFrameService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("sectionFrames", sectionFrames);
			
			
			model.addAttribute("typeClassification", typeClassification);
			model.addAttribute("classifications", classifications);
			
			List<Indicateur> indicateurs = indicateurService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("indicateurs", indicateurs);
			
			List<Zone> zones = zoneService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("zones", zones);
			
			List<Source> sources = sourceService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("sources", sources);
			
			List<Periode> periodes = periodeService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("periodes", periodes);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Referentiel/Saisie de la série de données");
			model.addAttribute("viewPath", "referentiel/saisieDonnees/parametreSaisie");
		
		
		}
		catch(Exception e){
				
			}
		return Template.defaultTemplate;
	}
	//@PreAuthorize("hasAuthority('mise à jour des données')")	
	@PostMapping("/parametreSaisieSubmit")
	public String parametreSaisieSubmit(@RequestParam(value="paramIndicateur[]") Integer[] paramIndicateur,@RequestParam(value="paramPeriode[]") Integer[] paramPeriode,@RequestParam(value="paramZone[]") Integer[] paramZone,@RequestParam(value="paramSource[]") Integer[] paramSource,Model model) {
		try{
			List<Integer> listeIndicateur = new ArrayList<Integer>();
			for(int i=0;i<paramIndicateur.length;i++)
			{
				listeIndicateur.add(paramIndicateur[i]);
			}
			List<Indicateur> indicateurs = indicateurService.findByIsDeletedFalseAndCodeInOrderByLibelleAsc(listeIndicateur);
			model.addAttribute("indicateurs",indicateurs);
			
			List<Integer> listePeriode = new ArrayList<Integer>();
			for(int i=0;i<paramPeriode.length;i++)
			{
				listePeriode.add(paramPeriode[i]);
			}
			List<Periode> periodes = periodeService.findByIsDeletedFalseAndCodeInOrderByLibelleAsc(listePeriode);
			model.addAttribute("periodes",periodes);
			
			List<Integer> listeZone = new ArrayList<Integer>();
			for(int i=0;i<paramZone.length;i++)
			{
				listeZone.add(paramZone[i]);
			}
			List<Zone> zones = zoneService.findByIsDeletedFalseAndCodeInOrderByLibelleAsc(listeZone);
			model.addAttribute("zones",zones);
			
			List<Integer> listeSource = new ArrayList<Integer>();
			for(int i=0;i<paramSource.length;i++)
			{
				listeSource.add(paramSource[i]);
			}
			List<Source> sources = sourceService.findByIsDeletedFalseAndCodeInOrderByLibelleAsc(listeSource);
			model.addAttribute("sources",sources);
			List<TypeClassification> typeClassifications = typeClassificationService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeClassifications", typeClassifications);
			List<SectionFrame> frames = frameService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("frames", frames);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Referentiel/Saisie de la série de données");
			model.addAttribute("viewPath", "referentiel/saisieDonnees/saisieDonnees");
			
			}
		catch(Exception e){
			System.out.println(e);
			}
			
		return Template.defaultTemplate;
	}
	//@PreAuthorize("hasAuthority('mise à jour des données')")
	@PostMapping("/editParametreSaisie/{code}")
	public String  editParametreSaisie(@PathVariable("code") int code,@RequestParam(value="params[]") String[] datas,Model model) {
		try{
			TypeClassification typeClassification = typeClassificationService.findByCode(code);
			List<Classification> classifications = classificationService.findByIsDeletedFalseAndTypeClassification_CodeOrderByLibelleAsc(code);
			
			List<TypeClassification> typeClassifications = typeClassificationService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeClassifications", typeClassifications);
			
			List<SectionFrame> frames = frameService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("frames", frames);
			
			model.addAttribute("typeClassification", typeClassification);
			model.addAttribute("classifications", classifications);
			
			List<Indicateur> indicateurs = indicateurService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("indicateurs", indicateurs);
			
			List<Zone> zones = zoneService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("zones", zones);
			
			List<Source> sources = sourceService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("sources", sources);
			
			List<Periode> periodes = periodeService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("periodes", periodes);
			
			List<Integer> indicateurSelected = new ArrayList<Integer>(); 
			List<Integer> sourceSelected = new ArrayList<Integer>(); 
			List<Integer> periodeSelected = new ArrayList<Integer>(); 
			List<Integer> zoneSelected = new ArrayList<Integer>(); 
			
			for(int i=0;i<datas.length;i++)
			{
				String str = datas[i];
				String[] data = str.split("/");
				indicateurSelected.add(Integer.valueOf(data[0]));
				sourceSelected.add(Integer.valueOf(data[1]));
				periodeSelected.add(Integer.valueOf(data[2]));
				zoneSelected.add(Integer.valueOf(data[3]));
				
			}
			model.addAttribute("indicateurSelected", indicateurSelected);
			model.addAttribute("sourceSelected", sourceSelected);
			model.addAttribute("periodeSelected", periodeSelected);
			model.addAttribute("zoneSelected", zoneSelected);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Referentiel/Saisie de la série de données");
			model.addAttribute("viewPath", "referentiel/saisieDonnees/editParametreSaisie");
		
		
		}
		catch(Exception e){
				
			}
		return Template.defaultTemplate;
	}
	
}
