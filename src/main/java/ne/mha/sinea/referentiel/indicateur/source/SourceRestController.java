package ne.mha.sinea.referentiel.indicateur.source;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class SourceRestController {

	@Autowired
	SourceRepository sourceService;
	//@PreAuthorize("hasAuthority('gestion des sources')")
	@PostMapping("/source")
	public int addSource(@Validated SourceForm sourceForm,BindingResult bindingResult, Model model) {
		Source savedSource = new Source();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					Source P = new Source();
					P.setLibelle(sourceForm.getLibelle());
					savedSource = sourceService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedSource.getCode();
		
        
    }
	@PreAuthorize("hasAuthority('gestion des sources')")
	@PostMapping("/updateSource")
    public int updateSource(@Validated SourceForm sourceForm,BindingResult bindingResult, Model model) {
		Source savedSource = new Source();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					Source P = sourceService.findByCode(sourceForm.getCode());
					P.setLibelle(sourceForm.getLibelle());
					savedSource = sourceService.save(P);
					
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedSource.getCode();
		
        
    }
	@PreAuthorize("hasAuthority('gestion des sources')")
	@PostMapping("/deleteSource")
    public int deleteSource(@Validated SourceForm sourceForm,BindingResult bindingResult, Model model) {
		Source savedSource = new Source();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					Source P = sourceService.findByCode(sourceForm.getCode());
					P.setDeleted(true);
					savedSource = sourceService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedSource.getCode();
		
        
    }
}
