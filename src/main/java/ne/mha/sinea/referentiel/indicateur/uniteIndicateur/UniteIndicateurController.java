package ne.mha.sinea.referentiel.indicateur.uniteIndicateur;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
//import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import ne.mha.sinea.nomenclature.typeClassification.TypeClassification;
import ne.mha.sinea.nomenclature.typeClassification.TypeClassificationRepository;
import ne.mha.sinea.nomenclature.unite.Unite;
import ne.mha.sinea.nomenclature.unite.UniteForm;
import ne.mha.sinea.nomenclature.unite.UniteRepository;
import ne.mha.sinea.referentiel.indicateur.indicateur.Indicateur;
import ne.mha.sinea.referentiel.indicateur.indicateur.IndicateurRepository;

@Controller
public class UniteIndicateurController {
	@Autowired
	IndicateurRepository indicateurService;
	
	@Autowired
	UniteRepository uniteService;
	
	@Autowired
	UniteIndicateurRepository uniteIndicateuirService;
	
	@Autowired
	TypeClassificationRepository typeClassificationService;
	
	//@PreAuthorize("hasAuthority('gestion des indicateurs')")
	@GetMapping("/ajouterUniteIndicateur/{code}")
	public String ajouterUnitesIndicateur(@PathVariable("code") int code,UniteForm uniteForm, Model model) {
		try{
			Indicateur indicateur = indicateurService.findByCode(code);
			model.addAttribute("indicateur", indicateur);
			List<TypeClassification> typeClassifications = typeClassificationService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeClassifications", typeClassifications);
			List<UniteIndicateur> unitesIndicateur = uniteIndicateuirService.findByIsDeletedFalseAndIndicateur_Code(code);
			model.addAttribute("unitesIndicateur", unitesIndicateur);
			
			List<Unite> unitesDisponibles = new ArrayList<Unite>();
			List<Integer> unitesUtilises = new ArrayList<Integer>();
			for(int i=0;i<unitesIndicateur.size();i++)
			{
				unitesUtilises.add(unitesIndicateur.get(i).getUnite().getCode());
			}
			if(unitesUtilises.isEmpty())
				unitesDisponibles = uniteService.findByIsDeletedFalse();
			else
				unitesDisponibles = uniteService.findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(unitesUtilises);
			
			model.addAttribute("unitesDisponibles", unitesDisponibles);
		}
		catch(Exception e){
				
			}
		return "unite/ajouterUniteIndicateur";
	}
		
	
}
