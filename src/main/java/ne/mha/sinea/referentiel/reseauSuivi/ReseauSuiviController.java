package ne.mha.sinea.referentiel.reseauSuivi;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
//import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import ne.mha.sinea.Template;

import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

@Controller
public class ReseauSuiviController {

	@Autowired
	ReseauSuiviRepository reseauSuiviService;
	
	//formulaire reseauSuivi dont l'ajout , l'import et l'export
	//@PreAuthorize("hasAuthority('Ajout ReseauSuivi'))
	@GetMapping("/reseauSuiviStationHydrometrique")
	public String  addEtablissementScolaire(ReseauSuiviForm reseauSuiviForm, Model model) {
		try{
			//récuperation de la liste des reseauSuivi de la base de données
			List<ReseauSuivi> reseauSuivis = reseauSuiviService.findByIsDeletedFalse();
			//envoie de la liste récuperée à la vue à travers le modele
			model.addAttribute("reseauSuivis", reseauSuivis);
			//indiquer quelle vue va être afficher (à l'interieur du template)
			model.addAttribute("viewPath", "reseauSuivi/reseauSuivi");
			//indiquer quel menu horizontal va être afficher (à l'interieur du template)
			model.addAttribute("horizontalMenu", "ressourceEau/horizontalMenu"); 
			//indiquer quel menu vertical(sidebar) va être afficher (à l'interieur du template)
			model.addAttribute("sidebarMenu", "ressourceEau/station/sidebarMenu"); 
			//indiquer quel breadCrumb va être afficher (à l'interieur du template)
			model.addAttribute("breadcrumb", "Reseau suivi");
			}
		catch(Exception e){
				
			}
		//afficher le template avec la vue (formulaire) à l'intérieur
		return Template.defaultTemplate;

	}
	
	//validation du formulaire d'ajout reseauSuivi
	@PostMapping("/reseauSuivi")
    public String addreseauSuiviStationHydrometriqueSubmit(@Validated ReseauSuiviForm reseauSuiviForm,BindingResult bindingResult, Model model) {
    	//s'il n'ya pas d'erreur lors de la validation des données du formulaire
		if (!bindingResult.hasErrors()) {
			
			try {
				//récuperer les données saisies
				ReseauSuivi RS = new ReseauSuivi();	
				RS.setNom(reseauSuiviForm.getNom());
				RS.setProprietaire(reseauSuiviForm.getProprietaire());
				RS.setStructureEnCharge(reseauSuiviForm.getStructureEnCharge());
				RS.setCommentaire(reseauSuiviForm.getCommentaire());
				
				reseauSuiviService.save(RS);

				//indiquer que l'operation d'ajout a réussie
				model.addAttribute("operationStatus", "operationStatus/success");
				//récuperation de la liste des reseauSuivi de la base de données
				List<ReseauSuivi> reseauSuivis = reseauSuiviService.findByIsDeletedFalse();
				//envoie de la liste récuperée à la vue à travers le modele
				model.addAttribute("reseauSuivis", reseauSuivis);
				//indiquer quelle vue va être afficher (à l'interieur du template)
				model.addAttribute("viewPath", "ressourceEau/station/form/reseauSuiviStationHydrometrique");
				//indiquer quel menu horizontal va être afficher (à l'interieur du template)
				model.addAttribute("horizontalMenu", "ressourceEau/horizontalMenu"); 
				//indiquer quel menu vertical(sidebar) va être afficher (à l'interieur du template)
				model.addAttribute("sidebarMenu", "ressourceEau/station/sidebarMenu"); 
				//indiquer quel breadCrumb va être afficher (à l'interieur du template)
				model.addAttribute("breadcrumb", "ressourceEau/breadcrumb");
				
				}
			catch(Exception e){
				//indiquer que l'operation d'ajout a réussie
				model.addAttribute("operationStatus", "operationStatus/unsuccess");
				//récuperation de la liste des reseauSuivi de la base de données
				List<ReseauSuivi> reseauSuivis =  reseauSuiviService.findByIsDeletedFalse();
				//envoie de la liste récuperée à la vue à travers le modele
				model.addAttribute("reseauSuivis", reseauSuivis);
				//indiquer quelle vue va être afficher (à l'interieur du template)
				model.addAttribute("viewPath", "ressourceEau/station/form/reseauSuiviStationHydrometrique");
				//indiquer quel menu horizontal va être afficher (à l'interieur du template)
				model.addAttribute("horizontalMenu", "ressourceEau/horizontalMenu"); 
				//indiquer quel menu vertical(sidebar) va être afficher (à l'interieur du template)
				model.addAttribute("sidebarMenu", "ressourceEau/station/sidebarMenu"); 
				//indiquer quel breadCrumb va être afficher (à l'interieur du template)
				model.addAttribute("breadcrumb", "ressourceEau/breadcrumb");
				
					}
			
    	}
		else{
    		try{
    			
    			//indiquer que l'operation d'ajout a réussie
				model.addAttribute("operationStatus", "operationStatus/unsuccess");
				//récuperation de la liste des reseauSuivi de la base de données
				List<ReseauSuivi> reseauSuivis = reseauSuiviService.findByIsDeletedFalse();
				//envoie de la liste récuperée à la vue à travers le modele
				model.addAttribute("reseauSuivis", reseauSuivis);
				//indiquer quelle vue va être afficher (à l'interieur du template)
				model.addAttribute("viewPath", "ressourceEau/station/form/reseauSuiviStationHydrometrique");
				//indiquer quel menu horizontal va être afficher (à l'interieur du template)
				model.addAttribute("horizontalMenu", "ressourceEau/horizontalMenu"); 
				//indiquer quel menu vertical(sidebar) va être afficher (à l'interieur du template)
				model.addAttribute("sidebarMenu", "ressourceEau/station/sidebarMenu"); 
				//indiquer quel breadCrumb va être afficher (à l'interieur du template)
				model.addAttribute("breadcrumb", "ressourceEau/breadcrumb");
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		//afficher le template avec la vue (formulaire) à l'intérieur
		return Template.defaultTemplate;
        
    }
	
	
	//modification d'une reseauSuivi
	@GetMapping("/updatereseauSuiviStationHydrometrique/{idReseauSuivi}")
    public String updatereseauSuiviStationHydrometrique(@PathVariable("idReseauSuivi") int codeReseauSuivi,ReseauSuiviForm reseauSuiviForm, Model model) 
	{ 
		try
		{
			//récuperer les informations de la reseauSuivi à modifier
			ReseauSuivi RS =   reseauSuiviService.findByCode(codeReseauSuivi);
    		reseauSuiviForm.setNom(RS.getNom());
    		reseauSuiviForm.setProprietaire(RS.getProprietaire());
    		reseauSuiviForm.setStructureEnCharge(RS.getStructureEnCharge());
    		reseauSuiviForm.setCommentaire(RS.getCommentaire());

			//récuperation de la liste des reseauSuivi de la base de données
    		List<ReseauSuivi> reseauSuivis = reseauSuiviService.findByIsDeletedFalse();
			//envoie de la liste récuperée à la vue à travers le modele
			model.addAttribute("reseauSuivis", reseauSuivis);
			//indiquer quelle vue va être afficher (à l'interieur du template)
			model.addAttribute("viewPath", "ressourceEau/station/form/updatereseauSuiviStationHydrometrique");
			//indiquer quel menu horizontal va être afficher (à l'interieur du template)
			model.addAttribute("horizontalMenu", "ressourceEau/horizontalMenu"); 
			//indiquer quel menu vertical(sidebar) va être afficher (à l'interieur du template)
			model.addAttribute("sidebarMenu", "ressourceEau/station/sidebarMenu"); 
			//indiquer quel breadCrumb va être afficher (à l'interieur du template)
			model.addAttribute("breadcrumb", "ressourceEau/breadcrumb");
		}
		catch(Exception e)
		{
			
		}
		return Template.defaultTemplate;
    		
    }
	
	@PostMapping("/updateReseauSuivi/{idReseauSuivi}")
    public RedirectView updatereseauSuiviStationHydrometriqueSubmit(@Validated ReseauSuiviForm reseauSuiviForm,BindingResult bindingResult,@PathVariable("idReseauSuivi") int codeReseauSuivi, Model model,RedirectAttributes redirectAttributes) 
	{
		final RedirectView redirectView = new RedirectView("/reseauSuivi", true);
    	if (!bindingResult.hasErrors()) 
    	{
			try
			{
				//recupérer les informations saisies
				ReseauSuivi RS = reseauSuiviService.findByCode(codeReseauSuivi);
				RS.setNom(reseauSuiviForm.getNom());
				RS.setProprietaire(reseauSuiviForm.getProprietaire());
				RS.setStructureEnCharge(reseauSuiviForm.getStructureEnCharge());
				RS.setCommentaire(reseauSuiviForm.getCommentaire());
				reseauSuiviService.save(RS);
				
				//indiquer que l'operation de modification a réussi
				redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/success");
				
			}
			catch(Exception e)
			{
				//indiquer que l'operation de modification a échoué
				redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/unsuccess");
			}
    	}
    	else
    	{
    		
    	}
		
    	//redirection vers la page d'ajout
		return redirectView;
        
    }
	
	@GetMapping("/deleteReseauSuivi/{idReseauSuivi}")
	public RedirectView deletereseauSuiviStationHydrometrique(@PathVariable("idReseauSuivi") int codeReseauSuivi, Model model,RedirectAttributes redirectAttributes) 
	{ 
		final RedirectView redirectView = new RedirectView("/reseauSuivi", true);
		try
		{
			ReseauSuivi RS = reseauSuiviService.findByCode(codeReseauSuivi);
			RS.setDeleted(true);
			reseauSuiviService.save(RS);
			
			//indiquer que l'operation de suppression a réussi
			redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/success");
		}
		catch(Exception e)
		{
			//indiquer que l'operation de suppression a échoué
			redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/unsuccess");
		}
		
		//redirection vers la page d'ajout
		return redirectView;
	    		
	}

	
}
