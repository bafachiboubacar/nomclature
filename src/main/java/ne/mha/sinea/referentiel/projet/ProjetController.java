package ne.mha.sinea.referentiel.projet;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class ProjetController {

	@Autowired
	ProjetRepository projetService;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/projet")
	public String  addProjet(ProjetForm projetForm, Model model) {
		try{
			List<Projet> projets = projetService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("projets", projets);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Unité");
			model.addAttribute("viewPath", "referentiel/projet/projet");
			
			}
		catch(Exception e){
				
			}
		
		return Template.defaultTemplate;

	}
	
	
}
