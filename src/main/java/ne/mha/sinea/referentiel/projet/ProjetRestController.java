package ne.mha.sinea.referentiel.projet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class ProjetRestController {

	@Autowired
	ProjetRepository projetService;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/projet")
	public int addProjet(@Validated ProjetForm projetForm,BindingResult bindingResult, Model model) {
		Projet savedProjet = new Projet();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					Projet P = new Projet();
					P.setLibelle(projetForm.getLibelle());
					savedProjet = projetService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedProjet.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateProjet")
    public int updateProjet(@Validated ProjetForm projetForm,BindingResult bindingResult, Model model) {
		Projet savedProjet = new Projet();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					Projet P = projetService.findByCode(projetForm.getCode());
					P.setLibelle(projetForm.getLibelle());
					savedProjet = projetService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedProjet.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteProjet")
    public int deleteProjet(@Validated ProjetForm projetForm,BindingResult bindingResult, Model model) {
		Projet savedProjet = new Projet();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					Projet P = projetService.findByCode(projetForm.getCode());
					P.setDeleted(true);
					savedProjet = projetService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedProjet.getCode();
		
        
    }
}
