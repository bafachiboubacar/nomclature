package ne.mha.sinea.referentiel.lieuPublic;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ne.mha.sinea.CommonProperties;
import ne.mha.sinea.nomenclature.typeLieuPublic.TypeLieuPublic;
import ne.mha.sinea.referentiel.zone.Localite;

//annotation de lombok pour générer les getter et setter
@Data
//annotation de lombok pour générer le constructeur sans paramètre
@NoArgsConstructor
//annotation de lombok pour générer le constructeur avec tous les paramètres
@AllArgsConstructor
//indique qu'il s'agit d'une entité=> création d'une table au niveau de la base de données
@Entity
@Table(name = "lieu_public")

public class LieuPublic extends CommonProperties {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "code")
	private Integer code;
	// annotation pour indiquer que l'attribut ne doit pas être null
	@NotNull
	@Column(name = "denomination")
	private String denomination;
	@Column(name = "latitude")
	private double latitude;
	@Column(name = "longitude")
	private double longitude;
	@Column(name = "annee_creation")
	private int anneeCreation;
	@JoinColumn(name = "code_type_lieu_public", referencedColumnName = "code")
	@ManyToOne
	private TypeLieuPublic typeLieuPublic;
	@ManyToOne
	@JoinColumn(name = "code_localite", referencedColumnName = "code")
	private Localite localite;
}
