package ne.mha.sinea.referentiel.lieuPublic;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import ne.mha.sinea.Template;
import ne.mha.sinea.nomenclature.typeLieuPublic.TypeLieuPublic;
import ne.mha.sinea.nomenclature.typeLieuPublic.TypeLieuPublicRepository;
import ne.mha.sinea.referentiel.zone.Localite;
import ne.mha.sinea.referentiel.zone.LocaliteRepository;

@Controller
public class LieuPublicController {

	@Autowired
	LieuPublicRepository lieuPublicService;
	@Autowired
	TypeLieuPublicRepository typeLieuPublicService;
	@Autowired
	LocaliteRepository localiteService;

	//@PreAuthorize("hasAuthority('Ajout lieuPublic'))
	@GetMapping("/lieuPublic")
	public String  addLieuPublic(LieuPublicForm lieuPublicForm, Model model) {
		try{
			//récuperation de la liste des lieuPublic de la base de données et envoie de la liste récuperée à la vue à travers le modele
			List<LieuPublic> lieuPublics =  (List<LieuPublic>) lieuPublicService.findByIsDeletedFalse();
			model.addAttribute("lieuPublics", lieuPublics);
			List<TypeLieuPublic> typeLieuPublics =  (List<TypeLieuPublic>) typeLieuPublicService.findByIsDeletedFalse();
			model.addAttribute("typeLieuPublics", typeLieuPublics);
			List<Localite> localites =  (List<Localite>) localiteService.findByIsDeletedFalse();
			model.addAttribute("localites", localites);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Referentiel/Type Lieu Public");
			model.addAttribute("viewPath", "referentiel/lieuPublic/lieuPublic");
			
			}
		catch(Exception e){
				
			}
		//afficher le template avec la vue (formulaire) à l'intérieur
		return Template.defaultTemplate;

	}
	
	//validation du formulaire d'ajout lieuPublic
	@PostMapping("/lieuPublic")
    public String addLieuPublicSubmit(@Validated LieuPublicForm lieuPublicForm,BindingResult bindingResult, Model model) {
    	//s'il n'ya pas d'erreur lors de la validation des données du formulaire
		if (!bindingResult.hasErrors()) {
			
			try {
				//récuperer les données saisies
				LieuPublic EP = new LieuPublic();
				
				EP.setDenomination(lieuPublicForm.getDenomination());
				EP.setLatitude(lieuPublicForm.getLatitude());
				EP.setLongitude(lieuPublicForm.getLongitude());
				EP.setTypeLieuPublic(typeLieuPublicService.findByCode(lieuPublicForm.getCodeTypeLieuPublic()));
				EP.setLocalite(localiteService.findByCode(lieuPublicForm.getCodeTypeLieuPublic()));
				
				lieuPublicService.save(EP);
				
				//récuperation de la liste des lieuPublic de la base de données et envoie de la liste récuperée à la vue à travers le modele
				List<LieuPublic> lieuPublics =  (List<LieuPublic>) lieuPublicService.findByIsDeletedFalse();
				model.addAttribute("lieuPublics", lieuPublics);
				List<TypeLieuPublic> typeLieuPublics =  (List<TypeLieuPublic>) typeLieuPublicService.findByIsDeletedFalse();
				model.addAttribute("typeLieuPublics", typeLieuPublics);
				List<Localite> localites =  (List<Localite>) localiteService.findByIsDeletedFalse();
				model.addAttribute("localites", localites);
				
				//envoie de la liste récuperée à la vue à travers le modele
				model.addAttribute("lieuPublics", lieuPublics);
				//indiquer que l'operation d'ajout a réussie
				model.addAttribute("operationStatus", "operationStatus/success");
				model.addAttribute("horizontalMenu", "horizontalMenu");
				model.addAttribute("sidebarMenu", "configurationSidebarMenu");
				model.addAttribute("breadcrumb", "breadcrumb");
				model.addAttribute("navigationPath", "Referentiel/Type Lieu Public");
				model.addAttribute("viewPath", "referentiel/lieuPublic/lieuPublic");
				
				}
			catch(Exception e){
					//récuperation de la liste des lieuPublic de la base de données et envoie de la liste récuperée à la vue à travers le modele
					List<LieuPublic> lieuPublics =  (List<LieuPublic>) lieuPublicService.findByIsDeletedFalse();
					model.addAttribute("lieuPublics", lieuPublics);
					List<TypeLieuPublic> typeLieuPublics =  (List<TypeLieuPublic>) typeLieuPublicService.findByIsDeletedFalse();
					model.addAttribute("typeLieuPublics", typeLieuPublics);
					List<Localite> localites =  (List<Localite>) localiteService.findByIsDeletedFalse();
					model.addAttribute("localites", localites);
					
					model.addAttribute("operationStatus", "operationStatus/unsuccess");
					model.addAttribute("horizontalMenu", "horizontalMenu");
					model.addAttribute("sidebarMenu", "configurationSidebarMenu");
					model.addAttribute("breadcrumb", "breadcrumb");
					model.addAttribute("navigationPath", "Referentiel/Type Lieu Public");
					model.addAttribute("viewPath", "referentiel/lieuPublic/lieuPublic");
					
					}
			
    	}
		else{
    		try{
    			
    			//récuperation de la liste des lieuPublic de la base de données et envoie de la liste récuperée à la vue à travers le modele
    			List<LieuPublic> lieuPublics =  (List<LieuPublic>) lieuPublicService.findByIsDeletedFalse();
    			model.addAttribute("lieuPublics", lieuPublics);
    			List<TypeLieuPublic> typeLieuPublics =  (List<TypeLieuPublic>) typeLieuPublicService.findByIsDeletedFalse();
    			model.addAttribute("typeLieuPublics", typeLieuPublics);
    			List<Localite> localites =  (List<Localite>) localiteService.findByIsDeletedFalse();
    			model.addAttribute("localites", localites);
    			model.addAttribute("operationStatus", "operationStatus/unsuccess");
    			
				model.addAttribute("horizontalMenu", "horizontalMenu");
				model.addAttribute("sidebarMenu", "configurationSidebarMenu");
				model.addAttribute("breadcrumb", "breadcrumb");
				model.addAttribute("navigationPath", "Referentiel/Type Lieu Public");
				model.addAttribute("viewPath", "referentiel/lieuPublic/lieuPublic");
				
				}
    		catch(Exception e){
				
				}
    	}
		
		//afficher le template avec la vue (formulaire) à l'intérieur
		return Template.defaultTemplate;
        
    }
	
	//modification d'une lieuPublic
	@GetMapping("/updateLieuPublic/{code}")
    public String updateLieuPublic(@PathVariable("code") int code,LieuPublicForm lieuPublicForm, Model model) 
	{ 
		try
		{
			//récuperer les informations de la lieuPublic à modifier
    		LieuPublic EP =   lieuPublicService.findByCode(code);
    		//envoie des informations de la lieuPublic à modifier à travers le modele
    		EP.setDenomination(lieuPublicForm.getDenomination());
			EP.setLatitude(lieuPublicForm.getLatitude());
			EP.setLongitude(lieuPublicForm.getLongitude());
			EP.setTypeLieuPublic(typeLieuPublicService.findByCode(lieuPublicForm.getCodeTypeLieuPublic()));
			EP.setLocalite(localiteService.findByCode(lieuPublicForm.getCodeTypeLieuPublic()));
			
			List<LieuPublic> lieuPublics =  (List<LieuPublic>) lieuPublicService.findByIsDeletedFalse();
			model.addAttribute("lieuPublics", lieuPublics);
			List<TypeLieuPublic> typeLieuPublics =  (List<TypeLieuPublic>) typeLieuPublicService.findByIsDeletedFalse();
			model.addAttribute("typeLieuPublics", typeLieuPublics);
			List<Localite> localites =  (List<Localite>) localiteService.findByIsDeletedFalse();
			model.addAttribute("localites", localites);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Referentiel/Type Lieu Public");
			model.addAttribute("viewPath", "referentiel/lieuPublic/lieuPublic");
			
		}
		catch(Exception e)
		{
			
		}
		return Template.defaultTemplate;
    		
    }
	
	@PostMapping("/updateLieuPublic/{code}")
    public RedirectView updateLieuPublicSubmit(@Validated LieuPublicForm lieuPublicForm,BindingResult bindingResult,@PathVariable("code") int code, Model model,RedirectAttributes redirectAttributes) 
	{
		final RedirectView redirectView = new RedirectView("/lieuPublic", true);
    	if (!bindingResult.hasErrors()) 
    	{
			try
			{
				//recupérer les informations saisies
				LieuPublic EP = lieuPublicService.findByCode(code);
				//envoie des informations de la lieuPublic à modifier à travers le modele
	    		EP.setDenomination(lieuPublicForm.getDenomination());
				EP.setLatitude(lieuPublicForm.getLatitude());
				EP.setLongitude(lieuPublicForm.getLongitude());
				EP.setTypeLieuPublic(typeLieuPublicService.findByCode(lieuPublicForm.getCodeTypeLieuPublic()));
				EP.setLocalite(localiteService.findByCode(lieuPublicForm.getCodeTypeLieuPublic()));
				//enregistrer les données (l'entité=) dans la base
				lieuPublicService.save(EP);
				
				//indiquer que l'operation de modification a réussi
				redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/success");
				
			}
			catch(Exception e)
			{
				//indiquer que l'operation de modification a échoué
				redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/unsuccess");
			}
    	}
    	else
    	{
    		
    	}
		
    	//redirection vers la page d'ajout
		return redirectView;
        
    }
	
	@GetMapping("/deleteLieuPublic/{code}")
	public RedirectView deleteLieuPublic(@PathVariable("code") int code, Model model,RedirectAttributes redirectAttributes) 
	{ 
		final RedirectView redirectView = new RedirectView("/lieuPublic", true);
		try
		{
			LieuPublic EP = lieuPublicService.findByCode(code);
			EP.setDeleted(true);
			lieuPublicService.save(EP);
			
			//indiquer que l'operation de suppression a réussi
			redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/success");
		}
		catch(Exception e)
		{
			//indiquer que l'operation de suppression a échoué
			redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/unsuccess");
		}
		
		//redirection vers la page d'ajout
		return redirectView;
	    		
	}

	
}
