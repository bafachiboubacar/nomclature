package ne.mha.sinea.referentiel.lieuPublic;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LieuPublicForm {

	private String denomination;
	private double latitude;
	private double longitude;
	private int anneeCreation;
	private Integer codeTypeLieuPublic;
	private Integer codeLocalite;
}
