package ne.mha.sinea.nomenclature.financement;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class FinancementController {

	@Autowired
	FinancementRepository financementService;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/financement")
	public String  addFinancement(FinancementForm financementForm, Model model) {
		try{
			List<Financement> financements = financementService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("financements", financements);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Nomenclature/Unité");
			model.addAttribute("viewPath", "nomenclature/financement/financement");
			
			}
		catch(Exception e){
				
			}
		
		return Template.defaultTemplate;

	}
	
	
}
