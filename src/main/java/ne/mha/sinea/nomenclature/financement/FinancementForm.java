package ne.mha.sinea.nomenclature.financement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FinancementForm {

	private int code;
	private String libelle;
	
}
