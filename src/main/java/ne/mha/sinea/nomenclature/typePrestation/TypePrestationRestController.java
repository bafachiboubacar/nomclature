package ne.mha.sinea.nomenclature.typePrestation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class TypePrestationRestController {

	@Autowired
	TypePrestationRepository typePrestationService;
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/typePrestation")
	public int addTypePrestation(@Validated TypePrestationForm typePrestationForm,BindingResult bindingResult, Model model) {
		TypePrestation savedTypePrestation = new TypePrestation();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypePrestation P = new TypePrestation();
					P.setLibelle(typePrestationForm.getLibelle());
					savedTypePrestation = typePrestationService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypePrestation.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/updateTypePrestation")
    public int updateTypePrestation(@Validated TypePrestationForm typePrestationForm,BindingResult bindingResult, Model model) {
		TypePrestation savedTypePrestation = new TypePrestation();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypePrestation P = typePrestationService.findByCode(typePrestationForm.getCode());
					P.setLibelle(typePrestationForm.getLibelle());
					savedTypePrestation = typePrestationService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypePrestation.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/deleteTypePrestation")
    public int deleteTypePrestation(@Validated TypePrestationForm typePrestationForm,BindingResult bindingResult, Model model) {
		TypePrestation savedTypePrestation = new TypePrestation();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypePrestation P = typePrestationService.findByCode(typePrestationForm.getCode());
					P.setDeleted(true);
					savedTypePrestation = typePrestationService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypePrestation.getCode();
		
        
    }
}
