package ne.mha.sinea.nomenclature.typeEquipementStationHydrometrique;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;
@Controller
public class TypeEquipementStationHydrometriqueController {

	@Autowired
	TypeEquipementStationHydrometriqueRepository typeEquipementStationHydrometriqueService;
	
	//@PreAuthorize("hasAuthority('gestion des types equipements station hydrometrique')")
	@GetMapping("/typeEquipementStationHydrometrique")
	public String  addTypeEquipementStationHydrometrique(TypeEquipementStationHydrometriqueForm typeEquipementStationHydrometriqueForm, Model model) {
		try{
			List<TypeEquipementStationHydrometrique> typeEquipementStationHydrometriques = typeEquipementStationHydrometriqueService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeEquipementStationHydrometriques", typeEquipementStationHydrometriques);
			model.addAttribute("viewPath", "nomenclature/typeEquipementStationHydrometrique/typeEquipementStationHydrometrique");
			
			}
		catch(Exception e){
				
			}
		
		return Template.defaultTemplate;

	}
	
	
}
