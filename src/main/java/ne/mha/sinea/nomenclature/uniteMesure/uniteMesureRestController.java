package ne.mha.sinea.nomenclature.uniteMesure;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class uniteMesureRestController {

	@Autowired
	uniteMesureRepository uniteMesureRepository;
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/uniteMesure")
	public int addUniteMesure(@Validated uniteMesureForm uniteMesureForm,BindingResult bindingResult, Model model) {
		uniteMesure savedUniteMesure = new uniteMesure();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					uniteMesure P = new uniteMesure();
					P.setLibelle(uniteMesureForm.getLibelle());
					savedUniteMesure = uniteMesureRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedUniteMesure.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/updateUniteMesure")
    public int updateUniteMesure(@Validated uniteMesureForm uniteMesureForm,BindingResult bindingResult, Model model) {
		uniteMesure savedUniteMesure = new uniteMesure();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					uniteMesure P = uniteMesureRepository.findByCode(uniteMesureForm.getCode());
					P.setLibelle(uniteMesureForm.getLibelle());
					savedUniteMesure = uniteMesureRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedUniteMesure.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/deleteUniteMesure")
    public int deleteUniteMesure(@Validated uniteMesureForm uniteMesureForm,BindingResult bindingResult, Model model) {
		uniteMesure savedUniteMesure = new uniteMesure();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					uniteMesure P = uniteMesureRepository.findByCode(uniteMesureForm.getCode());
					P.setDeleted(true);
					savedUniteMesure = uniteMesureRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedUniteMesure.getCode();
		
        
    }
}
