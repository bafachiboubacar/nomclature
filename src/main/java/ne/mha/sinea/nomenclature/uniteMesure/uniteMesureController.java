package ne.mha.sinea.nomenclature.uniteMesure;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;
@Controller
public class uniteMesureController {

	@Autowired
	uniteMesureRepository uniteMesureRepository;
	
	//@PreAuthorize("hasAuthority('gestion des types amenagement')")
	@GetMapping("/uniteMesure")
	public String  addUniteMesure(uniteMesureForm uniteMesureForm, Model model) {
		try{
			List<uniteMesure> uniteMesure = uniteMesureRepository.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("uniteMesure", uniteMesure);
			model.addAttribute("viewPath", "nomenclature/uniteMesureu/uniteMesure");
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
}
