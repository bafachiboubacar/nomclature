package ne.mha.sinea.nomenclature.etatOuvrage;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EtatOuvrageForm {

	private int code;
	private String libelle;
	
}
