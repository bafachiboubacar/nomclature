package ne.mha.sinea.nomenclature.etatOuvrage;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;
@Controller
public class EtatOuvrageController {

	@Autowired
	EtatOuvrageRepository etatOuvrageService;
	
	//@PreAuthorize("hasAuthority('gestion des types ressources')")
	@GetMapping("/etatOuvrage")
	public String  addEtatOuvrage(EtatOuvrageForm etatOuvrageForm, Model model) {
		try{
			List<EtatOuvrage> etatOuvrages = etatOuvrageService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("etatOuvrages", etatOuvrages);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Etat Ouvrage");
			model.addAttribute("viewPath", "nomenclature/etatOuvrage/etatOuvrage");
			
			}
		catch(Exception e){
				
			}
		
		return Template.defaultTemplate;

	}
	
	
}
