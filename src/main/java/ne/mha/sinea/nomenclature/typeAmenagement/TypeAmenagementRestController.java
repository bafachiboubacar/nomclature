package ne.mha.sinea.nomenclature.typeAmenagement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class TypeAmenagementRestController {

	@Autowired
	TypeAmenagementRepository typeAmenagementService;
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/typeAmenagement")
	public int addTypeAmenagement(@Validated TypeAmenagementForm typeAmenagementForm,BindingResult bindingResult, Model model) {
		TypeAmenagement savedTypeAmenagement = new TypeAmenagement();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeAmenagement P = new TypeAmenagement();
					P.setLibelle(typeAmenagementForm.getLibelle());
					savedTypeAmenagement = typeAmenagementService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeAmenagement.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/updateTypeAmenagement")
    public int updateTypeAmenagement(@Validated TypeAmenagementForm typeAmenagementForm,BindingResult bindingResult, Model model) {
		TypeAmenagement savedTypeAmenagement = new TypeAmenagement();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeAmenagement P = typeAmenagementService.findByCode(typeAmenagementForm.getCode());
					P.setLibelle(typeAmenagementForm.getLibelle());
					savedTypeAmenagement = typeAmenagementService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeAmenagement.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/deleteTypeAmenagement")
    public int deleteTypeAmenagement(@Validated TypeAmenagementForm typeAmenagementForm,BindingResult bindingResult, Model model) {
		TypeAmenagement savedTypeAmenagement = new TypeAmenagement();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeAmenagement P = typeAmenagementService.findByCode(typeAmenagementForm.getCode());
					P.setDeleted(true);
					savedTypeAmenagement = typeAmenagementService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeAmenagement.getCode();
		
        
    }
}
