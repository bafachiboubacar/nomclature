package ne.mha.sinea.nomenclature.typeAmenagement;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;
@Controller
public class TypeAmenagementController {

	@Autowired
	TypeAmenagementRepository typeAmenagementService;
	
	//@PreAuthorize("hasAuthority('gestion des types amenagement')")
	@GetMapping("/typeAmenagement")
	public String  addTypeAmenagement(TypeAmenagementForm typeAmenagementForm, Model model) {
		try{
			List<TypeAmenagement> typeAmenagements = typeAmenagementService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeAmenagements", typeAmenagements);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Nomenclature/Type Amenagement");
			model.addAttribute("viewPath", "nomenclature/typeAmenagement/typeAmenagement");
			
			}
		catch(Exception e){
				
			}
		
		return Template.defaultTemplate;

	}
	
	
}
