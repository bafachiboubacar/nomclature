package ne.mha.sinea.nomenclature.hydraulique;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class HydrauliqueRestController {

	@Autowired
	HydrauliqueRepository hydrauliqueService;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/hydraulique")
	public int addHydraulique(@Validated HydrauliqueForm hydrauliqueForm,BindingResult bindingResult, Model model) {
		Hydraulique savedHydraulique = new Hydraulique();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					Hydraulique P = new Hydraulique();
					P.setLibelle(hydrauliqueForm.getLibelle());
					savedHydraulique = hydrauliqueService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedHydraulique.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PutMapping("/updateHydraulique")
    public int updateHydraulique(@Validated HydrauliqueForm hydrauliqueForm,BindingResult bindingResult, Model model) {
		Hydraulique savedHydraulique = new Hydraulique();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					Hydraulique P = hydrauliqueService.findByCode(hydrauliqueForm.getCode());
					P.setLibelle(hydrauliqueForm.getLibelle());
					savedHydraulique = hydrauliqueService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedHydraulique.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@DeleteMapping("/deleteHydraulique")
    public int deleteHydraulique(@Validated HydrauliqueForm hydrauliqueForm,BindingResult bindingResult, Model model) {
		Hydraulique savedHydraulique = new Hydraulique();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					Hydraulique P = hydrauliqueService.findByCode(hydrauliqueForm.getCode());
					P.setDeleted(true);
					savedHydraulique = hydrauliqueService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedHydraulique.getCode();
		
        
    }
}
