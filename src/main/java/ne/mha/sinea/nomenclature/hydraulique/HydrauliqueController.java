package ne.mha.sinea.nomenclature.hydraulique;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class HydrauliqueController {

	@Autowired
	HydrauliqueRepository hydrauliqueService;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/hydraulique")
	public String  listHydraulique(HydrauliqueForm hydrauliqueForm, Model model) {
		try{
			List<Hydraulique> hydrauliques = hydrauliqueService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("hydrauliques", hydrauliques);
			model.addAttribute("viewPath", "nomenclature/hydraulique/hydraulique");
			
			}
		catch(Exception e){
				
			}
		
		return Template.defaultTemplate;

	}
	
	
}
