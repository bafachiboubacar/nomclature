package ne.mha.sinea.nomenclature.typeOuvrage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class TypeOuvrageRestController {

	@Autowired
	TypeOuvrageRepository typeOuvrageService;
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/typeOuvrage")
	public int addTypeOuvrage(@Validated TypeOuvrageForm typeOuvrageForm,BindingResult bindingResult, Model model) {
		TypeOuvrage savedTypeOuvrage = new TypeOuvrage();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeOuvrage P = new TypeOuvrage();
					P.setLibelle(typeOuvrageForm.getLibelle());
					savedTypeOuvrage = typeOuvrageService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeOuvrage.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/updateTypeOuvrage")
    public int updateTypeOuvrage(@Validated TypeOuvrageForm typeOuvrageForm,BindingResult bindingResult, Model model) {
		TypeOuvrage savedTypeOuvrage = new TypeOuvrage();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeOuvrage P = typeOuvrageService.findByCode(typeOuvrageForm.getCode());
					P.setLibelle(typeOuvrageForm.getLibelle());
					savedTypeOuvrage = typeOuvrageService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeOuvrage.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/deleteTypeOuvrage")
    public int deleteTypeOuvrage(@Validated TypeOuvrageForm typeOuvrageForm,BindingResult bindingResult, Model model) {
		TypeOuvrage savedTypeOuvrage = new TypeOuvrage();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeOuvrage P = typeOuvrageService.findByCode(typeOuvrageForm.getCode());
					P.setDeleted(true);
					savedTypeOuvrage = typeOuvrageService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeOuvrage.getCode();
		
        
    }
}
