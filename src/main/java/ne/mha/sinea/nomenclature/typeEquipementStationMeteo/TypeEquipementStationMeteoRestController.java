package ne.mha.sinea.nomenclature.typeEquipementStationMeteo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class TypeEquipementStationMeteoRestController {

	@Autowired
	TypeEquipementStationMeteoRepository typeEquipementStationMeteoService;
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/typeEquipementStationMeteo")
	public int addTypeEquipementStationMeteo(@Validated TypeEquipementStationMeteoForm typeEquipementStationMeteoForm,BindingResult bindingResult, Model model) {
		TypeEquipementStationMeteo savedTypeEquipementStationMeteo = new TypeEquipementStationMeteo();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeEquipementStationMeteo P = new TypeEquipementStationMeteo();
					P.setLibelle(typeEquipementStationMeteoForm.getLibelle());
					savedTypeEquipementStationMeteo = typeEquipementStationMeteoService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeEquipementStationMeteo.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/updateTypeEquipementStationMeteo")
    public int updateTypeEquipementStationMeteo(@Validated TypeEquipementStationMeteoForm typeEquipementStationMeteoForm,BindingResult bindingResult, Model model) {
		TypeEquipementStationMeteo savedTypeEquipementStationMeteo = new TypeEquipementStationMeteo();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeEquipementStationMeteo P = typeEquipementStationMeteoService.findByCode(typeEquipementStationMeteoForm.getCode());
					P.setLibelle(typeEquipementStationMeteoForm.getLibelle());
					savedTypeEquipementStationMeteo = typeEquipementStationMeteoService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeEquipementStationMeteo.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/deleteTypeEquipementStationMeteo")
    public int deleteTypeEquipementStationMeteo(@Validated TypeEquipementStationMeteoForm typeEquipementStationMeteoForm,BindingResult bindingResult, Model model) {
		TypeEquipementStationMeteo savedTypeEquipementStationMeteo = new TypeEquipementStationMeteo();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeEquipementStationMeteo P = typeEquipementStationMeteoService.findByCode(typeEquipementStationMeteoForm.getCode());
					P.setDeleted(true);
					savedTypeEquipementStationMeteo = typeEquipementStationMeteoService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeEquipementStationMeteo.getCode();
		
        
    }
}
