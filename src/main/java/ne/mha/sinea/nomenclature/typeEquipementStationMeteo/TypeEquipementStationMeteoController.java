package ne.mha.sinea.nomenclature.typeEquipementStationMeteo;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;
@Controller
public class TypeEquipementStationMeteoController {

	@Autowired
	TypeEquipementStationMeteoRepository typeEquipementStationMeteoService;
	
	//@PreAuthorize("hasAuthority('gestion des Type Equipement Station Meteo')")
	@GetMapping("/typeEquipementStationMeteo")
	public String  addTypeEquipementStationMeteo(TypeEquipementStationMeteoForm typeEquipementStationMeteoForm, Model model) {
		try{
			List<TypeEquipementStationMeteo> typeEquipementStationMeteos = typeEquipementStationMeteoService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeEquipementStationMeteos", typeEquipementStationMeteos);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Nomenclature/Type Equipement Station Meteo");
			model.addAttribute("viewPath", "nomenclature/typeEquipementStationMeteo/typeEquipementStationMeteo");
			
			}
		catch(Exception e){
				
			}
		
		return Template.defaultTemplate;

	}
	
	
}
