package ne.mha.sinea.nomenclature.typeAnalyse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class typeAnalyseRestController {

	@Autowired
	typeAnalyseRepository typeAnalyseRepository;
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/typeAnalyse")
	public int addTypeAnalyse(@Validated typeAnalyseForm typeAnalyseForm,BindingResult bindingResult, Model model) {
		typeAnalyse savedTypeAnalyse = new typeAnalyse();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					typeAnalyse P = new typeAnalyse();
					P.setLibelle(typeAnalyseForm.getLibelle());
					savedTypeAnalyse = typeAnalyseRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeAnalyse.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/updateTypeAnalyse")
    public int updateTypeAnalyse(@Validated typeAnalyseForm typeAnalyseForm,BindingResult bindingResult, Model model) {
		typeAnalyse savedTypeAnalyse = new typeAnalyse();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					typeAnalyse P = typeAnalyseRepository.findByCode(typeAnalyseForm.getCode());
					P.setLibelle(typeAnalyseForm.getLibelle());
					savedTypeAnalyse = typeAnalyseRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeAnalyse.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/deleteTypeAnalyse")
    public int deleteTypeAnalyse(@Validated typeAnalyseForm typeAnalyseForm,BindingResult bindingResult, Model model) {
		typeAnalyse savedTypeAnalyse = new typeAnalyse();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					typeAnalyse P = typeAnalyseRepository.findByCode(typeAnalyseForm.getCode());
					P.setDeleted(true);
					savedTypeAnalyse = typeAnalyseRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeAnalyse.getCode();
		
        
    }
}
