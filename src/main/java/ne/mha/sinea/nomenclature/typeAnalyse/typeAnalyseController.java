package ne.mha.sinea.nomenclature.typeAnalyse;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import ne.mha.sinea.Template;
@RestController
@Controller
public class typeAnalyseController {

	@Autowired
	typeAnalyseRepository typeAnalyseRepository;
	
	//@PreAuthorize("hasAuthority('gestion des types amenagement')")
	@GetMapping("/typeAnalyse")
	public String  addTypeAnalyse(typeAnalyseForm typeAnalyseForm, Model model) {
		try{
			List<typeAnalyse> typeAnalyse = typeAnalyseRepository.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeAnalyse", typeAnalyse);
			model.addAttribute("viewPath", "nomenclature/typeAnalyse/typeAnalyse");
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
}
