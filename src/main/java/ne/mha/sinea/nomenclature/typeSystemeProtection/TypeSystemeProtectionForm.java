package ne.mha.sinea.nomenclature.typeSystemeProtection;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TypeSystemeProtectionForm {

	private int code;
	private String libelle;
	
}
