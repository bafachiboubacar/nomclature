package ne.mha.sinea.nomenclature.typeSystemeProtection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class TypeSystemeProtectionRestController {

	@Autowired
	TypeSystemeProtectionRepository typeSystemeProtectionService;
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/typeSystemeProtection")
	public int addTypeSystemeProtection(@Validated TypeSystemeProtectionForm typeSystemeProtectionForm,BindingResult bindingResult, Model model) {
		TypeSystemeProtection savedTypeSystemeProtection = new TypeSystemeProtection();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeSystemeProtection P = new TypeSystemeProtection();
					P.setLibelle(typeSystemeProtectionForm.getLibelle());
					savedTypeSystemeProtection = typeSystemeProtectionService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeSystemeProtection.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/updateTypeSystemeProtection")
    public int updateTypeSystemeProtection(@Validated TypeSystemeProtectionForm typeSystemeProtectionForm,BindingResult bindingResult, Model model) {
		TypeSystemeProtection savedTypeSystemeProtection = new TypeSystemeProtection();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeSystemeProtection P = typeSystemeProtectionService.findByCode(typeSystemeProtectionForm.getCode());
					P.setLibelle(typeSystemeProtectionForm.getLibelle());
					savedTypeSystemeProtection = typeSystemeProtectionService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeSystemeProtection.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/deleteTypeSystemeProtection")
    public int deleteTypeSystemeProtection(@Validated TypeSystemeProtectionForm typeSystemeProtectionForm,BindingResult bindingResult, Model model) {
		TypeSystemeProtection savedTypeSystemeProtection = new TypeSystemeProtection();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeSystemeProtection P = typeSystemeProtectionService.findByCode(typeSystemeProtectionForm.getCode());
					P.setDeleted(true);
					savedTypeSystemeProtection = typeSystemeProtectionService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeSystemeProtection.getCode();
		
        
    }
}
