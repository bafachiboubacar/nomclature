package ne.mha.sinea.nomenclature.typeSystemeProtection;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;
@Controller
public class TypeSystemeProtectionController {

	@Autowired
	TypeSystemeProtectionRepository typeSystemeProtectionService;
	
	//@PreAuthorize("hasAuthority('gestion des Type Systeme Protection')")
	@GetMapping("/typeSystemeProtection")
	public String  addTypeSystemeProtection(TypeSystemeProtectionForm typeSystemeProtectionForm, Model model) {
		try{
			List<TypeSystemeProtection> typeSystemeProtections = typeSystemeProtectionService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeSystemeProtections", typeSystemeProtections);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Nomenclature/Type Systeme Protection");
			model.addAttribute("viewPath", "nomenclature/typeSystemeProtection/typeSystemeProtection");
			
			}
		catch(Exception e){
				
			}
		
		return Template.defaultTemplate;

	}
	
	
}
