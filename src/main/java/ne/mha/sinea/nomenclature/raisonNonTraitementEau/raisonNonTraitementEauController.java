package ne.mha.sinea.nomenclature.raisonNonTraitementEau;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class raisonNonTraitementEauController {

	@Autowired
	raisonNonTraitementEauRepository raisonNonTraitementEauRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/raisonNonTraitementEau")
	public String  addRaisonNonTraitementEau(raisonNonTraitementEauForm raisonNonTraitementEauForm, Model model) {
		try{
			List<raisonNonTraitementEau> raisonNonTraitementEau = raisonNonTraitementEauRepository.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("raisonNonTraitementEau", raisonNonTraitementEau);
			model.addAttribute("viewPath", "nomenclature/raisonNonTraitementEau/raisonNonTraitementEau");
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
}
