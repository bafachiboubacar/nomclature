package ne.mha.sinea.nomenclature.raisonNonTraitementEau;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class raisonNonTraitementEauRestController {

	@Autowired
	raisonNonTraitementEauRepository raisonNonTraitementEauRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/raisonNonTraitementEau")
	public int addRaisonNonTraitementEau(@Validated raisonNonTraitementEauForm raisonNonTraitementEauForm,BindingResult bindingResult, Model model) {
		raisonNonTraitementEau savedRaisonNonTraitementEau = new raisonNonTraitementEau();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					raisonNonTraitementEau P = new raisonNonTraitementEau();
					P.setLibelle(raisonNonTraitementEauForm.getLibelle());
					savedRaisonNonTraitementEau = raisonNonTraitementEauRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedRaisonNonTraitementEau.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateRaisonNonTraitementEau")
    public int updateRaisonNonTraitementEau(@Validated raisonNonTraitementEauForm raisonNonTraitementEauForm,BindingResult bindingResult, Model model) {
		raisonNonTraitementEau savedRaisonNonTraitementEau = new raisonNonTraitementEau();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					raisonNonTraitementEau P = raisonNonTraitementEauRepository.findByCode(raisonNonTraitementEauForm.getCode());
					P.setLibelle(raisonNonTraitementEauForm.getLibelle());
					savedRaisonNonTraitementEau = raisonNonTraitementEauRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedRaisonNonTraitementEau.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteRaisonNonTraitementEau")
    public int deleteRaisonNonTraitementEau(@Validated raisonNonTraitementEauForm raisonNonTraitementEauForm,BindingResult bindingResult, Model model) {
		raisonNonTraitementEau savedRaisonNonTraitementEau = new raisonNonTraitementEau();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					raisonNonTraitementEau P = raisonNonTraitementEauRepository.findByCode(raisonNonTraitementEauForm.getCode());
					P.setDeleted(true);
					savedRaisonNonTraitementEau = raisonNonTraitementEauRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedRaisonNonTraitementEau.getCode();
		
        
    }
}
