package ne.mha.sinea.nomenclature.usageSourceEauEcole;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface usageSourceEauEcoleRepository extends CrudRepository<usageSourceEauEcole, Integer> {
	usageSourceEauEcole findByCode(Integer code);
	usageSourceEauEcole findByIsDeletedFalseAndLibelle(String libelle);
	List<usageSourceEauEcole> findByIsDeletedFalseOrderByLibelleAsc();
	List<usageSourceEauEcole> findByIsDeletedFalse();
	List<usageSourceEauEcole> findByIsDeletedTrue();
	List<usageSourceEauEcole> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<usageSourceEauEcole> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
