package ne.mha.sinea.nomenclature.usageSourceEauEcole;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class usageSourceEauEcoleController {

	@Autowired
	usageSourceEauEcoleRepository usageSourceEauEcoleRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/usageSourceEauEcole")
	public String  addUsageSourceEauEcole(usageSourceEauEcoleForm usageSourceEauEcoleForm, Model model) {
		try{
			List<usageSourceEauEcole> usageSourceEauEcole = usageSourceEauEcoleRepository.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("usageSourceEauEcole", usageSourceEauEcole);
			model.addAttribute("viewPath", "nomenclature/usageSourceEauEcole/usageSourceEauEcole");
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
}
