package ne.mha.sinea.nomenclature.typeInstallationMiniAEP;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface typeInstallationMiniAEPRepository extends CrudRepository<typeInstallationMiniAEP, Integer> {
	typeInstallationMiniAEP findByCode(Integer code);
	typeInstallationMiniAEP findByIsDeletedFalseAndLibelle(String libelle);
	List<typeInstallationMiniAEP> findByIsDeletedFalseOrderByLibelleAsc();
	List<typeInstallationMiniAEP> findByIsDeletedFalse();
	List<typeInstallationMiniAEP> findByIsDeletedTrue();
	List<typeInstallationMiniAEP> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<typeInstallationMiniAEP> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
