package ne.mha.sinea.nomenclature.typeInstallationMiniAEP;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class typeInstallationMiniAEPRestController {

	@Autowired
	typeInstallationMiniAEPRepository typeInstallationMiniAEPRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/typeInstallationMiniAEP")
	public int addTypeInstallationMiniAEP(@Validated typeInstallationMiniAEPForm typeInstallationMiniAEPForm,BindingResult bindingResult, Model model) {
		typeInstallationMiniAEP savedTypeInstallationMiniAEP = new typeInstallationMiniAEP();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					typeInstallationMiniAEP P = new typeInstallationMiniAEP();
					P.setLibelle(typeInstallationMiniAEPForm.getLibelle());
					savedTypeInstallationMiniAEP = typeInstallationMiniAEPRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeInstallationMiniAEP.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateTypeInstallationMiniAEP")
    public int updateTypeInstallationMiniAEP(@Validated typeInstallationMiniAEPForm typeInstallationMiniAEPForm,BindingResult bindingResult, Model model) {
		typeInstallationMiniAEP savedTypeInstallationMiniAEP = new typeInstallationMiniAEP();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					typeInstallationMiniAEP P = typeInstallationMiniAEPRepository.findByCode(typeInstallationMiniAEPForm.getCode());
					P.setLibelle(typeInstallationMiniAEPForm.getLibelle());
					savedTypeInstallationMiniAEP = typeInstallationMiniAEPRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeInstallationMiniAEP.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteTypeInstallationMiniAEP")
    public int deleteTypeInstallationMiniAEP(@Validated typeInstallationMiniAEPForm typeInstallationMiniAEPForm,BindingResult bindingResult, Model model) {
		typeInstallationMiniAEP savedTypeInstallationMiniAEP = new typeInstallationMiniAEP();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					typeInstallationMiniAEP P = typeInstallationMiniAEPRepository.findByCode(typeInstallationMiniAEPForm.getCode());
					P.setDeleted(true);
					savedTypeInstallationMiniAEP = typeInstallationMiniAEPRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeInstallationMiniAEP.getCode();
		
        
    }
}
