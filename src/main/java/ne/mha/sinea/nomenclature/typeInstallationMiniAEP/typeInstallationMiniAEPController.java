package ne.mha.sinea.nomenclature.typeInstallationMiniAEP;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class typeInstallationMiniAEPController {

	@Autowired
	typeInstallationMiniAEPRepository typeInstallationMiniAEPRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/typeInstallationMiniAEP")
	public String  addTypeInstallationMiniAEP(typeInstallationMiniAEPForm typeInstallationMiniAEPForm, Model model) {
		try{
			List<typeInstallationMiniAEP> typeInstallationMiniAEP = typeInstallationMiniAEPRepository.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeInstallationMiniAEP", typeInstallationMiniAEP);
			model.addAttribute("viewPath", "nomenclature/typeInstallationMiniAEP/typeInstallationMiniAEP");
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
}
