package ne.mha.sinea.nomenclature.typeLieuPublic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class TypeLieuPublicRestController {

	@Autowired
	TypeLieuPublicRepository typeLieuPublicService;
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/typeLieuPublic")
	public int addTypeLieuPublic(@Validated TypeLieuPublicForm typeLieuPublicForm,BindingResult bindingResult, Model model) {
		TypeLieuPublic savedTypeLieuPublic = new TypeLieuPublic();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeLieuPublic P = new TypeLieuPublic();
					P.setLibelle(typeLieuPublicForm.getLibelle());
					savedTypeLieuPublic = typeLieuPublicService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeLieuPublic.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/updateTypeLieuPublic")
    public int updateTypeLieuPublic(@Validated TypeLieuPublicForm typeLieuPublicForm,BindingResult bindingResult, Model model) {
		TypeLieuPublic savedTypeLieuPublic = new TypeLieuPublic();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeLieuPublic P = typeLieuPublicService.findByCode(typeLieuPublicForm.getCode());
					P.setLibelle(typeLieuPublicForm.getLibelle());
					savedTypeLieuPublic = typeLieuPublicService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeLieuPublic.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/deleteTypeLieuPublic")
    public int deleteTypeLieuPublic(@Validated TypeLieuPublicForm typeLieuPublicForm,BindingResult bindingResult, Model model) {
		TypeLieuPublic savedTypeLieuPublic = new TypeLieuPublic();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeLieuPublic P = typeLieuPublicService.findByCode(typeLieuPublicForm.getCode());
					P.setDeleted(true);
					savedTypeLieuPublic = typeLieuPublicService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeLieuPublic.getCode();
		
        
    }
}
