package ne.mha.sinea.nomenclature.typeLieuPublic;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;
@Controller
public class TypeLieuPublicController {

	@Autowired
	TypeLieuPublicRepository typeLieuPublicService;
	
	//@PreAuthorize("hasAuthority('gestion des Type Lieu Public')")
	@GetMapping("/typeLieuPublic")
	public String  addTypeLieuPublic(TypeLieuPublicForm typeLieuPublicForm, Model model) {
		try{
			List<TypeLieuPublic> typeLieuPublics = typeLieuPublicService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeLieuPublics", typeLieuPublics);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Nomenclature/Type Lieu Public");
			model.addAttribute("viewPath", "nomenclature/typeLieuPublic/typeLieuPublic");
			
			}
		catch(Exception e){
				
			}
		
		return Template.defaultTemplate;

	}
	
	
}
