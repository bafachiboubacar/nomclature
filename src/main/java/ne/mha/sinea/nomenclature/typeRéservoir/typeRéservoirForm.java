package ne.mha.sinea.nomenclature.typeRéservoir;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class typeRéservoirForm {

	private int code;
	private String libelle;
	
}
