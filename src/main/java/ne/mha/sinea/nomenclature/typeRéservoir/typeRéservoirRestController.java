package ne.mha.sinea.nomenclature.typeRéservoir;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class typeRéservoirRestController {

	@Autowired
	typeRéservoirRepository typeRéservoirRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/typeRéservoirEcole")
	public int addTypeRéservoir(@Validated typeRéservoirForm typeRéservoirForm,BindingResult bindingResult, Model model) {
		typeRéservoir savedTypeRéservoir = new typeRéservoir();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					typeRéservoir P = new typeRéservoir();
					P.setLibelle(typeRéservoirForm.getLibelle());
					savedTypeRéservoir = typeRéservoirRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeRéservoir.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateTypeRéservoir")
    public int updateTypeRéservoir(@Validated typeRéservoirForm typeRéservoirForm,BindingResult bindingResult, Model model) {
		typeRéservoir savedTypeRéservoir = new typeRéservoir();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					typeRéservoir P = typeRéservoirRepository.findByCode(typeRéservoirForm.getCode());
					P.setLibelle(typeRéservoirForm.getLibelle());
					savedTypeRéservoir = typeRéservoirRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeRéservoir.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteTypeRéservoir")
    public int deleteTypeRéservoir(@Validated typeRéservoirForm typeRéservoirForm,BindingResult bindingResult, Model model) {
		typeRéservoir savedTypeRéservoir = new typeRéservoir();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					typeRéservoir P = typeRéservoirRepository.findByCode(typeRéservoirForm.getCode());
					P.setDeleted(true);
					savedTypeRéservoir = typeRéservoirRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeRéservoir.getCode();
		
        
    }
}
