package ne.mha.sinea.nomenclature.typeRéservoir;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface typeRéservoirRepository extends CrudRepository<typeRéservoir, Integer> {
	typeRéservoir findByCode(Integer code);
	typeRéservoir findByIsDeletedFalseAndLibelle(String libelle);
	List<typeRéservoir> findByIsDeletedFalseOrderByLibelleAsc();
	List<typeRéservoir> findByIsDeletedFalse();
	List<typeRéservoir> findByIsDeletedTrue();
	List<typeRéservoir> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<typeRéservoir> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
