package ne.mha.sinea.nomenclature.typeRéservoir;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class typeRéservoirController {

	@Autowired
	typeRéservoirRepository typeRéservoirRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/typeRéservoir")
	public String  addTypeRéservoir(typeRéservoirForm typeRéservoirForm, Model model) {
		try{
			List<typeRéservoir> typeRéservoir = typeRéservoirRepository.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeRéservoir", typeRéservoir);
			model.addAttribute("viewPath", "nomenclature/typeRéservoir/typeRéservoir");
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
}
