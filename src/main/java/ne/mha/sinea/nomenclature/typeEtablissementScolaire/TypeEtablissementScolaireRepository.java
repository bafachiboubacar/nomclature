package ne.mha.sinea.nomenclature.typeEtablissementScolaire;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface TypeEtablissementScolaireRepository extends CrudRepository<TypeEtablissementScolaire, Integer> {
	TypeEtablissementScolaire findByCode(Integer code);
	TypeEtablissementScolaire findByIsDeletedFalseAndLibelle(String libelle);
	List<TypeEtablissementScolaire> findByIsDeletedFalseOrderByLibelleAsc();
	List<TypeEtablissementScolaire> findByIsDeletedFalse();
	List<TypeEtablissementScolaire> findByIsDeletedTrue();
	List<TypeEtablissementScolaire> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<TypeEtablissementScolaire> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
