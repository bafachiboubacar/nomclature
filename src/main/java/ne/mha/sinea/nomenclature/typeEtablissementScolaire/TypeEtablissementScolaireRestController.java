package ne.mha.sinea.nomenclature.typeEtablissementScolaire;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class TypeEtablissementScolaireRestController {

	@Autowired
	TypeEtablissementScolaireRepository typeEtablissementScolaireService;
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/typeEtablissementScolaire")
	public int addTypeEtablissementScolaire(@Validated TypeEtablissementScolaireForm typeEtablissementScolaireForm,BindingResult bindingResult, Model model) {
		TypeEtablissementScolaire savedTypeEtablissementScolaire = new TypeEtablissementScolaire();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeEtablissementScolaire P = new TypeEtablissementScolaire();
					P.setLibelle(typeEtablissementScolaireForm.getLibelle());
					savedTypeEtablissementScolaire = typeEtablissementScolaireService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeEtablissementScolaire.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/updateTypeEtablissementScolaire")
    public int updateTypeEtablissementScolaire(@Validated TypeEtablissementScolaireForm typeEtablissementScolaireForm,BindingResult bindingResult, Model model) {
		TypeEtablissementScolaire savedTypeEtablissementScolaire = new TypeEtablissementScolaire();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeEtablissementScolaire P = typeEtablissementScolaireService.findByCode(typeEtablissementScolaireForm.getCode());
					P.setLibelle(typeEtablissementScolaireForm.getLibelle());
					savedTypeEtablissementScolaire = typeEtablissementScolaireService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeEtablissementScolaire.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/deleteTypeEtablissementScolaire")
    public int deleteTypeEtablissementScolaire(@Validated TypeEtablissementScolaireForm typeEtablissementScolaireForm,BindingResult bindingResult, Model model) {
		TypeEtablissementScolaire savedTypeEtablissementScolaire = new TypeEtablissementScolaire();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeEtablissementScolaire P = typeEtablissementScolaireService.findByCode(typeEtablissementScolaireForm.getCode());
					P.setDeleted(true);
					savedTypeEtablissementScolaire = typeEtablissementScolaireService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeEtablissementScolaire.getCode();
		
        
    }
}
