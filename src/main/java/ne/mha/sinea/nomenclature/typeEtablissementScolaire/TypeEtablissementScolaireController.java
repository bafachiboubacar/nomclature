package ne.mha.sinea.nomenclature.typeEtablissementScolaire;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;
@Controller
public class TypeEtablissementScolaireController {

	@Autowired
	TypeEtablissementScolaireRepository typeEtablissementScolaireService;
	
	//@PreAuthorize("hasAuthority('gestion des Type Etablissement Scolaire')")
	@GetMapping("/typeEtablissementScolaire")
	public String  addTypeEtablissementScolaire(TypeEtablissementScolaireForm typeEtablissementScolaireForm, Model model) {
		try{
			List<TypeEtablissementScolaire> typeEtablissementScolaires = typeEtablissementScolaireService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeEtablissementScolaires", typeEtablissementScolaires);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Nomenclature/Type Etablissement Scolaire");
			model.addAttribute("viewPath", "nomenclature/typeEtablissementScolaire/typeEtablissementScolaire");
			
			}
		catch(Exception e){
				
			}
		
		return Template.defaultTemplate;

	}
	
	
}
