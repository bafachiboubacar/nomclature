package ne.mha.sinea.nomenclature.milieu;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class MilieuController {

	@Autowired
	MilieuRepository milieuService;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/milieu")
	public String  addMilieu(MilieuForm milieuForm, Model model) {
		try{
			List<Milieu> milieus = milieuService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("milieus", milieus);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Nomenclature/Milieu");
			model.addAttribute("viewPath", "nomenclature/milieu/milieu");
			
			}
		catch(Exception e){
				
			}
		
		return Template.defaultTemplate;

	}
	
	
}
