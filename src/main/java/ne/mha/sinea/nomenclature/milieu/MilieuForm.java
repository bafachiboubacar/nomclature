package ne.mha.sinea.nomenclature.milieu;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MilieuForm {

	private int code;
	private String libelle;
	
}
