package ne.mha.sinea.nomenclature.etatPointEau;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class etatPointEauController {

	@Autowired
	etatPointEauRepository etatPointEauRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/etatPointEau")
	public String  addDisposition(etatPointEauForm dispositionForm, Model model) {
		try{
			List<etatPointEau> etatPointEau = etatPointEauRepository.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("etatPointEau",etatPointEau);
			model.addAttribute("viewPath", "nomenclature/etatPointEau/etatPointEau");
			
			}
		catch(Exception e){
				
			}
		
		return Template.defaultTemplate;

	}
	
	
}
