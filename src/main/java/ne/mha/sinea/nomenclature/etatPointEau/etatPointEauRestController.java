package ne.mha.sinea.nomenclature.etatPointEau;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class etatPointEauRestController {

	@Autowired
	etatPointEauRepository etatPointEauRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/etatPointEau")
	public int addEtatPointEau(@Validated etatPointEauForm etatPointEauForm,BindingResult bindingResult, Model model) {
		etatPointEau savedEtatPointEau = new etatPointEau();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					etatPointEau P = new etatPointEau();
					P.setLibelle(etatPointEauForm.getLibelle());
					savedEtatPointEau = etatPointEauRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedEtatPointEau.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateEtatPointEau")
    public int updateEtatPointEau(@Validated etatPointEauForm etatPointEauForm,BindingResult bindingResult, Model model) {
		etatPointEau savedEtatPointEau = new etatPointEau();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					etatPointEau P = etatPointEauRepository.findByCode(etatPointEauForm.getCode());
					P.setLibelle(etatPointEauForm.getLibelle());
					savedEtatPointEau = etatPointEauRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedEtatPointEau.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteEtatPointEau")
    public int deleteEtatPointEau(@Validated etatPointEauForm etatPointEauForm,BindingResult bindingResult, Model model) {
		etatPointEau savedEtatPointEau = new etatPointEau();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					etatPointEau P = etatPointEauRepository.findByCode(etatPointEauForm.getCode());
					P.setDeleted(true);
					savedEtatPointEau = etatPointEauRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedEtatPointEau.getCode();
		
        
    }
}
