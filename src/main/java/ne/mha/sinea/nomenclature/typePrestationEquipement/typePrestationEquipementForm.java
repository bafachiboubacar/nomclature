package ne.mha.sinea.nomenclature.typePrestationEquipement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class typePrestationEquipementForm {

	private int code;
	private String libelle;
	
}
