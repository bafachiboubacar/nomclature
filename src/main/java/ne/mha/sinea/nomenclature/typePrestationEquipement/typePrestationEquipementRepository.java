package ne.mha.sinea.nomenclature.typePrestationEquipement;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface typePrestationEquipementRepository extends CrudRepository<typePrestationEquipement, Integer> {
	typePrestationEquipement findByCode(Integer code);
	typePrestationEquipement findByIsDeletedFalseAndLibelle(String libelle);
	List<typePrestationEquipement> findByIsDeletedFalseOrderByLibelleAsc();
	List<typePrestationEquipement> findByIsDeletedFalse();
	List<typePrestationEquipement> findByIsDeletedTrue();
	List<typePrestationEquipement> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<typePrestationEquipement> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
