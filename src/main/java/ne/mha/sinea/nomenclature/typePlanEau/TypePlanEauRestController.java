package ne.mha.sinea.nomenclature.typePlanEau;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class TypePlanEauRestController {

	@Autowired
	TypePlanEauRepository typePlanEauService;
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/typePlanEau")
	public int addTypePlanEau(@Validated TypePlanEauForm typePlanEauForm,BindingResult bindingResult, Model model) {
		TypePlanEau savedTypePlanEau = new TypePlanEau();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypePlanEau P = new TypePlanEau();
					P.setLibelle(typePlanEauForm.getLibelle());
					savedTypePlanEau = typePlanEauService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypePlanEau.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/updateTypePlanEau")
    public int updateTypePlanEau(@Validated TypePlanEauForm typePlanEauForm,BindingResult bindingResult, Model model) {
		TypePlanEau savedTypePlanEau = new TypePlanEau();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypePlanEau P = typePlanEauService.findByCode(typePlanEauForm.getCode());
					P.setLibelle(typePlanEauForm.getLibelle());
					savedTypePlanEau = typePlanEauService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypePlanEau.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/deleteTypePlanEau")
    public int deleteTypePlanEau(@Validated TypePlanEauForm typePlanEauForm,BindingResult bindingResult, Model model) {
		TypePlanEau savedTypePlanEau = new TypePlanEau();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypePlanEau P = typePlanEauService.findByCode(typePlanEauForm.getCode());
					P.setDeleted(true);
					savedTypePlanEau = typePlanEauService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypePlanEau.getCode();
		
        
    }
}
