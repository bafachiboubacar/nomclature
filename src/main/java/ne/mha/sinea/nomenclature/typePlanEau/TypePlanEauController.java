package ne.mha.sinea.nomenclature.typePlanEau;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;
@Controller
public class TypePlanEauController {

	@Autowired
	TypePlanEauRepository typePlanEauService;
	
	//@PreAuthorize("hasAuthority('gestion des Type Plan Eau')")
	@GetMapping("/typePlanEau")
	public String  addTypePlanEau(TypePlanEauForm typePlanEauForm, Model model) {
		try{
			List<TypePlanEau> typePlanEaus = typePlanEauService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typePlanEaus", typePlanEaus);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Nomenclature/Type Plan Eau");
			model.addAttribute("viewPath", "nomenclature/typePlanEau/typePlanEau");
			
			}
		catch(Exception e){
				
			}
		
		return Template.defaultTemplate;

	}
	
	
}
