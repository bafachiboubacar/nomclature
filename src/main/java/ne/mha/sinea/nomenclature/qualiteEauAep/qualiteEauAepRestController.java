package ne.mha.sinea.nomenclature.qualiteEauAep;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class qualiteEauAepRestController {

	@Autowired
	qualiteEauAepRepository qualiteEauAepRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/qualiteEauAep")
	public int addQualiteEauAep(@Validated qualiteEauAepForm qualiteEauAepForm,BindingResult bindingResult, Model model) {
		qualiteEauAep savedQualiteEauAep = new qualiteEauAep();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					qualiteEauAep P = new qualiteEauAep();
					P.setLibelle(qualiteEauAepForm.getLibelle());
					savedQualiteEauAep = qualiteEauAepRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedQualiteEauAep.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateQualiteEauAep")
    public int updateQualiteEauAep(@Validated qualiteEauAepForm qualiteEauAepForm,BindingResult bindingResult, Model model) {
		qualiteEauAep savedQualiteEauAep = new qualiteEauAep();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					qualiteEauAep P = qualiteEauAepRepository.findByCode(qualiteEauAepForm.getCode());
					P.setLibelle(qualiteEauAepForm.getLibelle());
					savedQualiteEauAep = qualiteEauAepRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedQualiteEauAep.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteQualiteEauAep")
    public int deleteQualiteEauAep(@Validated qualiteEauAepForm qualiteEauAepForm,BindingResult bindingResult, Model model) {
		qualiteEauAep savedQualiteEauAep = new qualiteEauAep();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					qualiteEauAep P = qualiteEauAepRepository.findByCode(qualiteEauAepForm.getCode());
					P.setDeleted(true);
					savedQualiteEauAep = qualiteEauAepRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedQualiteEauAep.getCode();
		
        
    }
}
