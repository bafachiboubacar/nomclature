package ne.mha.sinea.nomenclature.qualiteEauAep;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class qualiteEauAepForm {

	private int code;
	private String libelle;
	
}
