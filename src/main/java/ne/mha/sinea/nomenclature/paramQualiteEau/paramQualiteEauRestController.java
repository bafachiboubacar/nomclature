package ne.mha.sinea.nomenclature.paramQualiteEau;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class paramQualiteEauRestController {

	@Autowired
	paramQualiteEauRepository paramQualiteEauRepository;
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/paramQualiteEau")
	public int addParamQualiteEau(@Validated paramQualiteEauForm paramQualiteEauForm,BindingResult bindingResult, Model model) {
		paramQualiteEau savedParamQualiteEau = new paramQualiteEau();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					paramQualiteEau P = new paramQualiteEau();
					P.setLibelle(paramQualiteEauForm.getLibelle());
					savedParamQualiteEau = paramQualiteEauRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedParamQualiteEau.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/updateParamQualiteEau")
    public int updateParamQualiteEau(@Validated paramQualiteEauForm paramQualiteEauForm,BindingResult bindingResult, Model model) {
		paramQualiteEau savedParamQualiteEau = new paramQualiteEau();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					paramQualiteEau P = paramQualiteEauRepository.findByCode(paramQualiteEauForm.getCode());
					P.setLibelle(paramQualiteEauForm.getLibelle());
					savedParamQualiteEau = paramQualiteEauRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedParamQualiteEau.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/deleteParamQualiteEau")
    public int deleteParamQualiteEau(@Validated paramQualiteEauForm paramQualiteEauForm,BindingResult bindingResult, Model model) {
		paramQualiteEau savedParamQualiteEau = new paramQualiteEau();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					paramQualiteEau P = paramQualiteEauRepository.findByCode(paramQualiteEauForm.getCode());
					P.setDeleted(true);
					savedParamQualiteEau = paramQualiteEauRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedParamQualiteEau.getCode();
		
        
    }
}
