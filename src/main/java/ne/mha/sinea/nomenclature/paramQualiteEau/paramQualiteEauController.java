package ne.mha.sinea.nomenclature.paramQualiteEau;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;
@Controller
public class paramQualiteEauController {

	@Autowired
	paramQualiteEauRepository paramQualiteEauRepository;
	
	//@PreAuthorize("hasAuthority('gestion des types amenagement')")
	@GetMapping("/paramQualiteEau")
	public String  addParamQualiteEau(paramQualiteEauForm paramQualiteEauForm, Model model) {
		try{
			List<paramQualiteEau> paramQualiteEau = paramQualiteEauRepository.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("paramQualiteEau", paramQualiteEau);
			model.addAttribute("viewPath", "nomenclature/paramQualiteEau/paramQualiteEau");
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
}
