package ne.mha.sinea.nomenclature.disposition;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class DispositionController {

	@Autowired
	DispositionRepository dispositionRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/disposition")
	public String  addDisposition(DispositionForm dispositionForm, Model model) {
		try{
			List<Disposition> dispositions = dispositionRepository.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("dispositions", dispositions);
			model.addAttribute("viewPath", "nomenclature/disposition/disposition");
			
			}
		catch(Exception e){
				
			}
		
		return Template.defaultTemplate;

	}
	
	
}
