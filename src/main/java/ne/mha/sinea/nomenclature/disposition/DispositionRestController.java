package ne.mha.sinea.nomenclature.disposition;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class DispositionRestController {

	@Autowired
	DispositionRepository dispositionRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/disposition")
	public int addDisposition(@Validated DispositionForm dispositionForm,BindingResult bindingResult, Model model) {
		Disposition savedDisposition = new Disposition();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					Disposition P = new Disposition();
					P.setLibelle(dispositionForm.getLibelle());
					savedDisposition = dispositionRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedDisposition.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateDisposition")
    public int updateDisposition(@Validated DispositionForm dispositionForm,BindingResult bindingResult, Model model) {
		Disposition savedDisposition = new Disposition();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					Disposition P = dispositionRepository.findByCode(dispositionForm.getCode());
					P.setLibelle(dispositionForm.getLibelle());
					savedDisposition = dispositionRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedDisposition.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteDisposition")
    public int deleteDisposition(@Validated DispositionForm dispositionForm,BindingResult bindingResult, Model model) {
		Disposition savedDisposition = new Disposition();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					Disposition P = dispositionRepository.findByCode(dispositionForm.getCode());
					P.setDeleted(true);
					savedDisposition = dispositionRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedDisposition.getCode();
		
        
    }
}
