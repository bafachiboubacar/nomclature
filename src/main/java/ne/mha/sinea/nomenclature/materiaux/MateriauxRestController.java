package ne.mha.sinea.nomenclature.materiaux;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class MateriauxRestController {

	@Autowired
	MateriauxRepository materiauxService;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/materiaux")
	public int addMateriaux(@Validated MateriauxForm materiauxForm,BindingResult bindingResult, Model model) {
		Materiaux savedMateriaux = new Materiaux();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					Materiaux P = new Materiaux();
					P.setLibelle(materiauxForm.getLibelle());
					savedMateriaux = materiauxService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedMateriaux.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateMateriaux")
    public int updateMateriaux(@Validated MateriauxForm materiauxForm,BindingResult bindingResult, Model model) {
		Materiaux savedMateriaux = new Materiaux();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					Materiaux P = materiauxService.findByCode(materiauxForm.getCode());
					P.setLibelle(materiauxForm.getLibelle());
					savedMateriaux = materiauxService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedMateriaux.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteMateriaux")
    public int deleteMateriaux(@Validated MateriauxForm materiauxForm,BindingResult bindingResult, Model model) {
		Materiaux savedMateriaux = new Materiaux();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					Materiaux P = materiauxService.findByCode(materiauxForm.getCode());
					P.setDeleted(true);
					savedMateriaux = materiauxService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedMateriaux.getCode();
		
        
    }
}
