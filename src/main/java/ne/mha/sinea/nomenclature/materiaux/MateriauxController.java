package ne.mha.sinea.nomenclature.materiaux;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class MateriauxController {

	@Autowired
	MateriauxRepository materiauxService;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/materiaux")
	public String  addMateriaux(MateriauxForm materiauxForm, Model model) {
		try{
			List<Materiaux> materiauxs = materiauxService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("materiauxs", materiauxs);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Nomenclature/Materiaux");
			model.addAttribute("viewPath", "nomenclature/materiaux/materiaux");
			
			}
		catch(Exception e){
				
			}
		
		return Template.defaultTemplate;

	}
	
	
}
