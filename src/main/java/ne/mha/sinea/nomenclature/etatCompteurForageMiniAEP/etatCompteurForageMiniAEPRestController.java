package ne.mha.sinea.nomenclature.etatCompteurForageMiniAEP;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class etatCompteurForageMiniAEPRestController {

	@Autowired
	etatCompteurForageMiniAEPRepository etatCompteurForageMiniAEPRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/etatCompteurForageMiniAEP")
	public int addEtatCompteurForageMiniAEP(@Validated etatCompteurForageMiniAEPForm etatCompteurForageMiniAEPForm,BindingResult bindingResult, Model model) {
		etatCompteurForageMiniAEP savedEtatCompteurForageMiniAEP = new etatCompteurForageMiniAEP();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					etatCompteurForageMiniAEP P = new etatCompteurForageMiniAEP();
					P.setLibelle(etatCompteurForageMiniAEPForm.getLibelle());
					savedEtatCompteurForageMiniAEP = etatCompteurForageMiniAEPRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedEtatCompteurForageMiniAEP.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateEtatCompteurForageMiniAEP")
    public int updateEtatCompteurForageMiniAEP(@Validated etatCompteurForageMiniAEPForm etatCompteurForageMiniAEPForm,BindingResult bindingResult, Model model) {
		etatCompteurForageMiniAEP savedEtatCompteurForageMiniAEP = new etatCompteurForageMiniAEP();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					etatCompteurForageMiniAEP P = etatCompteurForageMiniAEPRepository.findByCode(etatCompteurForageMiniAEPForm.getCode());
					P.setLibelle(etatCompteurForageMiniAEPForm.getLibelle());
					savedEtatCompteurForageMiniAEP = etatCompteurForageMiniAEPRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedEtatCompteurForageMiniAEP.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteEtatCompteurForageMiniAEP")
    public int deleteEtatCompteurForageMiniAEP(@Validated etatCompteurForageMiniAEPForm etatCompteurForageMiniAEPForm,BindingResult bindingResult, Model model) {
		etatCompteurForageMiniAEP savedEtatCompteurForageMiniAEP = new etatCompteurForageMiniAEP();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					etatCompteurForageMiniAEP P = etatCompteurForageMiniAEPRepository.findByCode(etatCompteurForageMiniAEPForm.getCode());
					P.setDeleted(true);
					savedEtatCompteurForageMiniAEP = etatCompteurForageMiniAEPRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedEtatCompteurForageMiniAEP.getCode();
		
        
    }
}
