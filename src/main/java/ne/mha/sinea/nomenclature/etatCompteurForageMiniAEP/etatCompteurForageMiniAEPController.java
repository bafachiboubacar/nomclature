package ne.mha.sinea.nomenclature.etatCompteurForageMiniAEP;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class etatCompteurForageMiniAEPController {

	@Autowired
	etatCompteurForageMiniAEPRepository etatCompteurForageMiniAEPRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/etatCompteurForageMiniAEP")
	public String  addEtatCompteurForageMiniAEP(etatCompteurForageMiniAEPForm etatCompteurForageMiniAEPForm, Model model) {
		try{
			List<etatCompteurForageMiniAEP>etatCompteurForageMiniAEP = etatCompteurForageMiniAEPRepository.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("etatCompteurForageMiniAEP", etatCompteurForageMiniAEP);
			model.addAttribute("viewPath", "nomenclature/etatCompteurForageMiniAEP/etatCompteurForageMiniAEP");
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
}
