package ne.mha.sinea.nomenclature.etatCompteurForageMiniAEP;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface etatCompteurForageMiniAEPRepository extends CrudRepository<etatCompteurForageMiniAEP, Integer> {
	etatCompteurForageMiniAEP findByCode(Integer code);
	etatCompteurForageMiniAEP findByIsDeletedFalseAndLibelle(String libelle);
	List<etatCompteurForageMiniAEP> findByIsDeletedFalseOrderByLibelleAsc();
	List<etatCompteurForageMiniAEP> findByIsDeletedFalse();
	List<etatCompteurForageMiniAEP> findByIsDeletedTrue();
	List<etatCompteurForageMiniAEP> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<etatCompteurForageMiniAEP> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
