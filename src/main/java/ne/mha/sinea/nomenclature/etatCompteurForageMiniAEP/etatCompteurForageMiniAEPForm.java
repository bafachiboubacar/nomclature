package ne.mha.sinea.nomenclature.etatCompteurForageMiniAEP;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class etatCompteurForageMiniAEPForm {

	private int code;
	private String libelle;
	
}
