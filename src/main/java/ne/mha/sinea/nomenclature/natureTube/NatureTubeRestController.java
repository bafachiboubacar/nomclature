package ne.mha.sinea.nomenclature.natureTube;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class NatureTubeRestController {

	@Autowired
	NatureTubeRepository natureTubeService;
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/natureTube")
	public int addNatureTube(@Validated NatureTubeForm natureTubeForm,BindingResult bindingResult, Model model) {
		NatureTube savedNatureTube = new NatureTube();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					NatureTube P = new NatureTube();
					P.setLibelle(natureTubeForm.getLibelle());
					savedNatureTube = natureTubeService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedNatureTube.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/updateNatureTube")
    public int updateNatureTube(@Validated NatureTubeForm natureTubeForm,BindingResult bindingResult, Model model) {
		NatureTube savedNatureTube = new NatureTube();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					NatureTube P = natureTubeService.findByCode(natureTubeForm.getCode());
					P.setLibelle(natureTubeForm.getLibelle());
					savedNatureTube = natureTubeService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedNatureTube.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/deleteNatureTube")
    public int deleteNatureTube(@Validated NatureTubeForm natureTubeForm,BindingResult bindingResult, Model model) {
		NatureTube savedNatureTube = new NatureTube();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					NatureTube P = natureTubeService.findByCode(natureTubeForm.getCode());
					P.setDeleted(true);
					savedNatureTube = natureTubeService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedNatureTube.getCode();
		
        
    }
}
