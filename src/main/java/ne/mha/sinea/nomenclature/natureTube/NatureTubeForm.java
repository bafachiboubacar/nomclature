package ne.mha.sinea.nomenclature.natureTube;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NatureTubeForm {

	private int code;
	private String libelle;
	
}
