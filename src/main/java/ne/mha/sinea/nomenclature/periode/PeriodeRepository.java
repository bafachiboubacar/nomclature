package ne.mha.sinea.nomenclature.periode;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface PeriodeRepository extends CrudRepository<Periode, Integer> {
	Periode findByCode(Integer code);
	Periode findByIsDeletedFalseAndLibelle(String libelle);
	List<Periode> findByIsDeletedFalseOrderByLibelleAsc();
	List<Periode> findByIsDeletedFalse();
	List<Periode> findByIsDeletedTrue();
	List<Periode> findByIsDeletedFalseAndParent_Code(Integer code);
	List<Periode> findByIsDeletedFalseAndParent_Libelle(String libelle);
	List<Periode> findByIsDeletedFalseAndNiveau(int niveau);
	List<Periode> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<Periode> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
