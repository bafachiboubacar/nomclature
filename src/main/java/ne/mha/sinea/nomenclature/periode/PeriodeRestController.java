package ne.mha.sinea.nomenclature.periode;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class PeriodeRestController {

	@Autowired
	PeriodeRepository periodeService;
	//@PreAuthorize("hasAuthority('gestion des périodes')")
	@PostMapping("/periode")
	public int addPeriode(@Validated PeriodeForm periodeForm,BindingResult bindingResult, Model model) {
		Periode savedPeriode = new Periode();
		if (!bindingResult.hasErrors()) {
			
			try {
				
				Periode P = new Periode();
				P.setLibelle(periodeForm.getLibelle());
				if(periodeForm.getParent_code() > 0)
				{
					Periode parent = periodeService.findByCode(periodeForm.getParent_code());
					P.setParent(parent);
					P.setNiveau(parent.getNiveau() + 1);
					
				}
				else
				{
					P.setParent(null);
					P.setNiveau(0);
				}
				
				savedPeriode = periodeService.save(P);
				
				
				}
			catch(Exception e){
				
				
					}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedPeriode.getCode();
		
        
    }
	//@PreAuthorize("hasAuthority('gestion des périodes')")
	@PostMapping("/updatePeriode")
    public int updatePeriode(@Validated PeriodeForm periodeForm,BindingResult bindingResult, Model model) {
		Periode savedPeriode = new Periode();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					Periode P = periodeService.findByCode(periodeForm.getCode());
					P.setLibelle(periodeForm.getLibelle());
					savedPeriode = periodeService.save(P);
					
				
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedPeriode.getCode();
		
        
    }
	//@PreAuthorize("hasAuthority('gestion des périodes')")
	@PostMapping("/deletePeriode")
    public int deletePeriode(@Validated PeriodeForm periodeForm,BindingResult bindingResult, Model model) {
		Periode savedPeriode = new Periode();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					Periode P = periodeService.findByCode(periodeForm.getCode());
					P.setDeleted(true);
					savedPeriode = periodeService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedPeriode.getCode();
		
        
    }
}
