package ne.mha.sinea.nomenclature.typePieceRegulationReseau;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TypePieceRegulationReseauForm {

	private int code;
	private String libelle;
	
}
