package ne.mha.sinea.nomenclature.typePieceRegulationReseau;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface TypePieceRegulationReseauRepository extends CrudRepository<TypePieceRegulationReseau, Integer> {
	TypePieceRegulationReseau findByCode(Integer code);
	TypePieceRegulationReseau findByIsDeletedFalseAndLibelle(String libelle);
	List<TypePieceRegulationReseau> findByIsDeletedFalseOrderByLibelleAsc();
	List<TypePieceRegulationReseau> findByIsDeletedFalse();
	List<TypePieceRegulationReseau> findByIsDeletedTrue();
	List<TypePieceRegulationReseau> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<TypePieceRegulationReseau> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
