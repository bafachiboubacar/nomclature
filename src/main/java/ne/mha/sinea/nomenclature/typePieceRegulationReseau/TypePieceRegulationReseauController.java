package ne.mha.sinea.nomenclature.typePieceRegulationReseau;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;
@Controller
public class TypePieceRegulationReseauController {

	@Autowired
	TypePieceRegulationReseauRepository typePieceRegulationReseauService;
	
	//@PreAuthorize("hasAuthority('gestion des Type Piece Regulation Reseau')")
	@GetMapping("/typePieceRegulationReseau")
	public String  addTypePieceRegulationReseau(TypePieceRegulationReseauForm typePieceRegulationReseauForm, Model model) {
		try{
			List<TypePieceRegulationReseau> typePieceRegulationReseaus = typePieceRegulationReseauService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typePieceRegulationReseaus", typePieceRegulationReseaus);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Nomenclature/Type Piece Regulation Reseau");
			model.addAttribute("viewPath", "nomenclature/typePieceRegulationReseau/typePieceRegulationReseau");
			
			}
		catch(Exception e){
				
			}
		
		return Template.defaultTemplate;

	}
	
	
}
