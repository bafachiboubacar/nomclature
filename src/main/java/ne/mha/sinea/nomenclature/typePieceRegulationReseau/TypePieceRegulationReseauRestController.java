package ne.mha.sinea.nomenclature.typePieceRegulationReseau;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class TypePieceRegulationReseauRestController {

	@Autowired
	TypePieceRegulationReseauRepository typePieceRegulationReseauService;
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/typePieceRegulationReseau")
	public int addTypePieceRegulationReseau(@Validated TypePieceRegulationReseauForm typePieceRegulationReseauForm,BindingResult bindingResult, Model model) {
		TypePieceRegulationReseau savedTypePieceRegulationReseau = new TypePieceRegulationReseau();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypePieceRegulationReseau P = new TypePieceRegulationReseau();
					P.setLibelle(typePieceRegulationReseauForm.getLibelle());
					savedTypePieceRegulationReseau = typePieceRegulationReseauService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypePieceRegulationReseau.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/updateTypePieceRegulationReseau")
    public int updateTypePieceRegulationReseau(@Validated TypePieceRegulationReseauForm typePieceRegulationReseauForm,BindingResult bindingResult, Model model) {
		TypePieceRegulationReseau savedTypePieceRegulationReseau = new TypePieceRegulationReseau();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypePieceRegulationReseau P = typePieceRegulationReseauService.findByCode(typePieceRegulationReseauForm.getCode());
					P.setLibelle(typePieceRegulationReseauForm.getLibelle());
					savedTypePieceRegulationReseau = typePieceRegulationReseauService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypePieceRegulationReseau.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/deleteTypePieceRegulationReseau")
    public int deleteTypePieceRegulationReseau(@Validated TypePieceRegulationReseauForm typePieceRegulationReseauForm,BindingResult bindingResult, Model model) {
		TypePieceRegulationReseau savedTypePieceRegulationReseau = new TypePieceRegulationReseau();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypePieceRegulationReseau P = typePieceRegulationReseauService.findByCode(typePieceRegulationReseauForm.getCode());
					P.setDeleted(true);
					savedTypePieceRegulationReseau = typePieceRegulationReseauService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypePieceRegulationReseau.getCode();
		
        
    }
}
