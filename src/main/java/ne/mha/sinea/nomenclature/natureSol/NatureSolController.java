package ne.mha.sinea.nomenclature.natureSol;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;
@Controller
public class NatureSolController {

	@Autowired
	NatureSolRepository natureSolService;
	
	//@PreAuthorize("hasAuthority('gestion des types ressources')")
	@GetMapping("/natureSol")
	public String  addNatureSol(NatureSolForm natureSolForm, Model model) {
		try{
			List<NatureSol> natureSols = natureSolService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("natureSols", natureSols);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Nomenclature/Type Ressource");
			model.addAttribute("viewPath", "nomenclature/natureSol/natureSol");
			
			}
		catch(Exception e){
				
			}
		
		return Template.defaultTemplate;

	}
	
	
}
