package ne.mha.sinea.nomenclature.natureSol;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class NatureSolRestController {

	@Autowired
	NatureSolRepository natureSolService;
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/natureSol")
	public int addNatureSol(@Validated NatureSolForm natureSolForm,BindingResult bindingResult, Model model) {
		NatureSol savedNatureSol = new NatureSol();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					NatureSol P = new NatureSol();
					P.setLibelle(natureSolForm.getLibelle());
					savedNatureSol = natureSolService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedNatureSol.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/updateNatureSol")
    public int updateNatureSol(@Validated NatureSolForm natureSolForm,BindingResult bindingResult, Model model) {
		NatureSol savedNatureSol = new NatureSol();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					NatureSol P = natureSolService.findByCode(natureSolForm.getCode());
					P.setLibelle(natureSolForm.getLibelle());
					savedNatureSol = natureSolService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedNatureSol.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/deleteNatureSol")
    public int deleteNatureSol(@Validated NatureSolForm natureSolForm,BindingResult bindingResult, Model model) {
		NatureSol savedNatureSol = new NatureSol();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					NatureSol P = natureSolService.findByCode(natureSolForm.getCode());
					P.setDeleted(true);
					savedNatureSol = natureSolService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedNatureSol.getCode();
		
        
    }
}
