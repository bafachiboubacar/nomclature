package ne.mha.sinea.nomenclature.parametreStationHydrometrique;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ParametreStationHydrometriqueForm {

	private int code;
	private String libelle;
	
}
