package ne.mha.sinea.nomenclature.parametreStationHydrometrique;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class ParametreStationHydrometriqueRestController {

	@Autowired
	ParametreStationHydrometriqueRepository parametreStationHydrometriqueService;
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/parametreStationHydrometrique")
	public int addParametreStationHydrometrique(@Validated ParametreStationHydrometriqueForm parametreStationHydrometriqueForm,BindingResult bindingResult, Model model) {
		ParametreStationHydrometrique savedParametreStationHydrometrique = new ParametreStationHydrometrique();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					ParametreStationHydrometrique P = new ParametreStationHydrometrique();
					P.setLibelle(parametreStationHydrometriqueForm.getLibelle());
					savedParametreStationHydrometrique = parametreStationHydrometriqueService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedParametreStationHydrometrique.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/updateParametreStationHydrometrique")
    public int updateParametreStationHydrometrique(@Validated ParametreStationHydrometriqueForm parametreStationHydrometriqueForm,BindingResult bindingResult, Model model) {
		ParametreStationHydrometrique savedParametreStationHydrometrique = new ParametreStationHydrometrique();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					ParametreStationHydrometrique P = parametreStationHydrometriqueService.findByCode(parametreStationHydrometriqueForm.getCode());
					P.setLibelle(parametreStationHydrometriqueForm.getLibelle());
					savedParametreStationHydrometrique = parametreStationHydrometriqueService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedParametreStationHydrometrique.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/deleteParametreStationHydrometrique")
    public int deleteParametreStationHydrometrique(@Validated ParametreStationHydrometriqueForm parametreStationHydrometriqueForm,BindingResult bindingResult, Model model) {
		ParametreStationHydrometrique savedParametreStationHydrometrique = new ParametreStationHydrometrique();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					ParametreStationHydrometrique P = parametreStationHydrometriqueService.findByCode(parametreStationHydrometriqueForm.getCode());
					P.setDeleted(true);
					savedParametreStationHydrometrique = parametreStationHydrometriqueService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedParametreStationHydrometrique.getCode();
		
        
    }
}
