package ne.mha.sinea.nomenclature.typeBranchement;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import ne.mha.sinea.Template;
@RestController
@Controller
public class typeBranchementController {

	@Autowired
	typeBranchementRepository typeBranchementRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/typeBranchement")
	public String  addTypeBranchement(typeBranchementForm typeBranchementForm, Model model) {
		try{
			List<typeBranchement> typeBranchement = typeBranchementRepository.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeBranchement", typeBranchement);
			model.addAttribute("viewPath", "nomenclature/typeBranchement/typeBranchement");
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
}
