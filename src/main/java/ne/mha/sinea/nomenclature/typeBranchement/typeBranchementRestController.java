package ne.mha.sinea.nomenclature.typeBranchement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@ResponseBody
public class typeBranchementRestController {

	@Autowired
	typeBranchementRepository typeBranchementRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/typeBranchement")
	public int addTypeBranchement(@Validated typeBranchementForm typeBranchementForm,BindingResult bindingResult, Model model) {
		typeBranchement savedTypeBranchement = new typeBranchement();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					typeBranchement P = new typeBranchement();
					P.setLibelle(typeBranchementForm.getLibelle());
					savedTypeBranchement = typeBranchementRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeBranchement.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateTypeBranchement")
    public int updateTypeBranchement(@Validated typeBranchementForm typeBranchementForm,BindingResult bindingResult, Model model) {
		typeBranchement savedTypeBranchement = new typeBranchement();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					typeBranchement P = typeBranchementRepository.findByCode(typeBranchementForm.getCode());
					P.setLibelle(typeBranchementForm.getLibelle());
					savedTypeBranchement = typeBranchementRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeBranchement.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteTypeBranchement")
    public int deleteTypeBranchement(@Validated typeBranchementForm typeBranchementForm,BindingResult bindingResult, Model model) {
		typeBranchement savedTypeBranchement = new typeBranchement();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					typeBranchement P = typeBranchementRepository.findByCode(typeBranchementForm.getCode());
					P.setDeleted(true);
					savedTypeBranchement = typeBranchementRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeBranchement.getCode();
		
        
    }
}
