package ne.mha.sinea.nomenclature.typeEquipement;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class typeEquipementController {

	@Autowired
	typeEquipementRepository typeEquipementRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/typeEquipement")
	public String  addTypeEquipement(typeEquipementForm typeEquipementForm, Model model) {
		try{
			List<typeEquipement> typeEquipement = typeEquipementRepository.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeEquipement", typeEquipement);
			model.addAttribute("viewPath", "nomenclature/typeEquipement/typeEquipement");
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
}
