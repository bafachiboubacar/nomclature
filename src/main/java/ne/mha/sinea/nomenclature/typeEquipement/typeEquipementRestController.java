package ne.mha.sinea.nomenclature.typeEquipement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class typeEquipementRestController {

	@Autowired
	typeEquipementRepository typeEquipementRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/typeEquipement")
	public int addTypeEquipement(@Validated typeEquipementForm typeEquipementForm,BindingResult bindingResult, Model model) {
		typeEquipement savedTypeEquipement = new typeEquipement();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					typeEquipement P = new typeEquipement();
					P.setLibelle(typeEquipementForm.getLibelle());
					savedTypeEquipement = typeEquipementRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeEquipement.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateTypeEquipement")
    public int updateTypeEquipement(@Validated typeEquipementForm typeEquipementForm,BindingResult bindingResult, Model model) {
		typeEquipement savedTypeEquipement = new typeEquipement();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					typeEquipement P = typeEquipementRepository.findByCode(typeEquipementForm.getCode());
					P.setLibelle(typeEquipementForm.getLibelle());
					savedTypeEquipement = typeEquipementRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeEquipement.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteTypeEquipement")
    public int deleteTypeEquipement(@Validated typeEquipementForm typeEquipementForm,BindingResult bindingResult, Model model) {
		typeEquipement savedTypeEquipement = new typeEquipement();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					typeEquipement P = typeEquipementRepository.findByCode(typeEquipementForm.getCode());
					P.setDeleted(true);
					savedTypeEquipement = typeEquipementRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeEquipement.getCode();
		
        
    }
}
