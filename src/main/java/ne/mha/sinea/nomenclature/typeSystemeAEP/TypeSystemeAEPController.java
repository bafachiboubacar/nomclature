package ne.mha.sinea.nomenclature.typeSystemeAEP;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;
@Controller
public class TypeSystemeAEPController {

	@Autowired
	TypeSystemeAEPRepository typeSystemeAEPService;
	
	//@PreAuthorize("hasAuthority('gestion des Type Systeme AEP')")
	@GetMapping("/typeSystemeAEP")
	public String  addTypeSystemeAEP(TypeSystemeAEPForm typeSystemeAEPForm, Model model) {
		try{
			List<TypeSystemeAEP> typeSystemeAEPs = typeSystemeAEPService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeSystemeAEPs", typeSystemeAEPs);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Nomenclature/Type Systeme AEP");
			model.addAttribute("viewPath", "nomenclature/typeSystemeAEP/typeSystemeAEP");
			
			}
		catch(Exception e){
				
			}
		
		return Template.defaultTemplate;

	}
	
	
}
