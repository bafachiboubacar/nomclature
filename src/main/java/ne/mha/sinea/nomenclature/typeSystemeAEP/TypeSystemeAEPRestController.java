package ne.mha.sinea.nomenclature.typeSystemeAEP;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class TypeSystemeAEPRestController {

	@Autowired
	TypeSystemeAEPRepository typeSystemeAEPService;
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/typeSystemeAEP")
	public int addTypeSystemeAEP(@Validated TypeSystemeAEPForm typeSystemeAEPForm,BindingResult bindingResult, Model model) {
		TypeSystemeAEP savedTypeSystemeAEP = new TypeSystemeAEP();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeSystemeAEP P = new TypeSystemeAEP();
					P.setLibelle(typeSystemeAEPForm.getLibelle());
					savedTypeSystemeAEP = typeSystemeAEPService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeSystemeAEP.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/updateTypeSystemeAEP")
    public int updateTypeSystemeAEP(@Validated TypeSystemeAEPForm typeSystemeAEPForm,BindingResult bindingResult, Model model) {
		TypeSystemeAEP savedTypeSystemeAEP = new TypeSystemeAEP();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeSystemeAEP P = typeSystemeAEPService.findByCode(typeSystemeAEPForm.getCode());
					P.setLibelle(typeSystemeAEPForm.getLibelle());
					savedTypeSystemeAEP = typeSystemeAEPService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeSystemeAEP.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/deleteTypeSystemeAEP")
    public int deleteTypeSystemeAEP(@Validated TypeSystemeAEPForm typeSystemeAEPForm,BindingResult bindingResult, Model model) {
		TypeSystemeAEP savedTypeSystemeAEP = new TypeSystemeAEP();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeSystemeAEP P = typeSystemeAEPService.findByCode(typeSystemeAEPForm.getCode());
					P.setDeleted(true);
					savedTypeSystemeAEP = typeSystemeAEPService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeSystemeAEP.getCode();
		
        
    }
}
