package ne.mha.sinea.nomenclature.quantiteEauAep;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface quantiteEauAepRepository extends CrudRepository<quantiteEauAep, Integer> {
	quantiteEauAep findByCode(Integer code);
	quantiteEauAep findByIsDeletedFalseAndLibelle(String libelle);
	List<quantiteEauAep> findByIsDeletedFalseOrderByLibelleAsc();
	List<quantiteEauAep> findByIsDeletedFalse();
	List<quantiteEauAep> findByIsDeletedTrue();
	List<quantiteEauAep> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<quantiteEauAep> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
