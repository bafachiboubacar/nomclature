package ne.mha.sinea.nomenclature.quantiteEauAep;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class quantiteEauAepRestController {

	@Autowired
	quantiteEauAepRepository quantiteEauAepRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/quantiteEauAep")
	public int addQuantiteEauAep(@Validated quantiteEauAepForm quantiteEauAepForm,BindingResult bindingResult, Model model) {
		quantiteEauAep savedQuantiteEauAep = new quantiteEauAep();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					quantiteEauAep P = new quantiteEauAep();
					P.setLibelle(quantiteEauAepForm.getLibelle());
					savedQuantiteEauAep = quantiteEauAepRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedQuantiteEauAep.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateQuantiteEauAep")
    public int updateQuantiteEauAep(@Validated quantiteEauAepForm quantiteEauAepForm,BindingResult bindingResult, Model model) {
		quantiteEauAep savedQuantiteEauAep = new quantiteEauAep();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					quantiteEauAep P = quantiteEauAepRepository.findByCode(quantiteEauAepForm.getCode());
					P.setLibelle(quantiteEauAepForm.getLibelle());
					savedQuantiteEauAep = quantiteEauAepRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedQuantiteEauAep.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteQuantiteEauAep")
    public int deleteQuantiteEauAep(@Validated quantiteEauAepForm quantiteEauAepForm,BindingResult bindingResult, Model model) {
		quantiteEauAep savedQuantiteEauAep = new quantiteEauAep();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					quantiteEauAep P = quantiteEauAepRepository.findByCode(quantiteEauAepForm.getCode());
					P.setDeleted(true);
					savedQuantiteEauAep = quantiteEauAepRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedQuantiteEauAep.getCode();
		
        
    }
}
