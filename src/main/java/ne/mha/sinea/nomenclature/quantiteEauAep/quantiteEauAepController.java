package ne.mha.sinea.nomenclature.quantiteEauAep;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class quantiteEauAepController {

	@Autowired
	quantiteEauAepRepository quantiteEauAepRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/quantiteEauAep")
	public String  addQuantiteEauAep(quantiteEauAepForm quantiteEauAepForm, Model model) {
		try{
			List<quantiteEauAep> quantiteEauAep = quantiteEauAepRepository.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("quantiteEauAep", quantiteEauAep);
			model.addAttribute("viewPath", "nomenclature/quantiteEauAep/quantiteEauAep");
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
}
