package ne.mha.sinea.nomenclature.intervenant;

import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class IntervenantForm {

	private String denomination;
	private Integer codeTypeIntervenant;
}
