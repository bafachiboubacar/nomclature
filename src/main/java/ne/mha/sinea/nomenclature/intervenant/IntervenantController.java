package ne.mha.sinea.nomenclature.intervenant;

import java.util.List;

import javax.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import ne.mha.sinea.Template;
import ne.mha.sinea.nomenclature.typeIntervenant.TypeIntervenant;
import ne.mha.sinea.nomenclature.typeIntervenant.TypeIntervenantRepository;

@Controller
public class IntervenantController {

	@Autowired
	EntityManager em;
	
	@Autowired
	IntervenantRepository intervenantService;
	@Autowired
	TypeIntervenantRepository typeIntervenantService;

	// formulaire intervenant dont l'ajout , l'import et l'export
	// @PreAuthorize("hasAuthority('Ajout Intervenant'))
	@GetMapping("/intervenant")
	public String addIntervenant(@Validated IntervenantForm intervenantForm, Model model) {
		try {
			List<TypeIntervenant> typeIntervenants = typeIntervenantService.findByIsDeletedFalse();
			model.addAttribute("typeIntervenants", typeIntervenants);
			List<Intervenant> intervenants = intervenantService.findByIsDeletedFalse();
			model.addAttribute("intervenants", intervenants);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Nomenclature/Intervenant");
			model.addAttribute("viewPath", "nomenclature/intervenant/intervenant");
			
		} catch (Exception e) {

		}
		return Template.defaultTemplate;

	}

	// validation du formulaire d'ajout intervenant
	@PostMapping("/intervenant")
	public String addIntervenantsSubmit(@Validated IntervenantForm intervenantForm, BindingResult bindingResult,
			Model model) {
		// s'il n'ya pas d'erreur lors de la validation des données du formulaire
		if (!bindingResult.hasErrors()) {

			try {
				// récuperer les données saisies
				Intervenant I = new Intervenant();
				I.setDenomination(intervenantForm.getDenomination());
				I.setTypeIntervenant(typeIntervenantService.findByCode(intervenantForm.getCodeTypeIntervenant()));
				intervenantService.save(I);

				// indiquer que l'operation d'ajout a réussie
				model.addAttribute("operationStatus", "operationStatus/success");
				List<TypeIntervenant> typeIntervenants = typeIntervenantService.findByIsDeletedFalse();
				model.addAttribute("typeIntervenants", typeIntervenants);
				List<Intervenant> intervenants = intervenantService.findByIsDeletedFalse();
				model.addAttribute("intervenants", intervenants);
				model.addAttribute("horizontalMenu", "horizontalMenu");
				model.addAttribute("sidebarMenu", "configurationSidebarMenu");
				model.addAttribute("breadcrumb", "breadcrumb");
				model.addAttribute("navigationPath", "Nomenclature/Intervenant");
				model.addAttribute("viewPath", "nomenclature/intervenant/intervenant");
				
			} catch (Exception e) {
				// indiquer que l'operation d'ajout a réussie
				model.addAttribute("operationStatus", "operationStatus/unsuccess");
				List<TypeIntervenant> typeIntervenants = typeIntervenantService.findByIsDeletedFalse();
				model.addAttribute("typeIntervenants", typeIntervenants);
				List<Intervenant> intervenants = intervenantService.findByIsDeletedFalse();
				// envoie de la liste récuperée à la vue à travers le modele
				model.addAttribute("intervenants", intervenants);
				model.addAttribute("horizontalMenu", "horizontalMenu");
				model.addAttribute("sidebarMenu", "configurationSidebarMenu");
				model.addAttribute("breadcrumb", "breadcrumb");
				model.addAttribute("navigationPath", "Nomenclature/Intervenant");
				model.addAttribute("viewPath", "nomenclature/intervenant/intervenant");
				
			}

		} else {
			try {
				// indiquer que l'operation d'ajout a réussie
				model.addAttribute("operationStatus", "operationStatus/unsuccess");
				List<TypeIntervenant> typeIntervenants = typeIntervenantService.findByIsDeletedFalse();
				model.addAttribute("typeIntervenants", typeIntervenants);
				List<Intervenant> intervenants = intervenantService.findByIsDeletedFalse();
				// envoie de la liste récuperée à la vue à travers le modele
				model.addAttribute("intervenants", intervenants);
				model.addAttribute("horizontalMenu", "horizontalMenu");
				model.addAttribute("sidebarMenu", "configurationSidebarMenu");
				model.addAttribute("breadcrumb", "breadcrumb");
				model.addAttribute("navigationPath", "Nomenclature/Intervenant");
				model.addAttribute("viewPath", "nomenclature/intervenant/intervenant");
				
			} catch (Exception e) {

			}
		}

		// afficher le template avec la vue (formulaire) à l'intérieur
		return Template.defaultTemplate;

	}

	// modification d'une intervenant
	@GetMapping("/updateIntervenant/{codeIntervenant}")
	public String updateIntervenant(@PathVariable("codeIntervenant") int codeIntervenant,
			IntervenantForm intervenantForm, Model model) {
		try {
			// récuperer les informations de la intervenant à modifier
			Intervenant intervenant = intervenantService.findByCode(codeIntervenant);
			// envoie des informations de la intervenant à modifier à travers le modele
			intervenantForm.setDenomination(intervenant.getDenomination());
			intervenantForm.setCodeTypeIntervenant(intervenant.getTypeIntervenant().getCode());

			// récuperation de la liste des intervenant de la base de données
			List<TypeIntervenant> typeIntervenants = typeIntervenantService.findByIsDeletedFalse();;
			model.addAttribute("typeIntervenants", typeIntervenants);
			List<Intervenant> intervenants = intervenantService.findByIsDeletedFalse();
			model.addAttribute("intervenants", intervenants);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Nomenclature/Intervenant");
			model.addAttribute("viewPath", "nomenclature/intervenant/intervenant");
			
		} catch (Exception e) {

		}
		return Template.defaultTemplate;

	}

	@PostMapping("/updateIntervenant/{codeIntervenant}")
	public RedirectView updateIntervenantSubmit(@Validated IntervenantForm intervenantForm, BindingResult bindingResult,
			@PathVariable("codeIntervenant") int codeIntervenant, Model model, RedirectAttributes redirectAttributes) {
		final RedirectView redirectView = new RedirectView("/intervenant", true);
		if (!bindingResult.hasErrors()) {
			try {
				// recupérer les informations saisies
				Intervenant I = intervenantService.findByCode(codeIntervenant);
				I.setDenomination(intervenantForm.getDenomination());
				I.setTypeIntervenant(typeIntervenantService.findByCode(intervenantForm.getCodeTypeIntervenant()));
				// enregistrer les informations dans la base de données
				intervenantService.save(I);

				// indiquer que l'operation de modification a réussi
				redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/success");

			} catch (Exception e) {
				// indiquer que l'operation de modification a échoué
				redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/unsuccess");
			}
		} else {

		}

		// redirection vers la page d'ajout
		return redirectView;

	}

	@GetMapping("/deleteIntervenant/{codeIntervenant}")
	public RedirectView deleteIntervenant(@PathVariable("codeIntervenant") int codeIntervenant, Model model,
			RedirectAttributes redirectAttributes) {
		final RedirectView redirectView = new RedirectView("/intervenant", true);
		try {
			Intervenant I = intervenantService.findByCode(codeIntervenant);
			I.setDeleted(true);
			intervenantService.save(I);;

			// indiquer que l'operation de suppression a réussi
			redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/success");
		} catch (Exception e) {
			// indiquer que l'operation de suppression a échoué
			redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/unsuccess");
		}

		// redirection vers la page d'ajout
		return redirectView;

	}

}
