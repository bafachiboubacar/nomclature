package ne.mha.sinea.nomenclature.intervenant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ne.mha.sinea.CommonProperties;
import ne.mha.sinea.nomenclature.typeIntervenant.TypeIntervenant;

//annotation de lombok pour générer les getter et setter
@Data
//annotation de lombok pour générer le constructeur sans paramètre
@NoArgsConstructor
//annotation de lombok pour générer le constructeur avec tous les paramètres
@AllArgsConstructor
//indique qu'il s'agit d'une entité=> création d'une table au niveau de la base de données
@Entity
@Table(name = "intervenant")
public class Intervenant extends CommonProperties {

	// annotation pour indiquier l'identifiant d'une table
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "code")
	private Integer code;
	@NotNull
	@Column(name = "denomination")
	private String denomination;
	@JoinColumn(name = "code_type_intervenant", referencedColumnName = "code")
	@ManyToOne
	private TypeIntervenant typeIntervenant;

}
