package ne.mha.sinea.nomenclature.methodeTraitementEauEcole;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class methodeTraitementEauEcoleForm {

	private int code;
	private String libelle;
	
}
