package ne.mha.sinea.nomenclature.methodeTraitementEauEcole;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class methodeTraitementEauEcoleController {

	@Autowired
	methodeTraitementEauEcoleRepository methodeTraitementEauEcoleRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/methodeTraitementEauEcole")
	public String  addDisponibiliteEau(methodeTraitementEauEcoleForm methodeTraitementEauEcoleForm, Model model) {
		try{
			List<methodeTraitementEauEcole> methodeTraitementEauEcole = methodeTraitementEauEcoleRepository.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("methodeTraitementEauEcole", methodeTraitementEauEcole);
			model.addAttribute("viewPath", "nomenclature/methodeTraitementEauEcole/methodeTraitementEauEcole");
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
}
