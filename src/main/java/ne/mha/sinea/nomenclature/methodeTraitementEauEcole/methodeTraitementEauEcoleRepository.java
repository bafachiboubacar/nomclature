package ne.mha.sinea.nomenclature.methodeTraitementEauEcole;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface methodeTraitementEauEcoleRepository extends CrudRepository<methodeTraitementEauEcole, Integer> {
	methodeTraitementEauEcole findByCode(Integer code);
	methodeTraitementEauEcole findByIsDeletedFalseAndLibelle(String libelle);
	List<methodeTraitementEauEcole> findByIsDeletedFalseOrderByLibelleAsc();
	List<methodeTraitementEauEcole> findByIsDeletedFalse();
	List<methodeTraitementEauEcole> findByIsDeletedTrue();
	List<methodeTraitementEauEcole> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<methodeTraitementEauEcole> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
