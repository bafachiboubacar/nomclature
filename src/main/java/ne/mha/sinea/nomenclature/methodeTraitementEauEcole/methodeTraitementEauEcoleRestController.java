package ne.mha.sinea.nomenclature.methodeTraitementEauEcole;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class methodeTraitementEauEcoleRestController {

	@Autowired
	methodeTraitementEauEcoleRepository methodeTraitementEauEcoleRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/methodeTraitementEauEcole")
	public int addMethodeTraitementEauEcole(@Validated methodeTraitementEauEcoleForm methodeTraitementEauEcoleForm,BindingResult bindingResult, Model model) {
		methodeTraitementEauEcole savedMethodeTraitementEauEcole = new methodeTraitementEauEcole();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					methodeTraitementEauEcole P = new methodeTraitementEauEcole();
					P.setLibelle(methodeTraitementEauEcoleForm.getLibelle());
					savedMethodeTraitementEauEcole = methodeTraitementEauEcoleRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedMethodeTraitementEauEcole.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateMethodeTraitementEauEcole")
    public int updateMethodeTraitementEauEcole(@Validated methodeTraitementEauEcoleForm methodeTraitementEauEcoleForm,BindingResult bindingResult, Model model) {
		methodeTraitementEauEcole savedMethodeTraitementEauEcole = new methodeTraitementEauEcole();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					methodeTraitementEauEcole P = methodeTraitementEauEcoleRepository.findByCode(methodeTraitementEauEcoleForm.getCode());
					P.setLibelle(methodeTraitementEauEcoleForm.getLibelle());
					savedMethodeTraitementEauEcole = methodeTraitementEauEcoleRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedMethodeTraitementEauEcole.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteMethodeTraitementEauEcole")
    public int deleteMethodeTraitementEauEcole(@Validated methodeTraitementEauEcoleForm methodeTraitementEauEcoleForm,BindingResult bindingResult, Model model) {
		methodeTraitementEauEcole savedMethodeTraitementEauEcole = new methodeTraitementEauEcole();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					methodeTraitementEauEcole P = methodeTraitementEauEcoleRepository.findByCode(methodeTraitementEauEcoleForm.getCode());
					P.setDeleted(true);
					savedMethodeTraitementEauEcole = methodeTraitementEauEcoleRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedMethodeTraitementEauEcole.getCode();
		
        
    }
}
