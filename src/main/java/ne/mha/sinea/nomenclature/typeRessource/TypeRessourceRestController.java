package ne.mha.sinea.nomenclature.typeRessource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class TypeRessourceRestController {

	@Autowired
	TypeRessourceRepository typeRessourceService;
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/typeRessource")
	public int addTypeRessource(@Validated TypeRessourceForm typeRessourceForm,BindingResult bindingResult, Model model) {
		TypeRessource savedTypeRessource = new TypeRessource();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeRessource P = new TypeRessource();
					P.setLibelle(typeRessourceForm.getLibelle());
					savedTypeRessource = typeRessourceService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeRessource.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/updateTypeRessource")
    public int updateTypeRessource(@Validated TypeRessourceForm typeRessourceForm,BindingResult bindingResult, Model model) {
		TypeRessource savedTypeRessource = new TypeRessource();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeRessource P = typeRessourceService.findByCode(typeRessourceForm.getCode());
					P.setLibelle(typeRessourceForm.getLibelle());
					savedTypeRessource = typeRessourceService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeRessource.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/deleteTypeRessource")
    public int deleteTypeRessource(@Validated TypeRessourceForm typeRessourceForm,BindingResult bindingResult, Model model) {
		TypeRessource savedTypeRessource = new TypeRessource();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeRessource P = typeRessourceService.findByCode(typeRessourceForm.getCode());
					P.setDeleted(true);
					savedTypeRessource = typeRessourceService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeRessource.getCode();
		
        
    }
}
