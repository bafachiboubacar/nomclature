package ne.mha.sinea.nomenclature.typeRessource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;
@Controller
public class TypeRessourceController {

	@Autowired
	TypeRessourceRepository typeRessourceService;
	
	//@PreAuthorize("hasAuthority('gestion des types ressources')")
	@GetMapping("/typeRessource")
	public String  addTypeRessource(TypeRessourceForm typeRessourceForm, Model model) {
		try{
			List<TypeRessource> typeRessources = typeRessourceService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeRessources", typeRessources);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Nomenclature/Type Ressource");
			model.addAttribute("viewPath", "nomenclature/typeRessource/typeRessource");
			
			}
		catch(Exception e){
				
			}
		
		return Template.defaultTemplate;

	}
	
	
}
