package ne.mha.sinea.nomenclature.typeIntervenant;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TypeIntervenantForm {

	private int code;
	private String libelle;
	
}
