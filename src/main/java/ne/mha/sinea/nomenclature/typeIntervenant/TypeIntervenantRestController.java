package ne.mha.sinea.nomenclature.typeIntervenant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class TypeIntervenantRestController {

	@Autowired
	TypeIntervenantRepository typeIntervenantService;
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/typeIntervenant")
	public int addTypeIntervenant(@Validated TypeIntervenantForm typeIntervenantForm,BindingResult bindingResult, Model model) {
		TypeIntervenant savedTypeIntervenant = new TypeIntervenant();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeIntervenant P = new TypeIntervenant();
					P.setLibelle(typeIntervenantForm.getLibelle());
					savedTypeIntervenant = typeIntervenantService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeIntervenant.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/updateTypeIntervenant")
    public int updateTypeIntervenant(@Validated TypeIntervenantForm typeIntervenantForm,BindingResult bindingResult, Model model) {
		TypeIntervenant savedTypeIntervenant = new TypeIntervenant();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeIntervenant P = typeIntervenantService.findByCode(typeIntervenantForm.getCode());
					P.setLibelle(typeIntervenantForm.getLibelle());
					savedTypeIntervenant = typeIntervenantService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeIntervenant.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/deleteTypeIntervenant")
    public int deleteTypeIntervenant(@Validated TypeIntervenantForm typeIntervenantForm,BindingResult bindingResult, Model model) {
		TypeIntervenant savedTypeIntervenant = new TypeIntervenant();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeIntervenant P = typeIntervenantService.findByCode(typeIntervenantForm.getCode());
					P.setDeleted(true);
					savedTypeIntervenant = typeIntervenantService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeIntervenant.getCode();
		
        
    }
}
