package ne.mha.sinea.nomenclature.typeIntervenant;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;
@Controller
public class TypeIntervenantController {

	@Autowired
	TypeIntervenantRepository typeIntervenantService;
	
	//@PreAuthorize("hasAuthority('gestion des Type Intervenant')")
	@GetMapping("/typeIntervenant")
	public String  addTypeIntervenant(TypeIntervenantForm typeIntervenantForm, Model model) {
		try{
			List<TypeIntervenant> typeIntervenants = typeIntervenantService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeIntervenants", typeIntervenants);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Nomenclature/Type Intervenant");
			model.addAttribute("viewPath", "nomenclature/typeIntervenant/typeIntervenant");
			
			}
		catch(Exception e){
				
			}
		
		return Template.defaultTemplate;

	}
	
	
}
