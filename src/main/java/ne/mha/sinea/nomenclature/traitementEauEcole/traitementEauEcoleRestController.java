package ne.mha.sinea.nomenclature.traitementEauEcole;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class traitementEauEcoleRestController {

	@Autowired
	traitementEauEcoleRepository traitementEauEcoleRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/traitementEauEcole")
	public int addTraitementEauEcole(@Validated traitementEauEcoleForm traitementEauEcoleForm,BindingResult bindingResult, Model model) {
		traitementEauEcole savedTraitementEauEcole = new traitementEauEcole();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					traitementEauEcole P = new traitementEauEcole();
					P.setLibelle(traitementEauEcoleForm.getLibelle());
					savedTraitementEauEcole = traitementEauEcoleRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTraitementEauEcole.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateTraitementEauEcole")
    public int updateTraitementEauEcole(@Validated traitementEauEcoleForm traitementEauEcoleForm,BindingResult bindingResult, Model model) {
		traitementEauEcole savedTraitementEauEcole = new traitementEauEcole();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					traitementEauEcole P = traitementEauEcoleRepository.findByCode(traitementEauEcoleForm.getCode());
					P.setLibelle(traitementEauEcoleForm.getLibelle());
					savedTraitementEauEcole = traitementEauEcoleRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTraitementEauEcole.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteTraitementEauEcole")
    public int deleteTraitementEauEcole(@Validated traitementEauEcoleForm traitementEauEcoleForm,BindingResult bindingResult, Model model) {
		traitementEauEcole savedTraitementEauEcole = new traitementEauEcole();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					traitementEauEcole P = traitementEauEcoleRepository.findByCode(traitementEauEcoleForm.getCode());
					P.setDeleted(true);
					savedTraitementEauEcole = traitementEauEcoleRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTraitementEauEcole.getCode();
		
        
    }
}
