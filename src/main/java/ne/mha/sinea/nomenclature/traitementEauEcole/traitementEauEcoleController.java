package ne.mha.sinea.nomenclature.traitementEauEcole;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class traitementEauEcoleController {

	@Autowired
	traitementEauEcoleRepository traitementEauEcoleRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/traitementEauEcole")
	public String  addTraitementEauEcole(traitementEauEcoleForm traitementEauEcoleForm, Model model) {
		try{
			List<traitementEauEcole> traitementEauEcole = traitementEauEcoleRepository.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("traitementEauEcole", traitementEauEcole);
			model.addAttribute("viewPath", "nomenclature/traitementEauEcole/traitementEauEcole");
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
}
