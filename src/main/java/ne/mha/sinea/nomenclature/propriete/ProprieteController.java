package ne.mha.sinea.nomenclature.propriete;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;
@Controller
public class ProprieteController {

	@Autowired
	ProprieteRepository proprieteService;
	
	//@PreAuthorize("hasAuthority('gestion des types ressources')")
	@GetMapping("/propriete")
	public String  addPropriete(ProprieteForm proprieteForm, Model model) {
		try{
			List<Propriete> proprietes = proprieteService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("proprietes", proprietes);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Nomenclature/Type Ressource");
			model.addAttribute("viewPath", "nomenclature/propriete/propriete");
			
			}
		catch(Exception e){
				
			}
		
		return Template.defaultTemplate;

	}
	
	
}
