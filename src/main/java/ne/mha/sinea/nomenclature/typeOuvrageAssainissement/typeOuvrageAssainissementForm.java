package ne.mha.sinea.nomenclature.typeOuvrageAssainissement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class typeOuvrageAssainissementForm {

	private int code;
	private String libelle;
	
}
