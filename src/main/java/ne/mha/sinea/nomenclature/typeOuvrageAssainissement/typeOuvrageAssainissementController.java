package ne.mha.sinea.nomenclature.typeOuvrageAssainissement;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class typeOuvrageAssainissementController {

	@Autowired
	typeOuvrageAssainissementRepository typeOuvrageAssainissementRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/typeOuvrageAssainissement")
	public String  addTypeOuvrageAssainissement(typeOuvrageAssainissementForm typeOuvrageAssainissementForm, Model model) {
		try{
			List<typeOuvrageAssainissement> typeOuvrageAssainissement = typeOuvrageAssainissementRepository.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeOuvrageAssainissement", typeOuvrageAssainissement);
			model.addAttribute("viewPath", "nomenclature/typeOuvrageAssainissement/typeOuvrageAssainissement");
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
}
