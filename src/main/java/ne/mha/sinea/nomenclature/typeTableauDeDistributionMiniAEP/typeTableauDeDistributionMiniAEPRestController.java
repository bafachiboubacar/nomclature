package ne.mha.sinea.nomenclature.typeTableauDeDistributionMiniAEP;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class typeTableauDeDistributionMiniAEPRestController {

	@Autowired
	typeTableauDeDistributionMiniAEPRepository typeTableauDeDistributionMiniAEPRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/typeTableauDeDistributionMiniAEP")
	public int addTypeTableauDeDistributionMiniAEP(@Validated typeTableauDeDistributionMiniAEPForm typeTableauDeDistributionMiniAEPForm,BindingResult bindingResult, Model model) {
		typeTableauDeDistributionMiniAEP savedTypeTableauDeDistributionMiniAEP = new typeTableauDeDistributionMiniAEP();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					typeTableauDeDistributionMiniAEP P = new typeTableauDeDistributionMiniAEP();
					P.setLibelle(typeTableauDeDistributionMiniAEPForm.getLibelle());
					savedTypeTableauDeDistributionMiniAEP = typeTableauDeDistributionMiniAEPRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeTableauDeDistributionMiniAEP.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateTypeTableauDeDistributionMiniAEP")
    public int updateTypeTableauDeDistributionMiniAEP(@Validated typeTableauDeDistributionMiniAEPForm typeTableauDeDistributionMiniAEPForm,BindingResult bindingResult, Model model) {
		typeTableauDeDistributionMiniAEP savedTypeTableauDeDistributionMiniAEP = new typeTableauDeDistributionMiniAEP();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					typeTableauDeDistributionMiniAEP P = typeTableauDeDistributionMiniAEPRepository.findByCode(typeTableauDeDistributionMiniAEPForm.getCode());
					P.setLibelle(typeTableauDeDistributionMiniAEPForm.getLibelle());
					savedTypeTableauDeDistributionMiniAEP = typeTableauDeDistributionMiniAEPRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeTableauDeDistributionMiniAEP.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteTypeTableauDeDistributionMiniAEP")
    public int deleteTypeTableauDeDistributionMiniAEP(@Validated typeTableauDeDistributionMiniAEPForm typeTableauDeDistributionMiniAEPForm,BindingResult bindingResult, Model model) {
		typeTableauDeDistributionMiniAEP savedTypeTableauDeDistributionMiniAEP = new typeTableauDeDistributionMiniAEP();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					typeTableauDeDistributionMiniAEP P = typeTableauDeDistributionMiniAEPRepository.findByCode(typeTableauDeDistributionMiniAEPForm.getCode());
					P.setDeleted(true);
					savedTypeTableauDeDistributionMiniAEP = typeTableauDeDistributionMiniAEPRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeTableauDeDistributionMiniAEP.getCode();
		
        
    }
}
