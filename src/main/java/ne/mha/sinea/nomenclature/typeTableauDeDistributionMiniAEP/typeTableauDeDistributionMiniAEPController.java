package ne.mha.sinea.nomenclature.typeTableauDeDistributionMiniAEP;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class typeTableauDeDistributionMiniAEPController {

	@Autowired
	typeTableauDeDistributionMiniAEPRepository typeTableauDeDistributionMiniAEPRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/typeTableauDeDistributionMiniAEP")
	public String  addTypeTableauDeDistributionMiniAEP(typeTableauDeDistributionMiniAEPForm typeTableauDeDistributionMiniAEPForm, Model model) {
		try{
			List<typeTableauDeDistributionMiniAEP> typeTableauDeDistributionMiniAEP = typeTableauDeDistributionMiniAEPRepository.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeTableauDeDistributionMiniAEP", typeTableauDeDistributionMiniAEP);
			model.addAttribute("viewPath", "nomenclature/typeTableauDeDistributionMiniAEP/typeTableauDeDistributionMiniAEP");
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
}
