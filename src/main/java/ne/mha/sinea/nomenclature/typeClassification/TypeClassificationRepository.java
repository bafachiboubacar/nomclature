package ne.mha.sinea.nomenclature.typeClassification;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface TypeClassificationRepository extends CrudRepository<TypeClassification, Integer> {
	TypeClassification findByCode(Integer code);
	TypeClassification findByLibelle(String libelle);
	TypeClassification findByIsDeletedFalseAndLibelle(String libelle);
	List<TypeClassification> findByIsDeletedFalseOrderByLibelleAsc();
	List<TypeClassification> findByIsDeletedFalse();
	List<TypeClassification> findByIsDeletedTrue();

	
	

}
