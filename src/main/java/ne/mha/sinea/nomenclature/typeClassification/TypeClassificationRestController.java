package ne.mha.sinea.nomenclature.typeClassification;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class TypeClassificationRestController {

	@Autowired
	TypeClassificationRepository typeClassificationService;
	
	//@PreAuthorize("hasAuthority('gestion des types de classification')")
	@PostMapping("/typeClassification")
	public int addTypeClassification(@Validated TypeClassificationForm typeClassificationForm,BindingResult bindingResult, Model model) {
		TypeClassification savedTypeClassification = new TypeClassification();
		if (!bindingResult.hasErrors()) {
			
			try {
				
				TypeClassification P = new TypeClassification();
				P.setLibelle(typeClassificationForm.getLibelle());
				savedTypeClassification = typeClassificationService.save(P);
				
				
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeClassification.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types de classification')")
	@PostMapping("/updateTypeClassification")
    public int updateTypeClassification(@Validated TypeClassificationForm typeClassificationForm,BindingResult bindingResult, Model model) {
		TypeClassification savedTypeClassification = new TypeClassification();
		if (!bindingResult.hasErrors()) {
			
			try {
				
				TypeClassification P = typeClassificationService.findByCode(typeClassificationForm.getCode());
				P.setLibelle(typeClassificationForm.getLibelle());
				savedTypeClassification = typeClassificationService.save(P);
				
				}
			catch(Exception e){
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeClassification.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types de classification')")
	@PostMapping("/deleteTypeClassification")
    public int deleteTypeClassification(@Validated TypeClassificationForm typeClassificationForm,BindingResult bindingResult, Model model) {
		TypeClassification savedTypeClassification = new TypeClassification();
		if (!bindingResult.hasErrors()) {
			
			try {
				
				TypeClassification P = typeClassificationService.findByCode(typeClassificationForm.getCode());
				P.setDeleted(true);
				savedTypeClassification = typeClassificationService.save(P);
				
				
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeClassification.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types de classification')")
	@GetMapping("/getTypeClassification/{code}")
	public String  listeTypeClassification(@PathVariable("code") int code) {
		String res = "";
		try{
			
			TypeClassification typeClassification = typeClassificationService.findByCode(code);
			res += ",{ id:"+typeClassification.getCode()+", pId:0, name:\""+typeClassification.getLibelle()+"\"}";
			res = "[{ id:0, pId:0, name:\"/\", open:true}"+res+"]";
			
			}
		catch(Exception e){
				
			}
		return res;
	}
	
	
	
}
