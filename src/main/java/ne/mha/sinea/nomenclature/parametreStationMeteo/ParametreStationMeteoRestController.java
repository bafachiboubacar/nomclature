package ne.mha.sinea.nomenclature.parametreStationMeteo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class ParametreStationMeteoRestController {

	@Autowired
	ParametreStationMeteoRepository parametreStationMeteoService;
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/parametreStationMeteo")
	public int addParametreStationMeteo(@Validated ParametreStationMeteoForm parametreStationMeteoForm,BindingResult bindingResult, Model model) {
		ParametreStationMeteo savedParametreStationMeteo = new ParametreStationMeteo();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					ParametreStationMeteo P = new ParametreStationMeteo();
					P.setLibelle(parametreStationMeteoForm.getLibelle());
					savedParametreStationMeteo = parametreStationMeteoService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedParametreStationMeteo.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/updateParametreStationMeteo")
    public int updateParametreStationMeteo(@Validated ParametreStationMeteoForm parametreStationMeteoForm,BindingResult bindingResult, Model model) {
		ParametreStationMeteo savedParametreStationMeteo = new ParametreStationMeteo();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					ParametreStationMeteo P = parametreStationMeteoService.findByCode(parametreStationMeteoForm.getCode());
					P.setLibelle(parametreStationMeteoForm.getLibelle());
					savedParametreStationMeteo = parametreStationMeteoService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedParametreStationMeteo.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/deleteParametreStationMeteo")
    public int deleteParametreStationMeteo(@Validated ParametreStationMeteoForm parametreStationMeteoForm,BindingResult bindingResult, Model model) {
		ParametreStationMeteo savedParametreStationMeteo = new ParametreStationMeteo();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					ParametreStationMeteo P = parametreStationMeteoService.findByCode(parametreStationMeteoForm.getCode());
					P.setDeleted(true);
					savedParametreStationMeteo = parametreStationMeteoService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedParametreStationMeteo.getCode();
		
        
    }
}
