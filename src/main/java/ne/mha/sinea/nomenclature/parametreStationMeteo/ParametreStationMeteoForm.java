package ne.mha.sinea.nomenclature.parametreStationMeteo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ParametreStationMeteoForm {

	private int code;
	private String libelle;
	
}
