package ne.mha.sinea.nomenclature.tuyauterie;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class TuyauterieRestController {

	@Autowired
	TuyauterieRepository tuyauterieService;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/tuyauterie")
	public int addTuyauterie(@Validated TuyauterieForm tuyauterieForm,BindingResult bindingResult, Model model) {
		Tuyauterie savedTuyauterie = new Tuyauterie();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					Tuyauterie P = new Tuyauterie();
					P.setLibelle(tuyauterieForm.getLibelle());
					savedTuyauterie = tuyauterieService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTuyauterie.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateTuyauterie")
    public int updateTuyauterie(@Validated TuyauterieForm tuyauterieForm,BindingResult bindingResult, Model model) {
		Tuyauterie savedTuyauterie = new Tuyauterie();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					Tuyauterie P = tuyauterieService.findByCode(tuyauterieForm.getCode());
					P.setLibelle(tuyauterieForm.getLibelle());
					savedTuyauterie = tuyauterieService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTuyauterie.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteTuyauterie")
    public int deleteTuyauterie(@Validated TuyauterieForm tuyauterieForm,BindingResult bindingResult, Model model) {
		Tuyauterie savedTuyauterie = new Tuyauterie();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					Tuyauterie P = tuyauterieService.findByCode(tuyauterieForm.getCode());
					P.setDeleted(true);
					savedTuyauterie = tuyauterieService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTuyauterie.getCode();
		
        
    }
}
