package ne.mha.sinea.nomenclature.tuyauterie;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TuyauterieForm {

	private int code;
	private String libelle;
	
}
