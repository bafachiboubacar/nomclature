package ne.mha.sinea.nomenclature.typeTube;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TypeTubeForm {

	private int code;
	private String libelle;
	
}
