package ne.mha.sinea.nomenclature.typeReseau;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class typeReseauForm {

	private int code;
	private String libelle;
	
}
