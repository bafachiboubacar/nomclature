package ne.mha.sinea.nomenclature.typeReseau;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class typeReseauRestController {

	@Autowired
	typeReseauRepository typeReseauRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/typeReseau")
	public int addTypeReseau(@Validated typeReseauForm typeReseauForm,BindingResult bindingResult, Model model) {
		typeReseau savedTypeReseau = new typeReseau();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					typeReseau P = new typeReseau();
					P.setLibelle(typeReseauForm.getLibelle());
					savedTypeReseau = typeReseauRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeReseau.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateTypeReseau")
    public int updateTypeReseau(@Validated typeReseauForm typeReseauForm,BindingResult bindingResult, Model model) {
		typeReseau savedTypeReseau = new typeReseau();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					typeReseau P = typeReseauRepository.findByCode(typeReseauForm.getCode());
					P.setLibelle(typeReseauForm.getLibelle());
					savedTypeReseau = typeReseauRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeReseau.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteTypeReseau")
    public int deleteTypeReseau(@Validated typeReseauForm typeReseauForm,BindingResult bindingResult, Model model) {
		typeReseau savedTypeReseau = new typeReseau();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					typeReseau P = typeReseauRepository.findByCode(typeReseauForm.getCode());
					P.setDeleted(true);
					savedTypeReseau = typeReseauRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeReseau.getCode();
		
        
    }
}
