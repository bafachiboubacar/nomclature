package ne.mha.sinea.nomenclature.disponibiliteEauPCV;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class disponibiliteEauPVCRestController {

	@Autowired
	disponibiliteEauPVCRepository disponibiliteEauPVCRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/disponibiliteEauPVCP")
	public int addDisponibiliteEauPVC(@Validated disponibiliteEauPVCForm disponibiliteEauPVCForm,BindingResult bindingResult, Model model) {
		disponibiliteEauPVC savedDisponibiliteEauPVC = new disponibiliteEauPVC();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					disponibiliteEauPVC P = new disponibiliteEauPVC();
					P.setLibelle(disponibiliteEauPVCForm.getLibelle());
					savedDisponibiliteEauPVC = disponibiliteEauPVCRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedDisponibiliteEauPVC.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateDisponibiliteEauPVC")
    public int updateDisponibiliteEauPVC(@Validated disponibiliteEauPVCForm disponibiliteEauPVCForm,BindingResult bindingResult, Model model) {
		disponibiliteEauPVC savedDisponibiliteEauPVC = new disponibiliteEauPVC();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					disponibiliteEauPVC P = disponibiliteEauPVCRepository.findByCode(disponibiliteEauPVCForm.getCode());
					P.setLibelle(disponibiliteEauPVCForm.getLibelle());
					savedDisponibiliteEauPVC = disponibiliteEauPVCRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedDisponibiliteEauPVC.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteDisponibiliteEauPVC")
    public int deleteDisponibiliteEauPVC(@Validated disponibiliteEauPVCForm disponibiliteEauPVCForm,BindingResult bindingResult, Model model) {
		disponibiliteEauPVC savedDisponibiliteEauPVC = new disponibiliteEauPVC();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					disponibiliteEauPVC P = disponibiliteEauPVCRepository.findByCode(disponibiliteEauPVCForm.getCode());
					P.setDeleted(true);
					savedDisponibiliteEauPVC = disponibiliteEauPVCRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedDisponibiliteEauPVC.getCode();
		
        
    }
}
