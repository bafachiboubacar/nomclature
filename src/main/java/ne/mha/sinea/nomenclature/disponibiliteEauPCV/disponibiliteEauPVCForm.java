package ne.mha.sinea.nomenclature.disponibiliteEauPCV;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class disponibiliteEauPVCForm {

	private int code;
	private String libelle;
	
}
