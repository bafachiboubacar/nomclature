package ne.mha.sinea.nomenclature.disponibiliteEauPCV;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class disponibiliteEauPVCController {

	@Autowired
	disponibiliteEauPVCRepository disponibiliteEauPVCRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/disponibiliteEauPVC")
	public String  addDisponibiliteEauPVC(disponibiliteEauPVCForm disponibiliteEauPVCForm, Model model) {
		try{
			List<disponibiliteEauPVC> disponibiliteEauPVC = disponibiliteEauPVCRepository.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("disponibiliteEauPVC", disponibiliteEauPVC);
			model.addAttribute("viewPath", "nomenclature/disponibiliteEauPVC/disponibiliteEauPVC");
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
}
