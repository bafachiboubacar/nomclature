package ne.mha.sinea.nomenclature.typeStationMeteo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class TypeStationMeteoRestController {

	@Autowired
	TypeStationMeteoRepository typeStationMeteoService;
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/typeStationMeteo")
	public int addTypeStationMeteo(@Validated TypeStationMeteoForm typeStationMeteoForm,BindingResult bindingResult, Model model) {
		TypeStationMeteo savedTypeStationMeteo = new TypeStationMeteo();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeStationMeteo P = new TypeStationMeteo();
					P.setLibelle(typeStationMeteoForm.getLibelle());
					savedTypeStationMeteo = typeStationMeteoService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeStationMeteo.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/updateTypeStationMeteo")
    public int updateTypeStationMeteo(@Validated TypeStationMeteoForm typeStationMeteoForm,BindingResult bindingResult, Model model) {
		TypeStationMeteo savedTypeStationMeteo = new TypeStationMeteo();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeStationMeteo P = typeStationMeteoService.findByCode(typeStationMeteoForm.getCode());
					P.setLibelle(typeStationMeteoForm.getLibelle());
					savedTypeStationMeteo = typeStationMeteoService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeStationMeteo.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/deleteTypeStationMeteo")
    public int deleteTypeStationMeteo(@Validated TypeStationMeteoForm typeStationMeteoForm,BindingResult bindingResult, Model model) {
		TypeStationMeteo savedTypeStationMeteo = new TypeStationMeteo();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeStationMeteo P = typeStationMeteoService.findByCode(typeStationMeteoForm.getCode());
					P.setDeleted(true);
					savedTypeStationMeteo = typeStationMeteoService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeStationMeteo.getCode();
		
        
    }
}
