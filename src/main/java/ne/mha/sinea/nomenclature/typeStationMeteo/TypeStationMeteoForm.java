package ne.mha.sinea.nomenclature.typeStationMeteo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TypeStationMeteoForm {

	private int code;
	private String libelle;
	
}
