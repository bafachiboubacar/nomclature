package ne.mha.sinea.nomenclature.typeLatrines;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class typeLatrinesController {

	@Autowired
	typeLatrinesRepository typeLatrinesRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/typeLatrines")
	public String  addTypeLatrines(typeLatrinesForm typeLatrinesForm, Model model) {
		try{
			List<typeLatrines> typeLatrines =  typeLatrinesRepository.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeLatrines", typeLatrines);
			model.addAttribute("viewPath", "nomenclature/typeLatrines/typeLatrines");
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
}
