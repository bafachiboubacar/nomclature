package ne.mha.sinea.nomenclature.typeLatrines;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface typeLatrinesRepository extends CrudRepository<typeLatrines, Integer> {
	typeLatrines findByCode(Integer code);
	typeLatrines findByIsDeletedFalseAndLibelle(String libelle);
	List<typeLatrines> findByIsDeletedFalseOrderByLibelleAsc();
	List<typeLatrines> findByIsDeletedFalse();
	List<typeLatrines> findByIsDeletedTrue();
	List<typeLatrines> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<typeLatrines> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
