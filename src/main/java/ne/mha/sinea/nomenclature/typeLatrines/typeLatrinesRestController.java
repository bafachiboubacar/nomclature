package ne.mha.sinea.nomenclature.typeLatrines;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class typeLatrinesRestController {

	@Autowired
	typeLatrinesRepository typeLatrinesRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/typeLatrines")
	public int addTypeLatrines(@Validated typeLatrinesForm typeLatrinesForm,BindingResult bindingResult, Model model) {
		typeLatrines savedTypeLatrines = new typeLatrines();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					typeLatrines P = new typeLatrines();
					P.setLibelle(typeLatrinesForm.getLibelle());
					savedTypeLatrines = typeLatrinesRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeLatrines.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateTypeLatrines")
    public int updateTypeLatrines(@Validated typeLatrinesForm typeLatrinesForm,BindingResult bindingResult, Model model) {
		typeLatrines savedTypeLatrines = new typeLatrines();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					typeLatrines P = typeLatrinesRepository.findByCode(typeLatrinesForm.getCode());
					P.setLibelle(typeLatrinesForm.getLibelle());
					savedTypeLatrines = typeLatrinesRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeLatrines.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteTypeLatrines")
    public int deleteTypeLatrines(@Validated typeLatrinesForm typeLatrinesForm,BindingResult bindingResult, Model model) {
		typeLatrines savedTypeLatrines = new typeLatrines();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					typeLatrines P = typeLatrinesRepository.findByCode(typeLatrinesForm.getCode());
					P.setDeleted(true);
					savedTypeLatrines = typeLatrinesRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeLatrines.getCode();
		
        
    }
}
