package ne.mha.sinea.nomenclature.marquePompe;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface MarquePompeRepository extends CrudRepository<MarquePompe, Integer> {
	MarquePompe findByCode(Integer code);
	MarquePompe findByIsDeletedFalseAndLibelle(String libelle);
	List<MarquePompe> findByIsDeletedFalseOrderByLibelleAsc();
	List<MarquePompe> findByIsDeletedFalse();
	List<MarquePompe> findByIsDeletedTrue();
	List<MarquePompe> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<MarquePompe> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
