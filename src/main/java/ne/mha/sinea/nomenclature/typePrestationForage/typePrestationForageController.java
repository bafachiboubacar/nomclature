package ne.mha.sinea.nomenclature.typePrestationForage;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class typePrestationForageController {

	@Autowired
	typePrestationForageRepository typePrestationForageRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/typePrestationForage")
	public String  addTypePrestationForage(typePrestationForageForm typePrestationForageForm, Model model) {
		try{
			List<typePrestationForage> typePrestationForage = typePrestationForageRepository.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typePrestationForage", typePrestationForage);
			model.addAttribute("viewPath", "nomenclature/typePrestationForage/typePrestationForage");
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
}
