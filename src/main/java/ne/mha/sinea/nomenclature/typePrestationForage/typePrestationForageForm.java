package ne.mha.sinea.nomenclature.typePrestationForage;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class typePrestationForageForm {

	private int code;
	private String libelle;
	
}
