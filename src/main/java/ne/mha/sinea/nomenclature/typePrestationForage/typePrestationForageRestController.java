package ne.mha.sinea.nomenclature.typePrestationForage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class typePrestationForageRestController {

	@Autowired
	typePrestationForageRepository typePrestationForageRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/typePrestationForage")
	public int addTypePrestationForage(@Validated typePrestationForageForm typePrestationForageForm,BindingResult bindingResult, Model model) {
		typePrestationForage savedTypePrestationForage = new typePrestationForage();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					typePrestationForage P = new typePrestationForage();
					P.setLibelle(typePrestationForageForm.getLibelle());
					savedTypePrestationForage = typePrestationForageRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypePrestationForage.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateTypePrestationForage")
    public int updateTypePrestationForage(@Validated typePrestationForageForm typePrestationForageForm,BindingResult bindingResult, Model model) {
		typePrestationForage savedTypePrestationForage = new typePrestationForage();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					typePrestationForage P = typePrestationForageRepository.findByCode(typePrestationForageForm.getCode());
					P.setLibelle(typePrestationForageForm.getLibelle());
					savedTypePrestationForage = typePrestationForageRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypePrestationForage.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteTypePrestationForage")
    public int deleteTypePrestationForage(@Validated typePrestationForageForm typePrestationForageForm,BindingResult bindingResult, Model model) {
		typePrestationForage savedTypePrestationForage = new typePrestationForage();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					typePrestationForage P = typePrestationForageRepository.findByCode(typePrestationForageForm.getCode());
					P.setDeleted(true);
					savedTypePrestationForage = typePrestationForageRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypePrestationForage.getCode();
		
        
    }
}
