package ne.mha.sinea.nomenclature.marqueOnduleur;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class marqueOnduleurForm {

	private int code;
	private String libelle;
	
}
