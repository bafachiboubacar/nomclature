package ne.mha.sinea.nomenclature.marqueOnduleur;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class marqueOnduleurController {

	@Autowired
	marqueOnduleurRepository marqueOnduleurRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/marqueOnduleur")
	public String  addMarqueOnduleur(marqueOnduleurForm marqueOnduleurForm, Model model) {
		try{
			List<marqueOnduleur> marqueOnduleur = marqueOnduleurRepository.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("marqueOnduleur", marqueOnduleur);
			model.addAttribute("viewPath", "nomenclature/marqueOnduleur/marqueOnduleur");
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
}
