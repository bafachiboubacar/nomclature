package ne.mha.sinea.nomenclature.modelePompe;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;
@Controller
public class ModelePompeController {

	@Autowired
	ModelePompeRepository modelePompeService;
	
	//@PreAuthorize("hasAuthority('gestion des types ressources')")
	@GetMapping("/modelePompe")
	public String  addModelePompe(ModelePompeForm modelePompeForm, Model model) {
		try{
			List<ModelePompe> modelePompes = modelePompeService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("modelePompes", modelePompes);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Nomenclature/Type Ressource");
			model.addAttribute("viewPath", "nomenclature/modelePompe/modelePompe");
			
			}
		catch(Exception e){
				
			}
		
		return Template.defaultTemplate;

	}
	
	
}
