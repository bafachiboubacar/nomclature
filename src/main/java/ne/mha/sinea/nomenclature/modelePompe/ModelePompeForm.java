package ne.mha.sinea.nomenclature.modelePompe;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ModelePompeForm {

	private int code;
	private String libelle;
	
}
