package ne.mha.sinea.nomenclature.typeUsage;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;
@Controller
public class TypeUsageController {

	@Autowired
	TypeUsageRepository typeUsageService;
	
	//@PreAuthorize("hasAuthority('gestion des Type Usage')")
	@GetMapping("/typeUsage")
	public String  addTypeUsage(TypeUsageForm typeUsageForm, Model model) {
		try{
			List<TypeUsage> typeUsages = typeUsageService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeUsages", typeUsages);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Nomenclature/Type Usage");
			model.addAttribute("viewPath", "nomenclature/typeUsage/typeUsage");
			
			}
		catch(Exception e){
				
			}
		
		return Template.defaultTemplate;

	}
	
	
}
