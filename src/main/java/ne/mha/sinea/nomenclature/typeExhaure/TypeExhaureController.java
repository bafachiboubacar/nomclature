package ne.mha.sinea.nomenclature.typeExhaure;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;
@Controller
public class TypeExhaureController {

	@Autowired
	TypeExhaureRepository typeExhaureService;
	
	//@PreAuthorize("hasAuthority('gestion des Type Exhaure')")
	@GetMapping("/typeExhaure")
	public String  addTypeExhaure(TypeExhaureForm typeExhaureForm, Model model) {
		try{
			List<TypeExhaure> typeExhaures = typeExhaureService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeExhaures", typeExhaures);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Nomenclature/Type Exhaure");
			model.addAttribute("viewPath", "nomenclature/typeExhaure/typeExhaure");
			
			}
		catch(Exception e){
				
			}
		
		return Template.defaultTemplate;

	}
	
	
}
