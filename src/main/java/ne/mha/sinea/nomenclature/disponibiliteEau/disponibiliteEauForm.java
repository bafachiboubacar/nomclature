package ne.mha.sinea.nomenclature.disponibiliteEau;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class disponibiliteEauForm {

	private int code;
	private String libelle;
	
}
