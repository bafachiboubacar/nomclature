package ne.mha.sinea.nomenclature.disponibiliteEau;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class disponibiliteEauController {

	@Autowired
	disponibiliteEauRepository disponibiliteEauRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/disponibiliteEau")
	public String  addDisponibiliteEau(disponibiliteEauForm disponibiliteEauForm, Model model) {
		try{
			List<disponibiliteEau> disponibiliteEau = disponibiliteEauRepository.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("disponibiliteEau", disponibiliteEau);
			model.addAttribute("viewPath", "nomenclature/disponibiliteEau/disponibiliteEau");
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
}
