package ne.mha.sinea.nomenclature.disponibiliteEau;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class disponibiliteEauRestController {

	@Autowired
	disponibiliteEauRepository disponibiliteEauRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/disponibiliteEau")
	public int addDisponibiliteEau(@Validated disponibiliteEauForm disponibiliteEauForm,BindingResult bindingResult, Model model) {
		disponibiliteEau savedDisponibiliteEau = new disponibiliteEau();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					disponibiliteEau P = new disponibiliteEau();
					P.setLibelle(disponibiliteEauForm.getLibelle());
					savedDisponibiliteEau = disponibiliteEauRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedDisponibiliteEau.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateDisponibiliteEau")
    public int updateDisponibiliteEau(@Validated disponibiliteEauForm disponibiliteEauForm,BindingResult bindingResult, Model model) {
		disponibiliteEau savedDisponibiliteEau = new disponibiliteEau();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					disponibiliteEau P = disponibiliteEauRepository.findByCode(disponibiliteEauForm.getCode());
					P.setLibelle(disponibiliteEauForm.getLibelle());
					savedDisponibiliteEau = disponibiliteEauRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedDisponibiliteEau.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteDisponibiliteEau")
    public int deleteDisponibiliteEau(@Validated disponibiliteEauForm disponibiliteEauForm,BindingResult bindingResult, Model model) {
		disponibiliteEau savedDisponibiliteEau = new disponibiliteEau();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					disponibiliteEau P = disponibiliteEauRepository.findByCode(disponibiliteEauForm.getCode());
					P.setDeleted(true);
					savedDisponibiliteEau = disponibiliteEauRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedDisponibiliteEau.getCode();
		
        
    }
}
