package ne.mha.sinea.nomenclature.disponibiliteEau;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface disponibiliteEauRepository extends CrudRepository<disponibiliteEau, Integer> {
	disponibiliteEau findByCode(Integer code);
	disponibiliteEau findByIsDeletedFalseAndLibelle(String libelle);
	List<disponibiliteEau> findByIsDeletedFalseOrderByLibelleAsc();
	List<disponibiliteEau> findByIsDeletedFalse();
	List<disponibiliteEau> findByIsDeletedTrue();
	List<disponibiliteEau> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<disponibiliteEau> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
