package ne.mha.sinea.nomenclature.typeRessourceStationHydrometrique;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;
@Controller
public class TypeRessourceStationHydrometriqueController {

	@Autowired
	TypeRessourceStationHydrometriqueRepository typeRessourceStationHydrometriqueService;
	
	//@PreAuthorize("hasAuthority('gestion des Type Ressource Station Hydrometrique')")
	@GetMapping("/typeRessourceStationHydrometrique")
	public String  addTypeRessourceStationHydrometrique(TypeRessourceStationHydrometriqueForm typeRessourceStationHydrometriqueForm, Model model) {
		try{
			List<TypeRessourceStationHydrometrique> typeRessourceStationHydrometriques = typeRessourceStationHydrometriqueService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeRessourceStationHydrometriques", typeRessourceStationHydrometriques);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Nomenclature/Type Ressource Station Hydrometrique");
			model.addAttribute("viewPath", "nomenclature/typeRessourceStationHydrometrique/typeRessourceStationHydrometrique");
			
			}
		catch(Exception e){
				
			}
		
		return Template.defaultTemplate;

	}
	
	
}
