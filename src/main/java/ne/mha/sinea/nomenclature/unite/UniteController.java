package ne.mha.sinea.nomenclature.unite;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class UniteController {

	@Autowired
	UniteRepository uniteService;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/unite")
	public String  addUnite(UniteForm uniteForm, Model model) {
		try{
			List<Unite> unites = uniteService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("unites", unites);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Nomenclature/Unité");
			model.addAttribute("viewPath", "nomenclature/unite/unite");
			
			}
		catch(Exception e){
				
			}
		
		return Template.defaultTemplate;

	}
	
	
}
