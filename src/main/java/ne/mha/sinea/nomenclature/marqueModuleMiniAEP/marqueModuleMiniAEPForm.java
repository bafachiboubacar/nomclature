package ne.mha.sinea.nomenclature.marqueModuleMiniAEP;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class marqueModuleMiniAEPForm {

	private int code;
	private String libelle;
	
}
