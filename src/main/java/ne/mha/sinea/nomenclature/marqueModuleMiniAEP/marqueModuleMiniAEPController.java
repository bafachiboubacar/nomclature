package ne.mha.sinea.nomenclature.marqueModuleMiniAEP;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class marqueModuleMiniAEPController {

	@Autowired
	marqueModuleMiniAEPRepository marqueModuleMiniAEPRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/marqueModuleMiniAEP")
	public String  addMarqueModuleMiniAEP(marqueModuleMiniAEPForm marqueModuleMiniAEPForm, Model model) {
		try{
			List<marqueModuleMiniAEP> marqueModuleMiniAEP = marqueModuleMiniAEPRepository.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("marqueModuleMiniAEP", marqueModuleMiniAEP);
			model.addAttribute("viewPath", "nomenclature/marqueModuleMiniAEP/marqueModuleMiniAEP");
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
}
