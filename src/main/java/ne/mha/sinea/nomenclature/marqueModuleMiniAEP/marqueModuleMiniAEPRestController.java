package ne.mha.sinea.nomenclature.marqueModuleMiniAEP;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class marqueModuleMiniAEPRestController {

	@Autowired
	marqueModuleMiniAEPRepository marqueModuleMiniAEPRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/marqueModuleMiniAEP")
	public int addMarqueModuleMiniAEP(@Validated marqueModuleMiniAEPForm marqueModuleMiniAEPForm,BindingResult bindingResult, Model model) {
		marqueModuleMiniAEP savedMarqueModuleMiniAEP = new marqueModuleMiniAEP();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					marqueModuleMiniAEP P = new marqueModuleMiniAEP();
					P.setLibelle(marqueModuleMiniAEPForm.getLibelle());
					savedMarqueModuleMiniAEP = marqueModuleMiniAEPRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedMarqueModuleMiniAEP.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateMarqueModuleMiniAEPu")
    public int updateMarqueModuleMiniAEP(@Validated marqueModuleMiniAEPForm marqueModuleMiniAEPForm,BindingResult bindingResult, Model model) {
		marqueModuleMiniAEP savedMarqueModuleMiniAEP = new marqueModuleMiniAEP();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					marqueModuleMiniAEP P = marqueModuleMiniAEPRepository.findByCode(marqueModuleMiniAEPForm.getCode());
					P.setLibelle(marqueModuleMiniAEPForm.getLibelle());
					savedMarqueModuleMiniAEP = marqueModuleMiniAEPRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedMarqueModuleMiniAEP.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteMarqueModuleMiniAEP")
    public int deleteMarqueModuleMiniAEP(@Validated marqueModuleMiniAEPForm marqueModuleMiniAEPForm,BindingResult bindingResult, Model model) {
		marqueModuleMiniAEP savedMarqueModuleMiniAEP = new marqueModuleMiniAEP();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					marqueModuleMiniAEP P = marqueModuleMiniAEPRepository.findByCode(marqueModuleMiniAEPForm.getCode());
					P.setDeleted(true);
					savedMarqueModuleMiniAEP = marqueModuleMiniAEPRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedMarqueModuleMiniAEP.getCode();
		
        
    }
}
