package ne.mha.sinea.nomenclature.lieu;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class LieuRestController {

	@Autowired
	LieuRepository lieuService;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/lieu")
	public int addLieu(@Validated LieuForm lieuForm,BindingResult bindingResult, Model model) {
		Lieu savedLieu = new Lieu();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					Lieu P = new Lieu();
					P.setLibelle(lieuForm.getLibelle());
					savedLieu = lieuService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedLieu.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateLieu")
    public int updateLieu(@Validated LieuForm lieuForm,BindingResult bindingResult, Model model) {
		Lieu savedLieu = new Lieu();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					Lieu P = lieuService.findByCode(lieuForm.getCode());
					P.setLibelle(lieuForm.getLibelle());
					savedLieu = lieuService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedLieu.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteLieu")
    public int deleteLieu(@Validated LieuForm lieuForm,BindingResult bindingResult, Model model) {
		Lieu savedLieu = new Lieu();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					Lieu P = lieuService.findByCode(lieuForm.getCode());
					P.setDeleted(true);
					savedLieu = lieuService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedLieu.getCode();
		
        
    }
}
