package ne.mha.sinea.nomenclature.lieu;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class LieuController {

	@Autowired
	LieuRepository lieuService;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/lieu")
	public String  addLieu(LieuForm lieuForm, Model model) {
		try{
			List<Lieu> lieux = lieuService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("lieux", lieux);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Nomenclature/Lieu");
			model.addAttribute("viewPath", "nomenclature/lieu/lieu");
			
			}
		catch(Exception e){
				
			}
		
		return Template.defaultTemplate;

	}
	
	
}
