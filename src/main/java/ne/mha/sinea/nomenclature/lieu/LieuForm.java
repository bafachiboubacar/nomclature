package ne.mha.sinea.nomenclature.lieu;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LieuForm {

	private int code;
	private String libelle;
	
}
