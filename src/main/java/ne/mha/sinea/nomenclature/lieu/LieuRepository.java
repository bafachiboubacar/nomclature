package ne.mha.sinea.nomenclature.lieu;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface LieuRepository extends CrudRepository<Lieu, Integer> {
	Lieu findByCode(Integer code);
	Lieu findByIsDeletedFalseAndLibelle(String libelle);
	List<Lieu> findByIsDeletedFalseOrderByLibelleAsc();
	List<Lieu> findByIsDeletedFalse();
	List<Lieu> findByIsDeletedTrue();
	List<Lieu> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<Lieu> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
