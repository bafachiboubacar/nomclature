package ne.mha.sinea.nomenclature.typeFormationSanitaire;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;
@Controller
public class TypeFormationSanitaireController {

	@Autowired
	TypeFormationSanitaireRepository typeFormationSanitaireService;
	
	//@PreAuthorize("hasAuthority('gestion des Type Formation Sanitaire')")
	@GetMapping("/typeFormationSanitaire")
	public String  addTypeFormationSanitaire(TypeFormationSanitaireForm typeFormationSanitaireForm, Model model) {
		try{
			List<TypeFormationSanitaire> typeFormationSanitaires = typeFormationSanitaireService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeFormationSanitaires", typeFormationSanitaires);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Nomenclature/Type Formation Sanitaire");
			model.addAttribute("viewPath", "nomenclature/typeFormationSanitaire/typeFormationSanitaire");
			
			}
		catch(Exception e){
				
			}
		
		return Template.defaultTemplate;

	}
	
	
}
