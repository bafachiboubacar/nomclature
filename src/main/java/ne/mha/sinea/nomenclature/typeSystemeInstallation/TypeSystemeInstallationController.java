package ne.mha.sinea.nomenclature.typeSystemeInstallation;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;
@Controller
public class TypeSystemeInstallationController {

	@Autowired
	TypeSystemeInstallationRepository typeSystemeInstallationService;
	
	//@PreAuthorize("hasAuthority('gestion des Type Systeme Installation')")
	@GetMapping("/typeSystemeInstallation")
	public String  addTypeSystemeInstallation(TypeSystemeInstallationForm typeSystemeInstallationForm, Model model) {
		try{
			List<TypeSystemeInstallation> typeSystemeInstallations = typeSystemeInstallationService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeSystemeInstallations", typeSystemeInstallations);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Nomenclature/Type Systeme Installation");
			model.addAttribute("viewPath", "nomenclature/typeSystemeInstallation/typeSystemeInstallation");
			
			}
		catch(Exception e){
				
			}
		
		return Template.defaultTemplate;

	}
	
	
}
