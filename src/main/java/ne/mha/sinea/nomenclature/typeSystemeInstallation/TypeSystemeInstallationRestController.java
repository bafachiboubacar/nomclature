package ne.mha.sinea.nomenclature.typeSystemeInstallation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class TypeSystemeInstallationRestController {

	@Autowired
	TypeSystemeInstallationRepository typeSystemeInstallationService;
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/typeSystemeInstallation")
	public int addTypeSystemeInstallation(@Validated TypeSystemeInstallationForm typeSystemeInstallationForm,BindingResult bindingResult, Model model) {
		TypeSystemeInstallation savedTypeSystemeInstallation = new TypeSystemeInstallation();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeSystemeInstallation P = new TypeSystemeInstallation();
					P.setLibelle(typeSystemeInstallationForm.getLibelle());
					savedTypeSystemeInstallation = typeSystemeInstallationService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeSystemeInstallation.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/updateTypeSystemeInstallation")
    public int updateTypeSystemeInstallation(@Validated TypeSystemeInstallationForm typeSystemeInstallationForm,BindingResult bindingResult, Model model) {
		TypeSystemeInstallation savedTypeSystemeInstallation = new TypeSystemeInstallation();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeSystemeInstallation P = typeSystemeInstallationService.findByCode(typeSystemeInstallationForm.getCode());
					P.setLibelle(typeSystemeInstallationForm.getLibelle());
					savedTypeSystemeInstallation = typeSystemeInstallationService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeSystemeInstallation.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/deleteTypeSystemeInstallation")
    public int deleteTypeSystemeInstallation(@Validated TypeSystemeInstallationForm typeSystemeInstallationForm,BindingResult bindingResult, Model model) {
		TypeSystemeInstallation savedTypeSystemeInstallation = new TypeSystemeInstallation();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeSystemeInstallation P = typeSystemeInstallationService.findByCode(typeSystemeInstallationForm.getCode());
					P.setDeleted(true);
					savedTypeSystemeInstallation = typeSystemeInstallationService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeSystemeInstallation.getCode();
		
        
    }
}
