package ne.mha.sinea.nomenclature.typeSystemeInstallation;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface TypeSystemeInstallationRepository extends CrudRepository<TypeSystemeInstallation, Integer> {
	TypeSystemeInstallation findByCode(Integer code);
	TypeSystemeInstallation findByIsDeletedFalseAndLibelle(String libelle);
	List<TypeSystemeInstallation> findByIsDeletedFalseOrderByLibelleAsc();
	List<TypeSystemeInstallation> findByIsDeletedFalse();
	List<TypeSystemeInstallation> findByIsDeletedTrue();
	List<TypeSystemeInstallation> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<TypeSystemeInstallation> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
