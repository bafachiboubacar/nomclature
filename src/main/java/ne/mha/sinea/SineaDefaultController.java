package ne.mha.sinea;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

//controller par defaut
@Controller
public class SineaDefaultController {	

	
	@GetMapping("/login")
	public String Connexion( Model model){
		model.addAttribute("viewPath", "connexion");
		return "connexion";
	}
	
	@GetMapping("/")
	public String dashboard( Model model){
		model.addAttribute("viewPath", "dashboard");
		//model.addAttribute("horizontalMenu", "horizontalMenu");
		//model.addAttribute("sidebarMenu", "configurationSidebarMenu");
		//return Template.dashboardTemplate;
		return "dashboard2";
	}
	
	@GetMapping("/publique/PointEau")
	public String PointEau( Model model){
		model.addAttribute("viewPath", "publique");
		//model.addAttribute("horizontalMenu", "horizontalMenu");
		//model.addAttribute("sidebarMenu", "configurationSidebarMenu");
		//return Template.dashboardTemplate;
		return "publique/pointEau";
	}
	

}