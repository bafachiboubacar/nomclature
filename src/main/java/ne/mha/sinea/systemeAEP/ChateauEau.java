package ne.mha.sinea.systemeAEP;

//import java.sql.Date;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import ne.mha.sinea.CommonProperties;
import ne.mha.sinea.nomenclature.disposition.Disposition;
import ne.mha.sinea.nomenclature.hydraulique.Hydraulique;
import ne.mha.sinea.nomenclature.materiaux.Materiaux;
import ne.mha.sinea.nomenclature.tuyauterie.Tuyauterie;

@Data
@Entity
@Table(name = "chateau_eau")
public class ChateauEau extends CommonProperties {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "code")
	protected Integer code;
	@Column(name = "numero_chateau")
	protected Integer numeroChateau;
	@Column(name = "code_ins")
	protected String codeINS;
	@Column(name = "capacite")
	protected Double capacite;
	@JoinColumn(name = "code_disposition", referencedColumnName = "code")
	@ManyToOne
	protected Disposition disposition;
	@Column(name = "hauteur")
	protected Double hauteur;
	@JoinColumn(name = "code_materiaux", referencedColumnName = "code")
	@ManyToOne
	protected Materiaux materiaux;
	@Column(name = "existence_compteur")
	private boolean existenceCompteur;
	@Column(name = "index_compteur")
	protected Double indexCompteur;
	@JoinColumn(name = "code_hydraulique", referencedColumnName = "code")
	@ManyToOne
	protected Hydraulique hydraulique;
	@JoinColumn(name = "code_tuyauterie", referencedColumnName = "code")
	@ManyToOne
	protected Tuyauterie tuyauterie;
	@JoinColumn(name = "code_systeme_aep", referencedColumnName = "code")
	@ManyToOne
	protected SystemeAEP systemeAEP;
	@Column(name = "date_demarrage_travaux")
	private Date dateDemarrageTravaux;
	@Column(name = "date_fin_travaux")
	private Date dateFinTravaux;
	
	
	
}
