package ne.mha.sinea.systemeAEP;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import ne.mha.sinea.Template;
import ne.mha.sinea.nomenclature.financement.Financement;
import ne.mha.sinea.nomenclature.financement.FinancementRepository;
import ne.mha.sinea.nomenclature.marquePompe.MarquePompe;
import ne.mha.sinea.nomenclature.marquePompe.MarquePompeRepository;
import ne.mha.sinea.nomenclature.modelePompe.ModelePompe;
import ne.mha.sinea.nomenclature.modelePompe.ModelePompeRepository;
import ne.mha.sinea.nomenclature.propriete.Propriete;
import ne.mha.sinea.nomenclature.propriete.ProprieteRepository;
import ne.mha.sinea.nomenclature.typeAmenagement.TypeAmenagement;
import ne.mha.sinea.nomenclature.typeAmenagement.TypeAmenagementRepository;
import ne.mha.sinea.nomenclature.typeUsage.TypeUsage;
import ne.mha.sinea.nomenclature.typeUsage.TypeUsageRepository;
import ne.mha.sinea.pem.ForageExcelService;
import ne.mha.sinea.referentiel.projet.Projet;
import ne.mha.sinea.referentiel.projet.ProjetRepository;
import ne.mha.sinea.referentiel.zone.Localite;
import ne.mha.sinea.referentiel.zone.LocaliteRepository;

@Controller
public class SystemeAEPController {
	@Autowired
	SystemeAEPExcelService systemeAEPExcelService;
	@Autowired
	SystemeAEPRepository systemeAEPService;
	@Autowired
	ProjetRepository projetService;
	@Autowired
	FinancementRepository financementService;
	@Autowired
	LocaliteRepository localiteService;
	@Autowired
	TypeUsageRepository typeUsageService;
	@Autowired
	ProprieteRepository proprieteService;
	@Autowired
	TypeAmenagementRepository typeAmenagementService;
	@Autowired
	ModelePompeRepository modelePompeService;
	@Autowired
	MarquePompeRepository marquePompeService;
	
	//@PreAuthorize("hasAuthority('Ajout ModelePompe'))
		@GetMapping("/aep/{code}")
		public String  addForage(@PathVariable("code") int code, Model model) {
			try{
				//récuperation de la liste des donnees de la base et envoie à la vue
				SystemeAEP aep = systemeAEPService.findByCode(code);
				model.addAttribute("aep", aep);
				List<Projet> projets =  (List<Projet>) projetService.findByIsDeletedFalse();
				model.addAttribute("projets", projets);
				List<Financement> financements =  (List<Financement>) financementService.findByIsDeletedFalse();
				model.addAttribute("financements", financements);
				List<ModelePompe> modelePompes =  (List<ModelePompe>) modelePompeService.findByIsDeletedFalse();
				model.addAttribute("modelePompes", modelePompes);
				List<MarquePompe> marquePompes =  (List<MarquePompe>) marquePompeService.findByIsDeletedFalse();
				model.addAttribute("modelePompes", modelePompes);
				List<TypeUsage> typeUsages =  (List<TypeUsage>) typeUsageService.findByIsDeletedFalse();
				model.addAttribute("typeUsages", typeUsages);
				List<Propriete> proprietes =  (List<Propriete>) proprieteService.findByIsDeletedFalse();
				model.addAttribute("proprietes", proprietes);
				List<TypeAmenagement> typeAmenagements =  (List<TypeAmenagement>) typeAmenagementService.findByIsDeletedFalse();
				model.addAttribute("typeAmenagements", typeAmenagements);
				List<Localite> localites =  (List<Localite>) localiteService.findByIsDeletedFalse();
				model.addAttribute("localites", localites);
				
				model.addAttribute("horizontalMenu", "horizontalMenu");
				model.addAttribute("sidebarMenu", "configurationSidebarMenu");
				model.addAttribute("breadcrumb", "breadcrumb");
				model.addAttribute("navigationPath", "Système AEP");
				model.addAttribute("viewPath", "systemeAEP/systemeAEP");
				
			}
			catch(Exception e){
				System.out.println("erreur "+e);	
				}
			return Template.defaultTemplate;

		}
		
		//@PreAuthorize("hasAuthority('gestion des forages')")
				@GetMapping("/importAEP")
				public String  uploadIndicateur(Model model) {
					
					return "systemeAEP/importAEP";

				}
				//@PreAuthorize("hasAuthority('gestion des forages')")
				@PostMapping("/importAEP")
				public RedirectView uploadIndicateurSubmit(@RequestParam("file") MultipartFile file,RedirectAttributes redirectAttributes) {
					String TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
					final RedirectView redirectView = new RedirectView("/initPEM", true);
					//si le fichier a la bonne extension
					if (TYPE.equals(file.getContentType())) {
						try {
							List<String> unsavedForages = systemeAEPExcelService.importData(file);
							redirectAttributes.addFlashAttribute("unsavedForages", unsavedForages);
					        
					      	} 
						catch (Exception e) {
							System.out.println(e);
							//indiquer que l'operation d'ajout a échoué
							redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/unsuccess");
						
							}
					   }
					else {
						//indiquer que l'operation d'ajout a échoué
						redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/unsuccess");
					}

					//redirection vers la page d'ajout
					return redirectView;
				    
				  } 
		
		
}
