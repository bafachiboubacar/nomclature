package ne.mha.sinea.systemeAEP;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
//import java.sql.Date;
import java.util.Date;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import ne.mha.sinea.nomenclature.disposition.DispositionRepository;
import ne.mha.sinea.nomenclature.etatOuvrage.EtatOuvrageRepository;
import ne.mha.sinea.nomenclature.financement.FinancementRepository;
import ne.mha.sinea.nomenclature.hydraulique.HydrauliqueRepository;
import ne.mha.sinea.nomenclature.lieu.LieuRepository;
import ne.mha.sinea.nomenclature.marquePompe.MarquePompeRepository;
import ne.mha.sinea.nomenclature.materiaux.MateriauxRepository;
import ne.mha.sinea.nomenclature.modelePompe.ModelePompeRepository;
import ne.mha.sinea.nomenclature.propriete.ProprieteRepository;
import ne.mha.sinea.nomenclature.tuyauterie.TuyauterieRepository;
import ne.mha.sinea.nomenclature.typePieceRegulationReseau.TypePieceRegulationReseauRepository;
import ne.mha.sinea.nomenclature.typeSystemeAEP.TypeSystemeAEPRepository;
import ne.mha.sinea.nomenclature.typeSystemeInstallation.TypeSystemeInstallationRepository;
import ne.mha.sinea.nomenclature.typeSystemeProtection.TypeSystemeProtectionRepository;
import ne.mha.sinea.pem.ForageRepository;
import ne.mha.sinea.referentiel.projet.ProjetRepository;
import ne.mha.sinea.referentiel.zone.LocaliteRepository;

@Service
public class SystemeAEPExcelService {
	@Autowired
	SystemeAEPRepository systemeAEPService;
	@Autowired
	TypeSystemeAEPRepository typeSystemeAEPService;
	@Autowired
	ProprieteRepository proprieteService;
	@Autowired
	LocaliteRepository localiteService;
	@Autowired
	EtatOuvrageRepository etatOuvrageService;
	@Autowired
	FinancementRepository financementService;
	@Autowired
	ProjetRepository projetService;
	@Autowired
	BorneFontaineRepository borneFontaineService;
	@Autowired
	AbreuvoirRepository abreuvoirService;
	@Autowired
	BranchementParticulierRepository branchementParticulierService;
	@Autowired
	ChateauEauRepository chateauEauService;
	@Autowired
	CaptageSystemeAEPRepository captageSystemeAEPService;
	@Autowired
	ReseauDistributionRepository reseauDistributionService;
	@Autowired
	ReseauRefoulementRepository reseauRefoulementService;
	@Autowired
	LieuRepository lieuService;
	@Autowired
	DispositionRepository dispositionService;
	@Autowired
	MateriauxRepository materiauxService;
	@Autowired
	HydrauliqueRepository hydrauliqueService;
	@Autowired
	TuyauterieRepository tuyauterieService;
	@Autowired
	ForageRepository forageService;
	@Autowired
	TypeSystemeProtectionRepository typeSystemeProtectionService;
	@Autowired
	TypeSystemeInstallationRepository typeSystemeInstallationService;
	@Autowired
	MarquePompeRepository marquePompeService;
	@Autowired
	ModelePompeRepository modelePompeService;
	@Autowired
	TypePieceRegulationReseauRepository typePieceRegulationReseauService;
	@Autowired
	PieceRegulationReseauRepository pieceRegulationReseauService;
	
	
	//définition des libellés de l'entête
	static String[] headerSystemeAEP = {"", ""};
	
	//placer le contenu du fichier excel dans un objet list
	public  List<SystemeAEP> excelToModeleSystemeAEP(InputStream is) {
		List<SystemeAEP> listeSystemeAEPs = new ArrayList<SystemeAEP>();
		try {
	    
		  //instancier un classeur Excel
	      Workbook workbook = new XSSFWorkbook(is);
	      //accéder à la première feuille
	      Sheet sheet = workbook.getSheetAt(0);
	      //partcourir les lignes de la feuille
	      Iterator<Row> rows = sheet.iterator();
	      int rowNumber = 0;
	      while (rows.hasNext()) {
	        Row currentRow = rows.next();
	        // ignorer l'entête du fichier Excel lors de l'import
	        if (rowNumber == 0) {
	          rowNumber++;
	          continue;
	        }
	        //verifier que la ligne à importer  n'est pas vide
	        if(!isRowEmpty(currentRow)) {
		        Iterator<Cell> cellsInRow = currentRow.iterator();
		        SystemeAEP saep = new SystemeAEP();
		        int cellIdx = 0;
		        //parcourir les colonnes d'une ligne
		        while (cellsInRow.hasNext()) {
		          Cell currentCell = cellsInRow.next();
		          switch (cellIdx) {
		          //première colonne
		          case 1: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  saep.setNumeroIRH(currentCell.getStringCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 2:
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  saep.setPropriete(proprieteService.findByIsDeletedFalseAndLibelle(currentCell.getStringCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 3:
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  saep.setTypeSystemeAEP(typeSystemeAEPService.findByIsDeletedFalseAndLibelle(currentCell.getStringCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 4:
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  saep.setLocalite(localiteService.findByCode((int)currentCell.getNumericCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 5: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  saep.setNomAEP(currentCell.getStringCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 6: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  saep.setAnneeRealisation((Date)currentCell.getDateCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 7:
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  saep.setFinancement(financementService.findByIsDeletedFalseAndLibelle(currentCell.getStringCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 8:
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  saep.setProjet(projetService.findByIsDeletedFalseAndLibelle(currentCell.getStringCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);} 
		          case 9: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        	  {
		        		  if(currentCell.getStringCellValue() == "1")
		        			  saep.setSourceEnergieSolaire(true);
		        		  else
		        			  saep.setSourceEnergieSolaire(false);
		        	  }
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 10: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        	  {
		        		  if(currentCell.getStringCellValue() == "1")
		        			  saep.setSourceEnergieThermique(true);
		        		  else
		        			  saep.setSourceEnergieThermique(false);
		        	  }
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 11: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        	  {
		        		  if(currentCell.getStringCellValue() == "1")
		        			  saep.setSourceEnergieReseauElectrique(true);
		        		  else
		        			  saep.setSourceEnergieReseauElectrique(false);
		        	  }
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 12: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        	  {
		        		  if(currentCell.getStringCellValue() == "1")
		        			  saep.setSourceEnergieEolienne(true);
		        		  else
		        			  saep.setSourceEnergieEolienne(false);
		        	  }
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 13: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        	  {
		        		  if(currentCell.getStringCellValue() == "1")
		        			  saep.setSourceEnergieAutres(true);
		        		  else
		        			  saep.setSourceEnergieAutres(false);
		        	  }
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 14: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  saep.setPrecisionEnergieAutres(currentCell.getStringCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 15: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        	  {
		        		  if(currentCell.getStringCellValue() == "1")
		        			  saep.setSourcePrincipaleSolaire(true);
		        		  else
		        			  saep.setSourcePrincipaleSolaire(false);
		        	  }
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 16: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        	  {
		        		  if(currentCell.getStringCellValue() == "1")
		        			  saep.setSourcePrincipaleThermique(true);
		        		  else
		        			  saep.setSourcePrincipaleThermique(false);
		        	  }
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 17: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        	  {
		        		  if(currentCell.getStringCellValue() == "1")
		        			  saep.setSourcePrincipaleReseauElectrique(true);
		        		  else
		        			  saep.setSourcePrincipaleReseauElectrique(false);
		        	  }
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 18: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        	  {
		        		  if(currentCell.getStringCellValue() == "1")
		        			  saep.setSourcePrincipaleEolienne(true);
		        		  else
		        			  saep.setSourcePrincipaleEolienne(false);
		        	  }
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 19: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        	  {
		        		  if(currentCell.getStringCellValue() == "1")
		        			  saep.setSourcePrincipaleAutres(true);
		        		  else
		        			  saep.setSourcePrincipaleAutres(false);
		        	  }
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 20: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  saep.setPrecisionSourcePrincipaleAutres(currentCell.getStringCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		        	  
		          }  
		          cellIdx++;
		        }
		        //ajouter la ligne récuperer à la liste des région (il s'agit pas encore de la sauvegarde dans la base
		        listeSystemeAEPs.add(saep);
	        }
	      } 

	      workbook.close();
	      
	    } catch (IOException e) {
	    	System.out.println(e);
	    }
		return listeSystemeAEPs;
	  }
	public  List<BorneFontaine> excelToModeleBorneFontaine(InputStream is) {
		List<BorneFontaine> listeBorneFontaines = new ArrayList<BorneFontaine>();
		try {
	    
		  //instancier un classeur Excel
	      Workbook workbook = new XSSFWorkbook(is);
	      //accéder à la première feuille
	      Sheet sheet = workbook.getSheetAt(1);
	      //partcourir les lignes de la feuille
	      Iterator<Row> rows = sheet.iterator();
	      int rowNumber = 0;
	      while (rows.hasNext()) {
	        Row currentRow = rows.next();
	        // ignorer l'entête du fichier Excel lors de l'import
	        if (rowNumber == 0) {
	          rowNumber++;
	          continue;
	        }
	        //verifier que la ligne à importer  n'est pas vide
	        if(!isRowEmpty(currentRow)) {
		        Iterator<Cell> cellsInRow = currentRow.iterator();
		        BorneFontaine bf = new BorneFontaine();
		        int cellIdx = 0;
		        //parcourir les colonnes d'une ligne
		        while (cellsInRow.hasNext()) {
		          Cell currentCell = cellsInRow.next();
		          switch (cellIdx) {
		          //première colonne
		          case 0: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  bf.setSystemeAEP(systemeAEPService.findByNumeroIRH(currentCell.getStringCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 1: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  bf.setNumeroBF((int)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 2: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  bf.setCodeINS(currentCell.getStringCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 3: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  bf.setNbRobinets((int)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 4: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  bf.setNbRobinetsFonctionnels((int)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 5: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        	  {
		        		  if(currentCell.getStringCellValue() == "1")
		        			  bf.setCompteurInstalle(true);
		        		  else
		        			  bf.setCompteurInstalle(false);
		        	  }
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 6: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  bf.setLongitude((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 7: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  bf.setLatitude((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          }
		          
		          cellIdx++;
		        }
		        //ajouter la ligne récuperer à la liste des région (il s'agit pas encore de la sauvegarde dans la base
		        listeBorneFontaines.add(bf);
	        }
	      } 

	      workbook.close();
	      
	    } catch (IOException e) {
	    	System.out.println(e);
	    }
		return listeBorneFontaines;
	  }
	public  List<Abreuvoir> excelToModeleAbreuvoir(InputStream is) {
		List<Abreuvoir> listeAbreuvoirs = new ArrayList<Abreuvoir>();
		try {
	    
		  //instancier un classeur Excel
	      Workbook workbook = new XSSFWorkbook(is);
	      //accéder à la première feuille
	      Sheet sheet = workbook.getSheetAt(1);
	      //partcourir les lignes de la feuille
	      Iterator<Row> rows = sheet.iterator();
	      int rowNumber = 0;
	      while (rows.hasNext()) {
	        Row currentRow = rows.next();
	        // ignorer l'entête du fichier Excel lors de l'import
	        if (rowNumber == 0) {
	          rowNumber++;
	          continue;
	        }
	        //verifier que la ligne à importer  n'est pas vide
	        if(!isRowEmpty(currentRow)) {
		        Iterator<Cell> cellsInRow = currentRow.iterator();
		        Abreuvoir abreuvoir = new Abreuvoir();
		        int cellIdx = 0;
		        //parcourir les colonnes d'une ligne
		        while (cellsInRow.hasNext()) {
		          Cell currentCell = cellsInRow.next();
		          switch (cellIdx) {
		          //première colonne
		          case 0: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  abreuvoir.setSystemeAEP(systemeAEPService.findByNumeroIRH(currentCell.getStringCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 1: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  abreuvoir.setNumeroAbreuvoir((int)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 2: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  abreuvoir.setCodeINS(currentCell.getStringCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 3: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  abreuvoir.setNbRobinets((int)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 4: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  abreuvoir.setNbRobinetsFonctionnels((int)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 5: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  abreuvoir.setLongitude((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 6: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  abreuvoir.setLatitude((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          }
		          
		          cellIdx++;
		        }
		        //ajouter la ligne récuperer à la liste des région (il s'agit pas encore de la sauvegarde dans la base
		        listeAbreuvoirs.add(abreuvoir);
	        }
	      } 

	      workbook.close();
	      
	    } catch (IOException e) {
	    	System.out.println(e);
	    }
		return listeAbreuvoirs;
	  }
	
	public  List<BranchementParticulier> excelToModeleBranchementParticulier(InputStream is) {
		List<BranchementParticulier> listeBranchementParticuliers = new ArrayList<BranchementParticulier>();
		try {
	    
		  //instancier un classeur Excel
	      Workbook workbook = new XSSFWorkbook(is);
	      //accéder à la première feuille
	      Sheet sheet = workbook.getSheetAt(1);
	      //partcourir les lignes de la feuille
	      Iterator<Row> rows = sheet.iterator();
	      int rowNumber = 0;
	      while (rows.hasNext()) {
	        Row currentRow = rows.next();
	        // ignorer l'entête du fichier Excel lors de l'import
	        if (rowNumber == 0) {
	          rowNumber++;
	          continue;
	        }
	        //verifier que la ligne à importer  n'est pas vide
	        if(!isRowEmpty(currentRow)) {
		        Iterator<Cell> cellsInRow = currentRow.iterator();
		        BranchementParticulier bp = new BranchementParticulier();
		        int cellIdx = 0;
		        //parcourir les colonnes d'une ligne
		        while (cellsInRow.hasNext()) {
		          Cell currentCell = cellsInRow.next();
		          switch (cellIdx) {
		          //première colonne
		          case 0: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  bp.setSystemeAEP(systemeAEPService.findByNumeroIRH(currentCell.getStringCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 1: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  bp.setCodeINS(currentCell.getStringCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 2: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  bp.setLieu(lieuService.findByCode((int)currentCell.getNumericCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 3: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  bp.setNomLieu(currentCell.getStringCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 4: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        	  {
		        		  if(currentCell.getStringCellValue() == "1")
		        			  bp.setCompteurInstalle(true);
		        		  else
		        			  bp.setCompteurInstalle(false);
		        	  }
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 5: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  bp.setLongitude((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 6: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  bp.setLatitude((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          }
		          
		          cellIdx++;
		        }
		        //ajouter la ligne récuperer à la liste des région (il s'agit pas encore de la sauvegarde dans la base
		        listeBranchementParticuliers.add(bp);
	        }
	      } 

	      workbook.close();
	      
	    } catch (IOException e) {
	    	System.out.println(e);
	    }
		return listeBranchementParticuliers;
	  }
	
	public  List<ChateauEau> excelToModeleChateauEau(InputStream is) {
		List<ChateauEau> listeChateauEaux = new ArrayList<ChateauEau>();
		try {
	    
		  //instancier un classeur Excel
	      Workbook workbook = new XSSFWorkbook(is);
	      //accéder à la première feuille
	      Sheet sheet = workbook.getSheetAt(1);
	      //partcourir les lignes de la feuille
	      Iterator<Row> rows = sheet.iterator();
	      int rowNumber = 0;
	      while (rows.hasNext()) {
	        Row currentRow = rows.next();
	        // ignorer l'entête du fichier Excel lors de l'import
	        if (rowNumber == 0) {
	          rowNumber++;
	          continue;
	        }
	        //verifier que la ligne à importer  n'est pas vide
	        if(!isRowEmpty(currentRow)) {
		        Iterator<Cell> cellsInRow = currentRow.iterator();
		        ChateauEau chateau = new ChateauEau();
		        int cellIdx = 0;
		        //parcourir les colonnes d'une ligne
		        while (cellsInRow.hasNext()) {
		          Cell currentCell = cellsInRow.next();
		          switch (cellIdx) {
		          //première colonne
		          case 0: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  chateau.setSystemeAEP(systemeAEPService.findByNumeroIRH(currentCell.getStringCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 1: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  chateau.setNumeroChateau((int)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 2: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  chateau.setCodeINS(currentCell.getStringCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 3: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  chateau.setCapacite((double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 4: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  chateau.setDisposition(dispositionService.findByCode((int)currentCell.getNumericCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 5: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  chateau.setHauteur((double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 6: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  chateau.setMateriaux(materiauxService.findByCode((int)currentCell.getNumericCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 7: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        	  {
		        		  if(currentCell.getStringCellValue() == "1")
		        			  chateau.setExistenceCompteur(true);
		        		  else
		        			  chateau.setExistenceCompteur(false);
		        	  }
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 8: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  chateau.setIndexCompteur((double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 9: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  chateau.setHydraulique(hydrauliqueService.findByCode((int)currentCell.getNumericCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 10: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  chateau.setTuyauterie(tuyauterieService.findByCode((int)currentCell.getNumericCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 11: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  chateau.setDateDemarrageTravaux((Date)currentCell.getDateCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 12: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  chateau.setDateFinTravaux((Date)currentCell.getDateCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		        
		          }
		          
		          cellIdx++;
		        }
		        //ajouter la ligne récuperer à la liste des région (il s'agit pas encore de la sauvegarde dans la base
		        listeChateauEaux.add(chateau);
	        }
	      } 

	      workbook.close();
	      
	    } catch (IOException e) {
	    	System.out.println(e);
	    }
		return listeChateauEaux;
	  }
	
	public  List<CaptageSystemeAEP> excelToModeleCaptageSystemeAEP(InputStream is) {
		List<CaptageSystemeAEP> listeCaptageSystemeAEPs = new ArrayList<CaptageSystemeAEP>();
		try {
	    
		  //instancier un classeur Excel
	      Workbook workbook = new XSSFWorkbook(is);
	      //accéder à la première feuille
	      Sheet sheet = workbook.getSheetAt(1);
	      //partcourir les lignes de la feuille
	      Iterator<Row> rows = sheet.iterator();
	      int rowNumber = 0;
	      while (rows.hasNext()) {
	        Row currentRow = rows.next();
	        // ignorer l'entête du fichier Excel lors de l'import
	        if (rowNumber == 0) {
	          rowNumber++;
	          continue;
	        }
	        //verifier que la ligne à importer  n'est pas vide
	        if(!isRowEmpty(currentRow)) {
		        Iterator<Cell> cellsInRow = currentRow.iterator();
		        CaptageSystemeAEP captage = new CaptageSystemeAEP();
		        int cellIdx = 0;
		        //parcourir les colonnes d'une ligne
		        while (cellsInRow.hasNext()) {
		          Cell currentCell = cellsInRow.next();
		          switch (cellIdx) {
		          //première colonne
		          case 0: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  captage.setSystemeAEP(systemeAEPService.findByNumeroIRH(currentCell.getStringCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 1: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  captage.setForage(forageService.findByNumeroIRH(currentCell.getStringCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 2: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  captage.setTypeSystemeProtection(typeSystemeProtectionService.findByCode((int)currentCell.getNumericCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 3: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        	  {
		        		  if(currentCell.getStringCellValue() == "1")
		        			  captage.setProtectionTeteForage(true);
		        		  else
		        			  captage.setProtectionTeteForage(false);
		        	  }
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 4: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        	  {
		        		  if(currentCell.getStringCellValue() == "1")
		        			  captage.setEtatCompteurForage(true);
		        		  else
		        			  captage.setEtatCompteurForage(false);
		        	  }
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 5: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  captage.setIndexCompteurForage((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 6: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  captage.setProfondeurTotaleMesure((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 7: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  captage.setProfondeurNiveauStatique((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 8: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  captage.setDiametreEquipement((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 9: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  captage.setTypeSystemeInstallation(typeSystemeInstallationService.findByCode((int)currentCell.getNumericCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 10: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  captage.setPuissance((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 11: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  captage.setDateInstallation((Date)currentCell.getDateCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 12: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  captage.setMarquePompe(marquePompeService.findByIsDeletedFalseAndLibelle(currentCell.getStringCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 13: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  captage.setModelePompe(modelePompeService.findByIsDeletedFalseAndLibelle(currentCell.getStringCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 14: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  captage.setDebitNominal((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 15: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  captage.setHMT((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 16: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  captage.setLongueurColonne((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 17: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  captage.setTypeColonneExhaure(currentCell.getStringCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 18: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  captage.setDiametreColonne((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 19: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  captage.setCoteInstallationPompe((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 20: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        	  {
		        		  if(currentCell.getStringCellValue() == "1")
		        			  captage.setTubageProtectionTete(true);
		        		  else
		        			  captage.setTubageProtectionTete(false);
		        	  }
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 21: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        	  {
		        		  if(currentCell.getStringCellValue() == "1")
		        			  captage.setCouvercleProtectionTete(true);
		        		  else
		        			  captage.setCouvercleProtectionTete(false);
		        	  }
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 22: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        	  {
		        		  if(currentCell.getStringCellValue() == "1")
		        			  captage.setPossibiliteMesurePiezometrique(true);
		        		  else
		        			  captage.setPossibiliteMesurePiezometrique(false);
		        	  }
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 23: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  captage.setDateInstallation2((Date)currentCell.getDateCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 24: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  captage.setMarquePompe2(marquePompeService.findByIsDeletedFalseAndLibelle(currentCell.getStringCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 25: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  captage.setModelePompe2(modelePompeService.findByIsDeletedFalseAndLibelle(currentCell.getStringCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 26: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  captage.setPuissance2((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 27: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  captage.setVoltage((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 28: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  captage.setTypeTableauDistribution(currentCell.getStringCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 29: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  captage.setPuissanceTransformateur((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 30: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  captage.setPuissanceCompteurElectrique((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 31: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  captage.setMarqueModule(currentCell.getStringCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 32: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  captage.setNombreModule((int)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 33: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  captage.setPuissanceUnitaireModule((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 34: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  captage.setPuissanceTotale((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 35: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  captage.setMarqueOnduleur(currentCell.getStringCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 36: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  captage.setPuissanceOnduleur((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          }
		          
		          cellIdx++;
		        }
		        //ajouter la ligne récuperer à la liste des région (il s'agit pas encore de la sauvegarde dans la base
		        listeCaptageSystemeAEPs.add(captage);
	        }
	      } 

	      workbook.close();
	      
	    } catch (IOException e) {
	    	System.out.println(e);
	    }
		return listeCaptageSystemeAEPs;
	  }
	
	public  List<ReseauDistribution> excelToModeleReseauDistribution(InputStream is) {
		List<ReseauDistribution> listeReseauDistributions = new ArrayList<ReseauDistribution>();
		try {
	    
		  //instancier un classeur Excel
	      Workbook workbook = new XSSFWorkbook(is);
	      //accéder à la première feuille
	      Sheet sheet = workbook.getSheetAt(1);
	      //partcourir les lignes de la feuille
	      Iterator<Row> rows = sheet.iterator();
	      int rowNumber = 0;
	      while (rows.hasNext()) {
	        Row currentRow = rows.next();
	        // ignorer l'entête du fichier Excel lors de l'import
	        if (rowNumber == 0) {
	          rowNumber++;
	          continue;
	        }
	        //verifier que la ligne à importer  n'est pas vide
	        if(!isRowEmpty(currentRow)) {
		        Iterator<Cell> cellsInRow = currentRow.iterator();
		        ReseauDistribution reseau = new ReseauDistribution();
		        int cellIdx = 0;
		        //parcourir les colonnes d'une ligne
		        while (cellsInRow.hasNext()) {
		          Cell currentCell = cellsInRow.next();
		          switch (cellIdx) {
		          //première colonne
		          case 0: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  reseau.setSystemeAEP(systemeAEPService.findByNumeroIRH(currentCell.getStringCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 1: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  reseau.setNumeroTroncon((int)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
				  case 2: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  reseau.setLongueur((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
				  case 3: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  reseau.setDiametre((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
				  case 4: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  reseau.setDepartLongitude((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 5: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  reseau.setDepartLatitude((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 6: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  reseau.setFinLongitude((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 7: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  reseau.setFinLatitude((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 8: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  reseau.setTuyauterie(tuyauterieService.findByCode((int)currentCell.getNumericCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		         
		          }
		          cellIdx++;
		        }
		        //ajouter la ligne récuperer à la liste des région (il s'agit pas encore de la sauvegarde dans la base
		        listeReseauDistributions.add(reseau);
	        }
	      } 

	      workbook.close();
	      
	    } catch (IOException e) {
	    	System.out.println(e);
	    }
		return listeReseauDistributions;
	  }
	
	public  List<ReseauRefoulement> excelToModeleReseauRefoulement(InputStream is) {
		List<ReseauRefoulement> listeReseauRefoulements = new ArrayList<ReseauRefoulement>();
		try {
	    
		  //instancier un classeur Excel
	      Workbook workbook = new XSSFWorkbook(is);
	      //accéder à la première feuille
	      Sheet sheet = workbook.getSheetAt(1);
	      //partcourir les lignes de la feuille
	      Iterator<Row> rows = sheet.iterator();
	      int rowNumber = 0;
	      while (rows.hasNext()) {
	        Row currentRow = rows.next();
	        // ignorer l'entête du fichier Excel lors de l'import
	        if (rowNumber == 0) {
	          rowNumber++;
	          continue;
	        }
	        //verifier que la ligne à importer  n'est pas vide
	        if(!isRowEmpty(currentRow)) {
		        Iterator<Cell> cellsInRow = currentRow.iterator();
		        ReseauRefoulement reseau = new ReseauRefoulement();
		        int cellIdx = 0;
		        //parcourir les colonnes d'une ligne
		        while (cellsInRow.hasNext()) {
		          Cell currentCell = cellsInRow.next();
		          switch (cellIdx) {
		          //première colonne
		          case 0: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  reseau.setSystemeAEP(systemeAEPService.findByNumeroIRH(currentCell.getStringCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 1: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  reseau.setNumeroTroncon((int)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
				  case 2: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  reseau.setLongueur((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
				  case 3: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  reseau.setDiametre((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
				  case 4: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  reseau.setDepartLongitude((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 5: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  reseau.setDepartLatitude((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 6: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  reseau.setFinLongitude((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 7: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  reseau.setFinLatitude((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 8: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  reseau.setTuyauterie(tuyauterieService.findByCode((int)currentCell.getNumericCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		         
		          }
		          cellIdx++;
		        }
		        //ajouter la ligne récuperer à la liste des région (il s'agit pas encore de la sauvegarde dans la base
		        listeReseauRefoulements.add(reseau);
	        }
	      } 

	      workbook.close();
	      
	    } catch (IOException e) {
	    	System.out.println(e);
	    }
		return listeReseauRefoulements;
	  }
	
	public  List<PieceRegulationReseau> excelToModelePieceRegulationReseau(InputStream is) {
		List<PieceRegulationReseau> listePieceRegulationReseaux = new ArrayList<PieceRegulationReseau>();
		try {
	    
		  //instancier un classeur Excel
	      Workbook workbook = new XSSFWorkbook(is);
	      //accéder à la première feuille
	      Sheet sheet = workbook.getSheetAt(1);
	      //partcourir les lignes de la feuille
	      Iterator<Row> rows = sheet.iterator();
	      int rowNumber = 0;
	      while (rows.hasNext()) {
	        Row currentRow = rows.next();
	        // ignorer l'entête du fichier Excel lors de l'import
	        if (rowNumber == 0) {
	          rowNumber++;
	          continue;
	        }
	        //verifier que la ligne à importer  n'est pas vide
	        if(!isRowEmpty(currentRow)) {
		        Iterator<Cell> cellsInRow = currentRow.iterator();
		        PieceRegulationReseau piece = new PieceRegulationReseau();
		        int cellIdx = 0;
		        //parcourir les colonnes d'une ligne
		        while (cellsInRow.hasNext()) {
		          Cell currentCell = cellsInRow.next();
		          switch (cellIdx) {
		          //première colonne 
		          case 0: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  piece.setTypePieceRegulationReseau(typePieceRegulationReseauService.findByCode((int)currentCell.getNumericCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 1: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  piece.setSystemeAEP(systemeAEPService.findByNumeroIRH(currentCell.getStringCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 2: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  piece.setNumeroOrdre((int)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
				  case 3: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  piece.setLongitude((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 4: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  piece.setLatitude((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		         
		          }
		          cellIdx++;
		        }
		        //ajouter la ligne récuperer à la liste des région (il s'agit pas encore de la sauvegarde dans la base
		        listePieceRegulationReseaux.add(piece);
	        }
	      } 

	      workbook.close();
	      
	    } catch (IOException e) {
	    	System.out.println(e);
	    }
		return listePieceRegulationReseaux;
	  }
	//sauvegarde des informations du fichier excel dans la base de données
	public List<String> importData(MultipartFile file) {
		List<String> unsaved = new ArrayList<String>();
		try {
	      //récupération des lignes Excel dans un objet List
	      List<SystemeAEP> listeSystemeAEPs = excelToModeleSystemeAEP(file.getInputStream());
	      List<BorneFontaine> listeBorneFontaines = excelToModeleBorneFontaine(file.getInputStream());
	      List<Abreuvoir> listeAbreuvoirs = excelToModeleAbreuvoir(file.getInputStream());
	      List<BranchementParticulier> listeBranchementParticuliers = excelToModeleBranchementParticulier(file.getInputStream());
	      List<ChateauEau> listeChateauEaux = excelToModeleChateauEau(file.getInputStream());
	      List<CaptageSystemeAEP> listeCaptageSystemeAEPs = excelToModeleCaptageSystemeAEP(file.getInputStream());
	      List<ReseauDistribution> listeReseauDistributions = excelToModeleReseauDistribution(file.getInputStream());
	      List<ReseauRefoulement> listeReseauRefoulements = excelToModeleReseauRefoulement(file.getInputStream());
	      List<PieceRegulationReseau> listePieceRegulationReseaux = excelToModelePieceRegulationReseau(file.getInputStream());
		     
	      
	      //sauvegarde des informations récuperés du fichier excel dans la base de données
	      
	      for(int i=0;i<listeSystemeAEPs.size();i++)
	      {
	    	  try {
	    		  systemeAEPService.save(listeSystemeAEPs.get(i));
		    	  
	    	  }catch(Exception e)
	    	  {
	    		  unsaved.add("SystemeAEP N°IRH "+listeSystemeAEPs.get(i).getNumeroIRH()); 
	    	  }
	    	  
	      }
	      //saepService.saveAll(listeSystemeAEPs);
	      systemeAEPService.saveAll(listeSystemeAEPs);
	      borneFontaineService.saveAll(listeBorneFontaines);
	      abreuvoirService.saveAll(listeAbreuvoirs);
	      branchementParticulierService.saveAll(listeBranchementParticuliers);
	      chateauEauService.saveAll(listeChateauEaux);
	      captageSystemeAEPService.saveAll(listeCaptageSystemeAEPs);
	      reseauDistributionService.saveAll(listeReseauDistributions);
	      reseauRefoulementService.saveAll(listeReseauRefoulements);
	      pieceRegulationReseauService.saveAll(listePieceRegulationReseaux);
		  
	      
	      
	    } catch (IOException e) {
	    	System.out.println(e);
	     
	    }
		return unsaved;
	  }
	
	//mettre le les informations dans un objet ByteArrayInputStream en vue de la création du fichier excel
	 public  ByteArrayInputStream modelePompeToExcel(List<SystemeAEP> saeps) {

		    try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) {
		      Sheet sheet = workbook.createSheet("SystemeAEPs");
		      // création de l'entête du fichier excel
		      Row headerRow = sheet.createRow(0);
		      //création des colonnes de l'entête
		      for (int col = 0; col < headerSystemeAEP.length; col++) {
		        Cell cell = headerRow.createCell(col);
		        cell.setCellValue(headerSystemeAEP[col]);
		      }

		      int rowIdx = 1;
		      //création des lignes du fichier
		      for (SystemeAEP saep : saeps) {
		        Row row = sheet.createRow(rowIdx++);
		        //colonne 1 / cellule
		        //row.createCell(0).setCellValue(modelePompe.getCode());
		      //colonne 2 / cellule
		        //row.createCell(1).setCellValue(modelePompe.getLibelle());
		      }

		      workbook.write(out);
		      return new ByteArrayInputStream(out.toByteArray());
		      
		    } catch (IOException e) {
		      throw new RuntimeException(e.getMessage());
		    }
		  }
	 
	 //récuperaton des données de la base pour les convertir en ByteArrayInputStream
	 public ByteArrayInputStream  exportData() {  
	    	List<SystemeAEP> saep = (List<SystemeAEP>) systemeAEPService.findAll();
	    	ByteArrayInputStream in = modelePompeToExcel(saep);
	    	return in;
		    
		  }
	 
	 //vérifier si une ligne est vide
	 private  boolean isRowEmpty(Row row) {
			boolean isEmpty = true;
			DataFormatter dataFormatter = new DataFormatter();
			if (row != null) {
				for (Cell cell : row) {
					if (dataFormatter.formatCellValue(cell).trim().length() > 0) {
						isEmpty = false;
						break;
					}
				}
			}

			return isEmpty;
		}
		
	
}
