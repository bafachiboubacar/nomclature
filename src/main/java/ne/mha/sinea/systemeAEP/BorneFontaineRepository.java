package ne.mha.sinea.systemeAEP;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface BorneFontaineRepository extends CrudRepository<BorneFontaine, Integer> {
	
	BorneFontaine findByCode(Integer code);
	List<BorneFontaine> findBySystemeAEP_NumeroIRH(String IRH);

}