package ne.mha.sinea.systemeAEP;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InitAEPForm {
	private String numeroIRH;
	private String nomAEP;
}
