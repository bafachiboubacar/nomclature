package ne.mha.sinea.systemeAEP;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;


public interface SystemeAEPRepository extends CrudRepository<SystemeAEP, Integer> {
	
	SystemeAEP findByCode(Integer code);
	SystemeAEP findByNumeroIRH(String IRH);
	
	@Transactional 
	@Query(nativeQuery = true, value="(select nom_aep as nom_ouvrage, 'AEP' as type_ouvrage, '' as latitude,'' as longitude, concat('aep/',code) as lien from systeme_aep) union (select nom_pem as nom_ouvrage, 'PCP_PCV' as type_ouvrage, concat('',latitude) as latitude,concat('',longitude) as longitude, concat('puits/',code) as lien from puits_cimente union (select nom_pem as nom_ouvrage, 'forage' as type_ouvrage, concat('',latitude) as latitude,concat('',longitude) as longitude, concat('forage/',code) as lien from forage))")
	List<Object[]> ListOuvrage();
	
}