package ne.mha.sinea.systemeAEP;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import ne.mha.sinea.CommonProperties;
import ne.mha.sinea.nomenclature.tuyauterie.Tuyauterie;

@Data
@Entity
@Table(name = "reseau_refoulement")
public class ReseauRefoulement extends CommonProperties {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "code")
	protected Integer code;
	@Column(name = "numero_troncon")
	protected Integer numeroTroncon;
	@Column(name = "longueur")
	protected Double longueur;
	@Column(name = "diametre")
	protected Double diametre;
	@Column(name = "depart_latitude")
	protected Double departLatitude;
	@Column(name = "depart_longitude")
	protected Double departLongitude;
	@Column(name = "fin_latitude")
	protected Double finLatitude;
	@Column(name = "fin_longitude")
	protected Double finLongitude;
	@JoinColumn(name = "code_tuyauterie", referencedColumnName = "code")
	@ManyToOne
	protected Tuyauterie tuyauterie;
	@JoinColumn(name = "code_systeme_aep", referencedColumnName = "code")
	@ManyToOne
	protected SystemeAEP systemeAEP;
	
}
