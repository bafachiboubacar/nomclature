package ne.mha.sinea.systemeAEP;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SystemeAEPInfosGeneralesForm {

	private String numeroIRH;
	private Date anneeRealisation;
	private Integer codeTypeSystemeAEP;
	private Integer codePropriete;
	private Integer codeFinancement;
	private Integer codeProjet;
	private Integer codeLocalite;
	private Integer codeEtatOuvrage;
	private boolean sourceEnergieSolaire;
	private boolean sourceEnergieThermique;
	private boolean sourceEnergieReseauElectrique;
	private boolean sourceEnergieEolienne;
	private boolean sourceEnergieAutres;
	private String precisionEnergieAutres;

}
