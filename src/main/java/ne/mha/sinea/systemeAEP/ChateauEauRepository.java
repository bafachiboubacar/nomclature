package ne.mha.sinea.systemeAEP;

import java.util.List;
import org.springframework.data.repository.CrudRepository;


public interface ChateauEauRepository extends CrudRepository<ChateauEau, Integer> {
	
	ChateauEau findByCode(Integer code);
	List<ChateauEau> findBySystemeAEP_NumeroIRH(String IRH);
	
}