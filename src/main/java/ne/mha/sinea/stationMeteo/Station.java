package ne.mha.sinea.stationMeteo;

import java.sql.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ne.mha.sinea.CommonProperties;
import ne.mha.sinea.nomenclature.typeStationMeteo.TypeStationMeteo;
import ne.mha.sinea.referentiel.reseauSuivi.ReseauSuivi;
import ne.mha.sinea.referentiel.zone.Localite;

//annotation de lombok pour générer les getter et setter
@Data
//annotation de lombok pour générer le constructeur sans paramètre
@NoArgsConstructor
//annotation de lombok pour générer le constructeur avec tous les paramètres
@AllArgsConstructor
//indique qu'il s'agit d'une entité=> création d'une table au niveau de la base de données
@Entity
@Table(name = "station")
public class Station extends CommonProperties{
	
	//annotation pour indiquier l'identifiant d'une table
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "code")
	private Integer code;
	@NotNull
	@Column(name = "nom")
	private String nom;
	@Column(name = "date_ouverture")
	private Date dateOuverture;
	@Column(name = "latitude")
	private double latitude;
	@Column(name = "longitude")
	private double longitude;
	@Column(name = "altitude")
	private double altitude;
	@Column(name = "date_fermeture")
	private Date dateFermeture;
	@Column(name = "cause_fermeture")
	private String causeFermeture;
	
	@JoinColumn(name = "code_reseau_suivi", referencedColumnName = "code")
	@ManyToOne
	private ReseauSuivi reseauSuivi;
	@JoinColumn(name = "code_localite", referencedColumnName = "code")
	@ManyToOne
	private Localite localite;
	@JoinColumn(name = "code_type_station", referencedColumnName = "code")
	@ManyToOne
	private TypeStationMeteo typeStation;
	@OneToMany(mappedBy = "station")
    Set<EquipementInstalle> equipementInstalles;
	@OneToMany(mappedBy = "station")
    Set<ChroniqueObservation> chroniqueObservations;
	
}
