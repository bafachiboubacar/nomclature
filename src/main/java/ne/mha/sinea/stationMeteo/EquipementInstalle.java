package ne.mha.sinea.stationMeteo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ne.mha.sinea.CommonProperties;
import ne.mha.sinea.nomenclature.typeEquipementStationMeteo.TypeEquipementStationMeteo;

//annotation de lombok pour générer les getter et setter
@Data
//annotation de lombok pour générer le constructeur sans paramètre
@NoArgsConstructor
//annotation de lombok pour générer le constructeur avec tous les paramètres
@AllArgsConstructor
//indique qu'il s'agit d'une entité=> création d'une table au niveau de la base de données
@Entity
@Table(name = "equipement_installe")
public class EquipementInstalle extends CommonProperties{

	@EmbeddedId
	private EquipementInstalleKey code;
	 
	@MapsId("codeStation")
	@JoinColumn(name = "code_station", referencedColumnName = "code")
	@ManyToOne
	private Station station;
	
	@MapsId("codeTypeEquipementStation")
	@JoinColumn(name = "code_type_equipement_station", referencedColumnName = "code")
	@ManyToOne
	private TypeEquipementStationMeteo typeEquipementStation;
	@Column(name = "date_installation")  
	private Date dateInstallation;
	 
	 
}