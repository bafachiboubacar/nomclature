package ne.mha.sinea.stationMeteo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import ne.mha.sinea.Template;
import ne.mha.sinea.nomenclature.typeStationMeteo.TypeStationMeteo;
import ne.mha.sinea.nomenclature.typeStationMeteo.TypeStationMeteoRepository;
import ne.mha.sinea.referentiel.reseauSuivi.ReseauSuivi;
import ne.mha.sinea.referentiel.reseauSuivi.ReseauSuiviRepository;
import ne.mha.sinea.referentiel.zone.Localite;
import ne.mha.sinea.referentiel.zone.LocaliteRepository;

@Controller
public class StationMeteoController {

	
	@Autowired
	StationMeteoRepository stationService;
	@Autowired
	TypeStationMeteoRepository typeStationService;
	@Autowired
	LocaliteRepository localiteService;
	@Autowired
	ReseauSuiviRepository reseauSuiviService;

	//formulaire station dont l'ajout , l'import et l'export
	//@PreAuthorize("hasAuthority('Ajout Station'))
	@GetMapping("/stationMeteo")
	public String  addStation(StationForm stationForm, Model model) {
		try{
			//récuperation de la liste des station de la base de données
			List<Station> stations =   stationService.findByIsDeletedFalse();
			List<TypeStationMeteo> typeStations =   typeStationService.findByIsDeletedFalse();
			List<ReseauSuivi> reseauSuivis =   reseauSuiviService.findByIsDeletedFalse();
			List<Localite> localites =   localiteService.findByIsDeletedFalse();
			//envoie de la liste récuperée à la vue à travers le modele
			model.addAttribute("stations", stations);
			model.addAttribute("typeStations", typeStations);
			model.addAttribute("reseauSuivis", reseauSuivis);
			model.addAttribute("localites", localites);
			//indiquer quelle vue va être afficher (à l'interieur du template)
			model.addAttribute("viewPath", "ressourceEau/station/form/stationMeteo");
			//indiquer quel menu horizontal va être afficher (à l'interieur du template)
			model.addAttribute("horizontalMenu", "ressourceEau/horizontalMenu"); 
			//indiquer quel menu vertical(sidebar) va être afficher (à l'interieur du template)
			model.addAttribute("sidebarMenu", "ressourceEau/station/sidebarMenu"); 
			//indiquer quel breadcrumb va être afficher (à l'interieur du template)
			model.addAttribute("breadcrumb", "ressourceEau/breadcrumb");
			}
		catch(Exception e){
				
			}
		//afficher le template avec la vue (formulaire) à l'intérieur
		return Template.defaultTemplate;

	}
	
	//validation du formulaire d'ajout station
	@PostMapping("/stationMeteo")
    public String addStationxSubmit(@Validated StationForm stationForm,BindingResult bindingResult, Model model) {
    	//s'il n'ya pas d'erreur lors de la validation des données du formulaire
		if (!bindingResult.hasErrors()) {
			
			try {
				//récuperer les données saisies
				Station station = new Station();
				
				station.setNom(stationForm.getNom());
				station.setDateOuverture(stationForm.getDateOuverture());
				station.setLatitude(stationForm.getLatitude());
				station.setLongitude(stationForm.getLongitude());
				station.setAltitude(stationForm.getAltitude());
				station.setDateFermeture(stationForm.getDateFermeture());
				station.setCauseFermeture(stationForm.getCauseFermeture());
				station.setTypeStation(typeStationService.findByCode(stationForm.getCodeTypeStation()));
				station.setReseauSuivi(reseauSuiviService.findByCode(stationForm.getCodeReseauSuivi()));
				station.setLocalite(localiteService.findByCode(stationForm.getCodeLocalite()));
				//enregistrer les données (l'entité=) dans la base
				stationService.save(station);
				
				//indiquer que l'operation d'ajout a réussie
				model.addAttribute("operationStatus", "operationStatus/success");
				
				//récuperation de la liste des station de la base de données
				List<Station> stations =   stationService.findByIsDeletedFalse();
				List<TypeStationMeteo> typeStations =   typeStationService.findByIsDeletedFalse();
				List<ReseauSuivi> reseauSuivis =   reseauSuiviService.findByIsDeletedFalse();
				List<Localite> localites =   localiteService.findByIsDeletedFalse();
				//envoie de la liste récuperée à la vue à travers le modele
				model.addAttribute("stations", stations);
				model.addAttribute("typeStations", typeStations);
				model.addAttribute("reseauSuivis", reseauSuivis);
				model.addAttribute("localites", localites);
				//indiquer quelle vue va être afficher (à l'interieur du template)
				model.addAttribute("viewPath", "ressourceEau/station/form/stationMeteo");
				//indiquer quel menu horizontal va être afficher (à l'interieur du template)
				model.addAttribute("horizontalMenu", "ressourceEau/horizontalMenu"); 
				//indiquer quel menu vertical(sidebar) va être afficher (à l'interieur du template)
				model.addAttribute("sidebarMenu", "ressourceEau/station/sidebarMenu"); 
				//indiquer quel breadcrumb va être afficher (à l'interieur du template)
				model.addAttribute("breadcrumb", "ressourceEau/breadcrumb");
				
				}
			catch(Exception e){
				//indiquer que l'operation d'ajout a réussie
				model.addAttribute("operationStatus", "operationStatus/unsuccess");
				
				//récuperation de la liste des station de la base de données
				List<Station> stations =   stationService.findByIsDeletedFalse();
				List<TypeStationMeteo> typeStations =   typeStationService.findByIsDeletedFalse();
				List<ReseauSuivi> reseauSuivis =   reseauSuiviService.findByIsDeletedFalse();
				List<Localite> localites =   localiteService.findByIsDeletedFalse();
				//envoie de la liste récuperée à la vue à travers le modele
				model.addAttribute("stations", stations);
				model.addAttribute("typeStations", typeStations);
				model.addAttribute("reseauSuivis", reseauSuivis);
				model.addAttribute("localites", localites);
				//indiquer quelle vue va être afficher (à l'interieur du template)
				model.addAttribute("viewPath", "ressourceEau/station/form/stationMeteo");
				//indiquer quel menu horizontal va être afficher (à l'interieur du template)
				model.addAttribute("horizontalMenu", "ressourceEau/horizontalMenu"); 
				//indiquer quel menu vertical(sidebar) va être afficher (à l'interieur du template)
				model.addAttribute("sidebarMenu", "ressourceEau/station/sidebarMenu"); 
				//indiquer quel breadcrumb va être afficher (à l'interieur du template)
				model.addAttribute("breadcrumb", "ressourceEau/breadcrumb");
				
					}
			
    	}
		else{
    		try{
    			//indiquer que l'operation d'ajout a réussie
				model.addAttribute("operationStatus", "operationStatus/unsuccess");
				
				//récuperation de la liste des station de la base de données
				List<Station> stations =   stationService.findByIsDeletedFalse();
				List<TypeStationMeteo> typeStations =   typeStationService.findByIsDeletedFalse();
				List<ReseauSuivi> reseauSuivis =   reseauSuiviService.findByIsDeletedFalse();
				List<Localite> localites =   localiteService.findByIsDeletedFalse();
				//envoie de la liste récuperée à la vue à travers le modele
				model.addAttribute("stations", stations);
				model.addAttribute("typeStations", typeStations);
				model.addAttribute("reseauSuivis", reseauSuivis);
				model.addAttribute("localites", localites);
				//indiquer quelle vue va être afficher (à l'interieur du template)
				model.addAttribute("viewPath", "ressourceEau/station/form/stationMeteo");
				//indiquer quel menu horizontal va être afficher (à l'interieur du template)
				model.addAttribute("horizontalMenu", "ressourceEau/horizontalMenu"); 
				//indiquer quel menu vertical(sidebar) va être afficher (à l'interieur du template)
				model.addAttribute("sidebarMenu", "ressourceEau/station/sidebarMenu"); 
				//indiquer quel breadcrumb va être afficher (à l'interieur du template)
				model.addAttribute("breadcrumb", "ressourceEau/breadcrumb");
				}
    		catch(Exception e){
				
				}
    	}
		
		//afficher le template avec la vue (formulaire) à l'intérieur
		return Template.defaultTemplate;
        
    }
	
	
	//modification d'une station
	@GetMapping("/updateStationMeteo/{codeStation}")
    public String updateStation(@PathVariable("codeStation") int codeStation,StationForm stationForm, Model model) 
	{ 
		try
		{
			//récuperer les informations de la station à modifier
    		Station station =   stationService.findByCode(codeStation);
    		//envoie des informations de la station à modifier à travers le modele
    		
    		stationForm.setNom(station.getNom());
    		stationForm.setDateOuverture(station.getDateOuverture());
    		stationForm.setLatitude(station.getLatitude());
    		stationForm.setLongitude(station.getLongitude());
    		stationForm.setAltitude(station.getAltitude());
    		stationForm.setDateFermeture(station.getDateFermeture());
    		stationForm.setCauseFermeture(station.getCauseFermeture());
    		stationForm.setCodeTypeStation(station.getTypeStation().getCode());
    		stationForm.setCodeReseauSuivi(station.getReseauSuivi().getCode());
    		stationForm.setCodeLocalite(station.getLocalite().getCode());

    		//récuperation de la liste des station de la base de données
    		List<Station> stations =   stationService.findByIsDeletedFalse();
			List<TypeStationMeteo> typeStations =   typeStationService.findByIsDeletedFalse();
			List<ReseauSuivi> reseauSuivis =   reseauSuiviService.findByIsDeletedFalse();
			List<Localite> localites =   localiteService.findByIsDeletedFalse();
			//envoie de la liste récuperée à la vue à travers le modele
			model.addAttribute("stations", stations);
			model.addAttribute("typeStations", typeStations);
			model.addAttribute("reseauSuivis", reseauSuivis);
			model.addAttribute("localites", localites);
			//indiquer quelle vue va être afficher (à l'interieur du template)
			model.addAttribute("viewPath", "ressourceEau/station/form/stationMeteo");
			//indiquer quel menu horizontal va être afficher (à l'interieur du template)
			model.addAttribute("horizontalMenu", "ressourceEau/horizontalMenu"); 
			//indiquer quel menu vertical(sidebar) va être afficher (à l'interieur du template)
			model.addAttribute("sidebarMenu", "ressourceEau/station/sidebarMenu"); 
			//indiquer quel breadcrumb va être afficher (à l'interieur du template)
			model.addAttribute("breadcrumb", "ressourceEau/breadcrumb");
		}
		catch(Exception e)
		{
			
		}
		return Template.defaultTemplate;
    		
    }
	
	@PostMapping("/updateStationMeteo/{codeStation}")
    public RedirectView updateStationSubmit(@Validated StationForm stationForm,BindingResult bindingResult,@PathVariable("codeStation") int codeStation, Model model,RedirectAttributes redirectAttributes) 
	{
		final RedirectView redirectView = new RedirectView("/stationMeteo", true);
    	if (!bindingResult.hasErrors()) 
    	{
			try
			{
				//recupérer les informations saisies
				Station station = stationService.findByCode(codeStation);
				station.setNom(stationForm.getNom());
				station.setDateOuverture(stationForm.getDateOuverture());
				station.setLatitude(stationForm.getLatitude());
				station.setLongitude(stationForm.getLongitude());
				station.setAltitude(stationForm.getAltitude());
				station.setDateFermeture(stationForm.getDateFermeture());
				station.setCauseFermeture(stationForm.getCauseFermeture());
				station.setTypeStation(typeStationService.findByCode(stationForm.getCodeTypeStation()));
				station.setReseauSuivi(reseauSuiviService.findByCode(stationForm.getCodeReseauSuivi()));
				station.setLocalite(localiteService.findByCode(stationForm.getCodeLocalite()));
				//enregistrer les informations dans la base de données
				stationService.save(station);
				
				//indiquer que l'operation de modification a réussi
				redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/success");
				
			}
			catch(Exception e)
			{
				//indiquer que l'operation de modification a échoué
				redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/unsuccess");
			}
    	}
    	else
    	{
    		
    	}
		
    	//redirection vers la page d'ajout
		return redirectView;
        
    }
	
	@GetMapping("/deleteStationMeteo/{codeStation}")
	public RedirectView deleteStation(@PathVariable("codeStation") int codeStation, Model model,RedirectAttributes redirectAttributes) 
	{ 
		final RedirectView redirectView = new RedirectView("/stationMeteo", true);
		try
		{
			Station station = stationService.findByCode(codeStation);
			station.setDeleted(true);
			stationService.save(station);
			
			//indiquer que l'operation de suppression a réussi
			redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/success");
		}
		catch(Exception e)
		{
			//indiquer que l'operation de suppression a échoué
			redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/unsuccess");
		}
		
		//redirection vers la page d'ajout
		return redirectView;
	    		
	}

	
}
