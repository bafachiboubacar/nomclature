package ne.mha.sinea.stationMeteo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;


public interface StationMeteoRepository extends CrudRepository<Station, Integer> {
	Station findByCode(Integer code);
	Station findByNom(String nom);
	List<Station> findByIsDeletedFalse();
	List<Station> findByIsDeletedTrue();

}
