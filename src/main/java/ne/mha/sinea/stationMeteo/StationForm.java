package ne.mha.sinea.stationMeteo;

import java.sql.Date;

import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StationForm {

	private String nom;
	private Date dateOuverture;
	private double latitude;
	private double longitude;
	private double altitude;
	private Date dateFermeture;
	private String causeFermeture;
	private Integer codeTypeStation;
	private Integer codeReseauSuivi;
	private Integer codeLocalite;
}
