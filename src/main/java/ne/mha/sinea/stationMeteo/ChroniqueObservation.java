package ne.mha.sinea.stationMeteo;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ne.mha.sinea.CommonProperties;
import ne.mha.sinea.nomenclature.parametreStationMeteo.ParametreStationMeteo;

//annotation de lombok pour générer les getter et setter
@Data
//annotation de lombok pour générer le constructeur sans paramètre
@NoArgsConstructor
//annotation de lombok pour générer le constructeur avec tous les paramètres
@AllArgsConstructor
//indique qu'il s'agit d'une entité=> création d'une table au niveau de la base de données
@Entity
@Table(name = "chronique_observation")
@IdClass(ChroniqueObservationKey.class)
public class ChroniqueObservation extends CommonProperties {
	@Id
	@ManyToOne
	// @PrimaryKeyJoinColumn
	@JoinColumn(name = "code_station", referencedColumnName = "code")
	private Station station;

	@Id
	@ManyToOne
	@JoinColumn(name = "code_parametre_observe", referencedColumnName = "code")
	private ParametreStationMeteo parametreObserve;

	@Id
	@Column(name = "date_debut_observation")
	private Date dateDebutObservation;

	@Id
	@Column(name = "date_fin_observation")
	private Date dateFinObservation;

	@Column(name = "valeur_mesure")
	private double valeurMesure;

}
