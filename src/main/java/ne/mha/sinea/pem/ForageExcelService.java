package ne.mha.sinea.pem;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
//import java.sql.Date;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import ne.mha.sinea.nomenclature.etatOuvrage.EtatOuvrageRepository;
import ne.mha.sinea.nomenclature.financement.FinancementRepository;
import ne.mha.sinea.nomenclature.marquePompe.MarquePompeRepository;
import ne.mha.sinea.nomenclature.modelePompe.ModelePompeRepository;
import ne.mha.sinea.nomenclature.natureSol.NatureSolRepository;
import ne.mha.sinea.nomenclature.natureTube.NatureTubeRepository;
import ne.mha.sinea.nomenclature.propriete.ProprieteRepository;
import ne.mha.sinea.nomenclature.typeAmenagement.TypeAmenagementRepository;
import ne.mha.sinea.nomenclature.typePrestation.TypePrestationRepository;
import ne.mha.sinea.nomenclature.typeTube.TypeTubeRepository;
import ne.mha.sinea.nomenclature.typeUsage.TypeUsageRepository;
import ne.mha.sinea.referentiel.projet.ProjetRepository;
import ne.mha.sinea.referentiel.zone.LocaliteRepository;

@Service
public class ForageExcelService {
	@Autowired
	ForageRepository forageService;
	@Autowired
	EquipementForageRepository equipementForageService;
	@Autowired
	PompeInstalleRepository pompeInstalleService;
	@Autowired
	DescriptionGeologiqueRepository descriptionGeologiqueService;
	@Autowired
	PrestataireRepository prestataireService;
	@Autowired
	TypePrestationRepository typePrestationService;
	@Autowired
	NatureSolRepository natureSolService;
	@Autowired
	ModelePompeRepository modelePompeService;
	@Autowired
	MarquePompeRepository marquePompeService;
	
	@Autowired
	ProprieteRepository proprieteService;
	@Autowired
	LocaliteRepository localiteService;
	@Autowired
	EtatOuvrageRepository etatOuvrageService;
	@Autowired
	TypeAmenagementRepository typeAmenagementService;
	@Autowired
	TypeUsageRepository typeUsageService;
	@Autowired
	FinancementRepository financementService;
	@Autowired
	ProjetRepository projetService;
	@Autowired
	NatureTubeRepository natureTubeService;
	@Autowired
	TypeTubeRepository typeTubeService;
	//définition des libellés de l'entête
	static String[] headerForage = {"", ""};
	
	//placer le contenu du fichier excel dans un objet list
	public  List<Forage> excelToModeleForage(InputStream is) {
		List<Forage> listeForages = new ArrayList<Forage>();
		try {
	    
		  //instancier un classeur Excel
	      Workbook workbook = new XSSFWorkbook(is);
	      //accéder à la première feuille
	      Sheet sheet = workbook.getSheetAt(0);
	      //partcourir les lignes de la feuille
	      Iterator<Row> rows = sheet.iterator();
	      int rowNumber = 0;
	      while (rows.hasNext()) {
	        Row currentRow = rows.next();
	        // ignorer l'entête du fichier Excel lors de l'import
	        if (rowNumber == 0) {
	          rowNumber++;
	          continue;
	        }
	        //verifier que la ligne à importer  n'est pas vide
	        if(!isRowEmpty(currentRow)) {
		        Iterator<Cell> cellsInRow = currentRow.iterator();
		        Forage forage = new Forage();
		        int cellIdx = 0;
		        //parcourir les colonnes d'une ligne
		        while (cellsInRow.hasNext()) {
		          Cell currentCell = cellsInRow.next();
		          
		          switch (cellIdx) {
		          //première colonne
		          case 0: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  forage.setNumeroIRH(currentCell.getStringCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 2:
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  forage.setPropriete(proprieteService.findByIsDeletedFalseAndLibelle(currentCell.getStringCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 3:
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  forage.setLocalite(localiteService.findByCode((int)currentCell.getNumericCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 5:
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  forage.setEtatOuvrage(etatOuvrageService.findByIsDeletedFalseAndLibelle(currentCell.getStringCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 7:
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  forage.setTypeAmenagement(typeAmenagementService.findByIsDeletedFalseAndLibelle(currentCell.getStringCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 8:
		        	 try{ if(!currentCell.toString().isEmpty())
		        		  forage.setTypeUsage(typeUsageService.findByIsDeletedFalseAndLibelle(currentCell.getStringCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 9: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  forage.setAnneeRealisation((Date)currentCell.getDateCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 10: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  forage.setLongitude((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 11: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  forage.setLatitude((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 12:
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  forage.setFinancement(financementService.findByIsDeletedFalseAndLibelle(currentCell.getStringCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 13:
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  forage.setProjet(projetService.findByIsDeletedFalseAndLibelle(currentCell.getStringCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 14: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  forage.setHauteurMargelle((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 15:
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  forage.setAutreInfoMargelle(currentCell.getStringCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 16:
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  forage.setRenseignementDivers(currentCell.getStringCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 17:
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  forage.setDateDemarrageForation((Date)currentCell.getDateCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 18:
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  forage.setDateFinForation((Date)currentCell.getDateCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 19: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  forage.setProfondeurTotaleForee((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 20: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        	  {
		        		  if(currentCell.getStringCellValue() == "1")
		        			  forage.setForagePositif(true);
		        		  else
		        			  forage.setForagePositif(false);
		        	  }
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 21: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  forage.setProfondeurTotaleEquipee((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 22: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  forage.setProfondeurNiveauStatique((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 23:
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  forage.setDateMesure((Date)currentCell.getDateCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 24: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        	  {
		        		  if(currentCell.getStringCellValue() == "1")
		        			  forage.setDeveloppementEffectue(true);
		        		  else
		        			  forage.setDeveloppementEffectue(false);
		        	  }
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 25: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  forage.setDebitAuSoufflage((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 26: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  forage.setEssaiSimplifieDureePompage((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 27: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  forage.setEssaiSimplifieRabattementMesure((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 28: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  forage.setEssaiSimplifieDebitMaximumEstime((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 29: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  forage.setEssaiLongueDureeDureePompage((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 30: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  forage.setEssaiLongueDureeRabattementMesure((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 31: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  forage.setEssaiLongueDureeDebitMaximumEstime((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          }
		          
		          cellIdx++;
		        }
		        //ajouter la ligne récuperer à la liste des région (il s'agit pas encore de la sauvegarde dans la base
		        listeForages.add(forage);
	        }
	      } 

	      workbook.close();
	      
	    } catch (IOException e) {
	    	System.out.println(e);
	    }
		return listeForages;
	  }
	public  List<EquipementForage> excelToModeleEquipementForage(InputStream is) {
		List<EquipementForage> listeEquipementForages = new ArrayList<EquipementForage>();
		try {
	    
		  //instancier un classeur Excel
	      Workbook workbook = new XSSFWorkbook(is);
	      int cellIdx = 0;
	      //accéder à la première feuille
	      Sheet sheet = workbook.getSheetAt(1);
	      //partcourir les lignes de la feuille
	      Iterator<Row> rows = sheet.iterator();
	      int rowNumber = 0;
	      while (rows.hasNext()) {
	        Row currentRow = rows.next();
	        System.out.println("colonne = "+cellIdx);
	        // ignorer l'entête du fichier Excel lors de l'import
	        if (rowNumber == 0) {
	          rowNumber++;
	          continue;
	        }
	        //verifier que la ligne à importer  n'est pas vide
	        if(!isRowEmpty(currentRow)) {
		        Iterator<Cell> cellsInRow = currentRow.iterator();
		        EquipementForage equipement = new EquipementForage();
		        cellIdx = 0;
		        //parcourir les colonnes d'une ligne
		        while (cellsInRow.hasNext()) {
		          Cell currentCell = cellsInRow.next();
		          switch (cellIdx) {
		          //première colonne
		          case 0: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  equipement.setPem(forageService.findByNumeroIRH(currentCell.getStringCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 1: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  equipement.setNumeroTube((int)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 2:
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  equipement.setNatureTube(natureTubeService.findByIsDeletedFalseAndLibelle(currentCell.getStringCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 3:
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  equipement.setTypeTube(typeTubeService.findByIsDeletedFalseAndLibelle(currentCell.getStringCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 4: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  equipement.setCoteSuperieure((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 5: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  equipement.setCoteInferieure((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 6: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  equipement.setDiametre((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          }
		          
		          cellIdx++;
		        }
		        //ajouter la ligne récuperer à la liste des région (il s'agit pas encore de la sauvegarde dans la base
		        listeEquipementForages.add(equipement);
	        }
	      } 

	      workbook.close();
	      
	    } catch (IOException e) {
	    	System.out.println(e);
	    }
		return listeEquipementForages;
	  }
	
	public  List<PompeInstalle> excelToModelePompeInstalle(InputStream is) {
		List<PompeInstalle> listePompeInstalles = new ArrayList<PompeInstalle>();
		try {
	    
		  //instancier un classeur Excel
	      Workbook workbook = new XSSFWorkbook(is);
	      //accéder à la première feuille
	      Sheet sheet = workbook.getSheetAt(2);
	      //partcourir les lignes de la feuille
	      Iterator<Row> rows = sheet.iterator();
	      int rowNumber = 0;
	      while (rows.hasNext()) {
	        Row currentRow = rows.next();
	        // ignorer l'entête du fichier Excel lors de l'import
	        if (rowNumber == 0) {
	          rowNumber++;
	          continue;
	        }
	        //verifier que la ligne à importer  n'est pas vide
	        if(!isRowEmpty(currentRow)) {
		        Iterator<Cell> cellsInRow = currentRow.iterator();
		        PompeInstalle pompe = new PompeInstalle();
		        int cellIdx = 0;
		        //parcourir les colonnes d'une ligne
		        while (cellsInRow.hasNext()) {
		          Cell currentCell = cellsInRow.next();
		          switch (cellIdx) {
		          //première colonne
		          case 0: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  pompe.setPem(forageService.findByNumeroIRH(currentCell.getStringCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 1:
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  pompe.setModelePompe(modelePompeService.findByIsDeletedFalseAndLibelle(currentCell.getStringCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 2:
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  pompe.setMarquePompe(marquePompeService.findByIsDeletedFalseAndLibelle(currentCell.getStringCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 3:
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  pompe.setDateInstallationPompe((Date)currentCell.getDateCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          }
		          
		          cellIdx++;
		        }
		        //ajouter la ligne récuperer à la liste des région (il s'agit pas encore de la sauvegarde dans la base
		        listePompeInstalles.add(pompe);
	        }
	      } 

	      workbook.close();
	      
	    } catch (IOException e) {
	    	System.out.println(e);
	    }
		return listePompeInstalles;
	  }
	
	public  List<DescriptionGeologique> excelToModeleDescriptionGeologique(InputStream is) {
		List<DescriptionGeologique> listeDescriptionGeologiques = new ArrayList<DescriptionGeologique>();
		try {
	    
		  //instancier un classeur Excel
	      Workbook workbook = new XSSFWorkbook(is);
	      //accéder à la première feuille
	      Sheet sheet = workbook.getSheetAt(3);
	      //partcourir les lignes de la feuille
	      Iterator<Row> rows = sheet.iterator();
	      int rowNumber = 0;
	      while (rows.hasNext()) {
	        Row currentRow = rows.next();
	        // ignorer l'entête du fichier Excel lors de l'import
	        if (rowNumber == 0) {
	          rowNumber++;
	          continue;
	        }
	        //verifier que la ligne à importer  n'est pas vide
	        if(!isRowEmpty(currentRow)) {
		        Iterator<Cell> cellsInRow = currentRow.iterator();
		        DescriptionGeologique descriptionGeologique = new DescriptionGeologique();
		        int cellIdx = 0;
		        //parcourir les colonnes d'une ligne
		        while (cellsInRow.hasNext()) {
		          Cell currentCell = cellsInRow.next();
		          DescriptionGeologiqueKey dgk = new DescriptionGeologiqueKey();
		          switch (cellIdx) {
		          //première colonne
		          case 0: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  descriptionGeologique.setPem(forageService.findByNumeroIRH(currentCell.getStringCellValue()));
		        	  	  dgk.setCodePem(forageService.findByNumeroIRH(currentCell.getStringCellValue()).getCode());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 1: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  dgk.setCoteSuperieure((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 2: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  dgk.setCoteInferieure((Double)currentCell.getNumericCellValue());
		        	  	  descriptionGeologique.setDescriptionGeologiqueKey(dgk);
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 3: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  descriptionGeologique.setNatureSol(natureSolService.findByIsDeletedFalseAndLibelle(currentCell.getStringCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          }
		          
		          cellIdx++;
		        }
		        //ajouter la ligne récuperer à la liste des région (il s'agit pas encore de la sauvegarde dans la base
		        listeDescriptionGeologiques.add(descriptionGeologique);
	        }
	      } 

	      workbook.close();
	      
	    } catch (IOException e) {
	    	System.out.println(e);
	    }
		return listeDescriptionGeologiques;
	  }

	public  List<Prestataire> excelToModelePrestataire(InputStream is) {
		List<Prestataire> listePrestataires = new ArrayList<Prestataire>();
		try {
	    
		  //instancier un classeur Excel
	      Workbook workbook = new XSSFWorkbook(is);
	      //accéder à la première feuille
	      Sheet sheet = workbook.getSheetAt(4);
	      //partcourir les lignes de la feuille
	      Iterator<Row> rows = sheet.iterator();
	      int rowNumber = 0;
	      while (rows.hasNext()) {
	        Row currentRow = rows.next();
	        // ignorer l'entête du fichier Excel lors de l'import
	        if (rowNumber == 0) {
	          rowNumber++;
	          continue;
	        }
	        //verifier que la ligne à importer  n'est pas vide
	        if(!isRowEmpty(currentRow)) {
		        Iterator<Cell> cellsInRow = currentRow.iterator();
		        Prestataire prestataire = new Prestataire();
		        int cellIdx = 0;
		        //parcourir les colonnes d'une ligne
		        while (cellsInRow.hasNext()) {
		          Cell currentCell = cellsInRow.next();
		          switch (cellIdx) {
		          //première colonne
		          case 0: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  prestataire.setPem(forageService.findByNumeroIRH(currentCell.getStringCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 1: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  prestataire.setTypePrestation(typePrestationService.findByIsDeletedFalseAndLibelle(currentCell.getStringCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 2:
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  prestataire.setNomPrestataire(currentCell.getStringCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 3: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  prestataire.setCoutPrestation((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          
		          
		          }
		          
		          cellIdx++;
		        }
		        //ajouter la ligne récuperer à la liste des région (il s'agit pas encore de la sauvegarde dans la base
		        listePrestataires.add(prestataire);
	        }
	      } 

	      workbook.close();
	      
	    } catch (IOException e) {
	    	System.out.println(e);
	    }
		return listePrestataires;
	  }

	
	//sauvegarde des informations du fichier excel dans la base de données
	public List<String> importData(MultipartFile file) {
		List<String> unsaved = new ArrayList<String>();
		try {
	      //récupération des lignes Excel dans un objet List
	      List<Forage> listeForages = excelToModeleForage(file.getInputStream());
	      List<EquipementForage> listeEquipementForages = excelToModeleEquipementForage(file.getInputStream());
	      List<PompeInstalle> listePompeInstalles = excelToModelePompeInstalle(file.getInputStream());
	      List<DescriptionGeologique> listeDescriptionGeologiques = excelToModeleDescriptionGeologique(file.getInputStream());
	      List<Prestataire> listePrestataires = excelToModelePrestataire(file.getInputStream());
	      
	      //sauvegarde des informations récuperés du fichier excel dans la base de données
	      for(int i=0;i<listeForages.size();i++)
	      {
	    	  try {
	    		  forageService.save(listeForages.get(i));
		    	  
	    	  }catch(Exception e)
	    	  {
	    		  unsaved.add("Forage N°IRH "+listeForages.get(i).getNumeroIRH()); 
	    	  }
	    	  
	      }
	      //forageService.saveAll(listeForages);
	      equipementForageService.saveAll(listeEquipementForages);
	      pompeInstalleService.saveAll(listePompeInstalles);
	      descriptionGeologiqueService.saveAll(listeDescriptionGeologiques);
	      prestataireService.saveAll(listePrestataires);
		    
	    } catch (IOException e) {
	    	System.out.println(e);
	     
	    }
		return unsaved;
	  }
	
	//mettre le les informations dans un objet ByteArrayInputStream en vue de la création du fichier excel
	 public  ByteArrayInputStream modelePompeToExcel(List<Forage> forages) {

		    try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) {
		      Sheet sheet = workbook.createSheet("Forages");
		      // création de l'entête du fichier excel
		      Row headerRow = sheet.createRow(0);
		      //création des colonnes de l'entête
		      for (int col = 0; col < headerForage.length; col++) {
		        Cell cell = headerRow.createCell(col);
		        cell.setCellValue(headerForage[col]);
		      }

		      int rowIdx = 1;
		      //création des lignes du fichier
		      for (Forage forage : forages) {
		        Row row = sheet.createRow(rowIdx++);
		        //colonne 1 / cellule
		        //row.createCell(0).setCellValue(modelePompe.getCode());
		      //colonne 2 / cellule
		        //row.createCell(1).setCellValue(modelePompe.getLibelle());
		      }

		      workbook.write(out);
		      return new ByteArrayInputStream(out.toByteArray());
		      
		    } catch (IOException e) {
		      throw new RuntimeException(e.getMessage());
		    }
		  }
	 
	 //récuperaton des données de la base pour les convertir en ByteArrayInputStream
	 public ByteArrayInputStream  exportData() {  
	    	List<Forage> modelePompex = (List<Forage>) forageService.findAll();
	    	ByteArrayInputStream in = modelePompeToExcel(modelePompex);
	    	return in;
		    
		  }
	 
	 //vérifier si une ligne est vide
	 private  boolean isRowEmpty(Row row) {
			boolean isEmpty = true;
			DataFormatter dataFormatter = new DataFormatter();
			if (row != null) {
				for (Cell cell : row) {
					if (dataFormatter.formatCellValue(cell).trim().length() > 0) {
						isEmpty = false;
						break;
					}
				}
			}

			return isEmpty;
		}
		
	
}
