package ne.mha.sinea.pem;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PrestataireForm {

	private String nomPrestataire;
	private Double coutPrestation;
	private Integer codeTypePrestation;
}
