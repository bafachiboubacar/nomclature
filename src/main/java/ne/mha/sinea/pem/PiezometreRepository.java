package ne.mha.sinea.pem;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface PiezometreRepository extends CrudRepository<Piezometre, Integer> {
	
	Piezometre findByCode(Integer code);
	List<Piezometre> findByIsDeletedFalse();
	
}