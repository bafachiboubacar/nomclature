package ne.mha.sinea.pem;

//import java.sql.Date;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ForageForm {

	private String numeroIRH;
	private Date anneeRealisation;
	private Double latitude;
	private Double longitude;
	private Integer codeFinancement;
	private Integer codeProjet;
	private Integer codeZone;
	
	private Double hauteurMargelle;
	private String autreInfoMargelle;
	private String renseignementDivers;
	private Date dateDemarrageForation;
	private Date dateFinForation;
	private Double profondeurTotaleForee;
	private boolean foragePositif;
	private Double profondeurTotaleEquipee;
	private Double profondeurNiveauStatique;
	private Date dateMesure;
	private boolean developpementEffectue;
	private Double debitAuSoufflage;
	private Double essaiSimplifie_DureePompage;
	private Double essaiSimplifie_RabattementMesure;
	private Double essaiSimplifie_DebitMaximumEstime;
	private Double essaiLongueDuree_DureePompage;
	private Double essaiLongueDuree_RabattementMesure;
	private Double essaiLongueDuree_DebitMaximumEstime;
	private Integer codeTypeUsage;
	private Integer codePropriete;
	private Integer codeTypeAmenagement;
	private Integer codeModelePompe;
	private Integer codeMarquePompe;
	private Date dateInstallationPompe;
}
