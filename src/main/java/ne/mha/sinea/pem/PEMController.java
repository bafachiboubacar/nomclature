package ne.mha.sinea.pem;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import ne.mha.sinea.Template;
import ne.mha.sinea.nomenclature.typeOuvrage.TypeOuvrage;
import ne.mha.sinea.nomenclature.typeOuvrage.TypeOuvrageRepository;
import ne.mha.sinea.referentiel.zone.Zone;
import ne.mha.sinea.referentiel.zone.ZoneRepository;
import ne.mha.sinea.systemeAEP.SystemeAEPRepository;

@Controller
public class PEMController {

	@Autowired
	ZoneRepository zoneService;
	
	@Autowired
	TypeOuvrageRepository typeOuvrageService;
	
	@Autowired
	SystemeAEPRepository systemeAEPService;
	
	//@PreAuthorize("hasAuthority('gestion des types ressources')")
	@GetMapping("/consultation")
	public String  selectVillage(Model model) {
		try{
			List<Zone> regions = zoneService.findByIsDeletedFalseAndNiveau(1);
			model.addAttribute("regions", regions);
			List<TypeOuvrage> typeOuvrages = typeOuvrageService.findByIsDeletedFalse();
			model.addAttribute("typeOuvrages", typeOuvrages);
			ArrayList<Ouvrage> ouvrages = new ArrayList<Ouvrage>(); 
			
			List<Object[]> listeOuvrage = systemeAEPService.ListOuvrage();
			for (Object[] obj : listeOuvrage)
	        {
				Ouvrage o = new Ouvrage();
				o.setNomOuvrage((String) obj[0]);
				o.setTypeOuvrage((String)obj[1]);
				o.setLatitude((String)obj[2]);
				o.setLongitude((String)obj[3]);
				o.setLien((String)obj[4]);
				ouvrages.add(o);
			}
			model.addAttribute("ouvrages", ouvrages);
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Point d'Eau Moderne");
			model.addAttribute("viewPath", "pem/consultation");
		
		
		}
		catch(Exception e){
				
			}
		return Template.defaultTemplate;
	}
	
	
}
