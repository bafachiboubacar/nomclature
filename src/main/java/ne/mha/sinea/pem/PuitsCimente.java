package ne.mha.sinea.pem;

//import java.sql.Date;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import ne.mha.sinea.nomenclature.etatOuvrage.EtatOuvrage;
import ne.mha.sinea.nomenclature.typeExhaure.TypeExhaure;
import ne.mha.sinea.nomenclature.typePuits.TypePuits;

@Data
@Entity
@Table(name = "puits_cimente")
public class PuitsCimente extends PEM {
	
	@JoinColumn(name = "code_type_puits", referencedColumnName = "code")
	@ManyToOne
	private TypePuits typePuits;
	
	
	@Column(name = "amenagement_surface")
	private Boolean amenagementSurface;
	@Column(name = "antibourbier")
	private Boolean antibourbier;
	@Column(name = "canal_evacuation_et_puits_perdu")
	private Boolean canalEvacuationEtPuitsPerdu;
	@Column(name = "aire_assainie")
	private Boolean aireAssainie;
	@Column(name = "mur_cloture")
	private Boolean murCloture;
	@Column(name = "couvercle")
	private Boolean couvercle;
	@Column(name = "puisette")
	private Boolean puisette;
	@Column(name = "fourche")
	private Boolean fourche;
	@Column(name = "developpement_effectue_au_cuffat")
	private Boolean developpementEffectueAuCuffat;
	@Column(name = "couleur_eau")
	private Boolean couleurEau;
	@Column(name = "trousse_coupante")
	private Boolean trousseCoupante;
	@Column(name = "dalle_fond")
	private Boolean dalleFond;
	@Column(name = "pompe_immergee")
	private Boolean pompeImmergee;
	
	@Column(name = "date_mesure")
	private Date dateMesure;
	@JoinColumn(name = "code_type_exhaure", referencedColumnName = "code")
	@ManyToOne
	private TypeExhaure typeExhaure;
	
	@Column(name = "potabilite_eau")
	private Boolean potabiliteEau;
	
	
	@Column(name = "escieh_duree_pompage")
	private Double ESCIEHDureePompage;
	@Column(name = "escieh_rabattement_mesure")
	private Double ESCIEHRabattementMesure;
	@Column(name = "escieh_debit_maximum_estime")
	private Double ESCIEHDebitMaximumEstime;
	@Column(name = "abreuvoirs")
	private Integer abreuvoirs;
	@Column(name = "portique")
	private Integer portique;
	@Column(name = "profondeur_totale")
	private Double profondeurTotale;
	@Column(name = "profondeur_cuvelee")
	private Double profondeurCuvelee;
	@Column(name = "diametre_cuvelage")
	private Double diametreCuvelage;
	@Column(name = "profondeur_niveau_statique")
	private Double profondeurNiveauStatique;
	@Column(name = "diametre_captage")
	private Double diametreCaptage;
	@Column(name = "nombre_buses_captage")
	private Integer nombreBusesCaptage;
	@Column(name = "profondeur_sommet_colonne_captage")
	private Double profondeurSommetColonneCaptage;
	@Column(name = "hauteur_captage")
	private Double hauteurCaptage;
	@Column(name = "hauteur_recouvrement")
	private Double hauteurRecouvrement;
	@Column(name = "hauteur_massif_filtrant")
	private Double hauteurMassifFiltrant;
	@Column(name = "renseignement_divers")
	private String renseignementDivers;
	
	
}
