package ne.mha.sinea.pem;


import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ne.mha.sinea.CommonProperties;
import ne.mha.sinea.nomenclature.natureSol.NatureSol;

//annotation de lombok pour générer les getter et setter
@Data
//annotation de lombok pour générer le constructeur sans paramètre
@NoArgsConstructor
//annotation de lombok pour générer le constructeur avec tous les paramètres
@AllArgsConstructor
//indique qu'il s'agit d'une entité=> création d'une table au niveau de la base de données

@Entity
@Table(name = "description_geologique")
//@IdClass(DescriptionGeologiqueKey.class)
public class DescriptionGeologique extends CommonProperties{

	@EmbeddedId
	private DescriptionGeologiqueKey descriptionGeologiqueKey;
	
	@ManyToOne
	@MapsId("code_pem")
	@JoinColumn(name="code_pem")
	private PEM pem;
	@ManyToOne
	@JoinColumn(name = "code_nature_sol", referencedColumnName = "code")
	private NatureSol natureSol;

}
