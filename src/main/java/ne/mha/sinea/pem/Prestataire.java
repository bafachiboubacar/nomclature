package ne.mha.sinea.pem;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import ne.mha.sinea.CommonProperties;
import ne.mha.sinea.nomenclature.typePrestation.TypePrestation;

@Data
@Entity
@Table(name = "prestataire")
public class Prestataire extends CommonProperties{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "code")
	private Integer code;
	@ManyToOne
	@JoinColumn(name = "code_pem", referencedColumnName = "code")
	private PEM pem;
	@Column(name = "nom_prestataire")
	private String nomPrestataire;
	@Column(name = "cout_prestation")
	private Double coutPrestation;
	@ManyToOne
	@JoinColumn(name = "code_type_prestation", referencedColumnName = "code")
	private TypePrestation typePrestation;

}
