package ne.mha.sinea.pem;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface PrestataireRepository extends CrudRepository<Prestataire, Integer> {
	
	Prestataire findByCode(Integer code);
	List<Prestataire> findByIsDeletedFalse();
	List<Prestataire> findByPem_CodeAndIsDeletedFalse(Integer code);
	
	
}