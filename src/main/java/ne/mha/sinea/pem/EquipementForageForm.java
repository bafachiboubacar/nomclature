package ne.mha.sinea.pem;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EquipementForageForm {
	private Integer numeroTube;
	private Double coteSuperieure;
	private Double coteInferieure;
	private Integer codeNatureTube;
	private Integer codeTypeTube;
	private Double diametre;
}
