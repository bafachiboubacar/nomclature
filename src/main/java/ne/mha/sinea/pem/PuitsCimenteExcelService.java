package ne.mha.sinea.pem;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
//import java.sql.Date;
import java.util.Date;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ne.mha.sinea.nomenclature.etatOuvrage.EtatOuvrageRepository;
import ne.mha.sinea.nomenclature.financement.FinancementRepository;
import ne.mha.sinea.nomenclature.natureSol.NatureSolRepository;
import ne.mha.sinea.nomenclature.propriete.ProprieteRepository;
import ne.mha.sinea.nomenclature.typeExhaure.TypeExhaureRepository;
import ne.mha.sinea.nomenclature.typePrestation.TypePrestationRepository;
import ne.mha.sinea.nomenclature.typePuits.TypePuitsRepository;
import ne.mha.sinea.referentiel.projet.ProjetRepository;
import ne.mha.sinea.referentiel.zone.LocaliteRepository;

@Service
public class PuitsCimenteExcelService {
	@Autowired
	PuitsCimenteRepository puitsService;
	@Autowired
	TypePuitsRepository typePuitsService;
	
	@Autowired
	TypeExhaureRepository typeExhaureService;
	@Autowired
	DescriptionGeologiqueRepository descriptionGeologiqueService;
	@Autowired
	PrestataireRepository prestataireService;
	@Autowired
	TypePrestationRepository typePrestationService;
	@Autowired
	NatureSolRepository natureSolService;
	@Autowired
	ProprieteRepository proprieteService;
	@Autowired
	LocaliteRepository localiteService;
	@Autowired
	EtatOuvrageRepository etatOuvrageService;
	@Autowired
	FinancementRepository financementService;
	@Autowired
	ProjetRepository projetService;
	
	//définition des libellés de l'entête
	static String[] headerPuitsCimente = {"", ""};
	
	//placer le contenu du fichier excel dans un objet list
	public  List<PuitsCimente> excelToModelePuitsCimente(InputStream is) {
		List<PuitsCimente> listePuitsCimentes = new ArrayList<PuitsCimente>();
		try {
	    
		  //instancier un classeur Excel
	      Workbook workbook = new XSSFWorkbook(is);
	      //accéder à la première feuille
	      Sheet sheet = workbook.getSheetAt(0);
	      //partcourir les lignes de la feuille
	      Iterator<Row> rows = sheet.iterator();
	      int rowNumber = 0;
	      while (rows.hasNext()) {
	        Row currentRow = rows.next();
	        // ignorer l'entête du fichier Excel lors de l'import
	        if (rowNumber == 0) {
	          rowNumber++;
	          continue;
	        }
	        //verifier que la ligne à importer  n'est pas vide
	        if(!isRowEmpty(currentRow)) {
		        Iterator<Cell> cellsInRow = currentRow.iterator();
		        PuitsCimente puits = new PuitsCimente();
		        int cellIdx = 0;
		        //parcourir les colonnes d'une ligne
		        while (cellsInRow.hasNext()) {
		          Cell currentCell = cellsInRow.next();
		          switch (cellIdx) {
		          //première colonne
		          case 0: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  puits.setNumeroIRH(currentCell.getStringCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 2:
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  puits.setLocalite(localiteService.findByCode((int)currentCell.getNumericCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 4:
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  puits.setPropriete(proprieteService.findByIsDeletedFalseAndLibelle(currentCell.getStringCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 5:
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  puits.setEtatOuvrage(etatOuvrageService.findByIsDeletedFalseAndLibelle(currentCell.getStringCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 7: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  puits.setAnneeRealisation((Date)currentCell.getDateCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 8: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  puits.setLongitude((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 9: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  puits.setLatitude((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 10:
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  puits.setFinancement(financementService.findByIsDeletedFalseAndLibelle(currentCell.getStringCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 11:
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  puits.setProjet(projetService.findByIsDeletedFalseAndLibelle(currentCell.getStringCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 12: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  puits.setHauteurMargelle((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 13:
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  puits.setAutreInfoMargelle(currentCell.getStringCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          
		          case 14:
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  	if(currentCell.getStringCellValue() == "1")
		        		  		puits.setTypePuits(typePuitsService.findByCode(1));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          
		          case 15:
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  	if(currentCell.getStringCellValue() == "1")
		        		  		puits.setTypePuits(typePuitsService.findByCode(2));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          
		          case 16: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        	  {
		        		  if(currentCell.getStringCellValue() == "1")
		        			  puits.setAmenagementSurface(true);
		        		  else
		        			  puits.setAmenagementSurface(false);
		        	  }
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 17: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        	  {
		        		  if(currentCell.getStringCellValue() == "1")
		        			  puits.setAntibourbier(true);
		        		  else
		        			  puits.setAntibourbier(false);
		        	  }
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 18: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        	  {
		        		  if(currentCell.getStringCellValue() == "1")
		        			  puits.setCanalEvacuationEtPuitsPerdu(true);
		        		  else
		        			  puits.setCanalEvacuationEtPuitsPerdu(false);
		        	  }
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 19: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        	  {
		        		  if(currentCell.getStringCellValue() == "1")
		        			  puits.setAireAssainie(true);
		        		  else
		        			  puits.setAireAssainie(false);
		        	  }
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 20: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        	  {
		        		  if(currentCell.getStringCellValue() == "1")
		        			  puits.setMurCloture(true);
		        		  else
		        			  puits.setMurCloture(false);
		        	  }
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 21: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  puits.setAbreuvoirs((int)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 22: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        	  {
		        		  if(currentCell.getStringCellValue() == "1")
		        			  puits.setCouvercle(true);
		        		  else
		        			  puits.setCouvercle(false);
		        	  }
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 23:
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  puits.setTypeExhaure(typeExhaureService.findByIsDeletedFalseAndLibelle(currentCell.getStringCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 24: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        	  {
		        		  if(currentCell.getStringCellValue() == "1")
		        			  puits.setPuisette(true);
		        		  else
		        			  puits.setPuisette(false);
		        	  }
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 25: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        	  {
		        		  if(currentCell.getStringCellValue() == "1")
		        			  puits.setFourche(true);
		        		  else
		        			  puits.setFourche(false);
		        	  }
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 26: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        	  {
		        		  if(currentCell.getStringCellValue() == "1")
		        			  puits.setDeveloppementEffectueAuCuffat(true);
		        		  else
		        			  puits.setDeveloppementEffectueAuCuffat(false);
		        	  }
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 27: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        	  {
		        		  if(currentCell.getStringCellValue() == "1")
		        			  puits.setCouleurEau(true);
		        		  else
		        			  puits.setCouleurEau(false);
		        	  }
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 28: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  puits.setESCIEHDureePompage((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 29: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  puits.setESCIEHRabattementMesure((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 30: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  puits.setESCIEHDebitMaximumEstime((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 31: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  puits.setPortique((int)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 32: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        	  {
		        		  if(currentCell.getStringCellValue() == "1")
		        			  puits.setPompeImmergee(true);
		        		  else
		        			  puits.setPompeImmergee(false);
		        	  }
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 33:
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  puits.setRenseignementDivers(currentCell.getStringCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 34: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  puits.setProfondeurTotale((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 35: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  puits.setProfondeurCuvelee((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 36: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  puits.setDiametreCuvelage((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 37: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  puits.setProfondeurNiveauStatique((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 38: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  puits.setDiametreCaptage((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 39: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  puits.setNombreBusesCaptage((int)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 40: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  puits.setProfondeurSommetColonneCaptage((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 41: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  puits.setHauteurCaptage((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 42: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  puits.setHauteurRecouvrement((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 43: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  puits.setHauteurMassifFiltrant((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 44: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        	  {
		        		  if(currentCell.getStringCellValue() == "1")
		        			  puits.setTrousseCoupante(true);
		        		  else
		        			  puits.setTrousseCoupante(false);
		        	  }
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 45: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        	  {
		        		  if(currentCell.getStringCellValue() == "1")
		        			  puits.setDalleFond(true);
		        		  else
		        			  puits.setDalleFond(false);
		        	  }
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 46:
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  puits.setDateMesure((Date)currentCell.getDateCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		         
		          }
		          
		          cellIdx++;
		        }
		        //ajouter la ligne récuperer à la liste des région (il s'agit pas encore de la sauvegarde dans la base
		        listePuitsCimentes.add(puits);
	        }
	      } 

	      workbook.close();
	      
	    } catch (IOException e) {
	    	System.out.println(e);
	    }
		return listePuitsCimentes;
	  }
		public  List<DescriptionGeologique> excelToModeleDescriptionGeologique(InputStream is) {
		List<DescriptionGeologique> listeDescriptionGeologiques = new ArrayList<DescriptionGeologique>();
		try {
	    
		  //instancier un classeur Excel
	      Workbook workbook = new XSSFWorkbook(is);
	      //accéder à la première feuille
	      Sheet sheet = workbook.getSheetAt(3);
	      //partcourir les lignes de la feuille
	      Iterator<Row> rows = sheet.iterator();
	      int rowNumber = 0;
	      while (rows.hasNext()) {
	        Row currentRow = rows.next();
	        // ignorer l'entête du fichier Excel lors de l'import
	        if (rowNumber == 0) {
	          rowNumber++;
	          continue;
	        }
	        //verifier que la ligne à importer  n'est pas vide
	        if(!isRowEmpty(currentRow)) {
		        Iterator<Cell> cellsInRow = currentRow.iterator();
		        DescriptionGeologique descriptionGeologique = new DescriptionGeologique();
		        int cellIdx = 0;
		        //parcourir les colonnes d'une ligne
		        while (cellsInRow.hasNext()) {
		          Cell currentCell = cellsInRow.next();
		          DescriptionGeologiqueKey dgk = new DescriptionGeologiqueKey();
		          switch (cellIdx) {
		          //première colonne
		          case 0: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  descriptionGeologique.setPem(puitsService.findByNumeroIRH(currentCell.getStringCellValue()));
		        	  	  dgk.setCodePem(puitsService.findByNumeroIRH(currentCell.getStringCellValue()).getCode());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 1: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  dgk.setCoteSuperieure((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 2: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  dgk.setCoteInferieure((Double)currentCell.getNumericCellValue());
		        	  	  descriptionGeologique.setDescriptionGeologiqueKey(dgk);
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 3: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  descriptionGeologique.setNatureSol(natureSolService.findByIsDeletedFalseAndLibelle(currentCell.getStringCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          }
		          
		          cellIdx++;
		        }
		        //ajouter la ligne récuperer à la liste des région (il s'agit pas encore de la sauvegarde dans la base
		        listeDescriptionGeologiques.add(descriptionGeologique);
	        }
	      } 

	      workbook.close();
	      
	    } catch (IOException e) {
	    	System.out.println(e);
	    }
		return listeDescriptionGeologiques;
	  }

	public  List<Prestataire> excelToModelePrestataire(InputStream is) {
		List<Prestataire> listePrestataires = new ArrayList<Prestataire>();
		try {
	    
		  //instancier un classeur Excel
	      Workbook workbook = new XSSFWorkbook(is);
	      //accéder à la première feuille
	      Sheet sheet = workbook.getSheetAt(3);
	      //partcourir les lignes de la feuille
	      Iterator<Row> rows = sheet.iterator();
	      int rowNumber = 0;
	      while (rows.hasNext()) {
	        Row currentRow = rows.next();
	        // ignorer l'entête du fichier Excel lors de l'import
	        if (rowNumber == 0) {
	          rowNumber++;
	          continue;
	        }
	        //verifier que la ligne à importer  n'est pas vide
	        if(!isRowEmpty(currentRow)) {
		        Iterator<Cell> cellsInRow = currentRow.iterator();
		        Prestataire prestataire = new Prestataire();
		        int cellIdx = 0;
		        //parcourir les colonnes d'une ligne
		        while (cellsInRow.hasNext()) {
		          Cell currentCell = cellsInRow.next();
		          switch (cellIdx) {
		          //première colonne
		          case 0: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  prestataire.setPem(puitsService.findByNumeroIRH(currentCell.getStringCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 1: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  prestataire.setTypePrestation(typePrestationService.findByIsDeletedFalseAndLibelle(currentCell.getStringCellValue()));
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 2:
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  prestataire.setNomPrestataire(currentCell.getStringCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          case 3: 
		        	  try{ if(!currentCell.toString().isEmpty())
		        		  prestataire.setCoutPrestation((Double)currentCell.getNumericCellValue());
		        	  break;}catch(Exception e) { System.out.println(e);}
		          
		          
		          }
		          
		          cellIdx++;
		        }
		        //ajouter la ligne récuperer à la liste des région (il s'agit pas encore de la sauvegarde dans la base
		        listePrestataires.add(prestataire);
	        }
	      } 

	      workbook.close();
	      
	    } catch (IOException e) {
	    	System.out.println(e);
	    }
		return listePrestataires;
	  }

	
	//sauvegarde des informations du fichier excel dans la base de données
	public List<String> importData(MultipartFile file) {
		List<String> unsaved = new ArrayList<String>();
		try {
	      //récupération des lignes Excel dans un objet List
	      List<PuitsCimente> listePuitsCimentes = excelToModelePuitsCimente(file.getInputStream());
	      List<DescriptionGeologique> listeDescriptionGeologiques = excelToModeleDescriptionGeologique(file.getInputStream());
	      List<Prestataire> listePrestataires = excelToModelePrestataire(file.getInputStream());
	      
	      //sauvegarde des informations récuperés du fichier excel dans la base de données
	      for(int i=0;i<listePuitsCimentes.size();i++)
	      {
	    	  try {
	    		  puitsService.save(listePuitsCimentes.get(i));
		    	  
	    	  }catch(Exception e)
	    	  {
	    		  unsaved.add("Puits Cimenté N°IRH "+listePuitsCimentes.get(i).getNumeroIRH()); 
	    	  }
	    	  
	      }
	      //puitsService.saveAll(listePuitsCimentes);
	      descriptionGeologiqueService.saveAll(listeDescriptionGeologiques);
	      prestataireService.saveAll(listePrestataires);
		    
	    } catch (IOException e) {
	    	System.out.println(e);
	     
	    }
		return unsaved;
	  }
	
	//mettre le les informations dans un objet ByteArrayInputStream en vue de la création du fichier excel
	 public  ByteArrayInputStream modelePompeToExcel(List<PuitsCimente> puitss) {

		    try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) {
		      Sheet sheet = workbook.createSheet("PuitsCimentes");
		      // création de l'entête du fichier excel
		      Row headerRow = sheet.createRow(0);
		      //création des colonnes de l'entête
		      for (int col = 0; col < headerPuitsCimente.length; col++) {
		        Cell cell = headerRow.createCell(col);
		        cell.setCellValue(headerPuitsCimente[col]);
		      }

		      int rowIdx = 1;
		      //création des lignes du fichier
		      for (PuitsCimente puits : puitss) {
		        Row row = sheet.createRow(rowIdx++);
		        //colonne 1 / cellule
		        //row.createCell(0).setCellValue(modelePompe.getCode());
		      //colonne 2 / cellule
		        //row.createCell(1).setCellValue(modelePompe.getLibelle());
		      }

		      workbook.write(out);
		      return new ByteArrayInputStream(out.toByteArray());
		      
		    } catch (IOException e) {
		      throw new RuntimeException(e.getMessage());
		    }
		  }
	 
	 //récuperaton des données de la base pour les convertir en ByteArrayInputStream
	 public ByteArrayInputStream  exportData() {  
	    	List<PuitsCimente> modelePompex = (List<PuitsCimente>) puitsService.findAll();
	    	ByteArrayInputStream in = modelePompeToExcel(modelePompex);
	    	return in;
		    
		  }
	 
	 //vérifier si une ligne est vide
	 private  boolean isRowEmpty(Row row) {
			boolean isEmpty = true;
			DataFormatter dataFormatter = new DataFormatter();
			if (row != null) {
				for (Cell cell : row) {
					if (dataFormatter.formatCellValue(cell).trim().length() > 0) {
						isEmpty = false;
						break;
					}
				}
			}

			return isEmpty;
		}
		
	
}
