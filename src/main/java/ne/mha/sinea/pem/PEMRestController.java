package ne.mha.sinea.pem;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import ne.mha.sinea.nomenclature.etatOuvrage.EtatOuvrageRepository;
import ne.mha.sinea.nomenclature.financement.FinancementRepository;
import ne.mha.sinea.nomenclature.marquePompe.MarquePompeRepository;
import ne.mha.sinea.nomenclature.modelePompe.ModelePompeRepository;
import ne.mha.sinea.nomenclature.natureSol.NatureSolRepository;
import ne.mha.sinea.nomenclature.natureTube.NatureTubeRepository;
import ne.mha.sinea.nomenclature.propriete.ProprieteRepository;
import ne.mha.sinea.nomenclature.typeAmenagement.TypeAmenagementRepository;
import ne.mha.sinea.nomenclature.typeExhaure.TypeExhaureRepository;
import ne.mha.sinea.nomenclature.typePrestation.TypePrestationRepository;
import ne.mha.sinea.nomenclature.typePuits.TypePuitsRepository;
import ne.mha.sinea.nomenclature.typeTube.TypeTubeRepository;
import ne.mha.sinea.nomenclature.typeUsage.TypeUsageRepository;
import ne.mha.sinea.referentiel.projet.ProjetRepository;
import ne.mha.sinea.referentiel.zone.Localite;
import ne.mha.sinea.referentiel.zone.LocaliteRepository;
import ne.mha.sinea.referentiel.zone.Zone;
import ne.mha.sinea.referentiel.zone.ZoneRepository;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class PEMRestController {

	@Autowired
	ZoneRepository zoneService;
	
	@Autowired
	LocaliteRepository localiteService;
	@Autowired
	TypePuitsRepository typePuitsService;
	
	@Autowired
	ForageRepository forageService;
	
	@Autowired
	PuitsCimenteRepository puitsCimenteService;
	@Autowired
	PompeInstalleRepository pompeInstalleService;
	@Autowired
	FinancementRepository financementService;
	@Autowired
	ProjetRepository projetService;
	@Autowired
	TypeUsageRepository typeUsageService;
	@Autowired
	TypeAmenagementRepository typeAmenagementService;
	@Autowired
	ModelePompeRepository modelePompeService;
	@Autowired
	MarquePompeRepository marquePompeService;
	@Autowired
	ProprieteRepository proprieteService;
	@Autowired
	NatureSolRepository natureSolService;
	@Autowired
	NatureTubeRepository natureTubeService;
	@Autowired
	TypeTubeRepository typeTubeService;
	@Autowired
	TypeExhaureRepository typeExhaureService;
	@Autowired
	TypePrestationRepository typePrestationService;
	@Autowired
	PrestataireRepository prestataireService;
	@Autowired
	EquipementForageRepository equipementForageService;
	@Autowired
	DescriptionGeologiqueRepository descriptionGeologiqueService;
	@Autowired
	PiezometreRepository piezometreService;
	@Autowired
	EtatOuvrageRepository etatOuvrageService;
	
	@GetMapping("/getZoneByParentCode/{code}")
	public String  getZoneByParentCode(@PathVariable("code") int code) {
		String res = "";
		try{
			
			List<Zone> zones = zoneService.findByIsDeletedFalseAndParent_code(code);
			for(int i=0;i<zones.size();i++)
			{
				res += "<option value=\""+zones.get(i).getCode()+"\">"+zones.get(i).getLibelle()+"</option>";
			}
			
			
		
		}
		catch(Exception e){
				
			}
		return res;
	}
	
	@GetMapping("/getLocaliteByParentCode/{code}")
	public String  getLocaliteByParentCode(@PathVariable("code") int code) {
		String res = "";
		try{
			
			List<Localite> localites = localiteService.findByIsDeletedFalseAndCommune_Code(code);
			if(localites.size() >0)
			{
				for(int i=0;i<localites.size();i++)
				{
					res += "<option value=\""+localites.get(i).getCode()+"\">"+localites.get(i).getLibelle()+"</option>";
				}
			}
			else
			{
				res += "<option value=\"\">Vide</option>";
			}
			
			
			
		
		}
		catch(Exception e){
				
			}
		return res;
	}
	
	@PostMapping("/addForage")
	public int addForage(@Validated InitPEMForm pem,BindingResult bindingResult) {
		Forage savedForage = new Forage();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					Forage F = new Forage();
					F.setNumeroIRH(pem.getNumeroIRH());
					F.setNomPEM(pem.getNomPEM());
					savedForage = forageService.save(F);
					
				}
			catch(Exception e){
				
					System.out.println(e);
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
    			System.out.println(e);
				}
    	}
		
		return savedForage.getCode();
		
        
    }
	
	@PostMapping("/addPuits")
	public int addPuits(@Validated InitPEMForm pem,BindingResult bindingResult) {
		PuitsCimente savedPuitsCimente = new PuitsCimente();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					PuitsCimente P = new PuitsCimente();
					P.setNumeroIRH(pem.getNumeroIRH());
					P.setNomPEM(pem.getNomPEM());
					savedPuitsCimente = puitsCimenteService.save(P);
					
				}
			catch(Exception e){
				
					System.out.println(e);
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
    			System.out.println(e);
				}
    	}
		
		return savedPuitsCimente.getCode();
		
        
    }
	
	@PostMapping("/forageInfosGenerales/{code}")
	public int infosGeneralesForage(@PathVariable("code") int code,@Validated ForageInfosGeneralesForm forage,BindingResult bindingResult) {
		//if (!bindingResult.hasErrors()) {
		if (true) {	
			try {
					Forage F = forageService.findByCode(code);
					F.setNumeroIRH(forage.getNumeroIRH());
					F.setAnneeRealisation(forage.getAnneeRealisation());
					F.setLatitude(forage.getLatitude());
					F.setLongitude(forage.getLongitude());
					F.setFinancement(financementService.findByCode(forage.getCodeFinancement()));
					F.setProjet(projetService.findByCode(forage.getCodeProjet()));
					F.setLocalite(localiteService.findByCode(forage.getCodeLocalite()));
					F.setForagePositif(forage.isForagePositif());
					F.setDateDemarrageForation(forage.getDateDemarrageForation());
					F.setDateFinForation(forage.getDateFinForation());
					F.setProfondeurTotaleForee(forage.getProfondeurTotaleForee());
					F.setProfondeurTotaleEquipee(forage.getProfondeurTotaleEquipee());
					F.setProfondeurNiveauStatique(forage.getProfondeurNiveauStatique());
					F.setDateMesure(forage.getDateMesure());
					F.setHauteurMargelle(forage.getHauteurMargelle());
					F.setAutreInfoMargelle(forage.getAutreInfoMargelle());
					F.setDeveloppementEffectue(forage.isDeveloppementEffectue());
					F.setDebitAuSoufflage(forage.getDebitAuSoufflage());
					F.setEssaiSimplifieDureePompage(forage.getEssaiSimplifieDureePompage());
					F.setEssaiSimplifieRabattementMesure(forage.getEssaiSimplifieRabattementMesure());
					F.setEssaiSimplifieDebitMaximumEstime(forage.getEssaiSimplifieDebitMaximumEstime());
					F.setEssaiLongueDureeDureePompage(forage.getEssaiLongueDureeDureePompage());
					F.setEssaiLongueDureeRabattementMesure(forage.getEssaiLongueDureeRabattementMesure());
					F.setEssaiLongueDureeDebitMaximumEstime(forage.getEssaiLongueDureeDebitMaximumEstime());
					F.setTypeUsage(typeUsageService.findByCode(forage.getCodeTypeUsage()));
					F.setPropriete(proprieteService.findByCode(forage.getCodePropriete()));
					F.setTypeAmenagement(typeAmenagementService.findByCode(forage.getCodeTypeAmenagement()));
					
					F.setRenseignementDivers(forage.getRenseignementDivers());
					forageService.save(F);
					
				}
			catch(Exception e){
				
					System.out.println(e);
				}
			
    	}
		
		
		return 1;
        
    }
	
	@PostMapping("/forageDescriptionGeologique/{code}")
	public String forageDescriptionGeologique(@PathVariable("code") int code,@Validated DescriptionGeologiqueForm descriptionGeologique,BindingResult bindingResult) {
		String res = "";
		//if (!bindingResult.hasErrors()) {
		if (true) {	
			try {
					Forage pem = forageService.findByCode(code);
					DescriptionGeologique dg = new DescriptionGeologique();
					dg.setDescriptionGeologiqueKey(new DescriptionGeologiqueKey(code, descriptionGeologique.getCoteSuperieure(), descriptionGeologique.getCoteInferieure()));
					dg.setPem(pem);
					dg.setNatureSol(natureSolService.findByCode(descriptionGeologique.getCodeNatureSol()));
					descriptionGeologiqueService.save(dg);
					List<DescriptionGeologique> descriptionGeologiques = descriptionGeologiqueService.findByPem_CodeAndIsDeletedFalse(code);
					for(int i=0;i<descriptionGeologiques.size();i++)
					{
						res += "<tr><td>"+descriptionGeologiques.get(i).getDescriptionGeologiqueKey().getCoteSuperieure()+"</td><td>"+descriptionGeologiques.get(i).getDescriptionGeologiqueKey().getCoteInferieure()+"</td><td>"+descriptionGeologiques.get(i).getNatureSol().getLibelle()+"</td><td><button coteInferieure=' "+descriptionGeologiques.get(i).getDescriptionGeologiqueKey().getCoteInferieure()+"' coteSuperieure='"+descriptionGeologiques.get(i).getDescriptionGeologiqueKey().getCoteSuperieure()+"' codeNatureSol='"+descriptionGeologiques.get(i).getNatureSol().getCode()+"'  class='btn_edit_descriptionGeologique btn btn-primary btn-xs'><i class='mdi mdi-pencil'></i> valider</button> <button coteInferieure='"+descriptionGeologiques.get(i).getDescriptionGeologiqueKey().getCoteInferieure()+"' coteSuperieure='"+descriptionGeologiques.get(i).getDescriptionGeologiqueKey().getCoteSuperieure()+"' codeNatureSol='"+descriptionGeologiques.get(i).getNatureSol().getCode()+"'  class='btn_delete_descriptionGeologique btn btn-danger btn-xs'><i class='mdi mdi-trash-can-outline'></i> valider</button></td></tr>";
					}
				}
			catch(Exception e){
				
					System.out.println(e);
				}
			
    	}
		
		return res;
        
    }
	
	@PostMapping("/updateForageDescriptionGeologique/{code}")
	public String updateForageDescriptionGeologique(@PathVariable("code") String code,@Validated DescriptionGeologiqueForm descriptionGeologique,BindingResult bindingResult) {
		String res = "";
		//if (!bindingResult.hasErrors()) {
		if (true) {	
			try {
					String[] codification = code.split("-"); 
					DescriptionGeologiqueKey dk = new DescriptionGeologiqueKey(Integer.parseInt(codification[0]),Double.parseDouble(codification[1]),Double.parseDouble(codification[2]));
					DescriptionGeologiqueKey dk2 = new DescriptionGeologiqueKey(Integer.parseInt(codification[0]), descriptionGeologique.getCoteSuperieure(), descriptionGeologique.getCoteInferieure());
					
					DescriptionGeologique dg = descriptionGeologiqueService.findByDescriptionGeologiqueKey(dk);
					dg.setDescriptionGeologiqueKey(dk2);
					dg.setNatureSol(natureSolService.findByCode(descriptionGeologique.getCodeNatureSol()));
					descriptionGeologiqueService.save(dg);
					List<DescriptionGeologique> descriptionGeologiques = descriptionGeologiqueService.findByPem_CodeAndIsDeletedFalse(Integer.parseInt(codification[0]));
					for(int i=0;i<descriptionGeologiques.size();i++)
					{
						res += "<tr><td>"+descriptionGeologiques.get(i).getDescriptionGeologiqueKey().getCoteSuperieure()+"</td><td>"+descriptionGeologiques.get(i).getDescriptionGeologiqueKey().getCoteInferieure()+"</td><td>"+descriptionGeologiques.get(i).getNatureSol().getLibelle()+"</td><td><button coteInferieure=' "+descriptionGeologiques.get(i).getDescriptionGeologiqueKey().getCoteInferieure()+"' coteSuperieure='"+descriptionGeologiques.get(i).getDescriptionGeologiqueKey().getCoteSuperieure()+"' codeNatureSol='"+descriptionGeologiques.get(i).getNatureSol().getCode()+"'  class='btn_edit_descriptionGeologique btn btn-primary btn-xs'><i class='mdi mdi-pencil'></i> valider</button> <button coteInferieure='"+descriptionGeologiques.get(i).getDescriptionGeologiqueKey().getCoteInferieure()+"' coteSuperieure='"+descriptionGeologiques.get(i).getDescriptionGeologiqueKey().getCoteSuperieure()+"' codeNatureSol='"+descriptionGeologiques.get(i).getNatureSol().getCode()+"'  class='btn_delete_descriptionGeologique btn btn-danger btn-xs'><i class='mdi mdi-trash-can-outline'></i> valider</button></td></tr>";
					}
				}
			catch(Exception e){
				
					System.out.println(e);
				}
			
    	}
		
		return res;
        
    }
	
	@PostMapping("/forageEquipementForage/{code}")
	public String forageEquipementForage(@PathVariable("code") int code,@Validated EquipementForageForm equipementForageForm,BindingResult bindingResult) {
		String res = "";
		//if (!bindingResult.hasErrors()) {
		if (true) {	
			try {
					Forage pem = forageService.findByCode(code);
					EquipementForage ef = new EquipementForage();
					ef.setPem(pem);
					ef.setNatureTube(natureTubeService.findByCode(equipementForageForm.getCodeNatureTube()));
					ef.setTypeTube(typeTubeService.findByCode(equipementForageForm.getCodeTypeTube()));
					ef.setDiametre(equipementForageForm.getDiametre());
					ef.setCoteSuperieure(equipementForageForm.getCoteSuperieure());
					ef.setCoteInferieure(equipementForageForm.getCoteInferieure());
					
					equipementForageService.save(ef);
					List<EquipementForage> equipementForages = equipementForageService.findByIsDeletedFalse();
					for(int i=0;i<equipementForages.size();i++)
					{
						res += "<tr><td>"+equipementForages.get(i).getCoteSuperieure()+"</td><td>"+equipementForages.get(i).getCoteInferieure()+"</td><td>"+equipementForages.get(i).getNatureTube().getLibelle()+"</td><td>"+equipementForages.get(i).getTypeTube().getLibelle()+"</td><td>"+equipementForages.get(i).getDiametre()+"</td><td><button code='"+equipementForages.get(i).getCode()+"' coteInferieure=' "+equipementForages.get(i).getCoteInferieure()+"' coteSuperieure='"+equipementForages.get(i).getCoteSuperieure()+"' codeNatureTube='"+equipementForages.get(i).getNatureTube().getCode()+"' codeTypeTube='"+equipementForages.get(i).getTypeTube().getCode() +"' diametre='"+equipementForages.get(i).getDiametre()+"' class='btn_edit_equipementForage btn btn-primary btn-xs'><i class='mdi mdi-pencil'></i> valider</button> <button code='"+equipementForages.get(i).getCode()+"' coteInferieure=' "+equipementForages.get(i).getCoteInferieure()+"' coteSuperieure='"+equipementForages.get(i).getCoteSuperieure()+"' codeNatureTube='"+equipementForages.get(i).getNatureTube().getCode()+"' codeTypeTube='"+equipementForages.get(i).getTypeTube().getCode() +"' diametre='"+equipementForages.get(i).getDiametre()+"' class='btn_delete_equipementForage btn btn-danger btn-xs'><i class='mdi mdi-trash-can-outline'></i> valider</button></td></tr>";
					}
				}
			catch(Exception e){
				
					System.out.println(e);
				}
			
    	}
		
		return res;
        
    }
	
	@PostMapping("/updateForageEquipementForage/{code}")
	public String updateForageEquipementForage(@PathVariable("code") Integer code,@Validated EquipementForageForm equipementForage,BindingResult bindingResult) {
		String res = "";
		//if (!bindingResult.hasErrors()) {
		if (true) {	
			try {
					
					EquipementForage ef = equipementForageService.findByCode(code);
					ef.setNatureTube(natureTubeService.findByCode(equipementForage.getCodeNatureTube()));
					ef.setTypeTube(typeTubeService.findByCode(equipementForage.getCodeTypeTube()));
					ef.setDiametre(equipementForage.getDiametre());
					ef.setCoteSuperieure(equipementForage.getCoteSuperieure());
					ef.setCoteInferieure(equipementForage.getCoteInferieure());
					
					equipementForageService.save(ef);
					List<EquipementForage> equipementForages = equipementForageService.findByIsDeletedFalse();
					for(int i=0;i<equipementForages.size();i++)
					{
						res += "<tr><td>"+equipementForages.get(i).getCoteSuperieure()+"</td><td>"+equipementForages.get(i).getCoteInferieure()+"</td><td>"+equipementForages.get(i).getNatureTube().getLibelle()+"</td><td>"+equipementForages.get(i).getTypeTube().getLibelle()+"</td><td>"+equipementForages.get(i).getDiametre()+"</td><td><button code='"+equipementForages.get(i).getCode()+"' coteInferieure=' "+equipementForages.get(i).getCoteInferieure()+"' coteSuperieure='"+equipementForages.get(i).getCoteSuperieure()+"' codeNatureTube='"+equipementForages.get(i).getNatureTube().getCode()+"' codeTypeTube='"+equipementForages.get(i).getTypeTube().getCode() +"' diametre='"+equipementForages.get(i).getDiametre()+"' class='btn_edit_equipementForage btn btn-primary btn-xs'><i class='mdi mdi-pencil'></i> valider</button> <button code='"+equipementForages.get(i).getCode()+"' coteInferieure=' "+equipementForages.get(i).getCoteInferieure()+"' coteSuperieure='"+equipementForages.get(i).getCoteSuperieure()+"' codeNatureTube='"+equipementForages.get(i).getNatureTube().getCode()+"' codeTypeTube='"+equipementForages.get(i).getTypeTube().getCode() +"' diametre='"+equipementForages.get(i).getDiametre()+"' class='btn_delete_equipementForage btn btn-danger btn-xs'><i class='mdi mdi-trash-can-outline'></i> valider</button></td></tr>";
					}
				}
			catch(Exception e){
				
					System.out.println(e);
				}
			
    	}
		
		return res;
        
    }
	
	@PostMapping("/foragePompeInstalle/{code}")
	public String foragePompeInstalle(@PathVariable("code") int code,@Validated PompeInstalleForm pompeInstalleForm,BindingResult bindingResult) {
		String res = "";
		//if (!bindingResult.hasErrors()) {
		if (true) {	
			try {
					Forage pem = forageService.findByCode(code);
					PompeInstalle pompe = new PompeInstalle();
					pompe.setPem(pem);
					pompe.setModelePompe(modelePompeService.findByCode(pompeInstalleForm.getCodeModelePompe()));
					pompe.setMarquePompe(marquePompeService.findByCode(pompeInstalleForm.getCodeMarquePompe()));
					pompe.setDateInstallationPompe(pompeInstalleForm.getDateInstallationPompe());
					
					pompeInstalleService.save(pompe);
					List<PompeInstalle> pompeInstalles = pompeInstalleService.findByIsDeletedFalse();
					for(int i=0;i<pompeInstalles.size();i++)
					{
						res += "<tr><td>"+pompeInstalles.get(i).getModelePompe().getLibelle()+"</td><td>"+pompeInstalles.get(i).getMarquePompe().getLibelle()+"</td><td>"+pompeInstalles.get(i).getDateInstallationPompe()+"</td><td><button code='"+pompeInstalles.get(i).getCode()+"' codeModelePompe='"+pompeInstalles.get(i).getModelePompe().getCode()+"' codeMarquePompe='"+pompeInstalles.get(i).getMarquePompe().getCode() +"' dateInstallationPompe='"+pompeInstalles.get(i).getDateInstallationPompe()+"' class='btn_edit_pompeInstalle btn btn-primary btn-xs'><i class='mdi mdi-pencil'></i> valider</button> <button code='"+pompeInstalles.get(i).getCode()+"' codeModelePompe='"+pompeInstalles.get(i).getModelePompe().getCode()+"' codeMarquePompe='"+pompeInstalles.get(i).getMarquePompe().getCode() +"' dateInstallationPompe='"+pompeInstalles.get(i).getDateInstallationPompe()+"' class='btn_delete_pompeInstalle btn btn-danger btn-xs'><i class='mdi mdi-trash-can-outline'></i> valider</button></td></tr>";
					}
				}
			catch(Exception e){
				
					System.out.println(e);
				}
			
    	}
		
		return res;
        
    }
	
	@PostMapping("/updateForagePompeInstalle/{code}")
	public String updateForagePompeInstalle(@PathVariable("code") Integer code,@Validated PompeInstalleForm pompeInstalle,BindingResult bindingResult) {
		String res = "";
		//if (!bindingResult.hasErrors()) {
		if (true) {	
			try {
					
					PompeInstalle pompe = pompeInstalleService.findByCode(code);
					pompe.setModelePompe(modelePompeService.findByCode(pompeInstalle.getCodeModelePompe()));
					pompe.setMarquePompe(marquePompeService.findByCode(pompeInstalle.getCodeMarquePompe()));
					pompe.setDateInstallationPompe(pompeInstalle.getDateInstallationPompe());
					
					pompeInstalleService.save(pompe);
					List<PompeInstalle> pompeInstalles = pompeInstalleService.findByIsDeletedFalse();
					for(int i=0;i<pompeInstalles.size();i++)
					{
						res += "<tr><td>"+pompeInstalles.get(i).getModelePompe().getLibelle()+"</td><td>"+pompeInstalles.get(i).getMarquePompe().getLibelle()+"</td><td>"+pompeInstalles.get(i).getDateInstallationPompe()+"</td><td><button code='"+pompeInstalles.get(i).getCode()+"' codeModelePompe='"+pompeInstalles.get(i).getModelePompe().getCode()+"' codeMarquePompe='"+pompeInstalles.get(i).getMarquePompe().getCode() +"' dateInstallationPompe='"+pompeInstalles.get(i).getDateInstallationPompe()+"' class='btn_edit_pompeInstalle btn btn-primary btn-xs'><i class='mdi mdi-pencil'></i> valider</button> <button code='"+pompeInstalles.get(i).getCode()+"' codeModelePompe='"+pompeInstalles.get(i).getModelePompe().getCode()+"' codeMarquePompe='"+pompeInstalles.get(i).getMarquePompe().getCode() +"' dateInstallationPompe='"+pompeInstalles.get(i).getDateInstallationPompe()+"' class='btn_delete_pompeInstalle btn btn-danger btn-xs'><i class='mdi mdi-trash-can-outline'></i> valider</button></td></tr>";
					}
				}
			catch(Exception e){
				
					System.out.println(e);
				}
			
    	}
		
		return res;
        
    }
	
	@PostMapping("/foragePrestataire/{code}")
	public String foragePrestataire(@PathVariable("code") int code,@Validated PrestataireForm prestataire,BindingResult bindingResult) {
		String res = "";
		//if (!bindingResult.hasErrors()) {
		if (true) {	
			try {
					Forage pem = forageService.findByCode(code);
					Prestataire p = new Prestataire();
					p.setPem(pem);
					p.setTypePrestation(typePrestationService.findByCode(prestataire.getCodeTypePrestation()));
					p.setCoutPrestation(prestataire.getCoutPrestation());
					p.setNomPrestataire(prestataire.getNomPrestataire());
					prestataireService.save(p);
					List<Prestataire> prestataires = prestataireService.findByPem_CodeAndIsDeletedFalse(code);
					for(int i=0;i<prestataires.size();i++)
					{
						res += "<tr><td>"+prestataires.get(i).getTypePrestation().getLibelle()+"</td><td>"+prestataires.get(i).getNomPrestataire()+"</td><td>"+prestataires.get(i).getCoutPrestation()+"</td><td><button code='"+prestataires.get(i).getCode()+"' codeTypePrestation='"+prestataires.get(i).getTypePrestation().getCode()+"' nomPrestataire='"+prestataires.get(i).getNomPrestataire()+"' coutPrestation='"+prestataires.get(i).getCoutPrestation()+"'  class='btn_edit_prestataire btn btn-primary btn-xs'><i class='mdi mdi-pencil'></i> valider</button> <button code='"+prestataires.get(i).getCode()+"' codeTypePrestation='"+prestataires.get(i).getTypePrestation().getCode()+"' nomPrestataire='"+prestataires.get(i).getNomPrestataire()+"' coutPrestation=' "+prestataires.get(i).getCoutPrestation()+"'   class='btn_delete_prestataire btn btn-danger btn-xs'><i class='mdi mdi-trash-can-outline'></i> valider</button></td></tr>";
					}
				}
			catch(Exception e){
				
					System.out.println(e);
				}
			
    	}
		
		return res;
        
    }
	
	@PostMapping("/updateForagePrestataire/{code}")
	public String updateForagePrestataire(@PathVariable("code") Integer code,@Validated PrestataireForm prestataire,BindingResult bindingResult) {
		String res = "";
		//if (!bindingResult.hasErrors()) {
		if (true) {	
			try {
					
					Prestataire p = prestataireService.findByCode(code);
					p.setTypePrestation(typePrestationService.findByCode(prestataire.getCodeTypePrestation()));
					p.setCoutPrestation(prestataire.getCoutPrestation());
					p.setNomPrestataire(prestataire.getNomPrestataire());
					prestataireService.save(p);
					List<Prestataire> prestataires = prestataireService.findByPem_CodeAndIsDeletedFalse(p.getPem().getCode());;
					for(int i=0;i<prestataires.size();i++)
					{
						res += "<tr><td>"+prestataires.get(i).getTypePrestation().getLibelle()+"</td><td>"+prestataires.get(i).getNomPrestataire()+"</td><td>"+prestataires.get(i).getCoutPrestation()+"</td><td><button code='"+prestataires.get(i).getCode()+"' codeTypePrestation='"+prestataires.get(i).getTypePrestation().getCode()+"' nomPrestataire='"+prestataires.get(i).getNomPrestataire()+"' coutPrestation='"+prestataires.get(i).getCoutPrestation()+"'  class='btn_edit_prestataire btn btn-primary btn-xs'><i class='mdi mdi-pencil'></i> valider</button> <button code='\"+prestataires.get(i).getCode()+\"' codeTypePrestation='\"+prestataires.get(i).getTypePrestation().getCode()+\"' nomPrestataire='\"+prestataires.get(i).getNomPrestataire()+\"' coutPrestation=' "+prestataires.get(i).getCoutPrestation()+"'   class='btn_delete_prestataire btn btn-danger btn-xs'><i class='mdi mdi-trash-can-outline'></i> valider</button></td></tr>";
					}
				}
			catch(Exception e){
				
					System.out.println(e);
				}
			
    	}
		
		return res;
        
    }
	
	@PostMapping("/foragePiezometre/{code}")
	public String foragePiezometre(@PathVariable("code") int code,@Validated PiezometreForm piezometreForm,BindingResult bindingResult) {
		String res = "";
		//if (!bindingResult.hasErrors()) {
		if (true) {	
			try {
					Forage pem = forageService.findByCode(code);
					Piezometre p = new Piezometre();
					p.setPem(pem);
					p.setDateObservation(piezometreForm.getDateObservation());
					p.setHeureObservation(piezometreForm.getHeureObservation());
					p.setProfondeurPiezo(piezometreForm.getProfondeurPiezo());
					p.setHauteurPiezo(piezometreForm.getHauteurPiezo());
					
					piezometreService.save(p);
					List<Piezometre> piezometres = piezometreService.findByIsDeletedFalse();
					for(int i=0;i<piezometres.size();i++)
					{
						res += "<tr><td>"+piezometres.get(i).getDateObservation()+"</td><td>"+piezometres.get(i).getHeureObservation()+"</td><td>"+piezometres.get(i).getProfondeurPiezo()+"</td><td>"+piezometres.get(i).getHauteurPiezo()+"</td><td><button code='"+piezometres.get(i).getCode()+"' dateObservation=' "+piezometres.get(i).getDateObservation()+"' heureObservation='"+piezometres.get(i).getHeureObservation()+"' profondeurPiezo='"+piezometres.get(i).getProfondeurPiezo()+"' hauteurPiezo='"+piezometres.get(i).getHauteurPiezo() +"' class='btn_edit_piezometre btn btn-primary btn-xs'><i class='mdi mdi-pencil'></i> valider</button> <button code='"+piezometres.get(i).getCode()+"' dateObservation=' "+piezometres.get(i).getDateObservation()+"' heureObservation='"+piezometres.get(i).getHeureObservation()+"' profondeurPiezo='"+piezometres.get(i).getProfondeurPiezo()+"' hauteurPiezo='"+piezometres.get(i).getHauteurPiezo() +"' class='btn_delete_piezometre btn btn-danger btn-xs'><i class='mdi mdi-trash-can-outline'></i> valider</button></td></tr>";
					}
				}
			catch(Exception e){
				
					System.out.println(e);
				}
			
    	}
		
		return res;
        
    }
	
	@PostMapping("/updateForagePiezometre/{code}")
	public String updateForagePiezometre(@PathVariable("code") Integer code,@Validated PiezometreForm piezometre,BindingResult bindingResult) {
		String res = "";
		//if (!bindingResult.hasErrors()) {
		if (true) {	
			try {
					
					Piezometre p = piezometreService.findByCode(code);
					p.setDateObservation(piezometre.getDateObservation());
					p.setHeureObservation(piezometre.getHeureObservation());
					p.setProfondeurPiezo(piezometre.getProfondeurPiezo());
					p.setHauteurPiezo(piezometre.getHauteurPiezo());
					
					piezometreService.save(p);
					List<Piezometre> piezometres = piezometreService.findByIsDeletedFalse();
					for(int i=0;i<piezometres.size();i++)
					{
						res += "<tr><td>"+piezometres.get(i).getDateObservation()+"</td><td>"+piezometres.get(i).getHeureObservation()+"</td><td>"+piezometres.get(i).getProfondeurPiezo()+"</td><td>"+piezometres.get(i).getHauteurPiezo()+"</td><td><button code='"+piezometres.get(i).getCode()+"' dateObservation=' "+piezometres.get(i).getDateObservation()+"' heureObservation='"+piezometres.get(i).getHeureObservation()+"' profondeurPiezo='"+piezometres.get(i).getProfondeurPiezo()+"' hauteurPiezo='"+piezometres.get(i).getHauteurPiezo() +"' class='btn_edit_piezometre btn btn-primary btn-xs'><i class='mdi mdi-pencil'></i> valider</button> <button code='"+piezometres.get(i).getCode()+"' dateObservation=' "+piezometres.get(i).getDateObservation()+"' heureObservation='"+piezometres.get(i).getHeureObservation()+"' profondeurPiezo='"+piezometres.get(i).getProfondeurPiezo()+"' hauteurPiezo='"+piezometres.get(i).getHauteurPiezo() +"' class='btn_delete_piezometre btn btn-danger btn-xs'><i class='mdi mdi-trash-can-outline'></i> valider</button></td></tr>";
					}
				}
			catch(Exception e){
				
					System.out.println(e);
				}
			
    	}
		
		return res;
        
    }
	
	@PostMapping("/puitsInfosGenerales/{code}")
	public int infosGeneralesPuits(@PathVariable("code") int code,@Validated PuitsInfosGeneralesForm puits,BindingResult bindingResult) {
		//if (!bindingResult.hasErrors()) {
		if (true) {	
			try {
				System.out.println("debut.........");
				PuitsCimente P = puitsCimenteService.findByCode(code);
				P.setNumeroIRH(puits.getNumeroIRH());
				P.setAnneeRealisation(puits.getAnneeRealisation());
				P.setLatitude(puits.getLatitude());
				P.setLongitude(puits.getLongitude());
				P.setFinancement(financementService.findByCode(puits.getCodeFinancement()));
				P.setProjet(projetService.findByCode(puits.getCodeProjet()));
				P.setLocalite(localiteService.findByCode(puits.getCodeLocalite()));
				
				P.setTypePuits(typePuitsService.findByCode(puits.getCodeTypePuits()));
				P.setAmenagementSurface(puits.isAmenagementSurface());
				P.setAntibourbier(puits.isAntibourbier());
				P.setCanalEvacuationEtPuitsPerdu(puits.isCanalEvacuationEtPuitsPerdu());
				P.setAireAssainie(puits.isAireAssainie());
				P.setMurCloture(puits.isMurCloture());
				P.setCouvercle(puits.isCouvercle());
				P.setPuisette(puits.isPuisette());
				P.setFourche(puits.isFourche());
				P.setDeveloppementEffectueAuCuffat(puits.isDeveloppementEffectueAuCuffat());
				P.setCouleurEau(puits.isCouleurEau());
				P.setTrousseCoupante(puits.isTrousseCoupante());
				P.setDalleFond(puits.isDalleFond());
				P.setPompeImmergee(puits.isPompeImmergee());
				P.setDateMesure(puits.getDateMesure());
				P.setTypeExhaure(typeExhaureService.findByCode(puits.getCodeTypeExhaure()));
				//P.setEtatOuvrage(puits.isEtatOuvrage());
				P.setEtatOuvrage(etatOuvrageService.findByCode(puits.getCodeEtatOuvrage()));
				
				P.setPotabiliteEau(puits.isPotabiliteEau());
				P.setESCIEHDureePompage(puits.getESCIEHDureePompage());
				P.setESCIEHRabattementMesure(puits.getESCIEHRabattementMesure());
				P.setESCIEHDebitMaximumEstime(puits.getESCIEHDebitMaximumEstime());
				P.setAbreuvoirs(puits.getAbreuvoirs());
				P.setPortique(puits.getPortique());
				P.setProfondeurTotale(puits.getProfondeurTotale());
				P.setProfondeurCuvelee(puits.getProfondeurCuvelee());
				P.setDiametreCuvelage(puits.getDiametreCuvelage());
				P.setProfondeurNiveauStatique(puits.getProfondeurNiveauStatique());
				P.setDiametreCaptage(puits.getDiametreCaptage());
				P.setNombreBusesCaptage(puits.getNombreBusesCaptage());
				P.setProfondeurSommetColonneCaptage(puits.getProfondeurSommetColonneCaptage());
				P.setHauteurCaptage(puits.getHauteurCaptage());
				P.setHauteurRecouvrement(puits.getHauteurRecouvrement());
				P.setHauteurMassifFiltrant(puits.getHauteurMassifFiltrant());
				P.setRenseignementDivers(puits.getRenseignementDivers());

				puitsCimenteService.save(P);
				System.out.println("fin.........");
					
				}
			catch(Exception e){
				System.out.println("erreur.........");
					System.out.println(e);
				}
			
    	}
		
		
		return 1;
        
    }
	
	@PostMapping("/puitsPrestataire/{code}")
	public String puitsPrestataire(@PathVariable("code") int code,@Validated PrestataireForm prestataire,BindingResult bindingResult) {
		String res = "";
		//if (!bindingResult.hasErrors()) {
		if (true) {	
			try {
					PuitsCimente pem = puitsCimenteService.findByCode(code);
					Prestataire p = new Prestataire();
					p.setPem(pem);
					p.setTypePrestation(typePrestationService.findByCode(prestataire.getCodeTypePrestation()));
					p.setCoutPrestation(prestataire.getCoutPrestation());
					p.setNomPrestataire(prestataire.getNomPrestataire());
					prestataireService.save(p);
					List<Prestataire> prestataires = prestataireService.findByPem_CodeAndIsDeletedFalse(code);
					for(int i=0;i<prestataires.size();i++)
					{
						res += "<tr><td>"+prestataires.get(i).getTypePrestation().getLibelle()+"</td><td>"+prestataires.get(i).getNomPrestataire()+"</td><td>"+prestataires.get(i).getCoutPrestation()+"</td><td><button code='"+prestataires.get(i).getCode()+"' codeTypePrestation='"+prestataires.get(i).getTypePrestation().getCode()+"' nomPrestataire='"+prestataires.get(i).getNomPrestataire()+"' coutPrestation='"+prestataires.get(i).getCoutPrestation()+"'  class='btn_edit_prestataire btn btn-primary btn-xs'><i class='mdi mdi-pencil'></i> valider</button> <button code='"+prestataires.get(i).getCode()+"' codeTypePrestation='"+prestataires.get(i).getTypePrestation().getCode()+"' nomPrestataire='"+prestataires.get(i).getNomPrestataire()+"' coutPrestation=' "+prestataires.get(i).getCoutPrestation()+"'   class='btn_delete_prestataire btn btn-danger btn-xs'><i class='mdi mdi-trash-can-outline'></i> valider</button></td></tr>";
					}
				}
			catch(Exception e){
				
					System.out.println(e);
				}
			
    	}
		
		return res;
        
    }
	
	@PostMapping("/updatePuitsPrestataire/{code}")
	public String updatePuitsPrestataire(@PathVariable("code") Integer code,@Validated PrestataireForm prestataire,BindingResult bindingResult) {
		String res = "";
		//if (!bindingResult.hasErrors()) {
		if (true) {	
			try {
					
					Prestataire p = prestataireService.findByCode(code);
					p.setTypePrestation(typePrestationService.findByCode(prestataire.getCodeTypePrestation()));
					p.setCoutPrestation(prestataire.getCoutPrestation());
					p.setNomPrestataire(prestataire.getNomPrestataire());
					prestataireService.save(p);
					List<Prestataire> prestataires = prestataireService.findByPem_CodeAndIsDeletedFalse(p.getPem().getCode());
					for(int i=0;i<prestataires.size();i++)
					{
						res += "<tr><td>"+prestataires.get(i).getTypePrestation().getLibelle()+"</td><td>"+prestataires.get(i).getNomPrestataire()+"</td><td>"+prestataires.get(i).getCoutPrestation()+"</td><td><button code='"+prestataires.get(i).getCode()+"' codeTypePrestation='"+prestataires.get(i).getTypePrestation().getCode()+"' nomPrestataire='"+prestataires.get(i).getNomPrestataire()+"' coutPrestation='"+prestataires.get(i).getCoutPrestation()+"'  class='btn_edit_prestataire btn btn-primary btn-xs'><i class='mdi mdi-pencil'></i> valider</button> <button code='\"+prestataires.get(i).getCode()+\"' codeTypePrestation='\"+prestataires.get(i).getTypePrestation().getCode()+\"' nomPrestataire='\"+prestataires.get(i).getNomPrestataire()+\"' coutPrestation=' "+prestataires.get(i).getCoutPrestation()+"'   class='btn_delete_prestataire btn btn-danger btn-xs'><i class='mdi mdi-trash-can-outline'></i> valider</button></td></tr>";
					}
				}
			catch(Exception e){
				
					System.out.println(e);
				}
			
    	}
		
		return res;
        
    }
	
	@PostMapping("/puitsDescriptionGeologique/{code}")
	public String puitsDescriptionGeologique(@PathVariable("code") int code,@Validated DescriptionGeologiqueForm descriptionGeologique,BindingResult bindingResult) {
		String res = "";
		//if (!bindingResult.hasErrors()) {
		if (true) {	
			try {
					PuitsCimente pem = puitsCimenteService.findByCode(code);
					DescriptionGeologique dg = new DescriptionGeologique();
					dg.setDescriptionGeologiqueKey(new DescriptionGeologiqueKey(code, descriptionGeologique.getCoteSuperieure(), descriptionGeologique.getCoteInferieure()));
					dg.setPem(pem);
					dg.setNatureSol(natureSolService.findByCode(descriptionGeologique.getCodeNatureSol()));
					descriptionGeologiqueService.save(dg);
					List<DescriptionGeologique> descriptionGeologiques = descriptionGeologiqueService.findByPem_CodeAndIsDeletedFalse(code);
					for(int i=0;i<descriptionGeologiques.size();i++)
					{
						res += "<tr><td>"+descriptionGeologiques.get(i).getDescriptionGeologiqueKey().getCoteSuperieure()+"</td><td>"+descriptionGeologiques.get(i).getDescriptionGeologiqueKey().getCoteInferieure()+"</td><td>"+descriptionGeologiques.get(i).getNatureSol().getLibelle()+"</td><td><button coteInferieure=' "+descriptionGeologiques.get(i).getDescriptionGeologiqueKey().getCoteInferieure()+"' coteSuperieure='"+descriptionGeologiques.get(i).getDescriptionGeologiqueKey().getCoteSuperieure()+"' codeNatureSol='"+descriptionGeologiques.get(i).getNatureSol().getCode()+"'  class='btn_edit_descriptionGeologique btn btn-primary btn-xs'><i class='mdi mdi-pencil'></i> valider</button> <button coteInferieure='"+descriptionGeologiques.get(i).getDescriptionGeologiqueKey().getCoteInferieure()+"' coteSuperieure='"+descriptionGeologiques.get(i).getDescriptionGeologiqueKey().getCoteSuperieure()+"' codeNatureSol='"+descriptionGeologiques.get(i).getNatureSol().getCode()+"'  class='btn_delete_descriptionGeologique btn btn-danger btn-xs'><i class='mdi mdi-trash-can-outline'></i> valider</button></td></tr>";
					}
				}
			catch(Exception e){
				
					System.out.println(e);
				}
			
    	}
		
		return res;
        
    }
	
	@PostMapping("/updatePuitsDescriptionGeologique/{code}")
	public String updatePuitsDescriptionGeologique(@PathVariable("code") String code,@Validated DescriptionGeologiqueForm descriptionGeologique,BindingResult bindingResult) {
		String res = "";
		//if (!bindingResult.hasErrors()) {
		if (true) {	
			try {
					String[] codification = code.split("-"); 
					DescriptionGeologiqueKey dk = new DescriptionGeologiqueKey(Integer.parseInt(codification[0]),Double.parseDouble(codification[1]),Double.parseDouble(codification[2]));
					DescriptionGeologiqueKey dk2 = new DescriptionGeologiqueKey(Integer.parseInt(codification[0]), descriptionGeologique.getCoteSuperieure(), descriptionGeologique.getCoteInferieure());
					
					DescriptionGeologique dg = descriptionGeologiqueService.findByDescriptionGeologiqueKey(dk);
					dg.setDescriptionGeologiqueKey(dk2);
					dg.setNatureSol(natureSolService.findByCode(descriptionGeologique.getCodeNatureSol()));
					descriptionGeologiqueService.save(dg);
					List<DescriptionGeologique> descriptionGeologiques = descriptionGeologiqueService.findByPem_CodeAndIsDeletedFalse(Integer.parseInt(codification[0]));
					for(int i=0;i<descriptionGeologiques.size();i++)
					{
						res += "<tr><td>"+descriptionGeologiques.get(i).getDescriptionGeologiqueKey().getCoteSuperieure()+"</td><td>"+descriptionGeologiques.get(i).getDescriptionGeologiqueKey().getCoteInferieure()+"</td><td>"+descriptionGeologiques.get(i).getNatureSol().getLibelle()+"</td><td><button coteInferieure=' "+descriptionGeologiques.get(i).getDescriptionGeologiqueKey().getCoteInferieure()+"' coteSuperieure='"+descriptionGeologiques.get(i).getDescriptionGeologiqueKey().getCoteSuperieure()+"' codeNatureSol='"+descriptionGeologiques.get(i).getNatureSol().getCode()+"'  class='btn_edit_descriptionGeologique btn btn-primary btn-xs'><i class='mdi mdi-pencil'></i> valider</button> <button coteInferieure='"+descriptionGeologiques.get(i).getDescriptionGeologiqueKey().getCoteInferieure()+"' coteSuperieure='"+descriptionGeologiques.get(i).getDescriptionGeologiqueKey().getCoteSuperieure()+"' codeNatureSol='"+descriptionGeologiques.get(i).getNatureSol().getCode()+"'  class='btn_delete_descriptionGeologique btn btn-danger btn-xs'><i class='mdi mdi-trash-can-outline'></i> valider</button></td></tr>";
					}
				}
			catch(Exception e){
				
					System.out.println(e);
				}
			
    	}
		
		return res;
        
    }
	
	
}
