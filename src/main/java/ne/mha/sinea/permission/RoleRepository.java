package ne.mha.sinea.permission;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepository extends CrudRepository<Role, Integer> {

	Role findByCode(Integer code);
	Role findByLibelle(String libelle);
	Role findByIsDeletedFalseAndLibelle(String libelle);
	List<Role> findByIsDeletedFalseOrderByLibelleAsc();
	List<Role> findByIsDeletedFalse();
	List<Role> findByIsDeletedTrue();
	
	List<Role> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(int[] listeId);
}
