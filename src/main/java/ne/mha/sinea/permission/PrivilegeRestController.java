package ne.mha.sinea.permission;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class PrivilegeRestController {

	@Autowired
	PrivilegeRepository privilegeService;
	
	@PreAuthorize("hasAuthority('gestion des accès')")
	@PostMapping("/privilege")
	public int addPrivilege(@Validated PrivilegeForm privilegeForm,BindingResult bindingResult, Model model) {
		Privilege savedPrivilege = new Privilege();
		if (!bindingResult.hasErrors()) {
			
			try {
				
				Privilege P = new Privilege();
				P.setLibelle(privilegeForm.getLibelle());
				savedPrivilege = privilegeService.save(P);
				
				
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedPrivilege.getCode();
		
        
    }
	@PreAuthorize("hasAuthority('gestion des accès')")
	@PostMapping("/updatePrivilege")
    public int updatePrivilege(@Validated PrivilegeForm privilegeForm,BindingResult bindingResult, Model model) {
		Privilege savedPrivilege = new Privilege();
		if (!bindingResult.hasErrors()) {
			
			try {
				
				Privilege P = privilegeService.findByCode(privilegeForm.getCode());
				P.setLibelle(privilegeForm.getLibelle());
				savedPrivilege = privilegeService.save(P);
				
				}
			catch(Exception e){
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedPrivilege.getCode();
		
        
    }
	
	@PreAuthorize("hasAuthority('gestion des accès')")
	@PostMapping("/deletePrivilege")
    public int deletePrivilege(@Validated PrivilegeForm privilegeForm,BindingResult bindingResult, Model model) {
		Privilege savedPrivilege = new Privilege();
		if (!bindingResult.hasErrors()) {
			
			try {
				
				Privilege P = privilegeService.findByCode(privilegeForm.getCode());
				P.setDeleted(true);
				savedPrivilege = privilegeService.save(P);
				
				
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedPrivilege.getCode();
		
        
    }
	
	@PreAuthorize("hasAuthority('gestion des accès')")
	@GetMapping("/getPrivilege/{code}")
	public String  listePrivilege(@PathVariable("code") int code) {
		String res = "";
		try{
			
			Privilege privilege = privilegeService.findByCode(code);
			res += ",{ id:"+privilege.getCode()+", pId:0, name:\""+privilege.getLibelle()+"\"}";
			res = "[{ id:0, pId:0, name:\"/\", open:true}"+res+"]";
			
			}
		catch(Exception e){
				
			}
		return res;
	}
	
	
	
}
