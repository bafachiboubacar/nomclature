package ne.mha.sinea.permission;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import ne.mha.sinea.Template;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Controller
public class UserController {
	@Autowired
	RoleRepository roleService;
	@Autowired
	UserRepository userService;
	@Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@PreAuthorize("hasAuthority('gestion des utilisateurs')")
	@GetMapping("/user")
	public String  permission(UserForm userForm,Model model) {
		try{
			List<Role> roles = roleService.findByIsDeletedFalse();
			model.addAttribute("roles", roles);
			List<User> users = userService.findByIsDeletedFalse();
			model.addAttribute("users", users);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Compte utilisateur");
			model.addAttribute("viewPath", "user/user");
		
		
		}
		catch(Exception e){
				
			}
		return Template.defaultTemplate;
		

	}
	
	@PreAuthorize("hasAuthority('gestion des utilisateurs')")
	@PostMapping("/user")
	public String addUsersSubmit(@Validated UserForm userForm, BindingResult bindingResult,
			Model model) {
		if (!bindingResult.hasErrors()) {
			try {
				
				User U = new User();
				U.setNom(userForm.getNom());
				U.setPrenom(userForm.getPrenom());
				U.setLogin(userForm.getLogin());
				U.setMotPasse(bCryptPasswordEncoder.encode(userForm.getMotPasse()));
				U.setActive(true);
				U.setTokenExpire(false);
				List<Role> roles = roleService.findByIsDeletedFalseAndCodeInOrderByLibelleAsc(userForm.getRoles());
				U.setRoles(roles);
				userService.save(U);
				
				model.addAttribute("operationStatus", "operationStatus/success");
				roles = roleService.findByIsDeletedFalse();
				model.addAttribute("roles", roles);
				List<User> users = userService.findByIsDeletedFalse();
				model.addAttribute("users", users);
				
				model.addAttribute("horizontalMenu", "horizontalMenu");
				model.addAttribute("sidebarMenu", "configurationSidebarMenu");
				model.addAttribute("breadcrumb", "breadcrumb");
				model.addAttribute("navigationPath", "Compte utilisateur");
				model.addAttribute("viewPath", "user/user");
				
			} catch (Exception e) {
				model.addAttribute("operationStatus", "operationStatus/unsuccess");
				List<Role> roles = roleService.findByIsDeletedFalse();
				model.addAttribute("roles", roles);
				List<User> users = userService.findByIsDeletedFalse();
				model.addAttribute("users", users);
				
				model.addAttribute("horizontalMenu", "horizontalMenu");
				model.addAttribute("sidebarMenu", "configurationSidebarMenu");
				model.addAttribute("breadcrumb", "breadcrumb");
				model.addAttribute("navigationPath", "Compte utilisateur");
				model.addAttribute("viewPath", "user/user");
				
			}

		} else {
			try {
				model.addAttribute("operationStatus", "operationStatus/unsuccess");

				List<Role> roles = roleService.findByIsDeletedFalse();
				model.addAttribute("roles", roles);
				List<User> users = userService.findByIsDeletedFalse();
				model.addAttribute("users", users);
				
				model.addAttribute("horizontalMenu", "horizontalMenu");
				model.addAttribute("sidebarMenu", "configurationSidebarMenu");
				model.addAttribute("breadcrumb", "breadcrumb");
				model.addAttribute("navigationPath", "Compte utilisateur");
				model.addAttribute("viewPath", "user/user");
				
			} catch (Exception e) {

			}
		}

		return Template.defaultTemplate;

	}
	@PreAuthorize("hasAuthority('gestion des utilisateurs')")
	@GetMapping("/updateUser/{codeUser}")
	public String updateUser(@PathVariable("codeUser") int codeUser,
			UserForm userForm, Model model) {
		try {
			
			User user = userService.findByCode(codeUser);
			userForm.setCode(user.getCode());
			userForm.setNom(user.getNom());
			userForm.setPrenom(user.getPrenom());
			userForm.setLogin(user.getLogin());
			int[] listRoles = new int[user.getRoles().size()];
			for(int i=0;i<user.getRoles().size();i++)
			{
				listRoles[i] = user.getRoles().get(i).getCode();
			}
			userForm.setRoles(listRoles);
			
			List<Role> roles = roleService.findByIsDeletedFalse();
			model.addAttribute("roles", roles);
			List<User> users = userService.findByIsDeletedFalse();
			model.addAttribute("users", users);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Compte utilisateur");
			model.addAttribute("viewPath", "user/user");
			
		} catch (Exception e) {

		}
		return Template.defaultTemplate;

	}

	@PreAuthorize("hasAuthority('gestion des utilisateurs')")
	@PostMapping("/updateUser/{codeUser}")
	public RedirectView updateUserSubmit(@Validated UserForm userForm, BindingResult bindingResult,
			@PathVariable("codeUser") int codeUser, Model model, RedirectAttributes redirectAttributes) {
		final RedirectView redirectView = new RedirectView("/user", true);
		if (!bindingResult.hasErrors()) {
			try {
				User U = userService.findByCode(codeUser);
				U.setNom(userForm.getNom());
				U.setPrenom(userForm.getPrenom());
				U.setLogin(userForm.getLogin());
				U.setMotPasse(bCryptPasswordEncoder.encode(userForm.getMotPasse()));
				//U.setActive(true);
				//U.setTokenExpire(false);
				List<Role> roles = roleService.findByIsDeletedFalseAndCodeInOrderByLibelleAsc(userForm.getRoles());
				U.setRoles(roles);
				userService.save(U);

				redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/success");

			} catch (Exception e) {
				redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/unsuccess");
			}
		} else {

		}

		return redirectView;

	}

	@PreAuthorize("hasAuthority('gestion des utilisateurs')")
	@GetMapping("/deleteUser/{codeUser}")
	public RedirectView deleteUser(@PathVariable("codeUser") int codeUser, Model model,
			RedirectAttributes redirectAttributes) {
		final RedirectView redirectView = new RedirectView("/user", true);
		try {
			User U = userService.findByCode(codeUser);
			U.setDeleted(true);
			userService.save(U);

			redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/success");
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/unsuccess");
		}

		return redirectView;

	}
		
	
}
