package ne.mha.sinea;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import com.sun.istack.NotNull;
import lombok.Data;

/**
 * 
 * @author nasser 
 * cette class Abstraite sert de super class a toutes les autres
 *         class Entity en permetant d'econominer du code pour les propriété
 *         qu'elles ont tous en communes
 *
 */
@Data
@MappedSuperclass
@EnableJpaAuditing(auditorAwareRef = "auditorProvider")
public abstract  class CommonProperties {
	
	
	@LastModifiedBy
	@Column(name = "modified_by")
	private String modifiedBy;
	
	@LastModifiedDate
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_at")
	private Date  modifiedAt;
	
	@NotNull
	@CreatedBy
	@Column(name = "created_by")
	private String createdBy;
	
	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@CreatedDate
	@Column(name = "created_at")
	private Date createdAt;
	
	@NotNull
	@Column(name = "is_deleted")
	private boolean isDeleted = false;
	
	/*@NotNull
	@Column(name = "is_deleted_central")
	private boolean isDeletedCentral;
	
	@NotNull
	@Column(name = "is_deleted_regional")
	private boolean isDeletedRegional;*/
	
	@PreUpdate
	public void setUpdatedAt() { 
		String createdByUser = getUsernameOfAuthenticatedUser();
	    this.modifiedAt= new Date(); 
	    this.modifiedBy = createdByUser;
	}
	
	@PrePersist
    public void prePersist() {
        String createdByUser = getUsernameOfAuthenticatedUser();
        this.createdBy = createdByUser;
        this.createdAt= new Date();
    }
	
	private String getUsernameOfAuthenticatedUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
 
        if (authentication == null || !authentication.isAuthenticated()) {
            return null;
        }
 
        return ((User) authentication.getPrincipal()).getUsername();
    }
}